import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PIDPage } from './pid.page';
var routes = [
    {
        path: '',
        component: PIDPage
    }
];
var PIDPageModule = /** @class */ (function () {
    function PIDPageModule() {
    }
    PIDPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PIDPage]
        })
    ], PIDPageModule);
    return PIDPageModule;
}());
export { PIDPageModule };
//# sourceMappingURL=pid.module.js.map