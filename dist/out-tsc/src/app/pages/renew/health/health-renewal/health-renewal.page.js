import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { CommonService } from 'src/app/services/common.service';
import { AlertController, } from '@ionic/angular';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { all } from 'q';
var HealthRenewalPage = /** @class */ (function () {
    function HealthRenewalPage(cs, alertCtrl, router) {
        this.cs = cs;
        this.alertCtrl = alertCtrl;
        this.router = router;
        this.showRenewal = false;
        this.yeararray = [];
        this.daysArray = [];
        this.temparr = [];
        this.showNoRecords = false;
    }
    HealthRenewalPage.prototype.ngOnInit = function () {
        var iskeralaCessFeature = localStorage.getItem('isKeralaCessEnabled');
        if (iskeralaCessFeature == 'Y') {
            this.isKeralaCessEnabled = true;
        }
        else {
            this.isKeralaCessEnabled = false;
        }
        this.minDate = moment().format('YYYY-MM-DD');
        var current_year = new Date().getFullYear();
        this.yeararray.push(current_year);
        this.yeararray.push(current_year + 1);
        for (var i = 0; i < 15; i++) {
            current_year = current_year - 1;
            this.yeararray.push(current_year);
        }
        this.yeararray.sort();
        var d = new Date();
        this.selectedMonth = d.getMonth() + 1;
        this.selectedYear = new Date().getFullYear();
        var totalDays = new Date(this.selectedYear, this.selectedMonth, 0).getDate();
        this.getDaysOnBasisofMonthandYear(totalDays);
        this.selectedProduct = 6;
        this.slideOpts = {
            loop: false,
            slidesPerView: "5",
            centeredSlides: false,
            autoplayDisableOnInteraction: false,
            slideNext: function (object, slideView) {
                slideView.slideNext(500).then(function () {
                });
            }
        };
        var productData = { productName: 'CHI', productId: this.selectedProduct };
        localStorage.setItem('product', JSON.stringify(productData));
        this.getRelation();
        this.productType = '6';
    };
    HealthRenewalPage.prototype.getAllData = function (d) {
        var tempmonth, tempday, tempRN;
        if (d.day == 1) {
            tempday = '01';
        }
        else if (d.day == 2) {
            tempday = '02';
        }
        else if (d.day == 3) {
            tempday = '03';
        }
        else if (d.day == 4) {
            tempday = '04';
        }
        else if (d.day == 5) {
            tempday = '05';
        }
        else if (d.day == 6) {
            tempday = '06';
        }
        else if (d.day == 7) {
            tempday = '07';
        }
        else if (d.day == 8) {
            tempday = '08';
        }
        else if (d.day == 9) {
            tempday = '09';
        }
        else {
            tempday = d.day;
        }
        tempmonth = this.selectedMonth;
        var tempData = tempday + '/' + tempmonth + '/' + this.selectedYear;
        if (d.day == "All") {
            this.RNFetch();
            this.showNoRecords = false;
        }
        else {
            this.cs.showLoader = true;
            this.ListData = [];
            for (var i = 0; i < this.RNData.length; i++) {
                var str = void 0;
                str = this.RNData[i].RenewalDate.split('/');
                if (tempday == str[0]) {
                    this.ListData.push(this.RNData[i]);
                }
            }
            if (this.ListData.length == 0) {
                this.showNoRecords = true;
            }
            else {
                this.showNoRecords = false;
            }
            this.cs.showLoader = false;
        }
    };
    HealthRenewalPage.prototype.getDaysOnBasisofMonthandYear = function (totalDays) {
        this.daysArray = [];
        this.daysArray.push({ day: "All", count: 0 });
        for (var i = 1; i <= totalDays; i++) {
            this.daysArray.push({ day: i, count: 0 });
        }
    };
    HealthRenewalPage.prototype.renewal = function (ev) {
        // console.log("Insta Pay", this.instaPaydata);
        document.getElementById("myNav2").style.height = "100%";
    };
    HealthRenewalPage.prototype.RNFetch = function () {
        var _this = this;
        this.cs.showLoader = true;
        var totalDays = new Date(this.selectedYear, this.selectedMonth, 0).getDate();
        var tempday;
        var count = 0;
        var postData = {
            "ProductCategory": 2,
            "ProductSubCategory": this.selectedProduct,
            "Month": this.selectedMonth,
            "Year": this.selectedYear,
            "UserType": "Agent",
        };
        this.cs.postWithParams('/api/HealthMaster/GetRenewalReminderSearch', postData).then(function (res) {
            _this.RNDatatemp = res;
            _this.daysArray = [];
            _this.RNData = _this.RNDatatemp.CustomerSearch;
            _this.showNoRecords = false;
            _this.ListData = _this.RNData;
            //this.RNData1 = res[0];
            var str;
            if (_this.RNData.length > 0) {
                _this.daysArray.push({ day: "All", count: _this.RNData.length });
                for (var i = 1; i <= totalDays; i++) {
                    if (i == 1) {
                        tempday = '01';
                    }
                    else if (i == 2) {
                        tempday = '02';
                    }
                    else if (i == 3) {
                        tempday = '03';
                    }
                    else if (i == 4) {
                        tempday = '04';
                    }
                    else if (i == 5) {
                        tempday = '05';
                    }
                    else if (i == 6) {
                        tempday = '06';
                    }
                    else if (i == 7) {
                        tempday = '07';
                    }
                    else if (i == 8) {
                        tempday = '08';
                    }
                    else if (i == 9) {
                        tempday = '09';
                    }
                    else {
                        tempday = i;
                    }
                    count = 0;
                    for (var c = 0; c < _this.RNData.length; c++) {
                        if (_this.RNData[c].RenewalDate != null && _this.RNData[c].RenewalDate != undefined) {
                            str = _this.RNData[c].RenewalDate.split('/');
                        }
                        else {
                            str = [""];
                        }
                        if (str[0] == tempday) {
                            count++;
                        }
                    }
                    _this.daysArray.push({ day: i, count: count });
                }
                var sum = 0;
                for (var i = 1; i < _this.daysArray.length; i++) {
                    sum = sum + _this.daysArray[i].count;
                }
                if (sum == 0) {
                    _this.daysArray[0].count = 0;
                }
            }
            else {
                _this.daysArray = [];
                _this.daysArray.push({ day: "All", count: 0 });
                for (var i = 1; i <= totalDays; i++) {
                    _this.daysArray.push({ day: i, count: 0 });
                }
                _this.showNoRecords = true;
            }
            _this.cs.showLoader = false;
        }).catch(function (err) {
            if (err != undefined) {
                _this.presentAlert(err.error);
                _this.cs.showLoader = false;
            }
        });
    };
    HealthRenewalPage.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HealthRenewalPage.prototype.gotoRenewal = function (selectedRn) {
        this.selectedRenewalPolicy = selectedRn;
        if (this.productType == 30) {
            document.getElementById("myNav3").style.height = "100%";
        }
        else {
            document.getElementById("myNav2").style.height = "100%";
        }
    };
    HealthRenewalPage.prototype.getRelation = function () {
        var _this = this;
        this.cs.showLoader = true;
        this.cs.postWithParams('/api/healthmaster/GetHealthProposalRelationships?Product=CHI', '').then(function (res) {
            console.log("Rel", res);
            if (res.StatusCode == 1) {
                localStorage.setItem('relations', JSON.stringify(res));
                _this.cs.showLoader = false;
                _this.RNFetch();
            }
            else {
                // console.log("Invalid relationship");
                _this.cs.showLoader = false;
                _this.RNFetch();
            }
        }).catch(function (err) {
            //console.log(err);
            _this.cs.showLoader = false;
            _this.RNFetch();
        });
    };
    HealthRenewalPage.prototype.getProduct = function (ev) {
        this.productType = ev.target.value;
        this.selectedProduct = ev.target.value;
        var product, productData;
        if (this.selectedProduct == 6) {
            productData = { productName: 'CHI', productId: this.selectedProduct };
        }
        else if (this.selectedProduct == 30) {
            productData = { productName: 'PPAP', productId: this.selectedProduct };
        }
        else {
            productData = { productName: 'Health Booster', productId: this.selectedProduct };
        }
        localStorage.setItem('product', JSON.stringify(productData));
        // this.getRelation();
        this.RNFetch();
    };
    HealthRenewalPage.prototype.close = function (ev) {
        document.getElementById("myNav2").style.height = "0%";
    };
    HealthRenewalPage.prototype.closePPAP = function (ev) {
        this.customerDOB = null;
        document.getElementById("myNav3").style.height = "0%";
    };
    HealthRenewalPage.prototype.submitPPAPRenewal = function () {
        var _this = this;
        var tempAgeDate = moment().diff(this.customerDOB, 'years');
        if (this.customerDOB == '' || this.customerDOB == undefined || this.customerDOB == null) {
            this.presentAlert('Please enter customer dob');
        }
        else if (tempAgeDate < 1) {
            this.presentAlert('Please select valid customer dob');
        }
        else {
            this.cs.showLoader = true;
            var tempDate = moment(this.customerDOB).format('DD/MM/YYYY');
            var postData = {
                "UserType": "Agent",
                "PolicyNo": this.selectedRenewalPolicy.Policy_Number,
                "DOB": tempDate
            };
            this.cs.postWithParams('/api/renewal/PPAPPolicyRenewalRN', postData).then(function (res) {
                _this.PPAPRNData = res;
                _this.cs.showLoader = false;
                if (_this.PPAPRNData.StatusCode == 0) {
                    _this.presentAlert(_this.PPAPRNData.StatusMessage);
                    document.getElementById("myNav3").style.height = "0%";
                    _this.customerDOB = null;
                }
                else {
                    localStorage.setItem('renewalPolicyData', JSON.stringify(_this.PPAPRNData.PPAPHealthList[0]));
                    document.getElementById("myNav3").style.height = "0%";
                    _this.customerDOB = null;
                    _this.router.navigateByUrl('health-renewal-summary');
                }
            }, function (err) {
                _this.presentAlert(err.error.ExceptionMessage);
                _this.cs.showLoader = false;
                document.getElementById("myNav3").style.height = "0%";
                _this.customerDOB = null;
                //this.route.navigateByUrl('health-renewal-summary');
            });
        }
    };
    HealthRenewalPage.prototype.closeEmail = function (ev) {
        document.getElementById("myNav4").style.height = "0%";
    };
    HealthRenewalPage.prototype.gotoSendEmail = function (selectedRn) {
        this.selectedRenewalPolicy = selectedRn;
        document.getElementById("myNav4").style.height = "100%";
    };
    HealthRenewalPage.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    HealthRenewalPage.prototype.submitEmail = function (ev) {
        var _this = this;
        var product;
        if (this.emailToBeSent == null || this.emailToBeSent == undefined || this.emailToBeSent == '') {
            this.presentAlert('Please enter Email Id');
        }
        else if (!this.validateEmail(this.emailToBeSent)) {
            this.presentAlert('Please enter valid Email Id');
        }
        else {
            this.cs.showLoader = true;
            if (this.productType == 6) {
                product = 'CHI';
            }
            else if (this.productType == 30) {
                product = 'PPAP';
            }
            else {
                product = 'Health Booster';
            }
            var postData = {
                "Policy_Number": this.selectedRenewalPolicy.Policy_Number,
                "Policy_Type": this.selectedRenewalPolicy.Policy_type,
                "Product": "Health",
                "Policy_Enddate": this.selectedRenewalPolicy.Policy_End_Date,
                "SubProduct": product,
                "MailId": this.emailToBeSent
            };
            this.cs.postWithParams('/api/policy/SendMailRnNotification', postData).then(function (res) {
                _this.emailResponse = res;
                if (_this.emailResponse.StatusCode == 1) {
                    _this.presentAlert('Email Sent Successfully');
                }
                else {
                    _this.presentAlert('Email Not Sent');
                }
                _this.emailToBeSent = null;
                document.getElementById("myNav4").style.height = "0%";
                _this.cs.showLoader = false;
            }, function (err) {
                _this.presentAlert(err.error.ExceptionMessage);
                _this.cs.showLoader = false;
                document.getElementById("myNav4").style.height = "0%";
                _this.emailToBeSent = null;
            });
        }
    };
    HealthRenewalPage.prototype.gotoDownloadPdf = function (rn) {
        var _this = this;
        this.selectedRenewalPolicy = rn;
        this.cs.showLoader = true;
        var product;
        if (this.productType == 6) {
            product = 'CHI';
        }
        else if (this.productType == 30) {
            product = 'PPAP';
        }
        else {
            product = 'Health Booster';
        }
        var postData = {
            "Policy_Number": this.selectedRenewalPolicy.Policy_Number,
            "Policy_Type": this.selectedRenewalPolicy.Policy_type,
            "Product": "Health",
            "Policy_Enddate": this.selectedRenewalPolicy.Policy_End_Date,
            "SubProduct": product
        };
        this.cs.postWithParams('/api/policy/RnNotification', postData).then(function (res) {
            _this.pdfResponse = res;
            if (_this.pdfResponse != null && _this.pdfResponse != undefined) {
                var fileName = _this.selectedRenewalPolicy.Policy_Number.split("/").join("_") + '.pdf';
                _this.cs.save('data:application/pdf;base64,' + _this.pdfResponse._buffer, fileName);
            }
            else {
                _this.presentAlert('Sorry no response');
            }
            _this.cs.showLoader = false;
        }, function (err) {
            _this.presentAlert(err.error.ExceptionMessage);
            _this.cs.showLoader = false;
            document.getElementById("myNav4").style.height = "0%";
            _this.emailToBeSent = null;
        });
    };
    HealthRenewalPage.prototype.getMonth = function (ev) {
        this.selectedMonth = ev.target.value;
        var totalDays = new Date(this.selectedYear, this.selectedMonth, 0).getDate();
        this.getDaysOnBasisofMonthandYear(totalDays);
        this.RNFetch();
    };
    HealthRenewalPage.prototype.getYear = function (ev) {
        this.selectedYear = ev.target.value;
        var totalDays = new Date(this.selectedYear, this.selectedMonth, 0).getDate();
        this.getDaysOnBasisofMonthandYear(totalDays);
        this.RNFetch();
    };
    HealthRenewalPage.prototype.getDay = function (d) {
        var tempmonth, tempday, tempRN = [];
        if (this.selectedMonth == 1) {
            tempmonth = '01';
        }
        else if (this.selectedMonth == 2) {
            tempmonth = '02';
        }
        else if (this.selectedMonth == 3) {
            tempmonth = '03';
        }
        else if (this.selectedMonth == 4) {
            tempmonth = '04';
        }
        else if (this.selectedMonth == 5) {
            tempmonth = '05';
        }
        else if (this.selectedMonth == 6) {
            tempmonth = '06';
        }
        else if (this.selectedMonth == 7) {
            tempmonth = '07';
        }
        else if (this.selectedMonth == 8) {
            tempmonth = '08';
        }
        else if (this.selectedMonth == 9) {
            tempmonth = '09';
        }
        else {
            tempmonth = this.selectedMonth;
        }
        if (d == 1) {
            tempday = '01';
        }
        else if (d == 2) {
            tempday = '02';
        }
        else if (d == 3) {
            tempday = '03';
        }
        else if (d == 4) {
            tempday = '04';
        }
        else if (d == 5) {
            tempday = '05';
        }
        else if (d == 6) {
            tempday = '06';
        }
        else if (d == 7) {
            tempday = '07';
        }
        else if (d == 8) {
            tempday = '08';
        }
        else if (d == 9) {
            tempday = '09';
        }
        else {
            tempday = d;
        }
        var tempData = tempday + '/' + tempmonth + '/' + this.selectedYear;
        console.log(tempData);
        if (d == all) {
            this.RNFetch();
        }
        else {
            if (this.RNData != undefined) {
                for (var i = 0; i < this.RNData.length; i++) {
                    if (this.RNData[i].RenewalDate == tempData) {
                        tempRN.push(this.RNData[i]);
                    }
                }
                if (tempRN.length > 1) {
                    this.RNData = tempRN;
                }
            }
            else {
                this.presentAlert('Please select data properly before filetring data..');
            }
        }
    };
    HealthRenewalPage.prototype.getHealthMaster = function (subType, val) {
        var _this = this;
        var req = {
            "UserID": "5924600",
            "SubType": subType
        };
        var str = JSON.stringify(req);
        this.cs.postWithParams('/api/healthmaster/HealthPlanMaster', str).then(function (res) {
            console.log("HBoosterplanMaster", res);
            if (res.StatusCode == 1) {
                var data = res;
                localStorage.setItem('healthPlanMaster', JSON.stringify(data));
                if (val == 'Renew' || val == 'Edit') {
                    //this.isKeralaCessEnabled = false; // for testing purpose only //
                    if (_this.renewPolicyData.HealthList[0].gstPartyStateName == 'KERALA' && _this.renewPolicyData.HealthList[0].StateName == 'KERALA' && _this.isKeralaCessEnabled == true) {
                        _this.router.navigateByUrl('health-renewal-proposal');
                        document.getElementById("myNav2").style.height = "0%";
                    }
                    else {
                        if (subType == 17 || subType == 14) {
                            _this.router.navigateByUrl('health-renewal-premium');
                        }
                        else {
                            _this.router.navigateByUrl('health-renewal-summary');
                        }
                        document.getElementById("myNav2").style.height = "0%";
                    }
                }
                _this.cs.showLoader = false;
            }
            else {
                _this.presentAlert('Something went wrong...');
                _this.cs.showLoader = false;
            }
        }).catch(function (err) {
            _this.presentAlert(err.error.ExceptionMessage);
            _this.cs.showLoader = false;
        });
    };
    HealthRenewalPage.prototype.getPolicyData = function (val) {
        var _this = this;
        var body = {
            "UserType": "AGENT",
            "PolicyNo": this.selectedRenewalPolicy.Policy_Number
        };
        var str = JSON.stringify(body);
        // console.log(val);
        this.cs.showLoader = true;
        this.cs.postWithParams('/api/renewal/HealthPolicyRenewalRN', str).then(function (res) {
            if (res.StatusCode == 1) {
                _this.renewPolicyData = res;
                _this.getHealthMaster(_this.renewPolicyData.HealthList[0].SubPolicyType, val);
                localStorage.setItem('renewalSubPolicy', _this.renewPolicyData.HealthList[0].SubPolicyType);
                localStorage.setItem('renewalPolicyData', JSON.stringify(_this.renewPolicyData.HealthList[0]));
            }
            else {
                _this.presentAlert(res.StatusMessage);
                _this.cs.showLoader = false;
            }
        }).catch(function (err) {
            _this.presentAlert(err.error.ExceptionMessage);
            _this.cs.showLoader = false;
        });
    };
    HealthRenewalPage.prototype.slidePrev = function () {
        this.slides.slidePrev();
    };
    HealthRenewalPage.prototype.slideNext = function () {
        this.slides.slideNext();
    };
    HealthRenewalPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    tslib_1.__decorate([
        ViewChild('slides'),
        tslib_1.__metadata("design:type", IonSlides)
    ], HealthRenewalPage.prototype, "slides", void 0);
    HealthRenewalPage = tslib_1.__decorate([
        Component({
            selector: 'app-health-renewal',
            templateUrl: './health-renewal.page.html',
            styleUrls: ['./health-renewal.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, AlertController, Router])
    ], HealthRenewalPage);
    return HealthRenewalPage;
}());
export { HealthRenewalPage };
//# sourceMappingURL=health-renewal.page.js.map