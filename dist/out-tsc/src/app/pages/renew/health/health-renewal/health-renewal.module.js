import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HealthRenewalPage } from './health-renewal.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
var routes = [
    {
        path: '',
        component: HealthRenewalPage
    }
];
var HealthRenewalPageModule = /** @class */ (function () {
    function HealthRenewalPageModule() {
    }
    HealthRenewalPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatInputModule,
                MatDatepickerModule,
                Ng2SearchPipeModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [HealthRenewalPage]
        })
    ], HealthRenewalPageModule);
    return HealthRenewalPageModule;
}());
export { HealthRenewalPageModule };
//# sourceMappingURL=health-renewal.module.js.map