import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import * as _ from 'underscore';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { config } from 'src/app/config.properties';
var HealthRenewalPremiumPage = /** @class */ (function () {
    function HealthRenewalPremiumPage(cs, router, alertCtrl) {
        this.cs = cs;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.isRenewalEdited = false;
        this.isInsuredChange = false;
        this.isAddon1 = true;
        this.isAddon6 = false;
        this.isAddon6Adult1 = true;
        this.isAddon6Adult2 = true;
        this.ischecked1 = true;
        this.ischecked2 = false;
        this.showMaxSI = true;
        this.showNextButton = false;
        this.inputLoadingPlan = false;
    }
    HealthRenewalPremiumPage.prototype.ngOnInit = function () {
        this.selectedTenure = '1';
        this.renewalRNData = JSON.parse(localStorage.getItem('renewalPolicyData'));
        this.renewalRNData.isRenewalthroughPremium = true;
        this.healthmasterData = JSON.parse(localStorage.getItem('healthPlanMaster'));
        console.log(this.healthmasterData);
        console.log(this.renewalRNData);
        this.obtainedSI = this.renewalRNData.SumInsured;
        this.obtainedConvertedSI = parseInt(this.renewalRNData.SumInsured);
        var insuredIndex = _.findIndex(this.healthmasterData.SumInsuredDetails, { SumAmount: this.obtainedConvertedSI });
        console.log(insuredIndex + 1);
        this.currentTotalPremium1Year = this.renewalRNData.TotalPremium1Year;
        this.currentTotalPremium2Year = this.renewalRNData.TotalPremium2Year;
        if (this.renewalRNData.NoOfAdults == 0 && this.renewalRNData.NoOfKids < 2) {
            this.isAdultKidCheck = false;
        }
        else if (this.renewalRNData.NoOfAdults == 1 && this.renewalRNData.NoOfKids < 1) {
            this.isAdultKidCheck = false;
        }
        else {
            this.isAdultKidCheck = true;
        }
        if ((insuredIndex + 1) == this.healthmasterData.SumInsuredDetails.length) {
            this.showMaxSI = false;
            this.ischecked2 = true;
        }
        else {
            var nextSumInsured = this.healthmasterData.SumInsuredDetails[insuredIndex + 1].SumAmount;
            console.log(nextSumInsured);
            this.nextSI = nextSumInsured;
            this.showMaxSI = true;
            this.getRelation();
            this.reCalulcate();
        }
    };
    HealthRenewalPremiumPage.prototype.getRelation = function () {
        var _this = this;
        this.cs.postWithParams('/api/healthmaster/GetHealthProposalRelationships?Product=CHI', '').then(function (res) {
            if (res.StatusCode == 1) {
                _this.nomineeApinteeData = res.NomineeAppointeeRelationship;
            }
            else {
            }
        }).catch(function (err) {
        });
    };
    HealthRenewalPremiumPage.prototype.getDeclarationApproval = function (val) {
        if (val == 'Yes') {
            this.isDeclarationAccepted = true;
        }
        else {
            this.isDeclarationAccepted = false;
        }
    };
    HealthRenewalPremiumPage.prototype.getPolicyTenure = function (val) {
        this.selectedTenure = val;
    };
    HealthRenewalPremiumPage.prototype.siSelect = function (val) {
        if (val == '1') {
            this.ischecked1 = true;
            this.ischecked2 = false;
        }
        else {
            this.ischecked1 = false;
            this.ischecked2 = true;
        }
    };
    HealthRenewalPremiumPage.prototype.toggle = function (val) {
        if (val == 'Yes') {
            this.isAddon1 = true;
            this.renewalRNData.AddOnCovers1Flag = true;
        }
        else {
            this.isAddon1 = false;
            this.renewalRNData.AddOnCovers1Flag = false;
        }
        this.reCalulcate();
    };
    HealthRenewalPremiumPage.prototype.toggle3 = function (val) {
        if (val == 'Yes') {
            this.isAddon6 = true;
            this.renewalRNData.AddOnCovers6Flag = this.isAddon6;
        }
        else {
            this.isAddon6 = false;
            this.renewalRNData.AddOnCovers6 = '';
            this.renewalRNData.AddOnCovers6Flag = this.isAddon6;
            this.reCalulcate();
        }
    };
    HealthRenewalPremiumPage.prototype.captureAddon6 = function () {
        this.isAddon6Adult1 = !this.isAddon6Adult1;
        if (this.isAddon6Adult1 == true) {
            this.renewalRNData.AddOnCovers6 = 'Adult 1';
        }
        else if (this.isAddon6Adult1 == false) {
            this.renewalRNData.AddOnCovers6 = '';
        }
    };
    HealthRenewalPremiumPage.prototype.captureAddon6Adult2 = function () {
        this.isAddon6Adult2 = !this.isAddon6Adult2;
        if (this.isAddon6Adult2 == true) {
            this.renewalRNData.AddOnCovers6 = 'Adult 2';
        }
        else if (this.isAddon6Adult2 == false) {
            this.renewalRNData.AddOnCovers6 = '';
        }
    };
    HealthRenewalPremiumPage.prototype.reCalulcate = function () {
        var _this = this;
        this.callRNCalculate(this.obtainedSI).then(function () {
            _this.callRNCalculate(_this.nextSI);
        });
    };
    HealthRenewalPremiumPage.prototype.callRNCalculate = function (si) {
        var _this = this;
        return new Promise(function (resolve) {
            var tempAppointDate, temprelName;
            // checking for add on 1 //
            _this.renewalRNData.AddOnCovers1Flag = _this.isAddon1;
            if (_this.renewalRNData.AddOnCovers1 == undefined || _this.renewalRNData.AddOnCovers1 == null) {
                _this.renewalRNData.AddOnCovers1 = "";
            }
            // checking for add on 2 and 4 //
            if (_this.renewalRNData.AddOnCovers2And4 == undefined || _this.renewalRNData.AddOnCovers2And4 == null) {
                _this.renewalRNData.AddOnCovers2And4 = "";
            }
            if (_this.renewalRNData.AddOnCovers2And4Flag == undefined || _this.renewalRNData.AddOnCovers2And4Flag == null) {
                _this.renewalRNData.AddOnCovers2And4Flag = "";
            }
            if (_this.renewalRNData.AddOnCovers2And4Type == undefined || _this.renewalRNData.AddOnCovers2And4Type == null) {
                _this.renewalRNData.AddOnCovers2And4Type = "";
            }
            // checking for add on 3 //
            if (_this.renewalRNData.AddOnCovers3 == undefined || _this.renewalRNData.AddOnCovers3 == null) {
                _this.renewalRNData.AddOnCovers3 = "";
            }
            if (_this.renewalRNData.AddOnCovers3Flag == undefined || _this.renewalRNData.AddOnCovers3Flag == null) {
                _this.renewalRNData.AddOnCovers3Flag = "";
            }
            if (_this.renewalRNData.AddOnCovers3Type == undefined || _this.renewalRNData.AddOnCovers3Type == null) {
                _this.renewalRNData.AddOnCovers3Type = "";
            }
            // checking for add on 5 //
            if (_this.renewalRNData.AddOnCovers5 == undefined || _this.renewalRNData.AddOnCovers5 == null) {
                _this.renewalRNData.AddOnCovers5 = "";
            }
            if (_this.renewalRNData.AddOnCovers5Flag == undefined || _this.renewalRNData.AddOnCovers5Flag == null) {
                _this.renewalRNData.AddOnCovers5Flag = "";
            }
            // checking for add on 6 //
            if (_this.renewalRNData.AddOnCovers6 == undefined || _this.renewalRNData.AddOnCovers6 == null) {
                _this.renewalRNData.AddOnCovers6 = "";
            }
            if (_this.renewalRNData.AddOnCovers6Flag == undefined || _this.renewalRNData.AddOnCovers6Flag == null) {
                _this.renewalRNData.AddOnCovers6Flag = "";
            }
            if (_this.renewalRNData.AddOnCovers6Flag == "True" || _this.renewalRNData.AddOnCovers6Flag == "true") {
                _this.renewalRNData.AddOnCovers6Flag = "false";
            }
            if (_this.renewalRNData.Landmark == undefined || _this.renewalRNData.Landmark == null) {
                _this.renewalRNData.Landmark = '';
            }
            if (_this.renewalRNData.AadhaarNumber == undefined || _this.renewalRNData.AadhaarNumber == null) {
                _this.renewalRNData.AadhaarNumber = '';
            }
            if (_this.renewalRNData.PANNumber == undefined || _this.renewalRNData.PANNumber == null) {
                _this.renewalRNData.PANNumber = '';
            }
            if (_this.renewalRNData.AddressLine2 == undefined || _this.renewalRNData.AddressLine2 == null) {
                _this.renewalRNData.AddressLine2 = '';
            }
            if (_this.renewalRNData.NomineeDOB == undefined || _this.renewalRNData.NomineeDOB == null) {
                _this.renewalRNData.NomineeDOB = '';
            }
            if (_this.renewalRNData.NomineeName == undefined || _this.renewalRNData.NomineeName == null) {
                _this.renewalRNData.NomineeName = '';
            }
            if (_this.renewalRNData.NomineeRelationShip == undefined || _this.renewalRNData.NomineeRelationShip == null || _this.renewalRNData.NomineeRelationShip == '') {
                _this.nomineeRelationId = '';
            }
            else {
                _this.nomineeRelationId = _this.nomineeApinteeData.find(function (x) { return x.RelationshipName == _this.renewalRNData.NomineeRelationShip; }).RelationshipID;
            }
            if (_this.renewalRNData.NomineeTitle == undefined || _this.renewalRNData.NomineeTitle == null) {
                _this.renewalRNData.NomineeTitle = '';
            }
            if (_this.renewalRNData.AppointeeDOB == undefined || _this.renewalRNData.AppointeeDOB == null) {
                _this.renewalRNData.AppointeeDOB = '';
            }
            if (_this.renewalRNData.AppointeeName == undefined || _this.renewalRNData.AppointeeName == null) {
                _this.renewalRNData.AppointeeName = '';
            }
            if (_this.renewalRNData.AppointeeRelationship == undefined || _this.renewalRNData.AppointeeRelationship == null || _this.renewalRNData.AppointeeRelationship == '') {
                _this.appointeeRelationId = '';
            }
            else {
                _this.appointeeRelationId = _this.nomineeApinteeData.find(function (x) { return x.RelationshipName == _this.renewalRNData.AppointeeRelationship; }).RelationshipID;
            }
            if (_this.renewalRNData.AddressLine2 == undefined || _this.renewalRNData.AddressLine2 == null) {
                _this.renewalRNData.AddressLine2 = '';
            }
            if (_this.renewalRNData.InsuredDetails[0].RelationwithApplicant == null || _this.renewalRNData.InsuredDetails[0].RelationwithApplicant == undefined) {
                temprelName = '';
            }
            else {
                temprelName = _this.renewalRNData.InsuredDetails[0].RelationwithApplicant;
            }
            var tempHeight = _this.renewalRNData.InsuredDetails[0].Height + '.' + _this.renewalRNData.InsuredDetails[0].HeightInInches;
            _this.isRenewalEdited = true;
            _this.isInsuredChange = false;
            _this.inputLoadingPlan = true;
            var body = {
                "UserType": "AGENT",
                "PolicyNo": _this.renewalRNData.PolicyNo,
                "ipaddress": config.ipAddress,
                "AddOnCovers1": _this.renewalRNData.AddOnCovers1,
                "AddOnCovers1Flag": _this.renewalRNData.AddOnCovers1Flag.toString(),
                "AddOnCovers2And4": '',
                "AddOnCovers2And4Flag": '',
                "AddOnCovers2And4Type": '',
                "AddOnCovers3": _this.renewalRNData.AddOnCovers3,
                "AddOnCovers3Flag": _this.renewalRNData.AddOnCovers3Flag,
                "AddOnCovers3Type": _this.renewalRNData.AddOnCovers3Type,
                "AddOnCovers5": _this.renewalRNData.AddOnCovers5,
                "AddOnCovers5Flag": _this.renewalRNData.AddOnCovers5Flag,
                "AddOnCovers6": _this.renewalRNData.AddOnCovers6,
                "AddOnCovers6Flag": _this.renewalRNData.AddOnCovers6Flag.toString(),
                "VoluntaryDeductible": _this.renewalRNData.VoluntaryDeductible,
                "AgeOfEldest": _this.renewalRNData.AgeOfEldest,
                "NoOfAdults": _this.renewalRNData.NoOfAdults,
                "NoOfKids": _this.renewalRNData.NoOfKids,
                "NomineeDOB": _this.renewalRNData.NomineeDOB,
                "NomineeName": _this.renewalRNData.NomineeName,
                "NomineeRelationShip": _this.appointeeRelationId,
                "NomineeTitle": _this.renewalRNData.NomineeTitle,
                "SubLimitApplicable": _this.renewalRNData.SubLimitApplicable,
                "SumInsured": si.toString(),
                "Members": {
                    "Member": [{
                            "MemberType": _this.renewalRNData.InsuredDetails[0].KidAdultType,
                            "TitleID": _this.renewalRNData.InsuredDetails[0].Title,
                            "Name": _this.renewalRNData.InsuredDetails[0].FullName,
                            "RelationshipID": _this.renewalRNData.InsuredDetails[0].RelationShipID,
                            "RelationshipName": temprelName,
                            "DOB": _this.renewalRNData.InsuredDetails[0].DateofBirth,
                            "Height": tempHeight,
                            "Weight": _this.renewalRNData.InsuredDetails[0].Weight,
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": ""
                        }]
                },
                "isRenewalEdited": _this.isRenewalEdited,
                "isInsuredChange": _this.isInsuredChange,
                "CustStateName": _this.renewalRNData.StateName,
                "CustStateId": _this.renewalRNData.StateID,
                "isGSTRegistered": _this.renewalRNData.GSTApplicable,
                "SoftCopyDiscount": ""
            };
            console.log(body);
            _this.cs.showLoader = true;
            var str = JSON.stringify(body);
            var addons = { AddOnCovers1: '', AddOnCovers1Flag: '', AddOnCovers6: '', AddOnCovers6Flag: '' };
            addons.AddOnCovers1 = _this.renewalRNData.AddOnCovers1;
            addons.AddOnCovers1Flag = _this.renewalRNData.AddOnCovers1Flag.toString();
            ;
            addons.AddOnCovers6 = _this.renewalRNData.AddOnCovers6;
            addons.AddOnCovers6Flag = _this.renewalRNData.AddOnCovers6Flag.toString();
            localStorage.setItem('RenewalPremiumAddons', JSON.stringify(addons));
            // localStorage.setItem('renewalBody', str);
            _this.cs.postWithParams('/api/health/HealthPolicyRenewalCalculate', str).then(function (res) {
                if (res.StatusCode == 1) {
                    _this.showNextButton = true;
                    _this.inputLoadingPlan = false;
                    if (_this.obtainedSI == si) {
                        _this.currentTotalPremium1Year = res.HealthList[0].TotalPremium1Year;
                        _this.currentTotalPremium2Year = res.HealthList[0].TotalPremium2Year;
                        _this.reCalculatedData = res.HealthList[0];
                    }
                    if (_this.nextSI == si) {
                        _this.nextTotalPremium1Year = res.HealthList[0].TotalPremium1Year;
                        _this.nextTotalPremium2Year = res.HealthList[0].TotalPremium2Year;
                        _this.reCalculatedData2 = res.HealthList[0];
                    }
                    _this.cs.showLoader = false;
                    resolve();
                }
                else {
                    _this.presentAlert(res.StatusMessage);
                    _this.cs.showLoader = false;
                    _this.inputLoadingPlan = false;
                    _this.showNextButton = false;
                    resolve();
                }
            }).catch(function (err) {
                _this.cs.showLoader = false;
                _this.showNextButton = false;
                _this.inputLoadingPlan = false;
                resolve();
            });
        });
    };
    HealthRenewalPremiumPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    HealthRenewalPremiumPage.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HealthRenewalPremiumPage.prototype.goToSummary = function () {
        if (this.ischecked2 == true) {
            if (this.selectedTenure == '1') {
                this.reCalculatedData.Premium1YearFlag = 'true';
                this.reCalculatedData.Premium2YearFlag = 'false';
            }
            else {
                this.reCalculatedData.Premium1YearFlag = 'false';
                this.reCalculatedData.Premium2YearFlag = 'true';
            }
            this.reCalculatedData.isInsuredChange = false;
            this.reCalculatedData.isRenewalEdited = true;
            this.reCalculatedData.isRenewalthroughPremium = true;
            localStorage.removeItem('renewalBody');
            if (this.showMaxSI == false) {
                localStorage.setItem('renewalBody', JSON.stringify(this.reCalculatedData));
            }
            else {
                if (this.reCalculatedData == undefined) {
                    localStorage.setItem('renewalBody', JSON.stringify(this.reCalculatedData));
                }
                else {
                    localStorage.setItem('renewalBody', JSON.stringify(this.reCalculatedData));
                }
            }
            if (this.selectedTenure == '1' && (this.currentTotalPremium1Year == undefined || this.currentTotalPremium1Year == null)) {
                this.presentAlert('Cannot proceed ahead as premium details is not available...');
            }
            else if (this.selectedTenure == '2' && (this.currentTotalPremium2Year == undefined || this.currentTotalPremium2Year == null)) {
                this.presentAlert('Cannot proceed ahead as premium details is not available...');
            }
            else {
                this.router.navigateByUrl('health-renewal-summary');
            }
        }
        if (this.ischecked1 == true) {
            if (this.selectedTenure == '1') {
                this.reCalculatedData2.Premium1YearFlag = 'true';
                this.reCalculatedData2.Premium2YearFlag = 'false';
            }
            else {
                this.reCalculatedData2.Premium1YearFlag = 'false';
                this.reCalculatedData2.Premium2YearFlag = 'true';
            }
            this.reCalculatedData2.isInsuredChange = false;
            this.reCalculatedData2.isRenewalEdited = true;
            localStorage.removeItem('renewalBody');
            this.reCalculatedData2.isRenewalthroughPremium = true;
            localStorage.setItem('renewalBody', JSON.stringify(this.reCalculatedData2));
            if (this.selectedTenure == '1' && (this.nextTotalPremium1Year == undefined || this.nextTotalPremium1Year == null)) {
                this.presentAlert('Cannot proceed ahead as premium details is not available...');
            }
            else if (this.selectedTenure == '2' && (this.nextTotalPremium2Year == undefined || this.nextTotalPremium2Year == null)) {
                this.presentAlert('Cannot proceed ahead as premium details is not available...');
            }
            else {
                if (this.isDeclarationAccepted != true && this.ischecked2 == false) {
                    this.presentAlert('Please provide health declaration.');
                }
                else {
                    this.router.navigateByUrl('health-renewal-summary');
                }
            }
        }
    };
    HealthRenewalPremiumPage = tslib_1.__decorate([
        Component({
            selector: 'app-health-renewal-premium',
            templateUrl: './health-renewal-premium.page.html',
            styleUrls: ['./health-renewal-premium.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService,
            Router,
            AlertController])
    ], HealthRenewalPremiumPage);
    return HealthRenewalPremiumPage;
}());
export { HealthRenewalPremiumPage };
//# sourceMappingURL=health-renewal-premium.page.js.map