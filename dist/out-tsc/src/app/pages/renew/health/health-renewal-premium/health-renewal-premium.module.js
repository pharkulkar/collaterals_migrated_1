import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HealthRenewalPremiumPage } from './health-renewal-premium.page';
var routes = [
    {
        path: '',
        component: HealthRenewalPremiumPage
    }
];
var HealthRenewalPremiumPageModule = /** @class */ (function () {
    function HealthRenewalPremiumPageModule() {
    }
    HealthRenewalPremiumPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [HealthRenewalPremiumPage]
        })
    ], HealthRenewalPremiumPageModule);
    return HealthRenewalPremiumPageModule;
}());
export { HealthRenewalPremiumPageModule };
//# sourceMappingURL=health-renewal-premium.module.js.map