import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import { IonicModule } from '@ionic/angular';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { HealthRenewalSummaryPage } from './health-renewal-summary.page';
var routes = [
    {
        path: '',
        component: HealthRenewalSummaryPage
    }
];
var HealthRenewalSummaryPageModule = /** @class */ (function () {
    function HealthRenewalSummaryPageModule() {
    }
    HealthRenewalSummaryPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatInputModule,
                MatDatepickerModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [HealthRenewalSummaryPage]
        })
    ], HealthRenewalSummaryPageModule);
    return HealthRenewalSummaryPageModule;
}());
export { HealthRenewalSummaryPageModule };
//# sourceMappingURL=health-renewal-summary.module.js.map