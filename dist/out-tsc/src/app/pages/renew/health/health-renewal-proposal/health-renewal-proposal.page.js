import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CommonService } from 'src/app/services/common.service';
import * as moment from 'moment';
import { FormGroup, FormControl } from '@angular/forms';
import * as _ from 'underscore';
import { Router } from '@angular/router';
import { config } from 'src/app/config.properties';
var HealthRenewalProposalPage = /** @class */ (function () {
    function HealthRenewalProposalPage(cs, alertController, router) {
        this.cs = cs;
        this.alertController = alertController;
        this.router = router;
        this.dataValuesHB = [];
        this.show = false;
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.toggleButton = false;
        this.isGSTN = true;
        this.isUIN = true;
        this.isRegWithGST = false;
        this.showApointee = false;
        this.showAddOnCover6 = false;
        this.showaddon2HB = false;
        this.inputLoading1 = false;
        this.howaddon2HB = false;
        this.showaddon3HB = false;
        this.isAdult1 = false;
        this.isAdult2 = false;
        this.isChild1 = false;
        this.isChild2 = false;
        this.isDisease3 = false;
        this.isDisease5 = false;
        this.showAddOnCover1 = false;
        this.isChild3 = false;
        this.isBasicDetails = false;
        this.showAddOnCover3 = false;
        this.showAddOnCover5 = false;
        this.isInsuredDetails = false;
        this.isApplicantDetails = false;
        this.questionList = [];
        this.adultRelationArray = [];
        this.childRelationArray = [];
        this.ailmentArray1 = [];
        this.ailmentArray2 = [];
        this.ailmentArray4 = [];
        this.ailmentArray3 = [];
        this.ailmentArray5 = [];
        this.height_feet = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        this.height_inch = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        this.showSliderHB = false;
        this.addOnCoverBasedOnAge = false;
        this.showHB = false;
        this.showAdultTable = false;
        this.isInsuredChange = false;
        this.isRenewalEdited = false;
        // dummyObj1 = {"id": "0", "val": "Mrs."};
        // dummyObj2 = {"id": "0", "val": "Mrs.", "name":"Preetam"};
        this.titles = [{ "id": "0", "val": "Mrs." }, { "id": "1", "val": "Mr." }, { "id": "2", "val": "Ms." }];
        this.subilimitA = false;
        this.subilimitB = false;
        this.subilimitC = false;
        this.subilimitN = false;
        this.isDeclarationAccepted = true;
        this.constituentArray = [{ "id": "0", "val": "Select Constitution of Business" }, { "id": "1", "val": "Non Resident Entity" }, { "id": "2", "val": "Foreign company registered in India" }, { "id": "3", "val": "Foreign LLP" },
            { "id": "4", "val": "Government Department" }, { "id": "5", "val": "Hindu Undivided Family" }, { "id": "6", "val": "LLP Partnership" }, { "id": "7", "val": "Local Authorities" }, { "id": "8", "val": "Partnership" },
            { "id": "9", "val": "Private Limited Company" }, { "id": "10", "val": "Proprietorship" }, { "id": "11", "val": "Others" }];
        this.customerTypeArray = [{ "id": "0", "val": "Select Customer Type" }, { "id": "21", "val": "General" }, { "id": "22", "val": "EOU/STP/EHTP" }, { "id": "23", "val": "Government" }, { "id": "24", "val": "Overseas" },
            { "id": "25", "val": "Related parties" }, { "id": "26", "val": "SEZ" }, { "id": "27", "val": "Others" }];
        this.gstStatusArray = [{ "id": "0", "val": "Select GST Status" }, { "id": "41", "val": "ARN Generated" }, { "id": "42", "val": "Provision ID Obtained" }, { "id": "43", "val": "To be commenced" },
            { "id": "44", "val": "Enrolled" }, { "id": "45", "val": "Not applicable" }];
    }
    HealthRenewalProposalPage.prototype.ngOnInit = function () {
        // this.getNomineeRelation();
        // this.getRenewProposal();
        var _this = this;
        var date = new Date();
        this.minDate = moment(date).format('YYYY-MM-DD');
        var today = new Date();
        this.childmaxDOB = new Date(today.setFullYear(new Date().getFullYear() - 6));
        this.childminDOB = new Date(today.setFullYear(new Date().getFullYear() - 20));
        this.childmaxDOB = moment(this.childmaxDOB).format('YYYY-MM-DD');
        this.childminDOB = moment(this.childminDOB).format('YYYY-MM-DD');
        this.createApplForm();
        this.createInsuredForm();
        this.createBasicForm();
        this.getHealthMaster();
        this.getQuestionList();
        this.localData = JSON.parse(localStorage.getItem('renewalPolicyData'));
        this.isDataEdit = JSON.parse(localStorage.getItem('isDataEdit'));
        this.editedData = JSON.parse(localStorage.getItem('editedData'));
        var productData = JSON.parse(localStorage.getItem('product'));
        console.log('productdata', productData);
        this.productType = productData.productId;
        if (this.isDataEdit == 'Yes') {
            this.renewalFetchResponse = this.editedData;
            this.dummyObject = this.editedData;
            this.getNomineeRelation().then(function () {
                _this.mapData();
                _this.mapNomData(_this.editedData.NomineeRelationShip, _this.editedData.NomineeTitle);
            });
        }
        else {
            this.renewalFetchResponse = this.localData;
            this.dummyObject = this.localData;
            this.getNomineeRelation().then(function () {
                _this.mapData();
            });
            this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(function (res) {
                _this.states = res;
            }, function (err) {
                _this.presentAlert(err.error.ExceptionMessage);
            });
        }
        this.noOfAudult = this.renewalFetchResponse.NoOfAdults;
        if (this.noOfAudult == 2) {
            this.childArray = this.adultChildMap.get("2");
        }
        if (this.noOfAudult == 1) {
            this.childArray = this.adultChildMap.get("1");
        }
        if (this.noOfAudult > 0) {
            this.showAdultTable = true;
            this.addOnCoverBasedOnAge = false;
        }
        else {
            this.showAdultTable = false;
        }
    };
    // compareObject(obj1:any, obj2:any):Promise<any>{
    //   return new Promise((resolve) => {
    //     let equal = JSON.stringify(obj1) === JSON.stringify(obj2);
    //     console.log("Is object equal", equal);
    //     resolve();
    //   });
    // }
    HealthRenewalProposalPage.prototype.mapNomData = function (data, titleId) {
        if (data && titleId) {
            this.nomRela = this.nomineeData.NomineeAppointeeRelationship.find(function (x) { return x.RelationshipID == data; });
            this.applicantForm.patchValue({ 'nomRelation': this.nomRela.RelationshipName });
            var titleName = this.titles.find(function (x) { return x.id == titleId; });
            this.applicantForm.patchValue({ 'nomTitle': titleName.val });
        }
    };
    HealthRenewalProposalPage.prototype.selectSublimit = function (ev) {
        var subLimit = [{ name: 'A' }, { name: 'B' }, { name: 'C' }];
        if (ev.target.innerText == 'A') {
            this.subilimitA = true;
            this.subilimitB = false;
            this.subilimitC = false;
            this.subilimitN = false;
            this.insuredForm.patchValue({ "subLimit": "A" });
        }
        else if (ev.target.innerText == 'B') {
            this.subilimitA = false;
            this.subilimitB = true;
            this.subilimitC = false;
            this.subilimitN = false;
            this.insuredForm.patchValue({ "subLimit": "B" });
        }
        else if (ev.target.innerText == 'C') {
            this.subilimitA = false;
            this.subilimitB = false;
            this.subilimitC = true;
            this.subilimitN = false;
            this.insuredForm.patchValue({ "subLimit": "C" });
        }
        else {
            this.subilimitA = false;
            this.subilimitB = false;
            this.subilimitC = false;
            this.subilimitN = true;
            this.insuredForm.patchValue({ "subLimit": "NO SUBLIMIT" });
        }
    };
    HealthRenewalProposalPage.prototype.isAdult = function (ev) {
        console.log(ev.detail.checked, ev.detail.value);
        if (ev.detail.checked = true && ev.detail.value == "isAdult1") {
            this.renewalFetchResponse.AddOnCovers6 = "Adult 1";
        }
        if (ev.detail.checked = false && ev.detail.value == "isAdult1") {
            this.renewalFetchResponse.AddOnCovers6 = "";
        }
        if (ev.detail.checked = true && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers6 = "Adult 2";
        }
        if (ev.detail.checked = false && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers6 = "";
        }
        if (ev.detail.checked = true && ev.detail.value == "isAdult1" && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers6 = "Both Adult";
        }
        if (ev.detail.checked = false && ev.detail.value == "isAdult1" && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers6 = "";
        }
        if (ev.detail.checked = true && ev.detail.value == "isAdult1") {
            this.renewalFetchResponse.AddOnCovers5 = "Adult 1";
        }
        if (ev.detail.checked = false && ev.detail.value == "isAdult1") {
            this.renewalFetchResponse.AddOnCovers5 = "";
        }
        if (ev.detail.checked = true && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers5 = "Adult 2";
        }
        if (ev.detail.checked = false && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers5 = "";
        }
        if (ev.detail.checked = true && ev.detail.value == "isAdult1" && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers5 = "Both Adult";
        }
        if (ev.detail.checked = false && ev.detail.value == "isAdult1" && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers5 = "";
        }
        if (ev.detail.checked = true && ev.detail.value == "isAdult1") {
            this.renewalFetchResponse.AddOnCovers3 = "Adult 1";
        }
        if (ev.detail.checked = false && ev.detail.value == "isAdult1") {
            this.renewalFetchResponse.AddOnCovers3 = "";
        }
        if (ev.detail.checked = true && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers3 = "Adult 2";
        }
        if (ev.detail.checked = false && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers3 = "";
        }
        if (ev.detail.checked = true && ev.detail.value == "isAdult1" && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers3 = "Both Adult";
        }
        if (ev.detail.checked = false && ev.detail.value == "isAdult1" && ev.detail.value == "isAdult2") {
            this.renewalFetchResponse.AddOnCovers3 = "";
        }
    };
    HealthRenewalProposalPage.prototype.getHealthMaster = function () {
        var _this = this;
        this.healthMasterResponse = JSON.parse(localStorage.getItem('healthPlanMaster'));
        console.log("HealthMaster", this.healthMasterResponse);
        this.adultChildMap = new Map();
        this.adultAgeMap = new Map();
        this.healthMasterResponse.AdultResponse.forEach(function (adult) {
            _this.adultChildMap.set(adult.AdultValue, adult.ChildDetails);
            _this.adultAgeMap.set(adult.AdultValue, adult.AgeEldestDetails);
            adult.checked = false;
        });
        this.childArray = [{
                NoofKids: "1"
            }];
        this.noOfChild = "0";
        this.dataValuesHB = [];
        this.healthMasterResponse.SumInsuredDetails.forEach(function (sum) {
            _this.dataValuesHB.push(sum.SumAmount.toString());
        });
        this.showSliderHB = true;
        // for VD details
        this.vdMap = new Map();
        this.healthMasterResponse.SumInsuredDetails.forEach(function (amount) {
            console.log("VD Amount", amount);
            console.log("VD KEY", amount.SumAmount);
            console.log("VD VALUE", amount.VDValues);
            _this.vdMap.set(amount.SumAmount, amount.VDValues);
            console.log("VD MAp", _this.vdMap);
        });
    };
    HealthRenewalProposalPage.prototype.segmentChanged = function (ev) {
        console.log(ev.detail.value);
        this.showSegment = ev.detail.value;
    };
    HealthRenewalProposalPage.prototype.getAgeStatus = function (event) {
        if (event.from_value) {
            this.addOnCoverBasedOnAge = true;
        }
        else {
            this.addOnCoverBasedOnAge = false;
        }
    };
    HealthRenewalProposalPage.prototype.toggle = function () {
        this.show = !this.show;
        console.log(this.show);
        this.insuredForm.patchValue({ addOnCover1: this.show });
    };
    HealthRenewalProposalPage.prototype.toggle1 = function () {
        this.show1 = !this.show1;
        console.log(this.show1);
        this.renewalFetchResponse.AddOnCovers3Flag = this.show1;
        // this.insuredForm.patchValue({ addOnCover3: this.show1 });
    };
    HealthRenewalProposalPage.prototype.toggle2 = function () {
        this.show2 = !this.show2;
        console.log(this.show2);
        this.renewalFetchResponse.AddOnCovers5Flag = this.show2;
        // this.insuredForm.patchValue({ addOnCover5: this.show2 });
    };
    HealthRenewalProposalPage.prototype.toggle3 = function (val) {
        this.show3 = val;
        console.log(this.show3);
        this.renewalFetchResponse.AddOnCovers6Flag = this.show3;
        // this.insuredForm.patchValue({ addOnCover6: this.show3 });
    };
    HealthRenewalProposalPage.prototype.addOnYesNo = function (event) {
        this.toggleButton = !this.toggleButton;
        console.log(event);
    };
    HealthRenewalProposalPage.prototype.getSumInsured = function (event) {
        if (this.renewalFetchResponse.SubPolicyType == '14') {
            console.log(event.target.value);
            if (event.target.value < '500000') {
                this.showAddOnCover1 = false;
                this.showAddOnCover3 = false;
                this.showAddOnCover5 = false;
                this.showAddOnCover6 = false;
                // this.insuredForm.patchValue({ 'addOnCover1': "true" });
            }
            else {
                this.showAddOnCover1 = true;
                this.showAddOnCover3 = false;
                this.showAddOnCover5 = false;
                this.showAddOnCover6 = false;
            }
        }
        else if (this.renewalFetchResponse.SubPolicyType == '12') {
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover3 = false;
            this.showAddOnCover6 = false;
        }
        else if (this.renewalFetchResponse.SubPolicyType == '16') {
            this.showAddOnCover3 = true;
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover6 = false;
        }
        else if (this.renewalFetchResponse.SubPolicyType == '15') {
            this.showAddOnCover5 = true;
            this.showAddOnCover1 = false;
            this.showAddOnCover3 = false;
            this.showAddOnCover6 = false;
        }
        else {
            this.showAddOnCover1 = true;
            this.showAddOnCover6 = true;
            this.showAddOnCover3 = false;
            this.showAddOnCover5 = false;
        }
    };
    HealthRenewalProposalPage.prototype.addOnYesNoHB = function (event, type) {
        if (!event.target.checked) {
            if (type == 'AddOn2') {
                this.insuredForm.controls.Addon2Members['controls'].Member['controls'].AdultType.setValue(false);
                this.insuredForm.controls.Addon2Members['controls'].Member['controls'].AdultType2.setValue(false);
                this.insuredForm.controls.AddOn2Varient.setValue(null);
                // this.quoteForm.controls.AddOn1Varient.setValue(false);
            }
            if (type == 'AddOn3') {
                this.insuredForm.controls.Addon3Members['controls'].Member['controls'].AdultType.setValue(false);
                this.insuredForm.controls.Addon3Members['controls'].Member['controls'].AdultType2.setValue(false);
                this.insuredForm.controls.AddOn3Varient.setValue(null);
            }
        }
    };
    HealthRenewalProposalPage.prototype.getAdult = function (ev) {
        var _this = this;
        this.noOfAudult = ev.target.value;
        console.log("Adult", this.noOfAudult, this.renewalFetchResponse);
        this.renewalFetchResponse.NoOfAdults = this.noOfAudult;
        if (this.noOfAudult > 0) {
            this.showAdultTable = true;
            this.addOnCoverBasedOnAge = false;
        }
        else {
            this.showAdultTable = false;
        }
        var child = this.adultChildMap.get(ev.target.value);
        this.childArray = child;
        var adultAge = this.adultAgeMap.get(ev.target.value);
        this.adultAgeArray = adultAge;
        if (this.noOfAudult == 2 && ev.target.checked == true) {
            this.childArray = this.adultChildMap.get(ev.target.value);
            this.healthMasterResponse.AdultResponse.forEach(function (adult) {
                adult.checked = true;
            });
            this.isAdult2 = true;
        }
        if (this.noOfAudult == 2 && ev.target.checked == false) {
            this.childArray = this.adultChildMap.get("1");
            this.isAdult2 = false;
        }
        else if (this.noOfAudult == 1 && ev.target.checked == true) {
            this.healthMasterResponse.AdultResponse.forEach(function (adult) {
                if (adult.AdultValue == 1) {
                    adult.checked = true;
                    true;
                    _this.childArray = _this.adultChildMap.get("1");
                }
            });
            this.isAdult1 = true;
        }
        else if (this.noOfAudult == 1 && ev.target.checked == false) {
            this.healthMasterResponse.AdultResponse.forEach(function (adult) {
                if (adult.AdultValue == 1) {
                    adult.checked = false;
                }
                _this.childArray = [{ NoofKids: "1", SumInsuredDetails: null }];
            });
        }
        console.log(this.adultAgeArray);
    };
    HealthRenewalProposalPage.prototype.getChild = function (ev, child) {
        this.noOfChild = child;
        console.log("Child", this.noOfChild, this.renewalFetchResponse);
        this.renewalFetchResponse.NoOfKids = this.noOfChild;
        if (this.noOfChild == 1 && ev.target.checked == false) {
            this.noOfChild = 0;
            this.isChild1 = false;
        }
        else if (this.noOfChild == 1 && ev.target.checked == true) {
            this.noOfChild = 1;
            this.isChild1 = true;
        }
        else if (this.noOfChild == 2 && ev.target.checked == false) {
            this.noOfChild = 0;
            this.isChild2 = false;
        }
        else if (this.noOfChild == 2 && ev.target.checked == true) {
            this.noOfChild = 2;
            this.isChild2 = true;
        }
        else if (this.noOfChild == 3 && ev.target.checked == false) {
            this.noOfChild = 0;
            this.isChild3 = false;
        }
        else if (this.noOfChild == 3 && ev.target.checked == true) {
            this.noOfChild = 3;
            this.isChild3 = true;
        }
    };
    HealthRenewalProposalPage.prototype.createBasicForm = function () {
        this.basicForm = new FormGroup({
            prodName: new FormControl(),
            noofAdult: new FormControl(),
            noofChild: new FormControl(),
            ageEldest: new FormControl(),
            siValue: new FormControl(''),
            additionalSum: new FormControl(),
            VDValues: new FormControl(),
        });
    };
    HealthRenewalProposalPage.prototype.mapData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("getRenewProposal", _this.renewalFetchResponse);
            _this.siList = _this.renewalFetchResponse.hospitalSIList;
            console.log(_this.siList);
            if (_this.renewalFetchResponse.NoOfAdults == 1) {
                _this.healthMasterResponse.AdultResponse.forEach(function (adult) {
                    if (adult.AdultValue == 1) {
                        adult.checked = true;
                    }
                });
            }
            if (_this.renewalFetchResponse.NoOfAdults == 2) {
                _this.healthMasterResponse.AdultResponse.forEach(function (adult) {
                    if (adult.AdultValue == 1 || adult.AdultValue == 2) {
                        adult.checked = true;
                    }
                });
            }
            // Basic Form
            if (_this.productType == 6) {
                _this.basicForm.patchValue({ 'prodName': "CHI" });
            }
            else {
                _this.basicForm.patchValue({ 'prodName': "Health Booster" });
            }
            _this.basicForm.patchValue({ 'ageEldest': _this.renewalFetchResponse.AgeOfEldest });
            _this.basicForm.patchValue({ 'siValue': _this.renewalFetchResponse.SumInsured });
            _this.basicForm.patchValue({ 'additionalSum': _this.renewalFetchResponse.AdditionalSI });
            if (_this.renewalFetchResponse.SubLimitApplicable == null || _this.renewalFetchResponse.SubLimitApplicable == undefined) {
                _this.insuredForm.patchValue({ 'subLimit': "NO SUBLIMIT" });
            }
            else {
                _this.insuredForm.patchValue({ 'subLimit': _this.renewalFetchResponse.SubLimitApplicable });
            }
            // Applicant Form
            _this.applicantForm.patchValue({ 'applName': _this.renewalFetchResponse.NameOfApplicant });
            _this.applicantForm.patchValue({ 'aadhaarNo': _this.renewalFetchResponse.AadhaarNumber });
            _this.applicantForm.patchValue({ 'panNumber': _this.renewalFetchResponse.PANNumber });
            _this.applicantForm.patchValue({ 'applDOB': _this.renewalFetchResponse.CustomerDOB });
            _this.applicantForm.patchValue({ 'customerType': _this.renewalFetchResponse.gstcustomertype });
            _this.applicantForm.patchValue({ 'panNo': _this.renewalFetchResponse.gstpan });
            _this.applicantForm.patchValue({ 'constitution': _this.renewalFetchResponse.gstconstitutionofbusiness });
            _this.applicantForm.patchValue({ 'gstRegStatus': _this.renewalFetchResponse.gstregistrationstatus });
            _this.applicantForm.patchValue({ 'GSTIN': _this.renewalFetchResponse.gstGSTIN });
            _this.applicantForm.patchValue({ 'UIN': _this.renewalFetchResponse.gstinno });
            _this.applicantForm.patchValue({ 'applAdd1': _this.renewalFetchResponse.AddressLine1 });
            _this.applicantForm.patchValue({ 'applAdd2': _this.renewalFetchResponse.AddressLine2 });
            _this.applicantForm.patchValue({ 'applLandMark': _this.renewalFetchResponse.Landmark });
            _this.applicantForm.patchValue({ 'applPinCode': _this.renewalFetchResponse.Pincode });
            _this.applicantForm.patchValue({ 'applCity': _this.renewalFetchResponse.CityName });
            _this.applicantForm.patchValue({ 'applState': _this.renewalFetchResponse.StateName });
            _this.applicantForm.patchValue({ 'applEmail': _this.renewalFetchResponse.Email });
            _this.applicantForm.patchValue({ 'applMobNo': _this.renewalFetchResponse.Mobile });
            if (_this.renewalFetchResponse.NomineeDOB != null && _this.renewalFetchResponse.NomineeDOB != '' && _this.renewalFetchResponse.NomineeDOB != undefined) {
                _this.applicantForm.patchValue({ 'nomDOB': moment(_this.renewalFetchResponse.NomineeDOB).format('YYYY-MM-DD') });
            }
            _this.applicantForm.patchValue({ 'nomName': _this.renewalFetchResponse.NomineeName });
            if (_this.isDataEdit !== 'Yes') {
                _this.applicantForm.patchValue({ 'nomTitle': _this.renewalFetchResponse.NomineeTitle });
                _this.applicantForm.patchValue({ 'nomRelation': _this.renewalFetchResponse.NomineeRelationShip });
            }
            if (_this.renewalFetchResponse.gstGSTIN != null && _this.renewalFetchResponse.gstGSTIN != undefined) {
                _this.isUIN = false;
                _this.isGSTN = true;
            }
            else {
                _this.isUIN = true;
                _this.isGSTN = false;
            }
            // this.applicantForm.patchValue({ 'apsId' :  this.renewalFetchResponse.NomineeName});
            // this.applicantForm.patchValue({ 'custRefNo' :  this.renewalFetchResponse.NomineeName});
            if (_this.renewalFetchResponse.NoOfAdults == "1") {
                _this.showSegment = 'adult1';
                _this.isAdult1 = true;
            }
            if (_this.renewalFetchResponse.NoOfAdults == "2") {
                _this.showSegment = 'adult1';
                _this.showSegment = 'adult2';
                _this.isAdult1 = true;
                _this.isAdult2 = true;
            }
            if (_this.renewalFetchResponse.noOfChild == "1") {
                _this.showSegment = 'child1';
                _this.isChild1 = true;
            }
            if (_this.renewalFetchResponse.noOfChild == "2") {
                _this.showSegment = 'child1';
                _this.showSegment = 'child2';
                _this.isChild1 = true;
                _this.isChild2 = true;
            }
            if (_this.renewalFetchResponse.noOfChild == "3") {
                _this.showSegment = 'child1';
                _this.showSegment = 'child2';
                _this.showSegment = 'child3';
                _this.isChild1 = true;
                _this.isChild2 = true;
                _this.isChild3 = true;
            }
            _this.getRelationTitle().then(function () {
                _this.mapInsuredData();
            });
            resolve();
        });
    };
    HealthRenewalProposalPage.prototype.getRelationTitle = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (_this.renewalFetchResponse.NoOfAdults == '0') {
                if (_this.renewalFetchResponse.NoOfKids == '1') {
                    _this.getRelation13(_this.renewalFetchResponse.InsuredDetails[0].RelationShipID);
                    _this.getTitle13(_this.renewalFetchResponse.InsuredDetails[0].Title);
                }
                resolve();
            }
            else if (_this.renewalFetchResponse.NoOfAdults == '1') {
                if (_this.renewalFetchResponse.NoOfKids == '0') {
                    _this.getRelation11(_this.renewalFetchResponse.InsuredDetails[0].RelationShipID);
                    _this.getTitle11(_this.renewalFetchResponse.InsuredDetails[0].Title);
                }
                else if (_this.renewalFetchResponse.NoOfKids == '1') {
                    _this.getRelation11(_this.renewalFetchResponse.InsuredDetails[0].RelationShipID);
                    _this.getRelation13(_this.renewalFetchResponse.InsuredDetails[1].RelationShipID);
                    _this.getTitle11(_this.renewalFetchResponse.InsuredDetails[0].Title);
                    _this.getTitle13(_this.renewalFetchResponse.InsuredDetails[1].Title);
                }
                else {
                    _this.getRelation11(_this.renewalFetchResponse.InsuredDetails[0].RelationShipID);
                    _this.getRelation13(_this.renewalFetchResponse.InsuredDetails[1].RelationShipID);
                    _this.getRelation14(_this.renewalFetchResponse.InsuredDetails[2].RelationShipID);
                    _this.getTitle11(_this.renewalFetchResponse.InsuredDetails[0].Title);
                    _this.getTitle13(_this.renewalFetchResponse.InsuredDetails[1].Title);
                    _this.getTitle14(_this.renewalFetchResponse.InsuredDetails[2].Title);
                }
                resolve();
            }
            else {
                if (_this.renewalFetchResponse.NoOfKids == '0') {
                    _this.getRelation11(_this.renewalFetchResponse.InsuredDetails[0].RelationShipID);
                    _this.getRelation12(_this.renewalFetchResponse.InsuredDetails[1].RelationShipID);
                    _this.getTitle11(_this.renewalFetchResponse.InsuredDetails[0].Title);
                    _this.getTitle12(_this.renewalFetchResponse.InsuredDetails[1].Title);
                }
                else if (_this.renewalFetchResponse.NoOfKids == '1') {
                    _this.getRelation11(_this.renewalFetchResponse.InsuredDetails[0].RelationShipID);
                    _this.getRelation12(_this.renewalFetchResponse.InsuredDetails[1].RelationShipID);
                    _this.getRelation13(_this.renewalFetchResponse.InsuredDetails[2].RelationShipID);
                    _this.getTitle11(_this.renewalFetchResponse.InsuredDetails[0].Title);
                    _this.getTitle12(_this.renewalFetchResponse.InsuredDetails[1].Title);
                    _this.getTitle13(_this.renewalFetchResponse.InsuredDetails[2].Title);
                }
                else if (_this.renewalFetchResponse.NoOfKids == '2') {
                    _this.getRelation11(_this.renewalFetchResponse.InsuredDetails[0].RelationShipID);
                    _this.getRelation12(_this.renewalFetchResponse.InsuredDetails[1].RelationShipID);
                    _this.getRelation13(_this.renewalFetchResponse.InsuredDetails[2].RelationShipID);
                    _this.getRelation14(_this.renewalFetchResponse.InsuredDetails[3].RelationShipID);
                    _this.getTitle11(_this.renewalFetchResponse.InsuredDetails[0].Title);
                    _this.getTitle12(_this.renewalFetchResponse.InsuredDetails[1].Title);
                    _this.getTitle13(_this.renewalFetchResponse.InsuredDetails[2].Title);
                    _this.getTitle14(_this.renewalFetchResponse.InsuredDetails[3].Title);
                }
                else {
                    _this.getRelation11(_this.renewalFetchResponse.InsuredDetails[0].RelationShipID);
                    _this.getRelation12(_this.renewalFetchResponse.InsuredDetails[1].RelationShipID);
                    _this.getRelation13(_this.renewalFetchResponse.InsuredDetails[2].RelationShipID);
                    _this.getRelation14(_this.renewalFetchResponse.InsuredDetails[3].RelationShipID);
                    _this.getRelation15(_this.renewalFetchResponse.InsuredDetails[4].RelationShipID);
                    _this.getTitle11(_this.renewalFetchResponse.InsuredDetails[0].Title);
                    _this.getTitle12(_this.renewalFetchResponse.InsuredDetails[1].Title);
                    _this.getTitle13(_this.renewalFetchResponse.InsuredDetails[2].Title);
                    _this.getTitle14(_this.renewalFetchResponse.InsuredDetails[3].Title);
                    _this.getTitle15(_this.renewalFetchResponse.InsuredDetails[4].Title);
                }
                resolve();
            }
        });
    };
    HealthRenewalProposalPage.prototype.toggleHB = function () {
        this.showHB = !this.showHB;
        console.log(this.showHB);
        this.insuredForm.patchValue({ AddOn1: this.showHB });
    };
    HealthRenewalProposalPage.prototype.toggle1HB = function () {
        this.showaddon2HB = !this.showaddon2HB;
        console.log(this.showaddon2HB);
        this.insuredForm.patchValue({ AddOn2: this.showaddon2HB });
    };
    HealthRenewalProposalPage.prototype.toggle2HB = function () {
        this.showaddon3HB = !this.showaddon3HB;
        console.log(this.showaddon3HB);
        this.insuredForm.patchValue({ AddOn3: this.showaddon3HB });
    };
    HealthRenewalProposalPage.prototype.getRelation11 = function (ev) {
        console.log(ev, this.adultRelationArray);
        this.relationId11 = this.adultRelationArray.find(function (x) { return x.RelationshipID == ev; });
        this.relationId1 = this.relationId11;
        console.log("rel11", this.relationId11, this.relationId1);
    };
    HealthRenewalProposalPage.prototype.getRelation12 = function (ev) {
        this.relationId12 = this.adultRelationArray.find(function (x) { return x.RelationshipID == ev; });
        console.log("rel12", this.relationId12);
        this.relationId2 = this.relationId12;
    };
    HealthRenewalProposalPage.prototype.getRelation13 = function (ev) {
        this.relationId13 = this.childRelationArray.find(function (x) { return x.RelationshipID == ev; });
        console.log("rel13", this.relationId13);
        this.relationId3 = this.relationId13;
    };
    HealthRenewalProposalPage.prototype.getRelation14 = function (ev) {
        this.relationId14 = this.childRelationArray.find(function (x) { return x.RelationshipID == ev; });
        console.log("rel14", this.relationId14);
        this.relationId4 = this.relationId14;
    };
    HealthRenewalProposalPage.prototype.getRelation15 = function (ev) {
        this.relationId15 = this.childRelationArray.find(function (x) { return x.RelationshipID == ev; });
        console.log("rel15", this.relationId15);
        this.relationId5 = this.relationId15;
    };
    HealthRenewalProposalPage.prototype.getTitle11 = function (ev) {
        this.titleId11 = this.titles.find(function (x) { return x.id == ev; });
        this.titleId1 = this.titleId11;
        console.log("rel15", this.titleId11, this.titleId1);
    };
    HealthRenewalProposalPage.prototype.getTitle12 = function (ev) {
        this.titleId12 = this.titles.find(function (x) { return x.id == ev; });
        console.log("rel15", this.titleId12);
        this.titleId2 = this.titleId12;
    };
    HealthRenewalProposalPage.prototype.getTitle13 = function (ev) {
        this.titleId13 = this.titles.find(function (x) { return x.id == ev; });
        console.log("rel15", this.titleId13);
        this.titleId3 = this.titleId13;
    };
    HealthRenewalProposalPage.prototype.getTitle14 = function (ev) {
        this.titleId14 = this.titles.find(function (x) { return x.id == ev; });
        console.log("rel15", this.titleId14);
        this.titleId4 = this.titleId14;
    };
    HealthRenewalProposalPage.prototype.getTitle15 = function (ev) {
        this.titleId15 = this.titles.find(function (x) { return x.id == ev; });
        console.log("rel15", this.titleId15);
        this.titleId5 = this.titleId15;
    };
    HealthRenewalProposalPage.prototype.mapInsuredData = function () {
        var _this = this;
        if (this.renewalFetchResponse.NoOfAdults == '0') {
            if (this.renewalFetchResponse.NoOfKids == '1') {
                this.insuredForm.patchValue({ 'child1Cat': this.renewalFetchResponse.InsuredDetails[1].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'child1Rela': this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant });
                var kid1Title1 = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[1].Title; }).val;
                this.insuredForm.patchValue({ 'child1Title': kid1Title1 });
                this.insuredForm.patchValue({ 'child1Name': this.renewalFetchResponse.InsuredDetails[1].FullName });
                this.insuredForm.patchValue({ 'child1DOB': this.renewalFetchResponse.InsuredDetails[1].DateofBirth });
                this.insuredForm.patchValue({ 'child1Feet': this.renewalFetchResponse.InsuredDetails[1].Height });
                this.insuredForm.patchValue({ 'child1Inches': this.renewalFetchResponse.InsuredDetails[1].HeightInInches });
                this.insuredForm.patchValue({ 'child1Weight': this.renewalFetchResponse.InsuredDetails[1].Weight });
                if (this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'child1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'child1Disease': this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease });
                }
            }
        }
        else if (this.renewalFetchResponse.NoOfAdults == '1') {
            if (this.renewalFetchResponse.NoOfKids == '0') {
                this.insuredForm.patchValue({ 'member1Cat': this.renewalFetchResponse.InsuredDetails[0].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member1Rela': this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant });
                var memb1title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[0].Title; }).val;
                this.insuredForm.patchValue({ 'member1Title': memb1title });
                this.insuredForm.patchValue({ 'member1Name': this.renewalFetchResponse.InsuredDetails[0].FullName });
                this.insuredForm.patchValue({ 'member1DOB': this.renewalFetchResponse.InsuredDetails[0].DateofBirth });
                this.insuredForm.patchValue({ 'member1Feet': this.renewalFetchResponse.InsuredDetails[0].Height });
                this.insuredForm.patchValue({ 'member1Inches': this.renewalFetchResponse.InsuredDetails[0].HeightInInches });
                this.insuredForm.patchValue({ 'member1Weight': this.renewalFetchResponse.InsuredDetails[0].Weight });
                if (this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'member1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'member1Disease': this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease });
                }
            }
            else if (this.renewalFetchResponse.NoOfKids == '1') {
                this.insuredForm.patchValue({ 'member1Cat': this.renewalFetchResponse.InsuredDetails[0].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member1Rela': this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant });
                var memb1title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[0].Title; }).val;
                this.insuredForm.patchValue({ 'member1Title': memb1title });
                this.insuredForm.patchValue({ 'member1Name': this.renewalFetchResponse.InsuredDetails[0].FullName });
                this.insuredForm.patchValue({ 'member1DOB': this.renewalFetchResponse.InsuredDetails[0].DateofBirth });
                this.insuredForm.patchValue({ 'member1Feet': this.renewalFetchResponse.InsuredDetails[0].Height });
                this.insuredForm.patchValue({ 'member1Inches': this.renewalFetchResponse.InsuredDetails[0].HeightInInches });
                this.insuredForm.patchValue({ 'member1Weight': this.renewalFetchResponse.InsuredDetails[0].Weight });
                if (this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'member1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'member1Disease': this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'child1Cat': this.renewalFetchResponse.InsuredDetails[1].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'child1Rela': this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant });
                var kid1Title1 = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[1].Title; }).val;
                this.insuredForm.patchValue({ 'child1Title': kid1Title1 });
                this.insuredForm.patchValue({ 'child1Name': this.renewalFetchResponse.InsuredDetails[1].FullName });
                this.insuredForm.patchValue({ 'child1DOB': this.renewalFetchResponse.InsuredDetails[1].DateofBirth });
                this.insuredForm.patchValue({ 'child1Feet': this.renewalFetchResponse.InsuredDetails[1].Height });
                this.insuredForm.patchValue({ 'child1Inches': this.renewalFetchResponse.InsuredDetails[1].HeightInInches });
                this.insuredForm.patchValue({ 'child1Weight': this.renewalFetchResponse.InsuredDetails[1].Weight });
                if (this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'child1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'child1Disease': this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease });
                }
            }
            else {
                this.insuredForm.patchValue({ 'member1Cat': this.renewalFetchResponse.InsuredDetails[0].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member1Rela': this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant });
                var memb1title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[0].Title; }).val;
                this.insuredForm.patchValue({ 'member1Title': memb1title });
                this.insuredForm.patchValue({ 'member1Name': this.renewalFetchResponse.InsuredDetails[0].FullName });
                this.insuredForm.patchValue({ 'member1DOB': this.renewalFetchResponse.InsuredDetails[0].DateofBirth });
                this.insuredForm.patchValue({ 'member1Feet': this.renewalFetchResponse.InsuredDetails[0].Height });
                this.insuredForm.patchValue({ 'member1Inches': this.renewalFetchResponse.InsuredDetails[0].HeightInInches });
                this.insuredForm.patchValue({ 'member1Weight': this.renewalFetchResponse.InsuredDetails[0].Weight });
                if (this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'member1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'member1Disease': this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'child1Cat': this.renewalFetchResponse.InsuredDetails[1].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'child1Rela': this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant });
                var kid1Title1 = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[1].Title; }).val;
                this.insuredForm.patchValue({ 'child1Title': kid1Title1 });
                this.insuredForm.patchValue({ 'child1Name': this.renewalFetchResponse.InsuredDetails[1].FullName });
                this.insuredForm.patchValue({ 'child1DOB': this.renewalFetchResponse.InsuredDetails[1].DateofBirth });
                this.insuredForm.patchValue({ 'child1Feet': this.renewalFetchResponse.InsuredDetails[1].Height });
                this.insuredForm.patchValue({ 'child1Inches': this.renewalFetchResponse.InsuredDetails[1].HeightInInches });
                this.insuredForm.patchValue({ 'child1Weight': this.renewalFetchResponse.InsuredDetails[1].Weight });
                if (this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'child1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'child1Disease': this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'child2Cat': this.renewalFetchResponse.InsuredDetails[2].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[2].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[2].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[2].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'child2Rela': this.renewalFetchResponse.InsuredDetails[2].RelationwithApplicant });
                var kid2Title1 = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[2].Title; }).val;
                this.insuredForm.patchValue({ 'child2Title': kid2Title1 });
                this.insuredForm.patchValue({ 'child2Name': this.renewalFetchResponse.InsuredDetails[2].FullName });
                this.insuredForm.patchValue({ 'child2DOB': this.renewalFetchResponse.InsuredDetails[2].DateofBirth });
                this.insuredForm.patchValue({ 'child2Feet': this.renewalFetchResponse.InsuredDetails[2].Height });
                this.insuredForm.patchValue({ 'child2Inches': this.renewalFetchResponse.InsuredDetails[2].HeightInInches });
                this.insuredForm.patchValue({ 'child2Weight': this.renewalFetchResponse.InsuredDetails[2].Weight });
                if (this.renewalFetchResponse.InsuredDetails[2].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[2].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'child2Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'child2Disease': this.renewalFetchResponse.InsuredDetails[2].PreExistingDisease });
                }
            }
        }
        else {
            if (this.renewalFetchResponse.NoOfKids == '0') {
                this.insuredForm.patchValue({ 'member1Cat': this.renewalFetchResponse.InsuredDetails[0].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member1Rela': this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant });
                var memb1title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[0].Title; }).val;
                this.insuredForm.patchValue({ 'member1Title': memb1title });
                this.insuredForm.patchValue({ 'member1Name': this.renewalFetchResponse.InsuredDetails[0].FullName });
                this.insuredForm.patchValue({ 'member1DOB': this.renewalFetchResponse.InsuredDetails[0].DateofBirth });
                this.insuredForm.patchValue({ 'member1Feet': this.renewalFetchResponse.InsuredDetails[0].Height });
                this.insuredForm.patchValue({ 'member1Inches': this.renewalFetchResponse.InsuredDetails[0].HeightInInches });
                this.insuredForm.patchValue({ 'member1Weight': this.renewalFetchResponse.InsuredDetails[0].Weight });
                if (this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'member1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'member1Disease': this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'member2Cat': this.renewalFetchResponse.InsuredDetails[1].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member2Rela': this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant });
                var memb2title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[1].Title; }).val;
                this.insuredForm.patchValue({ 'member2Title': memb2title });
                this.insuredForm.patchValue({ 'member2Name': this.renewalFetchResponse.InsuredDetails[1].FullName });
                this.insuredForm.patchValue({ 'member2DOB': this.renewalFetchResponse.InsuredDetails[1].DateofBirth });
                this.insuredForm.patchValue({ 'member2Feet': this.renewalFetchResponse.InsuredDetails[1].Height });
                this.insuredForm.patchValue({ 'member2Inches': this.renewalFetchResponse.InsuredDetails[1].HeightInInches });
                this.insuredForm.patchValue({ 'member2Weight': this.renewalFetchResponse.InsuredDetails[1].Weight });
                this.insuredForm.patchValue({ 'member2Disease': this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease });
            }
            else if (this.renewalFetchResponse.NoOfKids == '1') {
                this.insuredForm.patchValue({ 'member1Cat': this.renewalFetchResponse.InsuredDetails[0].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member1Rela': this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant });
                var memb1title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[0].Title; }).val;
                this.insuredForm.patchValue({ 'member1Title': memb1title });
                this.insuredForm.patchValue({ 'member1Name': this.renewalFetchResponse.InsuredDetails[0].FullName });
                this.insuredForm.patchValue({ 'member1DOB': this.renewalFetchResponse.InsuredDetails[0].DateofBirth });
                this.insuredForm.patchValue({ 'member1Feet': this.renewalFetchResponse.InsuredDetails[0].Height });
                this.insuredForm.patchValue({ 'member1Inches': this.renewalFetchResponse.InsuredDetails[0].HeightInInches });
                this.insuredForm.patchValue({ 'member1Weight': this.renewalFetchResponse.InsuredDetails[0].Weight });
                if (this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'member1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'member1Disease': this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'member2Cat': this.renewalFetchResponse.InsuredDetails[1].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member2Rela': this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant });
                var memb2title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[1].Title; }).val;
                this.insuredForm.patchValue({ 'member2Title': memb2title });
                this.insuredForm.patchValue({ 'member2Name': this.renewalFetchResponse.InsuredDetails[1].FullName });
                this.insuredForm.patchValue({ 'member2DOB': this.renewalFetchResponse.InsuredDetails[1].DateofBirth });
                this.insuredForm.patchValue({ 'member2Feet': this.renewalFetchResponse.InsuredDetails[1].Height });
                this.insuredForm.patchValue({ 'member2Inches': this.renewalFetchResponse.InsuredDetails[1].HeightInInches });
                this.insuredForm.patchValue({ 'member2Weight': this.renewalFetchResponse.InsuredDetails[1].Weight });
                if (this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'member2Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'member2Disease': this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'child1Cat': this.renewalFetchResponse.InsuredDetails[2].KidAdultType });
                this.insuredForm.patchValue({ 'child1Rela': this.renewalFetchResponse.InsuredDetails[2].RelationwithApplicant });
                var kid1Title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[2].Title; }).val;
                this.insuredForm.patchValue({ 'child1Title': kid1Title });
                this.insuredForm.patchValue({ 'child1Name': this.renewalFetchResponse.InsuredDetails[2].FullName });
                this.insuredForm.patchValue({ 'child1DOB': this.renewalFetchResponse.InsuredDetails[2].DateofBirth });
                this.insuredForm.patchValue({ 'child1Feet': this.renewalFetchResponse.InsuredDetails[2].Height });
                this.insuredForm.patchValue({ 'child1Inches': this.renewalFetchResponse.InsuredDetails[2].HeightInInches });
                this.insuredForm.patchValue({ 'child1Weight': this.renewalFetchResponse.InsuredDetails[2].Weight });
                if (this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'child1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'child1Disease': this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease });
                }
            }
            else if (this.renewalFetchResponse.NoOfKids == '2') {
                this.insuredForm.patchValue({ 'member1Cat': this.renewalFetchResponse.InsuredDetails[0].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member1Rela': this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant });
                var memb1title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[0].Title; }).val;
                this.insuredForm.patchValue({ 'member1Title': memb1title });
                this.insuredForm.patchValue({ 'member1Name': this.renewalFetchResponse.InsuredDetails[0].FullName });
                this.insuredForm.patchValue({ 'member1DOB': this.renewalFetchResponse.InsuredDetails[0].DateofBirth });
                this.insuredForm.patchValue({ 'member1Feet': this.renewalFetchResponse.InsuredDetails[0].Height });
                this.insuredForm.patchValue({ 'member1Inches': this.renewalFetchResponse.InsuredDetails[0].HeightInInches });
                this.insuredForm.patchValue({ 'member1Weight': this.renewalFetchResponse.InsuredDetails[0].Weight });
                if (this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'member1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'member1Disease': this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'member2Cat': this.renewalFetchResponse.InsuredDetails[1].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member2Rela': this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant });
                var memb2title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[1].Title; }).val;
                this.insuredForm.patchValue({ 'member2Title': memb2title });
                this.insuredForm.patchValue({ 'member2Name': this.renewalFetchResponse.InsuredDetails[1].FullName });
                this.insuredForm.patchValue({ 'member2DOB': this.renewalFetchResponse.InsuredDetails[1].DateofBirth });
                this.insuredForm.patchValue({ 'member2Feet': this.renewalFetchResponse.InsuredDetails[1].Height });
                this.insuredForm.patchValue({ 'member2Inches': this.renewalFetchResponse.InsuredDetails[1].HeightInInches });
                this.insuredForm.patchValue({ 'member2Weight': this.renewalFetchResponse.InsuredDetails[1].Weight });
                if (this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'member2Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'member2Disease': this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'child1Cat': this.renewalFetchResponse.InsuredDetails[2].KidAdultType });
                this.insuredForm.patchValue({ 'child1Rela': this.renewalFetchResponse.InsuredDetails[2].RelationwithApplicant });
                var kid1Title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[2].Title; }).val;
                this.insuredForm.patchValue({ 'child1Title': kid1Title });
                this.insuredForm.patchValue({ 'child1Name': this.renewalFetchResponse.InsuredDetails[2].FullName });
                this.insuredForm.patchValue({ 'child1DOB': this.renewalFetchResponse.InsuredDetails[2].DateofBirth });
                this.insuredForm.patchValue({ 'child1Feet': this.renewalFetchResponse.InsuredDetails[2].Height });
                this.insuredForm.patchValue({ 'child1Inches': this.renewalFetchResponse.InsuredDetails[2].HeightInInches });
                this.insuredForm.patchValue({ 'child1Weight': this.renewalFetchResponse.InsuredDetails[2].Weight });
                if (this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'child1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'child1Disease': this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'child2Cat': this.renewalFetchResponse.InsuredDetails[3].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[3].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[3].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[3].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'child2Rela': this.renewalFetchResponse.InsuredDetails[3].RelationwithApplicant });
                var kid2Title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[3].Title; }).val;
                this.insuredForm.patchValue({ 'child2Title': kid2Title });
                this.insuredForm.patchValue({ 'child2Name': this.renewalFetchResponse.InsuredDetails[3].FullName });
                this.insuredForm.patchValue({ 'child2DOB': this.renewalFetchResponse.InsuredDetails[3].DateofBirth });
                this.insuredForm.patchValue({ 'child2Feet': this.renewalFetchResponse.InsuredDetails[3].Height });
                this.insuredForm.patchValue({ 'child2Inches': this.renewalFetchResponse.InsuredDetails[3].HeightInInches });
                this.insuredForm.patchValue({ 'child2Weight': this.renewalFetchResponse.InsuredDetails[3].Weight });
                if (this.renewalFetchResponse.InsuredDetails[3].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[3].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'child2Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'child2Disease': this.renewalFetchResponse.InsuredDetails[3].PreExistingDisease });
                }
            }
            else {
                this.insuredForm.patchValue({ 'member1Cat': this.renewalFetchResponse.InsuredDetails[0].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member1Rela': this.renewalFetchResponse.InsuredDetails[0].RelationwithApplicant });
                var memb1title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[0].Title; }).val;
                this.insuredForm.patchValue({ 'member1Title': memb1title });
                this.insuredForm.patchValue({ 'member1Name': this.renewalFetchResponse.InsuredDetails[0].FullName });
                this.insuredForm.patchValue({ 'member1DOB': this.renewalFetchResponse.InsuredDetails[0].DateofBirth });
                this.insuredForm.patchValue({ 'member1Feet': this.renewalFetchResponse.InsuredDetails[0].Height });
                this.insuredForm.patchValue({ 'member1Inches': this.renewalFetchResponse.InsuredDetails[0].HeightInInches });
                this.insuredForm.patchValue({ 'member1Weight': this.renewalFetchResponse.InsuredDetails[0].Weight });
                if (this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'member1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'member1Disease': this.renewalFetchResponse.InsuredDetails[0].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'member2Cat': this.renewalFetchResponse.InsuredDetails[1].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'member2Rela': this.renewalFetchResponse.InsuredDetails[1].RelationwithApplicant });
                var memb2title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[1].Title; }).val;
                this.insuredForm.patchValue({ 'member2Title': memb2title });
                this.insuredForm.patchValue({ 'member2Name': this.renewalFetchResponse.InsuredDetails[1].FullName });
                this.insuredForm.patchValue({ 'member2DOB': this.renewalFetchResponse.InsuredDetails[1].DateofBirth });
                this.insuredForm.patchValue({ 'member2Feet': this.renewalFetchResponse.InsuredDetails[1].Height });
                this.insuredForm.patchValue({ 'member2Inches': this.renewalFetchResponse.InsuredDetails[1].HeightInInches });
                this.insuredForm.patchValue({ 'member2Weight': this.renewalFetchResponse.InsuredDetails[1].Weight });
                if (this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'member2Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'member2Disease': this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'child1Cat': this.renewalFetchResponse.InsuredDetails[2].KidAdultType });
                this.insuredForm.patchValue({ 'child1Rela': this.renewalFetchResponse.InsuredDetails[2].RelationwithApplicant });
                var kid1Title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[2].Title; }).val;
                this.insuredForm.patchValue({ 'child1Title': kid1Title });
                this.insuredForm.patchValue({ 'child1Name': this.renewalFetchResponse.InsuredDetails[2].FullName });
                this.insuredForm.patchValue({ 'child1DOB': this.renewalFetchResponse.InsuredDetails[2].DateofBirth });
                this.insuredForm.patchValue({ 'child1Feet': this.renewalFetchResponse.InsuredDetails[2].Height });
                this.insuredForm.patchValue({ 'child1Inches': this.renewalFetchResponse.InsuredDetails[2].HeightInInches });
                this.insuredForm.patchValue({ 'child1Weight': this.renewalFetchResponse.InsuredDetails[2].Weight });
                if (this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'child1Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'child1Disease': this.renewalFetchResponse.InsuredDetails[1].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'child2Cat': this.renewalFetchResponse.InsuredDetails[3].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[3].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[3].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[3].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'child2Rela': this.renewalFetchResponse.InsuredDetails[3].RelationwithApplicant });
                var kid2Title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[3].Title; }).val;
                this.insuredForm.patchValue({ 'child2Title': kid2Title });
                this.insuredForm.patchValue({ 'child2Name': this.renewalFetchResponse.InsuredDetails[3].FullName });
                this.insuredForm.patchValue({ 'child2DOB': this.renewalFetchResponse.InsuredDetails[3].DateofBirth });
                this.insuredForm.patchValue({ 'child2Feet': this.renewalFetchResponse.InsuredDetails[3].Height });
                this.insuredForm.patchValue({ 'child2Inches': this.renewalFetchResponse.InsuredDetails[3].HeightInInches });
                this.insuredForm.patchValue({ 'child2Weight': this.renewalFetchResponse.InsuredDetails[3].Weight });
                if (this.renewalFetchResponse.InsuredDetails[3].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[3].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'child2Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'child2Disease': this.renewalFetchResponse.InsuredDetails[3].PreExistingDisease });
                }
                this.insuredForm.patchValue({ 'child3Cat': this.renewalFetchResponse.InsuredDetails[4].KidAdultType });
                if (this.renewalFetchResponse.InsuredDetails[4].RelationwithApplicant == undefined || this.renewalFetchResponse.InsuredDetails[4].RelationwithApplicant == null) {
                    this.renewalFetchResponse.InsuredDetails[4].RelationwithApplicant = '';
                }
                this.insuredForm.patchValue({ 'child3Rela': this.renewalFetchResponse.InsuredDetails[4].RelationwithApplicant });
                var kid3Title = this.titles.find(function (x) { return x.id == _this.renewalFetchResponse.InsuredDetails[4].Title; }).val;
                this.insuredForm.patchValue({ 'child3Title': kid3Title });
                this.insuredForm.patchValue({ 'child3Name': this.renewalFetchResponse.InsuredDetails[4].FullName });
                this.insuredForm.patchValue({ 'child3DOB': this.renewalFetchResponse.InsuredDetails[4].DateofBirth });
                this.insuredForm.patchValue({ 'child3Feet': this.renewalFetchResponse.InsuredDetails[4].Height });
                this.insuredForm.patchValue({ 'child3Inches': this.renewalFetchResponse.InsuredDetails[4].HeightInInches });
                this.insuredForm.patchValue({ 'child3Weight': this.renewalFetchResponse.InsuredDetails[4].Weight });
                if (this.renewalFetchResponse.InsuredDetails[4].PreExistingDisease == null || this.renewalFetchResponse.InsuredDetails[4].PreExistingDisease == undefined) {
                    this.insuredForm.patchValue({ 'child3Disease': 'No' });
                }
                else {
                    this.insuredForm.patchValue({ 'child3Disease': this.renewalFetchResponse.InsuredDetails[4].PreExistingDisease });
                }
            }
        }
    };
    HealthRenewalProposalPage.prototype.getNomineeRelation = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.nomineeData = JSON.parse(localStorage.getItem('relations'));
            console.log(_this.nomineeData);
            _this.nomineeRelation = _this.nomineeData.NomineeAppointeeRelationship;
            _this.adultRelationArray = [];
            _this.childRelationArray = [];
            _this.nomineeData.InsuredRelationship.forEach(function (relation) {
                if (relation.KidAdultType == 'Adult') {
                    _this.adultRelationArray.push(relation);
                }
                else {
                    _this.childRelationArray.push(relation);
                }
            });
            _this.adultRelationArray = _.sortBy(_this.adultRelationArray, 'RelationshipName');
            _this.childRelationArray = _.sortBy(_this.childRelationArray, 'RelationshipName');
            resolve();
        });
    };
    HealthRenewalProposalPage.prototype.getPinCodeData = function (code) {
        var _this = this;
        console.log(code);
        var data = code.target.value;
        if (data.length == 6) {
            this.inputLoading1 = true;
            this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', data).then(function (res) {
                console.log(res);
                _this.inputLoading1 = false;
                if (res.StatusCode == 1) {
                    _this.pinIdBody = { "Pincode": data, "CityID": res.CityList[0].CityID };
                    _this.stateId = res.StateId;
                    _this.cityId = res.CityList[0].CityID;
                    _this.applicantForm.patchValue({ 'applCity': res.CityList[0].CityName });
                    _this.applicantForm.patchValue({ 'applState': res.StateName });
                    _this.getPinCodeDetails();
                }
                else {
                    _this.applicantForm.patchValue({ 'applCity': null });
                    _this.applicantForm.patchValue({ 'applState': null });
                }
            });
        }
        else {
            console.log("Invalid Pin");
        }
    };
    HealthRenewalProposalPage.prototype.getPinCodeDetails = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var str = JSON.stringify(_this.pinIdBody);
            _this.cs.postWithParams('/api/RTOList/GetPincodeID', str).then(function (res) {
                console.log("Get Pin Data Value", res);
                _this.pinDataValue = res;
                resolve();
            });
        });
    };
    HealthRenewalProposalPage.prototype.getNomTitle = function (ev) {
        this.nomineeTitle = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.nomineeRelId = function (ev) {
        this.nomineeRelationId = this.nomineeRelation.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.getAge = function (ev) {
        var date = new Date();
        var age = date.getFullYear() - new Date(ev.value).getFullYear();
        console.log(age);
        if (age < 18) {
            this.showApointee = true;
        }
        else {
            this.applicantForm.patchValue({ 'appName': '' });
            this.applicantForm.patchValue({ 'appRelation': '' });
            this.applicantForm.patchValue({ 'appDOB': '' });
            if (this.appointeeRelationId == undefined) {
                this.appointeeRelationId = "";
            }
            if (this.appointeeTitle == undefined) {
                this.appointeeTitle = "";
            }
            this.showApointee = false;
        }
    };
    HealthRenewalProposalPage.prototype.getAppTitle = function (ev) {
        this.appointeeTitle = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.appointeeRelId = function (ev) {
        this.appointeeRelationId = this.nomineeRelation.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.createApplForm = function () {
        this.applicantForm = new FormGroup({
            applName: new FormControl(),
            aadhaarNo: new FormControl(''),
            panNumber: new FormControl(''),
            applDOB: new FormControl(),
            customerType: new FormControl(''),
            panNo: new FormControl(''),
            constitution: new FormControl(''),
            gstRegStatus: new FormControl(''),
            GSTIN: new FormControl(''),
            UIN: new FormControl(''),
            applAdd1: new FormControl(''),
            applAdd2: new FormControl(''),
            applLandMark: new FormControl(''),
            applPinCode: new FormControl(),
            applCity: new FormControl(),
            applState: new FormControl(),
            applEmail: new FormControl(),
            applMobNo: new FormControl(),
            nomTitle: new FormControl(''),
            nomName: new FormControl(),
            nomRelation: new FormControl(''),
            nomDOB: new FormControl(),
            apsId: new FormControl(),
            custRefNo: new FormControl(),
            appTitle: new FormControl(''),
            appName: new FormControl(),
            appRelation: new FormControl(''),
            appDOB: new FormControl()
        });
    };
    HealthRenewalProposalPage.prototype.isGSTReg = function (val) {
        if (val) {
            this.isRegWithGST = true;
        }
        else {
            this.isRegWithGST = false;
        }
    };
    HealthRenewalProposalPage.prototype.radio = function (val) {
        if (val == true) {
            this.isGSTN = true;
            this.isUIN = false;
        }
        else {
            this.isGSTN = false;
            this.isUIN = true;
        }
    };
    HealthRenewalProposalPage.prototype.createInsuredForm = function () {
        this.insuredForm = new FormGroup({
            // HB ADD-ON
            AddOn1: new FormControl(),
            AddOn1Varient: new FormControl(),
            AddOn2: new FormControl(),
            AddOn2Varient: new FormControl(),
            Addon2Members: new FormGroup({
                Member: new FormGroup({
                    AdultType: new FormControl(false),
                    AdultType2: new FormControl(false),
                })
            }),
            AddOn3: new FormControl(),
            AddOn3Varient: new FormControl(),
            Addon3Members: new FormGroup({
                Member: new FormGroup({
                    AdultType: new FormControl(false),
                    AdultType2: new FormControl(false),
                })
            }),
            subLimit: new FormControl(),
            // FOR CHI ADDON
            addOnCover1: new FormControl(),
            addOnCover3: new FormControl(),
            addOnCover5: new FormControl(),
            addOnCover6: new FormControl(),
            memberAge1: new FormControl(),
            memberAge2: new FormControl(),
            // member 1
            member1Cat: new FormControl(),
            member1Rela: new FormControl(''),
            member1Title: new FormControl(''),
            member1Name: new FormControl(''),
            member1DOB: new FormControl(),
            member1Feet: new FormControl(''),
            member1Inches: new FormControl(''),
            member1Weight: new FormControl(''),
            member1Disease: new FormControl(),
            // Member 2
            member2Cat: new FormControl('Adult'),
            member2Rela: new FormControl(''),
            member2Title: new FormControl(''),
            member2Name: new FormControl(''),
            member2DOB: new FormControl(),
            member2Feet: new FormControl(''),
            member2Inches: new FormControl(''),
            member2Weight: new FormControl(''),
            member2Disease: new FormControl(),
            // CHild 1
            child1Cat: new FormControl('Child'),
            child1Rela: new FormControl(''),
            child1Title: new FormControl(''),
            child1Name: new FormControl(),
            child1DOB: new FormControl(),
            child1Feet: new FormControl(''),
            child1Inches: new FormControl(''),
            child1Weight: new FormControl(),
            child1Disease: new FormControl(),
            // Child 2
            child2Cat: new FormControl('Child'),
            child2Rela: new FormControl(''),
            child2Title: new FormControl(''),
            child2Name: new FormControl(),
            child2DOB: new FormControl(),
            child2Feet: new FormControl(''),
            child2Inches: new FormControl(''),
            child2Weight: new FormControl(),
            child2Disease: new FormControl(),
            // CHild 3
            child3Cat: new FormControl('Child'),
            child3Rela: new FormControl(''),
            child3Title: new FormControl(''),
            child3Name: new FormControl(),
            child3DOB: new FormControl(),
            child3Feet: new FormControl(''),
            child3Inches: new FormControl(''),
            child3Weight: new FormControl(),
            child3Disease: new FormControl(),
        });
    };
    HealthRenewalProposalPage.prototype.getRelation1 = function (ev) {
        console.log(ev.target.value);
        this.relationId1 = this.adultRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; });
        console.log("rel2", this.relationId1);
    };
    HealthRenewalProposalPage.prototype.getRelation2 = function (ev) {
        console.log(ev.target.value, this.adultRelationArray);
        this.relationId2 = this.adultRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; });
        console.log("rel2", this.relationId2);
    };
    HealthRenewalProposalPage.prototype.getRelation3 = function (ev) {
        console.log(ev.target.value, this.childRelationArray);
        this.relationId3 = this.childRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; });
        console.log(this.relationId3);
    };
    HealthRenewalProposalPage.prototype.getRelation4 = function (ev) {
        console.log(ev.target.value);
        this.relationId4 = this.childRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.getRelation5 = function (ev) {
        console.log(ev.target.value);
        this.relationId5 = this.childRelationArray.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.getTitle1 = function (ev) {
        console.log(ev.target.value);
        this.titleId1 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.getTitle2 = function (ev) {
        console.log(ev.target.value);
        this.titleId2 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.getTitle3 = function (ev) {
        console.log(ev.target.value);
        this.titleId3 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.getTitle4 = function (ev) {
        console.log(ev.target.value);
        this.titleId4 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.getTitle5 = function (ev) {
        console.log(ev.target.value);
        this.titleId5 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    HealthRenewalProposalPage.prototype.getQuestionList = function () {
        var _this = this;
        this.cs.getWithParams('/api/healthmaster/GetHealthAilmentList?isAgent=YES').then(function (res) {
            console.log("JSON Ailment", res);
            _this.questionList = res.Details;
        }).catch(function (err) {
            console.log(err);
        });
    };
    HealthRenewalProposalPage.prototype.isDiseaseVal2 = function (val) {
        if (val) {
            this.isDisease2 = true;
        }
        else {
            this.isDisease2 = false;
        }
    };
    HealthRenewalProposalPage.prototype.isDiseaseVal3 = function (val) {
        if (val) {
            this.isDisease3 = true;
        }
        else {
            this.isDisease3 = false;
        }
    };
    HealthRenewalProposalPage.prototype.isDiseaseVal4 = function (val) {
        if (val) {
            this.isDisease4 = true;
        }
        else {
            this.isDisease4 = false;
        }
    };
    HealthRenewalProposalPage.prototype.isDiseaseVal5 = function (val) {
        if (val) {
            this.isDisease5 = true;
        }
        else {
            this.isDisease5 = false;
        }
    };
    HealthRenewalProposalPage.prototype.getDiseases1 = function (d, ev) {
        if (ev.detail.checked) {
            for (var i = 0; i < this.ailmentArray1.length; i++) {
                if (this.ailmentArray1[i].AilmentName == d.Name) {
                    var index = this.ailmentArray1.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray1.splice(index, 1);
                }
            }
            this.ailmentArray1.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value });
        }
        else {
            for (var i = 0; i < this.ailmentArray1.length; i++) {
                if (this.ailmentArray1[i].AilmentName == d.Name) {
                    var index = this.ailmentArray1.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray1.splice(index, 1);
                }
            }
        }
    };
    HealthRenewalProposalPage.prototype.getDiseases2 = function (d, ev) {
        if (ev.detail.checked) {
            for (var i = 0; i < this.ailmentArray2.length; i++) {
                if (this.ailmentArray2[i].AilmentName == d.Name) {
                    var index = this.ailmentArray2.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray2.splice(index, 1);
                }
            }
            this.ailmentArray2.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value });
        }
        else {
            for (var i = 0; i < this.ailmentArray2.length; i++) {
                if (this.ailmentArray2[i].AilmentName == d.Name) {
                    var index = this.ailmentArray2.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray2.splice(index, 1);
                }
            }
        }
    };
    HealthRenewalProposalPage.prototype.getDiseases3 = function (d, ev) {
        if (ev.detail.checked) {
            for (var i = 0; i < this.ailmentArray3.length; i++) {
                if (this.ailmentArray3[i].AilmentName == d.Name) {
                    var index = this.ailmentArray3.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray3.splice(index, 1);
                }
            }
            this.ailmentArray3.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value });
        }
        else {
            for (var i = 0; i < this.ailmentArray3.length; i++) {
                if (this.ailmentArray3[i].AilmentName == d.Name) {
                    var index = this.ailmentArray3.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray3.splice(index, 1);
                }
            }
        }
    };
    HealthRenewalProposalPage.prototype.getDiseases4 = function (d, ev) {
        if (ev.detail.checked) {
            for (var i = 0; i < this.ailmentArray4.length; i++) {
                if (this.ailmentArray4[i].AilmentName == d.Name) {
                    var index = this.ailmentArray4.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray4.splice(index, 1);
                }
            }
            this.ailmentArray4.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value });
        }
        else {
            for (var i = 0; i < this.ailmentArray4.length; i++) {
                if (this.ailmentArray4[i].AilmentName == d.Name) {
                    var index = this.ailmentArray4.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray4.splice(index, 1);
                }
            }
        }
    };
    HealthRenewalProposalPage.prototype.getDiseases5 = function (d, ev) {
        if (ev.detail.checked) {
            for (var i = 0; i < this.ailmentArray5.length; i++) {
                if (this.ailmentArray5[i].AilmentName == d.Name) {
                    var index = this.ailmentArray5.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray5.splice(index, 1);
                }
            }
            this.ailmentArray5.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value });
        }
        else {
            for (var i = 0; i < this.ailmentArray5.length; i++) {
                if (this.ailmentArray5[i].AilmentName == d.Name) {
                    var index = this.ailmentArray5.findIndex(function (record) { return record.AilmentName === d.Name; });
                    this.ailmentArray5.splice(index, 1);
                }
            }
        }
    };
    HealthRenewalProposalPage.prototype.getFeet = function (ev) {
        //  console.log(ev.detail.value);
    };
    HealthRenewalProposalPage.prototype.getInches = function (ev) {
        //  console.log(ev.detail.value);
    };
    HealthRenewalProposalPage.prototype.showBasicDetails = function (ev) {
        this.isBasicDetails = !this.isBasicDetails;
        if (this.isBasicDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("basicDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("basicDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("basicDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("basicDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalProposalPage.prototype.showInsuredDetails = function (ev) {
        this.isInsuredDetails = !this.isInsuredDetails;
        if (this.isInsuredDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalProposalPage.prototype.showApplicantDetails = function (ev) {
        this.isApplicantDetails = !this.isApplicantDetails;
        if (this.isApplicantDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    HealthRenewalProposalPage.prototype.createRnMembers = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (_this.isDisease == false) {
                _this.ailmentArray1 = [];
            }
            if (_this.isDisease2 == false) {
                _this.ailmentArray2 = [];
            }
            if (_this.isDisease3 == false) {
                _this.ailmentArray3 = [];
            }
            if (_this.isDisease4 == false) {
                _this.ailmentArray4 = [];
            }
            if (_this.isDisease5 == false) {
                _this.ailmentArray5 = [];
            }
            if (_this.renewalFetchResponse.NoOfAdults == '0') {
                _this.memberObj = [{
                        "MemberType": _this.insuredForm.value.member1Cat,
                        "TitleID": _this.titleId3.id,
                        "Name": _this.insuredForm.value.child1Name,
                        "RelationshipID": _this.relationId3.RelationshipID,
                        "RelationshipName": _this.insuredForm.value.child1Rela,
                        "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'),
                        "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                        "Weight": _this.insuredForm.value.child1Weight,
                        "isExisting": "true",
                        "OtherDisease": "",
                        "Ailments": _this.ailmentArray3
                    }];
                resolve();
            }
            else if (_this.renewalFetchResponse.NoOfAdults == '1') {
                if (_this.renewalFetchResponse.NoOfKids == '0') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat,
                            "TitleID": _this.titleId1.id,
                            "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray1
                        }];
                    resolve();
                }
                else if (_this.renewalFetchResponse.NoOfKids == '1') {
                    console.log(_this.relationId3);
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat,
                            "TitleID": _this.titleId1.id,
                            "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat,
                            "TitleID": _this.titleId3.id,
                            "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray3
                        }];
                    resolve();
                }
                else if (_this.renewalFetchResponse.NoOfKids == '2') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat,
                            "TitleID": _this.titleId1.id,
                            "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat,
                            "TitleID": _this.titleId3.id,
                            "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray3
                        }, {
                            "MemberType": _this.insuredForm.value.child2Cat,
                            "TitleID": _this.titleId4.id,
                            "Name": _this.insuredForm.value.child2Name,
                            "RelationshipID": _this.relationId4.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.child2Rela,
                            "DOB": moment(_this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.child2Feet + '.' + _this.insuredForm.value.child2Inches,
                            "Weight": _this.insuredForm.value.child2Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray4
                        }];
                    resolve();
                }
            }
            else if (_this.renewalFetchResponse.NoOfAdults == '2') {
                if (_this.renewalFetchResponse.NoOfKids == '0') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat,
                            "TitleID": _this.titleId1.id,
                            "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.member2Cat,
                            "TitleID": _this.titleId2.id,
                            "Name": _this.insuredForm.value.member2Name,
                            "RelationshipID": _this.relationId2.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member2Rela,
                            "DOB": moment(_this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member2Feet + '.' + _this.insuredForm.value.member2Inches,
                            "Weight": _this.insuredForm.value.member2Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray2
                        }];
                    resolve();
                }
                else if (_this.renewalFetchResponse.NoOfKids == '1') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat,
                            "TitleID": _this.titleId1.id,
                            "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.member2Cat,
                            "TitleID": _this.titleId2.id,
                            "Name": _this.insuredForm.value.member2Name,
                            "RelationshipID": _this.relationId2.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member2Rela,
                            "DOB": moment(_this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member2Feet + '.' + _this.insuredForm.value.member2Inches,
                            "Weight": _this.insuredForm.value.member2Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray2
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat,
                            "TitleID": _this.titleId3.id,
                            "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray3
                        }];
                    resolve();
                }
                else if (_this.renewalFetchResponse.NoOfKids == '2') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat,
                            "TitleID": _this.titleId1.id,
                            "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.member2Cat,
                            "TitleID": _this.titleId2.id,
                            "Name": _this.insuredForm.value.member2Name,
                            "RelationshipID": _this.relationId2.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member2Rela,
                            "DOB": moment(_this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member2Feet + '.' + _this.insuredForm.value.member2Inches,
                            "Weight": _this.insuredForm.value.member2Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray2
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat,
                            "TitleID": _this.titleId3.id,
                            "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray3
                        }, {
                            "MemberType": _this.insuredForm.value.child2Cat,
                            "TitleID": _this.titleId4.id,
                            "Name": _this.insuredForm.value.child2Name,
                            "RelationshipID": _this.relationId4.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.child2Rela,
                            "DOB": moment(_this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.child2Feet + '.' + _this.insuredForm.value.child2Inches,
                            "Weight": _this.insuredForm.value.child2Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray4
                        }];
                    resolve();
                }
                else if (_this.renewalFetchResponse.NoOfKids == '2') {
                    _this.memberObj = [{
                            "MemberType": _this.insuredForm.value.member1Cat,
                            "TitleID": _this.titleId1.id,
                            "Name": _this.insuredForm.value.member1Name,
                            "RelationshipID": _this.relationId1.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member1Rela,
                            "DOB": moment(_this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member1Feet + '.' + _this.insuredForm.value.member1Inches,
                            "Weight": _this.insuredForm.value.member1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray1
                        }, {
                            "MemberType": _this.insuredForm.value.member2Cat,
                            "TitleID": _this.titleId2.id,
                            "Name": _this.insuredForm.value.member2Name,
                            "RelationshipID": _this.relationId2.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.member2Rela,
                            "DOB": moment(_this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.member2Feet + '.' + _this.insuredForm.value.member2Inches,
                            "Weight": _this.insuredForm.value.member2Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray2
                        }, {
                            "MemberType": _this.insuredForm.value.child1Cat,
                            "TitleID": _this.titleId3.id,
                            "Name": _this.insuredForm.value.child1Name,
                            "RelationshipID": _this.relationId3.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.child1Rela,
                            "DOB": moment(_this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.child1Feet + '.' + _this.insuredForm.value.child1Inches,
                            "Weight": _this.insuredForm.value.child1Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray3
                        }, {
                            "MemberType": _this.insuredForm.value.child2Cat,
                            "TitleID": _this.titleId4.id,
                            "Name": _this.insuredForm.value.child2Name,
                            "RelationshipID": _this.relationId4.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.child2Rela,
                            "DOB": moment(_this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.child2Feet + '.' + _this.insuredForm.value.child2Inches,
                            "Weight": _this.insuredForm.value.child2Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray4
                        }, {
                            "MemberType": _this.insuredForm.value.child3Cat,
                            "TitleID": _this.titleId5.id,
                            "Name": _this.insuredForm.value.child3Name,
                            "RelationshipID": _this.relationId5.RelationshipID,
                            "RelationshipName": _this.insuredForm.value.child3Rela,
                            "DOB": moment(_this.insuredForm.value.child3DOB).format('DD-MMM-YYYY'),
                            "Height": _this.insuredForm.value.child3Feet + '.' + _this.insuredForm.value.child3Inches,
                            "Weight": _this.insuredForm.value.child3Weight.toString(),
                            "isExisting": "true",
                            "OtherDisease": "",
                            "Ailments": _this.ailmentArray5
                        }];
                    resolve();
                }
            }
        });
    };
    HealthRenewalProposalPage.prototype.getConstitute = function (ev) {
        var constData = this.constituentArray.find(function (x) { return x.id == ev.target.value; });
        this.constitutionName = constData.val;
    };
    HealthRenewalProposalPage.prototype.getCustTypeName = function (ev) {
        console.log(ev.target.value, this.customerTypeArray);
        var custTypeData = this.customerTypeArray.find(function (x) { return x.id == ev.target.value; });
        this.customerType = custTypeData.val;
    };
    HealthRenewalProposalPage.prototype.getGSTName = function (ev) {
        var gstNameData = this.gstStatusArray.find(function (x) { return x.id == ev.target.value; });
        this.gstName = gstNameData.val;
    };
    HealthRenewalProposalPage.prototype.hello = function (ev) {
        console.log("Hello", ev);
    };
    HealthRenewalProposalPage.prototype.createObject = function (ev) {
        console.log("Dummy Data", JSON.stringify(this.dummyObject));
    };
    HealthRenewalProposalPage.prototype.submit = function (basic, applicant) {
        var _this = this;
        var tempAppointDate;
        var isFormValid = this.ValidationApplicant();
        var isinsuredFeildsValid = this.validatedInsuredMembers();
        // For HB AddOn
        if (this.productType == 43) {
            if (this.insuredForm.value.Addon2Members.Member.AdultType == true) {
                this.renewalFetchResponse.AddOnCovers2And4 = "Adult 1";
            }
            else if (this.insuredForm.value.Addon2Members.Member.AdultType2 == true) {
                this.renewalFetchResponse.AddOnCovers2And4 = "Adult 2";
            }
            else if (this.insuredForm.value.Addon2Members.Member.AdultType == true && this.insuredForm.value.Addon2Members.Member.AdultType2 == true) {
                this.renewalFetchResponse.AddOnCovers2And4 = "Both Adult";
            }
            else {
                this.renewalFetchResponse.AddOnCovers2And4 = "";
            }
            if (this.insuredForm.value.Addon3Members.Member.AdultType == true) {
                this.renewalFetchResponse.AddOnCovers3 = "Adult 1";
            }
            else if (this.insuredForm.value.Addon3Members.Member.AdultType2 == true) {
                this.renewalFetchResponse.AddOnCovers3 = "Adult 2";
            }
            else if (this.insuredForm.value.Addon3Members.Member.AdultType == true && this.insuredForm.value.Addon3Members.Member.AdultType2 == true) {
                this.renewalFetchResponse.AddOnCovers3 = "Both Adult";
            }
            else {
                this.renewalFetchResponse.AddOnCovers3 = "";
            }
        }
        if (this.productType == 6) {
            if (this.insuredForm.value.addOnCover1 == true) {
                this.insuredForm.value.AddOn1 = true;
            }
            else {
                this.insuredForm.value.AddOn1 = false;
            }
        }
        if (isFormValid && isinsuredFeildsValid) {
            if (basic.value.VDValues == undefined || basic.value.VDValues == null) {
                basic.value.VDValues = '';
            }
            if (this.insuredForm.value.AddOn1Varient == undefined || this.insuredForm.value.AddOn1Varient == null) {
                this.insuredForm.value.AddOn1Varient = "";
            }
            if (this.insuredForm.value.AddOn1 == undefined || this.insuredForm.value.AddOn1 == null) {
                this.insuredForm.value.AddOn1 = "";
            }
            if (this.renewalFetchResponse.AddOnCovers2And4 == undefined || this.renewalFetchResponse.AddOnCovers2And4 == null) {
                this.renewalFetchResponse.AddOnCovers2And4 = "";
            }
            if (this.insuredForm.value.AddOn2 == undefined || this.insuredForm.value.AddOn2 == null) {
                this.insuredForm.value.AddOn2 = "";
            }
            if (this.insuredForm.value.AddOn2Varient == undefined || this.insuredForm.value.AddOn2Varient == null) {
                this.insuredForm.value.AddOn2Varient = "";
            }
            if (this.renewalFetchResponse.AddOnCovers3 == undefined || this.renewalFetchResponse.AddOnCovers3 == null) {
                this.renewalFetchResponse.AddOnCovers3 = "";
            }
            if (this.insuredForm.value.AddOn3 == undefined || this.insuredForm.value.AddOn3 == null) {
                this.insuredForm.value.AddOn3 = "";
            }
            if (this.insuredForm.value.AddOn3Varient == undefined || this.insuredForm.value.AddOn3Varient == null) {
                this.insuredForm.value.AddOn3Varient = "";
            }
            if (this.renewalFetchResponse.AddOnCovers5 == undefined || this.renewalFetchResponse.AddOnCovers5 == null) {
                this.renewalFetchResponse.AddOnCovers5 = "";
            }
            if (this.renewalFetchResponse.AddOnCovers5Flag == undefined || this.renewalFetchResponse.AddOnCovers5Flag == null) {
                this.renewalFetchResponse.AddOnCovers5Flag = "";
            }
            if (this.renewalFetchResponse.AddOnCovers6 == undefined || this.renewalFetchResponse.AddOnCovers6 == null) {
                this.renewalFetchResponse.AddOnCovers6 = "";
            }
            if (this.renewalFetchResponse.AddOnCovers6Flag == undefined || this.renewalFetchResponse.AddOnCovers6Flag == null) {
                this.renewalFetchResponse.AddOnCovers6Flag = "";
            }
            if (this.applicantForm.value.applLandMark == undefined || this.applicantForm.value.applLandMark == null) {
                this.applicantForm.value.applLandMark = '';
            }
            if (this.applicantForm.value.aadhaarNo == undefined || this.applicantForm.value.aadhaarNo == null) {
                this.applicantForm.value.aadhaarNo = '';
            }
            if (this.applicantForm.value.panNumber == undefined || this.applicantForm.value.panNumber == null) {
                this.applicantForm.value.panNumber = '';
            }
            if (this.applicantForm.value.applAdd2 == undefined || this.applicantForm.value.applAdd2 == null) {
                this.applicantForm.value.applAdd2 = '';
            }
            if (this.applicantForm.value.appDOB == undefined || this.applicantForm.value.appDOB == null) {
                tempAppointDate = '';
            }
            else {
                tempAppointDate = moment(applicant.value.appDOB).format('YYYY-MM-DD');
            }
            this.isRenewalEdited = true;
            this.isInsuredChange = false;
            this.createRnMembers().then(function () {
                var body = {
                    "UserType": "AGENT",
                    "PolicyNo": _this.renewalFetchResponse.PolicyNo,
                    "ipaddress": config.ipAddress,
                    "AadhaarNumber": _this.applicantForm.value.aadhaarNo,
                    "AddOnCovers1": _this.insuredForm.value.AddOn1Varient,
                    "AddOnCovers1Flag": _this.insuredForm.value.AddOn1,
                    "AddOnCovers2And4": _this.renewalFetchResponse.AddOnCovers2And4,
                    "AddOnCovers2And4Flag": _this.insuredForm.value.AddOn2,
                    "AddOnCovers2And4Type": _this.insuredForm.value.AddOn2Varient,
                    "AddOnCovers3": _this.renewalFetchResponse.AddOnCovers3,
                    "AddOnCovers3Flag": _this.insuredForm.value.AddOn3,
                    "AddOnCovers3Type": _this.insuredForm.value.AddOn3Varient,
                    "AddOnCovers5": _this.renewalFetchResponse.AddOnCovers5,
                    "AddOnCovers5Flag": _this.renewalFetchResponse.AddOnCovers5Flag,
                    "AddOnCovers6": _this.renewalFetchResponse.AddOnCovers6,
                    "AddOnCovers6Flag": _this.renewalFetchResponse.AddOnCovers6Flag,
                    "AddressLine1": _this.applicantForm.value.applAdd1,
                    "AddressLine2": _this.applicantForm.value.applAdd2,
                    "VoluntaryDeductible": basic.value.VDValues,
                    "AgeOfEldest": _this.renewalFetchResponse.AgeOfEldest,
                    "AppointeeDOB": tempAppointDate,
                    "AppointeeLastName": _this.applicantForm.value.appName,
                    "AppointeeRelationShipID": _this.appointeeRelationId.RelationshipID,
                    "AppointeeRelationShipName": _this.applicantForm.value.appRelation,
                    "AppointeeTitleID": _this.appointeeTitle.id,
                    "EmailID": _this.applicantForm.value.applEmail,
                    "Landmark": _this.applicantForm.value.applLandMark,
                    "MobileNo": _this.applicantForm.value.applMobNo,
                    "NoOfAdults": _this.noOfAudult,
                    "NoOfKids": _this.noOfChild,
                    "PANNumber": _this.applicantForm.value.panNumber,
                    "PincodeID": _this.applicantForm.value.applPinCode,
                    'NameOfApplicant': _this.applicantForm.value.applName,
                    'CityName': _this.applicantForm.value.applCity,
                    'AdditionalSI': _this.basicForm.value.additionalSum,
                    "NomineeDOB": moment(applicant.value.nomDOB).format('YYYY-MM-DD'),
                    "NomineeName": applicant.value.nomName,
                    "NomineeRelationShip": applicant.value.nomRelation,
                    "NomineeRelationShipID": _this.nomineeRelationId.RelationshipID,
                    "NomineeTitle": _this.nomineeTitle.id,
                    "SubLimitApplicable": _this.insuredForm.value.subLimit,
                    "SumInsured": basic.value.siValue,
                    "isRenewalEdited": _this.isRenewalEdited,
                    "isInsuredChange": _this.isInsuredChange,
                    "PfCustomerId": _this.renewalFetchResponse.PfCustomerId,
                    "Members": {
                        "Member": _this.memberObj
                    },
                    "CustStateName": _this.renewalFetchResponse.StateName,
                    "CustStateId": _this.renewalFetchResponse.StateID,
                    "isGSTRegistered": _this.isRegWithGST,
                    "SoftCopyDiscount": ""
                };
                var str = JSON.stringify(body);
                localStorage.setItem('renewalBody', str);
                _this.cs.showLoader = true;
                _this.cs.postWithParams('/api/health/HealthPolicyRenewalCalculate', str).then(function (res) {
                    if (res.StatusCode == 1) {
                        localStorage.setItem('renewalPolicyData', JSON.stringify(res.HealthList[0]));
                        _this.router.navigateByUrl('health-renewal-summary');
                        _this.cs.showLoader = false;
                    }
                    else {
                        _this.cs.showLoader = false;
                    }
                }).catch(function (err) {
                    _this.cs.showLoader = false;
                });
            });
        }
    };
    HealthRenewalProposalPage.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HealthRenewalProposalPage.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    HealthRenewalProposalPage.prototype.validateMobile = function (mobile) {
        var mob = /^[0-9]{10}$/;
        return mob.test(mobile);
    };
    HealthRenewalProposalPage.prototype.validateGST = function (gst) {
        var _this = this;
        var re = /^[0-9]{2}[A-Z]{3}[PCHABGJLEFT][A-Z][0-9]{4}[A-Z]{1}[0-9A-Z]{1}[Z]{1}[0-9A-Z]{1}$/;
        var gstvalid = re.test(gst);
        if (gstvalid) {
            var gstcodeObtained = gst.substring(0, 2);
            var gstcode = this.states.find(function (x) { return x.StateId == _this.renewalFetchResponse.StateID; }).GSTStateCode;
            if (gstcodeObtained == gstcode) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return gstvalid;
        }
    };
    HealthRenewalProposalPage.prototype.validateAdhaarNo = function (addharno) {
        var re = /^[0-9]{12}$/;
        return re.test(addharno);
    };
    HealthRenewalProposalPage.prototype.validatePanNo = function (panno) {
        var re = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
        return re.test(panno);
    };
    HealthRenewalProposalPage.prototype.ValidateGSTSection = function () {
        if (this.isRegWithGST == true) {
            if (this.applicantForm.value.constitution == undefined || this.applicantForm.value.constitution == null || this.applicantForm.value.constitution == '') {
                this.presentAlert("Please select constitution of business for GST section");
                return false;
            }
            else if (this.applicantForm.value.panNo == undefined || this.applicantForm.value.panNo == null || this.applicantForm.value.panNo == '') {
                this.presentAlert("Please enter pan card number for GST section");
                return false;
            }
            else if (this.applicantForm.value.panNo != undefined && this.applicantForm.value.panNo != null && this.applicantForm.value.panNo != '' && !this.validatePanNo(this.applicantForm.value.panNo)) {
                this.presentAlert('Please enter valid pan card number for GST section');
                return false;
            }
            else if (this.customerType == undefined || this.customerType == null || this.customerType == '') {
                this.presentAlert("Please select customer type for GST section");
                return false;
            }
            else if (this.applicantForm.value.gstRegStatus == undefined || this.applicantForm.value.gstRegStatus == null || this.applicantForm.value.gstRegStatus == '') {
                this.presentAlert("Please select registration status for GST section");
                return false;
            }
            else if ((this.applicantForm.value.GSTIN == undefined || this.applicantForm.value.GSTIN == null || this.applicantForm.value.GSTIN == '') && this.isGSTN) {
                this.presentAlert("Please enter GSTIN number");
                return false;
            }
            else if (!this.validateGST(this.applicantForm.value.GSTIN) && this.isGSTN) {
                this.presentAlert("Please enter valid GSTIN number");
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    };
    HealthRenewalProposalPage.prototype.getDeclarationApproval = function (event) {
        this.isDeclarationAccepted = event.target.checked;
    };
    HealthRenewalProposalPage.prototype.ValidationApplicant = function () {
        var isGSTSectionValid = this.ValidateGSTSection();
        if (!isGSTSectionValid) {
            return false;
        }
        else if (this.applicantForm.value.aadhaarNo != undefined && this.applicantForm.value.aadhaarNo != null && this.applicantForm.value.aadhaarNo != '' && !this.validateAdhaarNo(this.applicantForm.value.aadhaarNo)) {
            this.presentAlert('Kindly Enter Valid Applicant Aadhaar Number');
            return false;
        }
        else if (this.applicantForm.value.panNumber != undefined && this.applicantForm.value.panNumber != null && this.applicantForm.value.panNumber != '' && !this.validatePanNo(this.applicantForm.value.panNumber)) {
            this.presentAlert('Kindly Enter Valid Applicant Pan Card Number');
            return false;
        }
        else if (this.applicantForm.value.applName == undefined || this.applicantForm.value.applName == null || this.applicantForm.value.applName == '') {
            this.presentAlert("Kindly Insert Applicant Name");
            return false;
        }
        else if (this.applicantForm.value.applDOB == undefined || this.applicantForm.value.applDOB == null || this.applicantForm.value.applDOB == '') {
            this.presentAlert("Kindly Enter Applicant Date of Birth");
            return false;
        }
        else if (this.applicantForm.value.applAdd1 == undefined || this.applicantForm.value.applAdd1 == null || this.applicantForm.value.applAdd1 == '') {
            this.presentAlert("Kindly Insert Applicant Correspondence Address");
            return false;
        }
        else if (this.applicantForm.value.applPinCode == undefined || this.applicantForm.value.applPinCode == null || this.applicantForm.value.applPinCode == '') {
            this.presentAlert("Kindly Insert Applicant Pincode");
            return false;
        }
        else if (this.applicantForm.value.applCity == undefined || this.applicantForm.value.applCity == null || this.applicantForm.value.applCity == '') {
            this.presentAlert("Applicant City not found");
            return false;
        }
        else if (this.applicantForm.value.applState == undefined || this.applicantForm.value.applState == null || this.applicantForm.value.applState == '') {
            this.presentAlert("Applicant State not found");
            return false;
        }
        else if (!this.validateEmail(this.applicantForm.value.applEmail)) {
            this.presentAlert("Kindly Insert Applicant applEmail");
            return false;
        }
        else if (this.applicantForm.value.nomTitle == undefined || this.applicantForm.value.nomTitle == null || this.applicantForm.value.nomTitle == '') {
            this.presentAlert("Kindly Select Nominee Title");
            return false;
        }
        else if (this.applicantForm.value.nomName == undefined || this.applicantForm.value.nomName == null || this.applicantForm.value.nomName == '') {
            this.presentAlert("Kindly Insert Nominee Name");
            return false;
        }
        else if (this.applicantForm.value.nomRelation == undefined || this.applicantForm.value.nomRelation == null || this.applicantForm.value.nomRelation == '') {
            this.presentAlert("Kindly Select Nominee Relation");
            return false;
        }
        else if (this.applicantForm.value.nomDOB == undefined || this.applicantForm.value.nomDOB == null || this.applicantForm.value.nomDOB == '') {
            this.presentAlert("Kindly Select Nominee Date of Birth");
            return false;
        }
        else if (this.applicantForm.value.applMobNo == undefined || this.applicantForm.value.applMobNo == null || this.applicantForm.value.applMobNo == '') {
            this.presentAlert("Kindly Insert Your Mobile Number");
            return false;
        }
        else if (!this.validateMobile(this.applicantForm.value.applMobNo)) {
            this.presentAlert("Mobile Number should be 10 digit");
            return false;
        }
        else if (this.isDeclarationAccepted == false) {
            this.presentAlert("Please Accept the Declaration");
            return false;
        }
        else {
            return true;
        }
    };
    HealthRenewalProposalPage.prototype.validatedInsuredMembers = function () {
        var totalAdults, totalKids, isAdult1Valid = true, isAdult2Valid = true, isChild1Valid = true, isChild2Valid = true, isChild3Valid = true;
        if (this.renewalFetchResponse.NoOfAdults != undefined && this.renewalFetchResponse.NoOfAdults != null && this.renewalFetchResponse.NoOfAdults != '') {
            totalAdults = this.renewalFetchResponse.NoOfAdults;
        }
        else {
            totalAdults = 0;
        }
        if (this.renewalFetchResponse.NoOfKids != undefined && this.renewalFetchResponse.NoOfKids != null && this.renewalFetchResponse.NoOfKids != '') {
            totalKids = this.renewalFetchResponse.NoOfKids;
        }
        else {
            totalKids = 0;
        }
        if (totalAdults == 1 || totalAdults == 2) {
            if (this.insuredForm.value.member1Rela == undefined || this.insuredForm.value.member1Rela == null || this.insuredForm.value.member1Rela == '') {
                this.presentAlert("Kindly Select Relationship of Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1Title == undefined || this.insuredForm.value.member1Title == null || this.insuredForm.value.member1Title == '') {
                this.presentAlert("Kindly Select Title of Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1Name == undefined || this.insuredForm.value.member1Name == null || this.insuredForm.value.member1Name == '') {
                this.presentAlert("Kindly Insert Name of Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1DOB == undefined || this.insuredForm.value.member1DOB == null || this.insuredForm.value.member1DOB == '') {
                this.presentAlert("Kindly Insert DOB for Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1Weight == undefined || this.insuredForm.value.member1Weight == null || this.insuredForm.value.member1Weight == '') {
                this.presentAlert("Kindly Insert Weight of Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1Feet == undefined || this.insuredForm.value.member1Feet == null || this.insuredForm.value.member1Feet == '') {
                this.presentAlert("Kindly Insert Height in Feet of Adult 1 under Insured");
                isAdult1Valid = false;
            }
            else if (this.insuredForm.value.member1Inches == undefined || this.insuredForm.value.member1Inches == null || this.insuredForm.value.member1Inches == '') {
                this.presentAlert("Kindly Insert Height in Inches of Adult 1 under Insured");
                isAdult1Valid = false;
            }
        }
        else {
            isAdult1Valid = true;
        }
        if (totalAdults == 2) {
            if (this.insuredForm.value.member2Rela == undefined || this.insuredForm.value.member2Rela == null || this.insuredForm.value.member2Rela == '') {
                this.presentAlert("Kindly Select Relationship for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2Title == undefined || this.insuredForm.value.member2Title == null || this.insuredForm.value.member2Title == '') {
                this.presentAlert("Kindly Select Title for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2Name == undefined || this.insuredForm.value.member2Name == null || this.insuredForm.value.member2Name == '') {
                this.presentAlert("Kindly Insert Name for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2DOB == undefined || this.insuredForm.value.member2DOB == null || this.insuredForm.value.member2DOB == '') {
                this.presentAlert("Kindly Insert DOB for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2Weight == undefined || this.insuredForm.value.member2Weight == null || this.insuredForm.value.member2Weight == '') {
                this.presentAlert("Kindly Insert Weight for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2Feet == undefined || this.insuredForm.value.member2Feet == null || this.insuredForm.value.member2Feet == '') {
                this.presentAlert("Kindly Insert Height in Feet for Adult 2 under Insured");
                isAdult2Valid = false;
            }
            else if (this.insuredForm.value.member2Inches == undefined || this.insuredForm.value.member2Inches == null || this.insuredForm.value.member2Inches == '') {
                this.presentAlert("Kindly Insert Height in Inches for Adult 2 under Insured");
                isAdult2Valid = false;
            }
        }
        else {
            isAdult2Valid = true;
        }
        if (totalKids == 1 || totalKids == 2 || totalKids == 3) {
            if (this.insuredForm.value.child1Rela == undefined || this.insuredForm.value.child1Rela == null || this.insuredForm.value.child1Rela == '') {
                this.presentAlert("Kindly Select Relationship for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1Title == undefined || this.insuredForm.value.child1Title == null || this.insuredForm.value.child1Title == '') {
                this.presentAlert("Kindly Select Title for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1Name == undefined || this.insuredForm.value.child1Name == null || this.insuredForm.value.child1Name == '') {
                this.presentAlert("Kindly Insert Name for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1DOB == undefined || this.insuredForm.value.child1DOB == null || this.insuredForm.value.child1DOB == '') {
                this.presentAlert("Kindly Insert DOB for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1Weight == undefined || this.insuredForm.value.child1Weight == null || this.insuredForm.value.child1Weight == '') {
                this.presentAlert("Kindly Insert Weight for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1Feet == undefined || this.insuredForm.value.child1Feet == null || this.insuredForm.value.child1Feet == '') {
                this.presentAlert("Kindly Insert Height in Feet for Child 1 under Insured");
                isChild1Valid = false;
            }
            else if (this.insuredForm.value.child1Inches == undefined || this.insuredForm.value.child1Inches == null || this.insuredForm.value.child1Inches == '') {
                this.presentAlert("Kindly Insert Height in Inches for Child 1 under Insured");
                isChild1Valid = false;
            }
        }
        else {
            isChild1Valid = true;
        }
        if (totalKids == 2 || totalKids == 3) {
            if (this.insuredForm.value.child2Rela == undefined || this.insuredForm.value.child2Rela == null || this.insuredForm.value.child2Rela == '') {
                this.presentAlert("Kindly Select  Relationship Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2Rela == undefined || this.insuredForm.value.child2Rela == null || this.insuredForm.value.child2Rela == '') {
                this.presentAlert("Kindly Select Title for Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2Name == undefined || this.insuredForm.value.child2Name == null || this.insuredForm.value.child2Name == '') {
                this.presentAlert("Kindly Insert Name for Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2DOB == undefined || this.insuredForm.value.child2DOB == null || this.insuredForm.value.child2DOB == '') {
                this.presentAlert("Kindly Insert DOB for Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2Weight == undefined || this.insuredForm.value.child2Weight == null || this.insuredForm.value.child2Weight == '') {
                this.presentAlert("Kindly Insert Weight for Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2Feet == undefined || this.insuredForm.value.child2Feet == null || this.insuredForm.value.child2Feet == '') {
                this.presentAlert("Kindly Insert Height in Feet for Child 2 under Insured");
                isChild2Valid = false;
            }
            else if (this.insuredForm.value.child2Inches == undefined || this.insuredForm.value.child2Inches == null || this.insuredForm.value.child2Inches == '') {
                this.presentAlert("Kindly Insert Height in Inches for Child 2 under Insured");
                isChild2Valid = false;
            }
        }
        else {
            isChild2Valid = true;
        }
        if (totalKids == 3) {
            if (this.insuredForm.value.child1Rela == undefined || this.insuredForm.value.child1Rela == null || this.insuredForm.value.child1Rela == '') {
                this.presentAlert("Kindly Select Relationship for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3Title == undefined || this.insuredForm.value.child3Title == null || this.insuredForm.value.child3Title == '') {
                this.presentAlert("Kindly Select Title for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3Name == undefined || this.insuredForm.value.child3Name == null || this.insuredForm.value.child3Name == '') {
                this.presentAlert("Kindly Insert Name for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3DOB == undefined || this.insuredForm.value.child3DOB == null || this.insuredForm.value.child3DOB == '') {
                this.presentAlert("Kindly Insert DOB for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3Weight == undefined || this.insuredForm.value.child3Weight == null || this.insuredForm.value.child3Weight == '') {
                this.presentAlert("Kindly Insert Weight for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3Feet == undefined || this.insuredForm.value.child3Feet == null || this.insuredForm.value.child3Feet == '') {
                this.presentAlert("Kindly Insert Height in Feet for Child 3 under Insured");
                isChild3Valid = false;
            }
            else if (this.insuredForm.value.child3Inches == undefined || this.insuredForm.value.child3Inches == null || this.insuredForm.value.child3Inches == '') {
                this.presentAlert("Kindly Insert Height in Inches for Child 3 under Insured");
                isChild3Valid = false;
            }
        }
        else {
            isChild3Valid = true;
        }
        if (isAdult1Valid == true && isAdult2Valid == true && isChild1Valid == true) {
            return true;
        }
        else {
            return false;
        }
    };
    HealthRenewalProposalPage.prototype.home = function (ev) {
        this.cs.goToHome();
    };
    HealthRenewalProposalPage = tslib_1.__decorate([
        Component({
            selector: 'app-health-renewal-proposal',
            templateUrl: './health-renewal-proposal.page.html',
            styleUrls: ['./health-renewal-proposal.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, AlertController, Router])
    ], HealthRenewalProposalPage);
    return HealthRenewalProposalPage;
}());
export { HealthRenewalProposalPage };
//# sourceMappingURL=health-renewal-proposal.page.js.map