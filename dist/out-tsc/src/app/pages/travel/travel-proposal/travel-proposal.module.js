import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TravelProposalPage } from './travel-proposal.page';
import { MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatInputModule } from '@angular/material';
var routes = [
    {
        path: '',
        component: TravelProposalPage
    }
];
var TravelProposalPageModule = /** @class */ (function () {
    function TravelProposalPageModule() {
    }
    TravelProposalPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatInputModule,
                MatDatepickerModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TravelProposalPage]
        })
    ], TravelProposalPageModule);
    return TravelProposalPageModule;
}());
export { TravelProposalPageModule };
//# sourceMappingURL=travel-proposal.module.js.map