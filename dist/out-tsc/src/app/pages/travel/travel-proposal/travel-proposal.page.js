import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import * as moment from 'moment';
import { ToastController, AlertController } from '@ionic/angular';
import { config } from 'src/app/config.properties';
var TravelProposalPage = /** @class */ (function () {
    function TravelProposalPage(router, alertCtrl, formBuilder, cs, toastCtrl) {
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.formBuilder = formBuilder;
        this.cs = cs;
        this.toastCtrl = toastCtrl;
        this.isShowUserList = false;
        this.gstChange = false;
        this.isBasic = false;
        this.isInsured = false;
        this.isApplicant = false;
        this.noOfMem = [];
        this.subType = 4;
        this.showPermAdd = false;
        this.showGST = false;
        this.isLoading = false;
        this.isUIN = false;
        this.isGSTIN = true;
        this.isSubmit = false;
        this.isExistance = false;
        this.inputLoading1 = false;
        this.inputLoading2 = false;
        this.memberArray = [];
        this.memNo = 0;
        this.isGSTValid = false;
        this.isCorraddSame = false;
        this.titles = [{ "id": "0", "val": "Mrs." }, { "id": "1", "val": "Mr." }, { "id": "2", "val": "Ms." }, { "id": "11", "val": "M/S" }, { "id": "12", "val": "Master" }];
        this.constituentArray = [{ "id": "0", "val": "Select Constitution of Business" }, { "id": "1", "val": "Non Resident Entity" }, { "id": "2", "val": "Foreign company registered in India" }, { "id": "3", "val": "Foreign LLP" },
            { "id": "4", "val": "Government Department" }, { "id": "5", "val": "Hindu Undivided Family" }, { "id": "6", "val": "LLP Partnership" }, { "id": "7", "val": "Local Authorities" }, { "id": "8", "val": "Partnership" },
            { "id": "9", "val": "Private Limited Company" }, { "id": "10", "val": "Proprietorship" }, { "id": "11", "val": "Others" }];
        this.customerTypeArray = [{ "id": "0", "val": "Select Customer Type" }, { "id": "21", "val": "General" }, { "id": "22", "val": "EOU/STP/EHTP" }, { "id": "23", "val": "Government" }, { "id": "24", "val": "Overseas" },
            { "id": "25", "val": "Related parties" }, { "id": "26", "val": "SEZ" }, { "id": "27", "val": "Others" }];
        this.gstStatusArray = [{ "id": "0", "val": "Select GST Status" }, { "id": "41", "val": "ARN Generated" }, { "id": "42", "val": "Provision ID Obtained" }, { "id": "43", "val": "To be commenced" },
            { "id": "44", "val": "Enrolled" }, { "id": "45", "val": "Not applicable" }];
    }
    TravelProposalPage.prototype.ngOnInit = function () {
        var date = new Date();
        console.log(date);
        date.setMonth(date.getMonth() - parseInt('216'));
        this.minDOBDate = moment(date).format('YYYY-MM-DD');
        console.log(date);
        this.createApplForm();
        this.getData();
        this.createInsuForm();
        this.getNomRelations();
        this.getApplRelations();
        this.show1('No');
    };
    TravelProposalPage.prototype.createApplForm = function () {
        this.applicantForm = new FormGroup({
            aadhaarNo: new FormControl('', [Validators.maxLength(12)]),
            panNumber: new FormControl('', [Validators.maxLength(10)]),
            applTitle: new FormControl('', [Validators.required]),
            applName: new FormControl('', [Validators.required]),
            applDOB: new FormControl('', [Validators.required]),
            applEmail: new FormControl('', [Validators.required, Validators.email]),
            mobileNo: new FormControl('', [Validators.required, Validators.pattern('[0-9]+'), Validators.maxLength(10)]),
            applAdd1: new FormControl('', [Validators.required]),
            applAdd2: new FormControl(''),
            applLandmark: new FormControl(''),
            applPinCode: new FormControl('', [Validators.required, Validators.maxLength(6)]),
            applCity: new FormControl(''),
            applState: new FormControl(''),
            permAdd1: new FormControl(''),
            permAdd2: new FormControl(''),
            permLandmark: new FormControl(''),
            permPinCode: new FormControl(''),
            permCity: new FormControl(''),
            permState: new FormControl(''),
            GSTIN: new FormControl(''),
            UIN: new FormControl(''),
            gstRegStatus: new FormControl(''),
            customerType: new FormControl(''),
            gstPanNo: new FormControl(''),
            constitution: new FormControl('')
        });
    };
    TravelProposalPage.prototype.createInsuForm = function () {
        this.insuredForm = this.formBuilder.group({
            insu1Title: [null], insu1Name: [null], insu1Rela: [null], insu1DOB: [null], insu1PassNo: [null], insu1Ailment: [null], insu1NomName: [null], insu1NomRela: [null],
            insu2Title: [null], insu2Name: [null], insu2Rela: [null], insu2DOB: [null], insu2PassNo: [null], insu2Ailment: [null], insu2NomName: [null], insu2NomRela: [null],
            insu3Title: [null], insu3Name: [null], insu3Rela: [null], insu3DOB: [null], insu3PassNo: [null], insu3Ailment: [null], insu3NomName: [null], insu3NomRela: [null],
            insu4Title: [null], insu4Name: [null], insu4Rela: [null], insu4DOB: [null], insu4PassNo: [null], insu4Ailment: [null], insu4NomName: [null], insu4NomRela: [null],
            insu5Title: [null], insu5Name: [null], insu5Rela: [null], insu5DOB: [null], insu5PassNo: [null], insu5Ailment: [null], insu5NomName: [null], insu5NomRela: [null],
            insu6Title: [null], insu6Name: [null], insu6Rela: [null], insu6DOB: [null], insu6PassNo: [null], insu6Ailment: [null], insu6NomName: [null], insu6NomRela: [null]
        });
    };
    TravelProposalPage.prototype.selectMember = function (index) {
        console.log(index);
        this.index = index + 1;
        if (this.index == 1) {
            this.insuredForm.patchValue({ 'insu2Title': '' });
            this.insuredForm.patchValue({ 'insu2Name': '' });
            this.insuredForm.patchValue({ 'insu2Rela': '' });
            this.insuredForm.patchValue({ 'insu2DOB': '' });
            this.insuredForm.patchValue({ 'insu2PassNo': '' });
            this.insuredForm.patchValue({ 'insu2Ailment': '' });
            this.insuredForm.patchValue({ 'insu2NomName': '' });
            this.insuredForm.patchValue({ 'insu2NomRela': '' });
            this.insuredForm.patchValue({ 'insu3Title': '' });
            this.insuredForm.patchValue({ 'insu3Name': '' });
            this.insuredForm.patchValue({ 'insu3Rela': '' });
            this.insuredForm.patchValue({ 'insu3DOB': '' });
            this.insuredForm.patchValue({ 'insu3PassNo': '' });
            this.insuredForm.patchValue({ 'insu3Ailment': '' });
            this.insuredForm.patchValue({ 'insu3NomName': '' });
            this.insuredForm.patchValue({ 'insu3NomRela': '' });
            this.insuredForm.patchValue({ 'insu4Title': '' });
            this.insuredForm.patchValue({ 'insu4Name': '' });
            this.insuredForm.patchValue({ 'insu4Rela': '' });
            this.insuredForm.patchValue({ 'insu4DOB': '' });
            this.insuredForm.patchValue({ 'insu4PassNo': '' });
            this.insuredForm.patchValue({ 'insu4Ailment': '' });
            this.insuredForm.patchValue({ 'insu4NomName': '' });
            this.insuredForm.patchValue({ 'insu4NomRela': '' });
            this.insuredForm.patchValue({ 'insu5Title': '' });
            this.insuredForm.patchValue({ 'insu5Name': '' });
            this.insuredForm.patchValue({ 'insu5Rela': '' });
            this.insuredForm.patchValue({ 'insu5DOB': '' });
            this.insuredForm.patchValue({ 'insu5PassNo': '' });
            this.insuredForm.patchValue({ 'insu5Ailment': '' });
            this.insuredForm.patchValue({ 'insu5NomName': '' });
            this.insuredForm.patchValue({ 'insu5NomRela': '' });
            this.insuredForm.patchValue({ 'insu6Title': '' });
            this.insuredForm.patchValue({ 'insu6Name': '' });
            this.insuredForm.patchValue({ 'insu6Rela': '' });
            this.insuredForm.patchValue({ 'insu6DOB': '' });
            this.insuredForm.patchValue({ 'insu6PassNo': '' });
            this.insuredForm.patchValue({ 'insu6Ailment': '' });
            this.insuredForm.patchValue({ 'insu6NomName': '' });
            this.insuredForm.patchValue({ 'insu6NomRela': '' });
        }
        else if (this.index == 2) {
            this.insuredForm.patchValue({ 'insu3Title': '' });
            this.insuredForm.patchValue({ 'insu3Name': '' });
            this.insuredForm.patchValue({ 'insu3Rela': '' });
            this.insuredForm.patchValue({ 'insu3DOB': '' });
            this.insuredForm.patchValue({ 'insu3PassNo': '' });
            this.insuredForm.patchValue({ 'insu3Ailment': '' });
            this.insuredForm.patchValue({ 'insu3NomName': '' });
            this.insuredForm.patchValue({ 'insu3NomRela': '' });
            this.insuredForm.patchValue({ 'insu4Title': '' });
            this.insuredForm.patchValue({ 'insu4Name': '' });
            this.insuredForm.patchValue({ 'insu4Rela': '' });
            this.insuredForm.patchValue({ 'insu4DOB': '' });
            this.insuredForm.patchValue({ 'insu4PassNo': '' });
            this.insuredForm.patchValue({ 'insu4Ailment': '' });
            this.insuredForm.patchValue({ 'insu4NomName': '' });
            this.insuredForm.patchValue({ 'insu4NomRela': '' });
            this.insuredForm.patchValue({ 'insu5Title': '' });
            this.insuredForm.patchValue({ 'insu5Name': '' });
            this.insuredForm.patchValue({ 'insu5Rela': '' });
            this.insuredForm.patchValue({ 'insu5DOB': '' });
            this.insuredForm.patchValue({ 'insu5PassNo': '' });
            this.insuredForm.patchValue({ 'insu5Ailment': '' });
            this.insuredForm.patchValue({ 'insu5NomName': '' });
            this.insuredForm.patchValue({ 'insu5NomRela': '' });
            this.insuredForm.patchValue({ 'insu6Title': '' });
            this.insuredForm.patchValue({ 'insu6Name': '' });
            this.insuredForm.patchValue({ 'insu6Rela': '' });
            this.insuredForm.patchValue({ 'insu6DOB': '' });
            this.insuredForm.patchValue({ 'insu6PassNo': '' });
            this.insuredForm.patchValue({ 'insu6Ailment': '' });
            this.insuredForm.patchValue({ 'insu6NomName': '' });
            this.insuredForm.patchValue({ 'insu6NomRela': '' });
        }
        else if (this.index == 3) {
            this.insuredForm.patchValue({ 'insu4Title': '' });
            this.insuredForm.patchValue({ 'insu4Name': '' });
            this.insuredForm.patchValue({ 'insu4Rela': '' });
            this.insuredForm.patchValue({ 'insu4DOB': '' });
            this.insuredForm.patchValue({ 'insu4PassNo': '' });
            this.insuredForm.patchValue({ 'insu4Ailment': '' });
            this.insuredForm.patchValue({ 'insu4NomName': '' });
            this.insuredForm.patchValue({ 'insu4NomRela': '' });
            this.insuredForm.patchValue({ 'insu5Title': '' });
            this.insuredForm.patchValue({ 'insu5Name': '' });
            this.insuredForm.patchValue({ 'insu5Rela': '' });
            this.insuredForm.patchValue({ 'insu5DOB': '' });
            this.insuredForm.patchValue({ 'insu5PassNo': '' });
            this.insuredForm.patchValue({ 'insu5Ailment': '' });
            this.insuredForm.patchValue({ 'insu5NomName': '' });
            this.insuredForm.patchValue({ 'insu5NomRela': '' });
            this.insuredForm.patchValue({ 'insu6Title': '' });
            this.insuredForm.patchValue({ 'insu6Name': '' });
            this.insuredForm.patchValue({ 'insu6Rela': '' });
            this.insuredForm.patchValue({ 'insu6DOB': '' });
            this.insuredForm.patchValue({ 'insu6PassNo': '' });
            this.insuredForm.patchValue({ 'insu6Ailment': '' });
            this.insuredForm.patchValue({ 'insu6NomName': '' });
            this.insuredForm.patchValue({ 'insu6NomRela': '' });
        }
        else if (this.index == 4) {
            this.insuredForm.patchValue({ 'insu5Title': '' });
            this.insuredForm.patchValue({ 'insu5Name': '' });
            this.insuredForm.patchValue({ 'insu5Rela': '' });
            this.insuredForm.patchValue({ 'insu5DOB': '' });
            this.insuredForm.patchValue({ 'insu5PassNo': '' });
            this.insuredForm.patchValue({ 'insu5Ailment': '' });
            this.insuredForm.patchValue({ 'insu5NomName': '' });
            this.insuredForm.patchValue({ 'insu5NomRela': '' });
            this.insuredForm.patchValue({ 'insu6Title': '' });
            this.insuredForm.patchValue({ 'insu6Name': '' });
            this.insuredForm.patchValue({ 'insu6Rela': '' });
            this.insuredForm.patchValue({ 'insu6DOB': '' });
            this.insuredForm.patchValue({ 'insu6PassNo': '' });
            this.insuredForm.patchValue({ 'insu6Ailment': '' });
            this.insuredForm.patchValue({ 'insu6NomName': '' });
            this.insuredForm.patchValue({ 'insu6NomRela': '' });
        }
        else if (this.index == 5) {
            this.insuredForm.patchValue({ 'insu6Title': '' });
            this.insuredForm.patchValue({ 'insu6Name': '' });
            this.insuredForm.patchValue({ 'insu6Rela': '' });
            this.insuredForm.patchValue({ 'insu6DOB': '' });
            this.insuredForm.patchValue({ 'insu6PassNo': '' });
            this.insuredForm.patchValue({ 'insu6Ailment': '' });
            this.insuredForm.patchValue({ 'insu6NomName': '' });
            this.insuredForm.patchValue({ 'insu6NomRela': '' });
        }
        else {
            console.log("All Members are OK");
        }
    };
    TravelProposalPage.prototype.getPincodeDetails = function (ev) {
        var _this = this;
        console.log(ev);
        if (ev.target.selectionStart == 6) {
            this.inputLoading1 = true;
            this.getCityState(ev).then(function () {
                _this.getPinCodeData(_this.applicantForm);
            });
        }
        else {
            console.log("Invalid Pin");
            this.applicantForm.patchValue({ 'applCity': '' });
            this.applicantForm.patchValue({ 'applState': '' });
            this.inputLoading1 = false;
        }
    };
    TravelProposalPage.prototype.getCityState = function (ev) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log(_this.isExistance);
            if (!_this.isExistance) {
                if (ev.target.value.length == 6) {
                    var body = ev.target.value;
                    _this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then(function (res) {
                        _this.pinData = res;
                        _this.applicantForm.patchValue({ 'applCity': _this.pinData.CityList[0].CityName });
                        _this.applicantForm.patchValue({ 'applState': _this.pinData.StateName });
                        _this.inputLoading1 = false;
                        resolve();
                    });
                }
                else {
                    _this.inputLoading1 = false;
                    console.log("Invalid Pin");
                    _this.applicantForm.patchValue({ 'applCity': '' });
                    _this.applicantForm.patchValue({ 'applState': '' });
                    resolve();
                }
            }
            else {
                var body = ev;
                _this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then(function (res) {
                    _this.pinData = res;
                    console.log(_this.pinData);
                    _this.applicantForm.patchValue({ 'applCity': _this.pinData.CityList[0].CityName });
                    _this.applicantForm.patchValue({ 'applState': _this.pinData.StateName });
                    resolve();
                });
            }
        });
    };
    TravelProposalPage.prototype.getPinCodeData = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            var body = { "Pincode": form.value.applPinCode, "CityID": _this.pinData.CityList[0].CityID };
            _this.cs.postWithParams('/api/RTOList/GetPincodeID', body).then(function (res) {
                console.log("Get Pin Data Value", res);
                _this.pinDataValue = res;
                resolve();
            });
        });
    };
    TravelProposalPage.prototype.getPincodeDetailsPer = function (ev) {
        var _this = this;
        if (ev.target.selectionStart == 6) {
            this.inputLoading1 = true;
            this.getCityStatePer(ev).then(function () {
                _this.getPinCodeDataPerm(_this.applicantForm);
            });
        }
        else {
            console.log("Invalid Pin");
            this.applicantForm.patchValue({ 'permCity': '' });
            this.applicantForm.patchValue({ 'permState': '' });
            this.inputLoading1 = false;
        }
    };
    TravelProposalPage.prototype.getCityStatePer = function (ev) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log(_this.isExistance);
            if (!_this.isExistance) {
                if (ev.target.value.length == 6) {
                    var body = ev.target.value;
                    _this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then(function (res) {
                        _this.pinDataPer = res;
                        console.log(_this.pinDataPer);
                        _this.applicantForm.patchValue({ 'permCity': _this.pinDataPer.CityList[0].CityName });
                        _this.applicantForm.patchValue({ 'permState': _this.pinDataPer.StateName });
                        resolve();
                    });
                }
                else {
                    console.log("Invalid Pin");
                    resolve();
                }
            }
            else {
                var body = ev;
                _this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then(function (res) {
                    _this.pinDataPer = res;
                    console.log(_this.pinDataPer);
                    _this.applicantForm.patchValue({ 'permCity': _this.pinDataPer.CityList[0].CityName });
                    _this.applicantForm.patchValue({ 'permState': _this.pinDataPer.StateName });
                    resolve();
                });
            }
        });
    };
    TravelProposalPage.prototype.getPinCodeDataPerm = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            var body = { "Pincode": form.value.permPinCode, "CityID": _this.pinDataPer.CityList[0].CityID };
            _this.cs.postWithParams('/api/RTOList/GetPincodeID', body).then(function (res) {
                console.log("Get Perm Pin Data ", res);
                _this.pinDataPermValue = res;
                resolve();
            });
        });
    };
    TravelProposalPage.prototype.getNomRelations = function () {
        var _this = this;
        this.cs.postWithParams('/api/Travel/GetNomineeRelationList', '').then(function (res) {
            console.log("Relation Nominee", res.lstNomineeRelationshipResponse);
            _this.nomRelations = res;
        });
    };
    TravelProposalPage.prototype.getApplRelations = function () {
        var _this = this;
        var body = { "CoverageType": this.request.coverageType, "SubType": this.subType };
        var str = JSON.stringify(body);
        this.cs.postWithParams('/api/Travel/GetRelationWithApplicant', str).then(function (res) {
            console.log("Relation Appl", res.lstApplicantRelationShipResponse);
            _this.applRelations = res;
        });
    };
    TravelProposalPage.prototype.getNomRel1 = function (ev) {
        console.log(ev.target.value);
        this.nomRela1 = this.nomRelations.lstNomineeRelationshipResponse.find(function (x) { return x.NomineeName == ev.target.value; });
    };
    TravelProposalPage.prototype.getNomRel2 = function (ev) {
        console.log(ev.target.value);
        this.nomRela2 = this.nomRelations.lstNomineeRelationshipResponse.find(function (x) { return x.NomineeName == ev.target.value; });
    };
    TravelProposalPage.prototype.getNomRel3 = function (ev) {
        console.log(ev.target.value);
        this.nomRela3 = this.nomRelations.lstNomineeRelationshipResponse.find(function (x) { return x.NomineeName == ev.target.value; });
    };
    TravelProposalPage.prototype.getNomRel4 = function (ev) {
        console.log(ev.target.value);
        this.nomRela4 = this.nomRelations.lstNomineeRelationshipResponse.find(function (x) { return x.NomineeName == ev.target.value; });
    };
    TravelProposalPage.prototype.getNomRel5 = function (ev) {
        console.log(ev.target.value);
        this.nomRela5 = this.nomRelations.lstNomineeRelationshipResponse.find(function (x) { return x.NomineeName == ev.target.value; });
    };
    TravelProposalPage.prototype.getNomRel6 = function (ev) {
        console.log(ev.target.value);
        this.nomRela6 = this.nomRelations.lstNomineeRelationshipResponse.find(function (x) { return x.NomineeName == ev.target.value; });
    };
    TravelProposalPage.prototype.getApplRel1 = function (ev) {
        console.log(ev.target.value);
        this.applRela1 = this.applRelations.lstApplicantRelationShipResponse.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    TravelProposalPage.prototype.getApplRel2 = function (ev) {
        console.log(ev.target.value);
        this.applRela2 = this.applRelations.lstApplicantRelationShipResponse.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    TravelProposalPage.prototype.getApplRel3 = function (ev) {
        console.log(ev.target.value);
        this.applRela3 = this.applRelations.lstApplicantRelationShipResponse.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    TravelProposalPage.prototype.getApplRel4 = function (ev) {
        console.log(ev.target.value);
        this.applRela4 = this.applRelations.lstApplicantRelationShipResponse.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    TravelProposalPage.prototype.getApplRel5 = function (ev) {
        console.log(ev.target.value);
        this.applRela5 = this.applRelations.lstApplicantRelationShipResponse.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    TravelProposalPage.prototype.getApplRel6 = function (ev) {
        console.log(ev.target.value);
        this.applRela6 = this.applRelations.lstApplicantRelationShipResponse.find(function (x) { return x.RelationshipName == ev.target.value; });
    };
    TravelProposalPage.prototype.getTitle = function (ev) {
        console.log("Title on search");
        this.titleDetails = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    TravelProposalPage.prototype.getInsuTitle1 = function (ev) {
        this.insuTitleDetails1 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    TravelProposalPage.prototype.getInsuTitle2 = function (ev) {
        this.insuTitleDetails2 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    TravelProposalPage.prototype.getInsuTitle3 = function (ev) {
        this.insuTitleDetails3 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    TravelProposalPage.prototype.getInsuTitle4 = function (ev) {
        this.insuTitleDetails4 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    TravelProposalPage.prototype.getInsuTitle5 = function (ev) {
        this.insuTitleDetails5 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    TravelProposalPage.prototype.getInsuTitle6 = function (ev) {
        this.insuTitleDetails6 = this.titles.find(function (x) { return x.val == ev.target.value; });
    };
    TravelProposalPage.prototype.isGST = function (msg) {
        console.log(msg);
        if (msg == 'Yes') {
            this.showGST = true;
            this.applicantForm.get('constitution').setValidators([Validators.required]);
            this.applicantForm.get('gstPanNo').setValidators([Validators.required]);
            this.applicantForm.get('customerType').setValidators([Validators.required]);
            this.applicantForm.get('gstRegStatus').setValidators([Validators.required]);
            this.applicantForm.get('constitution').updateValueAndValidity();
            this.applicantForm.get('gstPanNo').updateValueAndValidity();
            this.applicantForm.get('customerType').updateValueAndValidity();
            this.applicantForm.get('gstRegStatus').updateValueAndValidity();
        }
        else {
            this.showGST = false;
            this.isUIN = false;
            this.isGSTIN = false;
            this.applicantForm.patchValue({ 'GSTIN': "" });
            this.applicantForm.patchValue({ 'UIN': "" });
            this.applicantForm.patchValue({ 'gstRegStatus': "" });
            this.applicantForm.patchValue({ 'customerType': "" });
            this.applicantForm.patchValue({ 'gstPanNo': "" });
            this.applicantForm.patchValue({ 'constitution': "" });
            this.applicantForm.get('constitution').clearValidators();
            this.applicantForm.get('gstPanNo').clearValidators();
            this.applicantForm.get('customerType').clearValidators();
            this.applicantForm.get('gstRegStatus').clearValidators();
        }
    };
    TravelProposalPage.prototype.isGSTINUIN = function (msg) {
        if (msg == 'GSTIN' && this.showGST) {
            this.isGSTIN = true;
            this.isUIN = false;
            this.applicantForm.get('GSTIN').setValidators([Validators.required]);
            this.applicantForm.get('GSTIN').updateValueAndValidity();
            this.applicantForm.patchValue({ 'UIN': "" });
            this.applicantForm.get('UIN').clearValidators();
        }
        else if (msg == 'UIN' && this.showGST) {
            this.isUIN = true;
            this.isGSTIN = false;
            this.applicantForm.get('UIN').setValidators([Validators.required]);
            this.applicantForm.get('UIN').updateValueAndValidity();
            this.applicantForm.patchValue({ 'GSTIN': "" });
            this.applicantForm.get('GSTIN').clearValidators();
        }
        else {
            this.applicantForm.get('GSTIN').clearValidators();
            this.applicantForm.get('UIN').clearValidators();
        }
    };
    TravelProposalPage.prototype.show1 = function (msg) {
        console.log(msg);
        if (msg == 'No') {
            this.showPermAdd = true;
            this.isCorraddSame = false;
            this.applicantForm.patchValue({ 'permAdd1': '' });
            this.applicantForm.patchValue({ 'permAdd2': '' });
            this.applicantForm.patchValue({ 'permLandmark': '' });
            this.applicantForm.patchValue({ 'permPinCode': '' });
            this.applicantForm.patchValue({ 'permCity': '' });
            this.applicantForm.patchValue({ 'permState': '' });
            this.pinDataPermValue = '';
            this.pinDataPer = '';
        }
        else {
            this.showPermAdd = false;
            this.isCorraddSame = true;
            this.applicantForm.patchValue({ 'permAdd1': this.applicantForm.value.applAdd1 });
            this.applicantForm.patchValue({ 'permAdd2': this.applicantForm.value.applAdd2 });
            this.applicantForm.patchValue({ 'permLandmark': this.applicantForm.value.applLandmark });
            this.applicantForm.patchValue({ 'permPinCode': this.applicantForm.value.applPinCode });
            this.applicantForm.patchValue({ 'permCity': this.applicantForm.value.applCity });
            this.applicantForm.patchValue({ 'permState': this.applicantForm.value.applState });
            this.pinDataPermValue = this.pinDataValue;
            this.pinDataPer = this.pinData;
        }
    };
    TravelProposalPage.prototype.getGSTName = function (ev) {
        console.log(ev, this.isExistance);
        if (!this.isExistance) {
            var gstNameData = this.gstStatusArray.find(function (x) { return x.id == ev.target.value; });
            this.gstName = gstNameData.val;
        }
        else {
            var gstNameData = this.gstStatusArray.find(function (x) { return x.id == ev; });
            this.gstName = gstNameData.val;
        }
    };
    TravelProposalPage.prototype.getCustTypeName = function (ev) {
        console.log(this.isExistance, ev);
        if (!this.isExistance) {
            var custTypeData = this.customerTypeArray.find(function (x) { return x.id == ev.target.value; });
            this.customerType = custTypeData.val;
        }
        else {
            var custTypeData = this.customerTypeArray.find(function (x) { return x.id == ev; });
            this.customerType = custTypeData.val;
        }
    };
    TravelProposalPage.prototype.getConstitute = function (ev) {
        if (!this.isExistance) {
            var constData = this.constituentArray.find(function (x) { return x.id == ev.target.value; });
            this.constitutionName = constData.val;
        }
        else {
            var constData = this.constituentArray.find(function (x) { return x.id == ev; });
            this.constitutionName = constData.val;
        }
    };
    TravelProposalPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.request = JSON.parse(localStorage.getItem('quoteRequest'));
            _this.response = JSON.parse(localStorage.getItem('quoteResponse'));
            _this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
            console.log(_this.request);
            if (_this.request.NoOfTravelers == 2) {
                _this.noOfMem.push(1);
            }
            else {
                _this.noOfMem = [];
                for (var i = 1; i <= _this.request.NoOfTravelers; i++) {
                    _this.noOfMem.push(i);
                }
                console.log(_this.noOfMem);
            }
            if (_this.request.IsAnnualMultiTrip == 'Yes') {
                _this.subType = 5;
            }
            else {
                _this.subType = 4;
            }
            resolve();
        });
    };
    TravelProposalPage.prototype.patchDOB = function () {
        if (this.request.NoOfTravelers == 1) {
            this.insuredForm.patchValue({ 'insu1DOB': moment(this.request.Members[0].DateOfBirth).format('YYYY-MM-DD') });
        }
        else if (this.request.NoOfTravelers == 2) {
            this.insuredForm.patchValue({ 'insu1DOB': moment(this.request.Members[0].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu2DOB': moment(this.request.Members[1].DateOfBirth).format('YYYY-MM-DD') });
        }
        else if (this.request.NoOfTravelers == 3) {
            this.insuredForm.patchValue({ 'insu1DOB': moment(this.request.Members[0].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu2DOB': moment(this.request.Members[1].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu3DOB': moment(this.request.Members[2].DateOfBirth).format('YYYY-MM-DD') });
        }
        else if (this.request.NoOfTravelers == 4) {
            this.insuredForm.patchValue({ 'insu1DOB': moment(this.request.Members[0].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu2DOB': moment(this.request.Members[1].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu3DOB': moment(this.request.Members[2].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu4DOB': moment(this.request.Members[3].DateOfBirth).format('YYYY-MM-DD') });
        }
        else if (this.request.NoOfTravelers == 5) {
            this.insuredForm.patchValue({ 'insu1DOB': moment(this.request.Members[0].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu2DOB': moment(this.request.Members[1].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu3DOB': moment(this.request.Members[2].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu4DOB': moment(this.request.Members[3].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu5DOB': moment(this.request.Members[4].DateOfBirth).format('YYYY-MM-DD') });
        }
        else {
            this.insuredForm.patchValue({ 'insu1DOB': moment(this.request.Members[0].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu2DOB': moment(this.request.Members[1].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu3DOB': moment(this.request.Members[2].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu4DOB': moment(this.request.Members[3].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu5DOB': moment(this.request.Members[4].DateOfBirth).format('YYYY-MM-DD') });
            this.insuredForm.patchValue({ 'insu6DOB': moment(this.request.Members[5].DateOfBirth).format('YYYY-MM-DD') });
        }
    };
    TravelProposalPage.prototype.showBasic = function (ev) {
        this.isBasic = !this.isBasic;
        if (this.isBasic) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("basic").style.background = "url(" + imgUrl + ")";
            document.getElementById("basic").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("basic").style.background = "url(" + imgUrl + ")";
            document.getElementById("basic").style.backgroundRepeat = 'no-repeat';
        }
    };
    TravelProposalPage.prototype.showInsured = function (ev) {
        this.isInsured = !this.isInsured;
        this.patchDOB();
        if (this.isInsured) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("insured").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured").style.backgroundRepeat = 'no-repeat';
            this.selectMember(0);
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("insured").style.background = "url(" + imgUrl + ")";
            document.getElementById("insured").style.backgroundRepeat = 'no-repeat';
        }
    };
    TravelProposalPage.prototype.showApplicant = function (ev) {
        this.isApplicant = !this.isApplicant;
        if (this.isApplicant) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("applicant").style.background = "url(" + imgUrl + ")";
            document.getElementById("applicant").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("applicant").style.background = "url(" + imgUrl + ")";
            document.getElementById("applicant").style.backgroundRepeat = 'no-repeat';
        }
        if (this.request.GSTStateName == 'KERALA') {
            if (this.request.isGSTRegistered) {
                this.gstChange = true;
                this.showGST = true;
                this.applicantForm.get('constitution').setValidators([Validators.required]);
                this.applicantForm.get('gstPanNo').setValidators([Validators.required]);
                this.applicantForm.get('customerType').setValidators([Validators.required]);
                this.applicantForm.get('gstRegStatus').setValidators([Validators.required]);
                this.applicantForm.get('constitution').updateValueAndValidity();
                this.applicantForm.get('gstPanNo').updateValueAndValidity();
                this.applicantForm.get('customerType').updateValueAndValidity();
                this.applicantForm.get('gstRegStatus').updateValueAndValidity();
            }
            else {
                this.gstChange = false;
            }
        }
        else {
            this.gstChange = false;
        }
    };
    TravelProposalPage.prototype.isExist = function (ev) {
        console.log(ev);
        if (ev == 'No') {
            this.isExistance = false;
            document.getElementById("myNav1").style.height = "0%";
        }
        else {
            this.isExistance = true;
            document.getElementById("myNav1").style.height = "100%";
        }
    };
    TravelProposalPage.prototype.searchCustomer = function (ev) {
        var _this = this;
        var request = {
            "CustomerName": this.customerName,
            "CustomerEmail": "",
            "CustomerMobile": "",
            "CustomerStateCode": ""
        };
        var str = JSON.stringify(request);
        console.log(str);
        this.cs.showLoader = true;
        this.cs.postWithParams('/api/Customer/SearchCustomer', str).then(function (res) {
            console.log("search user", res);
            _this.searchData = res;
            if (_this.searchData.length > 0) {
                _this.isShowUserList = true;
                _this.cs.showLoader = false;
            }
            else {
                _this.isShowUserList = false;
                _this.cs.showLoader = false;
            }
        });
    };
    TravelProposalPage.prototype.selectedCust = function (cust) {
        console.log("Selected Data", cust);
        this.getCustDetails(cust);
        this.isExistance = true;
        document.getElementById("myNav1").style.height = "0%";
    };
    TravelProposalPage.prototype.getCustDetails = function (data) {
        var _this = this;
        console.log(data);
        this.cs.showLoader = true;
        this.cs.getWithParams('/api/Customer/GetCustomerDetails/' + data.CustomerID).then(function (res) {
            console.log(res);
            if (res != undefined || res != null) {
                _this.selecteUserData = res;
                _this.titleDetails = _this.titles.find(function (x) { return x.id == _this.selecteUserData.TitleValue; });
                console.log(_this.titleDetails);
                _this.applicantForm.patchValue({ 'applTitle': _this.titleDetails.val });
                _this.applicantForm.patchValue({ 'applName': _this.selecteUserData.Name });
                _this.applicantForm.patchValue({ 'applDOB': moment(_this.selecteUserData.DateOfBirth).format('YYYY-MM-DD') });
                _this.applicantForm.patchValue({ 'applEmail': _this.selecteUserData.EmailAddress });
                _this.applicantForm.patchValue({ 'mobileNo': _this.selecteUserData.MobileNumber });
                _this.applicantForm.patchValue({ 'applAdd1': _this.selecteUserData.PresentAddrLine1 });
                _this.applicantForm.patchValue({ 'applAdd2': _this.selecteUserData.PresentAddrLine2 });
                _this.applicantForm.patchValue({ 'applLandmark': _this.selecteUserData.PresentAddrLandmark });
                _this.applicantForm.patchValue({ 'applPinCode': _this.selecteUserData.PresentAddrPincodeText });
                _this.applicantForm.patchValue({ 'applCity': _this.selecteUserData.PresentAddrCityText });
                _this.applicantForm.patchValue({ 'applState': _this.selecteUserData.PresentAddrStateText });
                _this.applicantForm.patchValue({ 'permAdd1': _this.selecteUserData.PresentAddrLine1 });
                _this.applicantForm.patchValue({ 'permAdd2': _this.selecteUserData.PresentAddrLine2 });
                _this.applicantForm.patchValue({ 'permLandmark': _this.selecteUserData.PresentAddrLandmark });
                _this.applicantForm.patchValue({ 'permPinCode': _this.selecteUserData.PresentAddrPincodeText });
                _this.applicantForm.patchValue({ 'permCity': _this.selecteUserData.PresentAddrCityText });
                _this.applicantForm.patchValue({ 'permState': _this.selecteUserData.PresentAddrStateText });
                if (_this.selecteUserData.GSTDetails == null || _this.selecteUserData.GSTDetails == undefined) {
                    _this.applicantForm.patchValue({ 'GSTIN': '' });
                    _this.applicantForm.patchValue({ 'UIN': '' });
                    _this.applicantForm.patchValue({ 'gstRegStatus': '' });
                    _this.applicantForm.patchValue({ 'customerType': '' });
                    _this.applicantForm.patchValue({ 'gstPanNo': '' });
                    _this.applicantForm.patchValue({ 'constitution': '' });
                }
                else {
                    _this.applicantForm.patchValue({ 'GSTIN': _this.selecteUserData.GSTDetails.GSTIN_NO });
                    _this.applicantForm.patchValue({ 'UIN': _this.selecteUserData.GSTDetails.UIN_NO });
                    _this.applicantForm.patchValue({ 'gstRegStatus': _this.selecteUserData.GSTDetails.GST_REGISTRATION_STATUS });
                    _this.applicantForm.patchValue({ 'customerType': _this.selecteUserData.GSTDetails.CUSTOMER_TYPE });
                    _this.applicantForm.patchValue({ 'gstPanNo': _this.selecteUserData.GSTDetails.PAN_NO });
                    _this.applicantForm.patchValue({ 'constitution': _this.selecteUserData.GSTDetails.CONSTITUTION_OF_BUSINESS });
                }
                _this.getPincodeDetails(_this.selecteUserData.PresentAddrPincodeText);
                _this.getPincodeDetailsPer(_this.selecteUserData.PresentAddrPincodeText);
                _this.getGSTName(_this.selecteUserData.GSTDetails.GST_REGISTRATION_STATUS);
                _this.getCustTypeName(_this.selecteUserData.GSTDetails.CUSTOMER_TYPE);
                _this.getConstitute(_this.selecteUserData.GSTDetails.CONSTITUTION_OF_BUSINESS);
                _this.showPermAdd = false;
                _this.isGSTIN = _this.selecteUserData.isGSTINApplicable;
                _this.showGST = true;
                _this.validateForExistance();
                _this.cs.showLoader = false;
            }
            else {
                console.log("Invalid Data");
                _this.cs.showLoader = false;
            }
        });
    };
    TravelProposalPage.prototype.closeNav = function (ev) {
        document.getElementById("myNav1").style.height = "0%";
    };
    TravelProposalPage.prototype.validatePassport = function (ev) {
        var _this = this;
        console.log("Event", ev.target.value);
        var body = {
            "PassportFilter": {
                "Passport1": ev.target.value
            }
        };
        var str = JSON.stringify(body);
        this.cs.postWithParams('/api/travel/FilterPassport', str).then(function (res) {
            console.log("Passport", res);
            if (res.StatusCode == '0') {
                _this.showToast('Invalid Passport Number');
            }
        });
    };
    TravelProposalPage.prototype.showToast = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    TravelProposalPage.prototype.calProposal = function (applform, insuForm) {
        var _this = this;
        console.log("Application Form", applform);
        console.log("Insured Form", insuForm);
        var str = JSON.stringify(applform.value);
        console.log(str);
        this.isSubmit = true;
        this.validateMember().then(function () {
            if (_this.insuredForm.status == 'VALID' && _this.applicantForm.status == 'VALID') {
                _this.validateForExistance().then(function () {
                    console.log(_this.isGSTValid);
                    if (_this.isGSTValid) {
                        _this.createMemberObj(insuForm).then(function () {
                            console.log(_this.memberObj);
                            _this.createCustomer(applform).then(function () {
                                _this.travelProposal(applform, insuForm).then(function () {
                                });
                            });
                        });
                    }
                    else {
                        _this.showToast('Invalid GST details');
                    }
                    _this.validateForm();
                });
            }
            else {
                _this.showToast('Kindly fill Insured Member Data Properly');
            }
        });
    };
    TravelProposalPage.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TravelProposalPage.prototype.validateMobileLength = function (e) {
        if (e.target.value.length > 10) {
            this.presentAlert('Mobile No cannot more than 10 digits');
            this.applicantForm.patchValue({ 'mobileNo': null });
        }
    };
    TravelProposalPage.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    TravelProposalPage.prototype.validateMobile = function (mobile) {
        var mob = /^[0-9]{10}$/;
        return mob.test(mobile);
    };
    TravelProposalPage.prototype.validateAdhaarNo = function (addharno) {
        var re = /^[0-9]{12}$/;
        return re.test(addharno);
    };
    TravelProposalPage.prototype.validatePanNo = function (panno) {
        var re = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
        return re.test(panno);
    };
    TravelProposalPage.prototype.validateGST = function (gst) {
        var re = /^[0-9]{2}[A-Z]{3}[PCHABGJLEFT][A-Z][0-9]{4}[A-Z]{1}[0-9A-Z]{1}[Z]{1}[0-9A-Z]{1}$/;
        var gstvalid = re.test(gst);
        if (gstvalid) {
            var gstcodeObtained = gst.substring(0, 2);
            var stateData = JSON.parse(localStorage.getItem('selectedStateData'));
            var gstcode = stateData.GSTStateCode;
            if (gstcodeObtained == gstcode) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return gstvalid;
        }
    };
    TravelProposalPage.prototype.validateForm = function () {
        if (this.applicantForm.value.aadhaarNo != undefined && this.applicantForm.value.aadhaarNo != null && this.applicantForm.value.aadhaarNo != '' && !this.validateAdhaarNo(this.applicantForm.value.aadhaarNo)) {
            this.presentAlert('Please enter valid Aadhar number');
            return false;
        }
        else if (!this.validateMobile(this.applicantForm.value.mobileNo)) {
            this.presentAlert('Mobile Number should be 10 digit');
            return false;
        }
        else if (!this.validateEmail(this.applicantForm.value.applEmail)) {
            this.presentAlert('Kindly Insert valid Applicant Email');
            return false;
        }
        else if (this.applicantForm.value.panNumber != undefined && this.applicantForm.value.panNumber != null && this.applicantForm.value.panNumber != '' && !this.validatePanNo(this.applicantForm.value.panNumber)) {
            this.presentAlert('Kindly Enter Valid Applicant Pan Card Number');
            return false;
        }
        else {
            return true;
        }
    };
    // need to work
    TravelProposalPage.prototype.validateForExistance = function () {
        var _this = this;
        return new Promise(function (resolve) {
            console.log(_this.applicantForm.value.constitution, _this.showGST);
            if (_this.showGST) {
                if (_this.applicantForm.status == "VALID") {
                    if (_this.applicantForm.value.constitution == undefined || _this.applicantForm.value.constitution == null) {
                        _this.isGSTValid = false;
                        _this.isExistance = false;
                        resolve();
                    }
                    else {
                        if (_this.applicantForm.value.gstPanNo == undefined || _this.applicantForm.value.gstPanNo == '') {
                            _this.isGSTValid = false;
                            _this.isExistance = false;
                            resolve();
                        }
                        else {
                            if (_this.applicantForm.value.customerType == undefined || _this.applicantForm.value.customerType == '' || _this.applicantForm.value.customerType == 0) {
                                _this.isGSTValid = false;
                                _this.isExistance = false;
                                resolve();
                            }
                            else {
                                if (_this.applicantForm.value.gstRegStatus == undefined || _this.applicantForm.value.gstRegStatus == '' || _this.applicantForm.value.gstRegStatus == 0) {
                                    _this.isGSTValid = false;
                                    _this.isExistance = false;
                                    resolve();
                                }
                                else {
                                    _this.isGSTValid = true;
                                    _this.isExistance = true;
                                    resolve();
                                }
                            }
                        }
                    }
                }
                else {
                    console.log("Form invald");
                    _this.showToast('Kindly fill proper GST details');
                    resolve();
                }
            }
            else {
                console.log("Form valid");
                _this.isGSTValid = true;
                _this.isExistance = false;
                resolve();
            }
        });
    };
    TravelProposalPage.prototype.createCustomer = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log(_this.pinDataPermValue);
            console.log(form.value);
            var body = {
                "CustomerID": 0,
                "HasAddressChanged": true,
                "SetAsDefault": true,
                "TitleText": form.value.applTitle,
                "TitleValue": _this.titleDetails.id,
                "Name": form.value.applName,
                "DateOfBirth": moment(form.value.applDOB).format('DD-MMM-YYYY'),
                "MaritalStatusValue": -1,
                "MaritalStatusText": "",
                "OccupationValue": -1,
                "OccupationText": "",
                "AnnualIncomeValue": -1,
                "AnnualIncomeText": "",
                "IdentityProofValue": "2",
                "IdentityProofText": "Aadhaar Card",
                "IdentityNumber": "640660536358",
                "PresentAddrLine1": form.value.applAdd1,
                "PresentAddrLine2": form.value.applAdd2,
                "PresentAddrLandmark": form.value.applLandmark,
                "PresentAddrCityValue": _this.pinData.CityList[0].CityID,
                "PresentAddrCityText": form.value.applCity,
                "PresentAddrStateValue": _this.pinData.StateId,
                "PresentAddrStateText": form.value.applState,
                "PresentAddrPincodeValue": _this.pinDataValue.Details[0].Value,
                "PresentAddrPincodeText": form.value.applPinCode,
                "EmailAddress": form.value.applEmail,
                "MobileNumber": form.value.mobileNo,
                "LandlineNumber": "",
                "EmailAlternate": "",
                "MobileAlternate": "",
                "PANNumber": form.value.panNumber,
                "isPermanentAsPresent": _this.showPermAdd,
                "PermanentAddrLine1": form.value.permAdd1,
                "PermanentAddrLine2": form.value.permAdd2,
                "PermanentAddrLandmark": form.value.permLandmark,
                "PermanentAddrCityValue": _this.pinDataPer.CityList[0].CityID,
                "PermanentAddrCityText": form.value.permCity,
                "PermanentAddrStateValue": _this.pinDataPer.StateId,
                "PermanentAddrStateText": form.value.permState,
                "PermanentAddrPincodeValue": _this.pinDataPermValue.Details[0].Value,
                "PermanentAddrPincodeText": form.value.permPinCode,
                "isGSTINApplicable": _this.isGSTIN,
                "isUINApplicable": _this.isUIN,
                "GSTDetails": {
                    "GSTIN_NO": form.value.GSTIN,
                    "CONSTITUTION_OF_BUSINESS": form.value.constitution,
                    "CONSTITUTION_OF_BUSINESS_TEXT": _this.constitutionName,
                    "CUSTOMER_TYPE": form.value.customerType,
                    "CUSTOMER_TYPE_TEXT": _this.customerType,
                    "PAN_NO": form.value.gstPanNo,
                    "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
                    "GST_REGISTRATION_STATUS_TEXT": _this.gstName
                }
            };
            console.log("Create Cust Body", body);
            var str = JSON.stringify(body);
            localStorage.setItem('customerRequest', str);
            console.log("Create Cust stringify", str);
            _this.cs.showLoader = true;
            _this.cs.postWithParams('/api/customer/saveeditcustomer', str).then(function (res) {
                if ((res.PFCustomerID == '' && res.CustomerID == '') || (res.PFCustomerID == undefined && res.CustomerID == undefined)) {
                    _this.showToast("Failed to create customer");
                    _this.cs.showLoader = false;
                    resolve();
                }
                else {
                    console.log("Travel create Cust", res);
                    _this.customerData = res;
                    _this.cs.showLoader = false;
                    resolve();
                }
            }).catch(function (err) {
                console.log(err);
                _this.showToast(err.error.Message);
                _this.cs.showLoader = false;
                resolve();
            });
        });
    };
    TravelProposalPage.prototype.createMemberObj = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("Create Member Called");
            if (_this.request.NoOfTravelers == 1) {
                _this.memberObj = [{
                        "InsuredName": form.value.insu1Name, "PassportNumber": form.value.insu1PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu1NomName, "AilmentName": form.value.insu1Ailment, "DateOfBirth": form.value.insu1DOB,
                        "RelationshipName": form.value.insu1Rela, "RelationshipID": _this.applRela1.RelationshipID, "Title": _this.insuTitleDetails1.id
                    }];
            }
            else if (_this.request.NoOfTravelers == 2) {
                _this.memberObj = [{
                        "InsuredName": form.value.insu1Name, "PassportNumber": form.value.insu1PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu1NomName, "AilmentName": form.value.insu1Ailment, "DateOfBirth": form.value.insu1DOB,
                        "RelationshipName": form.value.insu1Rela, "RelationshipID": _this.applRela1.RelationshipID, "Title": _this.insuTitleDetails1.id
                    }, {
                        "InsuredName": form.value.insu2Name, "PassportNumber": form.value.insu2PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu2NomName, "AilmentName": form.value.insu2Ailment, "DateOfBirth": form.value.insu2DOB,
                        "RelationshipName": form.value.insu2Rela, "RelationshipID": _this.applRela2.RelationshipID, "Title": _this.insuTitleDetails2.id
                    }];
            }
            else if (_this.request.NoOfTravelers == 3) {
                _this.memberObj = [{
                        "InsuredName": form.value.insu1Name, "PassportNumber": form.value.insu1PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu1NomName, "AilmentName": form.value.insu1Ailment, "DateOfBirth": form.value.insu1DOB,
                        "RelationshipName": form.value.insu1Rela, "RelationshipID": _this.applRela1.RelationshipID, "Title": _this.insuTitleDetails1.id
                    }, {
                        "InsuredName": form.value.insu2Name, "PassportNumber": form.value.insu2PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu2NomName, "AilmentName": form.value.insu2Ailment, "DateOfBirth": form.value.insu2DOB,
                        "RelationshipName": form.value.insu2Rela, "RelationshipID": _this.applRela2.RelationshipID, "Title": _this.insuTitleDetails2.id
                    }, {
                        "InsuredName": form.value.insu3Name, "PassportNumber": form.value.insu3PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu3NomName, "AilmentName": form.value.insu3Ailment, "DateOfBirth": form.value.insu3DOB,
                        "RelationshipName": form.value.insu3Rela, "RelationshipID": _this.applRela3.RelationshipID, "Title": _this.insuTitleDetails3.id
                    }];
            }
            else if (_this.request.NoOfTravelers == 4) {
                _this.memberObj = [{
                        "InsuredName": form.value.insu1Name, "PassportNumber": form.value.insu1PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu1NomName, "AilmentName": form.value.insu1Ailment, "DateOfBirth": form.value.insu1DOB,
                        "RelationshipName": form.value.insu1Rela, "RelationshipID": _this.applRela1.RelationshipID, "Title": _this.insuTitleDetails1.id
                    }, {
                        "InsuredName": form.value.insu2Name, "PassportNumber": form.value.insu2PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu2NomName, "AilmentName": form.value.insu2Ailment, "DateOfBirth": form.value.insu2DOB,
                        "RelationshipName": form.value.insu2Rela, "RelationshipID": _this.applRela2.RelationshipID, "Title": _this.insuTitleDetails2.id
                    }, {
                        "InsuredName": form.value.insu3Name, "PassportNumber": form.value.insu3PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu3NomName, "AilmentName": form.value.insu3Ailment, "DateOfBirth": form.value.insu3DOB,
                        "RelationshipName": form.value.insu3Rela, "RelationshipID": _this.applRela3.RelationshipID, "Title": _this.insuTitleDetails3.id
                    }, {
                        "InsuredName": form.value.insu4Name, "PassportNumber": form.value.insu4PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu4NomName, "AilmentName": form.value.insu4Ailment, "DateOfBirth": form.value.insu4DOB,
                        "RelationshipName": form.value.insu4Rela, "RelationshipID": _this.applRela4.RelationshipID, "Title": _this.insuTitleDetails4.id
                    }];
            }
            else if (_this.request.NoOfTravelers == 5) {
                _this.memberObj = [{
                        "InsuredName": form.value.insu1Name, "PassportNumber": form.value.insu1PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu1NomName, "AilmentName": form.value.insu1Ailment, "DateOfBirth": form.value.insu1DOB,
                        "RelationshipName": form.value.insu1Rela, "RelationshipID": _this.applRela1.RelationshipID, "Title": _this.insuTitleDetails1.id
                    }, {
                        "InsuredName": form.value.insu2Name, "PassportNumber": form.value.insu2PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu2NomName, "AilmentName": form.value.insu2Ailment, "DateOfBirth": form.value.insu2DOB,
                        "RelationshipName": form.value.insu2Rela, "RelationshipID": _this.applRela2.RelationshipID, "Title": _this.insuTitleDetails2.id
                    }, {
                        "InsuredName": form.value.insu3Name, "PassportNumber": form.value.insu3PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu3NomName, "AilmentName": form.value.insu3Ailment, "DateOfBirth": form.value.insu3DOB,
                        "RelationshipName": form.value.insu3Rela, "RelationshipID": _this.applRela3.RelationshipID, "Title": _this.insuTitleDetails3.id
                    }, {
                        "InsuredName": form.value.insu4Name, "PassportNumber": form.value.insu4PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu4NomName, "AilmentName": form.value.insu4Ailment, "DateOfBirth": form.value.insu4DOB,
                        "RelationshipName": form.value.insu4Rela, "RelationshipID": _this.applRela4.RelationshipID, "Title": _this.insuTitleDetails4.id
                    }, {
                        "InsuredName": form.value.insu5Name, "PassportNumber": form.value.insu5PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu5NomName, "AilmentName": form.value.insu5Ailment, "DateOfBirth": form.value.insu5DOB,
                        "RelationshipName": form.value.insu5Rela, "RelationshipID": _this.applRela5.RelationshipID, "Title": _this.insuTitleDetails5.id
                    }];
            }
            else {
                _this.memberObj = [{
                        "InsuredName": form.value.insu1Name, "PassportNumber": form.value.insu1PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu1NomName, "AilmentName": form.value.insu1Ailment, "DateOfBirth": form.value.insu1DOB,
                        "RelationshipName": form.value.insu1Rela, "RelationshipID": _this.applRela1.RelationshipID, "Title": _this.insuTitleDetails1.id
                    }, {
                        "InsuredName": form.value.insu2Name, "PassportNumber": form.value.insu2PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu2NomName, "AilmentName": form.value.insu2Ailment, "DateOfBirth": form.value.insu2DOB,
                        "RelationshipName": form.value.insu2Rela, "RelationshipID": _this.applRela2.RelationshipID, "Title": _this.insuTitleDetails2.id
                    }, {
                        "InsuredName": form.value.insu3Name, "PassportNumber": form.value.insu3PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu3NomName, "AilmentName": form.value.insu3Ailment, "DateOfBirth": form.value.insu3DOB,
                        "RelationshipName": form.value.insu3Rela, "RelationshipID": _this.applRela3.RelationshipID, "Title": _this.insuTitleDetails3.id
                    }, {
                        "InsuredName": form.value.insu4Name, "PassportNumber": form.value.insu4PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu4NomName, "AilmentName": form.value.insu4Ailment, "DateOfBirth": form.value.insu4DOB,
                        "RelationshipName": form.value.insu4Rela, "RelationshipID": _this.applRela4.RelationshipID, "Title": _this.insuTitleDetails4.id
                    }, {
                        "InsuredName": form.value.insu5Name, "PassportNumber": form.value.insu5PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu5NomName, "AilmentName": form.value.insu5Ailment, "DateOfBirth": form.value.insu5DOB,
                        "RelationshipName": form.value.insu5Rela, "RelationshipID": _this.applRela5.RelationshipID, "Title": _this.insuTitleDetails5.id
                    }, {
                        "InsuredName": form.value.insu6Name, "PassportNumber": form.value.insu6PassNo, "MotherMaidenName": "NA",
                        "NomineeName": form.value.insu6NomName, "AilmentName": form.value.insu6Ailment, "DateOfBirth": form.value.insu6DOB,
                        "RelationshipName": form.value.insu6Rela, "RelationshipID": _this.applRela6.RelationshipID, "Title": _this.insuTitleDetails6.id
                    }];
            }
            resolve();
        });
    };
    TravelProposalPage.prototype.travelProposal = function (form, form2) {
        var _this = this;
        return new Promise(function (resolve) {
            var body = {
                "UserType": "AGENT",
                "ipaddress": config.ipAddress,
                "Product": "Travel",
                "ProductType": "International",
                "PolicyID": _this.response.PolicyID,
                "CustomerID": _this.customerData.CustomerID,
                "GSTStateCode": _this.request.GSTStateCode,
                "GSTStateName": _this.request.GSTStateName,
                "MailPolicyCopy": "NA",
                "SourcingCode": "NA",
                "PanCardNo": _this.applicantForm.value.panNumber,
                "Members": _this.memberObj,
                "GSTApplicable": "FALSE",
                "HasIbankRelationship": "FALSE",
                "iBankRelationship": { "IsIbankRelationship": "FALSE" },
                "PF_CUSTOMERID": _this.customerData.PFCustomerID
            };
            var str = JSON.stringify(body);
            console.log(str);
            localStorage.setItem('proposalRequest', str);
            _this.cs.showLoader = true;
            _this.cs.postWithParams('/api/Travel/SaveProposalDetails1', str).then(function (res) {
                console.log("Response", res);
                if (res.StatusCode == 1) {
                    localStorage.setItem('proposalResponse', JSON.stringify(res));
                    _this.router.navigateByUrl('travel-summary');
                    _this.cs.showLoader = false;
                    resolve();
                }
                else {
                    _this.showToast("Failed to create proposal");
                    console.log("Something Went wrong", res);
                    _this.cs.showLoader = false;
                    resolve();
                }
            });
        });
    };
    TravelProposalPage.prototype.getAilment = function (ev) {
        console.log(ev.target.value);
        if (ev.target.value != 'none') {
            var msg = 'Due to the ailment, We will not be able to process your request online. Please contact our customer support for further details.';
            this.presentAlertConfirm(msg);
        }
    };
    TravelProposalPage.prototype.presentAlertConfirm = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Alert',
                            message: msg,
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        console.log('Confirm Cancel: blah');
                                    }
                                }, {
                                    text: 'Okay',
                                    handler: function () {
                                        console.log('Confirm Okay');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TravelProposalPage.prototype.validateMember = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var index = _this.request.NoOfTravelers;
            if (index == 1) {
                console.log("Invalid Members 1");
                _this.insuredForm.get('insu1Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu1DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu1PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Title').updateValueAndValidity();
                _this.insuredForm.get('insu1Name').updateValueAndValidity();
                _this.insuredForm.get('insu1Rela').updateValueAndValidity();
                _this.insuredForm.get('insu1DOB').updateValueAndValidity();
                _this.insuredForm.get('insu1PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu1Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu1NomName').updateValueAndValidity();
                _this.insuredForm.get('insu1NomRela').updateValueAndValidity();
            }
            else if (index == 2) {
                console.log("Invalid Members 2");
                _this.insuredForm.get('insu1Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu1DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu1PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu2DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu2PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu2NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu2NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Title').updateValueAndValidity();
                _this.insuredForm.get('insu1Name').updateValueAndValidity();
                _this.insuredForm.get('insu1Rela').updateValueAndValidity();
                _this.insuredForm.get('insu1DOB').updateValueAndValidity();
                _this.insuredForm.get('insu1PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu1Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu1NomName').updateValueAndValidity();
                _this.insuredForm.get('insu1NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu2Title').updateValueAndValidity();
                _this.insuredForm.get('insu2Name').updateValueAndValidity();
                _this.insuredForm.get('insu2Rela').updateValueAndValidity();
                _this.insuredForm.get('insu2DOB').updateValueAndValidity();
                _this.insuredForm.get('insu2PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu2Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu2NomName').updateValueAndValidity();
                _this.insuredForm.get('insu2NomRela').updateValueAndValidity();
            }
            else if (index == 3) {
                console.log("Invalid Members 3");
                _this.insuredForm.get('insu1Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu1DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu1PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu2DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu2PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu2NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu2NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu3DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu3PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu3NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu3NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Title').updateValueAndValidity();
                _this.insuredForm.get('insu1Name').updateValueAndValidity();
                _this.insuredForm.get('insu1Rela').updateValueAndValidity();
                _this.insuredForm.get('insu1DOB').updateValueAndValidity();
                _this.insuredForm.get('insu1PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu1Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu1NomName').updateValueAndValidity();
                _this.insuredForm.get('insu1NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu2Title').updateValueAndValidity();
                _this.insuredForm.get('insu2Name').updateValueAndValidity();
                _this.insuredForm.get('insu2Rela').updateValueAndValidity();
                _this.insuredForm.get('insu2DOB').updateValueAndValidity();
                _this.insuredForm.get('insu2PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu2Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu2NomName').updateValueAndValidity();
                _this.insuredForm.get('insu2NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu3Title').updateValueAndValidity();
                _this.insuredForm.get('insu3Name').updateValueAndValidity();
                _this.insuredForm.get('insu3Rela').updateValueAndValidity();
                _this.insuredForm.get('insu3DOB').updateValueAndValidity();
                _this.insuredForm.get('insu3PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu3Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu3NomName').updateValueAndValidity();
                _this.insuredForm.get('insu3NomRela').updateValueAndValidity();
            }
            else if (index == 4) {
                console.log("Invalid Members 4");
                _this.insuredForm.get('insu1Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu1DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu1PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu2DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu2PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu2NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu2NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu3DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu3PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu3NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu3NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu4DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu4PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu4NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu4NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Title').updateValueAndValidity();
                _this.insuredForm.get('insu1Name').updateValueAndValidity();
                _this.insuredForm.get('insu1Rela').updateValueAndValidity();
                _this.insuredForm.get('insu1DOB').updateValueAndValidity();
                _this.insuredForm.get('insu1PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu1Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu1NomName').updateValueAndValidity();
                _this.insuredForm.get('insu1NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu2Title').updateValueAndValidity();
                _this.insuredForm.get('insu2Name').updateValueAndValidity();
                _this.insuredForm.get('insu2Rela').updateValueAndValidity();
                _this.insuredForm.get('insu2DOB').updateValueAndValidity();
                _this.insuredForm.get('insu2PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu2Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu2NomName').updateValueAndValidity();
                _this.insuredForm.get('insu2NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu3Title').updateValueAndValidity();
                _this.insuredForm.get('insu3Name').updateValueAndValidity();
                _this.insuredForm.get('insu3Rela').updateValueAndValidity();
                _this.insuredForm.get('insu3DOB').updateValueAndValidity();
                _this.insuredForm.get('insu3PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu3Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu3NomName').updateValueAndValidity();
                _this.insuredForm.get('insu3NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu4Title').updateValueAndValidity();
                _this.insuredForm.get('insu4Name').updateValueAndValidity();
                _this.insuredForm.get('insu4Rela').updateValueAndValidity();
                _this.insuredForm.get('insu4DOB').updateValueAndValidity();
                _this.insuredForm.get('insu4PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu4Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu4NomName').updateValueAndValidity();
                _this.insuredForm.get('insu4NomRela').updateValueAndValidity();
            }
            else if (index == 5) {
                console.log("Invalid Members 5");
                _this.insuredForm.get('insu1Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu1DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu1PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu2DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu2PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu2NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu2NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu3DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu3PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu3NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu3NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu4DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu4PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu4NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu4NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu5Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu5Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu5Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu5DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu5PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu5Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu5NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu5NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu5Title').updateValueAndValidity();
                _this.insuredForm.get('insu5Name').updateValueAndValidity();
                _this.insuredForm.get('insu5Rela').updateValueAndValidity();
                _this.insuredForm.get('insu5DOB').updateValueAndValidity();
                _this.insuredForm.get('insu5PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu5Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu5NomName').updateValueAndValidity();
                _this.insuredForm.get('insu5NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu1Title').updateValueAndValidity();
                _this.insuredForm.get('insu1Name').updateValueAndValidity();
                _this.insuredForm.get('insu1Rela').updateValueAndValidity();
                _this.insuredForm.get('insu1DOB').updateValueAndValidity();
                _this.insuredForm.get('insu1PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu1Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu1NomName').updateValueAndValidity();
                _this.insuredForm.get('insu1NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu2Title').updateValueAndValidity();
                _this.insuredForm.get('insu2Name').updateValueAndValidity();
                _this.insuredForm.get('insu2Rela').updateValueAndValidity();
                _this.insuredForm.get('insu2DOB').updateValueAndValidity();
                _this.insuredForm.get('insu2PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu2Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu2NomName').updateValueAndValidity();
                _this.insuredForm.get('insu2NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu3Title').updateValueAndValidity();
                _this.insuredForm.get('insu3Name').updateValueAndValidity();
                _this.insuredForm.get('insu3Rela').updateValueAndValidity();
                _this.insuredForm.get('insu3DOB').updateValueAndValidity();
                _this.insuredForm.get('insu3PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu3Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu3NomName').updateValueAndValidity();
                _this.insuredForm.get('insu3NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu4Title').updateValueAndValidity();
                _this.insuredForm.get('insu4Name').updateValueAndValidity();
                _this.insuredForm.get('insu4Rela').updateValueAndValidity();
                _this.insuredForm.get('insu4DOB').updateValueAndValidity();
                _this.insuredForm.get('insu4PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu4Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu4NomName').updateValueAndValidity();
                _this.insuredForm.get('insu4NomRela').updateValueAndValidity();
            }
            else {
                console.log("Invalid Members 6");
                _this.insuredForm.get('insu1Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu1DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu1PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu1Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu1NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu2DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu2PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu2Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu2NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu2NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu3DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu3PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu3Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu3NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu3NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu4DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu4PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu4Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu4NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu4NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu5Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu5Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu5Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu5DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu5PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu5Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu5NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu5NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu6Title').setValidators([Validators.required]);
                _this.insuredForm.get('insu6Name').setValidators([Validators.required]);
                _this.insuredForm.get('insu6Rela').setValidators([Validators.required]);
                _this.insuredForm.get('insu6DOB').setValidators([Validators.required]);
                _this.insuredForm.get('insu6PassNo').setValidators([Validators.required]);
                _this.insuredForm.get('insu6Ailment').setValidators([Validators.required]);
                _this.insuredForm.get('insu6NomName').setValidators([Validators.required]);
                _this.insuredForm.get('insu6NomRela').setValidators([Validators.required]);
                _this.insuredForm.get('insu6Title').updateValueAndValidity();
                _this.insuredForm.get('insu6Name').updateValueAndValidity();
                _this.insuredForm.get('insu6Rela').updateValueAndValidity();
                _this.insuredForm.get('insu6DOB').updateValueAndValidity();
                _this.insuredForm.get('insu6PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu6Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu6NomName').updateValueAndValidity();
                _this.insuredForm.get('insu6NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu5Title').updateValueAndValidity();
                _this.insuredForm.get('insu5Name').updateValueAndValidity();
                _this.insuredForm.get('insu5Rela').updateValueAndValidity();
                _this.insuredForm.get('insu5DOB').updateValueAndValidity();
                _this.insuredForm.get('insu5PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu5Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu5NomName').updateValueAndValidity();
                _this.insuredForm.get('insu5NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu1Title').updateValueAndValidity();
                _this.insuredForm.get('insu1Name').updateValueAndValidity();
                _this.insuredForm.get('insu1Rela').updateValueAndValidity();
                _this.insuredForm.get('insu1DOB').updateValueAndValidity();
                _this.insuredForm.get('insu1PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu1Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu1NomName').updateValueAndValidity();
                _this.insuredForm.get('insu1NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu2Title').updateValueAndValidity();
                _this.insuredForm.get('insu2Name').updateValueAndValidity();
                _this.insuredForm.get('insu2Rela').updateValueAndValidity();
                _this.insuredForm.get('insu2DOB').updateValueAndValidity();
                _this.insuredForm.get('insu2PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu2Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu2NomName').updateValueAndValidity();
                _this.insuredForm.get('insu2NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu3Title').updateValueAndValidity();
                _this.insuredForm.get('insu3Name').updateValueAndValidity();
                _this.insuredForm.get('insu3Rela').updateValueAndValidity();
                _this.insuredForm.get('insu3DOB').updateValueAndValidity();
                _this.insuredForm.get('insu3PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu3Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu3NomName').updateValueAndValidity();
                _this.insuredForm.get('insu3NomRela').updateValueAndValidity();
                _this.insuredForm.get('insu4Title').updateValueAndValidity();
                _this.insuredForm.get('insu4Name').updateValueAndValidity();
                _this.insuredForm.get('insu4Rela').updateValueAndValidity();
                _this.insuredForm.get('insu4DOB').updateValueAndValidity();
                _this.insuredForm.get('insu4PassNo').updateValueAndValidity();
                _this.insuredForm.get('insu4Ailment').updateValueAndValidity();
                _this.insuredForm.get('insu4NomName').updateValueAndValidity();
                _this.insuredForm.get('insu4NomRela').updateValueAndValidity();
            }
            resolve();
        });
    };
    TravelProposalPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    TravelProposalPage = tslib_1.__decorate([
        Component({
            selector: 'app-travel-proposal',
            templateUrl: './travel-proposal.page.html',
            styleUrls: ['./travel-proposal.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router, AlertController, FormBuilder, CommonService, ToastController])
    ], TravelProposalPage);
    return TravelProposalPage;
}());
export { TravelProposalPage };
//# sourceMappingURL=travel-proposal.page.js.map