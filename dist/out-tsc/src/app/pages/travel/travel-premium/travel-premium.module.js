import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TravelPremiumPage } from './travel-premium.page';
var routes = [
    {
        path: '',
        component: TravelPremiumPage
    }
];
var TravelPremiumPageModule = /** @class */ (function () {
    function TravelPremiumPageModule() {
    }
    TravelPremiumPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TravelPremiumPage]
        })
    ], TravelPremiumPageModule);
    return TravelPremiumPageModule;
}());
export { TravelPremiumPageModule };
//# sourceMappingURL=travel-premium.module.js.map