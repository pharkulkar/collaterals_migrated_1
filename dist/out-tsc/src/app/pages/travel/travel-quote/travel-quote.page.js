import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { config } from 'src/app/config.properties';
var TravelQuotePage = /** @class */ (function () {
    // public myDatePickerOptions: IMyOptions = {
    //   // Your options
    //   };
    function TravelQuotePage(activatedRoute, cs, formBuilder, location, router, xml2JsonService, toastCtrl) {
        this.activatedRoute = activatedRoute;
        this.cs = cs;
        this.formBuilder = formBuilder;
        this.location = location;
        this.router = router;
        this.xml2JsonService = xml2JsonService;
        this.toastCtrl = toastCtrl;
        this.customPopoverOptions = {};
        this.showAddOn = false;
        this.isMultiTrip = false;
        this.isFamilyCover = false;
        this.isCategoryValidation = false;
        this.isImmigrant = false;
        this.validForm = false;
        this.traveller1 = true;
        this.traveller2 = false;
        this.traveller3 = false;
        this.traveller4 = false;
        this.traveller5 = false;
        this.traveller6 = false;
        this.inputLoadingPlan = false;
        this.inputLoadingState = false;
        this.isCountryList = false;
        this.isKerala = false;
        this.isKeralaCess = false;
        this.noOfTraveller = 1;
        this.isIndividual = 'False';
        this.isFamily = 'False';
        this.isSeniorCitizen = 'False';
        this.isProfessional = 'false';
        this.isAdvanture = 'false';
        this.tripType = "Round Trip";
        this.IsAnnualMultiTrip = 'No';
        this.isVisitingUSCanada = "";
        this.isVisitingSCHENGEN = "";
        this.isVisitingOtherCountries = "";
        this.plans = [];
        this.usaOddData = [];
        this.usaEvenData = [];
        this.schengenOddData = [];
        this.schengenEvenData = [];
        this.isQuoteDetails = true;
        this.isPremiumDetails = true;
        this.isQuoteGenerated = false;
        this.isRecalculate = "false";
    }
    TravelQuotePage.prototype.ngOnInit = function () {
        var iskeralaCessFeature = localStorage.getItem('isKeralaCessEnabled');
        if (iskeralaCessFeature == 'Y') {
            this.isKeralaCessEnabled = true;
        }
        else {
            this.isKeralaCessEnabled = false;
        }
        this.isCategoryValidation = false;
        this.getCountryData();
        if (this.cs.isBackFormTravelPremium) {
            this.reCalculate();
        }
        else {
            this.createTravelForm();
        }
        var date = new Date();
        this.minDate = moment(date).format('YYYY-MM-DD');
        var maxDate = new Date(date);
        maxDate.setDate(maxDate.getDate() + parseInt('365'));
        this.maxDate = moment(maxDate).format('YYYY-MM-DD');
        var minDOB = new Date(date);
        minDOB.setDate(minDOB.getDate() - parseInt('91'));
        this.minDOB = moment(minDOB).format('YYYY-MM-DD');
        this.getStateMaster();
    };
    TravelQuotePage.prototype.getList = function (ev) {
        console.log(ev);
        if (ev == 'No') {
            this.isCountryList = false;
            document.getElementById("myNav").style.height = "0%";
        }
        else {
            this.isCountryList = true;
            document.getElementById("myNav").style.height = "100%";
        }
    };
    TravelQuotePage.prototype.getCountryData = function () {
        var _this = this;
        this.cs.postWithParams('/api/Travel/ListofEuropeanUnionMemberStates', '').then(function (res) {
            _this.travelEuropeanUnionMemberData = res.EuropeanUnionMemberStates;
            res.EuropeanUnionMemberStates.forEach(function (element, index) {
                if (index % 2 == 0) {
                    _this.usaEvenData.push(res.EuropeanUnionMemberStates[index]);
                }
                else {
                    _this.usaOddData.push(res.EuropeanUnionMemberStates[index]);
                }
            });
            console.log(_this.usaEvenData, _this.usaOddData);
            console.log(_this.travelEuropeanUnionMemberData);
        });
        this.cs.postWithParams('/api/Travel/ListofSchengenStates', '').then(function (res) {
            res.SchengenStates.forEach(function (element, index) {
                if (index % 2 == 0) {
                    _this.schengenEvenData.push(res.SchengenStates[index]);
                }
                else {
                    _this.schengenOddData.push(res.SchengenStates[index]);
                }
            });
            console.log(_this.schengenEvenData, _this.schengenOddData);
            console.log(_this.travelSchengenData);
        });
    };
    TravelQuotePage.prototype.closeNav = function (ev) {
        document.getElementById("myNav").style.height = "0%";
    };
    TravelQuotePage.prototype.getStateMaster = function () {
        var _this = this;
        console.log("State");
        this.inputLoadingState = true;
        this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(function (res) {
            _this.stateMaster = res;
            _this.inputLoadingState = false;
            console.log(res);
        }).catch(function (err) {
            _this.inputLoadingState = false;
            console.log(err);
        });
    };
    TravelQuotePage.prototype.getStateDetails = function (ev) {
        console.log(ev.target.value);
        this.stateDetails = this.stateMaster.find(function (x) { return x.StateName == ev.target.value; });
        if (this.isKeralaCessEnabled) {
            if (ev.target.value == 'KERALA') {
                this.isKerala = true;
            }
            else {
                this.travelForm.patchValue({ 'isKeralaGST': false });
                this.isKerala = false;
            }
        }
        else {
            this.isKerala = false;
            this.travelForm.patchValue({ 'isKeralaGST': false });
        }
    };
    TravelQuotePage.prototype.isKeralaGST = function (data) {
        console.log(data);
        if (data == 'Yes') {
            this.travelForm.patchValue({ 'isKeralaGST': true });
            ;
            this.isGSTKerala = true;
        }
        else {
            this.travelForm.patchValue({ 'isKeralaGST': false });
            this.isGSTKerala = false;
        }
    };
    TravelQuotePage.prototype.createTravelForm = function () {
        this.travelForm = this.formBuilder.group({
            productName: ['internationalTravel'],
            subProduct: [null], tripType: [null], travelLoc: [null], isImmigrantVisa: [null],
            leavingDate: [null], returningDate: [null], noOfTripDays: [null], coverageType: [null],
            coverNeeded: [null], noOfTraveller: [null], traveller1DOB: [null], traveller2DOB: [null],
            traveller3DOB: [null], traveller4DOB: [null], traveller5DOB: [null], traveller6DOB: [null],
            plan: [null], tripDuration: [null], policyStartDate: [null], state: [null], isKeralaGST: [null]
        });
        this.categoryValidation();
    };
    TravelQuotePage.prototype.reCalculate = function () {
        this.travelForm = this.formBuilder.group({
            productName: ['internationalTravel'],
            subProduct: [this.travelForm.value.subProduct], tripType: [null], travelLoc: [null], isImmigrantVisa: [null],
            leavingDate: [null], returningDate: [null], noOfTripDays: [null], coverageType: [null],
            coverNeeded: [null], noOfTraveller: [null], traveller1DOB: [null], traveller2DOB: [null],
            traveller3DOB: [null], traveller4DOB: [null], traveller5DOB: [null], traveller6DOB: [null],
            plan: [null], tripDuration: [null], policyStartDate: [null], state: [null], isKeralaGST: [null]
        });
        // this.categoryValidation();
    };
    TravelQuotePage.prototype.resetForm = function () {
        this.travelForm.patchValue({ 'leavingDate': "" });
        this.travelForm.patchValue({ 'returningDate': "" });
        this.travelForm.patchValue({ 'noOfTripDays': "" });
        this.travelForm.patchValue({ 'coverageType': "" });
        this.travelForm.patchValue({ 'coverNeeded': "" });
        this.travelForm.patchValue({ 'noOfTraveller': "" });
        this.travelForm.patchValue({ 'traveller1DOB': "" });
        this.travelForm.patchValue({ 'traveller2DOB': "" });
        this.travelForm.patchValue({ 'traveller3DOB': "" });
        this.travelForm.patchValue({ 'traveller4DOB': "" });
        this.travelForm.patchValue({ 'traveller5DOB': "" });
        this.travelForm.patchValue({ 'traveller6DOB': "" });
        this.travelForm.patchValue({ 'plan': "" });
        this.travelForm.patchValue({ 'tripDuration': "" });
        this.travelForm.patchValue({ 'policyStartDate': "" });
        this.travelForm.patchValue({ 'state': "0" });
        this.travelForm.patchValue({ 'isKeralaGST': "" });
        this.travelType = "";
    };
    TravelQuotePage.prototype.categoryValidation = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.travelForm.get('subProduct').valueChanges.subscribe(function (data) {
                if (data == 'Individual') {
                    _this.travelForm.get('tripType').setValidators([Validators.required]);
                    _this.travelForm.get('tripType').valueChanges.subscribe(function (data) {
                        console.log("Trip Type", data);
                        if (!data) {
                            // this.isCategoryValidation = true;
                            _this.travelForm.get('travelLoc').setValidators([Validators.required]);
                            _this.travelForm.get('isImmigrantVisa').setValidators([Validators.required]);
                            _this.travelForm.get('leavingDate').setValidators([Validators.required]);
                            _this.travelForm.get('returningDate').setValidators([Validators.required]);
                            _this.travelForm.get('plan').setValidators([Validators.required]);
                            _this.travelForm.get('state').setValidators([Validators.required]);
                            _this.travelForm.get('noOfTraveller').setValidators([Validators.required]);
                            _this.travelForm.get('traveller1DOB').setValidators([Validators.required]);
                            _this.travelForm.get('tripDuration').clearValidators();
                            _this.travelForm.get('policyStartDate').clearValidators();
                        }
                        else {
                            // this.isCategoryValidation = true;
                            _this.travelForm.get('tripDuration').setValidators([Validators.required]);
                            _this.travelForm.get('policyStartDate').setValidators([Validators.required]);
                            _this.travelForm.get('plan').setValidators([Validators.required]);
                            _this.travelForm.get('state').setValidators([Validators.required]);
                            _this.travelForm.get('noOfTraveller').setValidators([Validators.required]);
                            _this.travelForm.get('traveller1DOB').setValidators([Validators.required]);
                            _this.travelForm.get('travelLoc').clearValidators();
                            _this.travelForm.get('isImmigrantVisa').clearValidators();
                            _this.travelForm.get('leavingDate').clearValidators();
                            _this.travelForm.get('returningDate').clearValidators();
                        }
                        _this.travelForm.get('travelLoc').updateValueAndValidity();
                        _this.travelForm.get('isImmigrantVisa').updateValueAndValidity();
                        _this.travelForm.get('leavingDate').updateValueAndValidity();
                        _this.travelForm.get('returningDate').updateValueAndValidity();
                        _this.travelForm.get('tripDuration').updateValueAndValidity();
                        _this.travelForm.get('policyStartDate').updateValueAndValidity();
                        _this.travelForm.get('plan').updateValueAndValidity();
                        _this.travelForm.get('state').updateValueAndValidity();
                        _this.travelForm.get('noOfTraveller').updateValueAndValidity();
                        _this.travelForm.get('traveller1DOB').updateValueAndValidity();
                        // resolve();
                    });
                }
                else if (data == 'Family') {
                    // this.isCategoryValidation = true;
                    _this.travelForm.get('tripType').clearValidators();
                    _this.travelForm.get('policyStartDate').clearValidators();
                    _this.travelForm.get('tripDuration').clearValidators();
                    _this.travelForm.get('travelLoc').setValidators([Validators.required]);
                    _this.travelForm.get('isImmigrantVisa').setValidators([Validators.required]);
                    _this.travelForm.get('leavingDate').setValidators([Validators.required]);
                    _this.travelForm.get('returningDate').setValidators([Validators.required]);
                    _this.travelForm.get('plan').setValidators([Validators.required]);
                    _this.travelForm.get('state').setValidators([Validators.required]);
                    _this.travelForm.get('noOfTraveller').setValidators([Validators.required]);
                    _this.travelForm.get('traveller1DOB').setValidators([Validators.required]);
                }
                else if (data == 'Senior Citizen') {
                    // this.isCategoryValidation = true;
                    _this.travelForm.get('tripType').clearValidators();
                    _this.travelForm.get('tripDuration').clearValidators();
                    _this.travelForm.get('policyStartDate').clearValidators();
                    _this.travelForm.get('travelLoc').setValidators([Validators.required]);
                    _this.travelForm.get('isImmigrantVisa').setValidators([Validators.required]);
                    _this.travelForm.get('leavingDate').setValidators([Validators.required]);
                    _this.travelForm.get('returningDate').setValidators([Validators.required]);
                    _this.travelForm.get('plan').setValidators([Validators.required]);
                    _this.travelForm.get('state').setValidators([Validators.required]);
                    _this.travelForm.get('noOfTraveller').setValidators([Validators.required]);
                    _this.travelForm.get('traveller1DOB').setValidators([Validators.required]);
                }
                else {
                    console.log("Fill The form");
                }
                _this.travelForm.get('tripType').updateValueAndValidity();
                _this.travelForm.get('travelLoc').updateValueAndValidity();
                _this.travelForm.get('isImmigrantVisa').updateValueAndValidity();
                _this.travelForm.get('leavingDate').updateValueAndValidity();
                _this.travelForm.get('returningDate').updateValueAndValidity();
                _this.travelForm.get('tripDuration').updateValueAndValidity();
                _this.travelForm.get('policyStartDate').updateValueAndValidity();
                _this.travelForm.get('plan').updateValueAndValidity();
                _this.travelForm.get('state').updateValueAndValidity();
                _this.travelForm.get('noOfTraveller').updateValueAndValidity();
                _this.travelForm.get('traveller1DOB').updateValueAndValidity();
                // resolve();
            });
            resolve();
        });
    };
    TravelQuotePage.prototype.getCover = function (ev) {
        console.log(ev);
        if (ev.target.id == 'familyPlan') {
            this.isFamilyCover = true;
        }
        else {
            this.travelForm.patchValue({ 'noOfTraveller': '1' });
            this.isFamilyCover = false;
        }
    };
    TravelQuotePage.prototype.changeSubProduct = function (ev) {
        this.subProduct = ev.target.value;
        this.resetForm();
        console.log("Sub Product", this.subProduct);
        if (this.subProduct == "Individual") {
            this.travelType = 1;
            this.isIndividual = 'True';
            this.isFamily = 'False';
            this.isSeniorCitizen = 'False';
            this.travelForm.patchValue({ 'noOfTraveller': 1 });
            this.showTripByDefault();
            this.setTravelLocbyDefault();
        }
        else if (this.subProduct == "Family") {
            this.isMultiTrip = false;
            this.isIndividual = 'False';
            this.isFamily = 'True';
            this.isSeniorCitizen = 'False';
            this.noOfTraveller = 2;
            this.travelForm.patchValue({ 'coverageType': "FAMILY" });
            this.travelType = 2;
            this.travelForm.patchValue({ 'tripDuration': "0" });
            this.travelForm.patchValue({ 'policyStartDate': "" });
        }
        else if (this.subProduct == 'Senior Citizen') {
            this.isMultiTrip = false;
            this.isIndividual = 'False';
            this.isFamily = 'False';
            this.isSeniorCitizen = 'True';
            this.noOfTraveller = 1;
            this.travelForm.patchValue({ 'coverageType': "SENIOR" });
            this.travelType = 3;
            this.travelForm.patchValue({ 'tripDuration': "0" });
            this.travelForm.patchValue({ 'policyStartDate': "" });
        }
        else {
            console.log("Invalid SubProduct...");
        }
    };
    TravelQuotePage.prototype.seeImmigrant = function (ev) {
        console.log(ev);
        if (ev.target.textContent == 'Yes') {
            this.isImmigrant = true;
            this.travelForm.patchValue({ 'isImmigrantVisa': 'YES' });
            this.travelForm.patchValue({ 'leavingDate': '' });
            this.travelForm.patchValue({ 'returningDate': '' });
            this.travelForm.patchValue({ 'noOfTripDays': 0 });
        }
        else {
            this.isImmigrant = false;
            this.travelForm.patchValue({ 'isImmigrantVisa': 'NO' });
            this.travelForm.patchValue({ 'leavingDate': '' });
            this.travelForm.patchValue({ 'returningDate': '' });
            this.travelForm.patchValue({ 'noOfTripDays': 0 });
        }
    };
    TravelQuotePage.prototype.getMaxDate = function (ev) {
        console.log(this.isImmigrant, "Date event Called");
        var startDate = new Date(ev.target.value);
        this.minDate1 = moment(startDate).format('YYYY-MM-DD');
        if (this.travelForm.value.isImmigrantVisa == 'NO') {
            startDate.setDate(startDate.getDate() + parseInt('365'));
            this.maxDate = moment(startDate).format('YYYY-MM-DD');
        }
        else {
            startDate.setDate(startDate.getDate() + parseInt('89'));
            this.maxDate = moment(startDate).format('YYYY-MM-DD');
        }
    };
    TravelQuotePage.prototype.changeTrip = function (ev) {
        console.log("Trip Event", ev.target.id);
        if (ev.target.id == 'triptypeRound') {
            this.tripType = "Round Trip";
            this.isMultiTrip = false;
            this.IsAnnualMultiTrip = 'No';
            this.travelForm.patchValue({ 'tripType': false });
            this.travelForm.patchValue({ 'tripDuration': "0" });
            this.travelForm.patchValue({ 'policyStartDate': "" });
            this.travelForm.patchValue({ 'coverageType': "INDIVIDUAL" });
            this.travelForm.patchValue({ 'traveller2DOB': "" });
            this.travelForm.patchValue({ 'traveller3DOB': "" });
            this.travelForm.patchValue({ 'traveller4DOB': "" });
            this.travelForm.patchValue({ 'traveller5DOB': "" });
            this.travelForm.patchValue({ 'traveller6DOB': "" });
        }
        else {
            this.tripType = "Multi Trip";
            this.isMultiTrip = true;
            this.IsAnnualMultiTrip = 'Yes';
            this.isVisitingUSCanada = 'Yes';
            this.isVisitingSCHENGEN = 'No';
            this.isVisitingOtherCountries = 'No';
            this.travelForm.patchValue({ 'tripType': true });
            this.travelForm.patchValue({ 'coverageType': "SINGLE" });
        }
    };
    TravelQuotePage.prototype.showTripByDefault = function () {
        this.tripType = "Round Trip";
        this.isMultiTrip = false;
        this.IsAnnualMultiTrip = 'No';
        this.travelForm.patchValue({ 'tripType': false });
        this.travelForm.patchValue({ 'tripDuration': "0" });
        this.travelForm.patchValue({ 'policyStartDate': "" });
        this.travelForm.patchValue({ 'coverageType': "INDIVIDUAL" });
        this.travelForm.patchValue({ 'traveller2DOB': "" });
        this.travelForm.patchValue({ 'traveller3DOB': "" });
        this.travelForm.patchValue({ 'traveller4DOB': "" });
        this.travelForm.patchValue({ 'traveller5DOB': "" });
        this.travelForm.patchValue({ 'traveller6DOB': "" });
    };
    TravelQuotePage.prototype.selectStartDate = function (ev) {
        if (this.tripType == "Multi Trip") {
            this.travelForm.patchValue({ 'leavingDate': this.travelForm.value.policyStartDate });
            this.travelForm.patchValue({ 'returningDate': '' });
            this.travelForm.patchValue({ 'isImmigrantVisa': "" });
            this.travelForm.patchValue({ 'noOfTripDays': "" });
        }
    };
    TravelQuotePage.prototype.getTravelLoc = function (ev) {
        console.log("Location", ev);
        if (this.tripType == "Round Trip" && ev == 'USAorCANADA') {
            this.travelForm.patchValue({ 'travelLoc': 'USA or Canada' });
            this.isVisitingUSCanada = 'Yes';
            this.isVisitingSCHENGEN = 'No';
            this.isVisitingOtherCountries = 'No';
        }
        else if (this.tripType == "Round Trip" && ev == 'SCHENGEN') {
            this.travelForm.patchValue({ 'travelLoc': 'Schengen Countries' });
            this.isVisitingUSCanada = 'No';
            this.isVisitingSCHENGEN = 'Yes';
            this.isVisitingOtherCountries = 'No';
        }
        else if (this.tripType == "Round Trip" && ev == 'OTHER') {
            this.travelForm.patchValue({ 'travelLoc': 'Other Countries' });
            this.isVisitingUSCanada = 'No';
            this.isVisitingSCHENGEN = 'No';
            this.isVisitingOtherCountries = 'Yes';
        }
    };
    TravelQuotePage.prototype.setTravelLocbyDefault = function () {
        this.travelForm.patchValue({ 'travelLoc': 'Schengen Countries' });
        this.isVisitingUSCanada = 'No';
        this.isVisitingSCHENGEN = 'Yes';
        this.isVisitingOtherCountries = 'No';
    };
    TravelQuotePage.prototype.getReturnDate = function (ev) {
        console.log(ev.target.value, this.travelForm.value.isImmigrantVisa);
        this.leaveDate = moment(this.travelForm.value.leavingDate).format('DD-MMMM-YYYY');
        this.returnDate = moment(ev.target.value).format('DD-MMMM-YYYY');
        var difference = new Date(this.returnDate).getTime() - new Date(this.leaveDate).getTime();
        this.noOfDays = (difference / (24 * 3600 * 1000)) + 1;
        this.travelForm.patchValue({ 'noOfTripDays': this.noOfDays });
        this.getTravelPlan();
    };
    TravelQuotePage.prototype.getPolStartDate = function (ev) {
        console.log(ev.detail.value);
        var polStartDate = moment(ev.detail.value).format('DD-MMMM-YYYY');
        this.travelForm.patchValue({ 'policyStartDate': polStartDate });
        this.getTravelPlan();
    };
    TravelQuotePage.prototype.selectAddOn = function (ev) {
        console.log(ev.target.id);
        if (ev.target.id == 'semiProSports') {
            this.addOn = "Semi Profetional Sports";
            this.isProfessional = 'True';
        }
        else {
            this.addOn = "Advanture Sports";
            this.isAdvanture = 'True';
        }
    };
    TravelQuotePage.prototype.toggleAddOn = function (ev) {
        console.log("Toggle", ev);
        this.showAddOn = !this.showAddOn;
    };
    TravelQuotePage.prototype.getTravelPlan = function () {
        var _this = this;
        console.log(this.travelForm.value);
        this.inputLoadingPlan = true;
        var startDate;
        if (this.travelForm.value.policyStartDate) {
            startDate = moment(this.travelForm.value.policyStartDate).format('DD-MM-YYYY');
        }
        else {
            startDate = "";
        }
        var body = {
            "GeoCode": "WORLDWIDE",
            "isMultiTrip": this.travelForm.value.tripType,
            "LeavingIndiaDate": moment(this.travelForm.value.leavingDate).format('DD-MM-YYYY'),
            "PolicyStartDate": startDate,
            "TravelType": this.travelType,
            "NoOfTravellers": this.travelForm.value.noOfTraveller,
            "CoverNeededFloater": false,
            "IndividualPlan": true,
            "FamilyPlan": false
        };
        var str = JSON.stringify(body);
        console.log(str);
        this.cs.postWithParams('/api/Travel/GetInternationalTravelPlan', str).then(function (res) {
            console.log("Response", res);
            if (res.StatuMessage == "Success") {
                console.log("Response", res.lstTravelPlans.length);
                if (res.lstTravelPlans.length) {
                    _this.plans = [];
                    res.lstTravelPlans.forEach(function (plan) {
                        _this.plans.push(plan);
                    });
                    _this.inputLoadingPlan = false;
                }
                else {
                    _this.presentToast();
                    _this.inputLoadingPlan = false;
                }
            }
        });
    };
    TravelQuotePage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: 'No plan available for ' + this.noOfTraveller + ' members',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    TravelQuotePage.prototype.planValidateBaseNoofTraveller = function () {
        if (this.travelForm.value.policyStartDate) {
            this.getTravelPlan();
        }
        else {
            this.commonToast('Please Select Policy Start Date');
        }
    };
    TravelQuotePage.prototype.noOfTravellerChange = function (ev) {
        console.log(ev.target.value);
        this.noOfTraveller = ev.target.value;
        if (ev.target.value == 1) {
            this.travelForm.patchValue({ 'noOfTraveller': '1' });
            this.planValidateBaseNoofTraveller();
            this.traveller2 = false;
            this.traveller3 = false;
            this.traveller4 = false;
            this.traveller5 = false;
            this.traveller6 = false;
            this.travelForm.patchValue({ 'traveller2DOB': "" });
            this.travelForm.patchValue({ 'traveller3DOB': "" });
            this.travelForm.patchValue({ 'traveller4DOB': "" });
            this.travelForm.patchValue({ 'traveller5DOB': "" });
            this.travelForm.patchValue({ 'traveller6DOB': "" });
        }
        else if (ev.target.value == 2) {
            this.travelForm.patchValue({ 'noOfTraveller': '2' });
            this.planValidateBaseNoofTraveller();
            this.traveller2 = true;
            this.traveller3 = false;
            this.traveller4 = false;
            this.traveller5 = false;
            this.traveller6 = false;
            this.travelForm.patchValue({ 'traveller3DOB': "" });
            this.travelForm.patchValue({ 'traveller4DOB': "" });
            this.travelForm.patchValue({ 'traveller5DOB': "" });
            this.travelForm.patchValue({ 'traveller6DOB': "" });
        }
        else if (ev.target.value == 3) {
            this.travelForm.patchValue({ 'noOfTraveller': '3' });
            this.planValidateBaseNoofTraveller();
            this.traveller2 = true;
            this.traveller3 = true;
            this.traveller4 = false;
            this.traveller5 = false;
            this.traveller6 = false;
            this.travelForm.patchValue({ 'traveller4DOB': "" });
            this.travelForm.patchValue({ 'traveller5DOB': "" });
            this.travelForm.patchValue({ 'traveller6DOB': "" });
        }
        else if (ev.target.value == 4) {
            this.travelForm.patchValue({ 'noOfTraveller': '4' });
            this.planValidateBaseNoofTraveller();
            this.traveller2 = true;
            this.traveller3 = true;
            this.traveller4 = true;
            this.traveller5 = false;
            this.traveller6 = false;
            this.travelForm.patchValue({ 'traveller5DOB': "" });
            this.travelForm.patchValue({ 'traveller6DOB': "" });
        }
        else if (ev.target.value == 5) {
            this.travelForm.patchValue({ 'noOfTraveller': '5' });
            this.planValidateBaseNoofTraveller();
            this.traveller2 = true;
            this.traveller3 = true;
            this.traveller4 = true;
            this.traveller5 = true;
            this.traveller6 = false;
            this.travelForm.patchValue({ 'traveller6DOB': "" });
        }
        else {
            this.travelForm.patchValue({ 'noOfTraveller': '6' });
            this.planValidateBaseNoofTraveller();
            this.traveller2 = true;
            this.traveller3 = true;
            this.traveller4 = true;
            this.traveller5 = true;
            this.traveller6 = true;
        }
        if (this.subProduct == 'Family' || this.subProduct == 'Individual') {
            this.getTravelPlan();
        }
    };
    TravelQuotePage.prototype.getReturningDate = function (ev) {
        console.log(this.travelForm.value.isImmigrantVisa);
        if (this.travelForm.value.isImmigrantVisa == 'YES') {
            console.log('YES', this.travelForm.value.noOfTripDays);
            if (this.travelForm.value.noOfTripDays < 91) {
                console.log('OK');
                var newDate = new Date(this.travelForm.value.leavingDate);
                newDate.setDate(newDate.getDate() + parseInt(ev.target.value));
                var returnDate = moment(newDate).format('YYYY-MM-DD');
                this.travelForm.patchValue({ 'returningDate': returnDate });
                this.getTravelPlan();
            }
            else {
                console.log('NOT OK');
                this.commonToast('Kindly Select days below 90');
                this.travelForm.patchValue({ 'noOfTripDays': 0 });
            }
        }
        else {
            console.log('NO', this.travelForm.value.noOfTripDays);
            var newDate = new Date(this.travelForm.value.leavingDate);
            newDate.setDate(newDate.getDate() + parseInt(ev.target.value));
            var returnDate = moment(newDate).format('YYYY-MM-DD');
            this.travelForm.patchValue({ 'returningDate': returnDate });
            this.getTravelPlan();
        }
    };
    TravelQuotePage.prototype.doChange = function (ev) {
        console.log("Event", ev.target.checked, ev.target.id);
    };
    TravelQuotePage.prototype.getPlanDetail = function (ev) {
        console.log(ev.target.value);
        var plan = ev.target.value;
        var selectedPlan = this.plans.find(function (x) { return x.PlanName == plan; });
        console.log(selectedPlan);
        this.planType = selectedPlan.PlanType;
        this.planId = selectedPlan.PlanCode;
        localStorage.setItem('travelPlan', JSON.stringify(selectedPlan));
    };
    TravelQuotePage.prototype.validateDate = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            if (form.value.leavingDate != "") {
                _this.leavingDate = moment(form.value.leavingDate).format('DD-MMM-YYYY');
                if (form.value.returningDate != "") {
                    _this.returningDate = moment(form.value.returningDate).format('DD-MMM-YYYY');
                }
                else {
                    _this.returningDate = "";
                }
            }
            else {
                _this.leavingDate = "";
            }
            resolve();
        });
    };
    TravelQuotePage.prototype.validateForKerala = function (form) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log(_this.isKerala == true, form);
            if (_this.isKerala == true && form.value.isKeralaGST == '') {
                _this.validForm = false;
                resolve();
            }
            else {
                _this.validForm = true;
                resolve();
            }
        });
    };
    TravelQuotePage.prototype.calculateQuote = function (form) {
        var _this = this;
        console.log(form);
        this.categoryValidation().then(function () {
            if (form.value.subProduct == null || form.value.subProduct == undefined) {
                console.log("Invalid Subproduct");
                _this.commonToast("Please Select Sub-Product");
                if (form.value.tripType == null || form.value.subProduct == undefined) {
                    console.log("Invalid Trip Type");
                    _this.commonToast("Please Select Trip Type");
                }
            }
            else {
                if (_this.travelForm.status == 'VALID') {
                    _this.validateDate(form).then(function () {
                        _this.validateForKerala(form).then(function () {
                            console.log(_this.validForm);
                            _this.cs.showLoader = true;
                            if (_this.validForm) {
                                console.log(_this.travelForm.status);
                                var body = {
                                    "UserType": "AGENT",
                                    "ipaddress": config.ipAddress,
                                    "IsResIndia": "Yes",
                                    "IsPlanUpgrade": "No",
                                    "NoOfTravelers": form.value.noOfTraveller,
                                    "IsImmigrantVisa": form.value.isImmigrantVisa,
                                    "IsAnnualMultiTrip": _this.IsAnnualMultiTrip,
                                    "MaximumDurationPerTrip": form.value.tripDuration,
                                    "coverageType": form.value.coverageType,
                                    "planType": _this.planType,
                                    "IsVisitingUSCanadaYes": _this.isVisitingUSCanada,
                                    "IsVisitingSCHENGENYes": _this.isVisitingSCHENGEN,
                                    "IsVisitingOtherCountries": _this.isVisitingOtherCountries,
                                    "LeavingIndia": _this.leavingDate,
                                    "ReturnIndia": _this.returningDate,
                                    "TripDuration": form.value.noOfTripDays,
                                    "IsIndividualPlan": _this.isIndividual,
                                    "IsFamilyPlan": _this.isFamily,
                                    "IsCoverNeededIndividual": "False",
                                    "IsCoverNeededFloater": "False",
                                    "Members": [
                                        {
                                            "DateOfBirth": moment(form.value.traveller1DOB).format('DD-MMM-YYYY'),
                                            "PlanID": _this.planId
                                        },
                                        {
                                            "DateOfBirth": moment(form.value.traveller2DOB).format('DD-MMM-YYYY'),
                                            "PlanID": _this.planId
                                        },
                                        {
                                            "DateOfBirth": moment(form.value.traveller3DOB).format('DD-MMM-YYYY'),
                                            "PlanID": _this.planId
                                        },
                                        {
                                            "DateOfBirth": moment(form.value.traveller4DOB).format('DD-MMM-YYYY'),
                                            "PlanID": _this.planId
                                        },
                                        {
                                            "DateOfBirth": moment(form.value.traveller5DOB).format('DD-MMM-YYYY'),
                                            "PlanID": _this.planId
                                        },
                                        {
                                            "DateOfBirth": moment(form.value.traveller6DOB).format('DD-MMM-YYYY'),
                                            "PlanID": _this.planId
                                        }
                                    ],
                                    "planIds": _this.planId + ',',
                                    "IsProfessionalSportCover": _this.isProfessional,
                                    "IsAdventureSportCover": _this.isAdvanture,
                                    "GSTStateCode": _this.stateDetails.GSTStateCode,
                                    "GSTStateName": form.value.state,
                                    "isGSTRegistered": form.value.isKeralaGST
                                };
                                var body1 = Object.assign(body);
                                console.log(body1);
                                var str = JSON.stringify(body1);
                                localStorage.setItem('quoteRequest', str);
                                console.log(str);
                                _this.cs.postWithParams('/api/Travel/ITPFCalculatePremium', str).then(function (res) {
                                    console.log("Response", res);
                                    if (res.StatusCode == '1') {
                                        _this.isQuoteGenerated = true;
                                        localStorage.setItem('quoteResponse', JSON.stringify(res));
                                        _this.getData();
                                        _this.cs.showLoader = false;
                                    }
                                    else {
                                        console.log("Failed To calculate quate");
                                        _this.commonToast('Failed To calculate quate');
                                        _this.cs.showLoader = false;
                                    }
                                }).catch(function (err) {
                                    console.log(err);
                                    _this.cs.showLoader = false;
                                });
                            }
                            else {
                                _this.commonToast('Kindly select GST details');
                                _this.cs.showLoader = false;
                            }
                        });
                    });
                }
                else {
                    console.log("Invalid Form");
                    _this.commonToast("Please filled all data");
                    _this.cs.showLoader = false;
                }
            }
        });
    };
    TravelQuotePage.prototype.commonToast = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 5000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    TravelQuotePage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    TravelQuotePage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.request = JSON.parse(localStorage.getItem('quoteRequest'));
            _this.response = JSON.parse(localStorage.getItem('quoteResponse'));
            _this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
            console.log(_this.request);
            console.log(_this.response);
            console.log(_this.planDetails);
            resolve();
        });
    };
    TravelQuotePage.prototype.convertToPolicy = function (ev) {
        console.log("Hello");
        this.router.navigateByUrl('travel-proposal');
    };
    TravelQuotePage.prototype.showQuoteDetails = function (ev) {
        this.isQuoteDetails = !this.isQuoteDetails;
        if (this.isQuoteDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("quoteDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("quoteDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    TravelQuotePage.prototype.showPremiumDetails = function (ev) {
        this.isPremiumDetails = !this.isPremiumDetails;
        if (this.isPremiumDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("premiumDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("premiumDetails").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("premiumDetails").style.background = "url(" + imgUrl + ")";
            document.getElementById("premiumDetails").style.backgroundRepeat = 'no-repeat';
        }
    };
    TravelQuotePage.prototype.Recalculate = function () {
        this.calculateQuote(this.travelForm);
    };
    TravelQuotePage = tslib_1.__decorate([
        Component({
            selector: 'app-travel-quote',
            templateUrl: './travel-quote.page.html',
            styleUrls: ['./travel-quote.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ActivatedRoute, CommonService, FormBuilder, Location, Router, XmlToJsonService, ToastController])
    ], TravelQuotePage);
    return TravelQuotePage;
}());
export { TravelQuotePage };
//# sourceMappingURL=travel-quote.page.js.map