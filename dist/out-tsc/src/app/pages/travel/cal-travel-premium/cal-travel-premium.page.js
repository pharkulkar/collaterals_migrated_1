import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { ToastController } from '@ionic/angular';
import { config } from 'src/app/config.properties';
var CalTravelPremiumPage = /** @class */ (function () {
    function CalTravelPremiumPage(cs, xml2JsonService, toastCtrl) {
        this.cs = cs;
        this.xml2JsonService = xml2JsonService;
        this.toastCtrl = toastCtrl;
        this.customPopoverOptions = {};
        this.showAddOn = false;
        this.noOfTraveller = 1;
        this.isMultiTrip = false;
        this.isFamilyCover = false;
        this.traveller1 = true;
        this.traveller2 = false;
        this.traveller3 = false;
        this.traveller4 = false;
        this.traveller5 = false;
        this.traveller6 = false;
        this.plans = [];
        this.isImmigrant = false;
    }
    CalTravelPremiumPage.prototype.ngOnInit = function () {
        this.createTravelForm();
        var date = new Date();
        this.minDate = moment(date).format('YYYY-MM-DD');
        console.log(this.minDate);
    };
    CalTravelPremiumPage.prototype.createTravelForm = function () {
        this.travelForm = new FormGroup({
            productName: new FormControl('internationalTravel'),
            subProduct: new FormControl(), tripType: new FormControl(), travelLoc: new FormControl(),
            isImmigrantVisa: new FormControl(), leavingDate: new FormControl(), returningDate: new FormControl(),
            noOfTripDays: new FormControl(), coverageType: new FormControl(), coverNeeded: new FormControl(),
            noOfTraveller: new FormControl(), traveller1DOB: new FormControl(), traveller2DOB: new FormControl(),
            traveller3DOB: new FormControl(), traveller4DOB: new FormControl(), traveller5DOB: new FormControl(),
            traveller6DOB: new FormControl(), dateDiff: new FormControl(), plan: new FormControl(),
            tripDuration: new FormControl(), policyStartDate: new FormControl(), state: new FormControl()
        });
    };
    CalTravelPremiumPage.prototype.getCover = function (ev) {
        console.log(ev.target.innerText);
        if (ev.target.innerText == 'Individual Plan') {
            this.isFamilyCover = false;
        }
        else {
            this.isFamilyCover = true;
        }
    };
    CalTravelPremiumPage.prototype.changeSubProduct = function (ev) {
        console.log("Event", ev.target.value);
        this.subProduct = ev.target.value;
        console.log("Sub Product", this.subProduct);
        if (this.subProduct == "Individual") {
            this.travelType = 1;
            this.travelForm.patchValue({ 'noOfTraveller': 1 });
        }
        else if (this.subProduct == "Family") {
            this.isMultiTrip = false;
            this.noOfTraveller = 2;
            this.travelType = 2;
        }
        else if (this.subProduct == 'Senior Citizen') {
            this.isMultiTrip = false;
            this.noOfTraveller = 1;
            this.travelType = 3;
        }
        else {
            console.log("Invalid SubProduct...");
        }
    };
    CalTravelPremiumPage.prototype.seeImmigrant = function (ev) {
        console.log(ev);
        if (ev.target.textContent == 'Yes') {
            this.isImmigrant = true;
            this.travelForm.patchValue({ 'isImmigrantVisa': 'YES' });
        }
        else {
            this.isImmigrant = false;
            this.travelForm.patchValue({ 'isImmigrantVisa': 'NO' });
        }
    };
    CalTravelPremiumPage.prototype.changeTrip = function (ev) {
        console.log("Event", ev);
        this.tripType = ev.target.outerText;
        if (ev.target.outerText == "Round Trip") {
            this.isMultiTrip = false;
            this.IsAnnualMultiTrip = 'No';
            this.travelForm.patchValue({ 'tripType': false });
            this.travelForm.patchValue({ 'policyStartDate': "" });
        }
        else {
            this.isMultiTrip = true;
            this.IsAnnualMultiTrip = 'Yes';
            this.isVisitingUSCanada = 'Yes';
            this.isVisitingSCHENGEN = 'No';
            this.isVisitingOtherCountries = 'No';
            this.travelForm.patchValue({ 'tripType': true });
            this.travelForm.patchValue({ 'leavingDate': "" });
        }
    };
    CalTravelPremiumPage.prototype.getTravelLoc = function (ev) {
        console.log("Location", ev.target.checked, ev.target.id);
        if (this.tripType == "Round Trip" && ev.target.checked == true && ev.target.id == 'USAorCANADA') {
            this.isVisitingUSCanada = 'Yes';
            this.isVisitingSCHENGEN = 'No';
            this.isVisitingOtherCountries = 'No';
        }
        else if (this.tripType == "Round Trip" && ev.target.checked == true && ev.target.id == 'SCHENGEN') {
            this.isVisitingUSCanada = 'No';
            this.isVisitingSCHENGEN = 'Yes';
            this.isVisitingOtherCountries = 'No';
        }
        else if (this.tripType == "Round Trip" && ev.target.checked == true && ev.target.id == 'OTHER') {
            this.isVisitingUSCanada = 'No';
            this.isVisitingSCHENGEN = 'No';
            this.isVisitingOtherCountries = 'Yes';
        }
    };
    CalTravelPremiumPage.prototype.getReturnDate = function (ev) {
        this.leaveDate = moment(this.travelForm.value.leavingDate).format('DD-MMMM-YYYY');
        this.returnDate = moment(ev.target.value).format('DD-MMMM-YYYY');
        var difference = new Date(this.returnDate).getTime() - new Date(this.leaveDate).getTime();
        this.noOfDays = (difference / (24 * 3600 * 1000)) + 1;
        console.log(this.noOfDays);
        this.travelForm.patchValue({ 'dateDiff': this.noOfDays });
        this.getTravelPlan();
    };
    CalTravelPremiumPage.prototype.getPolStartDate = function (ev) {
        console.log(ev.detail.value);
        var polStartDate = moment(ev.detail.value).format('DD-MMMM-YYYY');
        this.travelForm.patchValue({ 'policyStartDate': polStartDate });
        this.getTravelPlan();
    };
    CalTravelPremiumPage.prototype.toggleAddOn = function (ev) {
        console.log(ev);
        this.showAddOn = !this.showAddOn;
    };
    CalTravelPremiumPage.prototype.getTravelPlan = function () {
        var _this = this;
        console.log(this.travelForm.value);
        var body = {
            "GeoCode": "WORLDWIDE",
            "isMultiTrip": this.travelForm.value.tripType,
            "LeavingIndiaDate": moment(this.travelForm.value.leavingDate).format('DD-MM-YYYY'),
            "PolicyStartDate": moment(this.travelForm.value.policyStartDate).format('DD-MM-YYYY'),
            "TravelType": this.travelType,
            "NoOfTravellers": this.travelForm.value.noOfTraveller,
            "CoverNeededFloater": false,
            "IndividualPlan": true,
            "FamilyPlan": false
        };
        var str = JSON.stringify(body);
        console.log(str);
        this.cs.postWithParams('/api/Travel/GetInternationalTravelPlan', str).then(function (res) {
            console.log("Response", res);
            if (res.StatuMessage == "Success") {
                console.log("Response", res.lstTravelPlans.length);
                if (res.lstTravelPlans.length) {
                    _this.plans = [];
                    res.lstTravelPlans.forEach(function (plan) {
                        _this.plans.push(plan);
                    });
                }
                else {
                    _this.presentToast();
                }
            }
        });
    };
    CalTravelPremiumPage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: 'No plan available for ' + this.noOfTraveller + ' members',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CalTravelPremiumPage.prototype.noOfTravellerChange = function (ev) {
        console.log(ev.target.value);
        this.noOfTraveller = ev.target.value;
        if (ev.target.value == 1) {
            this.travelForm.patchValue({ 'noOfTraveller': '1' });
            this.traveller2 = false;
            this.traveller3 = false;
            this.traveller4 = false;
            this.traveller5 = false;
            this.traveller6 = false;
        }
        else if (ev.target.value == 2) {
            this.travelForm.patchValue({ 'noOfTraveller': '2' });
            this.traveller2 = true;
            this.traveller3 = false;
            this.traveller4 = false;
            this.traveller5 = false;
            this.traveller6 = false;
        }
        else if (ev.target.value == 3) {
            this.travelForm.patchValue({ 'noOfTraveller': '3' });
            this.traveller2 = true;
            this.traveller3 = true;
            this.traveller4 = false;
            this.traveller5 = false;
            this.traveller6 = false;
        }
        else if (ev.target.value == 4) {
            this.travelForm.patchValue({ 'noOfTraveller': '4' });
            this.traveller2 = true;
            this.traveller3 = true;
            this.traveller4 = true;
            this.traveller5 = false;
            this.traveller6 = false;
        }
        else if (ev.target.value == 5) {
            this.travelForm.patchValue({ 'noOfTraveller': '5' });
            this.traveller2 = true;
            this.traveller3 = true;
            this.traveller4 = true;
            this.traveller5 = true;
            this.traveller6 = false;
        }
        else {
            this.travelForm.patchValue({ 'noOfTraveller': '6' });
            this.traveller2 = true;
            this.traveller3 = true;
            this.traveller4 = true;
            this.traveller5 = true;
            this.traveller6 = true;
        }
        if (this.subProduct == 'Individual') {
            this.getTravelPlan();
        }
    };
    CalTravelPremiumPage.prototype.traveller1DOB = function (ev) {
        var traveller1DOB = moment(ev.target.value).format('DD-MMM-YYYY');
        this.travelForm.patchValue({ 'traveller1DOB': traveller1DOB });
    };
    CalTravelPremiumPage.prototype.doChange = function (ev) {
        console.log("Event", ev.target.checked, ev.target.id);
    };
    CalTravelPremiumPage.prototype.getPlanDetail = function (ev) {
        console.log(ev.target.value);
        var plan = ev.target.value;
        var selectedPlan = this.plans.find(function (x) { return x.PlanName == plan; });
        console.log(selectedPlan);
        this.planType = selectedPlan.PlanType;
        this.planId = selectedPlan.PlanCode;
    };
    CalTravelPremiumPage.prototype.calculateQuote = function (form) {
        var _this = this;
        console.log(form);
        var xmlRequest = "<Request>\n    <UserID>NTkyNDYwMF9FWUZkKzE5RnlndFhJUHJNY0gxaVhJZ1Jxam5qSktXRW95c1dRYmZLQmhNPQ==</UserID>\n    <UserType>AGENT</UserType>\n    <ipaddress>" + config.ipAddress + "</ipaddress>\n    <IsResIndia>Yes</IsResIndia>\n    <PolicyStartDate></PolicyStartDate>\n    <PolicyEndDate></PolicyEndDate>\n    <IsPlanUpgrade>No</IsPlanUpgrade>\n    <email></email>\n    <telno></telno>\n    <NoOfTravelers>" + form.value.noOfTraveller + "</NoOfTravelers>\n    <IsImmigrantVisa>" + form.value.isImmigrantVisa + "</IsImmigrantVisa>\n    <IsAnnualMultiTrip>" + this.IsAnnualMultiTrip + "</IsAnnualMultiTrip>\n    <MaximumDurationPerTrip>0</MaximumDurationPerTrip>\n    <coverageType>INDIVIDUAL</coverageType>\n    <planType>" + this.planType + "</planType>\n    <IsVisitingUSCanadaYes>" + this.isVisitingUSCanada + "</IsVisitingUSCanadaYes>\n    <IsVisitingSCHENGENYes>" + this.isVisitingSCHENGEN + "</IsVisitingSCHENGENYes>\n    <IsVisitingOtherCountries>" + this.isVisitingOtherCountries + "</IsVisitingOtherCountries>\n    <LeavingIndia>" + moment(form.value.leavingDate).format('DD-MMM-YYYY') + "</LeavingIndia>\n    <ReturnIndia>" + moment(form.value.returningDate).format('DD-MMM-YYYY') + "</ReturnIndia>\n    <TripDuration>" + this.noOfDays + "</TripDuration>\n    <IsIndividualPlan>True</IsIndividualPlan>\n    <IsFamilyPlan>False</IsFamilyPlan>\n    <IsCoverNeededIndividual>False</IsCoverNeededIndividual>\n    <IsCoverNeededFloater>False</IsCoverNeededFloater>\n    <Travellers>\n        <Member>\n            <DateOfBirth>" + form.value.traveller1DOB + "</DateOfBirth>\n            <PlanID>" + this.planId + "</PlanID>\n        </Member>\n    </Travellers>\n    <planIds>" + this.planId + ",</planIds>\n    <IsProfessionalSportCover>false</IsProfessionalSportCover>\n    <IsAdventureSportCover>false</IsAdventureSportCover>\n    <GSTStateCode>55</GSTStateCode>\n    <GSTStateName><![CDATA[MAHARASHTRA]]></GSTStateName>\n</Request>";
        // let xmlRequest =  `<Request><UserID>NTkyNDYwMF9FWUZkKzE5RnlndFhJUHJNY0gxaVhJZ1Jxam5qSktXRW95c1dRYmZLQmhNPQ==</UserID><UserType>AGENT</UserType><ipaddress>MOBILE-ANDROID</ipaddress><IsResIndia>Yes</IsResIndia><PolicyStartDate /><PolicyEndDate /><IsPlanUpgrade>No</IsPlanUpgrade><email></email><telno></telno><NoOfTravelers>1</NoOfTravelers><IsImmigrantVisa>NO</IsImmigrantVisa><IsAnnualMultiTrip>No</IsAnnualMultiTrip><MaximumDurationPerTrip>0</MaximumDurationPerTrip><coverageType>INDIVIDUAL</coverageType><planType>SINGLE</planType><IsVisitingUSCanadaYes>Yes</IsVisitingUSCanadaYes><IsVisitingSCHENGENYes>No</IsVisitingSCHENGENYes><IsVisitingOtherCountries>No</IsVisitingOtherCountries><LeavingIndia>07-May-2019</LeavingIndia><ReturnIndia>09-May-2019</ReturnIndia><TripDuration>3</TripDuration><IsIndividualPlan>True</IsIndividualPlan><IsFamilyPlan>False</IsFamilyPlan><IsCoverNeededIndividual>False</IsCoverNeededIndividual><IsCoverNeededFloater>False</IsCoverNeededFloater><Travellers><Member><DateOfBirth>07-May-1993</DateOfBirth><PlanID>60888</PlanID></Member></Travellers><planIds>60888,</planIds><IsProfessionalSportCover>false</IsProfessionalSportCover><IsAdventureSportCover>false</IsAdventureSportCover><GSTStateCode>55</GSTStateCode><GSTStateName><![CDATA[MAHARASHTRA]]></GSTStateName></Request>`
        console.log(xmlRequest);
        this.cs.postTravel('/ITPFCalculatePremium/MzU1NTI3', xmlRequest).then(function (res) {
            console.log(res);
        }).catch(function (err) {
            _this.travelQuate = _this.xml2JsonService.xmlToJson(err.error.text);
            console.log(_this.travelQuate);
        });
    };
    CalTravelPremiumPage = tslib_1.__decorate([
        Component({
            selector: 'app-cal-travel-premium',
            templateUrl: './cal-travel-premium.page.html',
            styleUrls: ['./cal-travel-premium.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, XmlToJsonService, ToastController])
    ], CalTravelPremiumPage);
    return CalTravelPremiumPage;
}());
export { CalTravelPremiumPage };
//# sourceMappingURL=cal-travel-premium.page.js.map