import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DashBoardPage } from './dash-board.page';
var routes = [
    {
        path: '',
        component: DashBoardPage
    }
];
var DashBoardPageModule = /** @class */ (function () {
    function DashBoardPageModule() {
    }
    DashBoardPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [DashBoardPage]
        })
    ], DashBoardPageModule);
    return DashBoardPageModule;
}());
export { DashBoardPageModule };
//# sourceMappingURL=dash-board.module.js.map