import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
var HealthSummaryPage = /** @class */ (function () {
    function HealthSummaryPage(commonservice, alertController, xml2json, router) {
        this.commonservice = commonservice;
        this.alertController = alertController;
        this.xml2json = xml2json;
        this.router = router;
        this.isShowPlanDetails = false;
        this.isShowApplicantDetails = false;
        this.isShowGSTDetails = false;
        this.isShowMedicalDetails = true;
        // RequestData is stored inside below property 
        this.requestData = '<CustomerDetails><UserID>NTkyNDYwMF9FWUZkKzE5RnlndFhJUHJNY0gxaVhJZ1Jxam5qSktXRW95c1dRYmZLQmhNPQ==</UserID><AnnualIncomeText></AnnualIncomeText><AnnualIncomeValue>-1</AnnualIncomeValue><CustomerId>0</CustomerId><DateOfBirth>10-May-2001</DateOfBirth><EmailAddress><![CDATA[fhggg@fgg.com]]></EmailAddress><AlternateEmail/><HasAddressChanged>true</HasAddressChanged><HobbyValue>-1</HobbyValue><HobbyText></HobbyText><IdentityNumber>121212121212</IdentityNumber><IdentityProofText>121212121212</IdentityProofText><IdentityProofValue>2</IdentityProofValue><LandlineNumber/><MaritalStatusText>SINGLE</MaritalStatusText><MaritalStatusValue>1</MaritalStatusValue><AlternateMobile/><MobileNumber>8080619571</MobileNumber><Name><![CDATA[Bday jadhav]]></Name><OccupationText></OccupationText><OccupationValue>-1</OccupationValue><OwnerType>Individual</OwnerType><PANNumber>PANDA1234A</PANNumber><AadhaarNumber>121212121212</AadhaarNumber><PermanentAddrCityText><![CDATA[VASHI]]></PermanentAddrCityText><PermanentAddrCityValue>210</PermanentAddrCityValue><PermanentAddrLandmark><![CDATA[Fff]]></PermanentAddrLandmark><PermanentAddrLine1><![CDATA[Ggh]]></PermanentAddrLine1><PermanentAddrLine2><![CDATA[Fff]]></PermanentAddrLine2><PermanentAddrPincodeText>400703</PermanentAddrPincodeText><PermanentAddrPincodeValue>66870</PermanentAddrPincodeValue><PermanentAddrStateText><![CDATA[MAHARASHTRA]]></PermanentAddrStateText><PermanentAddrStateValue>55</PermanentAddrStateValue><PresentAddrCityText><![CDATA[VASHI]]></PresentAddrCityText><PresentAddrCityValue>210</PresentAddrCityValue><PresentAddrLandmark><![CDATA[Fff]]></PresentAddrLandmark><PresentAddrLine1><![CDATA[Ggh]]></PresentAddrLine1><PresentAddrLine2><![CDATA[Fff]]></PresentAddrLine2><PresentAddrPincodeText>400703</PresentAddrPincodeText><PresentAddrPincodeValue>66870</PresentAddrPincodeValue><PresentAddrStateText><![CDATA[MAHARASHTRA]]></PresentAddrStateText><PresentAddrStateValue>55</PresentAddrStateValue><SetAsDefault>true</SetAsDefault><TitleText>Mr.</TitleText><TitleValue>1</TitleValue><isPermanentAsPresent>false</isPermanentAsPresent><GSTApplicable>false</GSTApplicable><CustomerGSTDetails><GSTIN_NO></GSTIN_NO><CONSTITUTION_OF_BUSINESS></CONSTITUTION_OF_BUSINESS><CONSTITUTION_OF_BUSINESS_TEXT></CONSTITUTION_OF_BUSINESS_TEXT><CUSTOMER_TYPE></CUSTOMER_TYPE><CUSTOMER_TYPE_TEXT></CUSTOMER_TYPE_TEXT><PAN_NO></PAN_NO><GST_REGISTRATION_STATUS></GST_REGISTRATION_STATUS><GST_REGISTRATION_STATUS_TEXT></GST_REGISTRATION_STATUS_TEXT></CustomerGSTDetails></CustomerDetails>';
        this.isDeclarationApproved = true;
    }
    HealthSummaryPage.prototype.ngOnInit = function () {
        //this.quoteResponse = this.commonservice.responseData;
        this.convertedRequestData = this.xml2json.xmlToJson(this.requestData);
        //this.proposalResponse = this.quoteResponse.SavePolicy[0];
        localStorage.setItem('isRenewal', 'false');
        this.quoteData = JSON.parse(localStorage.getItem('quoteResponse'));
        this.quoteResponseData = this.quoteData.HealthDetails[0];
        this.quoteRequestData = JSON.parse(localStorage.getItem('quoteRequest'));
        if (this.quoteRequestData.ProductType == 'PPAP') {
            this.sumisnured = localStorage.getItem('SumInsured');
            this.ppapCovers = JSON.parse(localStorage.getItem('PPAPCovers'));
        }
        else {
            if (this.quoteRequestData.ProductType == 'CHI') {
                this.sumisnured = this.quoteRequestData.InsuredAmount;
                this.isOtherPol = localStorage.getItem('isOtherPolicyDetails');
                if ((this.quoteRequestData.AddOn1 == 'false' || this.quoteRequestData.AddOn1 == false) && (this.quoteRequestData.AddOn3 == 'false' || this.quoteRequestData.AddOn3 == false) && (this.quoteRequestData.AddOn5 == 'false' || this.quoteRequestData.AddOn3 == false) && (this.quoteRequestData.AddOn6 == 'false' || this.quoteRequestData.AddOn6 == false)) {
                    this.isAddOn = 'No';
                }
                else {
                    this.isAddOn = 'Yes';
                }
            }
            if (this.quoteRequestData.ProductType == 'HBOOSTER') {
                this.sumisnured = this.quoteRequestData.SumInsured;
                this.isOtherPol = localStorage.getItem('isOtherPolicyDetails');
                if ((this.quoteRequestData.AddOn1 == 'false' || this.quoteRequestData.AddOn1 == false) && (this.quoteRequestData.AddOn3 == 'false' || this.quoteRequestData.AddOn3 == false) && (this.quoteRequestData.AddOn2 == 'false' || this.quoteRequestData.AddOn2 == false)) {
                    this.isAddOn = 'No';
                }
                else {
                    this.isAddOn = 'Yes';
                }
            }
        }
        this.proposalRequestData = JSON.parse(localStorage.getItem('proposalRequest'));
        this.insuredData = this.proposalRequestData.Members;
        this.proposalResponseData = JSON.parse(localStorage.getItem('proposalResponse'));
        this.stateData = JSON.parse(localStorage.getItem('PermanentStateCity'));
    };
    // showPlanDetails(details: any) {
    //   this.isShowPlanDetails = !details;
    // }
    // showApplicantDetails(details: any) {
    //   this.isShowApplicantDetails = !details;
    // }
    // showGSTDetails(details: any) {
    //   this.isShowGSTDetails = !details;
    // }
    // showMedicalDetails(details: any) {
    //   this.isShowMedicalDetails = !details;
    // }
    HealthSummaryPage.prototype.getDeclarationApproval = function (event) {
        this.isDeclarationApproved = event.target.checked;
    };
    HealthSummaryPage.prototype.goToPayment = function (ev) {
        if (this.isDeclarationApproved) {
            var quoteReq = JSON.parse(localStorage.getItem('quoteRequest'));
            var quoteRes = JSON.parse(localStorage.getItem('quoteResponse'));
            var propReq = JSON.parse(localStorage.getItem('proposalRequest'));
            var propRes = JSON.parse(localStorage.getItem('proposalResponse'));
            var custReq = JSON.parse(localStorage.getItem('customerRequest'));
            var custRes = JSON.parse(localStorage.getItem('customerResponse'));
            console.log(quoteReq);
            console.log(quoteRes);
            console.log(propReq);
            console.log(propRes);
            console.log(custReq);
            console.log(custRes);
            this.router.navigateByUrl('payment');
        }
        else {
            this.presentAlert('Please accept the declaration to move on to payment.');
        }
    };
    HealthSummaryPage.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HealthSummaryPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.commonservice.goToHome();
    };
    HealthSummaryPage = tslib_1.__decorate([
        Component({
            selector: 'app-health-summary',
            templateUrl: './health-summary.page.html',
            styleUrls: ['./health-summary.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, AlertController, XmlToJsonService, Router])
    ], HealthSummaryPage);
    return HealthSummaryPage;
}());
export { HealthSummaryPage };
//# sourceMappingURL=health-summary.page.js.map