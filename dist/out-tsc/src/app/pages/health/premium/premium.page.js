import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { LoadingController, AlertController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { Location } from '@angular/common';
import { PedQuestionaireComponent } from '../ped-questionaire/ped-questionaire.component';
import { config } from 'src/app/config.properties';
var PremiumPage = /** @class */ (function () {
    function PremiumPage(route, _location, modalController, commonservice, xml2json, loadingCtrl, alertController) {
        this.route = route;
        this._location = _location;
        this.modalController = modalController;
        this.commonservice = commonservice;
        this.xml2json = xml2json;
        this.loadingCtrl = loadingCtrl;
        this.alertController = alertController;
        // TRAIL AND ERROR START
        this.isPremium = true;
        this.isHealthBooster = false;
        this.isCHI = false;
        this.isPPAP = false;
        this.requestData = '<Request><UserType>AGENT</UserType><ProductType>PPAP</ProductType><UserID>NTkyNDYwMF9FWUZkKzE5RnlndFhJUHJNY0gxaVhJZ1Jxam5qSktXRW95c1dRYmZLQmhNPQ==</UserID><ipaddress>' + config.ipAddress + '</ipaddress><SubProduct>PP</SubProduct><DOB>11-Apr-2001</DOB><Tenure>1</Tenure><IsJammuKashmir>false</IsJammuKashmir><Occupation>SELF EMPLOYED</Occupation><PlanCode>62585</PlanCode><GSTStateCode></GSTStateCode><GSTStateName>MAHARASHTRA</GSTStateName></Request>';
    }
    PremiumPage.prototype.ngOnInit = function () {
        var _this = this;
        //new code by preetam
        this.getRequest().then(function () {
            _this.getDataFromReq(_this.responseTemp);
        });
        this.responsetemp2 = '';
        console.log("Premium", this.responseTemp);
    };
    PremiumPage.prototype.getRequest = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.responseTemp = JSON.parse(localStorage.getItem('quoteRequest'));
            console.log("Preetam", _this.responseTemp);
            resolve();
        });
    };
    PremiumPage.prototype.getDataFromReq = function (data) {
        console.log(data);
        this.productType = data.ProductType;
        if (data.ProductType == 'HBOOSTER') {
            console.log("HB", this.responseTemp);
            this.isHealthBooster = true;
            this.isCHI = false;
            this.isPPAP = false;
            this.PremiumForThreeYear(1, '');
            var SoftLessCopyBool = void 0;
            this.quoteRequest = this.responseTemp;
            SoftLessCopyBool = this.responseTemp.SoftLessCopy;
            if (SoftLessCopyBool == "true") {
                this.SoftLessCopyValue = 100;
            }
            else {
                this.SoftLessCopyValue = 0;
            }
            if (this.responseTemp.ProductName == "HBOOSTER_TOPUP") {
                this.HBProductName = "Top Up Plan";
            }
            else {
                this.HBProductName = "Super Top Up Plan";
            }
        }
        else if (data.ProductType == 'CHI') {
            this.isHealthBooster = false;
            this.isCHI = true;
            this.isPPAP = false;
            console.log("CHI");
            this.quoteRequest = this.responseTemp;
            var res = JSON.parse(localStorage.getItem('quoteResponse'));
            this.quoteResponse = res;
            console.log(this.quoteResponse);
        }
        else {
            this.isHealthBooster = false;
            this.isCHI = false;
            this.isPPAP = true;
            this.quoteRequest = this.responseTemp;
            var res = JSON.parse(localStorage.getItem('quoteResponse'));
            this.quoteResponse = res;
            if (this.quoteRequest.SubProduct == "PP") {
                this.SubProductName = "Lum Sum Payment Option";
            }
            else if (this.quoteRequest.SubProduct == "PPA") {
                this.SubProductName = "Monthly Payment Option";
            }
            else if (this.quoteRequest.SubProduct == "PPC") {
                this.SubProductName = "Lump Sum Payment Option With Monthly Payout";
            }
            else {
                console.log("Error");
            }
        }
    };
    PremiumPage.prototype.ReCalculate = function () {
        //this.route.navigateByUrl('health');
        this._location.back();
        localStorage.removeItem('quoteRequest');
        localStorage.removeItem('quoteResponse');
    };
    PremiumPage.prototype.ConvertToPolicy = function (ev) {
        if (this.productType == 'PPAP') {
            this.openPEDModal();
        }
        else {
            this.route.navigateByUrl('health-proposal');
        }
    };
    PremiumPage.prototype.openPEDModal = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var gstModal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: PedQuestionaireComponent,
                            showBackdrop: false,
                            backdropDismiss: false
                        })];
                    case 1:
                        gstModal = _a.sent();
                        gstModal.onDidDismiss()
                            .then(function (data) {
                            var gstData = data['data'];
                            console.log(gstData); // Here's your selected user!
                        });
                        return [4 /*yield*/, gstModal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PremiumPage.prototype.PremiumForOneYear = function (data) {
        var _this = this;
        console.log("for 1 Year", data);
        data.YearPremium = '1';
        console.log(data);
        var req = JSON.stringify(data);
        this.showLoading();
        localStorage.setItem('Request', req);
        this.commonservice.postWithParams('/api/HBooster/SaveEditQuote', req).then(function (res) {
            console.log("New Response", res);
            if (res.StatusCode == 1) {
                console.log(res);
                _this.quoteResponse = res;
                localStorage.setItem('quoteResponse', JSON.stringify(res));
                _this.loading.dismiss();
            }
            else {
                _this.loading.dismiss();
                _this.presentAlert(res.StatusMessage);
            }
        }).catch(function (err) {
            _this.presentAlert(err);
            _this.loading.dismiss();
        });
    };
    PremiumPage.prototype.PremiumForTwoYear = function (data) {
        var _this = this;
        console.log("for 2 Year", data);
        data.YearPremium = '2';
        console.log(data);
        var req = JSON.stringify(data);
        this.showLoading();
        localStorage.setItem('Request', req);
        this.commonservice.postWithParams('/api/HBooster/SaveEditQuote', req).then(function (res) {
            console.log("New Response", res);
            if (res.StatusCode == 1) {
                console.log(res);
                _this.quoteResponse = res;
                localStorage.setItem('quoteResponse', JSON.stringify(res));
                _this.loading.dismiss();
            }
            else {
                _this.loading.dismiss();
                _this.presentAlert(res.StatusMessage);
            }
        }).catch(function (err) {
            _this.presentAlert(err);
            _this.loading.dismiss();
        });
    };
    PremiumPage.prototype.showLoading = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Loading',
                            })];
                    case 1:
                        _a.loading = _b.sent();
                        return [4 /*yield*/, this.loading.present()];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    PremiumPage.prototype.PremiumForThreeYear = function (call, data) {
        var _this = this;
        console.log("Premium");
        if (call == 1) {
            var res = JSON.parse(localStorage.getItem('quoteResponse'));
            this.quoteResponse = res;
        }
        else {
            data.YearPremium = '3';
            console.log(data);
            var req = JSON.stringify(data);
            this.showLoading();
            localStorage.setItem('Request', req);
            this.commonservice.postWithParams('/api/HBooster/SaveEditQuote', req).then(function (res) {
                console.log("New Response", res);
                if (res.StatusCode == 1) {
                    console.log(res);
                    _this.quoteResponse = res;
                    localStorage.setItem('quoteResponse', JSON.stringify(res));
                    _this.loading.dismiss();
                }
                else {
                    _this.loading.dismiss();
                    _this.presentAlert(res.StatusMessage);
                }
            }).catch(function (err) {
                _this.presentAlert(err);
                _this.loading.dismiss();
            });
        }
        console.log(this.quoteResponse);
    };
    // showPremium() {
    //   this.showLoading();
    //   this.isPremium = !this.isPremium;
    // }
    PremiumPage.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PremiumPage.prototype.getHBData = function (year) {
        this.hbyearwiseData = year;
        if (year == 1) {
            this.PremiumForOneYear(this.responseTemp);
        }
        else if (year == 2) {
            this.PremiumForTwoYear(this.responseTemp);
        }
        else if (year == 3) {
            this.PremiumForThreeYear(2, this.responseTemp);
        }
    };
    PremiumPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.commonservice.goToHome();
    };
    PremiumPage = tslib_1.__decorate([
        Component({
            selector: 'app-premium',
            templateUrl: './premium.page.html',
            styleUrls: ['./premium.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router, Location, ModalController, CommonService, XmlToJsonService, LoadingController, AlertController])
    ], PremiumPage);
    return PremiumPage;
}());
export { PremiumPage };
//# sourceMappingURL=premium.page.js.map