import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HealthProposalPage } from './health-proposal.page';
import { SearchCustComponent } from '../search-cust/search-cust.component';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
var routes = [
    {
        path: '',
        component: HealthProposalPage
    }
];
var HealthProposalPageModule = /** @class */ (function () {
    function HealthProposalPageModule() {
    }
    HealthProposalPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                ReactiveFormsModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatInputModule,
                MatDatepickerModule,
                RouterModule.forChild(routes)
            ],
            //declarations: [HealthProposalPage],
            declarations: [HealthProposalPage, SearchCustComponent],
            entryComponents: [SearchCustComponent, SearchCustComponent],
        })
    ], HealthProposalPageModule);
    return HealthProposalPageModule;
}());
export { HealthProposalPageModule };
//# sourceMappingURL=health-proposal.module.js.map