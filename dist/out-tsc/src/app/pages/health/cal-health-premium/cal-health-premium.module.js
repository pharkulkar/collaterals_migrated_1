import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CalHealthPremiumPage } from './cal-health-premium.page';
import { NgDatepickerModule } from 'ng2-datepicker';
import { DatePickerModule } from 'ionic4-date-picker';
import { IonRangeSliderModule } from 'ng2-ion-range-slider';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { PedQuestionaireComponent } from '../ped-questionaire/ped-questionaire.component';
var routes = [
    {
        path: '',
        component: CalHealthPremiumPage,
    }
];
var CalHealthPremiumPageModule = /** @class */ (function () {
    function CalHealthPremiumPageModule() {
    }
    CalHealthPremiumPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                DatePickerModule,
                NgDatepickerModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatInputModule,
                MatDatepickerModule,
                IonRangeSliderModule,
                IonicModule,
                // CompleteHealthInsurancePageModule,
                // PpapPageModule,
                // HealthBoosterPageModule,
                RouterModule.forChild(routes)
            ],
            // declarations: [CalHealthPremiumPage],
            declarations: [CalHealthPremiumPage, PedQuestionaireComponent],
            entryComponents: [PedQuestionaireComponent]
            // entryComponents: [CompleteHealthInsurancePage, PpapPage, HealthBoosterPage]
        })
    ], CalHealthPremiumPageModule);
    return CalHealthPremiumPageModule;
}());
export { CalHealthPremiumPageModule };
//# sourceMappingURL=cal-health-premium.module.js.map