import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PpapPage } from './ppap.page';
var routes = [
    {
        path: '',
        component: PpapPage
    }
];
var PpapPageModule = /** @class */ (function () {
    function PpapPageModule() {
    }
    PpapPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PpapPage]
        })
    ], PpapPageModule);
    return PpapPageModule;
}());
export { PpapPageModule };
//# sourceMappingURL=ppap.module.js.map