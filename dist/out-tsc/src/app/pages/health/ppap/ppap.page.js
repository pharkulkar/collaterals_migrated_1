import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CommonService } from "../../../services/common.service";
import { XmlToJsonService } from "../../../services/xml-to-json.service";
import { AlertController } from '@ionic/angular';
import { LoadingService } from "../../../services/loading.service";
import * as _ from 'underscore';
import * as moment from 'moment';
import { Router } from '@angular/router';
import * as M from 'materialize-css';
// import { FormGroup, FormControl, Validators } from '@angular/forms';
var PpapPage = /** @class */ (function () {
    function PpapPage(commonservice, routes, alertController, apiloading, xml2JsonService) {
        this.commonservice = commonservice;
        this.routes = routes;
        this.alertController = alertController;
        this.apiloading = apiloading;
        this.xml2JsonService = xml2JsonService;
        this.moreDetails = false;
        this.isPPAPPC = false;
        this.isPPA = false;
        this.isPPC = false;
        this.isEarning = false;
        this.canShowOptions = false;
        this.canShowOptionsDetails = false;
        this.optionsArray = [];
        this.OptionDetails = [];
        this.OptionsDetailsArray = [];
        this.showPPASumInsured = false;
        this.lumpsum = [];
        this.showPPCSumInsured = false;
        this.disableQuote = true;
        // select dropdown popvers configurations //
        this.productPopoveroptions = {
            header: 'Select Product Sub Type'
        };
        this.occuptionPopoverOptions = {
            header: 'Select Occupation'
        };
        this.statePopoverOptions = {
            header: 'Select State'
        };
        this.annualIncomePopoverOptions = {
            header: 'Select Annual Income'
        };
        this.policyDurationPopoverOptions = {
            header: 'Select Policy Duration'
        };
        this.sumInsuredPopoverOptions = {
            header: 'Select Sum Insured'
        };
        this.sumOptipnsPopoverOptions = {
            header: 'Select Options'
        };
        this.payoutAmountPopoverOptions = {
            header: 'Select Payout Amount'
        };
        this.payoutTenurePopoverOptions = {
            header: 'Select Payout Tenure'
        };
        this.lumSumPopoverOptions = {
            header: 'Select Lump Sum Amount'
        };
        this.customPopoverOptions = {
            header: 'Select'
        };
    }
    // @ViewChild('dropdown') dropdown: ElementRef;
    PpapPage.prototype.ngOnInit = function () {
        var _this = this;
        this.apiloading.present();
        // calling  states api on view intialization //
        this.commonservice.getAllStates().then(function (res) {
            _this.states = res;
            _this.states = _.sortBy(_this.states, 'StateName');
            _this.selectedState = 55;
            _this.apiloading.dismiss();
        }, function (err) {
            _this.presentAlert('Sorry Something went wrong');
            _this.apiloading.dismiss();
        });
    };
    PpapPage.prototype.ngAfterViewInit = function () {
    };
    // function to call alert //
    PpapPage.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // function getting triggered on changing annual income //
    PpapPage.prototype.getAnnualIncome = function (selectedAnuualIncome) {
        this.sum_insured = null;
        this.policy_duration = null;
        this.insured_options = null;
        this.payoutTenure = [];
        this.payout_amount = null;
        this.payout_tenure = null;
        this.optionsArray = [];
        this.OptionDetails = [];
        this.canShowOptions = false;
        this.canShowOptionsDetails = false;
        this.showPPASumInsured = false;
        this.lumpsum = [];
        this.lump_sum_payment = null;
        this.showPPCSumInsured = false;
        this.disableQuote = true;
    };
    //function to perform activity on duartion //
    PpapPage.prototype.getPolicyDuration = function (policyTenure) {
        this.sumInsuredArray = {};
        if (this.selectedPP == 'PP') {
            for (var i = 0; i < this.policyData.length; i++) {
                if (this.policyData[i].Value == policyTenure) {
                    this.insuredArray = this.policyData[i].SumInsureds;
                }
            }
            if (this.insuredArray.length > 1) {
                this.sumInsuredArray = _.uniq(this.insuredArray, false, function (p) { return p.Value; });
            }
            else {
                this.sumInsuredArray = this.insuredArray;
            }
        }
        else if (this.selectedPP == 'PPA' || this.selectedPP == 'PPC') {
            for (var i = 0; i < this.policyData.length; i++) {
                if (this.policyData[i].Value == policyTenure) {
                    this.payoutAmount = this.policyData[i].MonthlyPayouts;
                }
            }
        }
        this.disableQuote = true;
        this.sum_insured = null;
        this.payoutTenure = [];
        this.payout_amount = null;
        this.payout_tenure = null;
        this.optionsArray = [];
        this.OptionDetails = [];
        this.canShowOptions = false;
        this.canShowOptionsDetails = false;
        this.showPPASumInsured = false;
        this.lumpsum = [];
        this.lump_sum_payment = null;
        this.showPPCSumInsured = false;
        this.insured_options = null;
    };
    // function getting triggered on changing payout amount //
    PpapPage.prototype.getPayoutAmount = function (payoutamt) {
        this.payoutTenure = {};
        for (var i = 0; i < this.payoutAmount.length; i++) {
            if (this.payoutAmount[i].Value == payoutamt) {
                this.payoutTenure = this.payoutAmount[i].PayoutTenures;
                this.ppa_sum_insured = this.payoutTenure[0].SumInsured;
                // if (this.payoutAmount[i].PayoutTenures.length >=1) {
                //   this.payoutTenure.push(this.payoutAmount[i].PayoutTenures);
                // }
                // else {
                //   this.payoutTenure.push(this.payoutAmount[i].PayoutTenures);
                //   this.ppa_sum_insured = this.payoutAmount[i].PayoutTenures.PayoutTenure.SumInsured;
                // }
            }
        }
        this.payout_tenure = null;
        this.canShowOptions = false;
        this.canShowOptionsDetails = false;
        this.showPPASumInsured = false;
        this.lumpsum = [];
        this.lump_sum_payment = null;
        this.showPPCSumInsured = false;
        this.disableQuote = true;
    };
    // function getting triggered on changing payout tenure //
    PpapPage.prototype.getPayoutTenure = function (payoutTenure) {
        this.showPPASumInsured = true;
        this.OptionsDetailsArray = [];
        this.canShowOptions = false;
        this.canShowOptionsDetails = false;
        this.lumpsum = [];
        this.lump_sum_payment = null;
        this.showPPCSumInsured = false;
        this.disableQuote = true;
        if (this.selectedPP == 'PPA') {
            for (var i = 0; i < this.payoutAmount.length; i++) {
                if (this.payoutAmount[i].Value == this.payout_amount) {
                    if (this.payoutAmount[i].PayoutTenures[0].BenefitSummary != undefined && this.payoutAmount[i].PayoutTenures[0].BenefitSummary != null && this.payoutAmount[i].PayoutTenures[0].BenefitSummary != "") {
                        if (this.payoutAmount[i].PayoutTenures[0].BenefitSummary.length == undefined) {
                            this.OptionsDetailsArray.push(this.payoutAmount[i].PayoutTenures[0].BenefitSummary);
                            this.ppaPlancode = this.payoutAmount[i].PayoutTenures[0].PlanCode;
                            this.disableQuote = false;
                        }
                        if (this.payoutAmount[i].PayoutTenures[0].BenefitSummary.length >= 1) {
                            for (var j = 0; j < this.payoutAmount[i].PayoutTenures[0].BenefitSummary.length; j++) {
                                this.OptionsDetailsArray.push(this.payoutAmount[i].PayoutTenures[0].BenefitSummary[j]);
                            }
                            this.ppaPlancode = this.payoutAmount[i].PayoutTenures[0].PlanCode;
                            this.disableQuote = false;
                        }
                    }
                    else {
                        if (this.payoutAmount[i].PayoutTenures[0].BenefitSummary.length == 0) {
                            this.ppaPlancode = this.payoutAmount[i].PayoutTenures[0].PlanCode;
                            this.disableQuote = false;
                        }
                        else {
                            this.canShowOptions = false;
                            this.canShowOptionsDetails = false;
                            this.disableQuote = false;
                        }
                        this.canShowOptions = false;
                    }
                }
            }
            this.canShowOptionsDetails = true;
        }
        else {
            this.lumpsum = [];
            this.disableQuote = true;
            for (var i = 0; i < this.payoutAmount.length; i++) {
                if (this.payoutAmount[i].Value == this.payout_amount) {
                    if (this.payoutAmount[i].PayoutTenures[0].LumpSums.length != undefined) {
                        this.lumpsum.push(this.payoutAmount[i].PayoutTenures[0].LumpSums[0].Value);
                        this.ppc_sum_insured = this.payoutAmount[i].PayoutTenures[0].LumpSums[0].SumInsured;
                    }
                    else {
                        this.lumpsum.push(this.payoutAmount[i].PayoutTenures.PayoutTenure.LumpSums.Value);
                        this.ppc_sum_insured = this.payoutAmount[i].PayoutTenures.PayoutTenure.LumpSums.SumInsured;
                    }
                }
            }
        }
    };
    // function getting triggered on changing lump sump amount //
    PpapPage.prototype.getLumPSumpAmt = function (lumpsumpamt) {
        for (var i = 0; i < this.payoutAmount.length; i++) {
            if (this.payoutAmount[i].Value == this.payout_amount) {
                if (this.payoutAmount[i].PayoutTenures[0].LumpSums.length != undefined) {
                    this.getPPCDteails(this.payoutAmount[i]);
                }
                else {
                    this.getPPCDteails(this.payoutAmount[i]);
                }
            }
        }
        this.showPPCSumInsured = true;
    };
    PpapPage.prototype.getPPCDteails = function (array) {
        if (array.PayoutTenures[0].LumpSums[0].BenefitSummary != undefined && array.PayoutTenures[0].LumpSums[0].BenefitSummary != null && array.PayoutTenures[0].LumpSums[0].BenefitSummary != "") {
            for (var j = 0; j < array.PayoutTenures[0].LumpSums[0].BenefitSummary.length; j++) {
                this.OptionsDetailsArray.push(array.PayoutTenures[0].LumpSums[0].BenefitSummary[j]);
            }
            this.ppaPlancode = array.PayoutTenures[0].LumpSums[0].PlanCode;
            this.disableQuote = false;
        }
        else {
            this.canShowOptions = false;
            this.canShowOptionsDetails = false;
            this.disableQuote = false;
        }
        this.canShowOptionsDetails = true;
    };
    //function to perform activity on changing insurance //
    PpapPage.prototype.getSumInsured = function (sumInsured) {
        this.optionsArray = [];
        this.OptionDetails = [];
        this.disableQuote = true;
        this.insured_options = null;
        if (_.isArray(this.insuredArray)) {
            for (var i = 0; i < this.insuredArray.length; i++) {
                if (this.insuredArray[i].Value == sumInsured) {
                    this.optionsArray.push(this.insuredArray[i]);
                }
            }
        }
        else {
            this.optionsArray.push(this.insuredArray);
        }
        if (this.optionsArray.length == 1) {
            if (this.optionsArray[0].BenefitSummary != undefined && this.optionsArray[0].BenefitSummary != null && this.optionsArray[0].BenefitSummary != "") {
                if (this.optionsArray[0].BenefitSummary == undefined) {
                    this.canShowOptions = false;
                    this.OptionDetails.push(this.optionsArray[0].BenefitSummary);
                    this.OptionsDetailsArray = this.OptionDetails;
                    this.insured_options = this.optionsArray[0].PlanCode;
                    this.canShowOptionsDetails = true;
                    this.disableQuote = false;
                }
                if (this.optionsArray[0].BenefitSummary.length >= 1) {
                    this.canShowOptions = false;
                    this.OptionsDetailsArray = this.optionsArray[0].BenefitSummary;
                    this.insured_options = this.optionsArray[0].PlanCode;
                    this.canShowOptionsDetails = true;
                    this.disableQuote = false;
                }
            }
        }
        else {
            this.canShowOptions = true;
            this.canShowOptionsDetails = false;
        }
    };
    // function to perform activity on changing options if applicable //
    PpapPage.prototype.getOptions = function (selectedOption) {
        this.OptionDetails = [];
        for (var i = 0; i < this.optionsArray.length; i++) {
            if (this.optionsArray[i].PlanCode == selectedOption) {
                this.OptionDetails.push(this.optionsArray[i].BenefitSummary);
            }
        }
        if (this.OptionDetails[0].length == undefined) {
            this.OptionsDetailsArray = this.OptionDetails;
        }
        else {
            this.OptionsDetailsArray = this.OptionDetails[0];
        }
        this.canShowOptionsDetails = true;
        this.disableQuote = false;
    };
    // function to perform activity on changing states //
    PpapPage.prototype.getState = function (state) {
        this.selectedState = state;
    };
    // function to perform activity on changing product subtype //
    PpapPage.prototype.getSubType = function (subtype) {
        this.moreDetails = false;
        this.selectedOccupationType = null;
        this.disableQuote = true;
    };
    // function to perform activity on changing occupationtype //
    PpapPage.prototype.getOccupation = function (selectedoccupation) {
        var _this = this;
        this.moreDetails = false;
        if (this.selectedSubType == '0') {
            this.selectedPP = 'PP';
            this.isPPAPPC = false;
            this.isPPC = false;
            this.isPPA = false;
        }
        else if (this.selectedSubType == '1') {
            this.selectedPP = 'PPA';
            this.isPPAPPC = true;
            this.isPPA = true;
            this.isPPC = false;
        }
        else {
            this.selectedPP = 'PPC';
            this.isPPAPPC = true;
            this.isPPC = true;
            this.isPPA = false;
        }
        if (selectedoccupation == 0 || selectedoccupation == 1) {
            this.isEarning = true;
        }
        else {
            this.isEarning = false;
        }
        switch (selectedoccupation) {
            case "0":
                this.occType = 'Salaried';
                this.salariedType = 'salaried';
                break;
            case "1":
                this.occType = 'Self Employed';
                this.salariedType = 'salaried';
                break;
            case "2":
                this.occType = 'Housewife';
                this.salariedType = 'nonsalaried';
                break;
            case "3":
                this.occType = 'Student';
                this.salariedType = 'nonsalaried';
                break;
            case "4":
                this.occType = 'Retired';
                this.salariedType = 'nonsalaried';
                break;
        }
        if ((selectedoccupation == '2' || selectedoccupation == '3' || selectedoccupation == '4') && (this.selectedSubType == '1' || this.selectedSubType == '2')) {
            this.presentAlert('No plan available for Monthly Payment Option');
            this.isPPAPPC = false;
            this.isPPC = false;
            this.moreDetails = false;
        }
        else {
            var postData = {
                "SubProduct": this.selectedPP,
                "OccupationType": this.salariedType,
            };
            this.apiloading.present();
            this.commonservice.getMasterDataforPPAP(postData).then(function (res) {
                _this.masterDataPPAP = res;
                _this.incomeData = _this.masterDataPPAP.Incomes;
                _this.policyData = _this.masterDataPPAP.PolicyTenures;
                _this.moreDetails = true;
                _this.disableQuote = true;
                _this.sum_insured = null;
                _this.policy_duration = null;
                _this.annual_income = null;
                _this.payoutTenure = [];
                _this.payout_amount = null;
                _this.payout_tenure = null;
                _this.optionsArray = [];
                _this.OptionDetails = [];
                _this.canShowOptions = false;
                _this.canShowOptionsDetails = false;
                _this.showPPASumInsured = false;
                _this.lumpsum = [];
                _this.lump_sum_payment = null;
                _this.showPPCSumInsured = false;
                _this.disableQuote = true;
                _this.apiloading.dismiss();
                var elems = document.querySelectorAll('#dobValidator');
                var instances = M.Datepicker.init(elems);
            }, function (err) {
                _this.presentAlert('Sorry Something went wrong');
                _this.apiloading.dismiss();
            });
        }
        // let request = "<Policy><UserID>MzU1NTI3</UserID><SubProduct>" + this.selectedPP + "</SubProduct><OccupationType>" + this.occType + "</OccupationType></Policy>"
        // this.commonservice.ppmaster('/HealthMaster.svc/GetPPAPMasterDetails', request).then(res => {
        //   
        // }).catch(err => {
        //   if (err.status != 200) {
        //   }
        //   this.masterData = this.xml2JsonService.xmlToJson(err.error.text);
        //   this.incomeData = this.masterData.PPAPMasterResponse.Incomes.PPAPIncome;
        //   this.policyData = this.masterData.PPAPMasterResponse.PolicyTenures.Tenures;
        //   this.moreDetails = true;this.disableQuote=true;
        //   this.sum_insured = null; this.policy_duration = null; this.annual_income = null;
        //   this.optionsArray = []; this.OptionDetails = []; this.canShowOptions = false; this.canShowOptionsDetails = false;
        //   this.apiloading.dismiss();
        // },err =>{
        //   this.presentAlert(err);
        //   this.apiloading.dismiss();
        // })
    };
    PpapPage.prototype.generateQuote = function (form) {
        var _this = this;
        var isJammuKashmir;
        var tempstate = this.selectedState;
        if (this.selectedState == 149) {
            isJammuKashmir = true;
        }
        else {
            isJammuKashmir = false;
        }
        if (this.selectedState == undefined || this.selectedState == '' || this.selectedState == null) {
            this.presentAlert('Please select state');
        }
        else if (this.dob == undefined || this.dob == '' || this.dob == null) {
            this.presentAlert('Please select date of insured');
        }
        else {
            var statename = _.find(this.states, function (s) { return s.StateId == tempstate; }).StateName;
            var gstStateCode = _.find(this.states, function (s) { return s.StateId == tempstate; }).GSTStateCode;
            var dateofbirth = moment(this.dob).format('DD-MMMM-YYYY');
            var occupation = this.occType.toUpperCase();
            var plancode = void 0;
            if (this.selectedPP == 'PP') {
                plancode = this.insured_options;
            }
            else if (this.selectedPP == 'PPA') {
                plancode = this.ppaPlancode;
            }
            else if (this.selectedPP == 'PPC') {
                plancode = this.ppaPlancode;
            }
            else {
                alert('Plancode not found');
            }
            var postData = {
                "UserType": "AGENT",
                "ProductType": "PPAP",
                "ipaddress": "MOBILE-WS",
                "SubProduct": this.selectedPP,
                "DOB": dateofbirth,
                "Tenure": this.policy_duration,
                "IsJammuKashmir": isJammuKashmir,
                "Occupation": occupation,
                "PlanCode": plancode,
                "GSTStateCode": gstStateCode,
                "GSTStateName": statename
            };
            debugger;
            // this.apiloading.present();
            // this.commonservice.savePPAPQuotes(postData).then(res => {
            //   this.saveQuoteData = res;
            //   form.controls['dobValidator'].reset();form.controls['subProuctType'].reset();
            //   this.moreDetails = false; this.disableQuote = true;
            //   this.sum_insured = null; this.policy_duration = null; this.annual_income = null;
            //   this.payoutTenure = []; this.payout_amount = null; this.payout_tenure = null;
            //   this.optionsArray = []; this.OptionDetails = []; this.canShowOptions = false; this.canShowOptionsDetails = false;
            //   this.showPPASumInsured = false; this.lumpsum = []; this.lump_sum_payment = null; this.showPPCSumInsured = false;
            //   this.disableQuote = true; this.dob = null; this.selectedSubType = null; this.selectedOccupationType = null;
            //   this.selectedState = 55; this.payoutAmount = []; this.policyData = [];
            //   this.apiloading.dismiss();
            //   if (this.saveQuoteData.StatusCode == 1) {
            //     this.presentAlert('Quote Generated Successfully');
            //   }
            //   else {
            //     this.presentAlert(this.saveQuoteData.StatusMessage);
            //   }
            // }, err => {
            //   this.presentAlert('Sorry Something went wrong');
            //   this.apiloading.dismiss();
            // });
            this.apiloading.present();
            var requestData = '<Request><UserType>AGENT</UserType><ProductType>PPAP</ProductType><UserID>NTkyNDYwMF9FWUZkKzE5RnlndFhJUHJNY0gxaVhJZ1Jxam5qSktXRW95c1dRYmZLQmhNPQ==</UserID><ipaddress>MOBILE-WS</ipaddress>' +
                '<SubProduct>' + this.selectedPP + '</SubProduct><DOB>' + dateofbirth + '</DOB><Tenure>' + this.policy_duration + '</Tenure><IsJammuKashmir>' + isJammuKashmir + '</IsJammuKashmir>' +
                '<Occupation>' + occupation + '</Occupation><PlanCode>' + plancode + '</PlanCode><GSTStateCode>' + gstStateCode + '</GSTStateCode><GSTStateName>' + statename + '</GSTStateName></Request>';
            this.commonservice.ppmaster('/Quote.svc/GetQuote/Health/MzU1NTI3', requestData).then(function (res) {
            }).catch(function (err) {
                if (err.status != 200) {
                }
                var data = _this.xml2JsonService.xmlToJson(err.error.text);
                form.controls['dobValidator'].reset();
                form.controls['subProuctType'].reset();
                _this.moreDetails = false;
                _this.disableQuote = true;
                _this.sum_insured = null;
                _this.policy_duration = null;
                _this.annual_income = null;
                _this.payoutTenure = [];
                _this.payout_amount = null;
                _this.payout_tenure = null;
                _this.optionsArray = [];
                _this.OptionDetails = [];
                _this.canShowOptions = false;
                _this.canShowOptionsDetails = false;
                _this.showPPASumInsured = false;
                _this.lumpsum = [];
                _this.lump_sum_payment = null;
                _this.showPPCSumInsured = false;
                _this.disableQuote = true;
                _this.dob = null;
                _this.selectedSubType = null;
                _this.selectedOccupationType = null;
                _this.selectedState = 55;
                _this.payoutAmount = [];
                _this.policyData = [];
                _this.apiloading.dismiss();
                if (data.QuoteResponse.StatusCode == 1) {
                    _this.presentAlert('Quote generated successfully...');
                    // this.routes.navigate(['/premium']);
                }
                else {
                    _this.presentAlert(data.QuoteResponse.StatusMessage);
                }
            });
        }
    };
    PpapPage.prototype.convertAmountToCurrency = function (num) {
        if (num != undefined && num != null && num != '') {
            return num.toLocaleString('en-IN', { style: 'currency', currency: "INR", minimumFractionDigits: 0, maximumFractionDigits: 0 });
        }
    };
    PpapPage.prototype.getDate = function (event) {
        console.log(moment(this.dob).format('DD-MMMM-YYYY'));
    };
    PpapPage.prototype.onSubmit = function () {
    };
    PpapPage = tslib_1.__decorate([
        Component({
            selector: 'app-ppap',
            templateUrl: './ppap.page.html',
            styleUrls: ['./ppap.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, Router, AlertController, LoadingService, XmlToJsonService])
    ], PpapPage);
    return PpapPage;
}());
export { PpapPage };
//# sourceMappingURL=ppap.page.js.map