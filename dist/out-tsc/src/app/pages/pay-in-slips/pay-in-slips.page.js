import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { AlertController, } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import * as $ from "jquery";
var PayInSlipsPage = /** @class */ (function () {
    function PayInSlipsPage(cs, alertCtrl, route, _formBuilder) {
        this.cs = cs;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this._formBuilder = _formBuilder;
        this.healthArray = [];
        this.travelArray = [];
        this.isLinear = false;
        this.isTravel = false;
        this.isHealth = false;
        this.isSubmmited = "false";
        this.isHealthShow = false;
        this.isTravelShow = false;
        this.isAccepted = "false";
        this.isConfirmed = "false";
    }
    PayInSlipsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.getData().then(function () {
            _this.getMyPayInSlip(_this.product);
        });
        // this.firstFormGroup = this._formBuilder.group({
        //   firstCtrl: ['', Validators.required]
        // });
        // this.secondFormGroup = this._formBuilder.group({
        //   secondCtrl: ['', Validators.required]
        // });
    };
    PayInSlipsPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.userData = JSON.parse(localStorage.getItem('userData'));
            _this.isHealthRights(_this.userData.MappedProduct.Health).then(function () {
                if (_this.healthArray.indexOf(true) > -1) {
                    _this.isHealthShow = true;
                }
                else {
                    _this.isHealthShow = false;
                }
            });
            _this.isTravelRights(_this.userData.MappedProduct.Travel).then(function () {
                if (_this.travelArray.indexOf(true) > -1) {
                    _this.isTravelShow = true;
                }
                else {
                    _this.isTravelShow = false;
                }
            });
            resolve();
        });
    };
    PayInSlipsPage.prototype.isHealthRights = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            Object.keys(data).forEach(function (key) {
                console.log(key);
                if (key == 'isHealthBoosterMapped' || key == 'isHealthCHIMapped' || key == 'isHealthPPAPMapped') {
                    console.log(data[key], key);
                    _this.healthArray.push(data[key]);
                }
            });
            resolve();
        });
    };
    PayInSlipsPage.prototype.isTravelRights = function (data) {
        var _this = this;
        return new Promise(function (resolve) {
            Object.keys(data).forEach(function (key) {
                console.log(key);
                if (key == 'isTRAVELINTERNATIONALMapped' || key == 'isTRAVELSTUDENTMapped') {
                    console.log(data[key], key);
                    _this.travelArray.push(data[key]);
                }
            });
            resolve();
        });
    };
    PayInSlipsPage.prototype.getMyPayInSlip = function (type) {
        var _this = this;
        if (type == 'health') {
            this.isHealth = true;
            this.isTravel = false;
        }
        else {
            this.isTravel = true;
            this.isHealth = false;
        }
        this.cs.showLoader = true;
        this.cs.getMyPayInSlip('/api/policy/FetchMyPayInSlips?PolicyType=' + type).then(function (res) {
            _this.TravelData = res;
            console.log("travel data", _this.TravelData);
            _this.cs.showLoader = false;
        }, function (err) {
            _this.cs.showLoader = false;
        });
    };
    PayInSlipsPage.prototype.downloadProposal = function (ePFPaymentID, chequeNumber) {
        this.selectedChequeNumber = chequeNumber;
        if (ePFPaymentID.includes(":")) {
            this.paymentIDList = ePFPaymentID.split(":");
            document.getElementById("downloadOption").style.height = "100%";
        }
        else {
            var downloadURL = this.cs.pdfDownload('PAYINSLIP', ePFPaymentID);
            this.cs.save(downloadURL, chequeNumber + ".pdf");
        }
    };
    PayInSlipsPage.prototype.getChequeStatus = function (paymentID, chequeNumber) {
        var _this = this;
        this.selectedChequeNumber = chequeNumber;
        this.cs.showLoader = true;
        this.cs.getChequeStatus('/api/agent/GetChequepaymentStatus?Payment_id=' + paymentID).then(function (res) {
            _this.ChequeDetails = res;
            _this.showChequeDetails();
            console.log("cheque data", _this.ChequeDetails);
            _this.cs.showLoader = false;
        }, function (err) {
            _this.cs.showLoader = false;
        });
    };
    PayInSlipsPage.prototype.selectChange = function (e) {
        console.log(e);
    };
    PayInSlipsPage.prototype.showChequeDetails = function () {
        if (this.ChequeDetails.PaymentStatus == "SUBMITTED") {
            $("mat-step-header").eq(0).children().eq(1).attr('class', 'mat-step-icon');
            $("mat-step-header").eq(0).children().eq(1).css('background-color', '#008F11');
            $("mat-step-header").eq(1).children().eq(1).attr('class', 'mat-step-icon-not-touched');
            $("mat-step-header").eq(2).children().eq(1).attr('class', 'mat-step-icon-not-touched');
            this.isSubmmited = "true";
            this.isAccepted = "false";
            this.isConfirmed = "false";
        }
        else if (this.ChequeDetails.PaymentStatus == "ACCEPTED") {
            $("mat-step-header").eq(0).children().eq(1).attr('class', 'mat-step-icon');
            $("mat-step-header").eq(0).children().eq(1).css('background-color', '#008F11');
            $("mat-step-header").eq(1).children().eq(1).attr('class', 'mat-step-icon');
            $("mat-step-header").eq(1).children().eq(1).css('background-color', '#008F11');
            $("mat-step-header").eq(2).children().eq(1).attr('class', 'mat-step-icon-not-touched');
            this.isSubmmited = "true";
            this.isAccepted = "true";
            this.isConfirmed = "false";
        }
        else {
            for (var i = 0; i < 3; i++) {
                $("mat-step-header").eq(i).children().eq(1).attr('class', 'mat-step-icon');
                $("mat-step-header").eq(i).children().eq(1).css('background-color', '#008F11');
            }
            this.isSubmmited = "true";
            this.isAccepted = "true";
            this.isConfirmed = "true";
        }
        document.getElementById("chequeStatus").style.height = "100%";
    };
    PayInSlipsPage.prototype.closeCheque = function (ev) {
        document.getElementById("chequeStatus").style.height = "0%";
    };
    PayInSlipsPage.prototype.closeDownlaodOption = function (ev) {
        document.getElementById("downloadOption").style.height = "0%";
    };
    PayInSlipsPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    PayInSlipsPage = tslib_1.__decorate([
        Component({
            selector: 'app-pay-in-slips',
            templateUrl: './pay-in-slips.page.html',
            styleUrls: ['./pay-in-slips.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, AlertController, Router, FormBuilder])
    ], PayInSlipsPage);
    return PayInSlipsPage;
}());
export { PayInSlipsPage };
//# sourceMappingURL=pay-in-slips.page.js.map