import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PayInSlipsPage } from './pay-in-slips.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { IonicStepperModule } from 'ionic-stepper';
import { MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatInputModule, MatStepperModule } from '@angular/material';
var routes = [
    {
        path: '',
        component: PayInSlipsPage
    }
];
var PayInSlipsPageModule = /** @class */ (function () {
    function PayInSlipsPageModule() {
    }
    PayInSlipsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                MatDatepickerModule,
                MatFormFieldModule,
                MatNativeDateModule,
                MatInputModule,
                Ng2SearchPipeModule,
                IonicStepperModule,
                MatStepperModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PayInSlipsPage]
        })
    ], PayInSlipsPageModule);
    return PayInSlipsPageModule;
}());
export { PayInSlipsPageModule };
//# sourceMappingURL=pay-in-slips.module.js.map