import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { ToastController } from '@ionic/angular';
var PaymentConfirmationPage = /** @class */ (function () {
    function PaymentConfirmationPage(cs, toastCtrl) {
        this.cs = cs;
        this.toastCtrl = toastCtrl;
        this.paymentDetails = false;
        this.showCHI = false;
        this.showHB = false;
        this.showPPAP = false;
        this.travelConf = false;
        this.healthConf = false;
        this.showEmailPrompt = false;
        this.successMsg = false;
        this.isError = false;
        this.heathRenewal = false;
    }
    PaymentConfirmationPage.prototype.ngOnInit = function () {
        var _this = this;
        // document.getElementById("myNav4").style.height = "0%";  
        this.isRenewal = localStorage.getItem('isRenewal');
        this.getData().then(function () {
            _this.createTravelForm();
            _this.createCHIForm();
            _this.createHBForm();
            _this.createPPAPForm();
        });
    };
    PaymentConfirmationPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
            _this.quoteResponse = JSON.parse(localStorage.getItem('quoteResponse'));
            _this.travelPlan = JSON.parse(localStorage.getItem('travelPlan'));
            _this.proposalResponse = JSON.parse(localStorage.getItem('proposalResponse'));
            _this.proposalRequest = JSON.parse(localStorage.getItem('proposalRequest'));
            _this.renewalProduct = JSON.parse(localStorage.getItem('product'));
            _this.customerRequest = JSON.parse(localStorage.getItem('customerRequest'));
            _this.PIDData = JSON.parse(localStorage.getItem('PIDData'));
            _this.paymentMode = JSON.parse(localStorage.getItem('paymentMode'));
            _this.commonPayRes = JSON.parse(localStorage.getItem('commonPayRes'));
            _this.paymentConfRes = JSON.parse(localStorage.getItem('paymentConfRes'));
            console.log("IMP", _this.quoteRequest);
            console.log(_this.quoteResponse);
            console.log(_this.travelPlan);
            console.log(_this.proposalResponse);
            console.log(_this.proposalRequest);
            console.log(_this.customerRequest);
            console.log("PID", _this.PIDData);
            console.log("PID", _this.commonPayRes);
            console.log("PID", _this.paymentConfRes);
            if (_this.isRenewal == 'false') {
                if (_this.proposalRequest.Product == 'Travel') {
                    _this.travelConf = true;
                    _this.healthConf = false;
                    _this.heathRenewal = false;
                }
                else {
                    _this.travelConf = false;
                    _this.healthConf = true;
                    _this.heathRenewal = false;
                }
            }
            else if (_this.isRenewal == 'true') {
                if (_this.renewalProduct.productName == 'CHI' || _this.renewalProduct.productName == 'Health Booster' || _this.renewalProduct.productName == 'PPAP') {
                    _this.travelConf = false;
                    _this.healthConf = false;
                    _this.heathRenewal = true;
                }
            }
            resolve();
        });
    };
    PaymentConfirmationPage.prototype.createTravelForm = function () {
        this.travelPayConfForm = new FormGroup({
            policyNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].PolicyID),
            premium: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium),
            policyStartDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].StartDate).format('YYYY-MM-DD')),
            policyEndDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].EndDate).format('YYYY-MM-DD'))
        });
    };
    PaymentConfirmationPage.prototype.createCHIForm = function () {
        this.CHIPayConfForm = new FormGroup({
            policyNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].PolicyID),
            proposalNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].ProposalNumber),
            premium: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium),
            policyStartDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].StartDate).format('YYYY-MM-DD')),
            policyEndDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].EndDate).format('YYYY-MM-DD'))
        });
    };
    PaymentConfirmationPage.prototype.createHBForm = function () {
        this.HBPayConfForm = new FormGroup({
            policyNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].PolicyID),
            proposalNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].ProposalNumber),
            premium: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium),
            policyStartDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].StartDate).format('YYYY-MM-DD')),
            policyEndDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].EndDate).format('YYYY-MM-DD'))
        });
    };
    PaymentConfirmationPage.prototype.createPPAPForm = function () {
        this.PPAPPayConfForm = new FormGroup({
            policyNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].PolicyID),
            proposalNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].ProposalNumber),
            premium: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium),
            policyStartDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].StartDate).format('YYYY-MM-DD')),
            policyEndDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].EndDate).format('YYYY-MM-DD'))
        });
    };
    PaymentConfirmationPage.prototype.showDetails = function (ev) {
        this.paymentDetails = !this.paymentDetails;
        if (this.paymentDetails) {
            var imgUrl = './assets/images/minus.png';
            document.getElementById("details").style.background = "url(" + imgUrl + ")";
            document.getElementById("details").style.backgroundRepeat = 'no-repeat';
        }
        else {
            var imgUrl = './assets/images/plus.png';
            document.getElementById("details").style.background = "url(" + imgUrl + ")";
            document.getElementById("details").style.backgroundRepeat = 'no-repeat';
        }
    };
    PaymentConfirmationPage.prototype.showTabs = function (data) {
        console.log(data);
        if (data == 'CHI') {
            this.showCHI = true;
            this.showHB = false;
            this.showPPAP = false;
        }
        else if (data == 'HB') {
            this.showCHI = false;
            this.showHB = true;
            this.showPPAP = false;
        }
        else {
            this.showCHI = false;
            this.showHB = false;
            this.showPPAP = true;
        }
    };
    PaymentConfirmationPage.prototype.showEmail = function (ev) {
        // this.showEmailPrompt = !this.showEmailPrompt;
        console.log(document.getElementById("myNav4"));
        document.getElementById("myNav4").style.height = "100%";
    };
    PaymentConfirmationPage.prototype.close = function (ev) {
        document.getElementById("myNav4").style.height = "0%";
        document.getElementById("myNav5").style.height = "0%";
    };
    PaymentConfirmationPage.prototype.downloadPdf = function (ev) {
        var downloadURL = this.cs.pdfDownload('POLICY', this.paymentConfRes.PolicyDetails[0].EPolicyID);
        this.cs.save(downloadURL, this.paymentConfRes.PolicyDetails[0].ProposalNumber + ".pdf");
        // window.open(downloadURL, "_blank");
        this.presentToast('Your pdf will be downloaded');
    };
    PaymentConfirmationPage.prototype.sendEmail = function (ev) {
        var _this = this;
        console.log(this.email);
        var body = {
            "EPolicyID": this.paymentConfRes.PolicyDetails[0].EPolicyID,
            // "EPolicyID": 'ABCDE',
            "PDFType": "POLICY",
            "EmailID": this.email
        };
        var str = JSON.stringify(body);
        this.cs.showLoader = true;
        this.cs.postWithParams('/api/file/sendemailpdf', str).then(function (res) {
            console.log(res);
            _this.emailRes = res.Message;
            if (_this.emailRes.indexOf('successfully emailed') > -1) {
                document.getElementById("myNav4").style.height = "0%";
                document.getElementById("myNav5").style.height = "100%";
                _this.successMsg = true;
                _this.isError = false;
                _this.cs.showLoader = false;
            }
            else {
                _this.cs.showLoader = false;
                _this.successMsg = false;
                _this.isError = false;
                document.getElementById("myNav4").style.height = "0%";
                document.getElementById("myNav5").style.height = "100%";
            }
        }).catch(function (err) {
            console.log("error", err.statusText);
            _this.isError = true;
            _this.error = err.statusText;
            _this.successMsg = false;
            _this.cs.showLoader = false;
            document.getElementById("myNav4").style.height = "0%";
            document.getElementById("myNav5").style.height = "100%";
        });
    };
    PaymentConfirmationPage.prototype.presentToast = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 3000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PaymentConfirmationPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    PaymentConfirmationPage = tslib_1.__decorate([
        Component({
            selector: 'app-payment-confirmation',
            templateUrl: './payment-confirmation.page.html',
            styleUrls: ['./payment-confirmation.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, ToastController])
    ], PaymentConfirmationPage);
    return PaymentConfirmationPage;
}());
export { PaymentConfirmationPage };
//# sourceMappingURL=payment-confirmation.page.js.map