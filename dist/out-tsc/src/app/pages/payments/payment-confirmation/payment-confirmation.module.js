import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PaymentConfirmationPage } from './payment-confirmation.page';
var routes = [
    {
        path: '',
        component: PaymentConfirmationPage
    }
];
var PaymentConfirmationPageModule = /** @class */ (function () {
    function PaymentConfirmationPageModule() {
    }
    PaymentConfirmationPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PaymentConfirmationPage]
        })
    ], PaymentConfirmationPageModule);
    return PaymentConfirmationPageModule;
}());
export { PaymentConfirmationPageModule };
//# sourceMappingURL=payment-confirmation.module.js.map