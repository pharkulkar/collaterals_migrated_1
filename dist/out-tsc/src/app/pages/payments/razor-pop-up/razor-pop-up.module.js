import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RazorPopUpPage } from './razor-pop-up.page';
var routes = [
    {
        path: '',
        component: RazorPopUpPage
    }
];
var RazorPopUpPageModule = /** @class */ (function () {
    function RazorPopUpPageModule() {
    }
    RazorPopUpPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [RazorPopUpPage]
        })
    ], RazorPopUpPageModule);
    return RazorPopUpPageModule;
}());
export { RazorPopUpPageModule };
//# sourceMappingURL=razor-pop-up.module.js.map