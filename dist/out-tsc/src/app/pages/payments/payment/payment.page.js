import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading.service';
import { config } from 'src/app/config.properties';
import { NotificationService } from 'src/app/services/notification.service';
var PaymentPage = /** @class */ (function () {
    function PaymentPage(cs, httpClient, router, loading, notificationService) {
        this.cs = cs;
        this.httpClient = httpClient;
        this.router = router;
        this.loading = loading;
        this.notificationService = notificationService;
        this.isCustomer = false;
        this.showCustInt = false;
        this.isNewPID = false;
        this.custType = 'Customer';
        this.isWalletShow = true;
        this.isWalletShowAtPID = true;
        this.isEMISHow = true;
        this.isEMISHowAtPID = true;
        // this.notificationService.requestPermission();
    }
    PaymentPage.prototype.ngOnInit = function () {
        var _this = this;
        // this.newPID = JSON.parse(localStorage.getItem('newPID'));
        this.getNewPID().then(function () {
            _this.getData();
        });
        // if (!window['postMessage'])
        //   alert("oh crap");
        // else {
        //   if (window.addEventListener) {
        //     window.addEventListener("message", this.ReceiveMessage, false);
        //   }else {
        //     console.log("onmessage", this.ReceiveMessage);
        //     // window.attachEvent("onmessage", this.ReceiveMessage);
        //   }
        // }  
    };
    // notify(ev:any){
    //   let data: Array < any >= [];
    //       data.push({
    //           'title': 'Approval',
    //           'alertContent': 'This is First Alert -- By Debasis Saha'
    //       });
    //       data.push({
    //           'title': 'Request',
    //           'alertContent': 'This is Second Alert -- By Debasis Saha'
    //       });
    //       data.push({
    //           'title': 'Leave Application',
    //           'alertContent': 'This is Third Alert -- By Debasis Saha'
    //       });
    //       data.push({
    //           'title': 'Approval',
    //           'alertContent': 'This is Fourth Alert -- By Debasis Saha'
    //       });
    //       data.push({
    //           'title': 'To Do Task',
    //           'alertContent': 'This is Fifth Alert -- By Debasis Saha'
    //       });
    //       this.notificationService.generateNotification(data);
    // }
    PaymentPage.prototype.getNewPID = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.newPID = JSON.parse(localStorage.getItem('newPID'));
            if (_this.newPID) {
                if (_this.newPID.CustomerId) {
                    _this.newPIDCustId = _this.newPID.CustomerId;
                }
                else {
                    _this.newPIDCustId = "";
                }
                if (_this.newPID.custEmail) {
                    _this.custEmail = _this.newPID.custEmail;
                }
                else {
                    _this.custEmail = "";
                }
                if (_this.newPID.custMobile) {
                    _this.custMobile = _this.newPID.custMobile;
                }
                else {
                    _this.custMobile = "";
                }
                if (_this.newPID.custName) {
                    _this.custName = _this.newPID.custName;
                }
                else {
                    _this.custName = "";
                }
            }
            else {
                _this.newPIDCustId = "";
                _this.custEmail = "";
                _this.custMobile = "";
                _this.custName = "";
            }
            resolve();
        });
    };
    PaymentPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            console.log(_this.newPID);
            if (_this.newPID && _this.newPID.isNew) {
                _this.isNewPID = true;
                if (_this.newPID.pidAmount < 2000) {
                    _this.isWalletShowAtPID = true;
                    _this.isEMISHowAtPID = false;
                }
                else if (_this.newPID.pidAmount >= 3000) {
                    _this.isWalletShowAtPID = false;
                    _this.isEMISHowAtPID = true;
                }
                else {
                    _this.isWalletShowAtPID = false;
                    _this.isEMISHowAtPID = false;
                }
            }
            else {
                _this.isNewPID = false;
                _this.travelRequest = JSON.parse(localStorage.getItem('quoteRequest'));
                _this.travelResponse = JSON.parse(localStorage.getItem('quoteResponse'));
                _this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
                _this.travelProp = JSON.parse(localStorage.getItem('proposalResponse'));
                _this.travelPropReq = JSON.parse(localStorage.getItem('proposalRequest'));
                _this.createCustReq = JSON.parse(localStorage.getItem('customerRequest'));
                _this.renewalProduct = JSON.parse(localStorage.getItem('product'));
                _this.isRenewal = localStorage.getItem('isRenewal');
                if (_this.travelPropReq.ProductType == 'CHI' || _this.travelPropReq.ProductType == 'HBOOSTER' || _this.travelPropReq.ProductType == 'PPAP') {
                    _this.showCustInt = true;
                }
                else {
                    _this.showCustInt = false;
                }
                console.log(_this.travelRequest);
                console.log(_this.travelResponse);
                console.log(_this.planDetails);
                console.log(_this.travelProp);
                console.log(_this.travelPropReq);
                console.log(_this.createCustReq);
                console.log(_this.renewalProduct);
                console.log(_this.isRenewal);
                _this.custEmail = _this.createCustReq.EmailAddress;
                _this.custName = _this.createCustReq.Name;
                _this.custMobile = _this.createCustReq.MobileNumber;
                if (_this.travelProp.SavePolicy[0].TotalPremium < 20000) {
                    _this.isWalletShow = true;
                }
                else {
                    _this.isWalletShow = false;
                }
                if (_this.travelProp.SavePolicy[0].TotalPremium >= 3000) {
                    _this.isEMISHow = true;
                }
                else {
                    _this.isEMISHow = false;
                }
                if (_this.travelProp.SavePolicy[0].CustomerID == null || _this.travelProp.SavePolicy[0].CustomerID == undefined) {
                    _this.pidCustId = '';
                    _this.pidPFCustId = _this.travelProp.SavePolicy[0].PF_CustomerID;
                    _this.getPIDList();
                }
                else {
                    if (_this.travelProp.SavePolicy[0].PF_CustomerID == null || _this.travelProp.SavePolicy[0].PF_CustomerID == undefined) {
                        _this.pidCustId = _this.travelProp.SavePolicy[0].CustomerID;
                        _this.pidPFCustId = '';
                        _this.getPIDList();
                    }
                    else {
                        _this.pidCustId = '';
                        _this.pidPFCustId = _this.travelProp.SavePolicy[0].PF_CustomerID;
                        _this.getPIDList();
                    }
                }
                console.log(_this.pidCustId, _this.pidPFCustId);
                resolve();
            }
        });
    };
    PaymentPage.prototype.makePayment1 = function (mode, ev) {
        var _this = this;
        this.paymentMode = mode;
        console.log(this.paymentMode);
        var type;
        if (this.newPID && this.newPID.isNew) {
            type = this.newPID.policyType;
        }
        else {
            if (this.isRenewal == 'true') {
                type = 'Health';
            }
            else {
                if (this.travelPropReq.ProductType == 'CHI' || this.travelPropReq.ProductType == 'HBOOSTER' || this.travelPropReq.ProductType == 'PPAP') {
                    type = 'Health';
                    this.showCustInt = true;
                }
                else {
                    type = 'Travel';
                    this.showCustInt = false;
                }
            }
        }
        // this.cs.showLoader = true;
        this.beforeRazorPay().then(function () {
            _this.payByRazorPay(_this.commonDataRes, _this.paymentMode, type, ev);
        });
    };
    PaymentPage.prototype.beforeRazorPay = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var payBody;
            if (_this.newPID && _this.newPID.isNew) {
                payBody = {
                    "TransType": "PID",
                    "PaymentAmount": _this.newPID.pidAmount,
                    "PidDetails": {
                        "PolicyType": _this.newPID.policyType,
                        "PF_CustomerId": "",
                        "iPartnerCustomerId": _this.newPIDCustId
                    },
                    "GatewayReturnURL": "",
                    "PolicyIDs": "",
                    "PayerType": _this.newPID.PayerType,
                    "ModeID": 0,
                    "UserRole": "AGENT",
                    "IPAddress": config.ipAddress,
                    "PaymentMode": "RAZORPAY"
                };
            }
            else {
                payBody = {
                    "TransType": "POLICY_PAYMENT",
                    "GatewayReturnURL": "",
                    "PolicyIDs": _this.travelProp.SavePolicy[0].PolicyID,
                    "PayerType": "Customer",
                    "ModeID": 0,
                    "UserRole": "AGENT",
                    "IPAddress": config.ipAddress,
                    "PaymentMode": "RAZORPAY",
                    "PaymentAmount": ""
                };
            }
            var str = JSON.stringify(payBody);
            console.log("STR", str);
            localStorage.setItem('commonPayReq', str);
            _this.cs.showLoader = true;
            _this.cs.postWithParams('/api/Payment/CommonPayment', str).then(function (res) {
                console.log(res);
                _this.commonDataRes = res;
                localStorage.setItem('payData', JSON.stringify(res));
                _this.cs.showLoader = false;
                resolve();
            }).catch(function (err) {
                console.log("Error", err);
                _this.cs.showLoader = false;
                resolve();
            });
        });
    };
    PaymentPage.prototype.payByRazorPay = function (res, type, produtType, ev) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("Response", _this.commonDataRes);
            _this.cs.showLoader = true;
            var desc = "ICICI Lombard - " + produtType + " Insurance";
            _this.options = {
                "description": desc,
                "image": 'https://www.icicilombard.com/mobile/mclaim/images/favicon.ico',
                "currency": 'INR',
                "key": res.PublicKey,
                "order_id": res.RazorOrderID,
                "method": {
                    "netbanking": {
                        "order": ["ICIC", "HDFC", "SBIN", "UTIB", "IDFB", "IBKL"]
                    }
                },
                "prefill": {
                    email: _this.custEmail,
                    contact: _this.custMobile,
                    name: _this.custName,
                    method: type
                },
                "theme": {
                    "color": '#E04844',
                    "hide_topbar": true
                },
                "handler": function (response) {
                    console.log(response);
                    var payData = JSON.parse(localStorage.getItem('payData'));
                    console.log(payData);
                    var params = {
                        "iPaymentID": payData.PaymentID,
                        "PaymentID": response.razorpay_payment_id,
                        "OrderID": response.razorpay_order_id,
                        "Signature": response.razorpay_signature
                    };
                    // console.log("Screen",screen);
                    // let left = (screen.width/2)-(700/2);
                    // let top = (screen.height/2)-(400/2);
                    // let childWindow = window.open('./../../../child2.html','Childwindow','status=0,toolbar=0,menubar=0,resizable=0,scrollbars=1,top='+top+' ,left='+left+',height=375,width=650');
                    // console.log("Child Window 1 ", childWindow);
                    // childWindow.onload = (e) => {
                    //   console.log("Child Window", e)
                    //   if (childWindow == null || !window['postMessage']){
                    //     alert("oh IF");
                    //   }else{
                    //     alert("oh ELSE");
                    //     childWindow.postMessage({'razorPayDetails':params,'url':config.baseURL + '/PaymentGateway/RazorPayPaymentProcess'},'http://localhost:8100/#/payment');
                    //   }            
                    // }
                    var form = document.createElement("form");
                    form.setAttribute("method", "post");
                    form.setAttribute("action", config.baseURL + '/PaymentGateway/RazorPayPaymentProcess');
                    for (var key in params) {
                        if (params.hasOwnProperty(key)) {
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", key);
                            hiddenField.setAttribute("value", params[key]);
                            form.appendChild(hiddenField);
                        }
                    }
                    document.body.appendChild(form);
                    console.log("Form", form);
                    form.x();
                },
                "modal": {
                    "ondismiss": function () {
                        loading;
                    }
                }
            };
            var loading = _this.cs.showLoader = false;
            console.log("razorPayOptions = ", _this.options);
            var rzp1 = new Razorpay(_this.options);
            console.log(rzp1, _this.commonDataRes);
            rzp1.open();
            resolve();
        });
    };
    // ReceiveMessage(evt:any) {
    //   var message : string;
    //   if (false) {
    //     message = 'You ("' + evt.origin + '") are not worthy';
    //   }
    //   else {
    //     message = 'I got "' + JSON.stringify(evt.data) + '" from "' + evt.origin + '"';
    //   }
    //   var ta = document.getElementById("taRecvMessage");
    //   if (ta == null)
    //     console.log(message);
    //   else
    //     document.getElementById("taRecvMessage").innerHTML = message;
    // }
    PaymentPage.prototype.showData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var rzp1 = new Razorpay(_this.options);
            console.log(rzp1);
            rzp1.open();
            resolve();
        });
    };
    // submitForm(response: any) {
    //   console.log("Success", response, this.commonDataRes, this.options);
    //   let params = {
    //     "iPaymentID": this.commonDataRes.PaymentID,
    //     "PaymentID": response.razorpay_payment_id,
    //     "OrderID": response.razorpay_order_id,
    //     "Signature": response.razorpay_signature
    //   }
    //   var form = document.createElement("form");
    //   form.setAttribute("method", "post");
    //   form.setAttribute("action", config.baseURL + '/PaymentGateway/RazorPayPaymentProcess');
    //   for (var key in params) {
    //     if (params.hasOwnProperty(key)) {
    //       var hiddenField = document.createElement("input");
    //       hiddenField.setAttribute("type", "hidden");
    //       hiddenField.setAttribute("name", key);
    //       hiddenField.setAttribute("value", params[key]);
    //       form.appendChild(hiddenField);
    //     }
    //   }
    //   document.body.appendChild(form);
    //   console.log("Form", form);
    //   form.submit();
    // }
    PaymentPage.prototype.makePayment2 = function (mode) {
        this.router.navigateByUrl('payment-mode');
        localStorage.setItem('paymentMode', JSON.stringify(mode));
    };
    PaymentPage.prototype.showPID = function (ev) {
        var _this = this;
        this.forCust(ev).then(function () {
            _this.getPIDList();
        });
    };
    PaymentPage.prototype.forCust = function (ev) {
        var _this = this;
        return new Promise(function (resolve) {
            if (ev.target.checked) {
                _this.isCustomer = true;
                _this.custType = 'Intermediary';
                localStorage.setItem('custType', JSON.stringify(_this.custType));
            }
            else {
                _this.isCustomer = false;
                _this.custType = 'Customer';
                localStorage.setItem('custType', JSON.stringify(_this.custType));
            }
            resolve();
        });
    };
    PaymentPage.prototype.getPIDList = function () {
        var _this = this;
        return new Promise(function (resolve) {
            console.log(_this.pidCustId, _this.pidPFCustId);
            localStorage.removeItem('PIDData');
            if (_this.isCustomer) {
                _this.cs.showLoader = true;
                // this.isCustomer = true;
                console.log("Intermediary");
                var body = {
                    "PayerType": "Intermediary",
                    "PF_CustomerId": _this.pidPFCustId,
                    "iPartnerCustomerId": _this.pidCustId,
                    "PolicyType": _this.travelPropReq.Product
                };
                var str = JSON.stringify(body);
                _this.cs.postWithParams('/api/Payment/GetPidList', str).then(function (res) {
                    console.log(res);
                    localStorage.setItem('PIDData', JSON.stringify(res));
                    _this.cs.showLoader = false;
                    resolve();
                }).catch(function (err) {
                    console.log(err);
                    _this.cs.showLoader = false;
                    resolve();
                });
            }
            else {
                console.log("Customer", _this.travelPropReq.Product);
                _this.cs.showLoader = true;
                // this.isCustomer = false;
                var body = {
                    "PayerType": "Customer",
                    "PF_CustomerId": _this.pidPFCustId,
                    "iPartnerCustomerId": _this.pidCustId,
                    "PolicyType": _this.travelPropReq.Product
                };
                var str = JSON.stringify(body);
                console.log(str);
                _this.cs.postWithParams('/api/Payment/GetPidList', str).then(function (res) {
                    console.log(res);
                    localStorage.setItem('PIDData', JSON.stringify(res));
                    _this.cs.showLoader = false;
                    resolve();
                }).catch(function (err) {
                    console.log(err);
                    _this.cs.showLoader = false;
                    resolve();
                });
            }
        });
    };
    PaymentPage.prototype.payLater = function (ev) {
        var _this = this;
        console.log(ev);
        document.getElementById("myNav3").style.height = "100%";
        this.payLaterStart().then(function () {
            _this.payLaterConfirm();
        });
    };
    PaymentPage.prototype.payLaterStart = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var body = {
                "TransType": "POLICY_PAYMENT",
                "GatewayReturnURL": "",
                "PolicyIDs": _this.travelProp.SavePolicy[0].PolicyID,
                "PayerType": _this.custType,
                "ModeID": 0,
                "UserRole": "AGENT",
                "IPAddress": config.ipAddress,
                "PaymentMode": "None",
                "PaymentAmount": ""
            };
            var str = JSON.stringify(body);
            console.log(str);
            _this.cs.postWithParams('/api/Payment/CommonPayment', str).then(function (res) {
                console.log(res);
                _this.payLaterRes = res;
                resolve();
            }).catch(function (err) {
                console.log("Error", err);
                resolve();
            });
        });
    };
    PaymentPage.prototype.payLaterConfirm = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.cs.postWithParams('/api/Payment/CommonPaymentConfirmationByPID?EPaymentID=' + _this.payLaterRes.EPaymentID, '').then(function (res) {
                console.log(res);
                _this.payLaterConfRes = res;
            });
        });
    };
    PaymentPage.prototype.close = function (ev) {
        console.log(ev);
        document.getElementById("myNav3").style.height = "0%";
        this.cs.goToHome();
    };
    PaymentPage.prototype.home = function (ev) {
        console.log("Home", ev);
        this.cs.goToHome();
    };
    PaymentPage = tslib_1.__decorate([
        Component({
            selector: 'app-payment',
            templateUrl: './payment.page.html',
            styleUrls: ['./payment.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, HttpClient, Router, LoadingService, NotificationService])
    ], PaymentPage);
    return PaymentPage;
}());
export { PaymentPage };
//# sourceMappingURL=payment.page.js.map