import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PaymentModePage } from './payment-mode.page';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
var routes = [
    {
        path: '',
        component: PaymentModePage
    }
];
var PaymentModePageModule = /** @class */ (function () {
    function PaymentModePageModule() {
    }
    PaymentModePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatInputModule,
                MatDatepickerModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PaymentModePage]
        })
    ], PaymentModePageModule);
    return PaymentModePageModule;
}());
export { PaymentModePageModule };
//# sourceMappingURL=payment-mode.module.js.map