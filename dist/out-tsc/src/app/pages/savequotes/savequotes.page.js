import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { AlertController, } from '@ionic/angular';
import { Router } from '@angular/router';
var SavequotesPage = /** @class */ (function () {
    function SavequotesPage(cs, alertCtrl, router) {
        this.cs = cs;
        this.alertCtrl = alertCtrl;
        this.router = router;
    }
    SavequotesPage.prototype.ngOnInit = function () {
    };
    SavequotesPage.prototype.presentAlert = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Alert',
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SavequotesPage.prototype.goToHealthquote = function (t) {
        var _this = this;
        var postData = {
            "PolicyType": "t",
            "isIASCases": false
        };
        this.cs.postWithParams('/api/Policy/MySavedQuotes', postData).then(function (res) {
            //this.savequotes = res;
        }).catch(function (err) {
            //console.log(err);
            _this.cs.showLoader = false;
        });
    };
    SavequotesPage = tslib_1.__decorate([
        Component({
            selector: 'app-savequotes',
            templateUrl: './savequotes.page.html',
            styleUrls: ['./savequotes.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CommonService, AlertController, Router])
    ], SavequotesPage);
    return SavequotesPage;
}());
export { SavequotesPage };
//# sourceMappingURL=savequotes.page.js.map