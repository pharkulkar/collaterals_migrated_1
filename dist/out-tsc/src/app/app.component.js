import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CommonService } from './services/common.service';
import { Router } from '@angular/router';
var AppComponent = /** @class */ (function () {
    function AppComponent(platform, cs, splashScreen, statusBar, router) {
        this.platform = platform;
        this.cs = cs;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.router = router;
        this.initializeApp();
        // this.router.events.subscribe((ev) => {
        //   // console.log("Route", ev);
        //   if (ev instanceof NavigationEnd) { 
        //     console.log("Route", ev.url);
        //   }
        // });
    }
    AppComponent.prototype.ngOnInit = function () {
        console.log(this.cs.showLoader);
    };
    AppComponent.prototype.getRoute = function () {
        console.log("FUnction", this.router);
    };
    AppComponent.prototype.initializeApp = function () {
        this.platform.ready().then(function () {
            //   this.statusBar.styleDefault();
            //   this.splashScreen.hide();
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [Platform, CommonService,
            SplashScreen,
            StatusBar,
            Router])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map