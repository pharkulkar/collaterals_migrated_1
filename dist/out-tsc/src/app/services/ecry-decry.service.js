import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
var EcryDecryService = /** @class */ (function () {
    function EcryDecryService() {
        this.encKey = 'e78538a1-9bae-4a3e-9bc9-';
        this.encIV = 'ef17086a';
    }
    EcryDecryService.prototype.ngOnInit = function () {
    };
    EcryDecryService.prototype.encrypt = function () {
        this.hexEncKey = CryptoJS.enc.Utf8.parse(this.encKey);
        this.hexEncIV = CryptoJS.enc.Utf8.parse(this.encIV);
        var msg = "<SearchCustomer><strPassKey>NThiM2E5NWMtZTkyMy00MWEyLWIwOWMtZGFhOWFiYTJiZDNk</strPassKey><UserID>MTY0MTI1NDRfc011b2JtUWRmTDNPV3BReHlGTTJZZEx5YkZrVU1mSVVIVWpaT0UrT3cxVT0=</UserID><CustomerName>Sunita</CustomerName><MobileNo></MobileNo><EmailID></EmailID></SearchCustomer>";
        this.enCrytedData = CryptoJS.TripleDES.encrypt(msg, this.hexEncKey, {
            iv: this.hexEncIV,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        // this.enCrytedData = CryptoJS.TripleDES.encrypt(data, this.hexEncKey, {
        //   iv: this.hexEncIV,
        //   mode: CryptoJS.mode.CBC,
        //   padding: CryptoJS.pad.Pkcs7
        // });
        console.log("Encrypt Service", encodeURIComponent(this.enCrytedData.toString()));
        // return encodeURIComponent(this.enCrytedData.toString());
    };
    EcryDecryService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], EcryDecryService);
    return EcryDecryService;
}());
export { EcryDecryService };
//# sourceMappingURL=ecry-decry.service.js.map