import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatnumbers'
})
export class FormatnumbersPipe implements PipeTransform {

  transform(value: any): any {
    let formatNumber = new Intl.NumberFormat('en-IN');
    return formatNumber.format(value);
  }

}
