import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, merge, of, fromEvent } from 'rxjs';
import { Network } from '@ionic-native/network/ngx';
import { Platform } from '@ionic/angular';
import { mapTo, debounceTime } from 'rxjs/operators';

export enum ConnectionStatus {
  Online,
  Offline
}

@Injectable({
  providedIn: 'root'
})
export class NetworkService {
  platformStatus: any;
  online: any;
  isConnected:any;

  private status: BehaviorSubject<ConnectionStatus> = new BehaviorSubject(ConnectionStatus.Offline);

  constructor(public network: Network, public plaform: Platform) {
    // this.plaform.ready().then(() => {
    //   this.initializeNetworkEvents();
    //   console.log(this.network.type);
    //   let status =  this.network.type !== 'none' ? ConnectionStatus.Online : ConnectionStatus.Offline;
    //   this.status.next(status);
    //   console.log("status", status);
    // });

    this.online = Observable.create(observer => {
      observer.next(true);
    }).pipe(mapTo(true));

    if (this.plaform.is('cordova')) {
      // on Device
      this.online = merge(
        this.network.onConnect().pipe(mapTo(true)),
        this.network.onDisconnect().pipe(mapTo(false))
      );
    } else {
      // on Browser
      this.online = merge(
        of(navigator.onLine),
        fromEvent(window, 'online').pipe(mapTo(true)),
        fromEvent(window, 'offline').pipe(mapTo(false))
      );
    }
    console.log(this.online);
  }

  networkSubscriber():Promise<any>{
    return new Promise((resolve:any) => {
      this.getNetworkStatus()
      .pipe(debounceTime(300))
      .subscribe((connected: boolean) => {
        this.isConnected = connected;
        // return connected;
        console.log('[Home] isConnected', this.isConnected);
        // if(this.isConnected){
        //   console.log('ONLINE');
        //   localStorage.setItem('status', 'ONLINE');
        //   // return true;
        // }else{
        //   console.log('OFFLINE', localStorage.getItem('Hello'));
        //   localStorage.setItem('status', 'OFFLINE');
        //   // return false;
        // }
    });
    resolve();
    });    
      
    
  }

  public getNetworkType(): string {
    return this.network.type;
  }

  public getNetworkStatus(): Observable<boolean> {
    return this.online;
  }

}
