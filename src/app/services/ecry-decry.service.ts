import { Injectable, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';
@Injectable({
  providedIn: 'root'
})
export class EcryDecryService implements OnInit{

  encKey = 'e78538a1-9bae-4a3e-9bc9-';
  encIV = 'ef17086a';
  hexEncKey:any;
  hexEncIV:any;
  enCrytedData:any;
  deCryptedData:any;

  constructor() { }

  ngOnInit(){
    
  }

  encrypt(){
    this.hexEncKey = CryptoJS.enc.Utf8.parse(this.encKey);
    this.hexEncIV = CryptoJS.enc.Utf8.parse(this.encIV);
    let msg = `<SearchCustomer><strPassKey>NThiM2E5NWMtZTkyMy00MWEyLWIwOWMtZGFhOWFiYTJiZDNk</strPassKey><UserID>MTY0MTI1NDRfc011b2JtUWRmTDNPV3BReHlGTTJZZEx5YkZrVU1mSVVIVWpaT0UrT3cxVT0=</UserID><CustomerName>Sunita</CustomerName><MobileNo></MobileNo><EmailID></EmailID></SearchCustomer>`;
    this.enCrytedData = CryptoJS.TripleDES.encrypt(msg, this.hexEncKey, {
        iv: this.hexEncIV,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });
      
    // this.enCrytedData = CryptoJS.TripleDES.encrypt(data, this.hexEncKey, {
    //   iv: this.hexEncIV,
    //   mode: CryptoJS.mode.CBC,
    //   padding: CryptoJS.pad.Pkcs7
    // });


    console.log("Encrypt Service",encodeURIComponent(this.enCrytedData.toString()));
    // return encodeURIComponent(this.enCrytedData.toString());
  }

  // decrypt(encData:any){
  //   this.hexEncKey = CryptoJS.enc.Utf8.parse(this.encKey);
  //   this.hexEncIV = CryptoJS.enc.Utf8.parse(this.encIV);

  //   this.deCryptedData = CryptoJS.TripleDES.decrypt(
  //     {ciphertext: CryptoJS.enc.Base64.parse(encData)},
  //     this.hexEncKey ,
  //     {iv: this.hexEncIV,
  //     mode: CryptoJS.mode.CBC,
  //     padding: CryptoJS.pad.Pkcs7});

  //     return this.deCryptedData.toString(CryptoJS.enc.Utf8);
  // }
 
}
