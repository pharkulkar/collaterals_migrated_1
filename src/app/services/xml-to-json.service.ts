import { Injectable } from '@angular/core';
import xml2js from 'xml2js';

@Injectable({
  providedIn: 'root'
})
export class XmlToJsonService {

  jsonData: any;
  // xmlData = "<CHI><ProductType>CHI</ProductType><UserID>MTY0MTI1NDRfc011b2JtUWRmTDNPV3BReHlGTTJZZEx5YkZrVU1mSVVIVWpaT0UrT3cxVT0=</UserID><UserType>Agent</UserType><NoOfAdults>1</NoOfAdults><NoOfKids>2</NoOfKids><AgeGroup1>56-60</AgeGroup1><AgeGroup2></AgeGroup2><AgeGroup>56-60</AgeGroup><CHIProductName>PROTECT</CHIProductName><SubLimit></SubLimit><YearPremium>1</YearPremium><IsJammuKashmir>false</IsJammuKashmir><VolDedutible>0</VolDedutible><GSTStateCode></GSTStateCode><AddOn1>true</AddOn1><InsuredAmount>300000</InsuredAmount><AddOn6>false</AddOn6><AddOn3>false</AddOn3><AddOn5>false</AddOn5><GSTStateName>MAHARASHTRA</GSTStateName><Members><Member></Member></Members><AddOn6Members/></CHI>";

  constructor() { }

  xmlToJson(xmlData:any):any{
    xml2js.parseString(xmlData, {explicitArray: false}, (error:any, result:any)=>{
      if(error){
        throw new Error(error);        
      }else{
        this.jsonData = result;
      }
    });
    // console.log("Service",this.jsonData);
    return this.jsonData;
  }

  jsonToXML(jsonData:any):any{
    var builder = new xml2js.Builder();
    var xml = builder.buildObject(jsonData);
    return xml;
  }

  
}
