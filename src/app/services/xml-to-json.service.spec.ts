import { TestBed } from '@angular/core/testing';

import { XmlToJsonService } from './xml-to-json.service';

describe('XmlToJsonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: XmlToJsonService = TestBed.get(XmlToJsonService);
    expect(service).toBeTruthy();
  });
});
