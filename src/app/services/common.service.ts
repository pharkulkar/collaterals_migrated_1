import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { config } from '../config.properties';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class CommonService {

  showLoader = false;showSwapLoader=false;
  validArray = [];
  responseData: any;
  isBackFormTravelPremium = false;
  isPIDReceiveData = false;
  authToken: any; 
  hospifundToken:any;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'text/xml', //<- To SEND XML
      'Accept': 'text/xml',     //<- To ask for XML
      'Response-Type': 'text'             //<- b/c Angular understands text
      , 'No-Auth': 'True'
    })
  };
  agentAuthToken: any;

  constructor(public http: HttpClient, public router: Router) {
    console.log("BASEURL", config.baseURL);
    this.authToken = JSON.parse(localStorage.getItem('authToken'));
  }

  postWithParams(endPoint:any, body:any){
    let headers = new HttpHeaders();
    headers = headers.append("Authorization",this.authToken);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
  }

  swapAppMaster(endPoint:any, token:any){
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", token);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(config.baseURL + endPoint, '', { headers: headers }).toPromise();
  }

  getWithParams(endPoint:any){
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", this.authToken);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.get(config.baseURL + endPoint, { headers: headers }).toPromise();
  }

  getWithParams1(endPoint: any, token: any) {
    console.log(token);
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", token);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.get(config.baseURL + endPoint, { headers: headers }).toPromise();
  }

  getAuthToken(endPoint: any) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(config.baseURL + endPoint, { headers: headers }).toPromise();
  }

  getAgentAuthToken(endPoint:any,agentID: any)
  {  
  return this.http.get(config.baseURL + endPoint, agentID).toPromise();
  }


  postTravel(endPoint: any, body: any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'text/xml', //<- To SEND XML
        'Accept': 'text/xml',     //<- To ask for XML
        'Response-Type': 'text'             //<- b/c Angular understands text
      })
    };
    return this.http.post(config.baseURL + endPoint, body, httpOptions).toPromise();
  }

  postPropRel(endPoint: any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'text/xml', //<- To SEND XML
        'Accept': 'text/xml',     //<- To ask for XML
        'Response-Type': 'text'             //<- b/c Angular understands text
      })
    };
    return this.http.post(config.baseURL + endPoint, httpOptions).toPromise();
  }

  postPropCal(endPoint: any, body: any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'text/xml', //<- To SEND XML
        'Accept': 'text/xml',     //<- To ask for XML
        'Response-Type': 'text'             //<- b/c Angular understands text
      })
    };
    return this.http.post(config.baseURL + endPoint, body, httpOptions).toPromise();
  }

  // getPinCodeData(endPoint: any) {
  //   return this.http.get(config.baseURL + endPoint).toPromise();
  // }
  
  // getForAilmentList(endPoint: any) {
  //   return this.http.get(config.baseURL + endPoint).toPromise();
  // }

  

  postCustSearch(endPoint: any, body: any) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
  }

  getHomeAuth(endPoint: any, data: any){
    let userCredentials = data.userName + ":" + data.passWord;
    console.log(data);
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Basic " + btoa(userCredentials));
    // headers = headers.append("Authorization", this.authToken);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(config.baseURL + endPoint, '', { headers: headers }).toPromise();
  }

  // checkAuthToken(){
  //   let authToken1 = JSON.parse(localStorage.getItem('authToken'));
  //   console.log("Before Change", authToken1);

  //   Object.observe(authToken1, () => {

  //   })
  // }

  // getBank(endPoint: any) {
  //   let headers = new HttpHeaders({
  //     'Content-Type': 'application/json'
  //   });
  //   return this.http.post(config.baseURL + endPoint, { headers: headers }).toPromise();
  // }


  

  // swapAuth(endPoint: any, token: any) {
  //   console.log(token);
  //   let headers = new HttpHeaders();
  //   headers = headers.append("Authorization", token);
  //   headers = headers.append('Content-Type', 'application/json');

  //   return this.http.post(config.baseURL + endPoint, {}, { headers: headers }).toPromise();
  // }

  getDisbaledFeatures(endPoint: any, token: any) {
    console.log(token);
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", token);
    headers = headers.append('Content-Type', 'application/json');

    return this.http.get(config.baseURL + endPoint, { headers: headers }).toPromise();
  }

  getUserData(endPoint: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", this.authToken);
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    return this.http.post(config.baseURL + endPoint, {}, { headers: headers }).toPromise();
  }

  pdfDownload(type: any, policID: any) {
    return config.baseURL + '/api/file/DownloadPDF?type=' + type + '&value=' + policID;
  }

  generatePDF(policno,token) {
    let headers = new HttpHeaders({
      'Authorization':token      
    });
    
    let promise = new Promise((resolve, reject) => {
      this.http.get(config.hospifundBaseURL+'/ilservices/Misc/v1/Generic/PolicyCertificate?policyNo='+policno.trim(),{headers: headers, responseType: 'blob' as 'json'})
        .toPromise()
        .then(
          res => { // Success
            resolve(res);
          },
          msg => { // Error
            reject(msg);
          }
        );
    });
    return promise;
  }
  getAuthorizationDataforPDF() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json','Access-Control-Allow-Origin': '*','Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Accept': 'application/json'
    });
    let promise = new Promise((resolve, reject) => {
      this.http.get(config.baseURL + '/api/Utility/GetAPIToken?scope=esbgeneric', { headers: headers })
        .toPromise()
        .then(
          res => { // Success
            resolve(res);
          },
          msg => { // Error
            reject(msg);
          }
        );
    });
    return promise;
  }
  
  save(file: string, fileName: string) {
    console.log(file);
    if (window.navigator.msSaveOrOpenBlob) {
      navigator.msSaveBlob(file, fileName);
    } else {
      console.log('creation');
      const downloadLink = document.createElement("a");
      downloadLink.style.display = "none";
      document.body.appendChild(downloadLink);
      downloadLink.setAttribute("href", file);
      downloadLink.setAttribute("download", fileName);
      downloadLink.click();
      document.body.removeChild(downloadLink);
      console.log('clicked');
    }
  }
  
  saveBlob(file: Blob,fileName: string){
    let fileData = window.URL.createObjectURL(file);
    var a = document.createElement("a");
    a.href = fileData;
    a.download = fileName;
    a.target =  '_blank';
    document.body.appendChild(a);
    a.click();
  }

  goToHome() {
    let plan = localStorage.getItem('custplan');
    if(plan == 'HEALTHCHI' || plan == 'HEALTHBOOSTER' || plan == 'HEALTHPPAP' || plan == 'HEALTHHOSPIFUND' || plan=='HEALTHASP')
    {
      this.router.navigateByUrl('health');
    }
    else if(plan == 'TRAVELINT' )
    {
      this.router.navigateByUrl('travel-quote');
    }
    else
    {
      alert("Currently this product is not available, Kindly contact your partner");
    }
    // return new Promise((resolve) => {
    //   this.showSwapLoader = true;
    //   let agentId = JSON.parse(localStorage.getItem('userData'));
    // //  console.log(str);
    //   this.getAgentAuthToken('/api/common/GetIpartToken',agentId.AgentID).then((res:any) => {
    //  //   console.log("",res);
    //     // this.agentAuthToken = res;
    //     // let token = parseInt(this.agentAuthToken);
    //     let webLink:any = config.homeURL+ res
    //     window.location.href = webLink;
    //     this.showSwapLoader = false;
    //     resolve();
    //   }).catch((err) => {
    //     console.log("Error", err);
    //     this.showSwapLoader = false;
    //     resolve();
    //   });
    // });
    
  }

  //--------------------Renewal APIs------------------------//
  // getDatathroughRNFetch(endPoint: any, body: any) {
  //   let headers = new HttpHeaders();
  //   headers = headers.append("Authorization", this.authToken);
  //   headers = headers.append('Content-Type', 'application/json');
  //   return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
  // }

    // PPAPPolicyRenewalRN(endPoint: any, body: any) {
    //   let headers = new HttpHeaders();
    //   headers = headers.append("Authorization", this.authToken);
    //   headers = headers.append('Content-Type', 'application/json');
    //   return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
    // }

    isDataNullorUndefined(data:any){
      this.validArray = [];
      Object.keys(data).forEach((key) => {
         if(data[key]){
            this.validArray.push('Valid');
          }else{
            this.validArray.push('Invalid');
          }
      })     
    }

    isUndefinedORNull(data:any){
      if(data === undefined || data == null || data.length <= 0){
        return true;
      }else{
        return false;
      }
    }


  // SendMailRnNotification(endPoint: any, body: any) {
  //   let headers = new HttpHeaders();
  //   headers = headers.append("Authorization", this.authToken);
  //   headers = headers.append('Content-Type', 'application/json');
  //   return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
  // }

  // RnNotification(endPoint: any, body: any) {
  //   let headers = new HttpHeaders();
  //   headers = headers.append("Authorization", this.authToken);
  //   headers = headers.append('Content-Type', 'application/json');
  //   return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
  // }

  //------------------- Pay-In-slips -------------------

  getMyPayInSlip (endPoint: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", this.authToken);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(config.baseURL + endPoint, '',{ headers: headers }).toPromise();
  }

  getChequeStatus(endPoint: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", this.authToken);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.get(config.baseURL + endPoint,{ headers: headers }).toPromise();
  }

  getAuthorizationData()
  {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Accept': 'application/json'
    });
    let promise = new Promise((resolve, reject) => {
      this.http.get(config.baseURL + '/api/Utility/GetAPIToken?scope=esbzerotat', { headers: headers })
        .toPromise()
        .then(
          res => 
          { // Success
            resolve(res);
          },
          msg => 
          { // Error
            reject(msg);
          }
        );
    });
    return promise;
  }
  getDealIdForProposal(deal_id)
  {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('HospifundToken'),
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Accept': 'application/json'
    });
    let promise = new Promise((resolve, reject) => {
      this.http.post(config.baseURL + '/api/HospiFund/GetDealID',deal_id, { headers: headers })
        .toPromise()
        .then(
          res => 
          { // Success
            resolve(res);
          },
          msg => 
          { // Error
            reject(msg);
          }
        );
    });
    return promise;
  }
  postforHospifund(endpoint:any,body:any)
  {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", localStorage.getItem('HospifundToken'));
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(config.hospifundBaseURL+endpoint,body,{ headers: headers }).toPromise();
  }

  generateCSC(postData)
  {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Accept': 'application/json'
    });
    let promise = new Promise((resolve, reject) => {
      this.http.post( config.baseURL + +'/api/HospiFund/CSCDataSaved',postData, { headers: headers })
        .toPromise()
        .then(
          res => 
          { // Success
            resolve(res);
          },
          msg => 
          { // Error
            reject(msg);
          }
        );
    });
    return promise;
  }

  submitPincode(pin_code,city_id)
  {
    let headers = new HttpHeaders({
      'Content-Type': 'text/xml', //<- To SEND XML
      'Accept': 'text/xml',     //<- To ask for XML
      'Response-Type': 'text',            //<- b/c Angular understands text
      'No-Auth': 'True'
    });
    let promise = new Promise((resolve, reject) => {
      this.http.post('https://www.icicilombard.com/mobile/RTOList.svc/GetPincodeID/'+pin_code+'/'+city_id, { headers: headers })
        .toPromise()
        .then(
          res => 
          { // Success
            resolve(res);
          },
          msg => 
          { // Error
            reject(msg);
          }
        );
    });
    return promise;
  }
  SPConditionCheck() {
    //  let userCredentials = 'IM-46236' + ':' + 'lombard132';
      let headers = new HttpHeaders();
      // headers = headers.append("Authorization", 'IM-46236' + ':' + 'lombard132');
      headers = headers.append("Authorization", this.authToken);
      headers = headers.append('Content-Type', 'application/json');
      return this.http.post(config.baseURL + '/api/motor/SPConditionStatusCheck', '', { headers: headers }).toPromise();
  }

  SPPostCall(endPoint:any, body:any) {
    let headers = new HttpHeaders();  
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
  }
}
