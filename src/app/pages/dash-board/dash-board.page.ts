import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, AlertController } from '@ionic/angular';
import { NetworkService } from 'src/app/services/network.service';
import { CommonService } from 'src/app/services/common.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.page.html',
  styleUrls: ['./dash-board.page.scss'],
})
export class DashBoardPage implements OnInit {

  agentDetails:any;
  isHealth = false;
  isTravel = false;
  isMotor = false;
  isTwoWheeler = false;
  isPayInSlips = true;
  states:any; selectedState:any;
  constructor(public router: Router, public alertCtrl: AlertController, public cs: CommonService, public toastCtrl: ToastController, public network: NetworkService) { 
    console.log('DASHBOARD');
  }

  ngOnInit() {    
    this.network.networkSubscriber().then(() => {
      console.log("Online", this.network.isConnected);
      if(this.network.isConnected == 'ONLINE'){
        console.log('ONLINE');
        this.agentDetails = JSON.parse(localStorage.getItem('userData'));
        this.pageAuth(this.agentDetails);
        this.getStateMaster();
      }else{
        console.log('OFFLINE');
        this.agentDetails = JSON.parse(localStorage.getItem('userData'));
        this.pageAuth(this.agentDetails);
        this.getStateMaster();
      }
    });   
  }

  pageAuth(data:any){
    console.log(data.MappedProduct);
    let mappedProduct = data.MappedProduct;
    this.showHealth(mappedProduct).then(() => {
      this.showTravel(mappedProduct).then(() => {
        this.showCar(mappedProduct).then(()=>{
          this.showTwoWheeler(mappedProduct);
        });});});
  }

  showHealth(data:any):Promise<any>{
    return new Promise((resolve)=> {
      console.log("Health",data);
      if(data.Health.isHealthBoosterMapped || data.Health.isHealthCHIMapped || data.Health.isHealthPPAPMapped){
          this.isHealth = true;
      }
      resolve();
    });
  }

  showTravel(data:any):Promise<any>{
    return new Promise((resolve)=> {
      console.log("Travel",data);
      if(data.Travel.isTRAVELINTERNATIONALMapped || data.Travel.isTRAVELSTUDENTMapped){
          this.isTravel = true;
      }
      resolve();
    });
  }

  showCar(data:any):Promise<any>{
    return new Promise((resolve)=> {
      console.log("Car",data);
      if(data.Motor.isFWShopMapped || data.Motor.isFourWheelerMapped|| data.Motor.isGCVMapped || data.Motor.isPCVMapped || data.Motor.isTWShopMapped){
          this.isMotor = true;
      }
      resolve();
    });
  }

  showTwoWheeler(data:any):Promise<any>{
    return new Promise((resolve)=> {
      console.log("Two Wheeler",data);
      if(data.Motor.isTwoWheelerMapped){
          this.isTwoWheeler = true;
      }
      resolve();
    });
  }

  routeToPage(page:any){
    console.log("Page", page);
    let route = page.toUpperCase();
    console.log("ROUTE", route);
    localStorage.setItem('planFormURL', JSON.stringify(route));
    if(route == 'HEALTH'){
      this.router.navigateByUrl('health');
    }else if(route == 'TRAVEL'){
      this.router.navigateByUrl('travel-quote');
    }else if(route == 'TWOWHEELER'){
      this.presentToast();
    }else if(route == 'CAR'){
      this.presentToast();
    }else if(route == 'HEALTHRENEWAL'){
      this.router.navigateByUrl('health-renewal');
    }else if(route == 'PAYINSLIP'){
      this.router.navigateByUrl('pay-in-slips');
    }else if(route == 'PID'){
      console.log('PID ROUTE ');
      this.router.navigateByUrl('pid');
    }else if(route == 'SAVEQUOTE'){
      this.router.navigateByUrl("savequotes");
    }else if(route == 'PENDINGPAYMENT'){
      this.router.navigateByUrl("pendingPayment");
    }else if(route == 'INSTANTRENEWAL'){
      this.router.navigateByUrl('instant-renewal');
    }else{
      console.log('INVALID ROUTE');
    }
  }

  goToHealth(){
    this.router.navigateByUrl('health');
  }

  goToTravel(){
    this.router.navigateByUrl('travel-quote');
  }

  goToCar(){
    this.presentToast();
  }
  goToRenewal(){
    this.router.navigateByUrl('health-renewal');
  }
  goToinstantRenewal(){
    this.router.navigateByUrl('instant-renewal');
  }
  goToSaveQuotes(){
    this.router.navigateByUrl("savequotes");
  }
  goToTwoWheeler(){
    this.presentToast();
  }
  goToPayInSlip(){
    this.router.navigateByUrl('pay-in-slips');
  }

  goToPID(){
    this.router.navigateByUrl('pid');
  }
  goToPendingPayment(){
    this.router.navigateByUrl('pendingpyment');
  }

  async presentToast() {
    const toast = await this.toastCtrl.create({
      message: 'Work In Progress....',
      duration: 2000
    });
    toast.present();
  }

  getStateMaster(){
    this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(res => {
      this.states = res;
      this.states = _.sortBy(this.states, 'StateName');
      localStorage.setItem('stateMaster', JSON.stringify(this.states));
      this.selectedState = 55;
    }).catch((err: any) => {
      localStorage.setItem('stateMaster', JSON.stringify(''));
      this.presentAlert(err.error.ExceptionMessage);
    });
  }

  async presentAlert(message: any) {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

}
