import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../../services/common.service";
import { AlertController } from '@ionic/angular';
import { LoadingService } from "../../../services/loading.service";
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-ped-questionaire',
  templateUrl: './ped-questionaire.component.html',
  styleUrls: ['./ped-questionaire.component.scss'],
})
export class PedQuestionaireComponent implements OnInit {
  pedData:any;result:any;pedSelectedData={};
  constructor(public commonservice: CommonService,public modalController: ModalController, private routes: Router, public alertController: AlertController, public apiloading: LoadingService) { }

  ngOnInit() {
    this.apiloading.present();
    this.commonservice.postWithParams('/api/Agent/GetPEDQuestion', '').then(res => {
      this.result = res;
      this.pedData=this.result.Questions;
      for(let i=0;i<this.pedData.length;i++)
      {
        this.pedData[i].isChecked=false;
      }
      console.log(this.pedData);
      this.apiloading.dismiss();
    }, err => {
      this.presentAlert('Sorry Something went wrong');
      this.apiloading.dismiss();
    });
  }
 // function to call alert //
 async presentAlert(message: any) {
  const alert = await this.alertController.create({
    header: 'Alert',
    message: message,
    buttons: ['OK']
  });
  await alert.present();
}
async messageAlert(message: any) {
  const alert = await this.alertController.create({
    header: 'Alert',
    message: message,
    buttons: [{
      text: 'Okay',
      handler: () => {
        this.commonservice.goToHome();
      }
    }]
  });
  await alert.present();
}
gotoProposal()
{
  this.modalController.dismiss(); 
  let isDisease=0;
  localStorage.setItem('pedDetails',this.pedData);
  for(let i=0;i<this.pedData.length;i++)
  {
    if(this.pedData[i].isChecked==true){
      isDisease=isDisease+1;
    }
  }
  if(isDisease>0){
    this.messageAlert('Online booking is not available for this proposal. Kindly contact the nearest branch or your MO for further assistance.');
  }
  else{this.routes.navigateByUrl('health-proposal'); }
  
}
closeModal()
{
  this.modalController.dismiss();
}
}
