import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthSummaryPage } from './health-summary.page';

describe('HealthSummaryPage', () => {
  let component: HealthSummaryPage;
  let fixture: ComponentFixture<HealthSummaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthSummaryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthSummaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
