import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { AlertController, ToastController } from '@ionic/angular';
import { config } from 'src/app/config.properties';

@Component({
  selector: 'app-health-summary',
  templateUrl: './health-summary.page.html',
  styleUrls: ['./health-summary.page.scss'],
})
export class HealthSummaryPage implements OnInit {
  isShowPlanDetails: boolean = false; isShowApplicantDetails: boolean = false; isShowGSTDetails: boolean = false;
  isShowMedicalDetails: boolean = true; isTravel = false; tripDuration: any; noOfMember: any; planId: any;
  proposalResponse: {}; planType: any;
  // RequestData is stored inside below property 
  requestData: any = '<CustomerDetails><UserID>NTkyNDYwMF9FWUZkKzE5RnlndFhJUHJNY0gxaVhJZ1Jxam5qSktXRW95c1dRYmZLQmhNPQ==</UserID><AnnualIncomeText></AnnualIncomeText><AnnualIncomeValue>-1</AnnualIncomeValue><CustomerId>0</CustomerId><DateOfBirth>10-May-2001</DateOfBirth><EmailAddress><![CDATA[fhggg@fgg.com]]></EmailAddress><AlternateEmail/><HasAddressChanged>true</HasAddressChanged><HobbyValue>-1</HobbyValue><HobbyText></HobbyText><IdentityNumber>121212121212</IdentityNumber><IdentityProofText>121212121212</IdentityProofText><IdentityProofValue>2</IdentityProofValue><LandlineNumber/><MaritalStatusText>SINGLE</MaritalStatusText><MaritalStatusValue>1</MaritalStatusValue><AlternateMobile/><MobileNumber>8080619571</MobileNumber><Name><![CDATA[Bday jadhav]]></Name><OccupationText></OccupationText><OccupationValue>-1</OccupationValue><OwnerType>Individual</OwnerType><PANNumber>PANDA1234A</PANNumber><AadhaarNumber>121212121212</AadhaarNumber><PermanentAddrCityText><![CDATA[VASHI]]></PermanentAddrCityText><PermanentAddrCityValue>210</PermanentAddrCityValue><PermanentAddrLandmark><![CDATA[Fff]]></PermanentAddrLandmark><PermanentAddrLine1><![CDATA[Ggh]]></PermanentAddrLine1><PermanentAddrLine2><![CDATA[Fff]]></PermanentAddrLine2><PermanentAddrPincodeText>400703</PermanentAddrPincodeText><PermanentAddrPincodeValue>66870</PermanentAddrPincodeValue><PermanentAddrStateText><![CDATA[MAHARASHTRA]]></PermanentAddrStateText><PermanentAddrStateValue>55</PermanentAddrStateValue><PresentAddrCityText><![CDATA[VASHI]]></PresentAddrCityText><PresentAddrCityValue>210</PresentAddrCityValue><PresentAddrLandmark><![CDATA[Fff]]></PresentAddrLandmark><PresentAddrLine1><![CDATA[Ggh]]></PresentAddrLine1><PresentAddrLine2><![CDATA[Fff]]></PresentAddrLine2><PresentAddrPincodeText>400703</PresentAddrPincodeText><PresentAddrPincodeValue>66870</PresentAddrPincodeValue><PresentAddrStateText><![CDATA[MAHARASHTRA]]></PresentAddrStateText><PresentAddrStateValue>55</PresentAddrStateValue><SetAsDefault>true</SetAsDefault><TitleText>Mr.</TitleText><TitleValue>1</TitleValue><isPermanentAsPresent>false</isPermanentAsPresent><GSTApplicable>false</GSTApplicable><CustomerGSTDetails><GSTIN_NO></GSTIN_NO><CONSTITUTION_OF_BUSINESS></CONSTITUTION_OF_BUSINESS><CONSTITUTION_OF_BUSINESS_TEXT></CONSTITUTION_OF_BUSINESS_TEXT><CUSTOMER_TYPE></CUSTOMER_TYPE><CUSTOMER_TYPE_TEXT></CUSTOMER_TYPE_TEXT><PAN_NO></PAN_NO><GST_REGISTRATION_STATUS></GST_REGISTRATION_STATUS><GST_REGISTRATION_STATUS_TEXT></GST_REGISTRATION_STATUS_TEXT></CustomerGSTDetails></CustomerDetails>';
  // JSON request is stored in below property 
  convertedRequestData: {}; showAddOn = false; addOn: any; isProfessional = 'False'; isAdvanture = 'False';
  sumisnured: any; quoteData: any; quoteResponseData: any; quoteRequestData: any; insuredData: any;
  stateData: any; isAddOn: any; proposalRequestData: any; proposalResponseData: any; ppapCovers: any;
  isOtherPol: any; isDeclarationApproved: boolean = true; date: any; plans: any; member1DOB: any; member2DOB: any;
  inputLoadingPlan = false; startDate: any;
  isPedSelected: boolean = false;bmiyesorno: boolean = false;
  constructor(public commonservice: CommonService, public alertController: AlertController, public xml2json: XmlToJsonService, public router: Router, public toastCtrl: ToastController) { }

  ngOnInit() {

    var currentContent = $('body').html();
    console.log("Current Status", currentContent);
    //this.quoteResponse = this.commonservice.responseData;
    this.date = new Date();
    this.convertedRequestData = this.xml2json.xmlToJson(this.requestData);
    //this.proposalResponse = this.quoteResponse.SavePolicy[0];
    localStorage.setItem('isRenewal', 'false');
    this.quoteData = JSON.parse(localStorage.getItem('quoteResponse'));
    this.quoteResponseData = this.quoteData.HealthDetails[0];
    this.quoteRequestData = JSON.parse(localStorage.getItem('quoteRequest'));
    this.proposalRequestData = JSON.parse(localStorage.getItem('proposalRequest'));
    this.insuredData = this.proposalRequestData.Members;
    if (this.quoteRequestData.ProductType == 'PPAP') {
      this.sumisnured = localStorage.getItem('SumInsured');
      this.ppapCovers = JSON.parse(localStorage.getItem('PPAPCovers'));
    }
    else {
      if (this.quoteRequestData.ProductType == 'CHI' || this.quoteRequestData.ProductType == 'ASP') {
        this.sumisnured = this.quoteRequestData.InsuredAmount;
        this.isOtherPol = localStorage.getItem('isOtherPolicyDetails');
        if ((this.quoteRequestData.AddOn1 == 'false' || this.quoteRequestData.AddOn1 == false) && (this.quoteRequestData.AddOn3 == 'false' || this.quoteRequestData.AddOn3 == false) && (this.quoteRequestData.AddOn5 == 'false' || this.quoteRequestData.AddOn3 == false) && (this.quoteRequestData.AddOn6 == 'false' || this.quoteRequestData.AddOn6 == false)) {
          this.isAddOn = 'No';
        }
        else {
          this.isAddOn = 'Yes';
        }
        for (let i = 0; i < this.insuredData.length; i++) {
          if (this.insuredData[i].Ailments.length > 0) {
            this.isPedSelected = true;
            break;
          }
        }
        for (let i = 0; i < this.insuredData.length; i++) {
          let h1, h2, w1;
          if (this.insuredData[i].Height == null) { h1 = 0; h2 = 0; }
          else {
            let temp = this.insuredData[i].Height.split('.'); h1 = parseInt(temp[0]); h2 = parseInt(temp[1]);
          }

          if (this.insuredData[i].Weight == null) { w1 = 0; } else { w1 = parseInt(this.insuredData[i].Weight) }
          let BMIforAdultInt
          if (h1 == 0 && h2 == 0 && w1 == 0) { BMIforAdultInt = 0; }
          else {
            let BMIforAdultDouble = Math.round(w1 / (((((h1 * 12) + h2) * 2.54) / 100) * ((((h1 * 12) + h2) * 2.54) / 100)));
            BMIforAdultInt = BMIforAdultDouble;
          }
          if (BMIforAdultInt < 15 || BMIforAdultInt > 30) {
            this.bmiyesorno = true;
            break;
          }
        }
      }
      if (this.quoteRequestData.ProductType == 'HBOOSTER') {
        this.sumisnured = this.quoteRequestData.SumInsured;
        this.isOtherPol = localStorage.getItem('isOtherPolicyDetails');
        if ((this.quoteRequestData.AddOn1 == 'false' || this.quoteRequestData.AddOn1 == false) && (this.quoteRequestData.AddOn3 == 'false' || this.quoteRequestData.AddOn3 == false) && (this.quoteRequestData.AddOn2 == 'false' || this.quoteRequestData.AddOn2 == false)) {
          this.isAddOn = 'No';
        }
        else {
          this.isAddOn = 'Yes';
        }
      }
      for (let i = 0; i < this.insuredData.length; i++) {
        if (this.insuredData[i].Ailments.length > 0) {
          this.isPedSelected = true;
          break;
        }
      }
      for (let i = 0; i < this.insuredData.length; i++) {
        let h1, h2, w1;
        if (this.insuredData[i].Height == null) { h1 = 0; h2 = 0; }
        else {
          let temp = this.insuredData[i].Height.split('.'); h1 = parseInt(temp[0]); h2 = parseInt(temp[1]);
        }

        if (this.insuredData[i].Weight == null) { w1 = 0; } else { w1 = parseInt(this.insuredData[i].Weight) }
        let BMIforAdultInt
        if (h1 == 0 && h2 == 0 && w1 == 0) { BMIforAdultInt = 0; }
        else {
          let BMIforAdultDouble = Math.round(w1 / (((((h1 * 12) + h2) * 2.54) / 100) * ((((h1 * 12) + h2) * 2.54) / 100)));
          BMIforAdultInt = BMIforAdultDouble;
        }
        if (BMIforAdultInt < 15 || BMIforAdultInt > 30) {
          this.bmiyesorno = true;
          break;
        }
      }

    }


    if (this.quoteRequestData.NoOfAdults = 1) {
      this.member1DOB = moment(this.proposalRequestData.Members[0].DOB).format('DD/MM/YYYY');
      this.member2DOB = "";
    } else {
      this.member1DOB = moment(this.proposalRequestData.Members[0].DOB).format('DD/MM/YYYY');
      this.member2DOB = moment(this.proposalRequestData.Members[1].DOB).format('dd/MM/yyyy');
    }


    this.proposalResponseData = JSON.parse(localStorage.getItem('proposalResponse'));
    this.stateData = JSON.parse(localStorage.getItem('PermanentStateCity'));
  }

  getDeclarationApproval(event: any) {
    this.isDeclarationApproved = event.target.checked
  }

  goToPayment(ev: any) {
    if (this.isDeclarationApproved) {
      let quoteReq = JSON.parse(localStorage.getItem('quoteRequest'));
      let quoteRes = JSON.parse(localStorage.getItem('quoteResponse'));
      let propReq = JSON.parse(localStorage.getItem('proposalRequest'));
      let propRes = JSON.parse(localStorage.getItem('proposalResponse'));
      let custReq = JSON.parse(localStorage.getItem('customerRequest'));
      let custRes = JSON.parse(localStorage.getItem('customerResponse'));
      console.log(quoteReq);
      console.log(quoteRes);
      console.log(propReq);
      console.log(propRes);
      console.log(custReq);
      console.log(custRes);
      if (localStorage.getItem('isPendingPage')) {
        localStorage.removeItem('isPendingPage');
        localStorage.removeItem('pendingNavigation');
      }
      this.router.navigateByUrl('payment');
    }
    else {
      this.presentAlert('Please accept the declaration to move on to payment.');
    }
  }

  async presentAlert(message: any) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  home(ev: any) {
    console.log("Home", ev);
    this.commonservice.goToHome();
  }

  cancel(ev: any) {
    document.getElementById("myNav").style.height = "0%";
  }

  showTravel(ev: any) {
    console.log(ev.target.checked);
    if (!ev.target.checked) {
      document.getElementById("myNav").style.height = "0%";
    } else {
      document.getElementById("myNav").style.height = "100%";
    }
    console.log(this.isTravel);
  }

  getTravelPlan(ev: any) {
    this.inputLoadingPlan = true;
    console.log(ev, this.startDate);
    let body = {
      "GeoCode": "WORLDWIDE",
      "isMultiTrip": true,
      "LeavingIndiaDate": "",
      "PolicyStartDate": moment(this.startDate).format('DD-MM-YYYY'),
      "TravelType": "1",
      "NoOfTravellers": "1",
      "CoverNeededFloater": false,
      "IndividualPlan": true,
      "FamilyPlan": false
    }
    let str = JSON.stringify(body);
    console.log(str);
    this.commonservice.postWithParams('/api/Travel/GetInternationalTravelPlan', str).then((res: any) => {
      console.log("Response", res);
      if (res.StatuMessage == "Success") {
        console.log("Response", res.lstTravelPlans.length);
        if (res.lstTravelPlans.length) {
          this.plans = [];
          res.lstTravelPlans.forEach((plan: any) => {
            this.plans.push(plan);
          });
          this.inputLoadingPlan = false;
        } else {
          this.inputLoadingPlan = false;
          this.presentToast();
        }
      }
    });
  }

  toggleAddOn(ev: any) {
    console.log("Toggle", ev);
    this.showAddOn = !this.showAddOn;
  }

  async presentToast() {
    const toast = await this.toastCtrl.create({
      message: 'No plan available',
      duration: 2000
    });
    toast.present();
  }

  selectAddOn(ev: any) {
    console.log(ev.target.id);
    if (ev.target.id == 'semiProSports') {
      this.addOn = "Semi Profetional Sports";
      this.isProfessional = 'True';
    } else {
      this.addOn = "Advanture Sports";
      this.isAdvanture = 'True';
    }
  }

  selectedPlan(ev: any) {
    console.log(ev.target.value, this.plans);
    this.planType = ev.target.value;
    let planDetails = this.plans.find((x: any) => x.PlanName == ev.target.value);
    this.planId = planDetails.PlanCode;
    console.log(planDetails.PlanCode);
  }

  calculate(form: any) {
    console.log(this.tripDuration, this.noOfMember)
    let body = {
      "UserType": "AGENT",
      "ipaddress": config.ipAddress,
      "IsResIndia": "Yes",
      "IsPlanUpgrade": "No",
      "NoOfTravelers": "1",
      "IsImmigrantVisa": "NO",
      "IsAnnualMultiTrip": "Yes",
      "MaximumDurationPerTrip": this.tripDuration,
      "coverageType": "SINGLE",
      "IsVisitingUSCanadaYes": 'Yes',
      "IsVisitingSCHENGENYes": 'No',
      "IsVisitingOtherCountries": 'No',
      "LeavingIndia": moment(this.startDate).format('DD-MM-YYYY'),
      "ReturnIndia": "",
      "TripDuration": this.tripDuration,
      "IsIndividualPlan": "True",
      "IsFamilyPlan": "False",
      "IsCoverNeededIndividual": "False",
      "IsCoverNeededFloater": "False",
      "Members": [
        {
          "DateOfBirth": moment(this.member1DOB).format('DD-MMM-YYYY'),
          "PlanID": this.planId
        }
      ],
      "planIds": this.planId + ',',
      "IsProfessionalSportCover": this.isProfessional,
      "IsAdventureSportCover": this.isAdvanture,
      "GSTStateCode": this.quoteRequestData.GSTStateCode,
      "GSTStateName": this.quoteRequestData.GSTStateName,
      "isGSTRegistered": this.quoteRequestData.isGSTRegistered
    }

    console.log(body);
    let str = JSON.stringify(body);

    this.commonservice.postWithParams('/api/Travel/ITPFCalculatePremium', str).then((res: any) => {
      console.log("response for Travel", res);
      localStorage.setItem('CHITravelProp', JSON.stringify(res));
    });
  }

  changeTrip(ev: any) {
    console.log(ev);
  }

  reCalculate(form: any) {
    console.log(form);
  }


  // showPlanDetails(details: any) {
  //   this.isShowPlanDetails = !details;
  // }
  // showApplicantDetails(details: any) {
  //   this.isShowApplicantDetails = !details;
  // }
  // showGSTDetails(details: any) {
  //   this.isShowGSTDetails = !details;
  // }
  // showMedicalDetails(details: any) {
  //   this.isShowMedicalDetails = !details;
  // }
}
