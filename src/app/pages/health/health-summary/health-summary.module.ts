import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HealthSummaryPage } from './health-summary.page';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule  } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';

const routes: Routes = [
  {
    path: '',
    component: HealthSummaryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HealthSummaryPage]
})
export class HealthSummaryPageModule {}
