import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController, ModalController, ToastController } from '@ionic/angular';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { CommonService } from 'src/app/services/common.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as _ from 'underscore';
import * as moment from 'moment';
import { Location } from '@angular/common';
import { PedQuestionaireComponent } from '../ped-questionaire/ped-questionaire.component';
import { config } from 'src/app/config.properties';
import { NetworkService } from 'src/app/services/network.service';
import * as uuid from 'uuid';
import { Storage } from '@ionic/storage';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-cal-health-premium',
  templateUrl: './cal-health-premium.page.html',
  styleUrls: ['./cal-health-premium.page.scss'],
})
export class CalHealthPremiumPage implements OnInit {

  agentData: any; showCHI: boolean = false; showHealthBooster: boolean = false; showPPAP: boolean = false;
  loading: any; dataValues: any; stateMaster: any; quoteForm: FormGroup; healthMasterResponse: any;
  productCode: any; productName: any; noOfAudult: any; noOfChild: any; adult1checked: boolean = false;
  adult2checked: boolean = false; adultArray = []; adultChildMap: any; adultAgeMap: any; childArray: any;
  adultAgeArray: any; showAdultTable = false; adult2AgeArray = []; quateTotalPremium: any; toggleButton = false;
  showAddOnCover1 = false; showAddOnCover3 = false; showAddOnCover5 = false; showAddOnCover6 = false;
  addOnCoverBasedOnAge = false; button1 = false; button2 = false; button3 = false; subilimitA = false;
  subilimitB = false; subilimitC = false; subilimitN = false; isJammuKashmir = false; isKeralaCess = false;
  showProduct: any; stateid: any; public show: boolean = false; public show1: boolean = false; public show2: boolean = false;
  public show3: boolean = false; annualSumInsuredArray = []; customPopoverOptions: any = {}; loadingHB: any; agentDataHB: any;
  public showHB: boolean = false; public showaddon2HB: boolean = false; public showaddon3HB: boolean = false;
  quoteFormHB: FormGroup; stateMasterHB: any; healthMasterResponseHB: any; productCodeHB: any; vdMap: any;
  vDMapArray: any; isJammuKashmirHB = false; isKeralaCessHB = false; dobHB: any; dob1HB: string; quateTotalPremiumHB: any;
  dataValuesHB = []; noOfAudultHB = 0; noOfChildHB: any; noOfAudult1HB = 0; adultChildMapHB: any;
  childArrayHB: any; submittedHB = false; public ageHB = 21; adultValueHB: any; showAddOn1HB = false;
  i: any; productvalHB: string; adultAgeMapHB: any; customPopoverOptionsHB: any = {}; statusHB = [];
  memmaxDOBHB: any; meminDOBHB: any; stateidHB: any; showSliderHB: boolean = false; adult1checkedHB: boolean = false;
  adult2checkedHB: boolean = false; index: any = 0; nextAmountPremRequ: any; nextAmountPremRes: any;
  selectedPP: any; moreDetails: boolean = false; isPPAPPC: boolean = false; isPPA: boolean = false; masterData: any;
  isPPC: boolean = false; selectedSubType: string; selectedState: any; selectedOccupationType: string;
  isEarning: boolean = false; annual_income: any; dob: string; policy_duration: any; sum_insured: any;
  states: any; masterDataPPAP: any; policyData: any; occType: string; insured_options: any;
  canShowOptions: boolean = false; canShowOptionsDetails: boolean = false; ppa_sum_insured: string; ppc_sum_insured: any;
  incomeData: any; insuredArray = []; sumInsuredArray: any; optionsArray = []; OptionDetails = []; OptionsDetailsArray = [];
  payoutAmount = []; payoutTenure: any; showPPASumInsured: boolean = false; payout_amount: any; payout_tenure: any;
  ppaPlancode: any; lumpsum = []; lump_sum_payment: any; showPPCSumInsured: boolean = false; disableQuote: boolean = true;
  salariedType: any; datePickerObj: any; saveQuoteData: any; maxRiskDate: any; isRegWithGST: boolean = false;
  isQuoteDetails: boolean = true; isPremiumDetails: boolean = true; isQuoteGenerated: boolean = false;
  isKeralaCessEnabled: any; isRegWithGSTCHI: boolean = false; isRegWithGSTHB: boolean = false;
  isChecked = false; isChecked1 = false; nextSumInsured: any; oldQuoteRes: any;
  showSlider: boolean = false; iskeralaCessFeature: any; isRecalculate: boolean = false;
  showMaxInsured = true; inputLoadingPlan = false;
  @ViewChild('tenureCheck') tenureCheck;
  @ViewChild('tenureCheck1') tenureCheck1;
  @ViewChild('chirange') chirange;
  // @ViewChild('tenureCheck2')tenureCheck2;

  //Premium variables
  isHealthBooster = false; isCHI = false; isPPAP = false; hbyearwiseData: any; responsetemp2: any; responseTemp: any;
  quoteResponseForOneYear: any; quoteResponseForTwoYear: any; SubProductName: any; HBProductName: any;
  SoftLessCopyValue: any; quoteResponse: any; quoteRequest: any; boosterResponse: any; productType: any;
  isCHIRecalculate: boolean = false; isHBoosterRecalculate: boolean = false; isPPAPRecalculate: boolean = false;

  showHospifund: boolean = false;
  isNoOfMembersChanged: boolean = false;
  siData: any = 500; noOfDays: any = 5; isHosifund: boolean = true;
  siDataValues = []; noOfDaysValues = []; from_siData: any; showEBCVCFooter: boolean = false;
  showEBCCFooter: boolean = false; isLoader: boolean = false;
  dobInsured: any; quoteData: any; dobValidator: any;
  baseBenefit1: boolean = false; baseBenefit2: boolean = false;
  extnBenefit1: boolean = false; extnBenefit2: boolean = false; extnBenefit3: boolean = false;
  extnBenefit4: boolean = false; extnBenefit5: boolean = false; extnBenefit6: boolean = false;
  extnBenefit7: boolean = false; extnBenefit8: boolean = false; extnBenefit9: boolean = false;
  extnBenefit10: boolean = false; extnBenefit11: boolean = false; extnBenefit12: boolean = false;
  extnBenefit13: boolean = false; isDisabled: boolean = true;
  premium1: boolean = false; premium2: boolean = false; premium3: boolean = false;
  showPremiumPanel: boolean = false; coverDetails: any; sumofBaseBenefits: any; sumofExtensionBenefits: any;
  base1: any; tax1: any; totalPremium1: any; totalPremiuminRs1: any;
  base2: any; tax2: any; totalPremium2: any; totalPremiuminRs2: any;
  base3: any; tax3: any; totalPremium3: any; totalPremiuminRs3: any;
  showSubLimit: boolean = false;
  showannualsum: boolean = false; showinsuredTable: boolean = false; showNextButton: boolean = false;
  showPolicyTenure: boolean = false;
  selectedOption: any; premiumType: any; authData: any; minHospiDate: any; maxHospiDate: any; showHospifundPremium = false;
  errorCount: any; plan: any
  addon6: boolean;
  addon5: boolean;
  toggleButton1: boolean = false;

 //Arogya Sanjeevani declarations
 showSanjeevani: boolean = false;
 quoteSanjeevaniForm: FormGroup;
 showAdultASP: boolean = false; 
 showChildASP: boolean = false; 
 showSilderASP: boolean = false; 
 showPolicyTenureforASP: boolean = false;
 showAnnualSumInsuredforASP: boolean = false; 
 selectedQuoteRes = []; 
 selectednextQuoteRes = [];
 @ViewChild('chirangeASP') chirangeASP;
 @ViewChild('clickAddon6No') clickAddon6No;

 healthMasterResponseASP: any;
 showInsuredTable: boolean = false;
 button4: boolean = false; button5 = false; button6 = false; isASP: boolean = false;
 selctedAmountPremReq: any;
 showDateofBirthDiv: boolean = true;
 inputLoadingPlan1: boolean = true; 
 clickEventsubscription: Subscription;


  constructor(private routes: Router,
    public cs: CommonService,
    public xml2Json: XmlToJsonService,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public router: Router,
    public alertCtrl: AlertController,
    private _location: Location,
    public modalController: ModalController,
    public network: NetworkService,
    private storage: Storage
  ) {
    this.noOfAudult = 0;
    this.noOfChild = 0;

  }

  ngOnInit() {
    console.log("Online", this.network.isConnected);
    this.createQuoteForm();
    this.getStateMasterHB();
    this.createQuoteFormHB();
    console.log('ONLINE');
    this.agentData = JSON.parse(localStorage.getItem('userData'));
    console.log("AgentData", this.agentData);
    this.iskeralaCessFeature = localStorage.getItem('isKeralaCessEnabled');
    this.plan = localStorage.getItem('custplan');
    //this.stateMasterHB = JSON.parse(localStorage.getItem('stateMaster'));
    // this.stateMasterHB = _.sortBy(this.stateMasterHB, 'StateName');
    // let statename = this.stateMasterHB.find(x => x.StateId == 55).StateName;
    // this.quoteFormHB.patchValue({ 'state': statename });
    // this.quoteForm.patchValue({ 'state': statename });
    // this.selectedState = 55;
    if (this.iskeralaCessFeature == 'Y') { this.isKeralaCessEnabled = true; } else { this.isKeralaCessEnabled = false; }
    console.log(this.agentData.MappedProduct.Health.isHealthBoosterMapped);
    localStorage.removeItem('requestForProp');
    localStorage.removeItem('Request');
    if (this.agentData.MappedProduct.Health.isHealthCHIMapped && this.plan == 'HEALTHCHI' || this.plan == 'HEALTHCHI-IHEALTH') {
      this.showCHI = true; this.showHealthBooster = false; this.showPPAP = false; this.showHospifund = false;
    }
    else if (this.agentData.MappedProduct.Health.isHealthBoosterMapped && this.plan == 'HEALTHBOOSTER') {
      this.showCHI = false; this.showHealthBooster = true; this.showPPAP = false; this.showHospifund = false;
    }
    else if (this.agentData.MappedProduct.Health.isHealthPPAPMapped && this.plan == 'HEALTHPPAP') {
      this.showCHI = false; this.showHealthBooster = false; this.showPPAP = true; this.showHospifund = false;
    }
    else if (this.agentData.MappedProduct.Health.isHospifundMapped && this.plan == 'HEALTHHOSPIFUND') {
      this.showCHI = false; this.showHealthBooster = false; this.showPPAP = false; this.showHospifund = true;
    }   
     else if (this.agentData.MappedProduct.Health.isHealthASPMapped && this.plan == 'HEALTHASP') {
      this.showCHI = false; this.showHealthBooster = false; this.showPPAP = false; this.showHospifund = false;this.showSanjeevani = true;
      this.createArogyaSanjeevaniForm();
    }
    else {
      this.presentAlert('Currently this product is not available, Kindly contact your partner');
    }
    if (this.plan == 'HEALTHCHI-IHEALTH') {
      this.quoteForm.patchValue({ 'productName': "iHealth" });
      this.productCode = 17;
      this.getHealthMaster();
    }
    this.selectedSubType = '';
    this.selectedOccupationType = '';
    this.maxRiskDate = moment().format('YYYY-MM-DD');
    var today = new Date();
    this.memmaxDOBHB = new Date(today.setFullYear(new Date().getFullYear() - 21));
    this.meminDOBHB = new Date(today.setFullYear(new Date().getFullYear() - 100));
    this.memmaxDOBHB = moment(this.memmaxDOBHB).format('YYYY-MM-DD');
    this.meminDOBHB = moment(this.meminDOBHB).format('YYYY-MM-DD');

    let sinsuredDOB = new Date();
    let einsuredDOB = new Date();
    let min_date = new Date();
    min_date.setFullYear(min_date.getFullYear() - 66);
    this.minHospiDate = new Date(min_date.setDate(min_date.getDate() + 1));
    // this.minHospiDate = new Date(new Date().setFullYear(new Date().getFullYear() - 56));
    this.maxHospiDate = new Date(new Date().setDate(new Date().getDate() - 91));
    this.siDataValues = [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 6000, 7000, 8000, 9000, 10000];
    this.noOfDaysValues = [5, 10, 15, 30, 60, 90, 180];
    this.from_siData = 500;
    this.callTokenService(); this.selectedOption = '';
  }

  ngOnChanges() {
    console.log('View Updated');
  }

  callTokenService() {
    this.cs.getAuthorizationData().then(res => {
      this.authData = res;
      this.cs.hospifundToken = this.authData;
      localStorage.setItem('HospifundToken', this.authData);
    }, err => {
      this.presentAlert('Sorry Something went wrong');
    });
  }

  navigate(type: any) {
    if (type == 0) {
      this.showCHI = true; this.showHealthBooster = false;
      this.showPPAP = false; this.showHospifund = false;this.showSanjeevani = false;
    }
    else if (type == 1) {
      this.showCHI = false; this.showHealthBooster = true;
      this.showPPAP = false; this.showHospifund = false;this.showSanjeevani = false;
    }
    else if (type == 2) {
      this.showCHI = false; this.showHealthBooster = false;
      this.showPPAP = true; this.showHospifund = false;this.showSanjeevani = false;
    }
    else if(type == 3) {
      this.showCHI = false; this.showHealthBooster = false;
      this.showPPAP = false; this.showHospifund = true;this.showSanjeevani = false;
    } else {
      this.showCHI = false; this.showHealthBooster = false;
      this.showPPAP = false; this.showHospifund = false; this.showSanjeevani = true;
      // this.refreshCHISection(); 
      // this.refreshHBSection(); 
      // this.refreshPPAPSection();
      // this.refreshHospifundSection();
       this.createArogyaSanjeevaniForm();
    }
  }
  // CHI Code Begins //

  onFinishSliding(e: any) {
    console.log(e.from_value);
  }
  isGSTRegCHI(val) {
    if (val) {
      this.isRegWithGSTCHI = true;
    } else {
      this.isRegWithGSTCHI = false;
    }
  }
  isGSTRegHB(val) {
    if (val) {
      this.isRegWithGSTHB = true;
    } else {
      this.isRegWithGSTHB = false;
    }
  }

  async presentAlert(message: any) {
    const alert = await this.alertCtrl.create({
      header: 'ICICI Lombard',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  getAdult(ev: any) {
    if (this.noOfAudult != ev.target.value) {
      console.log('Adult: ' + this.noOfAudult + 'Target: ' + ev.target.value);
    }
    this.isQuoteGenerated = false; this.showPolicyTenure = true; this.isPremiumDetails = false;
    this.noOfAudult = ev.target.value; this.showSlider = true; this.showSubLimit = true; this.showannualsum = true;
    this.showinsuredTable = true; this.showNextButton = true;this.toggleButton=false;

    let audult1 = document.getElementById('oneAdult1') as HTMLInputElement;
    let audult2 = document.getElementById('oneAdult2') as HTMLInputElement;
    let child1 = document.getElementById('oneChild1') as HTMLInputElement;
    let child2 = document.getElementById('oneChild2') as HTMLInputElement;
    let child3 = document.getElementById('oneChild3') as HTMLInputElement;

    if (this.noOfAudult == 2 && this.noOfChild == 1) {
      audult1.checked = true;
      audult2.checked = true;
      this.noOfChild = 0;
      // this.refreshForm(this.productName);
    } else
      if (this.noOfAudult == 2) {
        audult1.checked = true;
        audult2.checked = true;
        // this.refreshForm(this.productName)
      }
      else if (this.noOfAudult == 1) {
        if (audult1.checked == true && audult2.checked == false && this.noOfChild == 1 && this.noOfAudult == 1) {
          this.noOfAudult = 1;
          this.noOfChild = 0;
          audult1.checked = true;
          audult2.checked = false;
        } else
          if (audult1.checked == true && audult2.checked == false && this.noOfAudult == 1) {
            this.noOfAudult = 1;
            audult1.checked = true;
            audult2.checked = false;
          } else
            if (audult1.checked == false && audult2.checked == true && this.noOfAudult == 1) {
              this.noOfAudult = 1;
              audult2.checked = false;
              audult1.checked = true;
            } else {
              this.noOfAudult = 0;
              child1.checked = true;
              child2.checked = false;
            }
      }
      else if (this.noOfAudult == 0) {
        audult1.checked = false;
        audult2.checked = false;
      } else {
        this.noOfAudult = 0;
        audult1.checked = false;
        audult2.checked = false;
      }
    if (this.noOfAudult > 0) {
      this.showAdultTable = true;
      this.addOnCoverBasedOnAge = true;
    } else {
      this.showAdultTable = false;
    }
    let child = this.adultChildMap.get(ev.target.value);
    this.childArray = child;
    let adultAge = this.adultAgeMap.get(ev.target.value);
    this.adultAgeArray = adultAge;
    if (this.noOfAudult == 2 && ev.target.checked == true) {
      this.childArray = this.adultChildMap.get(ev.target.value);
      this.healthMasterResponse.AdultResponse.forEach((adult: any) => {
        adult.checked = true;
      })
    }
    if (this.noOfAudult == 2 && ev.target.checked == false) {
      this.childArray = this.adultChildMap.get("1");
    }
    else if (this.noOfAudult == 1 && ev.target.checked == true) {
      this.healthMasterResponse.AdultResponse.forEach((adult: any) => {
        if (adult.AdultValue == 1) {
          adult.checked = true;
          this.childArray = this.adultChildMap.get("1");
        }
      })
    }
    else if (this.noOfAudult == 1 && ev.target.checked == false) {
      this.healthMasterResponse.AdultResponse.forEach((adult: any) => {
        if (adult.AdultValue == 1) {
          adult.checked = false;
        }
        this.childArray = [{ NoofKids: "1", SumInsuredDetails: null }]
      })
      // if(this.noOfAudult1HB > 1) {
      //   this.isNoOfMembersChanged = true;
      // }
    }

    // if (this.adultAgeArray && this.noOfAudult > 0) {
    //   let index = this.adultAgeArray.findIndex((data: any) => data.AgeValue == event.from_value);
    //   this.adult2AgeArray = [];
    //   for (let i = 0; i <= index; i++) {
    //     this.adult2AgeArray.push(this.adultAgeArray[i]);
    //   }
    // }

    console.log(this.adultAgeArray);
    if (this.noOfAudult > 0) {
      this.dataValues = [];
      this.adultAgeArray.forEach((element: any) => {
        this.dataValues.push(element.AgeValue);
      });
      this.quoteForm.patchValue({ 'eldestMemAge': this.dataValues[0] });
      setTimeout(() => {
        this.chirange.update({ from: this.dataValues[0], to: this.dataValues[this.dataValues.length - 1] });
      }, 500);
      this.quoteForm.patchValue({ 'annualSumInsured': '' });
      this.adult2AgeArray = [];
      this.adult2AgeArray.push(this.adultAgeArray[0]);
    } else {
      this.noOfChildHB = 1;
      this.noOfChild = 1;
      this.quoteForm.patchValue({ 'childNo': "1" });
      this.dataValues = ['06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'];
      // setTimeout(() => {
      //   this.chirange.update({ from: this.dataValues[0], to: this.dataValues[this.dataValues.length - 1] });
      // }, 500);
      this.quoteForm.patchValue({ 'annualSumInsured': '' });
    }

    console.log("CHI No Of Adult", this.noOfAudult);
    console.log("CHI No Of Child", this.noOfChild);
    this.button1 = true; this.subilimitN = true;
    this.quoteForm.patchValue({ "policyTenure": "1" });
    this.quoteForm.patchValue({ "subLimit": null });
  }

  getChild(ev: any, child: any) {
    //this.isNoOfMembersChanged = true;
    let audult1 = document.getElementById('oneAdult1') as HTMLInputElement;
    let child1 = document.getElementById('oneChild1') as HTMLInputElement;
    let child2 = document.getElementById('oneChild2') as HTMLInputElement;
    let child3 = document.getElementById('oneChild3') as HTMLInputElement;
    this.noOfChild = child; this.showSlider = true; this.showPolicyTenure = true; this.showSubLimit = true; this.showannualsum = true;
    this.showinsuredTable = true; this.showAddOnCover1 = true; this.showNextButton = true;this.toggleButton=false;


    this.button1 = true; this.subilimitN = true;
    this.quoteForm.patchValue({ "policyTenure": "1" });
    this.quoteForm.patchValue({ "subLimit": null });

    if (this.noOfAudult == "0" && this.noOfChild == "2") {
      this.presentAlert("Cannot Select 2 Child");
      this.showMaxInsured = false;
      this.nextSumInsured = false; this.isChecked = false;
      this.showSlider = false; this.showPolicyTenure = false; this.showSubLimit = false; this.showannualsum = false
      this.showinsuredTable = false; this.showNextButton = false; this.showAddOnCover1 = false;
    }
    console.log(this.noOfAudult, this.noOfChild, child1, child2, child3);
    if (this.noOfAudult == undefined || this.noOfAudult == null || this.noOfAudult < 1) {
      this.quoteForm.patchValue({ 'childNo': "1" });
      this.dataValues = ['06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'];
    }
    if (this.noOfChild == 1 && child1.checked == false && child2.checked == false) {
      console.log("Child", this.noOfChild);
      child1.checked = false;
      this.noOfChild = 0
      if (child2 != null) { child2.checked = false }
      if (child3 != null) { child3.checked = false }
    } else if (this.noOfChild == 1) {
      child1.checked = true;
      if (child2 != null) { child2.checked = false }
      if (child3 != null) { child3.checked = false }
    } else if (this.noOfChild == 2) {
      child1.checked = true;
      child2.checked = true;
      if (child3 != null) { child3.checked = false }
    } else if (this.noOfChild == 3) {
      child1.checked = true;
      child2.checked = true;
      child3.checked = true;
    } else {
      child2.checked = false;
      if (child3 != null) { child3.checked = false }
    }
    if (this.noOfAudult == 0) {
      this.quoteForm.patchValue({ 'eldestMemAge': '06' });
      this.quoteForm.patchValue({ 'memberAge1': '6' });
    }
    this.showAddOnCover6 = false; this.showAddOnCover1 = false;
    this.show1 = false; this.show2 = false;

  }

  addOnYesNo(event: any) {
    let val = event.target.value;
    this.toggleButton = !this.toggleButton;
    console.log(event);
    this.quoteForm.patchValue({ "memberAge1": this.quoteForm.value.eldestMemAge });
    if (this.quoteForm.value.memberAge2 == null || this.quoteForm.value.memberAge2 == undefined) {
      this.quoteForm.value.memberAge2 = '';
    }
    if (this.quoteForm.value.addOnCover6 == false) {
      this.quateCalculation(this.quoteForm.value, val).then(() => {
        this.quateCalculationCHI1(this.nextAmountPremRequ);
        this.isChecked = true;
        this.isChecked1 = false;
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        // if (this.isChecked == true) {
        //   this.isChecked = false;
        // }
        // else if (this.isChecked1 == true) {
        //   this.isChecked1 = false;
        // }
      })
    }
  }

  isAdult(ev: any) {
    this.isRecalculate = false;
  }

  selectTenure(ev: any) {
    console.log("Tenure", ev.target.innerText);
    if (this.productCode == 14 || this.productCode == 17) {
      if (this.quoteForm.value.annualSumInsured == undefined || this.quoteForm.value.annualSumInsured == '' || this.quoteForm.value.annualSumInsured == undefined == null) {
        this.cs.showLoader = false;
        if (ev.target.innerText == '1 Year') {
          this.quoteForm.patchValue({ "policyTenure": "1" });
          this.button1 = true; this.button2 = false; this.button3 = false;
        } else if (ev.target.innerText == '2 Years') {
          this.button1 = false; this.button2 = true; this.button3 = false;
          this.quoteForm.patchValue({ "policyTenure": "2" });
        } else {
          this.button1 = false; this.button2 = false; this.button3 = true;
          this.quoteForm.patchValue({ "policyTenure": "3" });
        }
      }
      else {
        if (ev.target.innerText == '1 Year') {
          this.quoteForm.patchValue({ "policyTenure": "1" });
          this.button1 = true; this.button2 = false; this.button3 = false;
          this.quateCalculation(this.quoteForm.value, 'No').then(() => {
            this.quateCalculationCHI1(this.nextAmountPremRequ);
            this.isChecked = true;
          })
          this.isQuoteGenerated = false;
          this.isPremiumDetails = false;
          if (this.isChecked == true) {
            this.isChecked = false;
          }
          else if (this.isChecked1 == true) {
            this.isChecked1 = false;
          }

        } else if (ev.target.innerText == '2 Years') {
          this.button1 = false; this.button2 = true; this.button3 = false;
          this.quoteForm.patchValue({ "policyTenure": "2" });
          this.quateCalculation(this.quoteForm.value, 'No').then(() => {
            this.quateCalculationCHI1(this.nextAmountPremRequ);
            this.isChecked = true;

          })
          this.isQuoteGenerated = false;
          this.isPremiumDetails = false;
          if (this.isChecked == true) {
            this.isChecked = false;
          } else if (this.isChecked1 == true) {
            this.isChecked1 = false;
          }
        } else {
          this.button1 = false; this.button2 = false; this.button3 = true;
          this.quoteForm.patchValue({ "policyTenure": "3" });
          this.quateCalculation(this.quoteForm.value, 'No').then(() => {
            this.quateCalculationCHI1(this.nextAmountPremRequ);
            this.isChecked = true;
          })
          this.isQuoteGenerated = false;
          this.isPremiumDetails = false;
          if (this.isChecked == true) {
            this.isChecked = false;
          }
          else if (this.isChecked1 == true) {
            this.isChecked1 = false;
          }
        }
      }
    }
    else {
      if (ev.target.innerText == '1 Year') {
        this.quoteForm.patchValue({ "policyTenure": "1" });
        this.button1 = true; this.button2 = false; this.button3 = false;
      } else if (ev.target.innerText == '2 Years') {
        this.button1 = false; this.button2 = true; this.button3 = false;
        this.quoteForm.patchValue({ "policyTenure": "2" });
      } else {
        this.button1 = false; this.button2 = false; this.button3 = true;
        this.quoteForm.patchValue({ "policyTenure": "3" });
      }
    }
  }

  selectSublimit(ev: any) {
    let subLimit = [{ name: 'A' }, { name: 'B' }, { name: 'C' }];
    console.log("Sublimit", ev.target.innerText);
    if (ev.target.innerText == 'A') {
      this.subilimitA = true; this.subilimitB = false; this.subilimitC = false; this.subilimitN = false;
      this.quoteForm.patchValue({ "subLimit": "A" });
      this.quateCalculation(this.quoteForm.value, 'No').then(() => {
        this.quateCalculationCHI1(this.nextAmountPremRequ);
        this.isChecked = true;

      })
      this.isQuoteGenerated = false;
      this.isPremiumDetails = false;
      if (this.isChecked == true) {
        this.isChecked = false;
      }
      else if (this.isChecked1 == true) {
        this.isChecked1 = false;
      }
    } else if (ev.target.innerText == 'B') {
      this.subilimitA = false; this.subilimitB = true; this.subilimitC = false; this.subilimitN = false;
      this.quoteForm.patchValue({ "subLimit": "B" });
      this.quateCalculation(this.quoteForm.value, 'No').then(() => {
        this.quateCalculationCHI1(this.nextAmountPremRequ);
        this.isChecked = true;

      })
      this.isQuoteGenerated = false;
      this.isPremiumDetails = false;
      if (this.isChecked == true) {
        this.isChecked = false;
      }
      else if (this.isChecked1 == true) {
        this.isChecked1 = false;
      }
    } else if (ev.target.innerText == 'C') {
      this.subilimitA = false; this.subilimitB = false; this.subilimitC = true; this.subilimitN = false;
      this.quoteForm.patchValue({ "subLimit": "C" });
      this.quateCalculation(this.quoteForm.value, 'No').then(() => {
        this.quateCalculationCHI1(this.nextAmountPremRequ);
        this.isChecked = true;

      })
      this.isQuoteGenerated = false;
      this.isPremiumDetails = false;
      if (this.isChecked == true) {
        this.isChecked = false;
      }
      else if (this.isChecked1 == true) {
        this.isChecked1 = false;
      }
    } else {
      this.subilimitA = false; this.subilimitB = false; this.subilimitC = false; this.subilimitN = true;
      this.quoteForm.patchValue({ "subLimit": null });
      this.quateCalculation(this.quoteForm.value, 'No').then(() => {
        this.quateCalculationCHI1(this.nextAmountPremRequ);
        this.isChecked = true;

      })
      this.isQuoteGenerated = false;
      this.isPremiumDetails = false;
      if (this.isChecked == true) {
        this.isChecked = false;
      }
      else if (this.isChecked1 == true) {
        this.isChecked1 = false;
      }
    }
  }



  // getStateMaster() {
  //   this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then((res: any) => {
  //     this.stateMaster = res;
  //     this.stateMaster = _.sortBy(this.stateMaster, 'StateName');
  //     let statename = this.stateMaster.find(x => x.StateId == 55).StateName;
  //     this.quoteForm.patchValue({ 'state': statename });
  //   });
  // }

  createQuoteForm() {
    this.quoteForm = new FormGroup({
      productName: new FormControl(''),
      state: new FormControl(''),
      adultNo: new FormControl(),
      childNo: new FormControl(),
      annualSumInsured: new FormControl(''),
      policyTenure: new FormControl(),
      eldestMemAge: new FormControl(),
      subLimit: new FormControl(),
      addOnCover1: new FormControl(),
      addOnCover3: new FormControl(),
      addOnCover5: new FormControl(),
      addOnCover6: new FormControl(),
      memberAge1: new FormControl(),
      memberAge2: new FormControl('')
    });
  }

  refreshForm(prodName: any) {
    this.addOnCoverBasedOnAge = false; this.showAdultTable = false;
    this.button1 = false; this.button2 = false; this.button3 = false;
    this.dataValues = [];
    this.subilimitA = false; this.subilimitB = false; this.subilimitC = false; this.subilimitN = false;
    this.quoteForm = new FormGroup({
      productName: new FormControl(prodName),
      state: new FormControl(),
      adultNo: new FormControl(),
      childNo: new FormControl(),
      annualSumInsured: new FormControl(''),
      policyTenure: new FormControl(),
      eldestMemAge: new FormControl(),
      subLimit: new FormControl(),
      addOnCover1: new FormControl(),
      addOnCover3: new FormControl(),
      addOnCover5: new FormControl(),
      addOnCover6: new FormControl(),
      memberAge1: new FormControl(),
      memberAge2: new FormControl('')
    });
  }

  getProductName(event: any): Promise<any> {
    console.log(this.quoteForm);
    return new Promise((resolve: any) => {
      this.isQuoteGenerated = false;
      this.isPremiumDetails = false;
      if (this.isChecked == true) {
        this.isChecked = false;
      }
      else if (this.isChecked1 == true) {
        this.isChecked1 = false;
      }
      this.refreshForm(event.target.value);
      this.showSlider = false;
      console.log('Values', event.target.value);
      this.productName = event.target.value;
      let statename = this.stateMasterHB.find(x => x.StateId == 55).StateName;
      this.quoteForm.patchValue({ 'state': statename });
      if (event.target.value == "PROTECT") { this.productCode = 14; } else
        if (event.target.value == "PROTECTPLUS") { this.productCode = 12; } else
          if (event.target.value == "SMART") { this.productCode = 16; } else
            if (event.target.value == "SMARTPLUS") { this.productCode = 15; } else
              if (event.target.value == "iHealth") { this.productCode = 17; }
      this.getHealthMaster();
      this.quoteForm.patchValue({ 'subLimit': '' });
      this.setAddOnCover(event.target.value);
      // this.loading.dismiss();
      //this.refreshPage();
      resolve();
    });

  }

  getState(e: any) {
    console.log(e.target.value);
    if (e.target.value == 'Kerala' || e.target.value == 'KERALA' || e.target.value == 'kerala') {
      this.isKeralaCess = true;
    }
    else {
      this.isKeralaCess = false; this.isRegWithGSTCHI = false;
    }
  }

  getHealthMaster() {
    this.cs.showLoader = true;
    let req = {
      "UserID": this.agentData.AgentID,
      "SubType": this.productCode
    }
    let str = JSON.stringify(req);
    this.cs.postWithParams('/api/healthmaster/HealthPlanMaster', str).then((res: any) => {
      console.log("CHI HealthplanMaster", res);
      if (res.StatusCode == 1) {
        this.healthMasterResponse = res;
        this.adultChildMap = new Map();
        this.adultAgeMap = new Map();
        this.healthMasterResponse.AdultResponse.forEach((adult: any) => {
          this.adultChildMap.set(adult.AdultValue, adult.ChildDetails);
          this.adultAgeMap.set(adult.AdultValue, adult.AgeEldestDetails);
        });

        this.childArray = [{
          NoofKids: "1",
          SumInsuredDetails: null
        }]
        this.noOfChildHB = "0";

        this.cs.showLoader = false;
      } else {
        console.log("Something went wrong")
      }
    }).catch((err: any) => {
      console.log("Error", err);
    });
  }

  showFormValidate(form: any) {
    let checkBox
    let checkBox1
    if (document.getElementById('annSumins5') as HTMLInputElement) {
      checkBox = document.getElementById('annSumins5') as HTMLInputElement;
    }

    if (document.getElementById('annSumins5') as HTMLInputElement) {
      checkBox1 = document.getElementById('annSumins6') as HTMLInputElement;
    }

    let message: any;
    if (!form.productName) {
      this.presentAlert("Please select Product");
      return false;
    } else if (!form.state) {
      this.presentAlert("Please select State");
      return false;
    } else if ((this.noOfAudultHB <= 0 || this.noOfAudultHB == undefined) &&
      (this.noOfChildHB <= 0 || this.noOfChildHB == undefined) &&
      (this.noOfAudult <= 0 || this.noOfAudult == undefined) &&
      (this.noOfChild <= 0 || this.noOfChild == undefined)) {
      this.presentAlert("Please select atleast 1 Adult or 1 Child");
      return false;
    } else if (!form.eldestMemAge) {
      this.presentAlert("Please select Member Age Range");
      return false;
    } else if (!form.policyTenure) {
      this.presentAlert("Please select Policy Tenure");
      return false;
    } else if (!form.annualSumInsured) {
      this.presentAlert("Please select Annual Sum Insured");
      return false;
    }

    if ((this.show2 || this.show3) && !checkBox.checked && (checkBox1 != null ? !checkBox1.checked : !checkBox.checked)) {
      this.presentAlert("Select Member for Add On Cover");
      return false;
    } else if (this.isNoOfMembersChanged) {
      this.presentAlert("You have changed No of Members, Please recalculate first");
      return false;
    } else if ((this.show2 || this.show3) && this.isRecalculate == false && (checkBox.checked || checkBox1.checked)) {
      this.presentAlert("You have changed Add-on Cover, please recalculate first");
      return false;
    } else {
      return true;
    }

  }

  toggle() {
    this.show = !this.show;
    console.log(this.show);
    this.quoteForm.patchValue({ addOnCover1: this.show });
    this.quateCalculation(this.quoteForm.value, 'No').then(() => {
      this.quateCalculationCHI1(this.nextAmountPremRequ);
      this.isChecked = true;

      this.cs.showLoader = false;
    })
    this.isQuoteGenerated = false;
    this.isPremiumDetails = false;
    if (this.isChecked == true) {
      this.isChecked = false;
    }
    else if (this.isChecked1 == true) {
      this.isChecked1 = false;
    }
  }

  toggle1() {
    this.show1 = !this.show1;
    console.log(this.show1);
    this.quoteForm.patchValue({ addOnCover3: this.show1 });
  }

  toggle2() {
    this.showAddOnCover6
    this.showAdultTable
    this.addOnCoverBasedOnAge
    this.toggleButton1
    this.toggleButton

    this.showAddOnCover5
    this.showAdultTable
    this.addOnCoverBasedOnAge
    this.toggleButton
    this.toggleButton1
    let checkbox = document.getElementById('addonCoverYes2') as HTMLInputElement;
    if (checkbox.checked) {
      this.show2 = true
      this.showAddOnCover6 = false;
      this.quoteForm.patchValue({ addOnCover5: this.show2 });
    }
    else {
      this.show2 = false
      this.quoteForm.patchValue({ addOnCover5: this.show2 });
    }
  }

  toggle3(val) {
    let checkbox = document.getElementById('addonCoverYes3') as HTMLInputElement;
    this.show3 = val;
    console.log(this.show3);

    this.showAddOnCover6
    this.showAdultTable
    this.addOnCoverBasedOnAge
    this.toggleButton1
    this.toggleButton

    this.showAddOnCover5
    this.showAdultTable
    this.addOnCoverBasedOnAge
    this.toggleButton1
    this.toggleButton

    if (checkbox.checked) {
      this.showAddOnCover5 = false
      this.quoteForm.patchValue({ addOnCover6: this.show3 });
    }
    else {
      this.quoteForm.patchValue({ addOnCover6: this.show3 });
    }

  }

  quateCalculation(form: any, val: any): Promise<any> {
    return new Promise((resolve) => {
      let valid = this.showFormValidate(form);
      if (valid) {
        this.cs.showLoader = true;
        if (form.childNo == '' || form.childNo == undefined || form.childNo == null) {
          form.childNo = "0";
        }

        this.stateid = this.stateMasterHB.find(x => x.StateName == form.state).StateId;
        console.log(this.stateid);
        if (this.stateid != 62) {
          this.isRegWithGSTCHI = false;
        }

        if (form.addOnCover1 == '' || form.addOnCover1 == undefined || form.addOnCover1 == null) {
          form.addOnCover1 = "false";
        }
        if (form.addOnCover3 == '' || form.addOnCover3 == undefined || form.addOnCover3 == null) {
          form.addOnCover3 = "false";
        }
        if (form.addOnCover5 == '' || form.addOnCover5 == undefined || form.addOnCover5 == null) {
          form.addOnCover5 = "false";
        }
        if (form.addOnCover6 == '' || form.addOnCover6 == undefined || form.addOnCover6 == null) {
          form.addOnCover6 = false;
        }
        console.log("CHI", this.noOfChild, this.noOfAudult);
        if (this.noOfChild == undefined || this.noOfChild == null) {
          this.noOfChild = '0';
        }
        if (this.noOfAudult == undefined || this.noOfAudult == null) {
          this.noOfAudult = '0';
        }

        let check1 = document.getElementById('annSumins5') as HTMLInputElement;
        let check2 = document.getElementById('annSumins6') as HTMLInputElement;
        let addOn6MemArr = [];

        if (form.memberAge1 && form.memberAge2 && form.memberAge1 != "" && form.memberAge1 != "") {
          addOn6MemArr.push(
            {
              "AddOnName": "Adult1",
              "AddOnAgeGroup": form.memberAge1
            },
            {
              "AddOnName": "Adult2",
              "AddOnAgeGroup": form.memberAge2
            }
          )
        }
        else if (form.memberAge1 !== "" || form.memberAge1 !== undefined || form.memberAge1 !== null) {
          addOn6MemArr.push(
            {
              "AddOnName": "Adult1",
              "AddOnAgeGroup": form.memberAge1
            }
          )
        } else if (form.memberAge2 !== "" || form.memberAge2 !== undefined || form.memberAge2 !== null) {
          addOn6MemArr.push(
            {
              "AddOnName": "Adult2",
              "AddOnAgeGroup": form.memberAge2
            }
          )
        }
        if (this.stateid == 149) {
          this.isJammuKashmir = true;
        } else {
          this.isJammuKashmir = false;
        }
        let request = {
          "ProductType": "CHI",
          "UserID": this.agentData.UserID,
          "UserType": "Agent",
          "NoOfAdults": this.noOfAudult.toString(),
          "NoOfKids": this.noOfChild.toString(),
          "AgeGroup1": form.eldestMemAge,
          "AgeGroup2": form.memberAge2,
          "AgeGroup": form.eldestMemAge,
          "CHIProductName": form.productName,
          "SubLimit": form.subLimit,
          "YearPremium": form.policyTenure,
          "IsJammuKashmir": this.isJammuKashmir.toString(),
          "VolDedutible": "0",
          "GSTStateCode": this.stateid.toString(),
          "AddOn1": form.addOnCover1.toString(),
          "InsuredAmount": form.annualSumInsured,
          "AddOn6": form.addOnCover6,
          "AddOn3": form.addOnCover3.toString(),
          "AddOn5": form.addOnCover5,
          "GSTStateName": form.state,
          "isGSTRegistered": this.isRegWithGSTCHI,
          "Members": null,
          "AddOn6Members": addOn6MemArr
        };
        let stateData = this.stateMasterHB.find((x: any) => x.StateName == form.state)
        localStorage.setItem('selectedStateData', JSON.stringify(stateData));
        let req = JSON.stringify(request);
        this.nextAmountPremRequ = request;
        localStorage.setItem('quoteRequest', req);
        console.log("JSON Request", req);
        this.cs.postWithParams('/api/Health/SaveEditQuote', req).then((res: any) => {
          if (res.StatusCode == 1) {
            this.oldQuoteRes = res;
            if (this.productCode == 14 || this.productCode == 17) {
              if (this.isChecked == true || this.isChecked1 == true) {
                localStorage.setItem('quoteResponse', JSON.stringify(res));
                this.isQuoteGenerated = true;
                this.isPremiumDetails = true;
                document.getElementById("quoteDetails").style.background = "url(./assets/images/plus.png)";
                if (val == 'Yes') {
                  document.getElementById("quoteDetailsBody").style.display = 'none';
                }

                document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
              }
              else if (this.isChecked == false || this.isChecked1 == false) {
                localStorage.setItem('quoteResponse', JSON.stringify(res));
                this.isQuoteGenerated = false;
                this.isPremiumDetails = false;
              }
            }
            else
              if (this.productCode == 12 || this.productCode == 16 || this.productCode == 15) {
                localStorage.setItem('quoteResponse', JSON.stringify(res));
                this.isQuoteGenerated = true;
                this.isPremiumDetails = true;
                document.getElementById("quoteDetails").style.background = "url(./assets/images/plus.png)"; document.getElementById("quoteDetailsBody").style.display = 'none';
                document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
              }

            this.isCHIRecalculate = true;
            this.getRequest().then(() => {
              this.getDataFromReq(this.responseTemp);
            })

            //this.router.navigateByUrl('premium');
            this.cs.showLoader = false; this.showHospifundPremium = false;
            resolve();
          } else {
            this.presentAlert(res.StatusMessage);
            console.log("CHI Get quote Error");
            this.cs.showLoader = false;
            this.isQuoteGenerated = false;
            this.isPremiumDetails = false; this.showHospifundPremium = false;
            resolve();
          }
        }).catch((err: any) => {
          console.log(err);
          this.presentAlert(err.error.ExceptionMessage);
          this.cs.showLoader = false;
          this.isQuoteGenerated = false;
          this.isPremiumDetails = false; this.showHospifundPremium = false;
          resolve();
        })
      }
      else {
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
      }
    });
  }

  reCalculateQuote() {
    this.isNoOfMembersChanged = false;
    console.log(event.target, "Recalculate");
    let check1 = document.getElementById('annSumins5') as HTMLInputElement;;
    let check2 = document.getElementById('annSumins6') as HTMLInputElement;;
    if (this.noOfAudult == 1 && this.show3 == true && check1.checked == false) {
      this.presentAlert("Select member for Add on")
    } else if (this.noOfAudult == 2 && this.show3 == true && check2.checked == false && check1.checked == false) {
      this.presentAlert("Select member for Add on");
    } else if (this.noOfAudult == 2 && this.show3 == true && check2.checked == true && check1.checked == false
      && this.quoteForm.value.memberAge2 == '') {
      this.presentAlert("Select Age Limit for Adult 2");
    } else if (this.noOfAudult == 2 && this.show3 == true && check2.checked == true && check1.checked == true
      && this.quoteForm.value.memberAge2 == '') {
      this.presentAlert("Select Age Limit for Adult 2");
    } else {
      this.isRecalculate = true;
      this.cs.showLoader = true;
      this.quateCalculation(this.quoteForm.value, 'No').then(() => {
        this.quateCalculationCHI1(this.nextAmountPremRequ);
        this.isChecked = true;
        this.isChecked1 = false;
        this.cs.showLoader = false;
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
      })
    }
  }

  checkProductForIndOrFloater(data) {
    console.log(data);
    if (this.productName == "SMART" || this.productName == "SMARTPLUS" || this.productName == "PROTECTPLUS") {
      if ((this.noOfAudult == 1 && this.noOfChild == 0) || (this.noOfAudult == 1 && this.noOfChild == undefined)
        || (this.noOfAudult == 0 && this.noOfChild == 1) || (this.noOfAudult == undefined && this.noOfChild == 1)) {
        this.presentAlert('Individual policy not allowed.');
      } else {
        this.calculateQuote(data);
      }
    } else {
      this.calculateQuote(data);
    }
  }


  calculateQuote(formData: any) {
    console.log(formData.value);
    if (formData.value.state == 'JAMMU & KASHMIR') {
      this.isJammuKashmir = true;
    } else {
      this.isJammuKashmir = false;
    }
    if (this.productCode == 14 || this.productCode == 17) {
      if (this.isChecked == true && this.isChecked1 == false) {
        this.quateCalculation(formData.value, 'No');
        this.isQuoteGenerated = true;
        this.isPremiumDetails = true;
        this.isQuoteDetails = !this.isQuoteDetails;
      } else if (this.isChecked1 == true && this.isChecked == false) {
        this.quateCalculation(this.quoteForm.value, 'No').then(() => {
          this.quateCalculationCHI1(this.nextAmountPremRequ);
          this.isChecked = true;
          this.isQuoteGenerated = true;
          this.isPremiumDetails = true;
          this.isQuoteDetails = !this.isQuoteDetails;
        })
      }
      else if ((this.noOfAudult <= 0 || this.noOfAudult == undefined) && (this.noOfChild <= 0 || this.noOfChild == undefined)) {
        let message = "Please select atleast 1 adult or 1 child";
        this.presentAlert2(message);
      }
      else {
        if (this.quoteForm.get('eldestMemAge').value == '') {
          this.presentAlert('Please select member age range');
          return;
        }
        if (this.tenureCheck.nativeElement.checked == false &&
          this.tenureCheck1.nativeElement.checked == false) {
          this.presentAlert('Please select policy tenure');
          return;
        }
        else {
          this.presentAlert('Please select atleast one Annual sum insured');
          return;
        }

      }
    } else {
      this.isQuoteGenerated = true;
      this.isPremiumDetails = true;
      // setTimeout(function(){document.getElementById("premiumDetails").scrollIntoView();}, 1000);
      this.isQuoteDetails = !this.isQuoteDetails;
      this.quateCalculation(this.quoteForm.value, 'No');
    }

    // this.isQuoteGenerated = true;
    // this.isPremiumDetails = true;
    // this.isQuoteDetails = !this.isQuoteDetails;

    // if (this.isChecked && !this.isChecked1) {
    //   this.quoteResponse = this.nextAmountPremRes;
    //   this.quoteRequest = this.nextAmountPremRequ;
    //   localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
    // } else if (!this.isChecked && this.isChecked1) {
    //   this.quoteResponse = this.oldQuoteRes;
    //   this.quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
    //   localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
    // }

  }



  quateCalculationCHI1(data: any) {
    console.log(this.nextSumInsured);
    this.inputLoadingPlan = true;
    let req1 = data;
    if (this.nextSumInsured != undefined) {
      req1.InsuredAmount = this.nextSumInsured.toString();
    }
    // req1.InsuredAmount = this.nextSumInsured.toString();
    let str = JSON.stringify(req1);
    console.log(str);
    this.cs.postWithParams('/api/Health/SaveEditQuote', str).then((res: any) => {
      if (res.StatusCode == 1) {
        this.nextAmountPremRes = res; this.inputLoadingPlan = false;
        console.log("NEW Next Amount Response", res);
        // localStorage.setItem('quoteResponse', JSON.stringify(res));
        this.cs.showLoader = false;

      } else {
        this.presentAlert(res.StatusMessage);
        console.log("CHI Get quate Error");
        this.cs.showLoader = false;
      }
    }).catch((err: any) => {
      console.log(err);
      this.presentAlert(err.error.ExceptionMessage);
      this.cs.showLoader = false;
    })
  }


  checkForAddoOns(val) {

    switch (this.productName) {
      case 'PROTECT':
        if (parseInt(val) >= 500000) {
          if (this.noOfAudult == 1 && this.noOfChild == 0) {
            this.showAddOnCover6 = false; this.showAddOnCover1 = false; this.showAddOnCover5 = false; this.showAddOnCover3 = false;
            this.quoteForm.patchValue({ 'addOnCover1': "true" }); this.show1 = false; this.show2 = false; this.show3 = false;
          }
          else if (this.noOfAudult >= 1 || this.noOfChild >= 1) {
            this.showAddOnCover6 = false; this.showAddOnCover1 = true; this.showAddOnCover5 = false; this.showAddOnCover3 = false;
            this.quoteForm.patchValue({ 'addOnCover1': "true" }); this.show = true; this.show1 = false; this.show2 = false; this.show3 = false;
          }
          else {
            this.quoteForm.patchValue({ 'addOnCover1': "true" }); this.show1 = false; this.show2 = false; this.show3 = false;
            this.showAddOnCover1 = false; this.showAddOnCover3 = false; this.showAddOnCover5 = false; this.showAddOnCover6 = false;
          }
        } else {
          this.quoteForm.patchValue({ 'addOnCover1': "true" }); this.show1 = false; this.show2 = false; this.show3 = false;
          this.showAddOnCover1 = false; this.showAddOnCover3 = false; this.showAddOnCover5 = false; this.showAddOnCover6 = false;
        }

        break;

      case 'PROTECTPLUS':
        this.showAddOnCover5 = true; this.showAddOnCover1 = false; this.showAddOnCover3 = false; this.showAddOnCover6 = false;
        break;

      case 'SMART':
        this.showAddOnCover5 = true; this.showAddOnCover3 = true; this.showAddOnCover6 = false; this.showAddOnCover1 = false;
        break;

      case 'SMARTPLUS':
        this.showAddOnCover5 = true; this.showAddOnCover3 = false; this.showAddOnCover6 = false; this.showAddOnCover1 = false;
        break;

      case 'iHealth':
        if (parseInt(val) >= 500000) {
          if (this.noOfAudult == 1 && this.noOfChild == 0) {
            this.showAddOnCover6 = true; this.showAddOnCover1 = false; this.showAddOnCover5 = false; this.showAddOnCover3 = false;
            this.quoteForm.patchValue({ 'addOnCover1': "true" }); this.show1 = false; this.show2 = false; this.show3 = false;
          }
          else if (this.noOfAudult >= 1 || this.noOfChild >= 1) {
            this.showAddOnCover6 = true; this.showAddOnCover1 = true; this.showAddOnCover5 = false; this.showAddOnCover3 = false;
            this.show = true; this.show1 = false; this.show2 = false; this.show3 = false;
          }
          else {
            this.showAddOnCover6 = true; this.showAddOnCover1 = false; this.showAddOnCover5 = false; this.showAddOnCover3 = false;
            this.quoteForm.patchValue({ 'addOnCover1': "true" }); this.show1 = false; this.show2 = false; this.show3 = false;
          }
        }

        if (this.noOfAudult == 0 && this.noOfChild == 1) {
          this.showAddOnCover6 = false; this.showAddOnCover1 = false; this.showAddOnCover5 = false; this.showAddOnCover3 = false;
          this.quoteForm.patchValue({ 'addOnCover1': "true" });
        }

        break;

      default:
        break;
    }
  }

  


  getSumInsured(event: any) {

    // if (this.productName == "PROTECT") {
    //   if (parseInt(event.target.value) < 500000) {
    //     if (this.noOfAudult == 0 && this.noOfChild < 2) {
    //       this.quoteForm.patchValue({ 'addOnCover1': "true" });
    //       this.showAddOnCover1 = false; this.showAddOnCover3 = false; this.showAddOnCover5 = false; this.showAddOnCover6 = false;
    //     } else if (this.noOfAudult == 1 && this.noOfChild < 1) {
    //       this.quoteForm.patchValue({ 'addOnCover1': "true" });
    //       this.showAddOnCover1 = false; this.showAddOnCover3 = false; this.showAddOnCover5 = false; this.showAddOnCover6 = false;
    //     } else {
    //       this.quoteForm.patchValue({ 'addOnCover1': "false" });
    //       this.showAddOnCover1 = true; this.showAddOnCover3 = false; this.showAddOnCover5 = false; this.showAddOnCover6 = false;
    //     }
    //   } else {
    //     this.quoteForm.patchValue({ 'addOnCover1': "true" });
    //     this.showAddOnCover1 = true; this.showAddOnCover3 = false; this.showAddOnCover5 = false; this.showAddOnCover6 = false;
    //   }
    // } else if (this.productName == "PROTECTPLUS") {
    //   this.showAddOnCover5 = true; this.showAddOnCover1 = false; this.showAddOnCover3 = false; this.showAddOnCover6 = false;
    // } else if (this.productName == "SMART") {
    //   this.showAddOnCover3 = true; this.showAddOnCover5 = true; this.showAddOnCover1 = false; this.showAddOnCover6 = false;
    // } else if (this.productName == "SMARTPLUS") {
    //   this.showAddOnCover5 = true; this.showAddOnCover1 = false; this.showAddOnCover3 = false; this.showAddOnCover6 = false;
    // } else if (this.productName == "iHealth") {
    //   if (parseInt(event.target.value) >= 500000) {
    //     if(this.noOfChild == undefined)
    //     {
    //       this.noOfChild = 0;
    //     }
    //     if (this.noOfAudult == 0 && this.noOfChild < 2) {
    //       this.quoteForm.patchValue({ 'addOnCover1': "true" });
    //       this.showAddOnCover1 = false; this.showAddOnCover6 = true; this.showAddOnCover3 = false; this.showAddOnCover5 = false;
    //     } else if (this.noOfAudult == 1 && this.noOfChild < 1) {
    //       this.quoteForm.patchValue({ 'addOnCover1': "true" });
    //       this.showAddOnCover1 = false; this.showAddOnCover6 = true; this.showAddOnCover3 = false; this.showAddOnCover5 = false;
    //     }
    //     else {
    //       this.quoteForm.patchValue({ 'addOnCover1': "true" }); this.show = true;
    //       this.showAddOnCover1 = true; this.showAddOnCover6 = true; this.showAddOnCover3 = false; this.showAddOnCover5 = false;
    //     }
    //   } else {
    //     this.quoteForm.patchValue({ 'addOnCover1': "true" }); this.show = true;
    //     this.showAddOnCover1 = true; this.showAddOnCover6 = true; this.showAddOnCover3 = false; this.showAddOnCover5 = false;
    //   }
    // }

    this.checkForAddoOns(event.target.value);

    if (this.productCode == 14 || this.productCode == 17) {
      // INSURED PREMIUM TABLE CODE
      this.quoteForm.value;
      console.log("New Premium", this.quoteForm.value);

      // next amount
      let insuredIndex = _.findIndex(this.healthMasterResponse.SumInsuredDetails, { SumAmount: parseInt(this.quoteForm.value.annualSumInsured) })
      console.log(insuredIndex + 1);

      if (insuredIndex + 1 == this.healthMasterResponse.SumInsuredDetails.length) {
        this.showMaxInsured = false;
      }
      else {
        this.showMaxInsured = true;
        this.nextSumInsured = this.healthMasterResponse.SumInsuredDetails[insuredIndex + 1].SumAmount;
      }
      console.log(this.nextSumInsured);

      console.log("New Premium Changed", this.quoteForm.value);
      this.cs.showLoader = true;
      this.quateCalculation(this.quoteForm.value, 'No').then(() => {
        if (this.productCode != 14) {
          this.quateCalculationCHI1(this.nextAmountPremRequ);
        }
        this.isChecked = true;
      })
      this.isQuoteGenerated = false;
      this.isPremiumDetails = false;
      if (this.isChecked == true) {
        this.isChecked = false;
      }
      else if (this.isChecked1 == true) {
        this.isChecked1 = false;
      }
      // this.cs.showLoader = false;
    }
  }


  getAgeStatus(event: any) {
    console.log(event);
    if (event.from_value) {
      this.addOnCoverBasedOnAge = true;
    } else {
      this.addOnCoverBasedOnAge = false;
    }
    this.quoteForm.patchValue({ 'memberAge1': event.from_value });
    this.quoteForm.patchValue({ 'eldestMemAge': event.from_value });
    console.log(this.quoteForm.value, this.quoteForm.controls.memberAge1);
    if (this.adultAgeArray && this.noOfAudult > 0) {
      let index = this.adultAgeArray.findIndex((data: any) => data.AgeValue == event.from_value);
      this.adult2AgeArray = [];
      for (let i = 0; i <= index; i++) {
        this.adult2AgeArray.push(this.adultAgeArray[i]);
      }
    }
    if (this.noOfChild == 1 && this.noOfAudult == 0) {
      this.quoteForm.patchValue({ 'eldestMemAge': '06-20' });
    }
  }

  setAddOnCover(prodName: any) {
    if (prodName == "PROTECT") {
      this.showAddOnCover1 = true; this.showAddOnCover3 = false; this.showAddOnCover5 = false; this.showAddOnCover6 = false;
      this.quoteForm.patchValue({ 'addOnCover3': 'false' });
      this.quoteForm.patchValue({ 'addOnCover5': 'false' });
      this.quoteForm.patchValue({ 'addOnCover6': 'false' });
    } else if (prodName == "PROTECTPLUS") {
      this.showAddOnCover5 = true; this.showAddOnCover1 = false; this.showAddOnCover3 = false; this.showAddOnCover6 = false;
      this.quoteForm.patchValue({ 'addOnCover3': 'false' });
      this.quoteForm.patchValue({ 'addOnCover1': 'false' });
      this.quoteForm.patchValue({ 'addOnCover6': 'false' });
    } else if (prodName == "SMART") {
      this.showAddOnCover3 = true; this.showAddOnCover5 = true; this.showAddOnCover1 = false; this.showAddOnCover6 = false;
      this.quoteForm.patchValue({ 'addOnCover1': 'false' });
      this.quoteForm.patchValue({ 'addOnCover6': 'false' });
    } else if (prodName == "SMARTPLUS") {
      this.showAddOnCover5 = true; this.showAddOnCover1 = false; this.showAddOnCover3 = false; this.showAddOnCover6 = false;
      this.quoteForm.patchValue({ 'addOnCover3': 'false' });
      this.quoteForm.patchValue({ 'addOnCover1': 'false' });
      this.quoteForm.patchValue({ 'addOnCover6': 'false' });
    } else {
      this.showAddOnCover1 = true; this.showAddOnCover6 = true; this.showAddOnCover3 = false; this.showAddOnCover5 = false;
      this.quoteForm.patchValue({ 'addOnCover1': 'true' });
      this.quoteForm.patchValue({ 'addOnCover6': 'false' });
      this.quoteForm.patchValue({ 'addOnCover3': 'false' });
      this.quoteForm.patchValue({ 'addOnCover5': 'false' });
    }
  }

  getPremium(ev: any) {
    this.isChecked = ev.target.checked;
    this.isChecked1 = false;
    console.log(ev.target.checked, this.isChecked, this.isChecked1, this.nextAmountPremRes, this.oldQuoteRes);
    if (this.isChecked && !this.isChecked1) {
      console.log("Check 1");
      localStorage.setItem('quoteResponse', JSON.stringify(this.nextAmountPremRes));
      this.quoteResponse = this.nextAmountPremRes;
      localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
    } else {
      console.log("Check 2");
      localStorage.setItem('quoteResponse', JSON.stringify(this.oldQuoteRes));
      this.quoteResponse = this.oldQuoteRes;
      localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
    }
    // localStorage.setItem('quoteResponse', JSON.stringify(res));
  }

  getPremium1(ev: any) {
    this.isChecked = false;
    this.isChecked1 = true;
    console.log(ev.target.checked, this.nextAmountPremRes, this.oldQuoteRes);
    // localStorage.setItem('quoteResponse', JSON.stringify(res));  
    if (!this.isChecked && this.isChecked1) {
      console.log("Check 2");
      localStorage.setItem('quoteResponse', JSON.stringify(this.oldQuoteRes));
      this.quoteResponse = this.oldQuoteRes;
      localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
    } else {
      console.log("Check 1");
      localStorage.setItem('quoteResponse', JSON.stringify(this.nextAmountPremRes));
      this.quoteResponse = this.nextAmountPremRes;
      localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
    }

  }

  async showAlert(response: any) {
    console.log("ICICI Lombard", response.QuoteResponse.HealthDetails.HealthOutputResponse);
    const alert = await this.alertCtrl.create({
      header: 'Premium Amount for' + response.QuoteResponse.HealthDetails.HealthOutputResponse.Tenure + 'Years',
      message: response.QuoteResponse.HealthDetails.HealthOutputResponse.TotalPremium,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
          }
        }]
    });
    await alert.present();
  }

  // CHI code ends here //

  // Health Booster Code Begins //

  toggleHB() {
    this.showHB = !this.showHB;
    console.log(this.showHB);
    this.quoteFormHB.patchValue({ AddOn1: this.showHB });
  }

  toggle1HB() {
    this.showaddon2HB = !this.showaddon2HB;
    console.log(this.showaddon2HB);
    this.quoteFormHB.patchValue({ AddOn2: this.showaddon2HB });
  }

  toggle2HB() {
    this.showaddon3HB = !this.showaddon3HB;
    console.log(this.showaddon3HB);
    this.quoteFormHB.patchValue({ AddOn3: this.showaddon3HB });
  }

  getAdultHB(ev: any) {
    this.noOfAudultHB = ev.target.value;
    console.log("Get Adult", this.noOfAudultHB, ev);
    let audult1 = document.getElementById('oneAdult1') as HTMLInputElement;
    let audult2 = document.getElementById('oneAdult2') as HTMLInputElement;
    let child1 = document.getElementById('oneChild1') as HTMLInputElement;
    let child2 = document.getElementById('oneChild2') as HTMLInputElement;
    // this.refreshForm(event.target);
    // if(child1.checked == false && audult1.checked == true && this.noOfChildHB == 1 )
    // {
    //   this.noOfAudultHB = 1;
    //   this.noOfChildHB = 0;
    // }

    console.log(ev);
    console.log(this.noOfAudult1HB);
    this.noOfAudultHB = ev.target.value;
    if (this.noOfAudultHB == 2 && ev.target.checked == true) {
      this.childArrayHB = this.adultChildMapHB.get(ev.target.value);
      this.healthMasterResponseHB.AdultResponse.forEach((adult: any) => {
        adult.checked = true;
      })
    }
    if (this.noOfAudultHB == 2 && ev.target.checked == false) {
      //this.childArrayHB = this.adultChildMapHB.get("1");
    }
    else if (this.noOfAudultHB == 1 && ev.target.checked == true) {
      this.healthMasterResponseHB.AdultResponse.forEach((adult: any) => {
        if (adult.AdultValue == 1) {
          adult.checked = true;
          this.childArrayHB = this.adultChildMapHB.get("1");
        }
      })

    } else if (this.noOfAudultHB == 1 && ev.target.checked == false) {
      this.childArrayHB = this.adultChildMapHB.get(ev.target.value);
      this.healthMasterResponseHB.AdultResponse.forEach((adult: any) => {
        if (adult.AdultValue == 1) {
          //adult.checked = false;
        }
        //this.childArrayHB = [{ NoofKids: "1", SumInsuredDetails: null }]
      })
    }


    if (this.noOfAudultHB == 2 && this.noOfChildHB == 1) {
      audult1.checked = true;
      audult2.checked = true;
      this.noOfChildHB = 0;
    } else if (this.noOfAudultHB == 2) {
      audult1.checked = true;
      audult2.checked = true;
      if (ev.target.checked == false && this.noOfChildHB > 1) {
        this.noOfChildHB = 0;
      }
    } else if (this.noOfAudultHB == 1) {
      if (audult1.checked == true && audult2.checked == false && this.noOfChildHB == 1 && this.noOfAudultHB == 1) {
        this.noOfAudult1HB = 1;
        //this.noOfChildHB = 0;
        audult1.checked = true;
        audult2.checked = false;
      } else if (audult1.checked == true && audult2.checked == false && this.noOfChildHB > 2 && this.noOfAudultHB == 1) {
        this.noOfAudult1HB = 1;
        this.noOfChildHB = 0;
        audult1.checked = true;
        audult2.checked = false;
      } else if (audult1.checked == true && audult2.checked == false && this.noOfAudultHB == 1) {
        this.noOfAudult1HB = 1;
        audult1.checked = true;
        audult2.checked = false;
      } else if (audult1.checked == false && audult2.checked == true && this.noOfAudultHB == 1) {
        //if(this.noOfChildHB > 2) {
        this.noOfChildHB = 0;
        //}
        this.noOfAudult1HB = 1;
        audult2.checked = false;
        audult1.checked = true;
      } else {
        this.noOfAudultHB = 0;
        this.noOfAudult1HB = 0;
        this.quoteFormHB.patchValue({ 'Adult1Age': null });
        this.noOfChildHB = 1;
        child2.checked = false;
        child1.checked = true;

      }
    } else if (this.noOfAudultHB == 0) {
      audult1.checked = false;
      audult2.checked = false;
      this.quoteFormHB.patchValue({ 'Adult1Age': null });
    } else {
      this.noOfAudultHB = 0;
      this.quoteFormHB.patchValue({ 'Adult1Age': null });
      audult1.checked = false;
      audult2.checked = false;
    }
  }

  getChildHB(ev: any, child: any) {
    // let child1 = document.getElementById('oneChild1') as HTMLInputElement;
    // let child2 = document.getElementById('oneChild2') as HTMLInputElement;
    // let child3 = document.getElementById('oneChild3') as HTMLInputElement;

    // this.noOfChildHB = ev.target.value;
    // if (ev.target.checked == true) {
    //   this.noOfChildHB = child;
    // }
    // else if (this.noOfChildHB == 1 && ev.target.checked == false) {
    //   this.noOfChildHB = 0;
    // }
    // else if (this.noOfChildHB == 2 && ev.target.checked == false) {
    //   this.noOfChildHB = 0;
    // }
    // else if (this.noOfChildHB == 3 && ev.target.checked == false) {
    //   this.noOfChildHB = 0;
    // }
    // console.log(this.noOfAudult, this.noOfChild);

    let child1 = document.getElementById('oneChild1') as HTMLInputElement;
    let child2 = document.getElementById('oneChild2') as HTMLInputElement;
    let child3 = document.getElementById('oneChild3') as HTMLInputElement;
    this.noOfChildHB = child;
    console.log(this.noOfAudultHB, this.noOfChildHB, child1, child2, child3);
    if (this.noOfChildHB == 1 && child1.checked == false && child2.checked == false) {
      console.log("Child", this.noOfChildHB);
      child1.checked = false;
      this.noOfChildHB = 0
      child2.checked = false;
      child3.checked = false;
    } else if (this.noOfChildHB == 1) {
      child1.checked = true;
      child2.checked = false;
      if (child3 != null && child3 != undefined)
        child3.checked = false;
    }
    else if (this.noOfChildHB == 2) {
      child1.checked = true;
      child2.checked = true;
      console.log("Child", this.noOfChildHB);
      if (child3 != null && child3 != undefined)
        child3.checked = false;

    } else if (this.noOfChildHB == 3) {
      child1.checked = true;
      child2.checked = true;
      child3.checked = true;
      console.log("Child", this.noOfChildHB);
    } else {
      child1.checked = false;
      child2.checked = false;
      child3.checked = false;
    }
    console.log("CHI No Of Child", this.noOfChildHB);
  }

  toggleSegmentHB(ev: any) {
    this.statusHB[ev.target.innerText] = !this.statusHB[ev.target.innerText];
    this.statusHB.forEach((ele, eleIndex) => {
      console.log((eleIndex).toString(), ev.target.innerText)
      if (eleIndex.toString() != ev.target.innerText) {
        this.statusHB[ev.target.innerText] = false;
      }
    });
  }

  addOnYesNoHB(event: any, type: string) {
    if (!event.target.checked) {
      if (type == 'AddOn2') {
        this.quoteFormHB.controls.Addon2Members['controls'].Member['controls'].AdultType.setValue(false);
        this.quoteFormHB.controls.Addon2Members['controls'].Member['controls'].AdultType2.setValue(false);
        this.quoteFormHB.controls.AddOn2Varient.setValue(null);
        // this.quoteForm.controls.AddOn1Varient.setValue(false);
      }
      if (type == 'AddOn3') {
        this.quoteFormHB.controls.Addon3Members['controls'].Member['controls'].AdultType.setValue(false);
        this.quoteFormHB.controls.Addon3Members['controls'].Member['controls'].AdultType2.setValue(false);
        this.quoteFormHB.controls.AddOn3Varient.setValue(null);
      }
    }
  }

  setAddOnHB(prodName: any) {
    if (prodName == "HBOOSTER_TOPUP" || prodName == "HBOOSTER_SUPERUP") {
      this.showAddOn1HB = false;
      this.quoteFormHB.patchValue({ 'AddOn1': 'false' });
      this.quoteFormHB.patchValue({ 'AddOn2': 'false' });
      this.quoteFormHB.patchValue({ 'AddOn3': 'false' });
    }
  }

  getVDValuesHB(ev: any) {
    this.quoteFormHB.patchValue({ 'annualSumInsured': ev.from_value });
    let arr = this.vdMap.get(ev.from_value);
    this.vDMapArray = arr;
    this.storage.set('annualSumInsured', ev.from_value);
  }

  async showLoadingHB() {
    this.loadingHB = await this.loadingCtrl.create({
      message: 'Loading',
      translucent: true
    });
    return await this.loadingHB.present();
  }

  getProductNameHB(event: any): Promise<any> {
    this.cs.showLoader = true;
    return new Promise((resolve: any) => {
      this.refreshFormHB(event.target.value);

      if (event.target.value == "HBOOSTER_TOPUP") {
        this.productCodeHB = 19;
      } else {
        this.productCodeHB = 20;
      }

      this.getHealthMasterHB();
      this.setAddOnHB(event.target.value);
      resolve();
    });
  }

  getStateMasterHB() {
    this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(res => {
      this.stateMasterHB = res;
      this.stateMasterHB = _.sortBy(this.stateMasterHB, 'StateName');
      let statename = this.stateMasterHB.find(x => x.StateId == 55).StateName;
      this.quoteFormHB.patchValue({ 'state': statename });
      this.quoteForm.patchValue({ 'state': statename });
      this.selectedState = 55;
    });
  }

  getStateHB(e: any) {
    console.log(e.target.value);
    if (e.target.value == 'Kerala' || e.target.value == 'KERALA' || e.target.value == 'kerala') {
      this.isKeralaCessHB = true;
    }
    else {
      this.isKeralaCessHB = false; this.isRegWithGSTHB = false;
    }
  }

  getHealthMasterHB() {
    let req = {
      "UserID": this.agentData.AgentID,
      "SubType": this.productCodeHB
    }
    let str = JSON.stringify(req);
    this.cs.postWithParams('/api/healthmaster/HealthPlanMaster', str).then((res: any) => {
      console.log("HBoosterplanMaster", res);
      if (res.StatusCode == 1) {
        this.healthMasterResponseHB = res;
        this.adultChildMapHB = new Map();
        this.adultAgeMapHB = new Map();
        this.healthMasterResponseHB.AdultResponse.forEach((adult: any) => {
          this.adultChildMapHB.set(adult.AdultValue, adult.ChildDetails);
          this.adultAgeMapHB.set(adult.AdultValue, adult.AgeEldestDetails);
          adult['checked'] = false;
        });

        this.childArrayHB = [{
          NoofKids: "1",
          SumInsuredDetails: null
        }]
        this.noOfChildHB = "0";

        this.dataValuesHB = [];
        this.healthMasterResponseHB.SumInsuredDetails.forEach((sum: any) => {
          this.dataValuesHB.push(sum.SumAmount.toString());

        });
        this.showSliderHB = true;
        // for VD details
        this.vdMap = new Map();
        this.healthMasterResponseHB.SumInsuredDetails.forEach((amount: any) => {
          console.log("VD Amount", amount);
          console.log("VD KEY", amount.SumAmount);
          console.log("VD VALUE", amount.VDValues);
          this.vdMap.set(amount.SumAmount, amount.VDValues);
          console.log("VD MAp", this.vdMap);
        });
        this.quoteFormHB.patchValue({ 'annualSumInsured': this.dataValuesHB[0] });
        let arr = this.vdMap.get(parseInt(this.dataValuesHB[0]));
        this.vDMapArray = arr;
        this.storage.set('annualSumInsured', this.dataValuesHB[0]);
        this.cs.showLoader = false;
      } else {
        console.log("Something went wrong");
        this.cs.showLoader = false;
      }
    }).catch((err: any) => {
      console.log("Error", err);
      this.cs.showLoader = false;
    });
  }

  getDateHB(event: any) {
    this.quoteFormHB.patchValue({ "Adult1Age": event.value });
    // console.log("Date", moment(event.value).format('DD-MMM-YYYY'));
  }

  getDate1HB(event: any) {
    this.quoteFormHB.patchValue({ "Adult2Age": event.value });
  }

  createQuoteFormHB() {
    this.quoteFormHB = new FormGroup({
      productName: new FormControl(''),
      state: new FormControl(''),
      adultNo: new FormControl(),
      childNo: new FormControl(),
      annualSumInsured: new FormControl(),
      VDValues: new FormControl(''),
      Adult1Age: new FormControl(),
      Adult2Age: new FormControl(),
      eldestMemAge: new FormControl(),
      deductibleSumInsured: new FormControl(),
      AddOn1: new FormControl(),
      AddOn1Varient: new FormControl(),
      AddOn2: new FormControl(),
      AddOn2Varient: new FormControl(),
      Addon2Members: new FormGroup({
        Member: new FormGroup({
          AdultType: new FormControl(false),
          AdultType2: new FormControl(false),
        })
      }),
      AddOn3: new FormControl(),
      AddOn3Varient: new FormControl(),
      Addon3Members: new FormGroup({
        Member: new FormGroup({
          AdultType: new FormControl(false),
          AdultType2: new FormControl(false),
        })
      }),
      SoftLessCopy: new FormControl('')
    });
  }

  refreshFormHB(prodName: any) {
    let statename = this.stateMasterHB.find(x => x.StateId == 55).StateName;
    this.quoteFormHB = new FormGroup({
      productName: new FormControl(prodName),
      state: new FormControl(statename),         //commented by Priyanka as per bug reported
      adultNo: new FormControl(),
      childNo: new FormControl(),
      annualSumInsured: new FormControl(this.dataValuesHB[0]),  //commented by Priyanka as per bug reported
      VDValues: new FormControl(''),
      Adult1Age: new FormControl(),
      Adult2Age: new FormControl(),
      eldestMemAge: new FormControl(),
      deductibleSumInsured: new FormControl(),
      AddOn1: new FormControl(),
      AddOn1Varient: new FormControl(),
      AddOn2: new FormControl(),
      AddOn2Varient: new FormControl(),
      Addon2Members: new FormGroup({
        Member: new FormGroup({
          AdultType: new FormControl(false),
          AdultType2: new FormControl(false),
        })
      }),
      AddOn3: new FormControl(),
      AddOn3Varient: new FormControl(),
      Addon3Members: new FormGroup({
        Member: new FormGroup({
          AdultType: new FormControl(false),
          AdultType2: new FormControl(false),
        })
      }),
      SoftLessCopy: new FormControl('')
    });
  }

  showFormValidateHB(formData1: any) {
    let message: any;
    if (this.quoteFormHB.value.productName == undefined || this.quoteFormHB.value.productName == null
      || this.quoteFormHB.value.productName == '') {
      message = "Please select product";
      this.presentAlert2(message);
      return false;
    } else if (this.quoteFormHB.value.state == undefined || this.quoteFormHB.value.state == null
      || this.quoteFormHB.value.state == '') {
      message = "Please select state";
      this.presentAlert2(message);
      return false;
    } else if (this.quoteFormHB.value.annualSumInsured == undefined || this.quoteFormHB.value.annualSumInsured == null
      || this.quoteFormHB.value.annualSumInsured == '') {
      message = "Please select Annual Sum Insured";
      this.presentAlert2(message);
      return false;
    } else if (this.noOfAudultHB <= 0 && this.noOfChildHB <= 0) {
      message = "Please select atleast one child or one adult";
      this.presentAlert2(message);
      return false;
    } else if (this.quoteFormHB.value.VDValues == undefined || this.quoteFormHB.value.VDValues == null
      || this.quoteFormHB.value.VDValues == '') {
      message = "Please select Voluntary Deductible Sum";
      this.presentAlert2(message);
      return false;
    } else if (this.quoteFormHB.value.AddOn2 == true && (this.quoteFormHB.value.AddOn2Varient == "SILVER"
      || this.quoteFormHB.value.AddOn2Varient == "GOLD") && (this.quoteFormHB.value.Addon2Members.Member.AdultType == false
        && this.quoteFormHB.value.Addon2Members.Member.AdultType2 == false)) {
      message = "Please select members for Add on 2";
      this.presentAlert2(message);
      return false;
    } else if (this.quoteFormHB.value.AddOn3 == true && (this.quoteFormHB.value.AddOn3Varient == "SILVER"
      || this.quoteFormHB.value.AddOn3Varient == "GOLD")
      && (this.quoteFormHB.value.Addon3Members.Member.AdultType == false &&
        this.quoteFormHB.value.Addon3Members.Member.AdultType2 == false)) {
      message = "Please select members for Add on 3";
      this.presentAlert2(message);
      return false;
    } else if (this.noOfAudultHB >= 1 && (this.quoteFormHB.value.Adult1Age == undefined || this.quoteFormHB.value.Adult1Age == null
      || this.quoteFormHB.value.Adult1Age == '')) {
      message = "Please select Date of Birth of Adult 1";
      this.presentAlert2(message);
      return false;
    } else if (this.noOfAudultHB == 2 && (this.quoteFormHB.value.Adult2Age == undefined
      || this.quoteFormHB.value.Adult2Age == null || this.quoteFormHB.value.Adult2Age == '')) {
      message = "Please select Date of Birth of 2";
      this.presentAlert2(message);
      return false;
    } else if (this.quoteFormHB.value.adultNo != null && this.quoteFormHB.value.adultNo != undefined
      && this.quoteFormHB.value.adultNo != '') {
      if (this.quoteFormHB.value.Adult1Age == undefined || this.quoteFormHB.value.Adult1Age == null
        || this.quoteFormHB.value.Adult1Age == '') {
        message = "Please select Date of Birth";
        this.presentAlert2(message);
        return false;
      } else {
        this.isQuoteGenerated = true;
        this.isPremiumDetails = true;
        // setTimeout(function(){document.getElementById("premiumDetails").scrollIntoView();}, 1000);
        this.isQuoteDetails = !this.isQuoteDetails;
        return true;
      }
    } else {
      this.isQuoteGenerated = true;
      this.isPremiumDetails = true;
      // setTimeout(function(){document.getElementById("premiumDetails").scrollIntoView();}, 1000);
      this.isQuoteDetails = !this.isQuoteDetails;
      return true;
    }
  }

  async presentToast(msg: any) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  calculateQuoteHB(formData1: any) {
    this.isQuoteGenerated = false;
    if (formData1.value.state == 'JAMMU & KASHMIR') {
      this.isJammuKashmirHB = true;
    } else {
      this.isJammuKashmirHB = false;
    }
    let valid = this.showFormValidateHB(formData1);
    if (valid) {
      let formData = formData1.value;
      console.log(formData1);
      Object.keys(this.quoteFormHB.value.Addon2Members.Member).map(member => {

      })
      Object.keys(this.quoteFormHB.value.Addon3Members.Member).map(member => {

      })
      console.log(this.quoteFormHB.value);

      if (formData.SoftLessCopy == '' || formData.SoftLessCopy == undefined || formData.SoftLessCopy == null) {
        formData.SoftLessCopy = "false";
      }

      if (formData.childNo == '' || formData.childNo == undefined || formData.childNo == null) {
        formData.childNo = "0";
      }

      this.stateidHB = this.stateMasterHB.find(x => x.StateName == formData.state).StateId;
      console.log(this.stateidHB);
      if (this.stateidHB != 62) { this.isRegWithGSTHB = false; }
      this.cs.showLoader = true;
      let request;
      let addon2 = [];
      let addon3 = [];
      console.log(formData.Addon2Members.Member.AdultType);
      console.log(formData.Addon2Members.Member.AdultType2);
      if (formData.Addon2Members.Member.AdultType == true && formData.Addon2Members.Member.AdultType2 == true) {
        addon2.push("Adult1");
        addon2.push("Adult2");
      } else if (formData.Addon2Members.Member.AdultType == true &&
        formData.Addon2Members.Member.AdultType2 == false) {
        addon2.push("Adult1");
      } else if (formData.Addon2Members.Member.AdultType == false &&
        formData.Addon2Members.Member.AdultType2 == true) {
        addon2.push("Adult2");
      } else {
        addon2 = [];
      }

      if (formData.Addon3Members.Member.AdultType == true && formData.Addon3Members.Member.AdultType2 == true) {
        addon3.push("Adult1");
        addon3.push("Adult2");
      } else if (formData.Addon3Members.Member.AdultType == true && formData.Addon3Members.Member.AdultType2 == false) {
        addon3.push("Adult1");
      } else if (formData.Addon3Members.Member.AdultType == false && formData.Addon3Members.Member.AdultType2 == true) {
        addon3.push("Adult2");
      } else {
        addon3 = [];
      }

      let adultDOB1;
      let adultDOB2

      if (formData.Adult1Age != undefined && formData.Adult1Age != null && formData.Adult1Age != '') {
        adultDOB1 = moment(formData.Adult1Age).format('DD-MMM-YYYY')
        console.log(adultDOB1);
      } else {
        adultDOB1 = null
      }

      if (formData.Adult2Age != undefined && formData.Adult2Age != null && formData.Adult2Age != '') {
        adultDOB2 = moment(formData.Adult2Age).format('DD-MMM-YYYY');
        console.log(adultDOB2);
      } else {
        adultDOB2 = null
      }
      if (this.noOfChildHB == undefined || this.noOfChildHB == null) {
        this.noOfChildHB = '0';
      }
      if (this.noOfAudultHB == undefined || this.noOfAudultHB == null) {
        this.noOfAudultHB = 0;
      }
      request = {
        "ProductType": "HBOOSTER",
        "UserType": "Agent",
        "ipaddress": config.ipAddress,
        "NoOfAdults": this.noOfAudultHB,
        "NoOfKids": this.noOfChildHB,
        "Adult1Age": adultDOB1,
        "Adult2Age": adultDOB2,
        "ProductName": formData.productName,
        "SumInsured": formData.annualSumInsured,
        "VolDedutible": formData.VDValues,
        "SoftLessCopy": formData.SoftLessCopy,
        "IsJammuKashmir": this.isJammuKashmirHB,
        "AddOn1": formData.AddOn1,
        "AddOn1Varient": formData.AddOn1Varient,
        "AddOn2": formData.AddOn2,
        "AddOn2Varient": formData.AddOn2Varient,
        "Addon2Members": addon2,
        "AddOn3": formData.AddOn3,
        "AddOn3Varient": formData.AddOn3Varient,
        "Addon3Members": addon3,
        "GSTStateCode": this.stateidHB,
        "GSTStateName": formData.state,
        "YearPremium": "3",
        "isGSTRegistered": this.isRegWithGSTHB,
      }

      const stateData = this.stateMasterHB.find((x: any) => x.StateName == formData.state)
      localStorage.setItem('selectedStateData', JSON.stringify(stateData));
      localStorage.setItem('annualSumInsured', formData.annualSumInsured);
      localStorage.setItem('quoteRequest', JSON.stringify(request));
      console.log("Request", JSON.stringify(request));
      const stringifyReq = JSON.stringify(request);
      this.cs.postWithParams('/api/HBooster/SaveEditQuote', stringifyReq).then((res: any) => {
        console.log("New Response", res);
        if (res.StatusCode == 1) {
          localStorage.setItem('quoteResponse', JSON.stringify(res));
          this.isQuoteGenerated = true;
          this.isPremiumDetails = true;
          // setTimeout(function(){document.getElementById("premiumDetails").scrollIntoView();}, 1000);
          this.isQuoteDetails = !this.isQuoteDetails;
          this.isHBoosterRecalculate = true;
          this.getRequest().then(() => {
            this.getDataFromReq(this.responseTemp);
          })
          this.cs.showLoader = false;
          this.isQuoteDetails = !this.isQuoteDetails;
          // setTimeout(function(){document.getElementById("premiumDetails").scrollIntoView();}, 1000);
          this.showHospifundPremium = false;
          let threeYearPrem = document.getElementById('threeyrPolten') as HTMLInputElement;
          if (!this.cs.isUndefinedORNull(threeYearPrem)) {
            threeYearPrem.checked = true;
          }

        } else {
          if (res.StatusMessage != undefined) {
            this.presentAlert2(res.StatusMessage);
            this.cs.showLoader = false; this.showHospifundPremium = false;
          } else {
            this.presentAlert2('Something went wrong...');
            this.cs.showLoader = false;
          }
        }
      }).catch((err) => {
        this.presentAlert2(err.error.ExceptionMessage);
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        this.cs.showLoader = false;
      });
    }
  }

  async presentAlert2(message: any) {
    const alert = await this.alertCtrl.create({
      header: 'ICICI Lombard',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  getSubTypeHB(data: any) {
    console.log("Selected Data", data);
  }
  changesoftcopyValHB(ev: any) {
    console.log(ev.target.value);
    const yesNO = ev.target.value
    console.log('value', yesNO);
    if (yesNO == 'true') {
      this.softCopyAlert();
    }
  }
  async softCopyAlert() {
    const alert = await this.alertCtrl.create({
      // header: 'Age',
      subHeader: 'iPartner Mobile ',
      message: 'You have opted for an e-policy instead of a hard copy.Your Premium shall be revised with a deduction of 100₹',
      buttons: ['OK']
    });

    await alert.present();
  }


  // Health Booster Code Ends //


  // PPAP code begins //

  // intializeDate() {
  //   let elems = document.querySelectorAll('#dobValidator');
  //   let options = {}
  //   let instances = M.Datepicker.init(elems, options);
  // }


  // function getting triggered on changing annual income //
  getAnnualIncome(selectedAnuualIncome) {
    this.sum_insured = ''; this.policy_duration = '';
    this.insured_options = ''; this.payoutTenure = []; this.payout_amount = ''; this.payout_tenure = '';
    this.optionsArray = []; this.OptionDetails = []; this.canShowOptions = false; this.canShowOptionsDetails = false;
    this.showPPASumInsured = false; this.lumpsum = []; this.lump_sum_payment = ''; this.showPPCSumInsured = false;
    this.disableQuote = true;
  }

  //function to perform activity on duartion //

  getPolicyDuration(policyTenure) {
    this.sumInsuredArray = {}; let tempSelectedSI; this.insuredArray = []; this.payoutAmount = [];
    if (this.selectedOccupationType == '0') {
      tempSelectedSI = this.incomeData.find(x => x.Income == this.annual_income).SalariedMaxSI;
    }
    else {
      tempSelectedSI = this.incomeData.find(x => x.Income == this.annual_income).SelfEmployedMaxSI;
    }
    if (this.selectedPP == 'PP') {
      for (let i = 0; i < this.policyData.length; i++) {
        if (this.policyData[i].Value == policyTenure) {
          for (let j = 0; j < this.policyData[i].SumInsureds.length; j++) {
            if (this.policyData[i].SumInsureds[j].Value <= tempSelectedSI)
              this.insuredArray.push(this.policyData[i].SumInsureds[j]);
          }

        }
      }
      if (this.insuredArray.length > 1) {
        this.sumInsuredArray = _.uniq(this.insuredArray, false, function (p: any) { return p.Value; });
      }
      else {
        this.sumInsuredArray = this.insuredArray
      }
    }
    else if (this.selectedPP == 'PPA') {
      let temp_payoutArray = [];
      for (let i = 0; i < this.policyData.length; i++) {
        if (this.policyData[i].Value == policyTenure) {
          let tempArray = this.policyData[i].MonthlyPayouts
          for (let j = 0; j < tempArray.length; j++) {
            temp_payoutArray = [];
            for (let k = 0; k < tempArray[j].PayoutTenures.length; k++) {
              if (tempArray[j].PayoutTenures[k].SumInsured <= tempSelectedSI) {
                temp_payoutArray.push(tempArray[j].PayoutTenures[k]);
              }
            }
            if (temp_payoutArray.length > 0) this.payoutAmount.push({ 'Value': tempArray[j].Value, 'PayoutTenures': temp_payoutArray });
          }
        }
      }
    }
    else if (this.selectedPP == 'PPC') {
      let temp_payoutArray = [];
      for (let i = 0; i < this.policyData.length; i++) {
        if (this.policyData[i].Value == policyTenure) {
          let tempArray = this.policyData[i].MonthlyPayouts
          for (let j = 0; j < tempArray.length; j++) {
            temp_payoutArray = [];
            for (let k = 0; k < tempArray[j].PayoutTenures.length; k++) {
              for (let l = 0; l < tempArray[j].PayoutTenures[k].LumpSums.length; l++) {
                if (tempArray[j].PayoutTenures[k].LumpSums[l].SumInsured <= tempSelectedSI) {
                  temp_payoutArray.push(tempArray[j].PayoutTenures[k]);
                }
              }
            }
            if (temp_payoutArray.length > 0) this.payoutAmount.push({ 'Value': tempArray[j].Value, 'PayoutTenures': temp_payoutArray });
          }
        }
      }
    }
    this.disableQuote = true;
    this.sum_insured = ''; this.payoutTenure = []; this.payout_amount = ''; this.payout_tenure = '';
    this.optionsArray = []; this.OptionDetails = []; this.canShowOptions = false; this.canShowOptionsDetails = false;
    this.showPPASumInsured = false; this.lumpsum = []; this.lump_sum_payment = ''; this.showPPCSumInsured = false;
    this.insured_options = '';
  }
  // function getting triggered on changing payout amount //
  getPayoutAmount(payoutamt) {
    this.payoutTenure = {};
    for (let i = 0; i < this.payoutAmount.length; i++) {
      if (this.payoutAmount[i].Value == payoutamt) {
        this.payoutTenure = this.payoutAmount[i].PayoutTenures;
        //this.ppa_sum_insured = this.payoutTenure[0].SumInsured;
        // if (this.payoutAmount[i].PayoutTenures.length >=1) {
        //   this.payoutTenure.push(this.payoutAmount[i].PayoutTenures);
        // }
        // else {
        //   this.payoutTenure.push(this.payoutAmount[i].PayoutTenures);
        //   this.ppa_sum_insured = this.payoutAmount[i].PayoutTenures.PayoutTenure.SumInsured;
        // }
      }
    }
    this.payoutTenure = _.uniq(this.payoutTenure, false, function (p: any) { return p.Value; });
    this.payout_tenure = ''; this.canShowOptions = false; this.canShowOptionsDetails = false; this.showPPASumInsured = false;
    this.lumpsum = []; this.lump_sum_payment = ''; this.showPPCSumInsured = false; this.disableQuote = true;
  }

  // function getting triggered on changing payout tenure //
  getPayoutTenure(payoutTenure) {
    //this.showPPASumInsured = true; 
    this.OptionsDetailsArray = []; this.optionsArray = []; this.canShowOptions = false; this.canShowOptionsDetails = false;
    this.lumpsum = []; this.lump_sum_payment = ''; this.showPPCSumInsured = false; this.disableQuote = true;
    if (this.selectedPP == 'PPA') {
      for (let i = 0; i < this.payoutAmount.length; i++) {
        if (this.payoutAmount[i].Value == this.payout_amount) {
          if (this.payoutAmount[i].PayoutTenures.length == 0) {
            this.OptionsDetailsArray.push(this.payoutAmount[i].PayoutTenures[0].BenefitSummary);
            this.ppaPlancode = this.payoutAmount[i].PayoutTenures[0].PlanCode;
            this.ppa_sum_insured = this.payoutAmount[i].PayoutTenures[0].SumInsured;
            this.showPPASumInsured = true;
            this.disableQuote = false;
          }
          if (this.payoutAmount[i].PayoutTenures.length == 1) {
            for (let j = 0; j < this.payoutAmount[i].PayoutTenures[0].BenefitSummary.length; j++) {
              this.OptionsDetailsArray.push(this.payoutAmount[i].PayoutTenures[0].BenefitSummary[j]);
            }
            this.ppaPlancode = this.payoutAmount[i].PayoutTenures[0].PlanCode; this.disableQuote = false;
            this.ppa_sum_insured = this.payoutAmount[i].PayoutTenures[0].SumInsured;
            this.showPPASumInsured = true;
          }
          else if (this.payoutAmount[i].PayoutTenures.length > 1) {
            for (let j = 0; j < this.payoutAmount[i].PayoutTenures.length; j++) {
              this.optionsArray.push(this.payoutAmount[i].PayoutTenures[j]);
              this.canShowOptions = true;
            }
            this.insured_options = this.optionsArray[0].PlanCode; this.getOptions(this.insured_options);
          }

          // else {
          //   if (this.payoutAmount[i].PayoutTenures[0].BenefitSummary.length == 0) {
          //     this.ppaPlancode = this.payoutAmount[i].PayoutTenures[0].PlanCode; 
          //     this.ppa_sum_insured = this.payoutAmount[i].PayoutTenures[0].SumInsured;
          //     this.disableQuote = false;  this.showPPASumInsured = true;
          //   }
          //   else {
          //     this.canShowOptions = false; this.canShowOptionsDetails = false; this.disableQuote = false;
          //   }
          //   this.canShowOptions = false;
          // }
        }
      }

      this.canShowOptionsDetails = true;
    }
    else {
      this.lumpsum = []; this.disableQuote = true;
      for (let i = 0; i < this.payoutAmount.length; i++) {
        if (this.payoutAmount[i].Value == this.payout_amount) {
          if (this.payoutAmount[i].PayoutTenures[0].LumpSums.length != undefined) {
            this.lumpsum.push(this.payoutAmount[i].PayoutTenures[0].LumpSums[0].Value);
            this.ppc_sum_insured = this.payoutAmount[i].PayoutTenures[0].LumpSums[0].SumInsured;
          }
          else {
            this.lumpsum.push(this.payoutAmount[i].PayoutTenures.PayoutTenure.LumpSums.Value);
            this.ppc_sum_insured = this.payoutAmount[i].PayoutTenures.PayoutTenure.LumpSums.SumInsured;
          }
        }
      }

    }

  }
  // function getting triggered on changing lump sump amount //
  getLumPSumpAmt(lumpsumpamt) {
    for (let i = 0; i < this.payoutAmount.length; i++) {
      if (this.payoutAmount[i].Value == this.payout_amount) {
        if (this.payoutAmount[i].PayoutTenures[0].LumpSums.length != undefined) {
          this.getPPCDteails(this.payoutAmount[i]);
        }
        else {
          this.getPPCDteails(this.payoutAmount[i]);
        }
      }
    }
    //this.showPPCSumInsured = true;
  }

  getPPCDteails(array) {
    if (array.PayoutTenures[0].LumpSums[0].BenefitSummary != undefined && array.PayoutTenures[0].LumpSums[0].BenefitSummary != null && array.PayoutTenures[0].LumpSums[0].BenefitSummary != "") {
      if (array.PayoutTenures[0].LumpSums.length == 1) {
        for (let j = 0; j < array.PayoutTenures[0].LumpSums[0].BenefitSummary.length; j++) {
          this.OptionsDetailsArray.push(array.PayoutTenures[0].LumpSums[0].BenefitSummary[j]);
        }
        this.ppaPlancode = array.PayoutTenures[0].LumpSums[0].PlanCode;
        this.showPPCSumInsured = true;
        this.disableQuote = false;
      }
      else if (array.PayoutTenures[0].LumpSums.length > 1) {
        for (let j = 0; j < array.PayoutTenures[0].LumpSums.length; j++) {
          {
            this.optionsArray.push(array.PayoutTenures[0].LumpSums[j]);
          }
        }
        this.canShowOptions = true; this.insured_options = this.optionsArray[0].PlanCode; this.getOptions(this.insured_options);
      }
    }
    else {
      this.canShowOptions = false; this.canShowOptionsDetails = false; this.disableQuote = false;
    }
    this.canShowOptionsDetails = true;
  }

  //function to perform activity on changing insurance //
  getPPAPSumInsured(sumInsured) {
    this.optionsArray = []; this.OptionDetails = []; this.disableQuote = true; this.insured_options = '';
    if (_.isArray(this.insuredArray)) {
      for (let i = 0; i < this.insuredArray.length; i++) {
        if (this.insuredArray[i].Value == sumInsured) {
          this.optionsArray.push(this.insuredArray[i]);
        }
      }
      this.insured_options = this.optionsArray[0].PlanCode; this.getOptions(this.insured_options);
    }
    else {
      this.optionsArray.push(this.insuredArray); this.getOptions(this.insured_options);
    }
    if (this.optionsArray.length == 1) {
      if (this.optionsArray[0].BenefitSummary != undefined && this.optionsArray[0].BenefitSummary != null && this.optionsArray[0].BenefitSummary != "") {
        if (this.optionsArray[0].BenefitSummary == undefined) {
          this.canShowOptions = false;
          this.OptionDetails.push(this.optionsArray[0].BenefitSummary);
          this.OptionsDetailsArray = this.OptionDetails; this.insured_options = this.optionsArray[0].PlanCode;
          this.canShowOptionsDetails = true; this.disableQuote = false;
        }
        if (this.optionsArray[0].BenefitSummary.length >= 1) {
          this.canShowOptions = false;
          this.OptionsDetailsArray = this.optionsArray[0].BenefitSummary;
          this.insured_options = this.optionsArray[0].PlanCode;
          this.canShowOptionsDetails = true; this.disableQuote = false;
        }

      }
    }
    else {
      if (this.optionsArray.length > 1) {
        this.canShowOptions = true; this.canShowOptionsDetails = true;
      }
      else {
        this.canShowOptions = true; this.canShowOptionsDetails = false;
      }
    }
  }

  // function to perform activity on changing options if applicable //
  getOptions(selectedOption) {
    this.OptionDetails = [];
    for (let i = 0; i < this.optionsArray.length; i++) {
      if (this.optionsArray[i].PlanCode == selectedOption) {
        this.OptionDetails.push(this.optionsArray[i].BenefitSummary);
        if (this.selectedPP == 'PPA') {
          this.ppaPlancode = selectedOption;
          this.ppa_sum_insured = this.optionsArray[i].SumInsured; this.showPPASumInsured = true;
        }
        if (this.selectedPP == 'PPC') {
          this.ppaPlancode = selectedOption;
          this.ppc_sum_insured = this.optionsArray[i].SumInsured; this.showPPCSumInsured = true;
        }
      }
    }
    if (this.OptionDetails[0].length == undefined) {
      this.OptionsDetailsArray = this.OptionDetails;
      if (this.selectedPP == 'PPA') {
        this.ppaPlancode = selectedOption;
        this.ppa_sum_insured = this.OptionDetails[0].SumInsured; this.showPPASumInsured = true;
      }
      if (this.selectedPP == 'PPC') {
        this.ppaPlancode = selectedOption;
        this.ppc_sum_insured = this.optionsArray[0].SumInsured; this.showPPCSumInsured = true;
      }
    }
    else {
      this.OptionsDetailsArray = this.OptionDetails[0];
    }
    this.canShowOptionsDetails = true; this.disableQuote = false;
  }
  // function to perform activity on changing states //
  getStatePPAP(state) {
    this.selectedState = state;
  }
  // function to perform activity on changing product subtype //
  getSubType(subtype) {
    this.moreDetails = false;
    this.selectedOccupationType = ''; this.disableQuote = true;
  }
  // function to perform activity on changing occupationtype //
  getOccupation(selectedoccupation) {
    this.moreDetails = false; this.annual_income = '';
    if (this.selectedSubType == '0') {
      this.selectedPP = 'PP';
      this.isPPAPPC = false; this.isPPC = false; this.isPPA = false;
    }
    else if (this.selectedSubType == '1') {
      this.selectedPP = 'PPA';
      this.isPPAPPC = true; this.isPPA = true; this.isPPC = false;
    }
    else {
      this.selectedPP = 'PPC';
      this.isPPAPPC = true; this.isPPC = true; this.isPPA = false;
    }
    if (selectedoccupation == 0 || selectedoccupation == 1) {
      this.isEarning = true;
    }
    else {
      this.isEarning = false;
    }
    switch (selectedoccupation) {
      case "0":
        this.occType = 'Salaried'; this.salariedType = 'salaried';
        break;
      case "1":
        this.occType = 'Self Employed'; this.salariedType = 'salaried';
        break;
      case "2":
        this.occType = 'Housewife'; this.salariedType = 'nonsalaried';
        break;
      case "3":
        this.occType = 'Student'; this.salariedType = 'nonsalaried';
        break;
      case "4":
        this.occType = 'Retired'; this.salariedType = 'nonsalaried';
        break;
      case "5":
        this.occType = 'Business man'; this.salariedType = 'BUSINESS MAN';
        break;
    }
    // if ((selectedoccupation == '2' || selectedoccupation == '3' || selectedoccupation == '4') && (this.selectedSubType == '1' || this.selectedSubType == '2')) {
    //   this.presentAlert('No plan available for Monthly Payment Option'); this.isPPAPPC = false; this.isPPC = false; this.moreDetails = false;
    // }
    // else {

    let postData =
    {
      "SubProduct": this.selectedPP,
      "OccupationType": this.salariedType,
    }
    // this.apiloading.present();
    this.cs.showLoader = true;
    this.cs.postWithParams('/api/healthmaster/GetPPAPMasterDetails', postData).then(res => {
      this.masterDataPPAP = res;
      this.incomeData = this.masterDataPPAP.Incomes;
      this.policyData = this.masterDataPPAP.PolicyTenures;
      if (this.incomeData.length < 1 || this.policyData.length < 1) {
        if (this.selectedPP == 'PPA') {
          this.cs.showLoader = false;
          this.moreDetails = false;
          this.presentAlert('No plan available for Monthly Payment Option');
        }
        else if (this.selectedPP == 'PPC') {
          this.cs.showLoader = false; this.moreDetails = false;
          this.presentAlert('No plan available for Lump Sum Payment Option with Monthly Payout');
        }
        else {
          this.cs.showLoader = false; this.moreDetails = false;
          this.presentAlert('No plan available');
        }

      }
      else {
        this.moreDetails = true; this.disableQuote = true;
        this.sum_insured = ''; this.policy_duration = ''; this.annual_income = '';
        this.payoutTenure = []; this.payout_amount = null; this.payout_tenure = '';
        this.optionsArray = []; this.OptionDetails = []; this.canShowOptions = false; this.canShowOptionsDetails = false;
        this.showPPASumInsured = false; this.lumpsum = []; this.lump_sum_payment = ''; this.showPPCSumInsured = false;
        // this.apiloading.dismiss();
        this.cs.showLoader = false;
      }
      // setTimeout(function () {
      //   let elems = document.querySelectorAll('#dobValidator');
      //   let sinsuredDOB = new Date();
      //   let einsuredDOB = new Date();
      //   sinsuredDOB.setFullYear(new Date().getFullYear() - 100);
      //   let today = new Date()
      //   let priorDate = new Date().setFullYear(new Date().getFullYear()-18); 
      //   let defDate = new Date(priorDate)
      //   let options = { format: 'dd-mm-yyyy', autoClose: true, disableWeekends: false, defaultDate: defDate, minDate: sinsuredDOB, maxDate: defDate, yearRange: 100 }
      //   let instances = M.Datepicker.init(elems, options);
      // }, 1000);
    }, err => {
      this.presentAlert('Sorry Something went wrong');
      // this.apiloading.dismiss();
      this.cs.showLoader = false;
    });
  }


  generateQuote(form) {
    if (this.selectedSubType == undefined || this.selectedSubType == null || this.selectedSubType == '') {
      this.presentAlert('Please select product sub type');
    }
    else if (this.selectedOccupationType == undefined || this.selectedOccupationType == null || this.selectedOccupationType == '') {
      this.presentAlert('Please select occupation of insured');
    }
    else if (this.selectedState == undefined || this.selectedState == null || this.selectedState == '') {
      this.presentAlert('Please select customer state');
    }
    else if (this.annual_income == undefined || this.annual_income == null || this.annual_income == '') {
      this.presentAlert('Please select annual income of insured');
    }
    else if (this.dob == undefined || this.dob == null || this.dob == '') {
      this.presentAlert('Please select date of birth');
    }
    else if (this.policy_duration == undefined || this.policy_duration == null || this.policy_duration == '') {
      this.presentAlert('Please select policy duration');
    }
    else if (this.payout_amount == undefined || this.payout_amount == null || this.payout_amount == '' && (this.moreDetails && this.isPPAPPC)) {
      this.presentAlert('Please select payout amount');
    }
    else if (this.payout_tenure == undefined || this.payout_tenure == null || this.payout_tenure == '' && (this.moreDetails && this.isPPAPPC)) {
      this.presentAlert('Please select payout tenure');
    }
    else if (this.lump_sum_payment == undefined || this.lump_sum_payment == null || this.lump_sum_payment == '' && (this.moreDetails && this.isPPAPPC && this.isPPC)) {
      this.presentAlert('Please select lump sum payment');
    }
    else if ((this.sum_insured == undefined || this.sum_insured == null || this.sum_insured == '') && (!this.isPPA && !this.isPPC)) {
      this.presentAlert('Please select sum insured');
    }
    else if ((this.insured_options == undefined || this.insured_options == null || this.insured_options == '') && (this.canShowOptions)) {
      this.presentAlert('Please select options');
    }
    else {
      if (this.incomeData.length < 1 || this.policyData.length < 1) {
        if (this.selectedPP == 'PPA') {
          this.presentAlert('No plan available for Monthly Payment Option');
        }
        else if (this.selectedPP == 'PPC') {
          this.presentAlert('No plan available for Lump Sum Payment Option with Monthly Payout');
        }
        else {
          this.presentAlert('No plan available');
        }
      }
      else {
        this.saveGetQuote(form);
      }
    }

  }

  saveGetQuote(form) {
    let isJammuKashmir; let tempstate = this.selectedState;
    if (this.selectedState != 62) { this.isRegWithGST = false; }
    if (this.selectedState == 149) { isJammuKashmir = true } else { isJammuKashmir = false }
    if (this.selectedState == undefined || this.selectedState == '' || this.selectedState == null) {
      this.presentAlert('Please select state');
    }
    else if (this.dob == undefined || this.dob == '' || this.dob == null) {
      this.presentAlert('Please select date of insured');
    }
    else if (this.dob != undefined || this.dob != '' || this.dob != null) {
      let tempAgeDate = moment().diff(this.dob, 'years');
      if (tempAgeDate < 1) { this.presentAlert('Please select valid date of insured'); }
      else {
        let statename = _.find(this.stateMasterHB, function (s: any) { return s.StateId == tempstate }).StateName;
        let gstStateCode = _.find(this.stateMasterHB, function (s: any) { return s.StateId == tempstate }).StateId
        let dateofbirth = moment(this.dob).format('DD-MMMM-YYYY');
        let occupation = this.occType.toUpperCase();
        let plancode;
        if (this.selectedPP == 'PP') {
          plancode = this.insured_options;
          localStorage.setItem('SumInsured', this.sum_insured.toString());
        }
        else if (this.selectedPP == 'PPA') {
          plancode = this.ppaPlancode;
          localStorage.setItem('SumInsured', this.ppa_sum_insured.toString());
        }
        else if (this.selectedPP == 'PPC') {
          plancode = this.ppaPlancode;
          localStorage.setItem('SumInsured', this.ppc_sum_insured.toString());
        }
        else { alert('Plancode not found'); }
        // debugger
        let postData =
        {
          "UserType": "AGENT",
          "ProductType": "PPAP",
          "ipaddress": config.ipAddress,
          "SubProduct": this.selectedPP,
          "DOB": dateofbirth,
          "Tenure": this.policy_duration,
          "IsJammuKashmir": isJammuKashmir,
          "Occupation": occupation,
          "PlanCode": plancode,
          "GSTStateCode": gstStateCode,
          "GSTStateName": statename,
          "isGSTRegistered": this.isRegWithGST
        }
        let stateData = this.stateMasterHB.find((x: any) => x.StateName == statename);
        localStorage.setItem('selectedStateData', JSON.stringify(stateData));
        localStorage.setItem('quoteRequest', JSON.stringify(postData));
        localStorage.setItem('AnuualIncome', this.annual_income.toString());
        if (this.OptionsDetailsArray.length > 0) {
          localStorage.setItem('PPAPCovers', JSON.stringify(this.OptionsDetailsArray));
        }
        this.cs.showLoader = true;
        this.cs.postWithParams('/api/PPAP/SaveEditQuote', postData).then(res => {
          this.saveQuoteData = res;
          // form.controls['dobValidator'].reset(); form.controls['subProuctType'].reset();
          // this.moreDetails = false; this.disableQuote = true;
          // this.sum_insured = ''; this.policy_duration = ''; this.annual_income = '';
          // this.payoutTenure = []; this.payout_amount = null; this.payout_tenure = '';
          // this.optionsArray = []; this.OptionDetails = []; this.canShowOptions = false; this.canShowOptionsDetails = false;
          // this.showPPASumInsured = false; this.lumpsum = []; this.lump_sum_payment = ''; this.showPPCSumInsured = false;
          // this.disableQuote = true; this.dob = null; this.selectedSubType = null; this.selectedOccupationType = null;
          // this.selectedState = 55; this.payoutAmount = []; this.policyData = [];
          if (this.saveQuoteData.StatusCode == 1) {
            this.cs.showLoader = false;
            localStorage.setItem('quoteResponse', JSON.stringify(this.saveQuoteData));
            this.isQuoteGenerated = true;
            this.isPPAPRecalculate = true;
            this.getRequest().then(() => {
              this.getDataFromReq(this.responseTemp);
            })
            document.getElementById("quoteDetails").style.background = "url(./assets/images/plus.png)"; document.getElementById("quoteDetailsBody").style.display = 'none';
            document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
            this.isQuoteDetails = !this.isQuoteDetails;
            // setTimeout(function(){document.getElementById("premiumDetails").scrollIntoView();}, 1000);
            this.showHospifundPremium = false;
          }
          else {
            this.presentAlert(this.saveQuoteData.StatusMessage);
            this.cs.showLoader = false; this.showHospifundPremium = false;
          }
        }, err => {
          this.presentAlert(err.error.ExceptionMessage);
          this.cs.showLoader = false;
        });
      }
    }

  }
  gotosummary() {
    this.routes.navigate(['../cal-health-premium/health_summary']);
  }
  convertAmountToCurrency(num) {
    if (num != undefined && num != null && num != '') {
      return num.toLocaleString('en-IN', { style: 'currency', currency: "INR", minimumFractionDigits: 0, maximumFractionDigits: 0 });
    }

  }
  getDate(event: any) {
    setTimeout(function () {
      $('button').removeClass('cdk-focused cdk-program-focused');
    });

    console.log(moment(this.dob).format('DD-MMMM-YYYY'));
  }
  // select dropdown popvers configurations //
  productPopoveroptions: any = {
    header: 'Select Product Sub Type'
  };
  occuptionPopoverOptions: any = {
    header: 'Select Occupation'
  };
  statePopoverOptions: any = {
    header: 'Select State'
  };
  annualIncomePopoverOptions: any = {
    header: 'Select Annual Income'
  };
  policyDurationPopoverOptions: any = {
    header: 'Select Policy Duration'
  };
  sumInsuredPopoverOptions: any = {
    header: 'Select Sum Insured'
  };
  sumOptipnsPopoverOptions: any = {
    header: 'Select Options'
  };
  payoutAmountPopoverOptions: any = {
    header: 'Select Payout Amount'
  };
  payoutTenurePopoverOptions: any = {
    header: 'Select Payout Tenure'
  };
  lumSumPopoverOptions: any = {
    header: 'Select Lump Sum Amount'
  }
  // customPopoverOptions: any = {
  //   header: 'Select'
  // };

  async showLoading() {
    this.loading = await this.loadingCtrl.create({
      message: 'Loading',
      translucent: true
    });
    return await this.loading.present();
  }
  isGSTReg(val) {
    if (val) {
      this.isRegWithGST = true;
    } else {
      this.isRegWithGST = false;
    }
  }

  // PPAP code ends //

  // hospifund code starts //

  getStateHospiFund(e: any) {
  }
  getBenefitOptions(val) {

  }
  getCVCBenefit() {
    if (this.extnBenefit13 == true) { this.showEBCCFooter = true; } else { this.showEBCCFooter = false; }
  }
  getCCBBenefit() {
    if (this.extnBenefit11 == true) { this.showEBCVCFooter = true; } else { this.showEBCVCFooter = false; }
  }
  getSelectedBenefits() {
    this.coverDetails = []; this.sumofBaseBenefits = 0; this.sumofExtensionBenefits = 0;
    if (this.baseBenefit1 == true) {
      this.coverDetails.push({ CoverName: 'Hospitalization Cash Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });
      this.sumofBaseBenefits = this.sumofBaseBenefits + this.siData;
    }
    if (this.baseBenefit2 == true) {
      this.coverDetails.push({ CoverName: 'Accidental Hospitalization Cash Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });

      this.sumofBaseBenefits = this.sumofBaseBenefits + this.siData;
    }
    if (this.extnBenefit1 == true) {
      this.coverDetails.push({ CoverName: 'Intensive Care Cash Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit2 == true) {
      this.coverDetails.push({ CoverName: 'Cancer Hospitalization Cash Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit3 == true) {
      this.coverDetails.push({ CoverName: 'Brain and Stroke Hospitalization Cash Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit4 == true) {
      this.coverDetails.push({ CoverName: 'Organ Transplant Hospitalization Cash Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit5 == true) {
      this.coverDetails.push({ CoverName: 'Heart Ailment Hospitalization Cash Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit6 == true) {
      this.coverDetails.push({ CoverName: 'Fracture and Burns Cash Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit7 == true) {
      this.coverDetails.push({ CoverName: 'Extension Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit8 == true) {
      this.coverDetails.push({ CoverName: 'Day Care Treatment Cash Benefit', CoverSI: this.siData.toString(), Column1: '5' });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit9 == true) {
      this.coverDetails.push({ CoverName: 'Hospital Attendant Cash Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit10 == true) {
      this.coverDetails.push({ CoverName: 'Child Care Cash Benefit', CoverSI: this.siData.toString(), Column1: this.noOfDays.toString() });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit11 == true) {
      this.coverDetails.push({ CoverName: 'Convalescence Cash Benefit', CoverSI: '10000', Column1: '1' });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit12 == true) {
      this.coverDetails.push({ CoverName: 'Ambulance Cover Benefit', CoverSI: this.siData.toString(), Column1: '5' });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    if (this.extnBenefit13 == true) {
      this.coverDetails.push({ CoverName: 'Compassionate Visit Cash Benefit', CoverSI: '2500', Column1: '1' });
      this.sumofExtensionBenefits = this.sumofExtensionBenefits + this.siData;
    }
    //  console.log(this.coverDetails); console.log(this.sumofBaseBenefits);
    //console.log(this.sumofExtensionBenefits);
  }
  onFinishSlidingSI(e) {
    this.siData = e.from_value;
  }
  onFinishSlidingDays(e) {
    this.noOfDays = e.from_value;
  }

  goToPremium() {
    let dateerror = false;
    let temp = moment(this.dobInsured, 'DD-MM-YYYY').format('DD-MMMM-YYYY');
    let tempDate = moment().diff(temp, 'years');
    let tempDateMonths = moment().diff(temp, 'months');
    if (tempDate < 18) {
      localStorage.setItem('isAdultorKid', 'Child');
      if (tempDate < 1) {
        localStorage.setItem('ageofInsured', tempDateMonths.toString() + ' months');
      }
      else { localStorage.setItem('ageofInsured', tempDate.toString()); }
    }
    else if (tempDate >= 18 && tempDate < 100) {
      localStorage.setItem('isAdultorKid', 'Adult');
      localStorage.setItem('ageofInsured', tempDate.toString());
    }
    else {
      dateerror = true;
    }

    if (this.baseBenefit1 == false && this.baseBenefit2 == false) {
      this.presentAlert('Please select atleast one base benefit');
    }
    else if (this.selectedState == null || this.selectedState == undefined) {
      this.presentAlert('Please select proper customer State');
    }
    else if (dateerror == true) {
      this.presentAlert('Please select valid Insured DOB');
    }
    else if (this.selectedState == undefined || this.selectedState == null || this.selectedState == '') {
      this.presentAlert('Please select valid Insured DOB');
    }
    else {
      this.getSelectedBenefits();
      if (this.extnBenefit1 == false && this.extnBenefit2 == false && this.extnBenefit3 == false && this.extnBenefit4 == false
        && this.extnBenefit5 == false && this.extnBenefit6 == false && this.extnBenefit7 == false && this.extnBenefit9 == false
        && this.extnBenefit10 == false && this.extnBenefit11 == false && this.extnBenefit12 == false && this.extnBenefit13 == false) {
        this.sumofBaseBenefits = this.sumofExtensionBenefits;
        this.callGetQuote(1); this.callGetQuote(2); this.callGetQuote(3);
      }
      else {
        // if (this.sumofExtensionBenefits > this.sumofBaseBenefits) {
        //   this.presentAlert('SI per Day of Extension Benefits cannot be greater than Base benefits');
        // }
        // else {
        //   this.callGetQuote(1);
        //   this.showPremiumPanel = true; this.isPremiumDetails = true;
        // }
        this.errorCount = 0;
        this.callGetQuote(1); this.callGetQuote(2); this.callGetQuote(3);
      }
    }

  }
  getMonth(date) {
    var month = date.getMonth() + 1;
    return month < 10 ? '0' + month : '' + month; // ('' + month) for string result
  }
  getMyDate(day) {
    return day < 10 ? '0' + day : '' + day;
  }
  callGetQuote(tenure) {
    let selectedState; let Tenure = tenure; this.cs.showLoader = false;
    let covertedDOB = moment(this.dobInsured, 'DD-MM-YYYY').format('YYYY-MM-DD');
    // let todaysDate = moment();
    // let duration = moment.duration(Tenure, 'years');
    // let tempPolicyEndDate = todaysDate.add(duration).format('YYYY-MM-DD');
    //let tdate = moment().format('YYYY-MM-DD');
    const myId = uuid.v4();
    var currDate = new Date();
    var end_date = new Date();
    end_date.setFullYear(end_date.getFullYear() + parseInt(tenure));
    end_date.setDate(end_date.getDate() - 1);
    let tdate = currDate.getFullYear() + "-" + (this.getMonth(currDate)) + "-" + this.getMyDate(currDate.getDate());
    let tempPolicyEndDate = end_date.getFullYear() + "-" + (this.getMonth(end_date)) + "-" + this.getMyDate(end_date.getDate());
    if (Tenure == 1) {
      localStorage.setItem('Tenure', Tenure);
      localStorage.setItem('PolicyStartDate', tdate);
      localStorage.setItem('PolicyEndDate', tempPolicyEndDate);
      localStorage.setItem('inwardDate', tdate);
      localStorage.setItem('siData', this.siData.toString());
      localStorage.setItem('noofDays', this.noOfDays.toString());
      localStorage.setItem('guid1', myId);
    }
    else if (Tenure == 2) {
      localStorage.setItem('guid2', myId);
    }
    else if (Tenure == 3) {
      localStorage.setItem('guid3', myId);
    }
    else { }
    localStorage.setItem('coverDetails', JSON.stringify(this.coverDetails));
    localStorage.setItem('insuredDOB', covertedDOB);
    for (let i = 0; i < this.stateMasterHB.length; i++) {
      if (this.selectedState == this.stateMasterHB[i].StateId) {
        selectedState = this.stateMasterHB[i].StateName;
      }
    }
    if (this.selectedState == 149) { localStorage.setItem('hospiIsJammuandKashmir', 'Yes'); }
    else { localStorage.setItem('hospiIsJammuandKashmir', 'No'); }
    let taxDetails =
    {
      'gstServiceDetails': [{ "ILGICStateName": selectedState, "partyStateName": selectedState, "taxExempted": "false" }]
    }
    let sicomponents =
    {
      'RiskSIComponent': 'Person',
      'CoverDetails': this.coverDetails
    }
    let planDetails =
      [{
        'PlanCode': config.hospiFundPlanCode,
        'RiskSIComponentDetails': [sicomponents]
      }]
    let productDetails =
    {
      'ProductCode': config.hospiFundProductCode,
      'planDetails': planDetails
    }

    let postData =
    {
      'CorrelationId': myId,
      'policyStartDate': tdate,
      "policyEndDate": tempPolicyEndDate,
      "transactionDate": "2019-05-17",
      "PolicyTenure": Tenure,
      "stampDutyChargable": "true",
      "inwardDate": tdate,
      "insuredDOB": covertedDOB,
      'taxDetails': taxDetails,
      'productDetails': productDetails,
    }
    // console.log(postData);
    let requestBody = JSON.stringify(postData);
    this.cs.showLoader = true; let count = 0;
    if (Tenure == 1) { localStorage.setItem('quoteRequest1', requestBody) }
    else if (Tenure == 2) { localStorage.setItem('quoteRequest2', requestBody) }
    else if (Tenure == 3) { localStorage.setItem('quoteRequest3', requestBody) }
    //Zerotat/Quote/Create
    //General/Quote/Create
    this.cs.postforHospifund('/ilservices/misc/v1/General/Quote/Create', requestBody).then(res => {
      this.quoteData = res;
      if (this.quoteData.statusMessage == 'Success') {
        let tempQuote = JSON.stringify(this.quoteData);
        if (Tenure == 1) {
          this.base1 = this.quoteData.basicPremium;
          this.totalPremium1 = this.quoteData.totalPremium;
          this.tax1 = this.quoteData.totalTax;
          this.totalPremiuminRs1 = this.quoteData.totalPremium;
          localStorage.setItem('quoteData1', JSON.stringify(this.quoteData));
        }
        else if (Tenure == 2) {
          this.base2 = this.quoteData.basicPremium;
          this.totalPremium2 = this.quoteData.totalPremium;
          this.tax2 = this.quoteData.totalTax;
          this.totalPremiuminRs2 = this.quoteData.totalPremium;
          localStorage.setItem('quoteData2', JSON.stringify(this.quoteData));
        }
        else if (Tenure == 3) {
          this.base3 = this.quoteData.basicPremium;
          this.totalPremium3 = this.quoteData.totalPremium;
          this.tax3 = this.quoteData.totalTax;
          this.totalPremiuminRs3 = this.quoteData.totalPremium;
          localStorage.setItem('quoteData3', JSON.stringify(this.quoteData));
        }
        document.getElementById("quoteDetails").style.background = "url(./assets/images/plus.png)"; document.getElementById("quoteDetailsBody").style.display = 'none';
        document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';

        this.showHospifundPremium = true; this.isPremiumDetails = true;
        this.cs.showLoader = false; this.isQuoteGenerated = true;
        this.isQuoteDetails = !this.isQuoteDetails;
        // setTimeout(function(){document.getElementById("premiumDetails").scrollIntoView();}, 1000);
        this.premium3 = true;
      }
      else {
        let tempErrorMessage = this.quoteData.message
        if (tempErrorMessage.includes("less than AddOn Cover Premium")) {
          this.presentAlert('SI per Day of Extension Benefits cannot be greater than Base benefits');
          this.showHospifundPremium = false; this.isPremiumDetails = false;
          this.cs.showLoader = false; this.isQuoteGenerated = false;
        }
        else {
          if (this.errorCount == 0) {
            this.presentAlert(this.quoteData.message);
            this.errorCount = this.errorCount + 1;
          }
          this.showPremiumPanel = false; this.isPremiumDetails = false;
          this.cs.showLoader = false; this.isQuoteGenerated = false;
        }
      }

    }, err => {
      if (this.errorCount == 0) {
        this.presentAlert('Sorry Something went wrong');
        this.errorCount = this.errorCount + 1;
      }
      this.showPremiumPanel = false; this.isPremiumDetails = false;
      this.cs.showLoader = false;
    });
    // console.log(postData);
  }

  // hospifund code ends //

    
  // arogya sanjeevani code begins here //

  createArogyaSanjeevaniForm() {
    this.quoteSanjeevaniForm = new FormGroup({
      productName: new FormControl('HEALTH_AROGYA_SANJEEVANI'),
      state: new FormControl(''),
      adultNo: new FormControl(),
      childNo: new FormControl(),
      annualSumInsured: new FormControl(),
      policyTenure: new FormControl(),
      eldestMemAge: new FormControl(),
      subLimit: new FormControl(),
      addOnCover1: new FormControl(),
      addOnCover3: new FormControl(),
      addOnCover5: new FormControl(),
      addOnCover6: new FormControl(),
      memberAge1: new FormControl(),
      memberAge2: new FormControl('')
    });
    if (this.stateMasterHB != undefined) {
      let statename = this.stateMasterHB.find(x => x.StateId == 55).StateName;
      this.quoteSanjeevaniForm.patchValue({ 'state': statename });
    }
    else{
      this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(res => {
        this.stateMasterHB = res;
        this.stateMasterHB = _.sortBy(this.stateMasterHB, 'StateName');
        let statename = this.stateMasterHB.find(x => x.StateId == 55).StateName;
        this.quoteSanjeevaniForm.patchValue({ 'state': statename });
      });
    }
    this.productCode = 22; this.getHealthMasterPlan('HEALTH_AROGYA_SANJEEVANI');
    this.showAdultASP = true; this.showChildASP = true;
  }

  getHealthMasterPlan(product_name) {
    this.cs.showLoader = true;
    let req = {
      "UserID": this.agentData.AgentID,
      "SubType": this.productCode
    }
    let str = JSON.stringify(req);
    this.cs.postWithParams('/api/healthmaster/HealthPlanMaster', str).then((res: any) => {
      // console.log("CHI HealthplanMaster", res);
      if (res.StatusCode == 1) {
        this.healthMasterResponseASP = res;
        this.adultChildMap = new Map();
        this.adultAgeMap = new Map();
        this.healthMasterResponseASP.SumInsuredDetails = _.sortBy(this.healthMasterResponseASP.SumInsuredDetails, 'SumAmount');
        this.healthMasterResponseASP.AdultResponse.forEach((adult: any) => {
          this.adultChildMap.set(adult.AdultValue, adult.ChildDetails);
          this.adultAgeMap.set(adult.AdultValue, adult.AgeEldestDetails);
        });
        this.childArray = [{
          NoofKids: "1",
          SumInsuredDetails: null
        }]
        this.noOfChildHB = "0";
        setTimeout(() => {
          this.setAdultasDefaultPlan(product_name);
        }, 500);

        this.cs.showLoader = false;
      } else {
        // console.log("Something went wrong");
        this.cs.showLoader = false;
      }
    }).catch((err: any) => {
      // console.log("Error", err);
      this.cs.showLoader = false;
    });
  }

  // if (this.isQuoteDetails) {
  //   var imgUrl = './assets/images/plus.png';
  //   document.getElementById("quoteDetails").style.background = "url(" + imgUrl + ") no-repeat 98.5% center";
  //   document.getElementById("quoteDetailsBody").style.display = 'none';
  //   document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat ';
  // } else {
  //   var imgUrl = './assets/images/minus.png';
  //   document.getElementById("quoteDetails").style.background = "url(" + imgUrl + ") no-repeat 98.5% center";
  //   document.getElementById("quoteDetailsBody").style.display = 'block';
  //   document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
  // }
  setAdultasDefaultPlan(product_name) {
    this.isQuoteGenerated = false; this.showPolicyTenureforASP = true; this.isPremiumDetails = false;
    this.showSilderASP = true; this.showAnnualSumInsuredforASP = true; this.showInsuredTable = true
    this.showAdultASP = true; this.showChildASP = true; this.showAdultTable = false;
    this.button4 = true;
    this.quoteSanjeevaniForm.patchValue({ "policyTenure": "1" });
    this.quoteSanjeevaniForm.patchValue({ "subLimit": null });
    this.noOfAudult = 1; this.noOfChild = 0; this.setDefaultValuesforPlan(1, 0);
  }

  setDefaultValuesforPlan(adults, childs) {
    if (adults == 1) {
      let adult1 = document.getElementById('oneAdult1') as HTMLInputElement;
      adult1.checked = true;
      let adultAge = this.adultAgeMap.get('1');
      this.adultAgeArray = adultAge;
      this.healthMasterResponseASP.AdultResponse.forEach((adult: any) => {
        if (adult.AdultValue == 1) {
          adult.checked = true;
          this.childArray = this.adultChildMap.get("1");
        }
      })
      this.adult2AgeArray = [];
      this.adult2AgeArray.push(this.adultAgeArray[0]);
      this.showAdultTable = true;
      this.addOnCoverBasedOnAge = true;
    }
    if (childs == 1) {
      let child = this.adultChildMap.get('1');
      this.childArray = child;
      setTimeout(() => {
        let child1 = document.getElementById('oneChild1') as HTMLInputElement;
        child1.checked = true;
      }, 100);

    }

    if (adults > 0) {
      this.dataValues = [];
      this.adultAgeArray.forEach((element: any) => {
        this.dataValues.push(element.AgeValue);
      });
      this.quoteSanjeevaniForm.patchValue({ 'eldestMemAge': this.dataValues[0] });
      this.chirangeASP.update({ from: this.dataValues[0], to: this.dataValues[this.dataValues.length - 1] });
    } else {
      this.noOfChild = 1;
      this.quoteSanjeevaniForm.patchValue({ 'childNo': "1" });
      this.dataValues = ['06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'];
    }
  }

  getAdultSanjeevani(ev: any) {
    this.isQuoteGenerated = false; this.showPolicyTenureforASP = true; this.isPremiumDetails = false;
    this.noOfAudult = ev; this.showSilderASP = true; this.showAnnualSumInsuredforASP = true; this.showInsuredTable = true
    this.showAdultASP = true; this.showChildASP = true; this.showAdultTable = false;
    this.isQuoteGenerated = false; this.isPremiumDetails = false;

    let audult1 = document.getElementById('oneAdult1') as HTMLInputElement;
    let audult2 = document.getElementById('oneAdult2') as HTMLInputElement;
    let child1 = document.getElementById('oneChild1') as HTMLInputElement;
    let child2 = document.getElementById('oneChild2') as HTMLInputElement;
    let child3 = document.getElementById('oneChild3') as HTMLInputElement;

    if (this.noOfAudult == 2 && this.noOfChild == 1) {
      audult1.checked = true;
      audult2.checked = true;
      this.noOfChild = 0;
    } else
      if (this.noOfAudult == 2) {
        audult1.checked = true;
        audult2.checked = true;
      }
      else if (this.noOfAudult == 1) {
        if (audult1.checked == true && audult2.checked == false && this.noOfChild == 1 && this.noOfAudult == 1) {
          this.noOfAudult = 1;
          //this.noOfChild = 0;
          audult1.checked = true;
          audult2.checked = false;
        } else
          if (audult1.checked == true && audult2.checked == false && this.noOfAudult == 1) {
            this.noOfAudult = 1;
            audult1.checked = true;
            audult2.checked = false;
          } else
            if (audult1.checked == false && audult2.checked == true && this.noOfAudult == 1) {
              this.noOfAudult = 1;
              audult2.checked = false;
              audult1.checked = true;
            } else {
              this.noOfAudult = 0; this.noOfChild = 1;
              child1.checked = true;
              child2.checked = false;
            }
      }
      else if (this.noOfAudult == 0) {
        audult1.checked = false;
        audult2.checked = false;
      } else {
        this.noOfAudult = 0;
        audult1.checked = false;
        audult2.checked = false;
      }
    if (this.noOfAudult > 0) {
      this.showAdultTable = true;
      this.addOnCoverBasedOnAge = true;
    } else {
      this.showAdultTable = false;
    }
    let child = this.adultChildMap.get(ev);
    this.childArray = child;
    let adultAge = this.adultAgeMap.get(ev);
    this.adultAgeArray = adultAge;
    if (this.noOfAudult == 2 && ev.checked == true) {
      this.childArray = this.adultChildMap.get(ev);
      this.healthMasterResponseASP.AdultResponse.forEach((adult: any) => {
        adult.checked = true;
      })
    }
    if (this.noOfAudult == 2 && ev.checked == false) {
      this.childArray = this.adultChildMap.get("1");
    }
    else if (this.noOfAudult == 1 && ev.checked == true) {
      this.healthMasterResponseASP.AdultResponse.forEach((adult: any) => {
        if (adult.AdultValue == 1) {
          adult.checked = true;
          this.childArray = this.adultChildMap.get("1");
        }
      })
    }
    else if (this.noOfAudult == 1 && ev.checked == false) {
      this.healthMasterResponseASP.AdultResponse.forEach((adult: any) => {
        if (adult.AdultValue == 1) {
          adult.checked = false;
        }
        this.childArray = [{ NoofKids: "1", SumInsuredDetails: null }]
      })
    }

    if (this.noOfAudult > 0) {
      this.dataValues = [];
      this.adultAgeArray.forEach((element: any) => {
        this.dataValues.push(element.AgeValue);
      });
      this.quoteSanjeevaniForm.patchValue({ 'eldestMemAge': this.dataValues[0] });
      this.chirangeASP.update({ from: this.dataValues[0], to: this.dataValues[this.dataValues.length - 1] });
      this.quoteSanjeevaniForm.patchValue({ 'annualSumInsured': null });
    } else {
      this.noOfChildHB = 1;
      this.noOfChild = 1;
      this.quoteSanjeevaniForm.patchValue({ 'childNo': "1" });
      this.dataValues = ['06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'];
      this.quoteSanjeevaniForm.patchValue({ 'annualSumInsured': null });
      this.quoteSanjeevaniForm.patchValue({ 'eldestMemAge': '06' });
      this.quoteSanjeevaniForm.patchValue({ 'memberAge1': '6' });
    }
    this.button4 = true;
    this.quoteSanjeevaniForm.patchValue({ "policyTenure": "1" });
    this.quoteSanjeevaniForm.patchValue({ "subLimit": null });
  }

  getChildSanjeevani(ev: any, child: any) {
    let child1 = document.getElementById('oneChild1') as HTMLInputElement;
    let child2 = document.getElementById('oneChild2') as HTMLInputElement;
    let child3 = document.getElementById('oneChild3') as HTMLInputElement;
    this.noOfChild = child;
    this.showSilderASP = true; this.showPolicyTenureforASP = true;
    this.showAdultASP = true; this.showChildASP = true; this.showAnnualSumInsuredforASP = true;
    this.showInsuredTable = true; this.isQuoteGenerated = false; this.isPremiumDetails = false;
    this.quoteSanjeevaniForm.patchValue({ 'annualSumInsured': null });
    this.button4 = true; this.subilimitN = true;
    this.quoteSanjeevaniForm.patchValue({ "policyTenure": "1" });
    //this.quoteSanjeevaniForm.patchValue({ "subLimit": null });

    if (this.noOfAudult == "0" && this.noOfChild == "2") {
      this.presentAlert("Cannot Select 2 Child");
      this.showMaxInsured = false;
      this.nextSumInsured = false; this.isChecked = false;
      this.showSilderASP = false; this.showPolicyTenureforASP = false; this.showSubLimit = false; this.showAnnualSumInsuredforASP = false
      this.showinsuredTable = false; this.showNextButton = false; this.showAddOnCover1 = false;

    }

    if (this.noOfAudult == undefined || this.noOfAudult == null || this.noOfAudult < 1) {
      this.quoteForm.patchValue({ 'childNo': "1" });
      this.dataValues = ['06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'];
    }
    (this.noOfChildHB == 1 && child1.checked == false)
    if (this.noOfChild == 1 && child1.checked == false && child2.checked == false) {
      child1.checked = false;
      this.noOfChild = 0;
      if (child2 != null) { child2.checked = false }
      if (child3 != null) { child3.checked = false }
      if (this.noOfAudult == 0) {
        this.showPolicyTenureforASP = false;
      }
    } else if (this.noOfChild == 1) {
      // console.log("Child", this.noOfChild);
      child1.checked = true;
      if (child2 != null) { child2.checked = false }
      if (child3 != null) { child3.checked = false }
    } else if (this.noOfChild == 2) {
      child1.checked = true;
      child2.checked = true;
      if (child3 != null) { child3.checked = false }
      // console.log("Child", this.noOfChild);
    } else if (this.noOfChild == 3) {
      child1.checked = true;
      child2.checked = true;
      child3.checked = true;
      // console.log("Child", this.noOfChild);
    } else {
      // child1.checked = false;
      child2.checked = false;
      if (child3 != null) { child3.checked = false }
    }
    this.button4 = true; this.subilimitN = true;
    this.quoteSanjeevaniForm.patchValue({ "policyTenure": "1" });
    this.quoteSanjeevaniForm.patchValue({ "subLimit": null });

    if (this.noOfAudult == 0) {
      this.quoteSanjeevaniForm.patchValue({ 'eldestMemAge': '06' });
      this.quoteSanjeevaniForm.patchValue({ 'memberAge1': '6' });
    }
    if (this.noOfAudult == 0 && this.noOfChild == 0) {
      this.showInsuredTable = false; this.showAnnualSumInsuredforASP = false;
    }
  }

  checkPOSUser2(ev: any) {
    this.selectedQuoteRes = []; this.selectednextQuoteRes = [];
    if (this.agentData.isPOSUser == true && parseInt(ev.target.value) > 500000) {
      this.presentAlert('As per IRDA guidelines, POS Intermediary Sum Insured capping is till 5 Lacs only');
      this.cs.showLoader = false;
      this.quoteSanjeevaniForm.value.annualSumInsured = null;
    } else {
      this.getSumInsured2(ev);
    }
  }

  getSumInsured2(event: any) {

    if (this.agentData.isPOSUser == false) {
      this.quoteSanjeevaniForm.value;
      let insuredIndex = _.findIndex(this.healthMasterResponseASP.SumInsuredDetails, { SumAmount: parseInt(this.quoteSanjeevaniForm.value.annualSumInsured) });
      if (insuredIndex + 1 == this.healthMasterResponseASP.SumInsuredDetails.length) {
        this.showMaxInsured = false;
        this.cs.showLoader = true;
        this.quoteCalculation(this.quoteSanjeevaniForm.value, 'No').then(() => {
          this.isChecked1 = true;
          this.cs.showLoader = false;
        })
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
          this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
          this.isChecked1 = false;
        }

      }
      else {
        this.showMaxInsured = true;
        this.nextSumInsured = this.healthMasterResponseASP.SumInsuredDetails[insuredIndex + 1].SumAmount;
        this.cs.showLoader = true;
        this.quoteCalculation(this.quoteSanjeevaniForm.value, 'No').then(() => {
          this.quoteCalculationCHI1(this.selctedAmountPremReq);
          this.isChecked = true;
        })
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
          this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
          this.isChecked1 = false;
        }
      }
    }
    else {
      this.showMaxInsured = false;
      this.cs.showLoader = true;
      this.quoteCalculation(this.quoteSanjeevaniForm.value, 'No').then(() => {
        this.isChecked1 = true;
        this.cs.showLoader = false;
      })
      this.isQuoteGenerated = false;
      this.isPremiumDetails = false;

      if (this.isChecked == true) {
        this.isChecked = false;
      }
      else if (this.isChecked1 == true) {
        this.isChecked1 = false;
      }

    }
  }

  getPremium3(ev: any) {
    this.isChecked = ev.target.checked;
    this.isChecked1 = false;

    if (this.isChecked && !this.isChecked1) {
      localStorage.setItem('quoteResponse', JSON.stringify(this.selectednextQuoteRes[0]));
      this.quoteResponse = this.selectednextQuoteRes[0];
      localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
      this.quoteRequest = this.nextAmountPremRequ;
      localStorage.setItem('quoteRequest', JSON.stringify(this.quoteRequest));
    } else {
      localStorage.setItem('quoteResponse', JSON.stringify(this.selectedQuoteRes[0]));
      this.quoteResponse = this.selectedQuoteRes[0];
      localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
      this.quoteRequest = this.selctedAmountPremReq;
      localStorage.setItem('quoteRequest', JSON.stringify(this.quoteRequest));
    }
  }

  getPremium4(ev: any) {
    this.isChecked = false;
    this.isChecked1 = ev.target.checked;
    if (!this.isChecked && this.isChecked1) {
      localStorage.setItem('quoteResponse', JSON.stringify(this.selectedQuoteRes[0]));
      this.quoteResponse = this.selectedQuoteRes[0];
      localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
      this.quoteRequest = this.selctedAmountPremReq;
      localStorage.setItem('quoteRequest', JSON.stringify(this.quoteRequest));
    } else {
      localStorage.setItem('quoteResponse', JSON.stringify(this.selectednextQuoteRes[0]));
      this.quoteResponse = this.selectednextQuoteRes[0];
      localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
      this.quoteRequest = this.nextAmountPremRequ;
      localStorage.setItem('quoteRequest', JSON.stringify(this.quoteRequest));
    }

  }

  quoteCalculation(form: any, val: any): Promise<any> {
    this.selectedQuoteRes = [];
    return new Promise((resolve) => {
      this.inputLoadingPlan1 = true;
      let valid = this.validateSanjeevaniForm(form);

      let addOn6MemArr = [];

      // if (form.memberAge1 && form.memberAge2 && form.memberAge1 != "" && form.memberAge1 != "") {
      //   addOn6MemArr.push(
      //     {
      //       "AddOnName": "Adult1",
      //       "AddOnAgeGroup": form.memberAge1
      //     },
      //     {
      //       "AddOnName": "Adult2",
      //       "AddOnAgeGroup": form.memberAge2
      //     }
      //   )
      // }
      // else if (form.memberAge1 !== "" || form.memberAge1 !== undefined || form.memberAge1 !== null) {
      //   addOn6MemArr.push(
      //     {
      //       "AddOnName": "Adult1",
      //       "AddOnAgeGroup": form.memberAge1
      //     }
      //   )
      // } else if (form.memberAge2 !== "" || form.memberAge2 !== undefined || form.memberAge2 !== null) {
      //   addOn6MemArr.push(
      //     {
      //       "AddOnName": "Adult2",
      //       "AddOnAgeGroup": form.memberAge2
      //     }
      //   )
      // }

      if (valid) {
        this.cs.showLoader = true;
        if (form.childNo == '' || form.childNo == undefined || form.childNo == null) {
          form.childNo = "0";
        }

        this.stateid = this.stateMasterHB.find(x => x.StateName == form.state).StateId;
        // console.log(this.stateid);
        if (this.stateid != 62) {
          this.isRegWithGSTCHI = false;
        }

        if (form.addOnCover1 == '' || form.addOnCover1 == undefined || form.addOnCover1 == null) {
          form.addOnCover1 = "false";
        }
        if (form.addOnCover3 == '' || form.addOnCover3 == undefined || form.addOnCover3 == null) {
          form.addOnCover3 = "false";
        }
        if (form.addOnCover5 == '' || form.addOnCover5 == undefined || form.addOnCover5 == null) {
          form.addOnCover5 = "false";
        }
        if (form.addOnCover6 == '' || form.addOnCover6 == undefined || form.addOnCover6 == null) {
          form.addOnCover6 = false;
        }
        // console.log("CHI", this.noOfChild, this.noOfAudult);
        // console.log('qoate calculate----------',form);
        let memaddon: any;
        if (form.addOnCover6) {
          memaddon = addOn6MemArr;
        }
        else if (form.addOnCover5) {
          memaddon = addOn6MemArr;
        }
        if (this.noOfChild == undefined || this.noOfChild == null) {
          this.noOfChild = '0';
        }
        if (this.noOfAudult == undefined || this.noOfAudult == null) {
          this.noOfAudult = '0';
        }
        if (this.stateid == 149) {
          this.isJammuKashmir = true;
        } else {
          this.isJammuKashmir = false;
        }
        let request = {
          "ProductType": "ASP",
          "UserID": this.agentData.UserID,
          "UserType": "Agent",
          "NoOfAdults": this.noOfAudult.toString(),
          "NoOfKids": this.noOfChild.toString(),
          "AgeGroup1": form.eldestMemAge,
          "AgeGroup2": form.memberAge2,
          "AgeGroup": form.eldestMemAge,
          "CHIProductName": form.productName,
          "SubLimit": form.subLimit,
          "YearPremium": form.policyTenure,
          "IsJammuKashmir": this.isJammuKashmir.toString(),
          "VolDedutible": "0",
          "GSTStateCode": this.stateid.toString(),
          "AddOn1": form.addOnCover1.toString(),
          "InsuredAmount": form.annualSumInsured,
          "AddOn6": form.addOnCover6,
          "AddOn3": form.addOnCover3.toString(),
          "AddOn5": form.addOnCover5,
          "GSTStateName": form.state,
          "isGSTRegistered": this.isRegWithGSTCHI,
          "Members": memaddon,
          "AddOn6Members": memaddon
        };
        let stateData = this.stateMasterHB.find((x: any) => x.StateName == form.state)
        localStorage.setItem('selectedStateData', JSON.stringify(stateData));
        let req = JSON.stringify(request);
        this.selctedAmountPremReq = request;
        localStorage.setItem('quoteRequest', JSON.stringify(request));
        this.cs.postWithParams('/api/Health/SaveEditQuote', req).then((res: any) => {
          if (res.StatusCode == 1) {
            this.selectedQuoteRes.push(res);
            this.inputLoadingPlan1 = false;
            if (this.isChecked == true || this.isChecked1 == true) {
              localStorage.setItem('quoteResponse', JSON.stringify(res));
              if (val == 'None') {
                this.isQuoteGenerated = false;
                this.isPremiumDetails = false;
              }
              else {
                this.isQuoteGenerated = true;
                this.isPremiumDetails = true;
              }
              document.getElementById("quoteDetails").style.background = "url(./assets/images/plus.png)";
              if (val == 'Yes') {
                document.getElementById("quoteDetailsBody").style.display = 'none';
              }

              document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
            }
            else if (this.isChecked == false || this.isChecked1 == false) {
              localStorage.setItem('quoteResponse', JSON.stringify(res));
              this.isQuoteGenerated = false;
              this.isPremiumDetails = false;
            }
            this.getRequest().then(() => {
              this.getDataFromReq(this.responseTemp);
            })

            // this.cs.showLoader = false; 
            this.showHospifundPremium = false;
            resolve();
          } else {
            this.presentAlert(res.StatusMessage); this.selectedQuoteRes = [];
            // console.log("CHI Get quote Error");
            this.cs.showLoader = false;
            this.isQuoteGenerated = false; this.inputLoadingPlan = false;
            this.isPremiumDetails = false; this.showHospifundPremium = false;
            //resolve();
          }
        }).catch((err: any) => {
          // console.log(err);
          this.presentAlert(err.error.ExceptionMessage); this.selectedQuoteRes = [];
          this.cs.showLoader = false;
          this.isQuoteGenerated = false; this.inputLoadingPlan = false;
          this.isPremiumDetails = false; this.showHospifundPremium = false;
          // resolve();
        })
      }
      else{
        this.cs.showLoader=false;
      }
    });
  }

  quoteCalculationCHI1(data: any) {
    this.inputLoadingPlan = true;
    this.inputLoadingPlan1 = true; this.selectednextQuoteRes = [];
    let req1 = data;
    if (this.nextSumInsured != undefined) {
      req1.InsuredAmount = this.nextSumInsured.toString();
    }
    let str = JSON.stringify(req1);
    this.nextAmountPremRequ = req1;
    this.cs.postWithParams('/api/Health/SaveEditQuote', str).then((res: any) => {
      if (res.StatusCode == 1) {
        this.selectednextQuoteRes.push(res);
        this.inputLoadingPlan = false; this.inputLoadingPlan1 = false;
        localStorage.setItem('quoteResponse', JSON.stringify(res));
        this.cs.showLoader = false;

      } else {
        this.presentAlert(res.StatusMessage);
        this.cs.showLoader = false; this.inputLoadingPlan = false;
        this.inputLoadingPlan1 = false; this.selectednextQuoteRes = [];
      }
    }).catch((err: any) => {
      this.presentAlert(err.error.ExceptionMessage);
      this.cs.showLoader = false; this.inputLoadingPlan = false;
      this.inputLoadingPlan1 = false; this.selectednextQuoteRes = [];
    })
  }

  validateSanjeevaniForm(form: any) {
    let checkBox
    let checkBox1
    if (document.getElementById('annSumins5') as HTMLInputElement) {
      checkBox = document.getElementById('annSumins5') as HTMLInputElement;
    }
    if (document.getElementById('annSumins5') as HTMLInputElement) {
      checkBox1 = document.getElementById('annSumins6') as HTMLInputElement;
    }
    console.log("form is", form);

    let message: any;
    if (!form.productName) {
      this.presentAlert("Please select Product");
      return false;
    } else if (!form.state) {
      this.presentAlert("Please select State");
      return false;
    } else if ((this.noOfAudultHB <= 0 || this.noOfAudultHB == undefined) &&
      (this.noOfChildHB <= 0 || this.noOfChildHB == undefined) &&
      (this.noOfAudult <= 0 || this.noOfAudult == undefined) &&
      (this.noOfChild <= 0 || this.noOfChild == undefined)) {
      this.presentAlert("Please select atleast 1 Adult or 1 Child");
      return false;
    } else if (!form.eldestMemAge && (this.noOfAudult > 0 && this.noOfChild > 1)) {
      this.presentAlert("Please select Member Age Range");
      return false;
    } else if (!form.policyTenure) {
      this.presentAlert("Please select Policy Tenure");
      return false;
    } else if (!form.annualSumInsured) {
      this.presentAlert("Please select Annual Sum Insured");
      return false;
    }

    if ((this.show2 || this.show3) && !checkBox.checked && (checkBox1 != null ? !checkBox1.checked : !checkBox.checked)) {
      this.presentAlert("Select Member for Add On Cover");
      return false;
    } else if (this.isNoOfMembersChanged) {
      this.presentAlert("You have changed No of Members, Please recalculate first");
      return false;
    } else if ((this.show3) && this.isRecalculate == false && (checkBox.checked || checkBox1.checked)) {
      this.presentAlert("You have changed Add-on Cover, please recalculate first");
      return false;
    } else {
      return true;
    }

  }

  showPremium2() {
    if (this.validateSanjeevaniForm(this.quoteSanjeevaniForm.value)) {
      this.isQuoteGenerated = true; this.isPremiumDetails = true; this.isQuoteDetails = !this.isQuoteDetails;
      if (this.isChecked && !this.isChecked1) {
        this.quoteResponse = this.selectednextQuoteRes[0];
        this.quoteRequest = this.nextAmountPremRequ;
        localStorage.setItem('quoteResponse', JSON.stringify(this.quoteResponse));
        localStorage.setItem('quoteRequest', JSON.stringify(this.quoteRequest));
        localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
        document.getElementById("quoteDetails").style.background = "url(./assets/images/plus.png)"; document.getElementById("quoteDetailsBody").style.display = 'none';
        document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
      } else if (!this.isChecked && this.isChecked1) {
        this.quoteResponse = this.selectedQuoteRes[0];
        this.quoteRequest = this.selctedAmountPremReq;
        localStorage.setItem('quoteResponse', JSON.stringify(this.quoteResponse));
        localStorage.setItem('quoteRequest', JSON.stringify(this.quoteRequest));
        localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
        document.getElementById("quoteDetails").style.background = "url(./assets/images/plus.png)"; document.getElementById("quoteDetailsBody").style.display = 'none';
        document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
      }
      else {
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        this.presentAlert('Please select atleat one premium');
      }
    }
  }

  selectTenure2(ev: any) {
    // console.log("Tenure", ev.target.innerText);
    this.selectedQuoteRes = []; this.selectednextQuoteRes = [];
    if (ev.target.innerText == '1 Year') {
      this.quoteSanjeevaniForm.patchValue({ "policyTenure": "1" });
      this.button4 = true; this.button5 = false; this.button6 = false;
      this.quoteSanjeevaniForm.value;
      let insuredIndex = _.findIndex(this.healthMasterResponseASP.SumInsuredDetails, { SumAmount: parseInt(this.quoteSanjeevaniForm.value.annualSumInsured) });
      if (insuredIndex + 1 == this.healthMasterResponseASP.SumInsuredDetails.length) {
        this.showMaxInsured = false;
        this.cs.showLoader = true;
        this.quoteCalculation(this.quoteSanjeevaniForm.value, 'No').then(() => {
          this.isChecked1 = true;
          this.cs.showLoader = false;
        })
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
          this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
          this.isChecked1 = false;
        }

      }
      else {
        this.showMaxInsured = true;
        this.nextSumInsured = this.healthMasterResponseASP.SumInsuredDetails[insuredIndex + 1].SumAmount;
        this.cs.showLoader = true;
        this.quoteCalculation(this.quoteSanjeevaniForm.value, 'No').then(() => {
          this.quoteCalculationCHI1(this.selctedAmountPremReq);
          this.isChecked = true;
        })
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
          this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
          this.isChecked1 = false;
        }
      }

    } else if (ev.target.innerText == '2 Years') {
      this.button4 = false; this.button5 = true; this.button6 = false;
      this.quoteSanjeevaniForm.patchValue({ "policyTenure": "2" });
      this.quoteSanjeevaniForm.value;
      let insuredIndex = _.findIndex(this.healthMasterResponseASP.SumInsuredDetails, { SumAmount: parseInt(this.quoteSanjeevaniForm.value.annualSumInsured) });
      if (insuredIndex + 1 == this.healthMasterResponseASP.SumInsuredDetails.length) {
        this.showMaxInsured = false;
        this.cs.showLoader = true;
        this.quoteCalculation(this.quoteSanjeevaniForm.value, 'No').then(() => {
          this.isChecked1 = true;
          this.cs.showLoader = false;
        })
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
          this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
          this.isChecked1 = false;
        }

      }
      else {
        this.showMaxInsured = true;
        this.nextSumInsured = this.healthMasterResponseASP.SumInsuredDetails[insuredIndex + 1].SumAmount;
        this.cs.showLoader = true;
        this.quoteCalculation(this.quoteSanjeevaniForm.value, 'No').then(() => {
          this.quoteCalculationCHI1(this.selctedAmountPremReq);
          this.isChecked = true;
        })
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
          this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
          this.isChecked1 = false;
        }
      }

    } else {
      this.button4 = false; this.button5 = false; this.button6 = true;
      this.quoteSanjeevaniForm.patchValue({ "policyTenure": "3" });
      this.quoteSanjeevaniForm.value;
      let insuredIndex = _.findIndex(this.healthMasterResponseASP.SumInsuredDetails, { SumAmount: parseInt(this.quoteSanjeevaniForm.value.annualSumInsured) });
      if (insuredIndex + 1 == this.healthMasterResponseASP.SumInsuredDetails.length) {
        this.showMaxInsured = false;
        this.cs.showLoader = true;
        this.quoteCalculation(this.quoteSanjeevaniForm.value, 'No').then(() => {
          this.isChecked1 = true;
          this.cs.showLoader = false;
        })
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
          this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
          this.isChecked1 = false;
        }

      }
      else {
        this.showMaxInsured = true;
        this.nextSumInsured = this.healthMasterResponseASP.SumInsuredDetails[insuredIndex + 1].SumAmount;
        this.cs.showLoader = true;
        this.quoteCalculation(this.quoteSanjeevaniForm.value, 'No').then(() => {
          this.quoteCalculationCHI1(this.selctedAmountPremReq);
          this.isChecked = true;
        })
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
          this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
          this.isChecked1 = false;
        }
      }
    }
  }

  getAgeStatus2(event: any) {
    this.selectedQuoteRes = []; this.selectednextQuoteRes = [];
    if (this.clickAddon6No) {
      this.clickAddon6No.nativeElement.click();
    }

    // console.log(event);
    if (event.from_value) {
      this.addOnCoverBasedOnAge = true;
    } else {
      this.addOnCoverBasedOnAge = false;
    }
    this.quoteSanjeevaniForm.patchValue({ 'memberAge1': event.from_value == "81 AND ABOVE" ? '81-125' : event.from_value });
    this.quoteSanjeevaniForm.patchValue({ 'eldestMemAge': event.from_value == "81 AND ABOVE" ? '81-125' : event.from_value });
    // console.log(this.quoteForm.value, this.quoteForm.controls.memberAge1);
    if (this.adultAgeArray && this.noOfAudult > 0) {
      let index = this.adultAgeArray.findIndex((data: any) => data.AgeValue == event.from_value);
      this.adult2AgeArray = [];
      for (let i = 0; i <= index; i++) {
        this.adult2AgeArray.push(this.adultAgeArray[i]);
      }
    }
    if (this.noOfChild == 1 && this.noOfAudult == 0) {
      this.quoteSanjeevaniForm.patchValue({ 'eldestMemAge': '06-20' });
    }
    if (this.quoteSanjeevaniForm.value.eldestMemAge && this.quoteSanjeevaniForm.value.annualSumInsured && this.showInsuredTable) {
      let insuredIndex = _.findIndex(this.healthMasterResponseASP.SumInsuredDetails, { SumAmount: parseInt(this.quoteSanjeevaniForm.value.annualSumInsured) })
      if (insuredIndex + 1 == this.healthMasterResponseASP.SumInsuredDetails.length) {
        this.showMaxInsured = false;
        this.cs.showLoader = true;
        this.quoteCalculation(this.quoteSanjeevaniForm.value, 'None').then(() => {
          this.isChecked1 = true;
          this.cs.showLoader = false;
        })
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
          this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
          this.isChecked1 = false;
        }
      }
      else {
        this.showMaxInsured = true;
        this.nextSumInsured = this.healthMasterResponseASP.SumInsuredDetails[insuredIndex + 1].SumAmount;
        this.cs.showLoader = true;
        this.quoteCalculation(this.quoteSanjeevaniForm.value, 'None').then(() => {
          this.quoteCalculationCHI1(this.selctedAmountPremReq);
          this.isChecked = true;
        })
        this.isQuoteGenerated = false;
        this.isPremiumDetails = false;
        if (this.isChecked == true) {
          this.isChecked = false;
        }
        else if (this.isChecked1 == true) {
          this.isChecked1 = false;
        }
      }
    }

  }

  // arogya sanjeevani code ends here //


  home(ev: any) {
    console.log("Home", ev);
    this.cs.goToHome();
  }


  showQuoteDetails(ev: any) {
    this.isQuoteDetails = !this.isQuoteDetails;
    if (this.isQuoteDetails) {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("quoteDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
      document.getElementById("quoteDetailsBody").style.display = 'none';
    } else {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("quoteDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
      document.getElementById("quoteDetailsBody").style.display = 'block';
    }
  }

  showPremiumDetails(ev: any) {

    this.isPremiumDetails = !this.isPremiumDetails;
    if (this.isPremiumDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("premiumDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("premiumDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("premiumDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("premiumDetails").style.backgroundRepeat = 'no-repeat';
    }
  }

  // Premium Code Begins Here //
  getRequest(): Promise<any> {
    return new Promise((resolve) => {
      this.responseTemp = JSON.parse(localStorage.getItem('quoteRequest'));
      console.log("Request Body", this.responseTemp);
      resolve();
    });
  }

  getDataFromReq(data: any) {
    console.log(data);
    this.productType = data.ProductType;
    if (data.ProductType == 'HBOOSTER') {
      console.log("HB", this.responseTemp);
      this.isHealthBooster = true; this.isCHI = false; this.isPPAP = false;this.isASP = false;
      this.PremiumForThreeYear(1, '');
      let SoftLessCopyBool: string;
      this.quoteRequest = this.responseTemp;
      SoftLessCopyBool = this.responseTemp.SoftLessCopy;
      if (SoftLessCopyBool == "true") { this.SoftLessCopyValue = 100; }
      else { this.SoftLessCopyValue = 0; }
      if (this.responseTemp.ProductName == "HBOOSTER_TOPUP") { this.HBProductName = "Top Up Plan"; }
      else { this.HBProductName = "Super Top Up Plan"; }
    } else if (data.ProductType == 'CHI') {
      this.isHealthBooster = false; this.isCHI = true; this.isPPAP = false;this.isASP = false;
      console.log("CHI", this.responseTemp);
      this.quoteRequest = this.responseTemp;
      let res = JSON.parse(localStorage.getItem('quoteResponse'));
      this.quoteResponse = res;
      console.log(this.quoteResponse);
    }else if (data.ProductType == 'ASP') {
      this.isHealthBooster = false; this.isCHI = false; this.isPPAP = false; this.isASP = true;
      this.quoteRequest = this.responseTemp;
      let res = JSON.parse(localStorage.getItem('quoteResponse'));
      this.quoteResponse = res;
    } else {
      this.isHealthBooster = false; this.isCHI = false; this.isPPAP = true;this.isASP = false;
      this.quoteRequest = this.responseTemp;
      let res = JSON.parse(localStorage.getItem('quoteResponse'));
      this.quoteResponse = res;
      if (this.quoteRequest.SubProduct == "PP") {
        this.SubProductName = "Lum Sum Payment Option";
      }
      else if (this.quoteRequest.SubProduct == "PPA") {
        this.SubProductName = "Monthly Payment Option";
      }
      else if (this.quoteRequest.SubProduct == "PPC") {
        this.SubProductName = "Lump Sum Payment Option With Monthly Payout";
      }
      else {
        console.log("Error");
      }
    }
  }

  ConvertToPolicy() {
    if (this.productType == 'PPAP') {
      this.openPEDModal();
    }
    else {
      this.router.navigateByUrl('health-proposal');
    }

  }

  async openPEDModal() {
    const gstModal = await this.modalController.create({
      component: PedQuestionaireComponent,
      showBackdrop: false,
      backdropDismiss: false
    });
    gstModal.onDidDismiss()
      .then((data) => {
        const gstData = data['data'];
        console.log(gstData) // Here's your selected user!
      });
    return await gstModal.present();

  }

  PremiumForOneYear(data: any) {
    console.log("for 1 Year", data);
    data.YearPremium = '1';
    console.log(data);
    let req = JSON.stringify(data);
    this.showLoading();
    localStorage.setItem('Request', req);
    this.cs.postWithParams('/api/HBooster/SaveEditQuote', req).then((res: any) => {
      console.log("New Response", res);
      if (res.StatusCode == 1) {
        console.log(res);
        this.quoteResponse = res;
        localStorage.setItem('quoteResponse', JSON.stringify(res));
        this.loading.dismiss();
      }
      else {
        this.loading.dismiss();
        this.presentAlert(res.StatusMessage);
      }

    }).catch((err) => {
      this.presentAlert(err);
      this.loading.dismiss();
    });
  }

  PremiumForTwoYear(data: any) {
    console.log("for 2 Year", data);
    data.YearPremium = '2';
    console.log(data);
    let req = JSON.stringify(data);
    this.showLoading();
    localStorage.setItem('Request', req);
    this.cs.postWithParams('/api/HBooster/SaveEditQuote', req).then((res: any) => {
      console.log("New Response", res);
      if (res.StatusCode == 1) {
        console.log(res);
        this.quoteResponse = res;
        localStorage.setItem('quoteResponse', JSON.stringify(res));
        this.loading.dismiss();
      }
      else {
        this.loading.dismiss();
        this.presentAlert(res.StatusMessage);
      }
    }).catch((err) => {
      this.presentAlert(err);
      this.loading.dismiss();
    });
  }

  PremiumForThreeYear(call, data: any) {
    console.log("Premium");
    if (call == 1) {
      let res = JSON.parse(localStorage.getItem('quoteResponse'));
      this.quoteResponse = res;
    }
    else {
      data.YearPremium = '3';
      console.log(data);
      let req = JSON.stringify(data);
      this.showLoading();
      localStorage.setItem('Request', req);
      this.cs.postWithParams('/api/HBooster/SaveEditQuote', req).then((res: any) => {
        console.log("New Response", res);
        if (res.StatusCode == 1) {
          console.log(res);
          this.quoteResponse = res;
          localStorage.setItem('quoteResponse', JSON.stringify(res));
          this.loading.dismiss();
        }
        else {
          this.loading.dismiss();
          this.presentAlert(res.StatusMessage);
        }
      }).catch((err) => {
        this.presentAlert(err);
        this.loading.dismiss();
      });
    }
    console.log(this.quoteResponse);
  }
  getHBData(year) {
    this.hbyearwiseData = year;
    if (year == 1) {
      this.PremiumForOneYear(this.responseTemp);
    }
    else if (year == 2) {
      this.PremiumForTwoYear(this.responseTemp);
    }
    else if (year == 3) {
      this.PremiumForThreeYear(2, this.responseTemp);
    }
  }

  showPremium() {

    // this.isQuoteDetails = !this.isQuoteDetails;
    if (this.showFormValidate(this.quoteForm.value)) {
      this.isQuoteGenerated = true;
      this.isPremiumDetails = true;
      setTimeout(function () { document.getElementById("premiumDetails").scrollIntoView(); }, 1000);
      if (this.isChecked && !this.isChecked1) {
        this.quoteResponse = this.nextAmountPremRes;
        this.quoteRequest = this.nextAmountPremRequ;
        localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
        document.getElementById("quoteDetails").style.background = "url(./assets/images/plus.png)"; document.getElementById("quoteDetailsBody").style.display = 'none';
        document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
      } else if (!this.isChecked && this.isChecked1) {
        this.quoteResponse = this.oldQuoteRes;
        this.quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
        localStorage.setItem('CHIAnnualSumInsured', JSON.stringify(this.quoteResponse.HealthDetails[0].InsuredAmount));
        document.getElementById("quoteDetails").style.background = "url(./assets/images/plus.png)"; document.getElementById("quoteDetailsBody").style.display = 'none';
        document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
      }
      else {
        this.presentAlert("Please Select atleast one Premium");
      }

    }
  }

  // Hospifund Premium //

  storeLocalData(tenure) {
    let Tenure = tenure;
    let covertedDOB = moment(this.dob, 'DD-MM-YY').format('YYYY-MM-DD');
    let todaysDate = moment();
    let duration = moment.duration(Tenure, 'years');
    let tempPolicyEndDate = todaysDate.add(duration).format('YYYY-MM-DD');
    let tdate = moment().format('YYYY-MM-DD');
    localStorage.setItem('Tenure', Tenure);
    localStorage.setItem('PolicyStartDate', tdate);
    localStorage.setItem('PolicyEndDate', tempPolicyEndDate);
    localStorage.setItem('inwardDate', tdate);
    localStorage.setItem('siData', this.siData.toString());
    localStorage.setItem('noofDays', this.noOfDays.toString());
  }
  triggerChangeOne() {
    this.premium2 = false; this.premium3 = false; this.premium1 = true;
    this.premiumType = 'oneyear';
  }
  triggerChangeTwo() {
    this.premium1 = false; this.premium3 = false; this.premium2 = true;
    this.premiumType = 'twoyear';
  }
  triggerChangeThree() {
    this.premium1 = false; this.premium2 = false; this.premium3 = true;
    this.premiumType = 'threeyear';
  }

  gotoProposal() {
    let dateerror = false;
    if (this.premium1 == true) {
      this.storeLocalData(1);
      let temprequest = localStorage.getItem('quoteRequest1');
      localStorage.setItem('quoteRequest', temprequest);
      let tempResponse = localStorage.getItem('quoteData1');
      localStorage.setItem('quoteData', tempResponse);
    }
    else if (this.premium2 == true) {
      this.storeLocalData(2);
      let temprequest = localStorage.getItem('quoteRequest2');
      localStorage.setItem('quoteRequest', temprequest);
      let tempResponse = localStorage.getItem('quoteData2');
      localStorage.setItem('quoteData', tempResponse);
    }
    else if (this.premium3 == true) {
      this.storeLocalData(3);
      let temprequest = localStorage.getItem('quoteRequest3');
      localStorage.setItem('quoteRequest', temprequest);
      let tempResponse = localStorage.getItem('quoteData3');
      localStorage.setItem('quoteData', tempResponse);
    }
    let temp = moment(this.dobInsured, 'DD-MM-YYYY').format('DD-MMMM-YYYY');
    let tempDate = moment().diff(temp, 'years');
    let tempDateMonths = moment().diff(temp, 'months');
    if (tempDate < 18) {
      localStorage.setItem('isAdultorKid', 'Child');
      if (tempDate < 1) {
        localStorage.setItem('ageofInsured', tempDateMonths.toString() + ' months');
      }
      else { localStorage.setItem('ageofInsured', tempDate.toString()); }
    }
    else if (tempDate >= 18 && tempDate < 100) {
      localStorage.setItem('isAdultorKid', 'Adult');
      localStorage.setItem('ageofInsured', tempDate.toString());
    }
    else if (this.selectedState == undefined || this.selectedState == null || this.selectedState == '') {
      this.presentAlert('Please select valid Insured DOB');
    }
    else {
      dateerror = true;
    }
    if (this.baseBenefit1 == false && this.baseBenefit2 == false) {
      this.presentAlert('Please select atleast one base benefit');
    }
    else if (dateerror) {
      this.presentAlert('Please select proper Insured DOB');
    }
    else if (this.premiumType == 'oneyear' && this.totalPremiuminRs1 < 100) {
      this.presentAlert('The premium cannot be less than ₹ 100. Please select a different combination of covers and/or SI per day/SI per event and Max No. of Days');
    }
    else if (this.premiumType == 'twoyear' && this.totalPremiuminRs2 < 100) {
      this.presentAlert('The premium cannot be less than ₹ 100. Please select a different combination of covers and/or SI per day/SI per event and Max No. of Days');
    }
    else if (this.premiumType == 'threeyear' && this.totalPremiuminRs3 < 100) {
      this.presentAlert('The premium cannot be less than ₹ 100. Please select a different combination of covers and/or SI per day/SI per event and Max No. of Days');
    }
    else {
      this.routes.navigateByUrl('hospifund-proposal');
    }


  }

  resetDisplayValues() {
    this.show3 = false;
    this.toggleButton = false;
  }

}
