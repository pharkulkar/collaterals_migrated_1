import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HealthProposalPage } from './health-proposal.page';
import { SearchCustComponent } from '../search-cust/search-cust.component';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule  } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { ApplicationPipesModule } from 'src/app/application-pipes/application-pipes.module';
const routes: Routes = [
  {
    path: '',
    component: HealthProposalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ApplicationPipesModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    RouterModule.forChild(routes)
  ],
  //declarations: [HealthProposalPage],
   declarations: [HealthProposalPage,SearchCustComponent],
  entryComponents: [SearchCustComponent, SearchCustComponent],
})
export class HealthProposalPageModule {}
