import { Component, OnInit, ChangeDetectorRef, ViewChild, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
// import { SearchCustComponent } from '../search-cust/search-cust.component';
import { ModalController, ToastController, LoadingController, AlertController } from '@ionic/angular';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { EcryDecryService } from 'src/app/services/ecry-decry.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { SearchCustComponent } from '../search-cust/search-cust.component';
import * as _ from 'underscore';
import { config } from 'src/app/config.properties';
@Component({
  selector: 'app-health-proposal',
  templateUrl: './health-proposal.page.html',
  styleUrls: ['./health-proposal.page.scss'],
})
export class HealthProposalPage implements OnInit {
  searchCustModal: any;
  agentData: any;
  data: any;
  memberObj: any;
  basicPropForm: FormGroup;
  requestFromPremium: any;
  jsonData: any;
  product: any;
  CHISubLimit: any;
  showSegment: any; showOtherPolicyDetails: boolean = false; showClaimsDetails: boolean = false;
  insuredForm: FormGroup; insuredPPAPForm: FormGroup;
  dummyForm: FormGroup;
  height_feet = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  height_inch = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  adultRelationShip = []; adultRelationArray = []; childRelationShip = []; childRelationArray = [];
  titleId: any;
  relationId: any;
  relation: any;
  questionList = [];
  titles = [{ "id": "0", "val": "Mrs." }, { "id": "1", "val": "Mr." }, { "id": "2", "val": "Ms." }];
  minDate: any;
  customPopoverOptions: [{}];
  // requestFromPremium: any;
  // jsonData: any;
  isAdult1 = false;
  isAdult2 = false;
  isChild1 = false;
  isChild2 = false;
  isChild3 = false;
  isBasicDetails = false;
  isInsuredDetails = true;
  isApplicantDetails = false;
  applTitle: any;
  appointeeTitle: any;
  nomineeTitle: any;

  titleId1: any; titleId2: any; titleId3: any; titleId4: any; titleId5: any;
  relationId1: any; relationId2: any; relationId3: any; relationId4: any; relationId5: any;
  InsuranceData: any; result: any; minRiskDate: any; maxRiskDate: any;
  productType: any; ppapDOB: any; ppapOccupation: any; validityyears = []; validityfromyears = []; ppapMemeberType: any;

  users: any; applicantForm: FormGroup; isPermanent = false; appointeeRelationId: any;
  isOther = false; isNomineeDeatils = false; isNew = true;
  // titleId1: any; titleId2: any;titleId3: any;  minDate: any;  relation: any;agentData: any;
  isCorraddSame = false; proposalResponse: any; pinData: any; pinDataPer: any;
  annualIncomeCode: any; maritalStatus: any; annualIncome: any; occupation: any;
  isNewCust = 'NO'; isRegWithGST: boolean = false; isExistPolicy1 = false; isExistPolicy2 = false; isExistPolicy3 = false; isExistPolicy4 = false; isExistPolicy5 = false; isDisease = false; isDisease2 = false; isDisease3 = false; isDisease4 = false; isDisease5 = false; isOtherPol = 'NO'; maritalStatusId: any;
  occupationId: any; annualIncomeId: any; searchData: any; isGSTN: boolean = true;
  isUIN = false; nomineeRelation: any; nomineeData: any; nomineeRelationId: any; constName: any; showApointee = false; apointee = false;
  custType: any; gstName: any; requestBody: any; responseBody: any; titleName: any; occName: any;
  marName: any; createCustData: any; pinDataValue: any; pinDataPermValue: any; insuredDetails: any;
  loading: any; quoteRequestData: any; ppapRealtions: any; childminDOB: any; childmaxDOB: any; eldestMax: any; eldestMin: any;
  ailmentArray1 = []; ailmentArray2 = []; ailmentArray3 = []; ailmentArray4 = []; ailmentArray5 = [];
  iskeralaCess: boolean = false; inputLoadingPlan = false;
  appCityList: any; corrCityId: any; appCityListPer: any; corrCityIdPer: any; ParentIMID: any;
  channelList: any; listOfChannel = []; listOfSPDetails = []; SPName: any;
  isSPFlagValid: boolean = false;isDisabled: boolean = false;
  constructor(public router: Router, public cdRef: ChangeDetectorRef, public alertController: AlertController, public cs: CommonService, public toastCtrl: ToastController, public modalCtrl: ModalController, public xml2Json: XmlToJsonService, public ErDrService: EcryDecryService, public loadingCtrl: LoadingController) { }

  ngOnInit() {
    console.log(localStorage.getItem('requestForProp'));
    var imgUrl = './assets/images/plus.png';
    //document.getElementById("basicDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("basicDetails").style.backgroundRepeat = 'no-repeat';
    //document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
    //document.getElementById("quoteDetailsBody").style.display = 'none';
    this.getCall().then(() => {
      this.router.navigate(['../health-proposal']);
    });
    this.getBackData();
    this.agentData = JSON.parse(localStorage.getItem('userData'));
    this.quoteRequestData = JSON.parse(localStorage.getItem('quoteRequest'));
    if (this.quoteRequestData.GSTStateCode == '62') {
      if (this.quoteRequestData.isGSTRegistered == true) {
        this.iskeralaCess = true;
        this.isRegWithGST = true;
      }
      else {
        this.iskeralaCess = false;
        this.isRegWithGST = false;
      }
    }
    else {
      this.iskeralaCess = false;
      this.isRegWithGST = false;
    }
    this.createBasicForm();
    this.getJsonData().then(() => {
      this.patchValue(this.requestFromPremium);
    });

    let date = new Date();
    this.minDate = moment(date).format('YYYY-MM-DD');
    var currentYear = date.getFullYear();
    var currentTillYear = date.getFullYear() + 2;
    var validFromYear = date.getFullYear() - 30;
    for (let i = validFromYear; i < currentYear; i++) {
      this.validityyears.push(i);
    }
    for (let i = validFromYear; i < currentTillYear; i++) {
      this.validityfromyears.push(i);
    }
    this.getRelations();
    this.getQuestionList();
    this.createApplForm();
    this.getAnnualIncome();
    this.getMaritalStatus();
    this.getOccupation();
    this.getRelation();
    this.getNomineeRelation();
    this.getInsuranceData();
    this.result = JSON.parse(localStorage.getItem('quoteRequest'));
    var today = new Date();
    this.productType = this.result.ProductType;
    this.childmaxDOB = new Date(new Date().setMonth(new Date().getMonth() - 3));
    if (this.productType == 'ASP') {
      this.childminDOB = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
    }
    else {
      this.childminDOB = new Date(new Date().setFullYear(new Date().getFullYear() - 21));
    }
    // this.childminDOB = new Date(new Date().setFullYear(new Date().getFullYear() - 21));
    this.childmaxDOB = moment(this.childmaxDOB).format('YYYY-MM-DD');
    this.childminDOB = moment(this.childminDOB).format('YYYY-MM-DD');
    if (this.productType == 'CHI' || this.productType == 'ASP') {
      this.createInsuredForm();
      let ageArray;
      if (this.result.NoOfAdults != 0) {
        ageArray = this.result.AgeGroup1.split("-");
      }
      else {
        ageArray = this.result.AgeGroup1;
      }
      var today1 = new Date();
      var tomorrow = new Date(today1.getTime() + (24 * 60 * 60 * 1000));
      this.eldestMax = new Date(today1.setFullYear(new Date().getFullYear() - ageArray[0]));
      this.eldestMin = new Date(tomorrow.setFullYear(new Date().getFullYear() - (parseInt(ageArray[1]) + 1)));
      this.eldestMax = moment(this.eldestMax).format('YYYY-MM-DD');
      this.eldestMin = moment(this.eldestMin).format('YYYY-MM-DD');

      // if (this.productType == 'ASP') {
      //   this.eldestMax = new Date(today1.setFullYear(new Date().getFullYear() - 18));
      // }
      // else {
      //   this.eldestMax = new Date(today1.setFullYear(new Date().getFullYear() - 21));
      // }

      localStorage.setItem('isOtherPolicyDetails', this.isOtherPol);
    }
    else if (this.productType == 'HBOOSTER') {
      this.createInsuredForm();
      localStorage.setItem('isOtherPolicyDetails', this.isOtherPol);
    }
    else {
      this.ppapDOB = this.result.DOB;
      let tempAgeDate = moment().diff(this.ppapDOB, 'years');
      this.minRiskDate = moment().add(1, 'days').format('YYYY-MM-DD');
      this.maxRiskDate = moment().add(15, 'days').format('YYYY-MM-DD');
      if (tempAgeDate < 18) { this.ppapMemeberType = 'Child'; } else { this.ppapMemeberType = 'Adult'; }
      this.ppapOccupation = this.result.Occupation;
      this.createPPAPInsuranceForm();
    }
    this.getJsonData().then(() => {
      this.showSegments(this.jsonData);
    });

    this.SPConditionCheck().then(() => {
      console.log("SP Check");
    })
  }


  //-Proposal Applicant Code Begins--//
  radio(val) {
    if (val == true) {
      this.isGSTN = true; this.isUIN = false;
    } else {
      this.isGSTN = false; this.isUIN = true;
    }
  }

  validateAlternateMobileLength(e: any) {
    if (e.target.value.length > 10) {
      this.presentAlert('Alterante Mobile No cannot more than 10 digits');
      this.applicantForm.patchValue({ 'applaltMobNo': null });
    }
  }
  validateMobileLength(e: any) {

    if (e.target.value.length > 10) {
      this.presentAlert('Mobile No cannot more than 10 digits');
      this.applicantForm.patchValue({ 'applMobNo': null });
    }

  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validateMobile(mobile) {
    var mob = /^[0-9]{10}$/;
    return mob.test(mobile);
  }
  validateAdhaarNo(addharno) {
    var re = /^[0-9]{12}$/;
    return re.test(addharno);
  }
  validatePanNo(panno) {
    var re = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
    return re.test(panno);
  }
  validateGST(gst) {
    var re = /^[0-9]{2}[A-Z]{3}[PCHABGJLEFT][A-Z][0-9]{4}[A-Z]{1}[0-9A-Z]{1}[Z]{1}[0-9A-Z]{1}$/;
    let gstvalid = re.test(gst);
    if (gstvalid) {
      let gstcodeObtained = gst.substring(0, 2);
      let stateData = JSON.parse(localStorage.getItem('selectedStateData'));
      let gstcode = stateData.GSTStateCode;
      if (gstcodeObtained == gstcode) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return gstvalid;
    }
  }

  calculateMemberAge(val) {
    let age = moment().diff(moment(val).format('DD-MMM-YYYY'), 'years');

  }


  ValidateGSTSection() {
    if (this.isRegWithGST == true) {

      if (this.applicantForm.value.constitution == undefined || this.applicantForm.value.constitution == null || this.applicantForm.value.constitution == '') {
        this.presentAlert("Please select constitution of business for GST section"); return false;
      }
      else if (this.applicantForm.value.panNo == undefined || this.applicantForm.value.panNo == null || this.applicantForm.value.panNo == '') {
        this.presentAlert("Please enter pan card number for GST section"); return false;
      }
      else if (this.applicantForm.value.panNo != undefined && this.applicantForm.value.panNo != null && this.applicantForm.value.panNo != '' && !this.validatePanNo(this.applicantForm.value.panNo)) {
        this.presentAlert('Please enter valid pan card number for GST section'); return false;
      }
      else if (this.applicantForm.value.customerType == undefined || this.applicantForm.value.customerType == null || this.applicantForm.value.customerType == '') {
        this.presentAlert("Please select customer type for GST section"); return false;
      }
      else if (this.applicantForm.value.gstRegStatus == undefined || this.applicantForm.value.gstRegStatus == null || this.applicantForm.value.gstRegStatus == '') {
        this.presentAlert("Please select registration status for GST section"); return false;
      }
      else if ((this.applicantForm.value.GSTIN == undefined || this.applicantForm.value.GSTIN == null || this.applicantForm.value.GSTIN == '') && this.isGSTN) {
        this.presentAlert("Please enter GSTIN number"); return false;
      }
      else if (!this.validateGST(this.applicantForm.value.GSTIN) && this.isGSTN) {
        this.presentAlert("Please enter valid GSTIN number"); return false;
      }
      else {
        return true;
      }
    }
    else {
      return true;
    }

  }

  ValidationApplicant() {
    let isGSTSectionValid = this.ValidateGSTSection();
    if (!isGSTSectionValid) { return false; }
    else if (this.applicantForm.value.aadhaarNo != undefined && this.applicantForm.value.aadhaarNo != null && this.applicantForm.value.aadhaarNo != '' && !this.validateAdhaarNo(this.applicantForm.value.aadhaarNo)) {
      this.presentAlert('Kindly Enter Valid Applicant Aadhaar Number'); return false;
    } else if (this.applicantForm.value.panNumber != undefined && this.applicantForm.value.panNumber != null && this.applicantForm.value.panNumber != '' && !this.validatePanNo(this.applicantForm.value.panNumber)) {
      this.presentAlert('Kindly Enter Valid Applicant Pan Card Number'); return false;
    } else if (this.titleId1 == undefined || this.titleId1 == null || this.titleId1 == '') {
      this.presentAlert("Kindly Insert Applicant Title"); return false;
    } else if (this.applicantForm.value.applName == undefined || this.applicantForm.value.applName == null || this.applicantForm.value.applName == '') {
      this.presentAlert("Kindly Insert Applicant Name"); return false;
    } else if (this.applicantForm.value.applDOB == undefined || this.applicantForm.value.applDOB == null || this.applicantForm.value.applDOB == '') {
      this.presentAlert("Kindly Enter Applicant Date of Birth"); return false;
    } else if (this.applicantForm.value.applAdd1 == undefined || this.applicantForm.value.applAdd1 == null || this.applicantForm.value.applAdd1 == '') {
      this.presentAlert("Kindly Insert Applicant Correspondence Address"); return false;
    } else if (this.applicantForm.value.applPinCode == undefined || this.applicantForm.value.applPinCode == null || this.applicantForm.value.applPinCode == '') {
      this.presentAlert("Kindly Insert Applicant Pincode"); return false;
    } else if (this.applicantForm.value.applCity == undefined || this.applicantForm.value.applCity == null || this.applicantForm.value.applCity == '') {
      this.presentAlert("Applicant City not found"); return false;
    } else if (this.applicantForm.value.applState == undefined || this.applicantForm.value.applState == null || this.applicantForm.value.applState == '') {
      this.presentAlert("Applicant State not found"); return false;
    } else if (!this.validateEmail(this.applicantForm.value.applEmail)) {
      this.presentAlert("Kindly Insert Applicant Email ID"); return false;
    } else if (this.applicantForm.value.applMarital == undefined || this.applicantForm.value.applMarital == null || this.applicantForm.value.applMarital == '') {
      this.presentAlert("Kindly Select Applicant Marital Status"); return false;
    } else if (this.applicantForm.value.applMobNo == undefined || this.applicantForm.value.applMobNo == null || this.applicantForm.value.applMobNo == '') {
      this.presentAlert("Kindly Insert Your Mobile Number"); return false;
    }

    else if (this.applicantForm.value.permAdd1 == undefined || this.applicantForm.value.permAdd1 == null || this.applicantForm.value.permAdd1 == '') {
      this.presentAlert("Kindly Insert Applicant Permanent Address"); return false;
    } else if (this.applicantForm.value.permPinCode == undefined || this.applicantForm.value.permPinCode == null || this.applicantForm.value.permPinCode == '') {
      this.presentAlert("Kindly Insert Applicant Permanent Pincode"); return false;
    } else if (this.applicantForm.value.permCity == undefined || this.applicantForm.value.permCity == null || this.applicantForm.value.permCity == '') {
      this.presentAlert("Applicant Permanent City not found"); return false;
    } else if (this.applicantForm.value.permState == undefined || this.applicantForm.value.permState == null || this.applicantForm.value.permState == '') {
      this.presentAlert("Applicant Permanent State not found"); return false;
    } else if (this.applicantForm.value.nomTitle == undefined || this.applicantForm.value.nomTitle == null || this.applicantForm.value.nomTitle == '') {
      this.presentAlert("Kindly Select Nominee Title"); return false;
    } else if (this.applicantForm.value.nomName == undefined || this.applicantForm.value.nomName == null || this.applicantForm.value.nomName == '') {
      this.presentAlert("Kindly Insert Nominee Name"); return false;
    } else if (this.applicantForm.value.nomRelation == undefined || this.applicantForm.value.nomRelation == null || this.applicantForm.value.nomRelation == '') {
      this.presentAlert("Kindly Select Nominee Relation"); return false;
    } else if (this.applicantForm.value.nomDOB == undefined || this.applicantForm.value.nomDOB == null || this.applicantForm.value.nomDOB == '') {
      this.presentAlert("Kindly Select Nominee Date of Birth"); return false;

    } else if (!this.validateMobile(this.applicantForm.value.applMobNo)) {
      this.presentAlert("Mobile Number should be 10 digit"); return false;
    } else if (this.applicantForm.value.applaltMobNo != undefined && this.applicantForm.value.applaltMobNo != null && this.applicantForm.value.applaltMobNo != '' && !this.validateMobile(this.applicantForm.value.applaltMobNo)) {
      this.presentAlert("Alternate Mobile Number should be 10 digit"); return false;
    } else if (this.applicantForm.value.applaltEmail != undefined && this.applicantForm.value.applaltEmail != null && this.applicantForm.value.applaltEmail != '' && !this.validateEmail(this.applicantForm.value.applaltEmail)) {
      this.presentAlert("Alternate Email should be valid"); return false;
    } else {
      return true;
    }
  }



  validatedInsuredMembers() {
    let totalAdults, totalKids, isAdult1Valid = true, isAdult2Valid = true, isChild1Valid = true, isChild2Valid = true, isChild3Valid = true;
    if (this.quoteRequestData.NoOfAdults != undefined && this.quoteRequestData.NoOfAdults != null && this.quoteRequestData.NoOfAdults != '') {
      totalAdults = this.quoteRequestData.NoOfAdults;
    }
    else {
      totalAdults = 0;
    }
    if (this.quoteRequestData.NoOfKids != undefined && this.quoteRequestData.NoOfKids != null && this.quoteRequestData.NoOfKids != '') {
      totalKids = this.quoteRequestData.NoOfKids;
    }
    else {
      totalKids = 0;
    }
    if (totalAdults == 1 || totalAdults == 2) {
      if (this.insuredForm.value.member1Rela == undefined || this.insuredForm.value.member1Rela == null || this.insuredForm.value.member1Rela == '') {
        this.presentAlert("Kindly Select Relationship of Adult 1 under Insured"); isAdult1Valid = false;
      } else if (this.insuredForm.value.member1Title == undefined || this.insuredForm.value.member1Title == null || this.insuredForm.value.member1Title == '') {
        this.presentAlert("Kindly Select Title of Adult 1 under Insured"); isAdult1Valid = false;
      } else if (this.insuredForm.value.member1Name == undefined || this.insuredForm.value.member1Name == null || this.insuredForm.value.member1Name == '') {
        this.presentAlert("Kindly Insert Name of Adult 1 under Insured"); isAdult1Valid = false;
      } else if (this.insuredForm.value.member1Weight == undefined || this.insuredForm.value.member1Weight == null || this.insuredForm.value.member1Weight.toString() == '') {
        this.presentAlert("Kindly Insert Weight of Adult 1 under Insured"); isAdult1Valid = false;
      } else if (this.insuredForm.value.member1Feet == undefined || this.insuredForm.value.member1Feet == null || this.insuredForm.value.member1Feet == '') {
        this.presentAlert("Kindly Insert Height in Feet of Adult 1 under Insured"); isAdult1Valid = false;
      } else if (this.insuredForm.value.member1Inches == undefined || this.insuredForm.value.member1Inches == null || this.insuredForm.value.member1Inches == '') {
        this.presentAlert("Kindly Insert Height in Inches of Adult 1 under Insured"); isAdult1Valid = false;
      }
      else if (this.productType == 'CHI' || this.productType == 'ASP') {
        if (this.insuredForm.value.member1DOB == undefined || this.insuredForm.value.member1DOB == null || this.insuredForm.value.member1DOB == '') {
          this.presentAlert("Kindly Insert DOB of Adult 1 under Insured"); isAdult1Valid = false;
        }
      }
    }
    else {
      isAdult1Valid = true;
    }
    if (totalAdults == 2) {
      if (this.insuredForm.value.member2Rela == undefined || this.insuredForm.value.member2Rela == null || this.insuredForm.value.member2Rela == '') {
        this.presentAlert("Kindly Select Relationship for Adult 2 under Insured"); isAdult2Valid = false;
      } else if (this.insuredForm.value.member2Title == undefined || this.insuredForm.value.member2Title == null || this.insuredForm.value.member2Title == '') {
        this.presentAlert("Kindly Select Title for Adult 2 under Insured"); isAdult2Valid = false;
      } else if (this.insuredForm.value.member2Name == undefined || this.insuredForm.value.member2Name == null || this.insuredForm.value.member2Name == '') {
        this.presentAlert("Kindly Insert Name for Adult 2 under Insured"); isAdult2Valid = false;
      } else if (this.insuredForm.value.member2Weight == undefined || this.insuredForm.value.member2Weight == null || this.insuredForm.value.member2Weight.toString() == '') {
        this.presentAlert("Kindly Insert Weight for Adult 2 under Insured"); isAdult2Valid = false;
      } else if (this.insuredForm.value.member2Feet == undefined || this.insuredForm.value.member2Feet == null || this.insuredForm.value.member2Feet == '') {
        this.presentAlert("Kindly Insert Height in Feet for Adult 2 under Insured"); isAdult2Valid = false;
      } else if (this.insuredForm.value.member2Inches == undefined || this.insuredForm.value.member2Inches == null || this.insuredForm.value.member2Inches == '') {
        this.presentAlert("Kindly Insert Height in Inches for Adult 2 under Insured"); isAdult2Valid = false;
      }
      else if (this.productType == 'CHI' || this.productType == 'ASP') {
        if (this.insuredForm.value.member2DOB == undefined || this.insuredForm.value.member2DOB == null || this.insuredForm.value.member2DOB == '') {
          this.presentAlert("Kindly Insert DOB of Adult 2 under Insured"); isAdult1Valid = false;
        }
      }
    }
    else {
      isAdult2Valid = true;
    }
    if (totalKids == 1 || totalKids == 2 || totalKids == 3) {
      if (this.insuredForm.value.child1Rela == undefined || this.insuredForm.value.child1Rela == null || this.insuredForm.value.child1Rela == '') {
        this.presentAlert("Kindly Select Relationship for Child 1 under Insured"); isChild1Valid = false;
      } else if (this.insuredForm.value.child1Title == undefined || this.insuredForm.value.child1Title == null || this.insuredForm.value.child1Title == '') {
        this.presentAlert("Kindly Select Title for Child 1 under Insured"); isChild1Valid = false;
      } else if (this.insuredForm.value.child1Name == undefined || this.insuredForm.value.child1Name == null || this.insuredForm.value.child1Name == '') {
        this.presentAlert("Kindly Insert Name for Child 1 under Insured"); isChild1Valid = false;
      } else if (this.insuredForm.value.child1DOB == undefined || this.insuredForm.value.child1DOB == null || this.insuredForm.value.child1DOB == '') {
        this.presentAlert("Kindly Insert DOB for Child 1 under Insured"); isChild1Valid = false;
      } else if (this.insuredForm.value.child1Weight == undefined || this.insuredForm.value.child1Weight == null || this.insuredForm.value.child1Weight.toString() == '') {
        this.presentAlert("Kindly Insert Weight for Child 1 under Insured"); isChild1Valid = false;
      } else if (this.insuredForm.value.child1Feet == undefined || this.insuredForm.value.child1Feet == null || this.insuredForm.value.child1Feet == '') {
        this.presentAlert("Kindly Insert Height in Feet for Child 1 under Insured"); isChild1Valid = false;
      } else if (this.insuredForm.value.child1Inches == undefined || this.insuredForm.value.child1Inches == null || this.insuredForm.value.child1Inches == '') {
        this.presentAlert("Kindly Insert Height in Inches for Child 1 under Insured"); isChild1Valid = false;
      }
    }
    else {
      isChild1Valid = true;
    }
    if (totalKids == 2 || totalKids == 3) {
      if (this.insuredForm.value.child2Rela == undefined || this.insuredForm.value.child2Rela == null || this.insuredForm.value.child2Rela == '') {
        this.presentAlert("Kindly Select  Relationship Child 2 under Insured"); isChild2Valid = false;
      } else if (this.insuredForm.value.child2Rela == undefined || this.insuredForm.value.child2Rela == null || this.insuredForm.value.child2Rela == '') {
        this.presentAlert("Kindly Select Title for Child 2 under Insured"); isChild2Valid = false;
      } else if (this.insuredForm.value.child2Name == undefined || this.insuredForm.value.child2Name == null || this.insuredForm.value.child2Name == '') {
        this.presentAlert("Kindly Insert Name for Child 2 under Insured"); isChild2Valid = false;
      } else if (this.insuredForm.value.child2DOB == undefined || this.insuredForm.value.child2DOB == null || this.insuredForm.value.child2DOB == '') {
        this.presentAlert("Kindly Insert DOB for Child 2 under Insured"); isChild2Valid = false;
      } else if (this.insuredForm.value.child2Weight == undefined || this.insuredForm.value.child2Weight == null || this.insuredForm.value.child2Weight.toString() == '') {
        this.presentAlert("Kindly Insert Weight for Child 2 under Insured"); isChild2Valid = false;
      } else if (this.insuredForm.value.child2Feet == undefined || this.insuredForm.value.child2Feet == null || this.insuredForm.value.child2Feet == '') {
        this.presentAlert("Kindly Insert Height in Feet for Child 2 under Insured"); isChild2Valid = false;
      } else if (this.insuredForm.value.child2Inches == undefined || this.insuredForm.value.child2Inches == null || this.insuredForm.value.child2Inches == '') {
        this.presentAlert("Kindly Insert Height in Inches for Child 2 under Insured"); isChild2Valid = false;
      }
    }
    else {
      isChild2Valid = true;
    }
    if (totalKids == 3) {
      if (this.insuredForm.value.child1Rela == undefined || this.insuredForm.value.child1Rela == null || this.insuredForm.value.child1Rela == '') {
        this.presentAlert("Kindly Select Relationship for Child 3 under Insured"); isChild3Valid = false;
      } else if (this.insuredForm.value.child3Title == undefined || this.insuredForm.value.child3Title == null || this.insuredForm.value.child3Title == '') {
        this.presentAlert("Kindly Select Title for Child 3 under Insured"); isChild3Valid = false;
      } else if (this.insuredForm.value.child3Name == undefined || this.insuredForm.value.child3Name == null || this.insuredForm.value.child3Name == '') {
        this.presentAlert("Kindly Insert Name for Child 3 under Insured"); isChild3Valid = false;
      } else if (this.insuredForm.value.child3DOB == undefined || this.insuredForm.value.child3DOB == null || this.insuredForm.value.child3DOB == '') {
        this.presentAlert("Kindly Insert DOB for Child 3 under Insured"); isChild3Valid = false;
      } else if (this.insuredForm.value.child3Weight == undefined || this.insuredForm.value.child3Weight == null || this.insuredForm.value.child3Weight.toString() == '') {
        this.presentAlert("Kindly Insert Weight for Child 3 under Insured"); isChild3Valid = false;
      } else if (this.insuredForm.value.child3Feet == undefined || this.insuredForm.value.child3Feet == null || this.insuredForm.value.child3Feet == '') {
        this.presentAlert("Kindly Insert Height in Feet for Child 3 under Insured"); isChild3Valid = false;
      } else if (this.insuredForm.value.child3Inches == undefined || this.insuredForm.value.child3Inches == null || this.insuredForm.value.child3Inches == '') {
        this.presentAlert("Kindly Insert Height in Inches for Child 3 under Insured"); isChild3Valid = false;
      }
    }
    else {
      isChild3Valid = true;
    }
    if (isAdult1Valid == true && isAdult2Valid == true && isChild1Valid == true) {
      return true;
    }
    else {
      return false;
    }
  }

  continueInsured() {
    let isinsuredFeildsValid = this.validatedInsuredMembers();
    if (isinsuredFeildsValid == true) {
      this.patchDataOnSelf();
      this.isBasicDetails = false;
      // this.isInsuredDetails = false;
      // setTimeout(function(){document.getElementById("applicantDetails").scrollIntoView();}, 1000);
      document.getElementById("insuredDetails").style.background = "url(./assets/images/plus.png)";
      document.getElementById("insuredDetailsBody").style.display = 'none';
      document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
      this.isApplicantDetails = true;
    }
  }
  patchDataOnSelf() {

    // let element = {};
    let element = this.insuredForm.value

    if (element.member1Rela == 'SELF') {
      this.isDisabled = true;

      this.applicantForm.patchValue({ 'applTitle': element.member1Title });
      let temp = this.titles.filter(x => x.val == element.member1Title);
      this.titleId1 = temp[0];
      this.applicantForm.patchValue({ 'applName': element.member1Name });
      this.applicantForm.patchValue({ 'applDOB': element.member1DOB });

      if (this.productType == 'HBOOSTER') {
        let dateAppl;
        if (element.member1Cat == 'Adult') {
          dateAppl = moment(this.quoteRequestData.Adult1Age, 'DD-MMM-YYYY').format('MM/DD/YYYY');
        }
        else if (element.member1Cat == 'Adult') {
          dateAppl = moment(this.quoteRequestData.Adult2Age, 'DD-MMM-YYYY').format('MM/DD/YYYY');
        }
        else {
          dateAppl = moment(element.member1DOB, 'DD-MMM-YYYY').format('MM/DD/YYYY');
        }
        this.applicantForm.patchValue({ 'applDOB': new Date(dateAppl) });
      } else if (this.productType == 'PPAP') {
        let dateAppl = moment(this.quoteRequestData.DOB, 'DD-MMMM-YYYY').format('MM/DD/YYYY');
        this.applicantForm.patchValue({ 'applDOB': new Date(dateAppl) });
      }
    }
    else if (element.member2Rela == 'SELF') {
      this.isDisabled = true;

      this.applicantForm.patchValue({ 'applTitle': element.member2Title });
      this.titleId2 = this.titles.filter(x => x.val == element.member2Title)
      this.applicantForm.patchValue({ 'applName': element.member2Name });
      this.applicantForm.patchValue({ 'applDOB': element.member2DOB });

      if (this.productType == 'HBOOSTER') {
        let dateAppl;
        if (element.member2Cat == 'Adult') {
          dateAppl = moment(this.quoteRequestData.Adult1Age, 'DD-MMM-YYYY').format('MM/DD/YYYY');
        }
        else if (element.member2Cat == 'Adult') {
          dateAppl = moment(this.quoteRequestData.Adult2Age, 'DD-MMM-YYYY').format('MM/DD/YYYY');
        }
        else {
          dateAppl = moment(element.member1DOB, 'DD-MMM-YYYY').format('MM/DD/YYYY');
        }
        this.applicantForm.patchValue({ 'applDOB': new Date(dateAppl) });
      } else if (this.productType == 'PPAP') {
        let dateAppl = moment(this.quoteRequestData.DOB, 'DD-MMMM-YYYY').format('MM/DD/YYYY');
        this.applicantForm.patchValue({ 'applDOB': new Date(dateAppl) });
      }
    }
    else if (element.child1Rela == 'SELF') {
      this.isDisabled = true;

      this.applicantForm.patchValue({ 'applTitle': element.child1Title });
      this.titleId3 = this.titles.filter(x => x.val == element.child1Title)
      this.applicantForm.patchValue({ 'applName': element.child1Name });
      this.applicantForm.patchValue({ 'applDOB': element.child1DOB });
    }
    else if (element.child2Rela == 'SELF') {
      this.isDisabled = true;

      this.applicantForm.patchValue({ 'applTitle': element.child2Title });
      this.titleId4 = this.titles.filter(x => x.val == element.child2Title)
      this.applicantForm.patchValue({ 'applName': element.child2Name });
      this.applicantForm.patchValue({ 'applDOB': element.child2DOB });
    }
    else if (element.child3Rela == 'SELF') {
      this.isDisabled = true;
      this.applicantForm.patchValue({ 'applTitle': element.child3Title });
      this.titleId1 = this.titles.filter(x => x.val == element.child3Title)
      this.applicantForm.patchValue({ 'applName': element.child3Name });
      this.applicantForm.patchValue({ 'applDOB': element.child3DOB });
    }
  }

  SimilarFieldsValidator(formData: any) {
    // if (formData.value.insuredpanno == null) {
    //   this.presentAlert('Please select pan card number for insured member');
    //   return false;
    // }
    if (formData.value.insuredpanno != undefined && formData.value.insuredpanno != null && formData.value.insuredpanno != '' && !this.validatepanCard(formData.value.insuredpanno)) {
      this.presentAlert('Please select valid pan card number for insured member');
      return false;
    }
    if (formData.value.member1Cat == null) {
      this.presentAlert('Member Type not present');
      return false;
    }
    else if (formData.value.member1Rela == null || formData.value.member1Rela == '') {
      this.presentAlert('Please select relationship with applicant for insured member');
      return false;
    }
    else if (formData.value.member1Title == null || formData.value.member1Title == '') {
      this.presentAlert('Please select  title for insured member'); return false;
    }
    else if (formData.value.member1Name == null) {
      this.presentAlert('Please select full name for insured member'); return false;
    }
    else if (formData.value.member1DOB == null) {
      this.presentAlert('Date of birth not present for insured member'); return false;
    }
    else if (formData.value.riskStartDate == null) {
      this.presentAlert('Please select Risk Date for insured member'); return false;
    }

    else if (formData.value.insuredOccupation == null) {
      this.presentAlert('Occupation not present for insured member'); return false;
    }
    else {
      return true;
    }
  }

  ValidationCheckPPAP(formData: any) {
    formData = this.insuredPPAPForm;
    if (this.showOtherPolicyDetails == true) {
      let isBasicFeildsValid = this.SimilarFieldsValidator(formData);
      if (!isBasicFeildsValid) { return false; }
      else if (formData.value.insurancePolicy == null || formData.value.insurancePolicy == '') {
        this.presentAlert('Please select Insurance Policy No for insured member'); return false;
      }
      else if (formData.value.policyValidFrom == null && formData.value.policyValidTill == null) {
        this.presentAlert('Please select Both policy valid from and policy valid till for insured member'); return false;
      }
      else if (formData.value.policyValidFrom == null || formData.value.policyValidFrom == '') {
        this.presentAlert('Please select policy valid from for insured member'); return false;
      }
      else if (formData.value.policyValidTill == '' || formData.value.policyValidTill == '') {
        this.presentAlert('Please select policy valid till for insured member'); return false;
      }
      else if (formData.value.policyValidFrom > formData.value.policyValidTill) {
        this.presentAlert('Please select policy valid till field must be less than policy valid from field');
        return false;
      }
      else if (formData.value.policySumInsured == null || formData.value.policySumInsured == '') {
        this.presentAlert('Please select Sum Insured for insured member'); return false;
      }
      else if (formData.value.insuranceComapny == null || formData.value.insuranceComapny == '') {
        this.presentAlert('Please select Insurance Company for insured member'); return false;
      }
      else {
        this.isBasicDetails = false;
        $('#insuredDetails').addClass('collapsed');
        $('#applicantDetailsHead').removeClass('collapsed')
        $('#insuredDetailsBody').removeClass('show');
        this.isApplicantDetails = true;
        if (this.insuredPPAPForm.value.member1Rela == 'SELF') {
          this.applicantForm.patchValue({ 'applTitle': this.insuredPPAPForm.value.member1Title });
          this.applicantForm.patchValue({ 'applName': this.insuredPPAPForm.value.member1Name });
          this.applicantForm.patchValue({ 'applDOB': moment(this.insuredPPAPForm.value.member1DOB, 'DD-MMM-YYYY').format('YYYY-MM-DD') });
          let temp = this.titles.filter(x => x.val == this.insuredPPAPForm.value.member1Title);
          this.titleId1 = temp[0];
        }
        return true
      }
    }
    else {
      let isBasicFeildsValid = this.SimilarFieldsValidator(formData);
      if (!isBasicFeildsValid) { return false; } else {
        this.isBasicDetails = false;
        $('#insuredDetails').addClass('collapsed');
        $('#applicantDetailsHead').removeClass('collapsed')
        $('#insuredDetailsBody').removeClass('show');
        this.isApplicantDetails = true;
        if (this.insuredPPAPForm.value.member1Rela == 'SELF') {
          this.applicantForm.patchValue({ 'applTitle': this.insuredPPAPForm.value.member1Title });
          this.applicantForm.patchValue({ 'applName': this.insuredPPAPForm.value.member1Name });
          this.applicantForm.patchValue({ 'applDOB': moment(this.insuredPPAPForm.value.member1DOB, 'DD-MMM-YYYY').format('YYYY-MM-DD') });
          let temp = this.titles.filter(x => x.val == this.insuredPPAPForm.value.member1Title);
          this.titleId1 = temp[0];
        }
        return true;
      }
    }
  }

  showFormValidate(form: any) {
    let message: any;
    // if (!this.insuredForm.value.member1Rela) {
    //   message = "Kindly Select Relationship with applicant in Insured Page";
    //   this.presentToast(message);
    // } 
    let Htype = JSON.parse(localStorage.getItem('quoteRequest'));
    if (Htype.ProductType == 'HBOOSTER' || Htype.ProductType == 'CHI' || this.productType == 'ASP') {
      let isapplicantFeildsValid = this.ValidationApplicant();
      let isinsuredFeildsValid = this.validatedInsuredMembers();
      if (isapplicantFeildsValid == true && isinsuredFeildsValid == true) {
        return true;
      }
      else {
        return false;
      }
    } else {
      if (this.showOtherPolicyDetails == true) {
        let isBasicFeildsValid = this.SimilarFieldsValidator(this.insuredPPAPForm);
        let isapplicantFeildsValid = this.ValidationApplicant();
        if (isBasicFeildsValid == true && isapplicantFeildsValid == true) {
          if (this.insuredPPAPForm.value.policyValidFrom == '' && this.insuredPPAPForm.value.policyValidTill == '') {
            this.presentAlert('Please select Both policy valid from and policy valid to'); return false;
          } else if (this.insuredPPAPForm.value.policyValidFrom == '') {
            this.presentAlert('Please select policy valid from'); return false;
          } else if (this.insuredPPAPForm.value.policyValidTill == '') {
            this.presentAlert('Please select policy valid to'); return false;
          } else if (this.insuredPPAPForm.value.policyValidFrom > this.insuredPPAPForm.value.policyValidTill) {
            this.presentAlert('Please select policy valid to field must be less than policy valid from field'); return false;
          } else if (this.insuredPPAPForm.value.insuranceComapny == null && this.insuredPPAPForm.value.insuranceComapny == '') {
            this.presentAlert('Please select Insurance Company'); return false;
          } else if (this.insuredPPAPForm.value.insurancePolicy == null) {
            this.presentAlert('Please select Insurance Policy'); return false;
          } else if (this.insuredPPAPForm.value.policySumInsured == null) {
            this.presentAlert('Please select Sum Insured'); return false;
          } else if (this.applicantForm.value.applTitle == undefined || this.applicantForm.value.applTitle == null || this.applicantForm.value.applTitle == '') {
            this.presentAlert("Kindly Select Applicant Title"); return false;
          }
          else {
            return true;
          }
        }
        else {
          return false
        }
      }
      else {
        let isBasicFeildsValid = this.SimilarFieldsValidator(this.insuredPPAPForm);
        let isapplicantFeildsValid = this.ValidationApplicant();
        if (isBasicFeildsValid == true && isapplicantFeildsValid == true) {
          return true
        }
        else {
          return false;
        }
      }

    }
  }



  createApplForm() {
    this.applicantForm = new FormGroup({
      aadhaarNo: new FormControl(),
      panNumber: new FormControl(),
      applTitle: new FormControl(''),
      applName: new FormControl(),
      applDOB: new FormControl(),
      applMobNo: new FormControl(),
      applAdd1: new FormControl(),
      applAdd2: new FormControl(),
      applLandMark: new FormControl(),
      applPinCode: new FormControl(),
      applCity: new FormControl(''),
      applState: new FormControl(),
      // isRegWithGST: new FormControl(),
      applMarital: new FormControl(''),
      applOccu: new FormControl(''),
      annuIncome: new FormControl(''),
      applEmail: new FormControl(),
      applaltEmail: new FormControl(),
      applaltMobNo: new FormControl(),
      landLine: new FormControl(),
      isCorrAndPerSame: new FormControl(),
      permAdd1: new FormControl(),
      permAdd2: new FormControl(),
      permLandMark: new FormControl(),
      permPinCode: new FormControl(),
      permCity: new FormControl(''),
      permState: new FormControl(),

      // sp changes
      SPAlternateRMcode: new FormControl(),
      SPName: new FormControl(),
      SPUniqueReferenceNo: new FormControl(),
      SPChannelName: new FormControl(),
      SPPrimaryRMCode: new FormControl(),
      SPSecondaryRMCode: new FormControl(),
      SPBancaField_01: new FormControl(),
      SPBancaField_02: new FormControl(),
      SPBancaField_03: new FormControl(),

      // isOtherPol: new FormControl(),
      _80DTax: new FormControl('Annual'),
      nomTitle: new FormControl(''),
      nomName: new FormControl(),
      nomRelation: new FormControl(''),
      nomDOB: new FormControl(),
      apsId: new FormControl(),
      custRefNo: new FormControl(),
      sourcingCode: new FormControl(),
      //
      GSTIN: new FormControl(),
      UIN: new FormControl(),
      gstRegStatus: new FormControl(''),
      customerType: new FormControl(''),
      panNo: new FormControl(),
      constitution: new FormControl(''),
      appTitle: new FormControl(''),
      appName: new FormControl(),
      appRelation: new FormControl(''),
      appDOB: new FormControl()
    });
  }

  // searchUser(form: any) {
  //   this.searchCust();
  //   console.log(form.value);
  // }

  isNewCustomer(ev: any) {
    // console.log(ev.detail.checked);
    if (ev == true) {
      this.isNew = true;
      this.isNewCust = 'YES';
      this.modalCtrl.dismiss();
      this.applicantForm.reset();
      this.applicantForm.patchValue({ 'applTitle': '' });
      this.applicantForm.patchValue({ 'annuIncome': '' });
      this.applicantForm.patchValue({ 'applOccu': '' });
      this.applicantForm.patchValue({ 'applMarital': '' });
      this.applicantForm.patchValue({ 'nomTitle': '' });
      this.applicantForm.patchValue({ 'nomRelation': '' });
      this.applicantForm.patchValue({ 'nomRelation': '' });
      this.applicantForm.patchValue({ 'customerType': '' });
      this.applicantForm.patchValue({ 'constitution': '' });
      this.applicantForm.patchValue({ 'gstRegStatus': '' });
      this.applicantForm.patchValue({ 'appTitle': '' });
      this.applicantForm.patchValue({ 'appRelation': '' });
    } else {
      this.applicantForm.reset();
      this.applicantForm.patchValue({ 'applTitle': '' });
      this.applicantForm.patchValue({ 'annuIncome': '' });
      this.applicantForm.patchValue({ 'applOccu': '' });
      this.applicantForm.patchValue({ 'applMarital': '' });
      this.applicantForm.patchValue({ 'nomTitle': '' });
      this.applicantForm.patchValue({ 'nomRelation': '' });
      this.applicantForm.patchValue({ 'nomRelation': '' });
      this.applicantForm.patchValue({ 'customerType': '' });
      this.applicantForm.patchValue({ 'constitution': '' });
      this.applicantForm.patchValue({ 'gstRegStatus': '' });
      this.applicantForm.patchValue({ 'appTitle': '' });
      this.applicantForm.patchValue({ 'appRelation': '' });
      this.isNew = false;
      this.isNewCust = 'NO';
      this.searchCust();
    }
  }

  isOtherPolicy(val) {
    if (val == 'YES') {
      this.isOtherPol = 'YES';
    } else {
      this.isOtherPol = 'NO';
    }
    localStorage.setItem('isOtherPolicyDetails', this.isOtherPol);
  }

  getPincodeDetailsifSame(val) {
    if (val.length == 6) {
      let body = val;
      this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then((res) => {
        this.pinDataPer = res;
        if (this.pinDataPer.StatusCode == 1) {
          this.corrCityIdPer = this.corrCityId;
          this.appCityListPer = this.pinDataPer.CityList;
          this.applicantForm.patchValue({ 'permCity': this.applicantForm.value.applCity });
          this.applicantForm.patchValue({ 'permState': this.pinDataPer.StateName });
          let proposalStateCity = { pin: body, city: this.applicantForm.value.applCity, state: this.pinDataPer.StateName };
          localStorage.setItem('PermanentStateCity', JSON.stringify(proposalStateCity));
        }
        else {
          this.applicantForm.patchValue({ 'permCity': null });
          this.applicantForm.patchValue({ 'permState': null });
          let proposalStateCity = { pin: body, city: null, state: null };
          this.presentAlert(this.pinDataPer.StatusMessage);
        }
      });
    }
  }

  corrFunction(val) {
    //console.log(ev.detail.checked);
    if (val) {
      this.isCorraddSame = true;
      this.applicantForm.patchValue({ 'permAdd1': this.applicantForm.value.applAdd1 });
      this.applicantForm.patchValue({ 'permAdd2': this.applicantForm.value.applAdd2 });
      this.applicantForm.patchValue({ 'permLandMark': this.applicantForm.value.applLandMark });
      this.applicantForm.patchValue({ 'permPinCode': this.applicantForm.value.applPinCode });
      this.getPincodeDetailsifSame(this.applicantForm.value.applPinCode);
    } else {
      this.isCorraddSame = false;
      this.applicantForm.patchValue({ 'permAdd1': '' });
      this.applicantForm.patchValue({ 'permAdd2': '' });
      this.applicantForm.patchValue({ 'permLandMark': '' });
      this.applicantForm.patchValue({ 'permPinCode': '' });
      this.applicantForm.patchValue({ 'permCity': '' });
      this.applicantForm.patchValue({ 'permState': '' });
    }
  }

  isGSTReg(val) {
    if (val) {
      this.isRegWithGST = true;
    } else {
      this.isRegWithGST = false;
    }
  }

  isExPolicy1(val) {
    if (val) {
      this.isExistPolicy1 = true;
    } else {
      this.isExistPolicy1 = false;
    }
  }

  isExPolicy2(val) {
    if (val) {
      this.isExistPolicy2 = true;
    } else {
      this.isExistPolicy2 = false;
    }
  }

  isExPolicy3(val) {
    if (val) {
      this.isExistPolicy3 = true;
    } else {
      this.isExistPolicy3 = false;
    }
  }

  isExPolicy4(val) {
    if (val) {
      this.isExistPolicy4 = true;
    } else {
      this.isExistPolicy4 = false;
    }
  }

  isExPolicy5(val) {
    if (val) {
      this.isExistPolicy5 = true;
    } else {
      this.isExistPolicy5 = false;
    }
  }

  isDiseaseVal(val) {
    if (val) {
      this.isDisease = true;
    }
    else {
      this.isDisease = false;
    }
  }

  isDiseaseVal2(val) {
    if (val) {
      this.isDisease2 = true;
    }
    else {
      this.isDisease2 = false;
    }
  }

  isDiseaseVal3(val) {
    if (val) {
      this.isDisease3 = true;
    }
    else {
      this.isDisease3 = false;
    }
  }

  isDiseaseVal4(val) {
    if (val) {
      this.isDisease4 = true;
    }
    else {
      this.isDisease4 = false;
    }
  }

  isDiseaseVal5(val) {
    if (val) {
      this.isDisease5 = true;
    }
    else {
      this.isDisease5 = false;
    }
  }

  getRelations() {
    this.cs.getWithParams('/api/healthmaster/GetRelations').then((res: any) => {
      this.ppapRealtions = res; let tempSelfData, a;
      for (let i = 0; i < this.ppapRealtions.length; i++) {
        if (this.ppapRealtions[i].Name == 'EMPLOYEE' || this.ppapRealtions[i].Name == "EMPLOYER" || this.ppapRealtions[i].Name == "INSTITUTION") {
          let tempdata = i;
          this.ppapRealtions.splice(tempdata, 1);
        }
        if (this.ppapRealtions[i].Name == 'SELF') {
          let tempSelfData = this.ppapRealtions[i];
          this.ppapRealtions.splice(this.ppapRealtions.indexOf(tempSelfData), 1);
          this.ppapRealtions.splice(0, 0, tempSelfData);
        }
      }
      this.relation = res;
    }).catch((err: any) => {
      console.log(err);
    })
  }

  getNomineeRelation() {
    let producttype;
    if (this.quoteRequestData.ProductType == 'CHI') {
      producttype = 'CHI'
    } else if (this.quoteRequestData.ProductType == 'HBOOSTER') {
      producttype = 'HBOOSTER'
    }else if( this.quoteRequestData.ProductType == 'ASP'){
      producttype = 'ASP'
    } else if (this.quoteRequestData.ProductType == "PPAP") {
      producttype = 'PPAP'
    } else { console.log('Invalid Product'); }
    this.cs.postWithParams('/api/healthmaster/GetHealthProposalRelationships?Product=' + producttype, '').then((res: any) => {
      console.log("Rel", res);
      this.nomineeData = res;
      this.nomineeRelation = this.nomineeData.NomineeAppointeeRelationship;

    }).catch((err: any) => {
      console.log(err);
    })
  }

  nomineeRelId(ev: any) {
    this.nomineeRelationId = this.nomineeRelation.find(x => x.RelationshipName == ev.target.value);
  }

  nomineePPAPRelId(ev: any) {
    this.nomineeRelationId = this.ppapRealtions.find(x => x.Name == ev.target.value);
  }

  appointeeRelId(ev: any) {
    this.appointeeRelationId = this.nomineeRelation.find(x => x.RelationshipName == ev.target.value).RelationshipID;
  }

  getTitle(ev: any) {
    this.titleId1 = this.titles.find(x => x.val == ev.target.value);
  }

  getNomTitle(ev: any) {
    this.nomineeTitle = this.titles.find(x => x.val == ev.target.value);
  }

  getAppTitle(ev: any) {
    this.appointeeTitle = this.titles.find(x => x.val == ev.target.value).id;
  }

  showPermanent() {
    this.isPermanent = !this.isPermanent;
  }

  showAppointeeDetail() {
    this.apointee = !this.apointee;
  }

  showOther() {
    this.isOther = !this.isOther;
  }

  showNomineeDetail() {
    this.isNomineeDeatils = !this.isNomineeDeatils;
  }

  getPincodeDetails(ev: any) {

    this.inputLoadingPlan = true;
    if (ev.target.value.length == 6) {
      //this.showLoading();
      let body = ev.target.value;
      this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then((res) => {
        this.pinData = res;

        if (this.pinData.StatusCode == 1) {
          this.inputLoadingPlan = false;
          this.appCityList = this.pinData.CityList;
          this.corrCityId = this.pinData.CityList[0].CityID;
          this.applicantForm.patchValue({ 'applCity': this.pinData.CityList[0].CityName });
          this.applicantForm.patchValue({ 'applState': this.pinData.StateName });
          let proposalStateCity = { pin: body, city: this.pinData.CityList[0].CityName, state: this.pinData.StateName };
          localStorage.setItem('CoresspondentStateCity', JSON.stringify(proposalStateCity));
          //  this.loading.dismiss();
        }
        else {
          this.applicantForm.patchValue({ 'applCity': '' });
          this.applicantForm.patchValue({ 'applState': null });
          let proposalStateCity = { pin: body, city: null, state: null };
          localStorage.setItem('CoresspondentStateCity', JSON.stringify(proposalStateCity));
          this.presentAlert(this.pinData.StatusMessage);
          //  this.loading.dismiss();
        }

      });
    }
  }

  getPincodeDetailsPer(ev: any) {
    this.inputLoadingPlan = true;
    if (ev.target.value.length == 6) {
      let body = ev.target.value;
      //this.showLoading();
      this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then((res) => {
        this.pinDataPer = res;
        if (this.pinDataPer.StatusCode == 1) {
          this.inputLoadingPlan = false;
          this.appCityListPer = this.pinDataPer.CityList;
          this.corrCityIdPer = this.pinDataPer.CityList[0].CityID;
          this.applicantForm.patchValue({ 'permCity': this.pinDataPer.CityList[0].CityName });
          this.applicantForm.patchValue({ 'permState': this.pinDataPer.StateName });
          let proposalStateCity = { pin: body, city: this.pinDataPer.CityList[0].CityName, state: this.pinDataPer.StateName };
          localStorage.setItem('PermanentStateCity', JSON.stringify(proposalStateCity));
          // this.loading.dismiss();
        }
        else {
          this.applicantForm.patchValue({ 'permCity': null });
          this.applicantForm.patchValue({ 'permState': null });
          let proposalStateCity = { pin: body, city: null, state: null };
          this.presentAlert(this.pinDataPer.StatusMessage);
          // this.loading.dismiss();
        }
      });
    }
  }



  getMaritalStatus() {
    this.cs.getWithParams('/api/healthmaster/getMaritalStatus').then((res: any) => {
      this.maritalStatus = res;
    });
  }

  showMaritalStatus(ev: any) {
    this.maritalStatusId = this.maritalStatus.MaritalStatusList.find((x: any) => x.MaritalStatusDesc == ev.target.value);
  }

  getAnnualIncome() {
    this.cs.postWithParams('/api/agent/GetAnnualIncome', '').then((res: any) => {
      this.annualIncome = res;
    }).catch((err: any) => {
      console.log(err);
    });
  }

  getAnnualIncId(ev: any) {
    this.annualIncomeId = this.annualIncome.Values.find((x: any) => x.Name == ev.target.value);
  }
  getInsuredRelation(ev: any) {
    this.relationId1 = this.adultRelationArray.find(x => x.RelationshipName == ev.target.value).RelationshipID;
  }

  getInsuredPPAPRelation(ev: any) {
    this.relationId1 = this.ppapRealtions.find(x => x.Name == ev.target.value).Value;
  }

  getOccupation() {
    this.cs.getWithParams('/api/healthmaster/GetOccupation').then((res: any) => {
      this.occupation = res;
      this.occupation = _.sortBy(this.occupation.OccupationLists, 'OCCUPATION');
    });
  }

  getInsuranceData() {
    this.cs.getWithParams('/api/healthmaster/GetInsuranceCompany').then((res: any) => {
      this.InsuranceData = res;
      this.InsuranceData = _.sortBy(this.InsuranceData, 'Name');
    });

  }

  getOccId(ev: any) {
    this.occupationId = this.occupation.find((x: any) => x.OCCUPATION == ev.target.value);
  }

  getConstName(ev: any) {
    if (ev.target.value == "0") { this.constName = "Constitution of Business" }
    else if (ev.target.value == "1") { this.constName = "Non Resident Entity" }
    else if (ev.target.value == "2") { this.constName = "Foreign company registered in India" }
    else if (ev.target.value == "3") { this.constName = "Foreign LLP" }
    else if (ev.target.value == "4") { this.constName = "Government Department" }
    else if (ev.target.value == "5") { this.constName = "Hindu Undivided Family" }
    else if (ev.target.value == "6") { this.constName = "LLP Partnership" }
    else if (ev.target.value == "7") { this.constName = "Local Authorities" }
    else if (ev.target.value == "8") { this.constName = "Partnership" }
    else if (ev.target.value == "9") { this.constName = "Private Limited Company" }
    else if (ev.target.value == "10") { this.constName = "Proprietorship" }
    else { this.constName = "Others" }
  }

  getGSTName(ev: any) {
    if (ev.target.value == "0") { this.gstName = "GST Registration Status" }
    else if (ev.target.value == "41") { this.gstName = "ARN Generated" }
    else if (ev.target.value == "42") { this.gstName = "Provision ID Obtained" }
    else if (ev.target.value == "43") { this.gstName = "To be commenced" }
    else if (ev.target.value == "44") { this.gstName = "Enrolled" }
    else { this.gstName = "Not applicable" }
  }

  getCustTypeName(ev: any) {
    if (ev.target.value == "0") { this.custType = "Customer Type" }
    else if (ev.target.value == "21") { this.custType = "General" }
    else if (ev.target.value == "22") { this.custType = "EOU/STP/EHTP" }
    else if (ev.target.value == "23") { this.custType = "Government" }
    else if (ev.target.value == "24") { this.custType = "Overseas" }
    else if (ev.target.value == "25") { this.custType = "Related parties" }
    else if (ev.target.value == "26") { this.custType = "SEZ" }
    else { this.custType = "Others" }
  }

  getBackData(): Promise<any> {
    return new Promise((resolve) => {
      this.requestBody = JSON.parse(localStorage.getItem('quoteRequest'));
      this.responseBody = JSON.parse(localStorage.getItem('quoteResponse'));
      resolve();
    })
  }

  calProposal(form: any) {
    console.log(form.value);
    let validForm = this.showFormValidate(form);
    if (validForm) {
      this.cs.showLoader = true;
      // this.getBackData().then(() => {

      this.getPinCodeData(form).then(() => {
        this.getPinCodeDataPerm(form).then(() => {
          this.createCust(form).then(() => {
            if (this.requestBody.ProductType == 'CHI' || this.requestBody.ProductType == 'ASP') {
              this.createCHIMembers(form).then(() => {
                this.proposalForCHI(form);
              });
            }
            else if (this.requestBody.ProductType == 'HBOOSTER') {
              this.createHBMembers(form).then(() => {
                this.proposalForHB(form);
              });
            } else {
              this.proposalForPPAP(form);
            }
          });
        });
      });
      // });
    }
  }

  saveProposal(form: any) {
    console.log(form.value);
    let validForm = this.showFormValidate(form);
    if (validForm) {
      this.cs.showLoader = true;
      this.getBackData().then(() => {
        console.log("Insured Data", this.insuredDetails);
        console.log("Response Data", this.responseBody);
        console.log("Request Data", this.requestBody);
        this.getPinCodeData(form).then(() => {
          this.getPinCodeDataPerm(form).then(() => {
            this.createCust(form).then(() => {
              if (this.requestBody.ProductType == 'CHI' || this.requestBody.ProductType == 'ASP') {
                this.createCHIMembers(form).then(() => {
                  this.saveProposalForCHI(form);
                });
              }
              else if (this.requestBody.ProductType == 'HBOOSTER') {
                this.createHBMembers(form).then(() => {
                  this.saveProposalForHB(form);
                });
              } else {
                this.saveProposalForPPAP(form);
              }
            });
          });
        });
      });
    }
  }

  getAge(ev: any) {
    let date = new Date();
    let age = date.getFullYear() - new Date(ev.value).getFullYear();
    console.log(age);
    if (age < 18) {
      this.showApointee = true;
    } else {
      this.applicantForm.patchValue({ 'appTitle': '' });
      this.applicantForm.patchValue({ 'appName': '' });
      this.applicantForm.patchValue({ 'appRelation': '' });
      this.applicantForm.patchValue({ 'appDOB': '' });
      if (this.appointeeRelationId == undefined) { this.appointeeRelationId = ""; }
      if (this.appointeeTitle == undefined) { this.appointeeTitle = ""; }
      this.showApointee = false;
    }
  }


  getPinCodeData(form: any): Promise<any> {
    return new Promise((resolve) => {
      let body = { "Pincode": form.value.applPinCode, "CityID": this.corrCityId };
      this.cs.postWithParams('/api/RTOList/GetPincodeID', body).then((res: any) => {
        console.log("Get Pin Data ", res);
        this.pinDataValue = res;
        resolve();
      }).catch((err) => {
        this.presentToast(err);
        this.loading.dismiss();
        resolve();
      });
    });
  }

  getPinCodeDataPerm(form: any): Promise<any> {
    return new Promise((resolve) => {
      let body = { "Pincode": form.value.permPinCode, "CityID": this.corrCityIdPer };
      this.cs.postWithParams('/api/RTOList/GetPincodeID', body).then((res: any) => {
        console.log("Get Pin Data ", res);
        this.pinDataPermValue = res;
        resolve();
      }).catch((err) => {
        this.presentToast(err);
        this.loading.dismiss();
        resolve();
      });
    });
  }

  createCust(form: any): Promise<any> {
    return new Promise((resolve) => {
      let occuid, annuid;
      if (form.value.panNumber == null || form.value.panNumber == undefined) {
        form.value.panNumber = '';
      }
      if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
        form.value.applAdd2 = '';
      }
      if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
        form.value.applLandMark = '';
      }
      if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
        form.value.permAdd2 = '';
      }
      if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
        form.value.permAdd2 = '';
      }
      if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
        form.value.applaltEmail = '';
      }
      if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
        form.value.applOccu = '';
        occuid = '';
      }
      else {
        occuid = this.occupationId.OCCUPATIONID;
      }
      if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
        form.value.annuIncome == '';
        annuid = '';
      }
      else {
        annuid = this.annualIncomeId.Value;
      }

      if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
        form.value.applaltMobNo = '';
      }
      let proofText, proofValue;
      if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined || form.value.aadhaarNo == '') {
        form.value.aadhaarNo = ''; proofText = ''; proofValue = ''
      }
      else {
        proofText = "Aadhaar Card"; proofValue = '2'
      }
      let dob;
      dob = new Date(new Date().setMonth(new Date().getMonth() - 3));
      // dob = moment(form.value.applDOB, 'DD-MM-YYYY').format('DD-MMM-YYYY');
      if (this.productType == "PPAP") {
        if (this.insuredPPAPForm.value.member1Rela == 'SELF') {
          dob = moment(form.value.applDOB, 'YYYY-MM-DD').format('DD-MMM-YYYY')
        }
        else {
          dob = moment(form.value.applDOB, 'DD-MM-YYYY').format('DD-MMM-YYYY');
        }
      }
      else {
        dob = moment(form.value.applDOB, 'DD-MM-YYYY').format('DD-MMM-YYYY');
      }
      let body = {
        "CustomerID": 0,
        "HasAddressChanged": true,
        "SetAsDefault": true,
        "TitleText": form.value.applTitle,
        "TitleValue": this.titleId1.id, // pass id
        "Name": form.value.applName,
        "DateOfBirth": dob,
        "MaritalStatusValue": this.maritalStatusId.MaritalStatusCode, // pass id
        "MaritalStatusText": form.value.applMarital,
        "OccupationValue": occuid,
        "OccupationText": form.value.applOccu,
        "AnnualIncomeValue": annuid,  //annual income id
        "AnnualIncomeText": form.value.annuIncome,
        "IdentityProofValue": proofValue,
        "IdentityProofText": proofText,
        "IdentityNumber": form.value.aadhaarNo,
        "PresentAddrLine1": form.value.applAdd1,
        "PresentAddrLine2": form.value.applAdd2,
        "PresentAddrLandmark": form.value.applLandMark,
        "PresentAddrCityValue": this.corrCityId, //pass id
        "PresentAddrCityText": form.value.applCity,
        "PresentAddrStateValue": this.pinData.StateId,
        "PresentAddrStateText": form.value.applState,
        "PresentAddrPincodeValue": this.pinDataValue.Details[0].Value,
        "PresentAddrPincodeText": form.value.applPinCode,
        "EmailAddress": form.value.applEmail,
        "MobileNumber": form.value.applMobNo.toString(),
        "LandlineNumber": form.value.landLine,
        "EmailAlternate": form.value.applaltEmail,
        "MobileAlternate": form.value.applaltMobNo.toString(),
        "PANNumber": form.value.panNumber,
        "isPermanentAsPresent": true,
        "PermanentAddrLine1": form.value.permAdd1,
        "PermanentAddrLine2": form.value.permAdd2,
        "PermanentAddrLandmark": form.value.permLandMark,
        "PermanentAddrCityValue": this.corrCityIdPer,
        "PermanentAddrCityText": form.value.permCity,
        "PermanentAddrStateValue": this.pinDataPer.StateId, //pass id
        "PermanentAddrStateText": form.value.permState,
        "PermanentAddrPincodeValue": this.pinDataPermValue.Details[0].Value,
        "PermanentAddrPincodeText": form.value.permPinCode,
        "isGSTINApplicable": false,
        "isUINApplicable": false,
        "GSTDetails": {
          "GSTIN_NO": form.value.GSTIN,
          "CONSTITUTION_OF_BUSINESS": form.value.constitution,
          "CONSTITUTION_OF_BUSINESS_TEXT": this.constName,
          "CUSTOMER_TYPE": form.value.customerType,
          "CUSTOMER_TYPE_TEXT": this.custType,
          "PAN_NO": form.value.panNo,
          "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
          "GST_REGISTRATION_STATUS_TEXT": this.gstName
        }
      }
      console.log("Create Cust Body", body);
      let str = JSON.stringify(body);
      console.log("Create Cust stringify", str);
      localStorage.setItem('customerRequest', str);
      if (this.requestBody.ProductType != 'CHI' || this.requestBody.ProductType != 'ASP') {
        this.cs.postWithParams('/api/customer/saveeditcustomer', str).then((res: any) => {
          this.createCustData = res;
          localStorage.setItem('customerResponse', JSON.stringify(res));
          resolve();
        }).catch((err) => {
          this.presentAlert(err.error.Message);
          this.cs.showLoader = false;
          resolve();
        });
      } else {
        this.cs.postWithParams('/api/customer/saveeditcustomer', str).then((res: any) => {
          this.createCustData = res;
          resolve();
        }).catch((err) => {
          this.presentAlert(err.error.Message);
          this.cs.showLoader = false;
          resolve();
        });
      }
      // resolve();
    });
  }

  // calculate proposal
  proposalForCHI(form: any) {
    console.log("Request", this.requestBody, this.pinDataValue, this.pinDataPermValue);
    //this.showLoading();
    let appointeeDate; let occuid, annuid;
    if (this.isRegWithGST == false) {
      form.value.GSTIN = ''; form.value.GSTIN = '';
      this.constName = ''; form.value.customerType = ''; this.custType = '';
      form.value.panNo = ''; form.value.gstRegStatus = ''; this.gstName = '';
    }
    if (this.showApointee == false) {
      form.value.appRelation = '';
      appointeeDate = ''
      form.value.appTitle = '';
      form.value.appName = '';
      this.appointeeTitle = '';
      this.appointeeRelationId = '';
    }
    else {
      appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY')
    }
    if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
      form.value.aadhaarNo = '';
    }
    if (form.value.panNumber == null || form.value.panNumber == undefined) {
      form.value.panNumber = '';
    }
    if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
      form.value.applAdd2 = '';
    }
    if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
      form.value.applLandMark = '';
    }
    if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
      form.value.applaltEmail = '';
    }

    if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
      form.value.applaltMobNo = '';
    }
    if (form.value.apsId == null || form.value.apsId == undefined) {
      form.value.apsId = '';
    }
    if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
      form.value.custRefNo = '';
    }
    if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
      form.value.applOccu = '';
      occuid = '';
    }
    else {
      occuid = this.occupationId.OCCUPATIONID;
    }
    if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
      form.value.annuIncome == '';
      annuid = '';
    }
    else {
      annuid = this.annualIncomeId.Value;
    }
    let Body;

    Body = {
      "IsCustomerPID": "true",
      "UserType": "Agent",
      "ipaddress": config.ipAddress,
      "IPGPayment": "true",
      "ProductType": this.requestBody.ProductType,
      "ProductName": this.requestBody.CHIProductName,
      "GSTApplicable": "false",
      "UINApplicable": "false",
      "PolicyID": this.responseBody.HealthDetails[0].PolicyID,    //add quate Id
      "DealID": this.responseBody.HealthDetails[0].DealId,
      "IS_IBANK_RELATIONSHIP": "NO",
      "APS_ID": form.value.apsId,
      "CUSTOMER_REF_NO": form.value.custRefNo,
      "Members": this.memberObj,
      "NomineeName": form.value.nomName,
      "NomineeTitleID": this.nomineeTitle.id,
      "NomineeRelationShipID": this.nomineeRelationId.RelationshipID, // check once
      "NomineeRelationShipText": form.value.nomRelation,
      "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
      "HUF": "Y",
      "AppointeeLastName": form.value.appName,
      "AppointeeTitleID": this.appointeeTitle,//this.titleId3.id,
      "AppointeeRelationShipID": this.appointeeRelationId,//this.appointeeRelationId.RelationshipID,
      "AppointeeRelationShip": form.value.appRelation,
      "AppointeeDOB": appointeeDate,
      "SetAsDefault": "NO",
      "AddressChange": true,
      "CustomerID": this.createCustData.CustomerID,
      "PF_CUSTOMERID": this.createCustData.PFCustomerID,
      "OccupationID": occuid,
      "OccupationDesc": form.value.applOccu,
      "IDProofID": -1,
      "IDProofValue": "",
      "AadharEKYCYesNo": "N",
      "Title": "2",
      "InsuredID": "4",
      "CustomerName": form.value.applName,
      "Address1": form.value.applAdd1,
      "Address2": form.value.applAdd2,
      "Landmark": form.value.applLandMark,
      "StateID": this.pinData.StateId,
      "CityID": this.corrCityId, //pass id
      "PincodeID": this.pinDataValue.Details[0].Value,//form.value.applPinCode,
      "PermanentAddrLine1": form.value.permAdd1,
      "PermanentAddrLine2": form.value.permAdd2,
      "PermanentAddrLandmark": form.value.permLandMark,
      "PermanentAddrStateID": this.pinDataPer.StateId, //pass id
      "PermanentAddrCityID": this.corrCityIdPer,
      "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,//form.value.permPinCode,
      "Mobile": form.value.applMobNo.toString(),
      "LandLineNo": '',
      "STD": "",
      "Email": form.value.applEmail,
      "FinancierBranch": "",
      "FinancierName": "",
      "PaymentMode": "NONE",
      "Cheque_Type": "",
      "ModeID": "",
      "CardExpiryYear": "",
      "CardExpiryMonth": "",
      "CardNumber": "",
      "CVVNumber": "",
      "CardType": "",
      "CardHolderName": "",
      "PANNumber": form.value.panNumber,
      "AadhaarNumber": form.value.aadhaarNo,
      "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
      "SubLimit": this.CHISubLimit,
      "AddOn1": this.requestBody.AddOn1,
      "AddOn3": this.requestBody.AddOn3,
      "AddOn5": this.requestBody.AddOn5,
      "AddOn6": this.requestBody.AddOn6,
      "MaritalStatus": "SINGLE",
      "AnnualIncome": annuid,
      "isHobbyOccupationVisible": "false",
      "Product24": form.value._80DTax,
    }
    let spdetails;
    if (this.isSPFlagValid) {
      spdetails =
      {
        'AlternateRMCode': 'online ',
        'AlternateRMName': 'online ',
        "CustomerReferenceNumber": this.createCustData.PFCustomerID,
        'ChannelName': 'RLG',
        'PrimaryRMCode': 'online',
        'SecondaryRMCode': '',
        'BancaField01': '',
        'BancaField02': '',
        'BBancaField03': '',
      }
      Body.SpDetails = spdetails;
    }
    // else if(this.ParentIMID == '' || this.ParentIMID == null || this.ParentIMID != undefined)
    // {
    //   spdetails=
    //   {
    //     AlternateRMCode:'' ,
    //     AlternateRMName:'' ,
    //     CustomerReferenceNumber:'' ,
    //     ChannelName:'' ,
    //     PrimaryRMCode:'' ,
    //     SecondaryRMCode:'' ,
    //     BancaField01:'' ,
    //     BancaField02:'' ,
    //     BancaField03:'' ,
    //   }

    // }

    console.log("CHI RequestBody", Body);
    let str = JSON.stringify(Body);
    console.log("Stringify", str);
    localStorage.setItem('proposalRequest', JSON.stringify(Body));
    this.cs.postWithParams('/api/Health/SaveEditProposal', str).then((res: any) => {
      console.log("CHI Response", res);
      if (res.StatusCode == '1') {
        localStorage.setItem('proposalResponse', JSON.stringify(res));
        this.cs.showLoader = false;
        this.router.navigateByUrl('health-summary');
      }
      else { this.cs.showLoader = false; this.messageAlert(res.StatusMessage); }
      //this.loading.dismiss();
    }).catch((err) => {
      this.presentToast(err.error.Message);
      this.cs.showLoader = false;
    });
  }

  saveProposalForCHI(form: any) {
    console.log("Request", this.requestBody, this.pinDataValue, this.pinDataPermValue);
    //this.showLoading();
    let appointeeDate; let occuid, annuid;
    if (this.isRegWithGST == false) {
      form.value.GSTIN = ''; form.value.GSTIN = '';
      this.constName = ''; form.value.customerType = ''; this.custType = '';
      form.value.panNo = ''; form.value.gstRegStatus = ''; this.gstName = '';
    }
    if (this.showApointee == false) {
      form.value.appRelation = '';
      appointeeDate = ''
      form.value.appTitle = '';
      form.value.appName = '';
      this.appointeeTitle = '';
      this.appointeeRelationId = '';
    }
    else {
      appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY')
    }
    if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
      form.value.aadhaarNo = '';
    }
    if (form.value.panNumber == null || form.value.panNumber == undefined) {
      form.value.panNumber = '';
    }
    if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
      form.value.applAdd2 = '';
    }
    if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
      form.value.applLandMark = '';
    }
    if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
      form.value.applaltEmail = '';
    }

    if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
      form.value.applaltMobNo = '';
    }
    if (form.value.apsId == null || form.value.apsId == undefined) {
      form.value.apsId = '';
    }
    if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
      form.value.custRefNo = '';
    }
    if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
      form.value.applOccu = '';
      occuid = '';
    }
    else {
      occuid = this.occupationId.OCCUPATIONID;
    }
    if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
      form.value.annuIncome == '';
      annuid = '';
    }
    else {
      annuid = this.annualIncomeId.Value;
    }
    let Body
    Body = {
      "IsCustomerPID": "true",
      "UserType": "Agent",
      "ipaddress": config.ipAddress,
      "IPGPayment": "true",
      "ProductType": this.requestBody.ProductType,
      "ProductName": this.requestBody.CHIProductName,
      "GSTApplicable": "false",
      "UINApplicable": "false",
      "PolicyID": this.responseBody.HealthDetails[0].PolicyID,    //add quate Id
      "DealID": this.responseBody.HealthDetails[0].DealId,
      "IS_IBANK_RELATIONSHIP": "NO",
      "APS_ID": form.value.apsId,
      "CUSTOMER_REF_NO": form.value.custRefNo,
      'Members': this.memberObj,
      "NomineeName": form.value.nomName,
      "NomineeTitleID": this.nomineeTitle.id,
      "NomineeRelationShipID": this.nomineeRelationId.RelationshipID, // check once
      "NomineeRelationShipText": form.value.nomRelation,
      "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
      "HUF": "Y",
      "AppointeeLastName": form.value.appName,
      "AppointeeTitleID": this.appointeeTitle,
      "AppointeeRelationShipID": this.appointeeRelationId,
      "AppointeeRelationShip": form.value.appRelation,
      "AppointeeDOB": appointeeDate,
      "SetAsDefault": "NO",
      "AddressChange": true,
      "CustomerID": this.createCustData.CustomerID,
      "PF_CUSTOMERID": this.createCustData.PFCustomerID,
      "OccupationID": occuid,
      "OccupationDesc": form.value.applOccu,
      "IDProofID": -1,
      "IDProofValue": "",
      "AadharEKYCYesNo": "N",
      "Title": this.titleId1.id,
      "InsuredID": "4",
      "CustomerName": form.value.applName,
      "Address1": form.value.applAdd1,
      "Address2": form.value.applAdd2,
      "Landmark": form.value.applLandMark,
      "StateID": this.pinData.StateId,
      "CityID": this.pinData.CityList[0].CityID, //pass id
      "PincodeID": this.pinDataValue.Details[0].Value,//form.value.applPinCode,
      "PermanentAddrLine1": form.value.permAdd1,
      "PermanentAddrLine2": form.value.permAdd2,
      "PermanentAddrLandmark": form.value.permLandMark,
      "PermanentAddrStateID": this.pinDataPer.StateId, //pass id
      "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
      "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,//form.value.permPinCode,
      "Mobile": form.value.applMobNo.toString(),
      "LandLineNo": '',
      "STD": "",
      "Email": form.value.applEmail,
      "FinancierBranch": "",
      "FinancierName": "",
      "PaymentMode": "NONE",
      "Cheque_Type": "",
      "ModeID": "00001",
      "CardExpiryYear": "2014",
      "CardExpiryMonth": "07",
      "CardNumber": "5454454545544554",
      "CVVNumber": "123",
      "CardType": "MC",
      "CardHolderName": "xfgshg",
      "PANNumber": form.value.panNumber,
      "AadhaarNumber": form.value.aadhaarNo,
      "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
      "SubLimit": this.CHISubLimit,
      "AddOn1": this.requestBody.AddOn1,
      "AddOn3": this.requestBody.AddOn3,
      "AddOn5": this.requestBody.AddOn5,
      "AddOn6": this.requestBody.AddOn6,
      "MaritalStatus": "SINGLE",
      "AnnualIncome": annuid,
      "isHobbyOccupationVisible": "false",
      "SaveType": "PROPOSAL",
      "Product24": form.value._80DTax,
    }
    let spdetails;
    if (this.ParentIMID != '' && this.ParentIMID != null && this.ParentIMID != undefined) {
      spdetails =
      {
        'AlternateRMCode': 'online ',
        'AlternateRMName': 'online ',
        "CustomerReferenceNumber": this.createCustData.PFCustomerID,
        'ChannelName': 'RLG',
        'PrimaryRMCode': 'online',
        'SecondaryRMCode': '',
        'BancaField01': '',
        'BancaField02': '',
        'BBancaField03': '',
      }
      Body.SpDetails = spdetails;
    }
    // else if(this.ParentIMID == '' || this.ParentIMID == null || this.ParentIMID != undefined)
    // {
    //   spdetails=
    //   {
    //     AlternateRMCode:'' ,
    //     AlternateRMName:'' ,
    //     CustomerReferenceNumber:'' ,
    //     ChannelName:'' ,
    //     PrimaryRMCode:'' ,
    //     SecondaryRMCode:'' ,
    //     BancaField01:'' ,
    //     BancaField02:'' ,
    //     BancaField03:'' ,
    //   }

    // }

    console.log("CHI RequestBody", Body);
    let str = JSON.stringify(Body);
    console.log("Stringify", str);
    localStorage.setItem('reqFromProp', JSON.stringify(Body));
    this.cs.postWithParams('/api/Health/SaveEditProposal', str).then((res: any) => {
      console.log("CHI Response", res);
      if (res.StatusCode == '1') {
        localStorage.setItem('resFromProp', JSON.stringify(res));
        this.cs.showLoader = false;
        this.messageAlert('Your Proposal hasbeen saved with Policy Id: ' + res.SavePolicy[0].PolicyID + ' and will be expired in 7 days');
        this.router.navigateByUrl('health');
      }
      else { this.cs.showLoader = false; this.messageAlert(res.StatusMessage); }
    }).catch((err) => {
      this.presentToast(err.error.Message);
      this.cs.showLoader = false;
    });
  }

  proposalForPPAP(form: any) {
    // this.showLoading();
    let appointeeDate; let occuid, annuid;
    let isotherPolicy, otherPolicyNo, claimsMade, PolicyValidFrom, PolicyValidTo, KidAdultType, InsuranceCompany, Gender;
    let insuredData = this.insuredPPAPForm.value;
    let quoteRequestData = JSON.parse(localStorage.getItem('quoteRequest'));
    if (this.showOtherPolicyDetails == false) {
      isotherPolicy = 'false'; otherPolicyNo = ''; PolicyValidFrom = -1; PolicyValidTo = -1;
      InsuranceCompany = ''; claimsMade = false;
    }
    else {
      isotherPolicy = 'true';
      otherPolicyNo = insuredData.insurancePolicy;
      PolicyValidFrom = insuredData.policyValidFrom;
      PolicyValidTo = insuredData.policyValidTill;
      InsuranceCompany = insuredData.insuranceComapny;
      claimsMade = this.showClaimsDetails;
    }
    if (insuredData.member1Cat == 'Adult') { KidAdultType = '1' } else { KidAdultType = '2'; }
    if (insuredData.member1Title == 'Mr.') { Gender = 'Male' } else { Gender = 'Female' };
    if (insuredData.insuredpanno == null || insuredData.insuredpanno == undefined || insuredData.insuredpanno == '') {
      insuredData.insuredpanno = '';
    }
    //  let proposalDate=moment().add(2, 'days').format('DD-MMM-YYYY');
    let proposalDate = moment(insuredData.riskStartDate).format('DD-MMM-YYYY');
    let insuredRelationName = insuredData.member1Rela;
    // let insuredRelationshipID = this.adultRelationArray.find((x: any) => x.Name == insuredRelationName);
    if (this.isRegWithGST == false) {
      form.value.GSTIN = ''; form.value.GSTIN = '';
      this.constName = ''; form.value.customerType = ''; this.custType = '';
      form.value.panNo = ''; form.value.gstRegStatus = ''; this.gstName = '';
    }
    if (this.showApointee == false) {
      form.value.appRelation = '';
      appointeeDate = ''
      form.value.appTitle = '';
      form.value.appName = '';
      this.appointeeTitle = '';
      this.appointeeRelationId = '';
    }
    else {
      appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY')
    }
    if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
      form.value.aadhaarNo = '';
    }
    if (form.value.panNumber == null || form.value.panNumber == undefined) {
      form.value.panNumber = '';
    }
    if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
      form.value.applAdd2 = '';
    }
    if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
      form.value.applLandMark = '';
    }
    if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
      form.value.applaltEmail = '';
    }

    if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
      form.value.applaltMobNo = '';
    }
    if (form.value.apsId == null || form.value.apsId == undefined) {
      form.value.apsId = '';
    }
    if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
      form.value.custRefNo = '';
    }
    if (form.value.sourcingCode == null || form.value.sourcingCode == undefined) {
      form.value.sourcingCode = '';
    }
    if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
      form.value.applOccu = '';
      occuid = '';
    }
    else {
      occuid = this.occupationId.OCCUPATIONID;
    }
    if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
      form.value.annuIncome == '';
      annuid = '';
    }
    else {
      annuid = this.annualIncomeId.Value;
    }
    let Body
    Body = {
      "IsCustomerPID": "true",
      "UserType": "Agent",
      "ipaddress": config.ipAddress,
      "IPGPayment": "true",
      "ProductType": this.requestBody.ProductType,
      "SubProduct": this.requestBody.SubProduct,
      "ProposalDate": proposalDate,
      "GSTApplicable": "false",
      "UINApplicable": "false",
      "PolicyID": this.responseBody.HealthDetails[0].PolicyID,
      "DealID": this.responseBody.HealthDetails[0].DealId,
      "IS_IBANK_RELATIONSHIP": "NO",
      "APS_ID": form.value.apsId,
      "CUSTOMER_REF_NO": form.value.custRefNo,
      "CustomerID": this.createCustData.CustomerID,
      "PF_CUSTOMERID": this.createCustData.PFCustomerID,
      "SumInsured": localStorage.getItem('SumInsured'),
      "CustomerGSTDetails": {
        "GSTIN_NO": form.value.GSTIN,
        "CONSTITUTION_OF_BUSINESS": form.value.constitution,
        "CONSTITUTION_OF_BUSINESS_TEXT": this.constName,
        "CUSTOMER_TYPE": form.value.customerType,
        "CUSTOMER_TYPE_TEXT": this.custType,
        "PAN_NO": form.value.panNo,
        "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
        "GST_REGISTRATION_STATUS_TEXT": this.gstName
      },
      "Members": [
        {
          "Name": insuredData.member1Name,
          "Gender": Gender,
          "RelationshipID": this.relationId1,
          "RelationshipName": insuredRelationName,
          "PanNumber": insuredData.insuredpanno,
          "DOB": quoteRequestData.DOB,
          "IsOtherPolicy": isotherPolicy,
          "InsuranceCompany": InsuranceCompany,
          "OtherPolicyNo": otherPolicyNo,
          "PolicyValidFrom": PolicyValidFrom,
          "PolicyValidTill": PolicyValidTo,
          "ClaimsMade": claimsMade,
          "KidAdultType": KidAdultType,
          "FirstName": "",
          "MiddleName": "",
          "LastName": "",
          "Height": "",
          "Weight": ""
        }
      ],
      "NomineeName": form.value.nomName,
      "NomineeTitleID": this.nomineeTitle.id,
      "NomineeRelationShipID": this.nomineeRelationId.Value, // check once
      "NomineeRelationShipText": form.value.nomRelation,
      "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
      "HUF": "Y",
      "AppointeeLastName": form.value.appName,
      "AppointeeTitleID": this.appointeeTitle,
      "AppointeeRelationShipID": this.appointeeRelationId,//this.appointeeRelationId.RelationshipID,
      "AppointeeRelationShip": form.value.appRelation,
      "AppointeeDOB": appointeeDate,
      "SetAsDefault": "NO",
      "AddressChange": true,
      "OccupationID": occuid,
      "OccupationDesc": form.value.applOccu,
      "IDProofID": -1,
      "IDProofValue": "",
      "AadharEKYCYesNo": "N",
      "Title": this.titleId1.id,
      "InsuredID": "4",
      "CustomerName": form.value.applName,
      "Address1": form.value.applAdd1,
      "Address2": form.value.applAdd2,
      "Landmark": form.value.applLandMark,
      "StateID": this.pinData.StateId,
      "CityID": this.pinData.CityList[0].CityID, //pass id
      "PincodeID": this.pinDataValue.Details[0].Value,//form.value.applPinCode,
      "PermanentAddrLine1": form.value.permAdd1,
      "PermanentAddrLine2": form.value.permAdd2,
      "PermanentAddrLandmark": form.value.permLandMark,
      "PermanentAddrStateID": this.pinDataPer.StateId, //pass id
      "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
      "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,//form.value.permPinCode,
      "Mobile": form.value.applMobNo.toString(),
      "LandLineNo": '',
      "STD": "",
      "Email": form.value.applEmail,
      "FinancierBranch": "",
      "FinancierName": "",
      "PaymentMode": "NONE",
      "CardType": "",
      "ModeID": "0",
      "CardHolderName": "",
      "CardNumber": "",
      "CVVNumber": "",
      "CardExpiryMonth": "",
      "CardExpiryYear": "",
      "PANNumber": form.value.panNumber,
      "AadhaarNumber": form.value.aadhaarNo,
      "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
      "MaritalStatus": this.maritalStatusId.MaritalStatusCode, // pass id
      "AnnualIncome": annuid,  //annual income id
      "HobbyID": "",
      "AlternateEMail": form.value.applaltEmail,
      "AlternateMobile": form.value.applaltMobNo.toString(),
      "SourcingCode": form.value.sourcingCode,
      "SaveType": "",
      "Product24": form.value._80DTax,
    }
    let spdetails;
    if (this.isSPFlagValid) {
      spdetails =
      {
        'AlternateRMCode': 'online ',
        'AlternateRMName': 'online ',
        "CustomerReferenceNumber": this.createCustData.PFCustomerID,
        'ChannelName': 'RLG',
        'PrimaryRMCode': 'online',
        'SecondaryRMCode': '',
        'BancaField01': '',
        'BancaField02': '',
        'BBancaField03': '',
      }
      Body.SpDetails = spdetails;
    }
    // else if(this.ParentIMID == '' || this.ParentIMID == null || this.ParentIMID != undefined)
    // {
    //   spdetails=
    //   {
    //     AlternateRMCode:'' ,
    //     AlternateRMName:'' ,
    //     CustomerReferenceNumber:'' ,
    //     ChannelName:'' ,
    //     PrimaryRMCode:'' ,
    //     SecondaryRMCode:'' ,
    //     BancaField01:'' ,
    //     BancaField02:'' ,
    //     BancaField03:'' ,
    //   }

    // }

    this.cs.showLoader = true;
    let stringifyReq = JSON.stringify(Body);
    localStorage.setItem('proposalRequest', JSON.stringify(Body));
    this.cs.postWithParams('/api/PPAP/SaveEditProposal', stringifyReq).then((res: any) => {
      console.log("PPAP Response", res);
      if (res.StatusCode == '1') {
        localStorage.setItem('proposalResponse', JSON.stringify(res));
        this.router.navigateByUrl('health-summary');
        this.cs.showLoader = false;
      }
      else { this.cs.showLoader = false; this.messageAlert(res.StatusMessage); }
      //this.loading.dismiss();

    }).catch((err: any) => {
      this.presentToast(err.error.Message);
      this.cs.showLoader = false;
    })

  }

  saveProposalForPPAP(form: any) {
    let appointeeDate; let occuid, annuid;
    let isotherPolicy, otherPolicyNo, claimsMade, PolicyValidFrom, PolicyValidTo, KidAdultType, InsuranceCompany, Gender;
    let insuredData = this.insuredPPAPForm.value;
    let quoteRequestData = JSON.parse(localStorage.getItem('quoteRequest'));
    if (this.showOtherPolicyDetails == false) {
      isotherPolicy = 'false'; otherPolicyNo = ''; PolicyValidFrom = -1; PolicyValidTo = -1;
      InsuranceCompany = ''; claimsMade = 'false';
    }
    else {
      isotherPolicy = 'true';
      otherPolicyNo = insuredData.insurancePolicy;
      PolicyValidFrom = insuredData.policyValidFrom;
      PolicyValidTo = insuredData.policyValidTill;
      InsuranceCompany = insuredData.insuranceComapny;
      claimsMade = this.showClaimsDetails;
    }
    if (insuredData.member1Cat == 'Adult') { KidAdultType = '1' } else { KidAdultType = '2'; }
    if (insuredData.member1Title == 'Mr.') { Gender = 'Male' } else { Gender = 'Female' };
    //  let proposalDate=moment().add(2, 'days').format('DD-MMM-YYYY');
    let proposalDate = moment(insuredData.riskStartDate).format('DD-MMM-YYYY');
    let insuredRelationName = insuredData.member1Rela;
    // let insuredRelationshipID = this.adultRelationArray.find((x: any) => x.Name == insuredRelationName);
    if (this.isRegWithGST == false) {
      form.value.GSTIN = ''; form.value.GSTIN = '';
      this.constName = ''; form.value.customerType = ''; this.custType = '';
      form.value.panNo = ''; form.value.gstRegStatus = ''; this.gstName = '';
    }
    if (this.showApointee == false) {
      form.value.appRelation = '';
      appointeeDate = ''
      form.value.appTitle = '';
      form.value.appName = '';
      this.appointeeTitle = '';
      this.appointeeRelationId = '';
    }
    else {
      appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY')
    }
    if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
      form.value.aadhaarNo = '';
    }
    if (form.value.panNumber == null || form.value.panNumber == undefined) {
      form.value.panNumber = '';
    }
    if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
      form.value.applAdd2 = '';
    }
    if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
      form.value.applLandMark = '';
    }
    if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
      form.value.applaltEmail = '';
    }

    if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
      form.value.applaltMobNo = '';
    }
    if (form.value.apsId == null || form.value.apsId == undefined) {
      form.value.apsId = '';
    }
    if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
      form.value.custRefNo = '';
    }
    if (form.value.sourcingCode == null || form.value.sourcingCode == undefined) {
      form.value.sourcingCode = '';
    }
    if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
      form.value.applOccu = '';
      occuid = '';
    }
    else {
      occuid = this.occupationId.OCCUPATIONID;
    }
    if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
      form.value.annuIncome == '';
      annuid = '';
    }
    else {
      annuid = this.annualIncomeId.Value;
    }
    let Body
    Body = {
      "IsCustomerPID": "true",
      "UserType": "Agent",
      "ipaddress": config.ipAddress,
      "IPGPayment": "true",
      "ProductType": this.requestBody.ProductType,
      "SubProduct": this.requestBody.SubProduct,
      "ProposalDate": proposalDate,
      "GSTApplicable": "false",
      "UINApplicable": "false",
      "PolicyID": this.responseBody.HealthDetails[0].PolicyID,
      "DealID": this.responseBody.HealthDetails[0].DealId,
      "IS_IBANK_RELATIONSHIP": "NO",
      "APS_ID": form.value.apsId,
      "CUSTOMER_REF_NO": form.value.custRefNo,
      "CustomerID": this.createCustData.CustomerID,
      "PF_CUSTOMERID": this.createCustData.PFCustomerID,
      "SumInsured": localStorage.getItem('SumInsured'),
      "Product24": form.value._80DTax,
      "CustomerGSTDetails": {
        "GSTIN_NO": form.value.GSTIN,
        "CONSTITUTION_OF_BUSINESS": form.value.constitution,
        "CONSTITUTION_OF_BUSINESS_TEXT": this.constName,
        "CUSTOMER_TYPE": form.value.customerType,
        "CUSTOMER_TYPE_TEXT": this.custType,
        "PAN_NO": form.value.panNo,
        "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
        "GST_REGISTRATION_STATUS_TEXT": this.gstName
      },
      "Members": [
        {
          "Name": insuredData.member1Name,
          "Gender": Gender,
          "RelationshipID": this.relationId1,
          "RelationshipName": insuredRelationName,
          "PanNumber": insuredData.insuredpanno,
          "DOB": quoteRequestData.DOB,
          "IsOtherPolicy": isotherPolicy,
          "InsuranceCompany": InsuranceCompany,
          "OtherPolicyNo": otherPolicyNo,
          "PolicyValidFrom": PolicyValidFrom,
          "PolicyValidTill": PolicyValidTo,
          "ClaimsMade": claimsMade,
          "KidAdultType": KidAdultType,
          "FirstName": "",
          "MiddleName": "",
          "LastName": "",
          "Height": "",
          "Weight": ""
        }
      ],
      "NomineeName": form.value.nomName,
      "NomineeTitleID": this.nomineeTitle.id,
      "NomineeRelationShipID": this.nomineeRelationId.Value, // check once
      "NomineeRelationShipText": form.value.nomRelation,
      "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
      "HUF": "Y",
      "AppointeeLastName": form.value.appName,
      "AppointeeTitleID": this.appointeeTitle,
      "AppointeeRelationShipID": this.appointeeRelationId,//this.appointeeRelationId.RelationshipID,
      "AppointeeRelationShip": form.value.appRelation,
      "AppointeeDOB": appointeeDate,
      "SetAsDefault": "NO",
      "AddressChange": true,
      "OccupationID": occuid,
      "OccupationDesc": form.value.applOccu,
      "IDProofID": -1,
      "IDProofValue": "",
      "AadharEKYCYesNo": "N",
      "Title": this.titleId1.id,
      "InsuredID": "4",
      "CustomerName": form.value.applName,
      "Address1": form.value.applAdd1,
      "Address2": form.value.applAdd2,
      "Landmark": form.value.applLandMark,
      "StateID": this.pinData.StateId,
      "CityID": this.pinData.CityList[0].CityID, //pass id
      "PincodeID": this.pinDataValue.Details[0].Value,//form.value.applPinCode,
      "PermanentAddrLine1": form.value.permAdd1,
      "PermanentAddrLine2": form.value.permAdd2,
      "PermanentAddrLandmark": form.value.permLandMark,
      "PermanentAddrStateID": this.pinDataPer.StateId, //pass id
      "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
      "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,//form.value.permPinCode,
      "Mobile": form.value.applMobNo.toString(),
      "LandLineNo": '',
      "STD": "",
      "Email": form.value.applEmail,
      "FinancierBranch": "",
      "FinancierName": "",
      "PaymentMode": "NONE",
      "CardType": "",
      "ModeID": "0",
      "CardHolderName": "",
      "CardNumber": "",
      "CVVNumber": "",
      "CardExpiryMonth": "",
      "CardExpiryYear": "",
      "PANNumber": form.value.panNumber,
      "AadhaarNumber": form.value.aadhaarNo,
      "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
      "MaritalStatus": this.maritalStatusId.MaritalStatusCode, // pass id
      "AnnualIncome": annuid,  //annual income id
      "HobbyID": "",
      "AlternateEMail": form.value.applaltEmail,
      "AlternateMobile": form.value.applaltMobNo.toString(),
      "SourcingCode": form.value.sourcingCode,
      "SaveType": "PROPOSAL"
    }
    let spdetails;
    if (this.ParentIMID != '' && this.ParentIMID != null && this.ParentIMID != undefined) {
      spdetails =
      {
        'AlternateRMCode': 'online ',
        'AlternateRMName': 'online ',
        "CustomerReferenceNumber": this.createCustData.PFCustomerID,
        'ChannelName': 'RLG',
        'PrimaryRMCode': 'online',
        'SecondaryRMCode': '',
        'BancaField01': '',
        'BancaField02': '',
        'BBancaField03': '',
      }
      Body.SpDetails = spdetails;
    }
    // else if(this.ParentIMID == '' || this.ParentIMID == null || this.ParentIMID != undefined)
    // {
    //   spdetails=
    //   {
    //     AlternateRMCode:'' ,
    //     AlternateRMName:'' ,
    //     CustomerReferenceNumber:'' ,
    //     ChannelName:'' ,
    //     PrimaryRMCode:'' ,
    //     SecondaryRMCode:'' ,
    //     BancaField01:'' ,
    //     BancaField02:'' ,
    //     BancaField03:'' ,
    //   }

    // }
    // Body.SpDetails=spdetails;

    let stringifyReq = JSON.stringify(Body);
    localStorage.setItem('reqFromProp', JSON.stringify(Body));
    this.cs.postWithParams('/api/PPAP/SaveEditProposal', stringifyReq).then((res: any) => {
      console.log("PPAP Response", res);
      if (res.StatusCode == '1') {
        localStorage.setItem('resFromProp', JSON.stringify(res));
        this.cs.showLoader = false;
        this.messageAlert('Your Proposal has been saved with Policy Id: ' + res.SavePolicy[0].PolicyID + ' and will be expired in 7 days');
        this.router.navigateByUrl('health');
      }
      else { this.cs.showLoader = false; this.messageAlert(res.StatusMessage); }
      //this.loading.dismiss();

    }).catch((err: any) => {
      this.presentToast(err.error.Message);
      this.cs.showLoader = false;
    })
  }

  proposalForHB(form: any) {
    // this.showLoading();
    let appointeeDate; let occuid, annuid;
    if (this.isRegWithGST == false) {
      form.value.GSTIN = ''; form.value.GSTIN = '';
      this.constName = ''; form.value.customerType = ''; this.custType = '';
      form.value.panNo = ''; form.value.gstRegStatus = ''; this.gstName = '';
    }

    if (this.isExistPolicy1 == false) {
      this.insuredForm.value.TypeofPolicy1 = ''; this.insuredForm.value.PolicyDuration1 = '';
      this.insuredForm.value.InsuranceCompany1 = ''; this.insuredForm.value.SumInsured1 = '';
    }

    if (this.isExistPolicy2 == false) {
      this.insuredForm.value.TypeofPolicy2 = ''; this.insuredForm.value.PolicyDuration2 = '';
      this.insuredForm.value.InsuranceCompany2 = ''; this.insuredForm.value.SumInsured2 = '';
    }

    if (this.isExistPolicy3 == false) {
      this.insuredForm.value.TypeofPolicy3 = ''; this.insuredForm.value.PolicyDuration3 = '';
      this.insuredForm.value.InsuranceCompany3 = ''; this.insuredForm.value.SumInsured3 = '';
    }

    if (this.isExistPolicy4 == false) {
      this.insuredForm.value.TypeofPolicy4 = ''; this.insuredForm.value.PolicyDuration4 = '';
      this.insuredForm.value.InsuranceCompany4 = ''; this.insuredForm.value.SumInsured4 = '';
    }

    if (this.isExistPolicy5 == false) {
      this.insuredForm.value.TypeofPolicy5 = ''; this.insuredForm.value.PolicyDuration5 = '';
      this.insuredForm.value.InsuranceCompany5 = ''; this.insuredForm.value.SumInsured5 = '';
    }

    if (this.showApointee == false) {
      form.value.appRelation = '';
      appointeeDate = ''
      form.value.appTitle = '';
      form.value.appName = '';
      this.appointeeTitle = '';
      this.appointeeRelationId = '';
    }
    else {
      appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY')
    }
    if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
      form.value.aadhaarNo = '';
    }
    if (form.value.panNumber == null || form.value.panNumber == undefined) {
      form.value.panNumber = '';
    }
    if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
      form.value.applAdd2 = '';
    }
    if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
      form.value.applLandMark = '';
    }
    if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
      form.value.applaltEmail = '';
    }

    if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
      form.value.applaltMobNo = '';
    }
    if (form.value.apsId == null || form.value.apsId == undefined) {
      form.value.apsId = '';
    }
    if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
      form.value.custRefNo = '';
    }
    if (form.value.sourcingCode == null || form.value.sourcingCode == undefined) {
      form.value.sourcingCode = '';
    }
    if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
      form.value.applOccu = '';
      occuid = '';
    }
    else {
      occuid = this.occupationId.OCCUPATIONID;
    }
    if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
      form.value.annuIncome == '';
      annuid = '';
    }
    else {
      annuid = this.annualIncomeId.Value;
    }

    let userDate = this.quoteRequestData.Adult1Age;
    let requestBodyHB
    requestBodyHB = {
      "Product": "NA",
      "IsCustomerPID": "true",
      "UserType": "Agent",
      "ipaddress": config.ipAddress,
      "IPGPayment": "true",
      "ProductType": this.requestBody.ProductType,
      "ProductName": this.requestBody.ProductName,
      "GSTApplicable": "false",
      "UINApplicable": "false",
      "CustomerGSTDetails": {
        "GSTIN_NO": form.value.GSTIN,
        "CONSTITUTION_OF_BUSINESS": form.value.constitution,
        "CONSTITUTION_OF_BUSINESS_TEXT": this.constName,
        "CUSTOMER_TYPE": form.value.customerType,
        "CUSTOMER_TYPE_TEXT": this.custType,
        "PAN_NO": form.value.panNo,
        "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
        "GST_REGISTRATION_STATUS_TEXT": this.gstName
      },
      "PolicyID": this.responseBody.HealthDetails[0].PolicyID,    //add quate Id
      "DealID": this.responseBody.HealthDetails[0].DealId,
      "IS_IBANK_RELATIONSHIP": "NO",
      "APS_ID": [form.value.apsId],
      'Members': this.memberObj,
      "NomineeName": form.value.nomName,
      "NomineeTitleID": this.nomineeTitle.id,
      "NomineeRelationShipID": this.nomineeRelationId.RelationshipID, // check once
      "NomineeRelationShipText": form.value.nomRelation,
      "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
      "HUF": "Y",
      "AppointeeLastName": form.value.appName,
      "AppointeeTitleID": this.appointeeTitle,
      "AppointeeRelationShipID": this.appointeeRelationId,//this.appointeeRelationId.RelationshipID,
      "AppointeeRelationShip": form.value.appRelation,
      "AppointeeDOB": appointeeDate,
      "SetAsDefault": "NO",
      "AddressChange": "YES",
      "CustomerID": this.createCustData.CustomerID,
      "OccupationID": occuid,
      "OccupationDesc": form.value.applOccu,
      "IDProofID": "2",
      "IDProofValue": "978456320164",
      "AadharEKYCYesNo": "N",
      "Title": this.titleId1.id, // pass id
      "InsuredID": "4",
      "CustomerName": form.value.applName,
      "Address1": form.value.applAdd1,
      "Address2": form.value.applAdd2,
      "Landmark": form.value.applLandMark,
      "StateID": this.pinData.StateId,
      "CityID": this.pinData.CityList[0].CityID, //pass id
      "PincodeID": this.pinDataValue.Details[0].Value,//form.value.applPinCode,
      "PermanentAddrLine1": form.value.permAdd1,
      "PermanentAddrLine2": form.value.permAdd2,
      "PermanentAddrLandmark": form.value.permLandMark,
      "PermanentAddrStateID": this.pinDataPer.StateId, //pass id
      "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
      "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,//form.value.permPinCode,
      "Mobile": form.value.applMobNo.toString(),
      "LandLineNo": '',
      "STD": "",
      "Email": form.value.applEmail,
      "FinancierBranch": "",
      "FinancierName": "",
      "PaymentMode": "NONE",
      "Cheque_Type": "",
      "ModeID": "0001",
      "CardExpiryYear": "",
      "CardExpiryMonth": "",
      "CardNumber": "",
      "CVVNumber": "",
      "CardType": "",
      "CardHolderName": "",
      "PANNumber": form.value.panNumber,
      "AadhaarNumber": form.value.aadhaarNo,
      "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
      "SubLimit": "",
      "AddOn1": this.requestBody.AddOn1,
      "AddOn3": this.requestBody.AddOn3,
      "AddOn5": "false",
      "AddOn6": "false",
      "MaritalStatus": this.maritalStatusId.MaritalStatusCode, // pass id
      "AnnualIncome": annuid,  //annual income id
      "PF_CUSTOMERID": this.createCustData.PFCustomerID,
      "Product24": form.value._80DTax,
    }
    let spdetails;
    if (this.isSPFlagValid) {
      spdetails =
      {
        'AlternateRMCode': 'online ',
        'AlternateRMName': 'online ',
        "CustomerReferenceNumber": this.createCustData.PFCustomerID,
        'ChannelName': 'RLG',
        'PrimaryRMCode': 'online',
        'SecondaryRMCode': '',
        'BancaField01': '',
        'BancaField02': '',
        'BBancaField03': '',
      }
      requestBodyHB.SpDetails = spdetails;
    }
    // else if(this.ParentIMID == '' || this.ParentIMID == null || this.ParentIMID != undefined)
    // {
    //   spdetails=
    //   {
    //     AlternateRMCode:'' ,
    //     AlternateRMName:'' ,
    //     CustomerReferenceNumber:'' ,
    //     ChannelName:'' ,
    //     PrimaryRMCode:'' ,
    //     SecondaryRMCode:'' ,
    //     BancaField01:'' ,
    //     BancaField02:'' ,
    //     BancaField03:'' ,
    //   }

    // }
    // requestBodyHB.SpDetails=spdetails;
    console.log("Request for save Proposal", requestBodyHB);
    console.log("Stringify Request", JSON.stringify(requestBodyHB));
    localStorage.setItem('proposalRequest', JSON.stringify(requestBodyHB));
    let stringifyReq = JSON.stringify(requestBodyHB);
    this.cs.postWithParams('/api/HBooster/SaveEditProposal', stringifyReq).then((res: any) => {
      console.log("HB Response", res);
      if (res.StatusCode == '1') {
        localStorage.setItem('proposalResponse', JSON.stringify(res));
        this.cs.showLoader = false;
        this.router.navigateByUrl('health-summary');
      }
      else { this.cs.showLoader = false; this.messageAlert(res.StatusMessage); }
    }).catch((err: any) => {
      this.presentToast(err.error.Message);
      this.cs.showLoader = false;
    })
  }

  saveProposalForHB(form: any) {
    let appointeeDate; let occuid, annuid;
    if (this.isRegWithGST == false) {
      form.value.GSTIN = ''; form.value.GSTIN = '';
      this.constName = ''; form.value.customerType = ''; this.custType = '';
      form.value.panNo = ''; form.value.gstRegStatus = ''; this.gstName = '';
    }

    if (this.isExistPolicy1 == false) {
      this.insuredForm.value.TypeofPolicy1 = ''; this.insuredForm.value.PolicyDuration1 = '';
      this.insuredForm.value.InsuranceCompany1 = ''; this.insuredForm.value.SumInsured1 = '';
    }

    if (this.isExistPolicy2 == false) {
      this.insuredForm.value.TypeofPolicy2 = ''; this.insuredForm.value.PolicyDuration2 = '';
      this.insuredForm.value.InsuranceCompany2 = ''; this.insuredForm.value.SumInsured2 = '';
    }

    if (this.isExistPolicy3 == false) {
      this.insuredForm.value.TypeofPolicy3 = ''; this.insuredForm.value.PolicyDuration3 = '';
      this.insuredForm.value.InsuranceCompany3 = ''; this.insuredForm.value.SumInsured3 = '';
    }

    if (this.isExistPolicy4 == false) {
      this.insuredForm.value.TypeofPolicy4 = ''; this.insuredForm.value.PolicyDuration4 = '';
      this.insuredForm.value.InsuranceCompany4 = ''; this.insuredForm.value.SumInsured4 = '';
    }

    if (this.isExistPolicy5 == false) {
      this.insuredForm.value.TypeofPolicy5 = ''; this.insuredForm.value.PolicyDuration5 = '';
      this.insuredForm.value.InsuranceCompany5 = ''; this.insuredForm.value.SumInsured5 = '';
    }

    if (this.showApointee == false) {
      form.value.appRelation = '';
      appointeeDate = ''
      form.value.appTitle = '';
      form.value.appName = '';
      this.appointeeTitle = '',
        this.appointeeRelationId = '';
    }
    else {
      appointeeDate = moment(form.value.appDOB).format('DD MMM YYYY')
    }
    if (form.value.aadhaarNo == null || form.value.aadhaarNo == undefined) {
      form.value.aadhaarNo = '';
    }
    if (form.value.panNumber == null || form.value.panNumber == undefined) {
      form.value.panNumber = '';
    }
    if (form.value.applAdd2 == null || form.value.applAdd2 == undefined) {
      form.value.applAdd2 = '';
    }
    if (form.value.applLandMark == null || form.value.applLandMark == undefined) {
      form.value.applLandMark = '';
    }
    if (form.value.permAdd2 == null || form.value.permAdd2 == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.permLandMark == null || form.value.permLandMark == undefined) {
      form.value.permAdd2 = '';
    }
    if (form.value.applaltEmail == null || form.value.applaltEmail == undefined) {
      form.value.applaltEmail = '';
    }

    if (form.value.applaltMobNo == null || form.value.applaltMobNo == undefined) {
      form.value.applaltMobNo = '';
    }
    if (form.value.apsId == null || form.value.apsId == undefined) {
      form.value.apsId = '';
    }
    if (form.value.custRefNo == null || form.value.custRefNo == undefined) {
      form.value.custRefNo = '';
    }
    if (form.value.sourcingCode == null || form.value.sourcingCode == undefined) {
      form.value.sourcingCode = '';
    }
    if (form.value.applOccu == null || form.value.applOccu == undefined || form.value.applOccu == '') {
      form.value.applOccu = '';
      occuid = '';
    }
    else {
      occuid = this.occupationId.OCCUPATIONID;
    }
    if (form.value.annuIncome == null || form.value.annuIncome == undefined || form.value.annuIncome == '') {
      form.value.annuIncome == '';
      annuid = '';
    }
    else {
      annuid = this.annualIncomeId.value;
    }
    let userDate = this.quoteRequestData.Adult1Age;
    let requestBodyHB
    requestBodyHB = {
      "Product": "NA",
      "IsCustomerPID": "true",
      "UserType": "Agent",
      "ipaddress": config.ipAddress,
      "IPGPayment": "true",
      "ProductType": this.requestBody.ProductType,
      "ProductName": this.requestBody.ProductName,
      "GSTApplicable": "false",
      "UINApplicable": "false",
      "CustomerGSTDetails": {
        "GSTIN_NO": form.value.GSTIN,
        "CONSTITUTION_OF_BUSINESS": form.value.constitution,
        "CONSTITUTION_OF_BUSINESS_TEXT": this.constName,
        "CUSTOMER_TYPE": form.value.customerType,
        "CUSTOMER_TYPE_TEXT": this.custType,
        "PAN_NO": form.value.panNo,
        "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
        "GST_REGISTRATION_STATUS_TEXT": this.gstName
      },
      "PolicyID": this.responseBody.HealthDetails[0].PolicyID,    //add quate Id
      "DealID": this.responseBody.HealthDetails[0].DealId,
      "IS_IBANK_RELATIONSHIP": "NO",
      "APS_ID": [form.value.apsId],
      'Members': this.memberObj,
      "NomineeName": form.value.nomName,
      "NomineeTitleID": this.nomineeTitle.id,
      "NomineeRelationShipID": this.nomineeRelationId.RelationshipID, // check once
      "NomineeRelationShipText": form.value.nomRelation,
      "NomineeDOB": moment(form.value.nomDOB).format('DD MMM YYYY'),
      "HUF": "Y",
      "AppointeeLastName": form.value.appName,
      "AppointeeTitleID": this.appointeeTitle,
      "AppointeeRelationShipID": this.appointeeRelationId,//this.appointeeRelationId.RelationshipID,
      "AppointeeRelationShip": form.value.appRelation,
      "AppointeeDOB": appointeeDate,
      "SetAsDefault": "NO",
      "AddressChange": "YES",
      "CustomerID": this.createCustData.CustomerID,
      "OccupationID": occuid,
      "OccupationDesc": form.value.applOccu,
      "IDProofID": "2",
      "IDProofValue": "978456320164",
      "AadharEKYCYesNo": "N",
      "Title": this.titleId1.id, // pass id
      "InsuredID": "4",
      "CustomerName": form.value.applName,
      "Address1": form.value.applAdd1,
      "Address2": form.value.applAdd2,
      "Landmark": form.value.applLandMark,
      "StateID": this.pinData.StateId,
      "CityID": this.pinData.CityList[0].CityID, //pass id
      "PincodeID": this.pinDataValue.Details[0].Value,//form.value.applPinCode,
      "PermanentAddrLine1": form.value.permAdd1,
      "PermanentAddrLine2": form.value.permAdd2,
      "PermanentAddrLandmark": form.value.permLandMark,
      "PermanentAddrStateID": this.pinDataPer.StateId, //pass id
      "PermanentAddrCityID": this.pinDataPer.CityList[0].CityID,
      "PermanentAddrPincodeID": this.pinDataPermValue.Details[0].Value,//form.value.permPinCode,
      "Mobile": form.value.applMobNo.toString(),
      "LandLineNo": '',
      "STD": "",
      "Email": form.value.applEmail,
      "FinancierBranch": "",
      "FinancierName": "",
      "PaymentMode": "NONE",
      "Cheque_Type": "",
      "ModeID": "0001",
      "CardExpiryYear": "",
      "CardExpiryMonth": "",
      "CardNumber": "",
      "CVVNumber": "",
      "CardType": "",
      "CardHolderName": "",
      "PANNumber": form.value.panNumber,
      "AadhaarNumber": form.value.aadhaarNo,
      "CustomerDOB": moment(form.value.applDOB).format('DD-MMM-YYYY'),
      "SubLimit": "",
      "AddOn1": this.requestBody.AddOn1,
      "AddOn3": this.requestBody.AddOn3,
      "AddOn5": "false",
      "AddOn6": "false",
      "MaritalStatus": this.maritalStatusId.MaritalStatusCode, // pass id
      "AnnualIncome": annuid,  //annual income id
      "PF_CUSTOMERID": this.createCustData.PFCustomerID,
      "SaveType": "PROPOSAL",
      "Product24": form.value._80DTax,
    }
    let spdetails;
    if (this.ParentIMID != '' && this.ParentIMID != null && this.ParentIMID != undefined) {
      spdetails =
      {
        'AlternateRMCode': 'online ',
        'AlternateRMName': 'online ',
        "CustomerReferenceNumber": this.createCustData.PFCustomerID,
        'ChannelName': 'RLG',
        'PrimaryRMCode': 'online',
        'SecondaryRMCode': '',
        'BancaField01': '',
        'BancaField02': '',
        'BBancaField03': '',
      }
      requestBodyHB.SpDetails = spdetails;
    }
    // else if(this.ParentIMID == '' || this.ParentIMID == null || this.ParentIMID != undefined)
    // {
    //   spdetails=
    //   {
    //     AlternateRMCode:'' ,
    //     AlternateRMName:'' ,
    //     CustomerReferenceNumber:'' ,
    //     ChannelName:'' ,
    //     PrimaryRMCode:'' ,
    //     SecondaryRMCode:'' ,
    //     BancaField01:'' ,
    //     BancaField02:'' ,
    //     BancaField03:'' ,
    //   }

    // }
    // requestBodyHB.SpDetails=spdetails;
    console.log("Request for save Proposal", requestBodyHB);
    console.log("Stringify Request", JSON.stringify(requestBodyHB));
    localStorage.setItem('reqFromProp', JSON.stringify(requestBodyHB));
    let stringifyReq = JSON.stringify(requestBodyHB);
    this.cs.postWithParams('/api/HBooster/SaveEditProposal', stringifyReq).then((res: any) => {
      console.log("HB Response", res);
      if (res.StatusCode == '1') {
        localStorage.setItem('resFromProp', JSON.stringify(res));
        this.cs.showLoader = false;
        this.messageAlert('Your Proposal hasbeen saved with Policy Id: ' + res.SavePolicy[0].PolicyID + ' and will be expired in 7 days');
        this.router.navigateByUrl('health');
      }
      else { this.cs.showLoader = false;; this.messageAlert(res.StatusMessage); }
    }).catch((err: any) => {
      this.presentToast(err.error.Message);
      this.cs.showLoader = false;
    })
  }

  async searchCust() {
    console.log("modal");
    this.searchCustModal = await this.modalCtrl.create({
      component: SearchCustComponent,
      cssClass: 'custom_modal',
      componentProps: { users: this.users },
    });

    this.searchCustModal.onDidDismiss().then((data: any, type: any) => {
      console.log(data.data);
      if (!data.data) {
        this.isNew = true;
      }

      if (data.role == '0') { this.getCustomerDetails(data.data); }
      else if (data.role == '1') { this.mapDataToExistanceUserByPFId(data.data); }

    })
    return await this.searchCustModal.present();

  }

  getCustomerDetails(userData: any) {
    console.log("User Data", userData);
    this.cs.getWithParams('/api/Customer/GetCustomerDetails/' + userData.CustomerID).then((res: any) => {
      console.log("Cust Details", res);
      this.getDataName(res).then(() => {
        this.mapDataToExistanceUser(res);
        //this.isRegWithGST = true;
      }, (err: any) => {
        this.presentAlert('Sorry Something went wrong');
      });
    });
  }

  getDataName(data: any): Promise<any> {
    return new Promise((resolve) => {
      this.getTitleName(data);
      this.getMaritalName(data);
      this.getOccupationName(data);
      resolve();
    });
  }

  getTitleName(data: any) {
    if (data.TitleValue == '0') {
      this.titleName = 'Mrs.'
    } else if (data.TitleValue == '1') {
      this.titleName = 'Mr.'
    } else {
      this.titleName = 'Ms.'
    }
  }
  getMaritalName(data: any) {
    // this.marName = this.maritalStatus.MaritalStatusList.find(x => x.MaritalStatusCode = data.)
  }
  getOccupationName(data: any) {
    // this.occName = this.occupation.OccupationLists.find(x => )
  }
  mapDataToExistanceUserByPFId(data: any) {
    if (data.AadhaarNo != null && data.AadhaarNo != undefined && data.AadhaarNo != '') {
      this.applicantForm.patchValue({ 'aadhaarNo': data.AadhaarNo });
    }
    if (data.PAN_NO != null && data.PAN_NO != undefined && data.PAN_NO != '') {
      this.applicantForm.patchValue({ 'panNumber': data.PAN_NO });
    }
    if (data.TitleText != null && data.TitleText != undefined && data.TitleText != '') {
      this.applicantForm.patchValue({ 'applTitle': data.TitleText });
    }
    if (data.Name != null && data.Name != undefined && data.Name != '') {
      this.applicantForm.patchValue({ 'applName': data.Name });
    }
    if (data.DOB != null && data.DOB != undefined && data.DOB != '') {
      this.applicantForm.patchValue({ 'applDOB': moment(data.DOB).format('YYYY-MM-DD') });
    }
    if (data.Address1 != null && data.Address1 != undefined && data.Address1 != '') {
      this.applicantForm.patchValue({ 'applAdd1': data.Address1 });
    }
    if (data.Address2 != null && data.Address2 != undefined && data.Address2 != '') {
      this.applicantForm.patchValue({ 'applAdd2': data.Address2 });
    }
    if (data.PresentAddrLandmark != null && data.PresentAddrLandmark != undefined && data.PresentAddrLandmark != '') {
      this.applicantForm.patchValue({ 'applLandMark': data.PresentAddrLandmark });
    }
    if (data.PinCode != null && data.PinCode != undefined && data.PinCode != '') {
      this.applicantForm.patchValue({ 'applPinCode': data.PinCode });
    }
    if (data.MARITAL_STATUS != null && data.MARITAL_STATUS != undefined && data.MARITAL_STATUS != '') {
      this.applicantForm.patchValue({ 'applMarital': data.MARITAL_STATUS });

    }
    if (data.Occupation != null && data.Occupation != undefined && data.Occupation != '') {
      this.applicantForm.patchValue({ 'applOccu': data.Occupation });
    }
    if (data.AnnualIncomeText != null && data.AnnualIncomeText != undefined && data.AnnualIncomeText != '') {
      this.applicantForm.patchValue({ 'annuIncome': data.AnnualIncomeText });
    }
    if (data.EmailID != null && data.EmailID != undefined && data.EmailID != '') {
      this.applicantForm.patchValue({ 'applEmail': data.EmailID });
    }
    if (data.Mobile != null && data.Mobile != undefined && data.Mobile != '') {
      this.applicantForm.patchValue({ 'applMobNo': data.Mobile });
    }
    if (data.GSTDetails != null && data.GSTDetails != undefined) {
      this.isGSTReg(data.GSTApplicable);
      this.radio(!data.IS_UIN_APPLICABLE);
      if (data.GSTIN_UIN_NO != null && data.GSTIN_UIN_NO != undefined && data.GSTIN_UIN_NO != '') {
        this.applicantForm.patchValue({ 'GSTIN': data.GSTIN_UIN_NO });
        this.applicantForm.patchValue({ 'UIN': data.GSTIN_UIN_NO });
      }
      if (data.GST_REGISTRATION_STATUS != null && data.GST_REGISTRATION_STATUS != undefined && data.GST_REGISTRATION_STATUS != '') {
        this.applicantForm.patchValue({ 'gstRegStatus': data.GST_REGISTRATION_STATUS });
      }
      if (data.CUSTOMER_TYPE != null && data.CUSTOMER_TYPE != undefined && data.CUSTOMER_TYPE != '') {
        this.applicantForm.patchValue({ 'customerType': data.CUSTOMER_TYPE });
      }
      if (data.PAN_NO != null && data.PAN_NO != undefined && data.PAN_NO != '') {
        this.applicantForm.patchValue({ 'panNo': data.PAN_NO });
      }
      if (data.CONSTITUTION_OF_BUSINESS != null && data.CONSTITUTION_OF_BUSINESS != undefined && data.CONSTITUTION_OF_BUSINESS != '') {
        this.applicantForm.patchValue({ 'constitution': data.CONSTITUTION_OF_BUSINESS });
      }
    }
    if (data.PresentAddrStateValue == '62') {
      this.isRegWithGST = true;
      this.iskeralaCess = true;
    }
    else {
      this.iskeralaCess = false;
      this.isRegWithGST = false;
    }
  }

  mapDataToExistanceUser(data: any) {
    // this.applicantForm.patchValue({'aadhaarNo': data });
    if (data.PANNumber != null && data.PANNumber != undefined && data.PANNumber != '') {
      this.applicantForm.patchValue({ 'panNumber': data.PANNumber });
    }
    if (data.TitleText != null && data.TitleText != undefined && data.TitleText != '') {
      this.applicantForm.patchValue({ 'applTitle': data.TitleText });
    }
    if (data.Name != null && data.Name != undefined && data.Name != '') {
      this.applicantForm.patchValue({ 'applName': data.Name });
    }
    if (data.DateOfBirth != null && data.DateOfBirth != undefined && data.DateOfBirth != '') {
      this.applicantForm.patchValue({ 'applDOB': moment(data.DateOfBirth, 'DD/MM/YYYY').format('DD-MM-YYYY') });
    }
    if (data.PresentAddrLine1 != null && data.PresentAddrLine1 != undefined && data.PresentAddrLine1 != '') {
      this.applicantForm.patchValue({ 'applMobNo': data.MobileNumber });
    }
    if (data.PresentAddrLine1 != null && data.PresentAddrLine1 != undefined && data.PresentAddrLine1 != '') {
      this.applicantForm.patchValue({ 'applAdd1': data.PresentAddrLine1 });
    }
    if (data.PresentAddrLine2 != null && data.PresentAddrLine2 != undefined && data.PresentAddrLine2 != '') {
      this.applicantForm.patchValue({ 'applAdd2': data.PresentAddrLine2 });
    }
    if (data.PresentAddrLandmark != null && data.PresentAddrLandmark != undefined && data.PresentAddrLandmark != '') {
      this.applicantForm.patchValue({ 'applLandMark': data.PresentAddrLandmark });
    }
    if (data.PresentAddrPincodeText != null && data.PresentAddrPincodeText != undefined && data.PresentAddrPincodeText != '') {
      this.applicantForm.patchValue({ 'applPinCode': data.PresentAddrPincodeText });
      this.getPincodeForExitingUser(data.PresentAddrPincodeText);
    }
    if (data.MaritalStatusText != undefined && data.MaritalStatusText != null && data.PresentAddrPincodeText != '') {
      this.applicantForm.patchValue({ 'applMarital': data.MaritalStatusText });
      this.showMaritalStatusForExisitng(data.MaritalStatusText);
    }
    if (data.OccupationText != undefined && data.OccupationText != null && data.OccupationText != '') {
      this.applicantForm.patchValue({ 'applOccu': data.OccupationText });
      this.getOccIdForExisitng(data.OccupationText);
    }
    if (data.AnnualIncomeText != undefined && data.AnnualIncomeText != null && data.AnnualIncomeText != '') {
      this.applicantForm.patchValue({ 'annuIncome': data.AnnualIncomeText });
      this.getAnnualIncIdForExisitng(data.AnnualIncomeText);
    }
    if (data.EmailAddress != undefined && data.EmailAddress != null && data.EmailAddress != '') {
      this.applicantForm.patchValue({ 'applEmail': data.EmailAddress });
    }
    if (data.LandlineNumber != undefined && data.LandlineNumber != null && data.AlternateEmail != '') {
      this.applicantForm.patchValue({ 'landLine': data.LandlineNumber });
    }
    if (data.AlternateEmail != undefined && data.AlternateEmail != null && data.EmailAddress != '') {
      this.applicantForm.patchValue({ 'applaltEmail': data.AlternateEmail });
    }
    if (data.AlternateMobile != undefined && data.AlternateMobile != null && data.AlternateMobile != '') {
      this.applicantForm.patchValue({ 'applaltMobNo': data.AlternateMobile });
    }
    if (data.PermanentAddrLine1 != undefined && data.PermanentAddrLine1 != null && data.PermanentAddrLine1 != '') {
      this.applicantForm.patchValue({ 'permAdd1': data.PermanentAddrLine1 });
    }
    if (data.PermanentAddrLine2 != undefined && data.PermanentAddrLine2 != null && data.PermanentAddrLine2 != '') {
      this.applicantForm.patchValue({ 'permAdd2': data.PermanentAddrLine2 });
    }
    if (data.PermanentAddrLandmark != undefined && data.PermanentAddrLandmark != null && data.PermanentAddrLandmark != '') {
      this.applicantForm.patchValue({ 'permLandMark': data.PermanentAddrLandmark });
    }
    if (data.PermanentAddrPincodeText != undefined && data.PermanentAddrPincodeText != null && data.EmailAddress != '') {
      this.applicantForm.patchValue({ 'permPinCode': data.PermanentAddrPincodeText });
      this.getPerPincodeForExitingUser(data.PresentAddrPincodeText);
    }
    if (data.GSTDetails != null && data.GSTDetails != undefined) {
      this.applicantForm.patchValue({ 'GSTIN': data.GSTDetails.GSTIN_NO });
      // this.applicantForm.patchValue({'UIN': data });
      this.applicantForm.patchValue({ 'gstRegStatus': data.GSTDetails.GST_REGISTRATION_STATUS_TEXT });
      this.applicantForm.patchValue({ 'customerType': data.GSTDetails.CUSTOMER_TYPE_TEXT });
      this.applicantForm.patchValue({ 'panNo': data.GSTDetails.PAN_NO });
      this.applicantForm.patchValue({ 'constitution': data.GSTDetails.CONSTITUTION_OF_BUSINESS_TEXT });
      this.isRegWithGST = true;
    }
    if (data.PresentAddrStateValue == '62') {
      this.isRegWithGST = true;
      this.iskeralaCess = true;
    }
    else {
      this.iskeralaCess = false;
    }

  }
  showMaritalStatusForExisitng(ev: any) {
    this.maritalStatusId = this.maritalStatus.MaritalStatusList.find((x: any) => x.MaritalStatusDesc == ev);
  }
  getAnnualIncIdForExisitng(ev: any) {
    this.annualIncomeId = this.annualIncome.Values.find((x: any) => x.Name == ev);
  }

  getOccIdForExisitng(ev: any) {
    this.occupationId = this.occupation.find((x: any) => x.OCCUPATION == ev);
  }

  getPincodeForExitingUser(val: any) {
    // 
    if (val.length == 6) {
      //this.showLoading();
      let body = val;
      this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then((res) => {
        this.pinData = res;

        if (this.pinData.StatusCode == 1) {
          this.applicantForm.patchValue({ 'applCity': this.pinData.CityList[0].CityName });
          this.applicantForm.patchValue({ 'applState': this.pinData.StateName });
          let proposalStateCity = { pin: body, city: this.pinData.CityList[0].CityName, state: this.pinData.StateName };
          localStorage.setItem('CoresspondentStateCity', JSON.stringify(proposalStateCity));
          //  this.loading.dismiss();
        }
        else {
          this.applicantForm.patchValue({ 'applCity': '' });
          this.applicantForm.patchValue({ 'applState': null });
          let proposalStateCity = { pin: body, city: null, state: null };
          localStorage.setItem('CoresspondentStateCity', JSON.stringify(proposalStateCity));
          this.presentAlert(this.pinData.StatusMessage);
          //  this.loading.dismiss();
        }

      });
    }
  }

  getPerPincodeForExitingUser(val) {
    if (val.length == 6) {
      let body = val;
      //this.showLoading();
      this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then((res) => {
        this.pinDataPer = res;
        if (this.pinDataPer.StatusCode == 1) {
          this.applicantForm.patchValue({ 'permCity': this.pinDataPer.CityList[0].CityName });
          this.applicantForm.patchValue({ 'permState': this.pinDataPer.StateName });
          let proposalStateCity = { pin: body, city: this.pinDataPer.CityList[0].CityName, state: this.pinDataPer.StateName };
          localStorage.setItem('PermanentStateCity', JSON.stringify(proposalStateCity));
          // this.loading.dismiss();
        }
        else {
          this.applicantForm.patchValue({ 'permCity': null });
          this.applicantForm.patchValue({ 'permState': null });
          let proposalStateCity = { pin: body, city: null, state: null };
          this.presentAlert(this.pinDataPer.StatusMessage);
          // this.loading.dismiss();
        }
      });
    }
  }
  //--Proposal Applicant Code Begins--//

  //--Proposal Insured Code Begins--//

  getCall(): Promise<any> {
    return new Promise((resolve) => {
      this.data = localStorage.getItem('requestForProp');
      resolve();
    });
  }


  // -- Proposal Insured Code Begins--//
  showSegments(form: any) {
    console.log("form", form);
    if (form.ProductType == 'CHI' || form.ProductType == 'ASP') {
      let data = form;
      if (data.NoOfAdults == '0') {
        this.isAdult1 = false; this.isAdult2 = false; this.isChild1 = true; this.isChild2 = false; this.isChild3 = false;
      } else if (data.NoOfAdults == '1') {
        this.isAdult1 = true; this.isAdult2 = false;
        if (data.NoOfKids == '0' || data.NoOfKids == '') {
          this.isChild1 = false; this.isChild2 = false; this.isChild3 = false;
        } else if (data.NoOfKids == '1') {
          this.isChild1 = true; this.isChild2 = false; this.isChild3 = false;
        } else {
          this.isChild1 = true; this.isChild2 = true; this.isChild3 = false;
        }
      } else {
        this.isAdult1 = true; this.isAdult2 = true;
        if (data.NoOfKids == '0' || data.NoOfKids == '') {
          this.isChild1 = false; this.isChild2 = false; this.isChild3 = false;
        } else if (data.NoOfKids == '1') {
          this.isChild1 = true; this.isChild2 = false; this.isChild3 = false;
        } else if (data.NoOfKids == '2') {
          this.isChild1 = true; this.isChild2 = true; this.isChild3 = false;
        } else {
          this.isChild1 = true; this.isChild2 = true; this.isChild3 = true;
        }
      }
    } else if (form.ProductType == 'HBOOSTER') {
      let data = form;
      if (data.NoOfAdults == '0') {
        this.isAdult1 = false; this.isAdult2 = false; this.isChild1 = true; this.isChild2 = false; this.isChild3 = false;
      } else if (data.NoOfAdults == '1') {
        this.isAdult1 = true; this.isAdult2 = false;
        if (data.NoOfKids == '0' || data.NoOfKids == '') {
          this.isChild1 = false; this.isChild2 = false; this.isChild3 = false;
        } else if (data.NoOfKids == '1') {
          this.isChild1 = true; this.isChild2 = false; this.isChild3 = false;
        } else {
          this.isChild1 = true; this.isChild2 = true; this.isChild3 = false;
        }
      } else {
        this.isAdult1 = true; this.isAdult2 = true;
        if (data.NoOfKids == '0' || data.NoOfKids == '') {
          this.isChild1 = false; this.isChild2 = false; this.isChild3 = false;
        } else if (data.NoOfKids == '1') {
          this.isChild1 = true; this.isChild2 = false; this.isChild3 = false;
        } else if (data.NoOfKids == '2') {
          this.isChild1 = true; this.isChild2 = true; this.isChild3 = false;
        } else {
          this.isChild1 = true; this.isChild2 = true; this.isChild3 = true;
        }
      }
    }
  }
  segmentChanged(ev: any) {
    console.log(ev.detail.value);
    this.showSegment = ev.detail.value;
  }

  createInsuredForm() {
    this.insuredForm = new FormGroup({
      member1Cat: new FormControl('Adult'),
      member1Rela: new FormControl(''),
      member1Title: new FormControl(''),
      member1Name: new FormControl(),
      member1DOB: new FormControl(),
      member1Feet: new FormControl(''),
      member1Inches: new FormControl(''),
      member1Weight: new FormControl(),
      member1Disease: new FormControl(),

      // FOR HBOOSTER
      TypeofPolicy1: new FormControl(''),
      PolicyDuration1: new FormControl(),
      InsuranceCompany1: new FormControl(''),
      SumInsured1: new FormControl(),

      member2Cat: new FormControl('Adult'),
      member2Rela: new FormControl(''),
      member2Title: new FormControl(''),
      member2Name: new FormControl(),
      member2DOB: new FormControl(),
      member2Feet: new FormControl(''),
      member2Inches: new FormControl(''),
      member2Weight: new FormControl(),
      member2Disease: new FormControl(),

      // FOR HBOOSTER
      TypeofPolicy2: new FormControl(''),
      PolicyDuration2: new FormControl(),
      InsuranceCompany2: new FormControl(''),
      SumInsured2: new FormControl(),

      child1Cat: new FormControl('Child'),
      child1Rela: new FormControl(''),
      child1Title: new FormControl(''),
      child1Name: new FormControl(),
      child1DOB: new FormControl(),
      child1Feet: new FormControl(''),
      child1Inches: new FormControl(''),
      child1Weight: new FormControl(),
      child1Disease: new FormControl(),
      // FOR HBOOSTER
      TypeofPolicy3: new FormControl(''),
      PolicyDuration3: new FormControl(),
      InsuranceCompany3: new FormControl(''),
      SumInsured3: new FormControl(),

      child2Cat: new FormControl('Child'),
      child2Rela: new FormControl(''),
      child2Title: new FormControl(''),
      child2Name: new FormControl(),
      child2DOB: new FormControl(),
      child2Feet: new FormControl(''),
      child2Inches: new FormControl(''),
      child2Weight: new FormControl(),
      child2Disease: new FormControl(),
      // FOR HBOOSTER
      TypeofPolicy4: new FormControl(''),
      PolicyDuration4: new FormControl(),
      InsuranceCompany4: new FormControl(''),
      SumInsured4: new FormControl(),

      child3Cat: new FormControl('Child'),
      child3Rela: new FormControl(''),
      child3Title: new FormControl(''),
      child3Name: new FormControl(),
      child3DOB: new FormControl(),
      child3Feet: new FormControl(''),
      child3Inches: new FormControl(''),
      child3Weight: new FormControl(),
      child3Disease: new FormControl(),
      // FOR HBOOSTER
      TypeofPolicy5: new FormControl(''),
      PolicyDuration5: new FormControl(),
      InsuranceCompany5: new FormControl(''),
      SumInsured5: new FormControl(),
    });
  }

  createPPAPInsuranceForm() {
    let dob = this.ppapDOB; let occ = this.ppapOccupation;
    let memberType = this.ppapMemeberType;
    this.insuredPPAPForm = new FormGroup({
      member1Cat: new FormControl(memberType),
      member1Rela: new FormControl(''),
      member1Title: new FormControl(''),
      member1Name: new FormControl(),
      member1DOB: new FormControl(dob),
      insuredpanno: new FormControl(),
      insuredOccupation: new FormControl(occ),
      riskStartDate: new FormControl(),
      aiPolicy: new FormControl(),
      insuranceComapny: new FormControl(''),
      insurancePolicy: new FormControl(),
      policyValidFrom: new FormControl(''),
      policyValidTill: new FormControl(''),
      policySumInsured: new FormControl(),
      claimsMade: new FormControl(),
    });
  }
  acitrigger(val) {
    this.insuredPPAPForm.value.aiPolicy = val;
    this.showOtherPolicyDetails = this.insuredPPAPForm.value.aiPolicy;

  }
  claimstrigger(val) {
    this.insuredPPAPForm.value.claimsMade = val;
    this.showClaimsDetails = this.insuredPPAPForm.value.claimsMade;
  }
  getFeet(ev: any) {
    console.log(ev.target.value);
  }

  getInches(ev: any) {
    console.log(ev.target.value);
  }

  getRelation() {
    let producttype;
    if (this.quoteRequestData.ProductType == 'CHI') {
      producttype = 'CHI'
    }
    if (this.quoteRequestData.ProductType == 'HBOOSTER') {
      producttype = 'HBOOSTER'
    }
    if (this.quoteRequestData.ProductType == "PPAP") {
      producttype = 'PPAP'
    }
    if (this.quoteRequestData.ProductType == 'ASP') {
      producttype = 'ASP'
    }
    // else
    // {
    //   producttype='PPAP'
    // }
    this.cs.postWithParams('/api/healthmaster/GetHealthProposalRelationships?Product=' + producttype, '').then((res: any) => {
      console.log("Rel", res)
      this.adultRelationArray = [];
      this.childRelationArray = [];
      res.InsuredRelationship.forEach((relation: any) => {
        if (relation.KidAdultType == 'Adult') {
          this.adultRelationArray.push(relation);
        } else {
          this.childRelationArray.push(relation);
        }
      });
      this.adultRelationArray = _.sortBy(this.adultRelationArray, 'RelationshipName');
      this.childRelationArray = _.sortBy(this.childRelationArray, 'RelationshipName');
    }).catch((err: any) => {
      console.log(err);
    });
  }

  isDisease1(ev: any) {
    console.log(ev.detail);
  }

  getQuestionList() {
    let endpoint;
    if (this.quoteRequestData.ProductType == 'ASP') {
      endpoint = '/api/healthmaster/GetHealthAilmentList?isAgent=YES&producttype=ASP'
    }
    else {
      endpoint = '/api/healthmaster/GetHealthAilmentList?isAgent=YES'
    }
    this.cs.getWithParams(endpoint).then((res: any) => {
      console.log("JSON Ailment", res);
      this.questionList = res.Details;
      // for(let i=0;i<this.questionList.length;i++)
      // {
      //   this.questionList[i].isChecked=false;
      // }
    }).catch((err: any) => {
      console.log(err);
    })
  }

  getTitle1(ev: any) {
    console.log(ev.target.value);
    this.titleId1 = this.titles.find(x => x.val == ev.target.value);
    console.log(this.titleId1);
  }

  getTitle2(ev: any) {
    console.log(ev.target.value);
    this.titleId2 = this.titles.find(x => x.val == ev.target.value);
  }

  getTitle3(ev: any) {
    console.log(ev.target.value);
    this.titleId3 = this.titles.find(x => x.val == ev.target.value);
  }

  getTitle4(ev: any) {
    console.log(ev.target.value);
    this.titleId4 = this.titles.find(x => x.val == ev.target.value);
  }

  getTitle5(ev: any) {
    console.log(ev.target.value);
    this.titleId5 = this.titles.find(x => x.val == ev.target.value);
  }

  getRelation1(ev: any) {
    console.log(ev.target.value);
    this.relationId1 = this.adultRelationArray.find(x => x.RelationshipName == ev.target.value);
  }

  getRelation2(ev: any) {
    console.log(ev.target.value);
    this.relationId2 = this.adultRelationArray.find(x => x.RelationshipName == ev.target.value);
  }

  getRelation3(ev: any) {
    console.log(ev.target.value);
    this.relationId3 = this.childRelationArray.find(x => x.RelationshipName == ev.target.value);
  }

  getRelation4(ev: any) {
    console.log(ev.target.value);
    this.relationId4 = this.childRelationArray.find(x => x.RelationshipName == ev.target.value);
  }

  getRelation5(ev: any) {
    console.log(ev.target.value);
    this.relationId5 = this.childRelationArray.find(x => x.RelationshipName == ev.target.value);
  }

  getDiseases1(d: any, ev: any) {
    if (ev.detail.checked) {
      for (let i = 0; i < this.ailmentArray1.length; i++) {
        if (this.ailmentArray1[i].AilmentName == d.Name) {
          let index = this.ailmentArray1.findIndex(record => record.AilmentName === d.Name);
          this.ailmentArray1.splice(index, 1);
        }
      }
      this.ailmentArray1.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value })
    }
    else {
      for (let i = 0; i < this.ailmentArray1.length; i++) {
        if (this.ailmentArray1[i].AilmentName == d.Name) {
          let index = this.ailmentArray1.findIndex(record => record.AilmentName === d.Name);
          this.ailmentArray1.splice(index, 1);
        }
      }
    }
  }
  getDiseases2(d: any, ev: any) {
    if (ev.detail.checked) {
      for (let i = 0; i < this.ailmentArray2.length; i++) {
        if (this.ailmentArray2[i].AilmentName == d.Name) {
          let index = this.ailmentArray2.findIndex(record => record.AilmentName === d.Name);
          this.ailmentArray2.splice(index, 1);
        }
      }
      this.ailmentArray2.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value })
    }
    else {
      for (let i = 0; i < this.ailmentArray2.length; i++) {
        if (this.ailmentArray2[i].AilmentName == d.Name) {
          let index = this.ailmentArray2.findIndex(record => record.AilmentName === d.Name);
          this.ailmentArray2.splice(index, 1);
        }
      }
    }
  }
  getDiseases3(d: any, ev: any) {
    if (ev.detail.checked) {
      for (let i = 0; i < this.ailmentArray3.length; i++) {
        if (this.ailmentArray3[i].AilmentName == d.Name) {
          let index = this.ailmentArray3.findIndex(record => record.AilmentName === d.Name);
          this.ailmentArray3.splice(index, 1);
        }
      }
      this.ailmentArray3.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value })
    }
    else {
      for (let i = 0; i < this.ailmentArray3.length; i++) {
        if (this.ailmentArray3[i].AilmentName == d.Name) {
          let index = this.ailmentArray3.findIndex(record => record.AilmentName === d.Name);
          this.ailmentArray3.splice(index, 1);
        }
      }
    }
  }
  getDiseases4(d: any, ev: any) {
    if (ev.detail.checked) {
      for (let i = 0; i < this.ailmentArray4.length; i++) {
        if (this.ailmentArray4[i].AilmentName == d.Name) {
          let index = this.ailmentArray4.findIndex(record => record.AilmentName === d.Name);
          this.ailmentArray4.splice(index, 1);
        }
      }
      this.ailmentArray4.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value })
    }
    else {
      for (let i = 0; i < this.ailmentArray4.length; i++) {
        if (this.ailmentArray4[i].AilmentName == d.Name) {
          let index = this.ailmentArray4.findIndex(record => record.AilmentName === d.Name);
          this.ailmentArray4.splice(index, 1);
        }
      }
    }
  }
  getDiseases5(d: any, ev: any) {
    if (ev.detail.checked) {
      for (let i = 0; i < this.ailmentArray5.length; i++) {
        if (this.ailmentArray5[i].AilmentName == d.Name) {
          let index = this.ailmentArray5.findIndex(record => record.AilmentName === d.Name);
          this.ailmentArray5.splice(index, 1);
        }
      }
      this.ailmentArray5.push({ "AilmentID": 0, "Month": "1", "Year": "1", "AilmentName": d.Name, "PEDCode": d.Value })
    }
    else {
      for (let i = 0; i < this.ailmentArray5.length; i++) {
        if (this.ailmentArray5[i].AilmentName == d.Name) {
          let index = this.ailmentArray5.findIndex(record => record.AilmentName === d.Name);
          this.ailmentArray5.splice(index, 1);
        }
      }
    }
  }

  createHBMembers(data: any): Promise<any> {
    return new Promise((resolve) => {
      console.log("create Dummy obj", data);
      let isxistingPolicy1, isxistingPolicy2, isxistingPolicy3, isxistingPolicy4, isxistingPolicy5;
      if (this.isExistPolicy1 == false) {
        this.insuredForm.value.TypeofPolicy1 = ''; this.insuredForm.value.SumInsured1 = '';
        this.insuredForm.value.PolicyDuration1 = ''; this.insuredForm.value.InsuranceCompany1 = '';
        isxistingPolicy1 = 'false';
      }
      else {
        isxistingPolicy1 = 'true';
      }
      if (this.isExistPolicy2 == false) {
        this.insuredForm.value.TypeofPolicy2 = ''; this.insuredForm.value.SumInsured2 = '';
        this.insuredForm.value.PolicyDuration2 = ''; this.insuredForm.value.InsuranceCompany2 = '';
        isxistingPolicy2 = 'false';
      }
      else {
        isxistingPolicy2 = 'true';
      }
      if (this.isExistPolicy3 == false) {
        this.insuredForm.value.TypeofPolicy3 = ''; this.insuredForm.value.SumInsured3 = '';
        this.insuredForm.value.PolicyDuration3 = ''; this.insuredForm.value.InsuranceCompany3 = '';
        isxistingPolicy3 = 'false';
      }
      else {
        isxistingPolicy3 = 'true';
      }
      if (this.isExistPolicy4 == false) {
        this.insuredForm.value.TypeofPolicy4 = ''; this.insuredForm.value.SumInsured4 = '';
        this.insuredForm.value.PolicyDuration4 = ''; this.insuredForm.value.InsuranceCompany4 = '';
        isxistingPolicy4 = 'false';
      }
      else {
        isxistingPolicy4 = 'true';
      }
      if (this.isExistPolicy5 == false) {
        this.insuredForm.value.TypeofPolicy5 = ''; this.insuredForm.value.SumInsured5 = '';
        this.insuredForm.value.PolicyDuration5 = ''; this.insuredForm.value.InsuranceCompany5 = '';
        isxistingPolicy5 = 'false';
      }
      else {
        isxistingPolicy5 = 'true';
      }
      if (this.isDisease == false) {
        this.ailmentArray1 = [];
      }
      if (this.isDisease2 == false) {
        this.ailmentArray2 = [];
      }
      if (this.isDisease3 == false) {
        this.ailmentArray3 = [];
      }
      if (this.isDisease4 == false) {
        this.ailmentArray4 = [];
      }
      if (this.isDisease5 == false) {
        this.ailmentArray5 = [];
      }
      if (this.product == "HBOOSTER") { this.insuredForm.value.member1DOB = this.quoteRequestData.Adult1Age; }
      if (this.product == "HBOOSTER") { this.insuredForm.value.member2DOB = this.quoteRequestData.Adult2Age; }
      if (this.jsonData.NoOfAdults == '0') {

        this.memberObj = [{
          "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
          "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
          "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
          "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
          "isExisting": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
          "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3,
          "Ailments": this.ailmentArray3
        }];
        resolve();
      } else if (this.jsonData.NoOfAdults == '1') {
        if (this.jsonData.NoOfKids == '0') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1,
            "Ailments": this.ailmentArray1
          }];
          resolve();
        } else if (this.jsonData.NoOfKids == '1') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1,
            "Ailments": this.ailmentArray1
          }, {
            "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
            "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
            "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
            "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3,
            "Ailments": this.ailmentArray3
          }];
          resolve();
        } else if (this.jsonData.NoOfKids == '2') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1,
            "Ailments": this.ailmentArray1
          }, {
            "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
            "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
            "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
            "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3,
            "Ailments": this.ailmentArray3
          }, {
            "MemberType": this.insuredForm.value.child2Cat, "TitleID": this.titleId4.id, "Name": this.insuredForm.value.child2Name,
            "RelationshipID": this.relationId4.RelationshipID, "RelationshipName": this.insuredForm.value.child2Rela,
            "DOB": moment(this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child2Feet + '.' + this.insuredForm.value.child2Inches,
            "Weight": this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy4, "TypeofPolicy": this.insuredForm.value.TypeofPolicy4, "PolicyDuration": this.insuredForm.value.PolicyDuration4,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany4, "SumInsured": this.insuredForm.value.SumInsured4,
            "Ailments": this.ailmentArray4
          }];
          resolve();
        }
      } else if (this.jsonData.NoOfAdults == '2') {
        if (this.jsonData.NoOfKids == '0') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1,
            "Ailments": this.ailmentArray1

          }, {
            "MemberType": this.insuredForm.value.member2Cat, "TitleID": this.titleId2.id, "Name": this.insuredForm.value.member2Name,
            "RelationshipID": this.relationId2.RelationshipID, "RelationshipName": this.insuredForm.value.member2Rela,
            "DOB": moment(this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member2Feet + '.' + this.insuredForm.value.member2Inches,
            "Weight": this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy2, "TypeofPolicy": this.insuredForm.value.TypeofPolicy2, "PolicyDuration": this.insuredForm.value.PolicyDuration2,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany2, "SumInsured": this.insuredForm.value.SumInsured2,
            "Ailments": this.ailmentArray2
          }];
          resolve();
        }
        else if (this.jsonData.NoOfKids == '1') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1,
            "Ailments": this.ailmentArray1

          }, {
            "MemberType": this.insuredForm.value.member2Cat, "TitleID": this.titleId2.id, "Name": this.insuredForm.value.member2Name,
            "RelationshipID": this.relationId2.RelationshipID, "RelationshipName": this.insuredForm.value.member2Rela,
            "DOB": moment(this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member2Feet + '.' + this.insuredForm.value.member2Inches,
            "Weight": this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy2, "TypeofPolicy": this.insuredForm.value.TypeofPolicy2, "PolicyDuration": this.insuredForm.value.PolicyDuration2,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany2, "SumInsured": this.insuredForm.value.SumInsured2,
            "Ailments": this.ailmentArray2

          }, {
            "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
            "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
            "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
            "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3,
            "Ailments": this.ailmentArray3

          }];
          resolve();
        } else if (this.jsonData.NoOfKids == '2') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1,
            "Ailments": this.ailmentArray1

          }, {
            "MemberType": this.insuredForm.value.member2Cat, "TitleID": this.titleId2.id, "Name": this.insuredForm.value.member2Name,
            "RelationshipID": this.relationId2.RelationshipID, "RelationshipName": this.insuredForm.value.member2Rela,
            "DOB": moment(this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member2Feet + '.' + this.insuredForm.value.member2Inches,
            "Weight": this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy2, "TypeofPolicy": this.insuredForm.value.TypeofPolicy2, "PolicyDuration": this.insuredForm.value.PolicyDuration2,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany2, "SumInsured": this.insuredForm.value.SumInsured2,
            "Ailments": this.ailmentArray2

          }, {
            "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
            "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
            "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
            "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3,
            "Ailments": this.ailmentArray3

          }, {
            "MemberType": this.insuredForm.value.child2Cat, "TitleID": this.titleId4.id, "Name": this.insuredForm.value.child2Name,
            "RelationshipID": this.relationId4.RelationshipID, "RelationshipName": this.insuredForm.value.child2Rela,
            "DOB": moment(this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child2Feet + '.' + this.insuredForm.value.child2Inches,
            "Weight": this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy4, "TypeofPolicy": this.insuredForm.value.TypeofPolicy4, "PolicyDuration": this.insuredForm.value.PolicyDuration4,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany4, "SumInsured": this.insuredForm.value.SumInsured4,
            "Ailments": this.ailmentArray4

          }];
          resolve();
        } else if (this.jsonData.NoOfKids == '3') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1,
            "Ailments": this.ailmentArray1

          }, {
            "MemberType": this.insuredForm.value.member2Cat, "TitleID": this.titleId2.id, "Name": this.insuredForm.value.member2Name,
            "RelationshipID": this.relationId2.RelationshipID, "RelationshipName": this.insuredForm.value.member2Rela,
            "DOB": moment(this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member2Feet + '.' + this.insuredForm.value.member2Inches,
            "Weight": this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy2, "TypeofPolicy": this.insuredForm.value.TypeofPolicy2, "PolicyDuration": this.insuredForm.value.PolicyDuration2,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany2, "SumInsured": this.insuredForm.value.SumInsured2,
            "Ailments": this.ailmentArray2

          }, {
            "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
            "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
            "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
            "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3,
            "Ailments": this.ailmentArray3

          }, {
            "MemberType": this.insuredForm.value.child2Cat, "TitleID": this.titleId4.id, "Name": this.insuredForm.value.child2Name,
            "RelationshipID": this.relationId4.RelationshipID, "RelationshipName": this.insuredForm.value.child2Rela,
            "DOB": moment(this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child2Feet + '.' + this.insuredForm.value.child2Inches,
            "Weight": this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy4, "TypeofPolicy": this.insuredForm.value.TypeofPolicy4, "PolicyDuration": this.insuredForm.value.PolicyDuration4,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany4, "SumInsured": this.insuredForm.value.SumInsured4,
            "Ailments": this.ailmentArray4

          }, {
            "MemberType": this.insuredForm.value.child3Cat, "TitleID": this.titleId5.id, "Name": this.insuredForm.value.child3Name,
            "RelationshipID": this.relationId5.RelationshipID, "RelationshipName": this.insuredForm.value.child3Rela,
            "DOB": moment(this.insuredForm.value.child3DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child3Feet + '.' + this.insuredForm.value.child3Inches,
            "Weight": this.insuredForm.value.child3Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "isExisting": isxistingPolicy5, "TypeofPolicy": data.value.TypeofPolicy5, "PolicyDuration": this.insuredForm.value.PolicyDuration5,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany5, "SumInsured": this.insuredForm.value.SumInsured5,
            "Ailments": this.ailmentArray5

          }];
          resolve();
        }
      }
    });
  }

  createCHIMembers(data: any): Promise<any> {
    return new Promise((resolve) => {
      console.log("create Dummy obj", data);
      let isxistingPolicy1, isxistingPolicy2, isxistingPolicy3, isxistingPolicy4, isxistingPolicy5;
      if (this.isExistPolicy1 == false) {
        this.insuredForm.value.TypeofPolicy1 = ''; this.insuredForm.value.SumInsured1 = '';
        this.insuredForm.value.PolicyDuration1 = ''; this.insuredForm.value.InsuranceCompany1 = '';
        isxistingPolicy1 = 'No';
      }
      else {
        isxistingPolicy1 = 'Yes';
      }
      if (this.isExistPolicy2 == false) {
        this.insuredForm.value.TypeofPolicy2 = ''; this.insuredForm.value.SumInsured2 = '';
        this.insuredForm.value.PolicyDuration2 = ''; this.insuredForm.value.InsuranceCompany2 = '';
        isxistingPolicy2 = 'No';
      }
      else {
        isxistingPolicy2 = 'Yes';
      }
      if (this.isExistPolicy3 == false) {
        this.insuredForm.value.TypeofPolicy3 = ''; this.insuredForm.value.SumInsured3 = '';
        this.insuredForm.value.PolicyDuration3 = ''; this.insuredForm.value.InsuranceCompany3 = '';
        isxistingPolicy3 = 'No';
      }
      else {
        isxistingPolicy3 = 'Yes';
      }
      if (this.isExistPolicy4 == false) {
        this.insuredForm.value.TypeofPolicy4 = ''; this.insuredForm.value.SumInsured4 = '';
        this.insuredForm.value.PolicyDuration4 = ''; this.insuredForm.value.InsuranceCompany4 = '';
        isxistingPolicy4 = 'No';
      }
      else {
        isxistingPolicy4 = 'Yes';
      }
      if (this.isExistPolicy5 == false) {
        this.insuredForm.value.TypeofPolicy5 = ''; this.insuredForm.value.SumInsured5 = '';
        this.insuredForm.value.PolicyDuration5 = ''; this.insuredForm.value.InsuranceCompany5 = '';
        isxistingPolicy5 = 'No';
      }
      else {
        isxistingPolicy5 = 'Yes';
      }
      if (this.product == "HBOOSTER") { this.insuredForm.value.member1DOB = this.quoteRequestData.Adult1Age; }
      if (this.product == "HBOOSTER") { this.insuredForm.value.member2DOB = this.quoteRequestData.Adult2Age; }
      if (this.isDisease == false) {
        this.ailmentArray1 = [];
      }
      if (this.isDisease2 == false) {
        this.ailmentArray2 = [];
      }
      if (this.isDisease3 == false) {
        this.ailmentArray3 = [];
      }
      if (this.isDisease4 == false) {
        this.ailmentArray4 = [];
      }
      if (this.isDisease5 == false) {
        this.ailmentArray5 = [];
      }
      let quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
      if (this.jsonData.NoOfAdults == '0') {

        this.memberObj = [{
          "MemberType": this.insuredForm.value.child1Cat,
          "TitleID": this.titleId3.id,
          "Name": this.insuredForm.value.child1Name,
          "RelationshipID": this.relationId3.RelationshipID,
          "RelationshipName": this.insuredForm.value.child1Rela,
          "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'),
          "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
          "Weight": this.insuredForm.value.child1Weight.toString(),
          "OtherDisease": "",
          "AddOnName": "",
          "AddOnName6": "",
          "isExistingPolicy": isxistingPolicy3,
          "TypeofPolicy": this.insuredForm.value.TypeofPolicy3,
          "PolicyDuration": this.insuredForm.value.PolicyDuration3,
          "InsuranceCompany": this.insuredForm.value.InsuranceCompany3,
          "SumInsured": this.insuredForm.value.SumInsured3,
          "Ailments": this.ailmentArray3
        }];
        resolve();
      } else if (this.jsonData.NoOfAdults == '1') {
        if (this.jsonData.NoOfKids == '0') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat,
            "TitleID": this.titleId1.id,
            "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID,
            "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'),
            "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(),
            "OtherDisease": "",
            "AddOnName": "",
            "AddOnName6": "",
            "isExistingPolicy": isxistingPolicy1,
            "TypeofPolicy": this.insuredForm.value.TypeofPolicy1,
            "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1,
            "SumInsured": this.insuredForm.value.SumInsured1,
            "Ailments": this.ailmentArray1
          }];
          if ((quoteRequest.AddOn6 == 'true' || quoteRequest.AddOn6 == true)) {
            this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
          }
          else {
            this.memberObj[0].AddOnName6 = '';
          }
          resolve();
        } else if (this.jsonData.NoOfKids == '1') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1
          }, {
            "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
            "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
            "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
            "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray3, "isExistingPolicy": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3
          }];
          if ((quoteRequest.AddOn6 == 'true' || quoteRequest.AddOn6 == true)) {
            this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
          }
          else {
            this.memberObj[0].AddOnName6 = '';
          }
          resolve();
        } else if (this.jsonData.NoOfKids == '2') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1
          }, {
            "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
            "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
            "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
            "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray3, "isExistingPolicy": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3
          }, {
            "MemberType": this.insuredForm.value.child2Cat, "TitleID": this.titleId4.id, "Name": this.insuredForm.value.child2Name,
            "RelationshipID": this.relationId4.RelationshipID, "RelationshipName": this.insuredForm.value.child2Rela,
            "DOB": moment(this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child2Feet + '.' + this.insuredForm.value.child2Inches,
            "Weight": this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray4, "isExistingPolicy": isxistingPolicy4, "TypeofPolicy": this.insuredForm.value.TypeofPolicy4, "PolicyDuration": this.insuredForm.value.PolicyDuration4,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany4, "SumInsured": this.insuredForm.value.SumInsured4
          }];
          if ((quoteRequest.AddOn6 == 'true' || quoteRequest.AddOn6 == true)) {
            this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
          }
          else {
            this.memberObj[0].AddOnName6 = '';
          }
          resolve();
        }
      } else if (this.jsonData.NoOfAdults == '2') {
        if (this.jsonData.NoOfKids == '0') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1
          }, {
            "MemberType": this.insuredForm.value.member2Cat, "TitleID": this.titleId2.id, "Name": this.insuredForm.value.member2Name,
            "RelationshipID": this.relationId2.RelationshipID, "RelationshipName": this.insuredForm.value.member2Rela,
            "DOB": moment(this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member2Feet + '.' + this.insuredForm.value.member2Inches,
            "Weight": this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray2, "isExistingPolicy": isxistingPolicy2, "TypeofPolicy": this.insuredForm.value.TypeofPolicy2, "PolicyDuration": this.insuredForm.value.PolicyDuration2,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany2, "SumInsured": this.insuredForm.value.SumInsured2
          }];
          if ((quoteRequest.AddOn6 == 'true' || quoteRequest.AddOn6 == true)) {
            if (quoteRequest.AddOn6Members.length > 1) {
              this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
              this.memberObj[1].AddOnName6 = quoteRequest.AddOn6Members[1].AddOnName;
            } else {
              this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
            }
          }
          else {
            this.memberObj[0].AddOnName6 = '';
          }
          resolve();
        }
        else if (this.jsonData.NoOfKids == '1') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1
          }, {
            "MemberType": this.insuredForm.value.member2Cat, "TitleID": this.titleId2.id, "Name": this.insuredForm.value.member2Name,
            "RelationshipID": this.relationId2.RelationshipID, "RelationshipName": this.insuredForm.value.member2Rela,
            "DOB": moment(this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member2Feet + '.' + this.insuredForm.value.member2Inches,
            "Weight": this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray2, "isExistingPolicy": isxistingPolicy2, "TypeofPolicy": this.insuredForm.value.TypeofPolicy2, "PolicyDuration": this.insuredForm.value.PolicyDuration2,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany2, "SumInsured": this.insuredForm.value.SumInsured2
          }, {
            "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
            "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
            "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
            "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray3, "isExistingPolicy": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3
          }];
          if ((quoteRequest.AddOn6 == 'true' || quoteRequest.AddOn6 == true)) {
            if (quoteRequest.AddOn6Members.length > 1) {
              this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
              this.memberObj[1].AddOnName6 = quoteRequest.AddOn6Members[1].AddOnName;
            } else {
              this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
            }
          }
          else {
            this.memberObj[0].AddOnName6 = '';
          }
          resolve();
        } else if (this.jsonData.NoOfKids == '2') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1
          }, {
            "MemberType": this.insuredForm.value.member2Cat, "TitleID": this.titleId2.id, "Name": this.insuredForm.value.member2Name,
            "RelationshipID": this.relationId2.RelationshipID, "RelationshipName": this.insuredForm.value.member2Rela,
            "DOB": moment(this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member2Feet + '.' + this.insuredForm.value.member2Inches,
            "Weight": this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray2, "isExistingPolicy": isxistingPolicy2, "TypeofPolicy": this.insuredForm.value.TypeofPolicy2, "PolicyDuration": this.insuredForm.value.PolicyDuration2,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany2, "SumInsured": this.insuredForm.value.SumInsured2
          }, {
            "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
            "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
            "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
            "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray3, "isExistingPolicy": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3
          }, {
            "MemberType": this.insuredForm.value.child2Cat, "TitleID": this.titleId4.id, "Name": this.insuredForm.value.child2Name,
            "RelationshipID": this.relationId4.RelationshipID, "RelationshipName": this.insuredForm.value.child2Rela,
            "DOB": moment(this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child2Feet + '.' + this.insuredForm.value.child2Inches,
            "Weight": this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray4, "isExistingPolicy": isxistingPolicy4, "TypeofPolicy": this.insuredForm.value.TypeofPolicy4, "PolicyDuration": this.insuredForm.value.PolicyDuration4,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany4, "SumInsured": this.insuredForm.value.SumInsured4
          }];
          if ((quoteRequest.AddOn6 == 'true' || quoteRequest.AddOn6 == true)) {
            if (quoteRequest.AddOn6Members.length > 1) {
              this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
              this.memberObj[1].AddOnName6 = quoteRequest.AddOn6Members[1].AddOnName;
            } else {
              this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
            }
          }
          else {
            this.memberObj[0].AddOnName6 = '';
          }
          resolve();
        } else if (this.jsonData.NoOfKids == '3') {
          this.memberObj = [{
            "MemberType": this.insuredForm.value.member1Cat, "TitleID": this.titleId1.id, "Name": this.insuredForm.value.member1Name,
            "RelationshipID": this.relationId1.RelationshipID, "RelationshipName": this.insuredForm.value.member1Rela,
            "DOB": moment(this.insuredForm.value.member1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member1Feet + '.' + this.insuredForm.value.member1Inches,
            "Weight": this.insuredForm.value.member1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray1, "isExistingPolicy": isxistingPolicy1, "TypeofPolicy": this.insuredForm.value.TypeofPolicy1, "PolicyDuration": this.insuredForm.value.PolicyDuration1,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany1, "SumInsured": this.insuredForm.value.SumInsured1
          }, {
            "MemberType": this.insuredForm.value.member2Cat, "TitleID": this.titleId2.id, "Name": this.insuredForm.value.member2Name,
            "RelationshipID": this.relationId2.RelationshipID, "RelationshipName": this.insuredForm.value.member2Rela,
            "DOB": moment(this.insuredForm.value.member2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.member2Feet + '.' + this.insuredForm.value.member2Inches,
            "Weight": this.insuredForm.value.member2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray2, "isExistingPolicy": isxistingPolicy2, "TypeofPolicy": this.insuredForm.value.TypeofPolicy2, "PolicyDuration": this.insuredForm.value.PolicyDuration2,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany2, "SumInsured": this.insuredForm.value.SumInsured2
          }, {
            "MemberType": this.insuredForm.value.child1Cat, "TitleID": this.titleId3.id, "Name": this.insuredForm.value.child1Name,
            "RelationshipID": this.relationId3.RelationshipID, "RelationshipName": this.insuredForm.value.child1Rela,
            "DOB": moment(this.insuredForm.value.child1DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child1Feet + '.' + this.insuredForm.value.child1Inches,
            "Weight": this.insuredForm.value.child1Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray3, "isExistingPolicy": isxistingPolicy3, "TypeofPolicy": this.insuredForm.value.TypeofPolicy3, "PolicyDuration": this.insuredForm.value.PolicyDuration3,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany3, "SumInsured": this.insuredForm.value.SumInsured3
          }, {
            "MemberType": this.insuredForm.value.child2Cat, "TitleID": this.titleId4.id, "Name": this.insuredForm.value.child2Name,
            "RelationshipID": this.relationId4.RelationshipID, "RelationshipName": this.insuredForm.value.child2Rela,
            "DOB": moment(this.insuredForm.value.child2DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child2Feet + '.' + this.insuredForm.value.child2Inches,
            "Weight": this.insuredForm.value.child2Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray4, "isExistingPolicy": isxistingPolicy4, "TypeofPolicy": this.insuredForm.value.TypeofPolicy4, "PolicyDuration": this.insuredForm.value.PolicyDuration4,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany4, "SumInsured": this.insuredForm.value.SumInsured4
          }, {
            "MemberType": this.insuredForm.value.child3Cat, "TitleID": this.titleId5.id, "Name": this.insuredForm.value.child3Name,
            "RelationshipID": this.relationId5.RelationshipID, "RelationshipName": this.insuredForm.value.child3Rela,
            "DOB": moment(this.insuredForm.value.child3DOB).format('DD-MMM-YYYY'), "Height": this.insuredForm.value.child3Feet + '.' + this.insuredForm.value.child3Inches,
            "Weight": this.insuredForm.value.child3Weight.toString(), "OtherDisease": "", "AddOnName": "", "AddOnName6": "",
            "Ailments": this.ailmentArray5, "isExistingPolicy": isxistingPolicy5, "TypeofPolicy": data.value.TypeofPolicy5, "PolicyDuration": this.insuredForm.value.PolicyDuration5,
            "InsuranceCompany": this.insuredForm.value.InsuranceCompany5, "SumInsured": this.insuredForm.value.SumInsured5
          }];
          if ((quoteRequest.AddOn6 == 'true' || quoteRequest.AddOn6 == true)) {
            if (quoteRequest.AddOn6Members.length > 1) {
              this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
              this.memberObj[1].AddOnName6 = quoteRequest.AddOn6Members[1].AddOnName;
            } else {
              this.memberObj[0].AddOnName6 = quoteRequest.AddOn6Members[0].AddOnName;
            }
          }
          else {
            this.memberObj[0].AddOnName6 = '';
          }
          resolve();
        }
      }
    });
  }



  SavePPAPInsuredData(formData: any) {
    let isFormValid = this.ValidationCheckPPAP(formData);
    if (isFormValid) {
      if (formData.value.member1Cat == 'Adult') {
        for (let i = 0; i < this.adultRelationArray.length; i++) {
          if (this.adultRelationArray[i].RelationshipName = formData.value.member1Rela) {
            localStorage.setItem('InsuredRealtionData', JSON.stringify(this.adultRelationArray[i]));
          }
        }
      } else {
        for (let i = 0; i < this.childRelationArray.length; i++) {
          if (this.childRelationArray[i].RelationshipName = formData.value.member1Rela) {
            localStorage.setItem('InsuredRealtionData', JSON.stringify(this.childRelationArray[i]));
          }
        }
      }
      this.router.navigateByUrl('health-proposal/propApplicant');
      localStorage.setItem('PPAPInsuredData', JSON.stringify(formData.value))
    }

  }

  validatepanCard(val) {
    if (new RegExp('[A-Z]{5}[0-9]{4}[A-Z]{1}').test(val)) {
      return true;
    }
    return false;
  }


  async presentToast(msg: any) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      cssClass: "toastscheme ",
      showCloseButton: true,
      closeButtonText: "OK",
      position: 'middle'
    });
    toast.present();
  }
  async presentAlert(message: any) {
    const alert = await this.alertController.create({
      header: 'ICICI Lombard',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  //-- Proposal Insured Code Ends --//
  //-- Proposal Basic Code Begins --///
  getJsonData(): Promise<any> {
    return new Promise((resolve) => {
      this.requestFromPremium = JSON.parse(localStorage.getItem('quoteRequest'));
      this.jsonData = this.requestFromPremium;
      console.log(this.requestFromPremium);
      resolve();
    });
  }

  getData(): Promise<any> {
    return new Promise((resolve) => {
      this.requestFromPremium = localStorage.getItem('requestForProp');
      resolve();
    });
  }

  xmlJson(data: any): Promise<any> {
    return new Promise((resolve) => {
      this.jsonData = this.xml2Json.xmlToJson(data);
      console.log(this.jsonData);
      resolve();
    });
  }

  createBasicForm() {
    this.basicPropForm = new FormGroup({
      productName: new FormControl(),
      subProductname: new FormControl(),
      noOfAdult: new FormControl(),
      eldestMembAge: new FormControl(),
      noOfChild: new FormControl(),
      policyTenure: new FormControl(),
      annualSumInsured: new FormControl(),
      subLimit: new FormControl(),
      JKProp: new FormControl(),
      addOnCover1: new FormControl(),
      addOnCover3: new FormControl(),
      addOnCover5: new FormControl(),
      addOnCover6: new FormControl(),
      ////// for HBooster
      softCopy: new FormControl(),
      addOnCover2: new FormControl(),
      deductible: new FormControl(),
      ///for PPAP
      premium: new FormControl(),
      sumInsured: new FormControl()
    });
  }

  patchValue(form: any) {
    console.log(form);
    if (form.ProductType == 'CHI') {
      let data = form; this.product = form.ProductType;
      if (data.IsJammuKashmir == 'true') {
        this.basicPropForm.patchValue({ 'JKProp': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'JKProp': 'No' });
      }
      if (data.AddOn1 == "true" || data.AddOn1 == true) {
        this.basicPropForm.patchValue({ 'addOnCover1': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover1': 'No' });
      }
      if (data.AddOn3 == "true" || data.AddOn3 == true) {
        this.basicPropForm.patchValue({ 'addOnCover3': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover3': 'No' });
      }
      if (data.AddOn5 == "true" || data.AddOn5 == true) {
        this.basicPropForm.patchValue({ 'addOnCover5': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover5': 'No' });
      }
      if (data.AddOn6 == true || data.AddOn6 == 'true') {
        this.basicPropForm.patchValue({ 'addOnCover6': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover6': 'No' });
      }
      if (!data.SubLimit) {
        this.basicPropForm.patchValue({ 'subLimit': 'None' });
        this.CHISubLimit = null;
      } else {
        this.basicPropForm.patchValue({ 'subLimit': data.SubLimit });
        this.CHISubLimit = data.SubLimit;
      }
      if (data.CHIProductName == "PROTECT") { this.basicPropForm.patchValue({ 'productName': 'HEALTH PROTECT' }); } else
        if (data.CHIProductName == "PROTECTPLUS") { this.basicPropForm.patchValue({ 'productName': 'HEALTH PROTECT PLUS' }); } else
          if (data.CHIProductName == "SMART") { this.basicPropForm.patchValue({ 'productName': 'HEALTH SMART' }); } else
            if (data.CHIProductName == "SMARTPLUS") { this.basicPropForm.patchValue({ 'productName': 'HEALTH SMART PLUS' }); } else
              if (data.CHIProductName == "iHealth") { this.basicPropForm.patchValue({ 'productName': data.CHIProductName }); }
      this.basicPropForm.patchValue({ 'noOfAdult': data.NoOfAdults });
      this.basicPropForm.patchValue({ 'eldestMembAge': data.AgeGroup });
      this.basicPropForm.patchValue({ 'noOfChild': data.NoOfKids });
      this.basicPropForm.patchValue({ 'policyTenure': data.YearPremium });
      let anuualInsured = JSON.parse(localStorage.getItem('CHIAnnualSumInsured'));
      this.basicPropForm.patchValue({ 'annualSumInsured': anuualInsured });
      // this.basicPropForm.patchValue({'subLimit':data.SubLimit});
      if (parseInt(data.NoOfAdults) > 0 && parseInt(data.NoOfKids) <= 0) {
        this.showSegment = 'adult1';
      }
      if (parseInt(data.NoOfAdults) <= 0 && parseInt(data.NoOfKids) > 0) {
        this.showSegment = 'child1';
      }
      if (parseInt(data.NoOfAdults) > 0 && parseInt(data.NoOfKids) > 0) {
        this.showSegment = 'adult1';
      }
    } else if (form.ProductType == 'ASP') {
      let data = form; this.product = form.ProductType;
      if (data.IsJammuKashmir == 'true') {
        this.basicPropForm.patchValue({ 'JKProp': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'JKProp': 'No' });
      }
      if (data.AddOn1 == "true" || data.AddOn1 == true) {
        this.basicPropForm.patchValue({ 'addOnCover1': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover1': 'No' });
      }
      if (data.AddOn3 == "true" || data.AddOn3 == true) {
        this.basicPropForm.patchValue({ 'addOnCover3': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover3': 'No' });
      }
      if (data.AddOn5 == "true" || data.AddOn5 == true) {
        this.basicPropForm.patchValue({ 'addOnCover5': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover5': 'No' });
      }
      if (data.AddOn6 == true || data.AddOn6 == 'true') {
        this.basicPropForm.patchValue({ 'addOnCover6': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover6': 'No' });
      }
      if (!data.SubLimit) {
        this.basicPropForm.patchValue({ 'subLimit': 'None' });
        this.CHISubLimit = null;
      } else {
        this.basicPropForm.patchValue({ 'subLimit': data.SubLimit });
        this.CHISubLimit = data.SubLimit;
      }
      this.basicPropForm.patchValue({ 'productName': 'HEALTH_AROGYA_SANJEEVANI' });
      this.basicPropForm.patchValue({ 'noOfAdult': data.NoOfAdults });
      this.basicPropForm.patchValue({ 'eldestMembAge': data.AgeGroup });
      this.basicPropForm.patchValue({ 'noOfChild': data.NoOfKids });
      this.basicPropForm.patchValue({ 'policyTenure': data.YearPremium });
      let anuualInsured = JSON.parse(localStorage.getItem('CHIAnnualSumInsured'));
      this.basicPropForm.patchValue({ 'annualSumInsured': anuualInsured });
      if (parseInt(data.NoOfAdults) > 0 && parseInt(data.NoOfKids) <= 0) {
        this.showSegment = 'adult1';
      }
      if (parseInt(data.NoOfAdults) <= 0 && parseInt(data.NoOfKids) > 0) {
        this.showSegment = 'child1';
      }
      if (parseInt(data.NoOfAdults) > 0 && parseInt(data.NoOfKids) > 0) {
        this.showSegment = 'adult1';
      }
    }  
    else if (form.ProductType == 'HBOOSTER') {
      console.log("Health Booster");
      let data = form; this.product = form.ProductType;
      if (data.IsJammuKashmir == 'true') {
        this.basicPropForm.patchValue({ 'JKProp': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'JKProp': 'No' });
      }
      if (data.AddOn1 == 'true' || data.AddOn1 == true) {
        this.basicPropForm.patchValue({ 'addOnCover1': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover1': 'No' });
      }
      if (data.AddOn2 == 'true' || data.AddOn2 == true) {
        this.basicPropForm.patchValue({ 'addOnCover2': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover2': 'No' });
      }
      if (data.AddOn3 == 'true' || data.AddOn3 == true) {
        this.basicPropForm.patchValue({ 'addOnCover3': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'addOnCover3': 'No' });
      }
      if (data.SoftLessCopy == 'true') {
        this.basicPropForm.patchValue({ 'softCopy': 'Yes' });
      } else {
        this.basicPropForm.patchValue({ 'softCopy': 'No' });
      }
      this.basicPropForm.patchValue({ 'productName': 'Health Booster' });
      if (data.ProductName == 'HBOOSTER_TOPUP') {
        this.basicPropForm.patchValue({ 'subProductname': 'Health Booster-Top Up Plan' });
        this.basicPropForm.patchValue({ 'productName': 'Top Up Plan' });
      }
      else {
        this.basicPropForm.patchValue({ 'subProductname': 'Health Booster-Super Top Up Plan' });
        this.basicPropForm.patchValue({ 'productName': 'Super Top Up Plan' });
      }

      this.basicPropForm.patchValue({ 'noOfAdult': data.NoOfAdults });
      this.basicPropForm.patchValue({ 'eldestMembAge': data.Adult1Age });
      this.basicPropForm.patchValue({ 'noOfChild': data.NoOfKids });
      this.basicPropForm.patchValue({ 'policyTenure': this.responseBody.HealthDetails[0].Tenure });
      this.basicPropForm.patchValue({ 'annualSumInsured': data.SumInsured });
      this.basicPropForm.patchValue({ 'deductible': data.VolDedutible });
      if (parseInt(data.NoOfAdults) > 0 && parseInt(data.NoOfKids) <= 0) {
        this.showSegment = 'adult1';
      }
      if (parseInt(data.NoOfAdults) <= 0 && parseInt(data.NoOfKids) > 0) {
        this.showSegment = 'child1';
      }
      if (parseInt(data.NoOfAdults) > 0 && parseInt(data.NoOfKids) > 0) {
        this.showSegment = 'adult1';
      }
    }
    else {
      let data = form; let tempName;
      this.product = form.ProductType;
      let annual = localStorage.getItem('AnuualIncome');
      let sum = localStorage.getItem('SumInsured');
      let response = JSON.parse(localStorage.getItem('quoteResponse'));
      if (data.SubProduct == 'PP') { tempName = 'Lump Sum Payment Option'; }
      else if (data.SubProduct == 'PPA') { tempName = 'Monthly Payment Option'; }
      else if (data.SubProduct == 'PPC') { tempName = 'Lump Sum Payment Option With Monthly Payout'; }
      this.basicPropForm.patchValue({ 'productName': data.ProductType });
      let tempTenure = parseInt(response.HealthDetails[0].Tenure);
      if (tempTenure == 1) { this.basicPropForm.patchValue({ 'policyTenure': response.HealthDetails[0].Tenure + ' year(s)' }); }
      else if (tempTenure > 1) { this.basicPropForm.patchValue({ 'policyTenure': response.HealthDetails[0].Tenure + ' years' }); }
      let tempASum = parseInt(annual);
      if (tempASum == 1) { this.basicPropForm.patchValue({ 'annualSumInsured': '₹ ' + annual + ' Lakh(s)' }); }
      else if (tempASum > 1) { this.basicPropForm.patchValue({ 'annualSumInsured': '₹ ' + annual + ' Lacs' }); }
      else { this.basicPropForm.patchValue({ 'annualSumInsured': '₹ ' + annual + ' Lacs' }); }
      this.basicPropForm.patchValue({ 'subProductname': tempName });
      this.basicPropForm.patchValue({ 'sumInsured': '₹ ' + sum });
      this.basicPropForm.patchValue({ 'premium': '₹ ' + response.HealthDetails[0].TotalPremium });
    }
  }
  //-- Proposal Basic Code Ends --///
  async showLoading() {
    this.loading = await this.loadingCtrl.create({
      message: 'Loading',
    });
    return await this.loading.present();
  }

  async messageAlert(message: any) {
    const alert = await this.alertController.create({
      header: 'ICICI Lombard',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  home(ev: any) {
    console.log("Home", ev);
    this.cs.goToHome();
  }

  showBasicDetails(ev: any) {
    this.isBasicDetails = !this.isBasicDetails;
    // if (this.isBasicDetails) {
    //   var imgUrl = './assets/images/minus.png';
    //   document.getElementById("basicDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("basicDetails").style.backgroundRepeat = 'no-repeat';
    //   document.getElementById("basicDetailsBody").style.display = 'block';
    // } else {
    //   var imgUrl = './assets/images/plus.png';
    //   document.getElementById("basicDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("basicDetails").style.backgroundRepeat = 'no-repeat';
    //   document.getElementById("basicDetailsBody").style.display = 'none';
    // }
  }

  showInsuredDetails(ev: any) {
    this.isInsuredDetails = !this.isInsuredDetails;
    if (this.isInsuredDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
      document.getElementById("insuredDetailsBody").style.display = 'block';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
      document.getElementById("insuredDetailsBody").style.display = 'none';
    }
  }

  showApplicantDetails(ev: any) {
    let isApplicantFeildsValid = this.validatedInsuredMembers();
    if (isApplicantFeildsValid == true) {
      this.isApplicantDetails = !this.isApplicantDetails;
    }

    if (this.isApplicantDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
    }
  }

  changeCityName(ev) {
    this.applicantForm.patchValue({ 'applCity': ev.target.value });
    let cityListForId = this.appCityList.filter(city => city.CityName == ev.target.value)
    this.corrCityId = cityListForId[0].CityID;
  }
  changeCityCodePer(ev) {
    this.applicantForm.patchValue({ 'permCity': ev.target.value });
    let cityListForIdPer = this.appCityListPer.filter(city => city.CityName == ev.target.value)
    this.corrCityIdPer = cityListForIdPer[0].CityID;
  }

  SPConditionCheck(): Promise<any> {
    return new Promise((resolve) => {

      this.cs.SPConditionCheck().then((res: any) => {
        if (res.StatusCode != 0 && res.Parent_IM_ID != "" && res.INTERMEDIARY_CATEGORY_TYPE == "CORPORATE AGENT") {
          this.ParentIMID = res.Parent_IM_ID;
          this.isSPFlagValid = true;
          resolve();
        }
        else {
          this.isSPFlagValid = false;
          resolve();
        }
      }).catch((err) => {
        console.log(err)
        return;
      });
    });
  }

  alphaOnly(event: any): boolean {
    const key = event.keyCode;
    return ((key >= 65 && key <= 90) || key == 8 || key == 32);
  }

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
