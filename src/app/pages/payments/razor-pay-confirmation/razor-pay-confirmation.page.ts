import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import * as moment from 'moment';
import { AlertController, ToastController } from '@ionic/angular';
import { resolve } from 'url';

@Component({
  selector: 'app-razor-pay-confirmation',
  templateUrl: './razor-pay-confirmation.page.html',
  styleUrls: ['./razor-pay-confirmation.page.scss'],
})
export class RazorPayConfirmationPage implements OnInit {
  paymentDetails = false; showCHI = false; showHB = false; showPPAP = false; travelConf = false; healthConf = false;
  showEmailPrompt = false; email: any; successMsg = false; emailRes: any; error: any; isError = false;
  quoteRequest: any; quoteResponse: any; travelPlan: any; proposalResponse: any; customerRequest: any;
  PIDData: any; paymentMode: any; proposalRequest: any; commonPayRes: any; paymentConfRes: any;
  renewalProduct: any; heathRenewal: boolean = false; isRenewal: any; razorPayData: any;
  razorPayForm: FormGroup; travelPayConfForm: FormGroup; CHIPayConfForm: FormGroup; HBPayConfForm: FormGroup;
  PPAPPayConfForm: FormGroup;
  pendingPaymentData: any;
  travelPendingConf: boolean;
  healthPendingConf: boolean;
  showhospifund=false;HospifundPayConfForm:FormGroup;
  authData:any;
  showUnderWritter: boolean=false;showASP:boolean=false;
  ASPPayConfForm: FormGroup;

  constructor(public cs: CommonService, public alertCtrl: AlertController, public toastCtrl: ToastController) { }

  ngOnInit() {
    this.createTravelForm();
    this.createCHIForm();
    this.createHBForm();
    this.createPPAPForm();
    this.createHospiForm();
    this.createASPForm();
    this.isRenewal = localStorage.getItem('isRenewal');

    if (localStorage.getItem('pendingNavigation') == 'true') {
      this.getPendingPaymentData().then(() => {
        this.paymantConfirmation(this.razorPayData).then(() => {
          this.dataBindTravel();
          this.dataBindCHI();
          this.dataBindHB();
          this.dataBindPPAP();
          this.dataBindHospifund();
          this.dataBindASP();
        });
      });
    }
    else {
      this.getData().then(() => {
        this.paymantConfirmation(this.razorPayData).then(() => {
          this.dataBindTravel();
          this.dataBindCHI();
          this.dataBindHB();
          this.dataBindPPAP();
          this.dataBindHospifund();
          this.dataBindASP();
        });
      });
    }

  }

  getPendingPaymentData(): Promise<any> {
    return new Promise((resolve: any) => {
      this.travelConf = false;
      this.healthConf = false;
      this.razorPayData = JSON.parse(localStorage.getItem('razorPayData'));
      this.pendingPaymentData = JSON.parse(localStorage.getItem('paymentData'));
      console.log('this.pendingPaymentData.PolicySubType',this.pendingPaymentData[0].PolicySubType);

      if (localStorage.getItem('isHealthProposal') != 'true') {
        this.travelPendingConf = true; this.healthPendingConf = false;
      } else {
        this.travelPendingConf = false; this.healthPendingConf = true;
      }
      resolve();
    

  })
}

  getData(): Promise<any> {
    return new Promise((resolve: any) => {
      this.travelPendingConf = false;
      this.healthPendingConf = false;
      this.quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
      this.quoteResponse = JSON.parse(localStorage.getItem('quoteResponse'));
      this.travelPlan = JSON.parse(localStorage.getItem('travelPlan'));
      this.proposalResponse = JSON.parse(localStorage.getItem('proposalResponse'));
      this.proposalRequest = JSON.parse(localStorage.getItem('proposalRequest'));
      this.renewalProduct = JSON.parse(localStorage.getItem('product'));
      this.customerRequest = JSON.parse(localStorage.getItem('customerRequest'));
      this.PIDData = JSON.parse(localStorage.getItem('PIDData'));
      this.paymentMode = JSON.parse(localStorage.getItem('paymentMode'));
      this.commonPayRes = JSON.parse(localStorage.getItem('commonPayRes'));
      this.paymentConfRes = JSON.parse(localStorage.getItem('paymentConfRes'));
      this.razorPayData = JSON.parse(localStorage.getItem('razorPayData'));
      if (this.proposalRequest.ProductType == 'CHI') {
        this.showCHI = true;
      }
      else if (this.proposalRequest.ProductType == 'HBOOSTER') {
        this.showHB = true;
      }
      else if (this.proposalRequest.ProductType == 'PPAP') {
        this.showPPAP = true;
      }
      else if (this.proposalRequest.ProductType == 'Hospifund') {
        this.showhospifund = true;
      }
      else if (this.proposalRequest.ProductType == 'ASP') {
        this.showASP = true;
      }
      if (this.isRenewal == 'false') {
        if (this.proposalRequest.Product == 'Travel') {
          this.travelConf = true; this.healthConf = false; this.heathRenewal = false;
        } else {
          this.travelConf = false; this.healthConf = true; this.heathRenewal = false;
        }
      }
      else if (this.isRenewal == 'true') {
        if (this.renewalProduct.productName == 'CHI' || this.renewalProduct.productName == 'Health Booster' || this.renewalProduct.productName == 'PPAP') {
          this.travelConf = false; this.healthConf = false; this.heathRenewal = true;
        }
      }
      resolve();
    });
  }


  createTravelForm() {
    this.travelPayConfForm = new FormGroup({
      policyNumber: new FormControl(),
      premium: new FormControl(),
      policyStartDate: new FormControl(),
      policyEndDate: new FormControl()
    });
  }

  createCHIForm() {
    this.CHIPayConfForm = new FormGroup({
      policyNumber: new FormControl(),
      proposalNumber: new FormControl(),
      premium: new FormControl(),
      policyStartDate: new FormControl(),
      policyEndDate: new FormControl()
    });
  }

  createHBForm() {
    this.HBPayConfForm = new FormGroup({
      policyNumber: new FormControl(),
      proposalNumber: new FormControl(),
      premium: new FormControl(),
      policyStartDate: new FormControl(),
      policyEndDate: new FormControl()
    });
  }

  createPPAPForm() {
    this.PPAPPayConfForm = new FormGroup({
      policyNumber: new FormControl(),
      proposalNumber: new FormControl(),
      premium: new FormControl(),
      policyStartDate: new FormControl(),
      policyEndDate: new FormControl()
    });
  }
  createHospiForm(){
    this.HospifundPayConfForm = new FormGroup({
      policyNumber: new FormControl(),
      proposalNumber: new FormControl(),
      premium: new FormControl(),
      policyStartDate: new FormControl(),
      policyEndDate: new FormControl()
    });
  }
  createASPForm() {
    this.ASPPayConfForm = new FormGroup({
      policyNumber: new FormControl(),
      proposalNumber: new FormControl(),
      premium: new FormControl(),
      policyStartDate: new FormControl(),
      policyEndDate: new FormControl()
    });
  }

  paymantConfirmation(data: any): Promise<any> {
    return new Promise((resolve) => {
      let msg: any;
      this.cs.getUserData('/api/Payment/CommonPaymentConfirmationByPID?EPaymentID=' + data.epaymentId).then((res: any) => {
        console.log(res);
        this.paymentConfRes = res.ConfirmPolicy[0];
        if (res.Message) {
          msg = res.Message;
          this.presentAlert(msg);
          localStorage.setItem('isNewPIDReceivedData', 'false');
          resolve();
        } else {
          if (res.PaymentStatus == 'Successful Payment') {
            console.log("PAyment succ");
            msg = 'Payment is Successful. Your Transaction ID is ' + res.TransactionID;
            this.presentAlert(msg);
            if(this.paymentConfRes.PolicyStatus.includes('Pending'))
            {
              this.showUnderWritter=true;
            }
            else{
              this.showUnderWritter=false;
            }
          } else if (res.PaymentStatus == 'Pending Payment') {
            msg = 'Your Cheque details saved successfully';
            this.presentAlert(msg);
          } else if (res.PaymentStatus == 'Failed Payment') {
            if (res.GatewayError != '' || res.GatewayError != null) {
              msg = 'Payment is Unsuccessful. Message: ' + data.GatewayError;
              this.presentAlert(msg);
            } else {
              msg = 'Payment is Unsuccessful. Please Try again';
              this.presentAlert(msg);
            }
          } else {
            msg = 'Payment is Cancelled. Please Try again';
            this.presentAlert(msg);
          }
          localStorage.setItem('isNewPIDReceivedData', 'false');
          resolve();
        }
      });
    });

  }

  dataBindTravel() {
    console.log(this.paymentConfRes);
    this.travelPayConfForm.patchValue({ 'policyNumber': this.paymentConfRes.PolicyNumber });
    this.travelPayConfForm.patchValue({ 'premium': this.paymentConfRes.Premium });
    this.travelPayConfForm.patchValue({ 'policyStartDate': moment(this.paymentConfRes.PolicyStartDate).format('YYYY-MM-DD') });
    this.travelPayConfForm.patchValue({ 'policyEndDate': moment(this.paymentConfRes.PolicyEndDate).format('YYYY-MM-DD') });
  }

  dataBindCHI() {
    this.CHIPayConfForm.patchValue({ 'policyNumber': this.paymentConfRes.PolicyNumber });
    this.CHIPayConfForm.patchValue({ 'proposalNumber': this.paymentConfRes.ProposalNumber });
    this.CHIPayConfForm.patchValue({ 'premium': this.paymentConfRes.Premium });
    this.CHIPayConfForm.patchValue({ 'policyStartDate': moment(this.paymentConfRes.PolicyStartDate).format('YYYY-MM-DD') });
    this.CHIPayConfForm.patchValue({ 'policyEndDate': moment(this.paymentConfRes.PolicyEndDate).format('YYYY-MM-DD') });
  }

  dataBindHB() {
    this.HBPayConfForm.patchValue({ 'policyNumber': this.paymentConfRes.PolicyNumber });
    this.HBPayConfForm.patchValue({ 'proposalNumber': this.paymentConfRes.ProposalNumber });
    this.HBPayConfForm.patchValue({ 'premium': this.paymentConfRes.Premium });
    this.HBPayConfForm.patchValue({ 'policyStartDate': moment(this.paymentConfRes.PolicyStartDate).format('YYYY-MM-DD') });
    this.HBPayConfForm.patchValue({ 'policyEndDate': moment(this.paymentConfRes.PolicyEndDate).format('YYYY-MM-DD') });
  }

  dataBindPPAP() {
    this.PPAPPayConfForm.patchValue({ 'policyNumber': this.paymentConfRes.PolicyNumber });
    this.PPAPPayConfForm.patchValue({ 'proposalNumber': this.paymentConfRes.ProposalNumber });
    this.PPAPPayConfForm.patchValue({ 'premium': this.paymentConfRes.Premium });
    this.PPAPPayConfForm.patchValue({ 'policyStartDate': moment(this.paymentConfRes.PolicyStartDate).format('YYYY-MM-DD') });
    this.PPAPPayConfForm.patchValue({ 'policyEndDate': moment(this.paymentConfRes.PolicyEndDate).format('YYYY-MM-DD') });
  }

  dataBindHospifund(){
    this.HospifundPayConfForm.patchValue({'policyNumber':this.paymentConfRes.PolicyNumber});
    this.HospifundPayConfForm.patchValue({'proposalNumber':this.paymentConfRes.ProposalNumber});
    this.HospifundPayConfForm.patchValue({'premium':this.paymentConfRes.Premium});
    this.HospifundPayConfForm.patchValue({'policyStartDate':moment(this.paymentConfRes.PolicyStartDate).format('YYYY-MM-DD')});
    this.HospifundPayConfForm.patchValue({'policyEndDate':moment(this.paymentConfRes.PolicyEndDate).format('YYYY-MM-DD')});
  }

  dataBindASP() {
    this.ASPPayConfForm.patchValue({ 'policyNumber': this.paymentConfRes.PolicyNumber });
    this.ASPPayConfForm.patchValue({ 'proposalNumber': this.paymentConfRes.ProposalNumber });
    this.ASPPayConfForm.patchValue({ 'premium': this.paymentConfRes.Premium });
    this.ASPPayConfForm.patchValue({ 'policyStartDate': moment(this.paymentConfRes.PolicyStartDate).format('YYYY-MM-DD') });
    this.ASPPayConfForm.patchValue({ 'policyEndDate': moment(this.paymentConfRes.PolicyEndDate).format('YYYY-MM-DD') });
  }

  async presentAlert(msg:any) {
    const alert = await this.alertCtrl.create({
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }





  showDetails(ev: any) {
    this.paymentDetails = !this.paymentDetails;
    if (this.paymentDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("details").style.background = "url(" + imgUrl + ")";
      document.getElementById("details").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("details").style.background = "url(" + imgUrl + ")";
      document.getElementById("details").style.backgroundRepeat = 'no-repeat';
    }
  }

  showTabs(data: any) {
    // console.log(data);
    if (data == 'CHI') {
      this.showCHI = true; this.showHB = false; this.showPPAP = false; this.showhospifund = false;this.showASP=false;
    } else if (data == 'HB') {
      this.showCHI = false; this.showHB = true; this.showPPAP = false; this.showhospifund = false;this.showASP=false;
    } else if (data == 'PPAP') {
      this.showCHI = false; this.showHB = false; this.showPPAP = true; this.showhospifund = false;this.showASP=false;
    }
    else if(data == 'Hospifund') {
      this.showCHI = false; this.showHB = false; this.showPPAP = false; this.showhospifund = true;this.showASP=false;
    }
    else {
      this.showCHI = false; this.showHB = false; this.showPPAP = false; this.showhospifund = false;this.showASP=true;
    }
  }

  showEmail(ev: any) {
    // this.showEmailPrompt = !this.showEmailPrompt;
    console.log(document.getElementById("myNav4"));
    document.getElementById("myNav4").style.height = "100%";
  }

  close(ev: any) {
    document.getElementById("myNav4").style.height = "0%";
    document.getElementById("myNav5").style.height = "0%";
  }

  downloadPdf(ev: any) {
    let downloadURL;
    if (localStorage.getItem('pendingNavigation') != 'true'){
      if(this.proposalRequest.ProductType == 'Hospifund')
      {
        this.getToken();
      }
      else{
        downloadURL = this.cs.pdfDownload('POLICY', this.paymentConfRes.EPolicyID);
        this.cs.save(downloadURL, this.paymentConfRes.ProposalNumber + ".pdf");
      }
    }else{
      downloadURL = this.cs.pdfDownload('POLICY', this.paymentConfRes.EPolicyID);
      this.cs.save(downloadURL, this.paymentConfRes.ProposalNumber + ".pdf");
    }
    // window.open(downloadURL, "_blank");
    this.presentToast('Your pdf will be downloaded');
  }
  getToken()
  {
    this.cs.getAuthorizationDataforPDF().then(res => {
      this.authData = res;
      this.generatePDF();
    }, err => {
      this.presentToast('Sorry Something went wrong');
    });
  }
  generatePDF()
  {
    this.cs.showLoader=true;
    this.cs.generatePDF(this.paymentConfRes.PolicyDetails[0].PolicyNumber,this.authData).then((res:any) => {
      console.log(res);
      var fileName = this.paymentConfRes.PolicyDetails[0].PolicyNumber+ '.pdf';
      this.cs.saveBlob(res, fileName);
      this.cs.showLoader=false;
    }, err => {
        this.presentToast('Sorry something went wrong');
        this.cs.showLoader=false;
    }); 
  }

  sendEmail(ev: any) {
    let body;
    console.log(this.email);
    if (localStorage.getItem('pendingNavigation') != 'true'){
      body = {
        "EPolicyID": this.paymentConfRes.PolicyDetails[0].EPolicyID,
        // "EPolicyID": 'ABCDE',
        "PDFType": "POLICY",
        "EmailID": this.email
      }
    }else{
      body = {
        "EPolicyID": this.paymentConfRes.EPolicyID,
        // "EPolicyID": 'ABCDE',
        "PDFType": "POLICY",
        "EmailID": this.email
      }
    }
    
    let str = JSON.stringify(body);
    this.cs.showLoader = true;
    this.cs.postWithParams('/api/file/sendemailpdf', str).then((res: any) => {
      console.log(res);
      this.emailRes = res.Message;
      if (this.emailRes.indexOf('successfully emailed') > -1) {
        document.getElementById("myNav4").style.height = "0%";
        document.getElementById("myNav5").style.height = "100%";
        this.successMsg = true; this.isError = false;
        this.cs.showLoader = false;
      } else {
        this.cs.showLoader = false;
        this.successMsg = false; this.isError = false;
        document.getElementById("myNav4").style.height = "0%";
        document.getElementById("myNav5").style.height = "100%";
      }
    }).catch((err: any) => {
      console.log("error", err.statusText);
      if(err.error.Message){
        this.presentAlert(err.error.Message);
        this.isError = true;
        this.error = err.statusText;
        this.successMsg = false;
        this.cs.showLoader = false;
        document.getElementById("myNav4").style.height = "0%";
        document.getElementById("myNav5").style.height = "100%";
        return;
      }
     
    });
  }

  async presentToast(msg: any) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  home(ev: any) {
    console.log("Home", ev);
    this.cs.goToHome();
  }


}
