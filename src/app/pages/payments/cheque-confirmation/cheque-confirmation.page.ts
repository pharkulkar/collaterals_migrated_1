import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-cheque-confirmation',
  templateUrl: './cheque-confirmation.page.html',
  styleUrls: ['./cheque-confirmation.page.scss'],
})
export class ChequeConfirmationPage implements OnInit {

  chequeDetails = false; quoteRequest:any; quoteResponse:any; travelPlan:any; proposalResponse:any;
  proposalRequest:any; customerRequest:any; paymentModeReq:any; commonPayRes:any; paymentConfRes:any;
  chequeBankName: any;
  chequeConfForm: FormGroup;authData:any;
  constructor(public cs:CommonService, public toastCtrl: ToastController) { }

  ngOnInit() {
    this.createChequeForm();
    this.getData().then(() => {
      this.bindChequeForm();
    })
  }

  getData():Promise<any>{
    return new Promise((resolve:any) => {
      this.quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
      this.quoteResponse = JSON.parse(localStorage.getItem('quoteResponse'));
      this.travelPlan = JSON.parse(localStorage.getItem('travelPlan'));
      this.proposalResponse = JSON.parse(localStorage.getItem('proposalResponse'));
      this.proposalRequest = JSON.parse(localStorage.getItem('proposalRequest'));
      this.customerRequest = JSON.parse(localStorage.getItem('customerRequest'));
      this.paymentModeReq = JSON.parse(localStorage.getItem('paymentModeReq'));
      this.commonPayRes = JSON.parse(localStorage.getItem('commonPayRes'));
      this.paymentConfRes = JSON.parse(localStorage.getItem('paymentConfRes'));
      this.chequeBankName = JSON.parse(localStorage.getItem('chequeBank'));
      console.log("IMP",this.quoteRequest);
      console.log(this.quoteResponse);
      console.log(this.proposalResponse);
      console.log(this.travelPlan);
      console.log(this.proposalRequest);
      console.log(this.customerRequest);
      console.log(this.paymentModeReq);
      console.log(this.commonPayRes);
      console.log(this.paymentConfRes);
      resolve();
    });
  }

  createChequeForm(){
    this.chequeConfForm = new FormGroup({
      proposalNumber: new FormControl(),
      pfProposalNumber: new FormControl(),
      chequeNumber: new FormControl(),
      bank: new FormControl(),
      chequeDate: new FormControl(),
      chequeAmount: new FormControl()
    })
  }

  bindChequeForm(){
    this.chequeConfForm = new FormGroup({
      proposalNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].ProposalNumber),
      pfProposalNumber: new FormControl(this.proposalResponse.SavePolicy[0].PF_ProsalNumber),
      chequeNumber: new FormControl(this.paymentModeReq.ChequeDetails.InstrumentNo),
      bank: new FormControl(this.chequeBankName),
      chequeDate: new FormControl(moment(this.paymentModeReq.ChequeDetails.InstrumentDate).format('YYYY-MM-DD')),
      chequeAmount: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium)
    })
  }

  showChequeDetails(ev:any){
    this.chequeDetails = !this.chequeDetails;
    if (this.chequeDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("details").style.background = "url(" + imgUrl + ")";
      document.getElementById("details").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("details").style.background = "url(" + imgUrl + ")";
      document.getElementById("details").style.backgroundRepeat = 'no-repeat';
    }
  }

  downloadPdf(ev:any){
    if(this.proposalRequest.ProductType == 'Hospifund')
    {
      this.getToken();
    }
    else{
      let downloadURL = 'http://cldiliptrapp03.cloudapp.net:9006/mobileapi/api/file/DownloadPDF?type=PAYINSLIP&value=JY9RJLS%2fY6S7UeFJOWxaNQ%3d%3d';
      this.cs.save(downloadURL, this.paymentConfRes.EPFPaymentID+".pdf");
      this.presentToast('Your pdf will be downloaded');
    }
  
  }

  getToken()
  {
    this.cs.getAuthorizationDataforPDF().then(res => {
      this.authData = res;
      this.generatePDF();
    }, err => {
      this.presentToast('Sorry Something went wrong');
    });
  }
  generatePDF()
  {
    this.cs.showLoader=true;
    this.cs.generatePDF(this.paymentConfRes.PolicyDetails[0].PolicyNumber,this.authData).then((res:any) => {
      console.log(res);
      var fileName = this.paymentConfRes.PolicyDetails[0].PolicyNumber+ '.pdf';
      this.cs.saveBlob(res, fileName);
      this.cs.showLoader=false;
    }, err => {
        this.presentToast('Sorry something went wrong');
        this.cs.showLoader=false;
    }); 
  }

  async presentToast(msg:any) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  home(ev:any){
    console.log("Home", ev);
    this.cs.goToHome();
  }

}
