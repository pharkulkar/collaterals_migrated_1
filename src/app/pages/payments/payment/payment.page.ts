import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingService } from 'src/app/services/loading.service';
import { config } from 'src/app/config.properties';
import { NotificationService } from 'src/app/services/notification.service';
import { Modal } from 'materialize-css';
import { Observable } from 'rxjs';

declare var Razorpay: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  paymentMode: any; options: any; commonDataRes: any; response: any; isCustomer = false; travelRequest: any; newPID: any;
  travelResponse: any; planDetails: any; travelProp: any; travelPropReq: any; createCustReq: any; showCustInt = false; isNewPID = false;
  pidCustId: any; pidPFCustId: any; payLaterRes: any; payLaterConfRes: any; custType = 'Customer'; renewalProduct: any; isRenewal: any;
  isWalletShow = true; isWalletShowAtPID = true; isEMISHow = true; isEMISHowAtPID = true; newPIDCustId: any;
  custEmail: any; custMobile: any; custName: any; childPageDetails: any; planFormURL: any;
  pendingNavigation: any;
  pendingPaymentData: any;
  totalPendingAmount: any;
  isHealthProposal: any;
  PayerType: any;
  IspendingRazorPay: any;
  policyIDS: any = [];
  policyProductType: string;
  policyType: any;
  constructor(public cs: CommonService, public route: ActivatedRoute, public httpClient: HttpClient, public router: Router, public loading: LoadingService, public notificationService: NotificationService) { }



  ngOnInit() {

    localStorage.setItem('isNewPIDReceivedData', 'false');
    this.ReceiveMessage = this.ReceiveMessage.bind(this);
    if (!window['postMessage']) {
      alert("oh crap");
    } else {
      if (window.addEventListener) {
        window.addEventListener("message", this.ReceiveMessage, false);
      } else {
        console.log("onmessage", this.ReceiveMessage);
        // window.attachEvent("onmessage", this.ReceiveMessage);
      }
    }

    if (localStorage.getItem('isPendingPage') == 'true') {
      this.pendingNavigation = true;
      this.travelProp = false;
      this.isHealthProposal = localStorage.getItem('isHealthProposal');
      // this.isHealthProposal == 'true' ?  this.showCustInt = false :  this.showCustInt = true;
      if (this.isHealthProposal == 'true') {
        this.showCustInt = false;
        this.policyType = 'Health';
      } else {
        this.showCustInt = true;
        this.policyType = 'Travel'
      }
      localStorage.setItem('pendingNavigation', this.pendingNavigation);
      localStorage.setItem('isHealthProposal', this.isHealthProposal);
      if (this.isCustomer) {
        this.custType = 'Intermediary';
        localStorage.setItem('custType', JSON.stringify(this.custType));
      } else {
        this.custType = 'Customer';
        localStorage.setItem('custType', JSON.stringify(this.custType));
      }
      this.getPendingPaymentData();

    }
    else {
      this.pendingNavigation = false;
      this.getNewPID().then(() => {
        this.getData();
      });
    }

    // this.route.queryParams.subscribe(params => {
    //   if (params["isPendingPage"]) {
    //     this.pendingNavigation = true;
    //     this.travelProp = false;
    //     this.isHealthProposal = params["isHealthProposal"];
    //     this.isHealthProposal == 'true' ?  this.showCustInt = false :  this.showCustInt = true;
    //     localStorage.setItem('pendingNavigation', this.pendingNavigation);
    //     localStorage.setItem('isHealthProposal',  this.isHealthProposal);
    //     this.getPendingPaymentData();
    //   } else {
    //     this.pendingNavigation = false;
    //     this.getNewPID().then(() => {
    //       this.getData();
    //     })
    //   }
    // });
  }


  getNewPID(): Promise<any> {
    return new Promise((resolve) => {
      this.planFormURL = JSON.parse(localStorage.getItem('planFormURL'));
      this.newPID = JSON.parse(localStorage.getItem('newPID'));
      if (this.newPID) {
        if (this.newPID.CustomerId) { this.newPIDCustId = this.newPID.CustomerId; } else { this.newPIDCustId = "" }
        if (this.newPID.custEmail) { this.custEmail = this.newPID.custEmail; } else { this.custEmail = "" }
        if (this.newPID.custMobile) { this.custMobile = this.newPID.custMobile; } else { this.custMobile = "" }
        if (this.newPID.custName) { this.custName = this.newPID.custName; } else { this.custName = "" }
      } else {
        this.newPIDCustId = "";
        this.custEmail = "";
        this.custMobile = "";
        this.custName = "";
      }
      resolve();
    });
  }

  getData(): Promise<any> {
    return new Promise((resolve: any) => {
      console.log(this.newPID, this.planFormURL);
      if (this.newPID && this.newPID.isNew) {
        this.isNewPID = true;
        if (this.newPID.pidAmount < 2000) {
          this.isWalletShowAtPID = true;
          this.isEMISHowAtPID = false;
        } else if (this.newPID.pidAmount >= 3000) {
          this.isWalletShowAtPID = false;
          this.isEMISHowAtPID = true;
        } else {
          this.isWalletShowAtPID = false;
          this.isEMISHowAtPID = false;
        }
      } else {
        this.isNewPID = false;
        this.travelRequest = JSON.parse(localStorage.getItem('quoteRequest'));
        this.travelResponse = JSON.parse(localStorage.getItem('quoteResponse'));
        this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
        this.travelProp = JSON.parse(localStorage.getItem('proposalResponse'));
        this.travelPropReq = JSON.parse(localStorage.getItem('proposalRequest'));
        this.createCustReq = JSON.parse(localStorage.getItem('customerRequest'));
        this.renewalProduct = JSON.parse(localStorage.getItem('product'));
        this.isRenewal = localStorage.getItem('isRenewal');
        if (this.travelPropReq.ProductType == 'CHI' || this.travelPropReq.ProductType == 'HBOOSTER' || this.travelPropReq.ProductType == 'PPAP' || this.travelPropReq.ProductType == 'ASP') {
          this.showCustInt = true;
        } else {
          this.showCustInt = false;
        }
        console.log(this.travelRequest);
        console.log(this.travelResponse);
        console.log(this.planDetails);
        console.log(this.travelProp);
        console.log(this.travelPropReq);
        console.log(this.createCustReq);
        console.log(this.renewalProduct);
        console.log(this.isRenewal);
        this.custEmail = this.createCustReq.EmailAddress
        this.custName = this.createCustReq.Name;
        this.custMobile = this.createCustReq.MobileNumber
        if (this.travelProp.SavePolicy[0].TotalPremium < 20000) {
          this.isWalletShow = true;
        } else {
          this.isWalletShow = false;
        }

        if (this.travelProp.SavePolicy[0].TotalPremium >= 3000) {
          this.isEMISHow = true;
        } else {
          this.isEMISHow = false;
        }

        if (this.travelProp.SavePolicy[0].CustomerID == null || this.travelProp.SavePolicy[0].CustomerID == undefined) {
          this.pidCustId = '';
          this.pidPFCustId = this.travelProp.SavePolicy[0].PF_CustomerID;
          this.getPIDList();
        } else {
          if (this.travelProp.SavePolicy[0].PF_CustomerID == null || this.travelProp.SavePolicy[0].PF_CustomerID == undefined) {
            this.pidCustId = this.travelProp.SavePolicy[0].CustomerID;
            this.pidPFCustId = '';
            this.getPIDList();
          } else {
            this.pidCustId = '';
            this.pidPFCustId = this.travelProp.SavePolicy[0].PF_CustomerID;
            this.getPIDList();
          }
        }
        console.log(this.pidCustId, this.pidPFCustId);
        resolve();
      }
    });
  }

  getPendingPaymentData() {
    this.totalPendingAmount = JSON.parse(localStorage.getItem('totalPendingAmount'));
    this.pendingPaymentData = JSON.parse(localStorage.getItem('paymentData'));
    this.pendingPaymentData.forEach((data) => {
      Object.keys(data).forEach((key) => {
        if (key == 'PolicyID') {
          this.policyIDS.push(data.PolicyID);
        }
      });
    })
    if (this.pendingPaymentData) {
      if (this.pendingPaymentData[0].CustomerDetails.CustomerID) { this.newPIDCustId = this.pendingPaymentData[0].CustomerDetails.CustomerID; } else { this.newPIDCustId = "" }
      if (this.pendingPaymentData[0].CustomerDetails.EmailID) { this.custEmail = this.pendingPaymentData[0].CustomerDetails.EmailID; } else { this.custEmail = "" }
      if (this.pendingPaymentData[0].CustomerDetails.MobileNo) { this.custMobile = this.pendingPaymentData[0].CustomerDetails.MobileNo; } else { this.custMobile = "" }
      if (this.pendingPaymentData[0].CustomerDetails.CustomerName) { this.custName = this.pendingPaymentData[0].CustomerDetails.CustomerName; } else { this.custName = "" }


      //       if (this.pendingPaymentData[0].PolicySubType == 12 || this.pendingPaymentData[0].PolicySubType == 14 || this.pendingPaymentData[0].PolicySubType == 15
      //         || this.pendingPaymentData[0].PolicySubType == 17 || this.pendingPaymentData[0].PolicySubType == 16) {
      //         this.policyProductType = 'CHI';
      //       }    
      //      else  if(this.pendingPaymentData[0].PolicySubType == 19 || this.pendingPaymentData[0].PolicySubType == 20){
      //         this.policyProductType = 'HBOOSTER';
      //       }
      //      else  if(this.pendingPaymentData[0].PolicySubType == 21){
      //         this.policyProductType = 'PPAP';
      //       }
      // else if(){
      //   this.policyProductType = 'PPAP';
      // }

      if (this.pendingPaymentData[0].PFCustomerID == null || this.pendingPaymentData[0].PFCustomerID == undefined) {
        this.pidCustId = this.pendingPaymentData[0].CustomerDetails.CustomerID;
        this.pidPFCustId = '';
        this.getPIDList();
      } else {
        this.pidCustId = '';
        this.pidPFCustId = this.pendingPaymentData[0].PFCustomerID;
        this.getPIDList();
      }


    } else {
      this.newPIDCustId = "";
      this.custEmail = "";
      this.custMobile = "";
      this.custName = "";
    }
  }

  makePayment1(mode: any, ev: any) {
    this.paymentMode = mode;
    console.log(this.paymentMode);
    let type: any;
    if (!this.pendingNavigation) {
      if (this.newPID && this.newPID.isNew) {
        type = this.newPID.policyType;
      } else {
        if (this.isRenewal == 'true') {
          type = 'Health';
        } else {
          if (this.travelPropReq.ProductType == 'CHI' || this.travelPropReq.ProductType == 'HBOOSTER' || this.travelPropReq.ProductType == 'PPAP' || this.travelPropReq.ProductType == 'ASP') {
            type = 'Health';
            this.showCustInt = true;
          }
          else if (this.travelPropReq.ProductType == 'Hospifund') { type = 'Health'; }
          else {
            type = 'Travel';
            this.showCustInt = false;
          }
        }
      }
    }
    else {
      if (this.isHealthProposal == 'true') {
        type = 'Health';
      }
      else {
        type = 'Travel'
      }
    }
    // this.cs.showLoader = true;
    this.beforeRazorPay().then(() => {
      this.payByRazorPay(this.commonDataRes, this.paymentMode, type, ev, this.router);
    });
  }

  beforeRazorPay(): Promise<any> {
    return new Promise((resolve: any) => {
      let payBody: any;
      if (this.newPID && this.newPID.isNew) {
        payBody = {
          "TransType": "PID",
          "PaymentAmount": this.newPID.pidAmount,
          "PidDetails": {
            "PolicyType": this.newPID.policyType,
            "PF_CustomerId": "",
            "iPartnerCustomerId": this.newPIDCustId
          },
          "GatewayReturnURL": "",
          "PolicyIDs": "",
          "PayerType": this.newPID.PayerType,
          "ModeID": 0,
          "UserRole": "AGENT",
          "IPAddress": config.ipAddress,
          "PaymentMode": "RAZORPAY"
        }
      } else if (this.pendingNavigation) {
        payBody = {
          "TransType": "POLICY_PAYMENT",
          "GatewayReturnURL": "",
          "PolicyIDs": this.policyIDS.join(':'),
          "PayerType": this.isHealthProposal ? "Customer" : this.custType,
          "ModeID": 0,
          "UserRole": "AGENT",
          "IPAddress": config.ipAddress,
          "PaymentMode": "RAZORPAY",
          "PaymentAmount": this.totalPendingAmount
        }

      }
      else {
        payBody = {
          "TransType": "POLICY_PAYMENT",
          "GatewayReturnURL": "",
          "PolicyIDs": this.travelProp.SavePolicy[0].PolicyID,
          "PayerType": "Customer",
          "ModeID": 0,
          "UserRole": "AGENT",
          "IPAddress": config.ipAddress,
          "PaymentMode": "RAZORPAY",
          "PaymentAmount": ""
        }
      }

      let str = JSON.stringify(payBody);
      console.log("STR", str);
      localStorage.setItem('commonPayReq', str);
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/Payment/CommonPayment', str).then((res: any) => {
        console.log(res);
        this.commonDataRes = res;
        localStorage.setItem('payData', JSON.stringify(res));
        this.cs.showLoader = false;
        resolve();
      }).catch((err: any) => {
        console.log("Error", err);
        this.cs.showLoader = false;
        resolve();
      });
    });
  }

  payByRazorPay(res: any, type: any, produtType: any, ev: any, router: any): Promise<any> {
    return new Promise((resolve) => {
      console.log("Response", this.commonDataRes);
      this.cs.showLoader = true;
      var desc = "ICICI Lombard - " + produtType + " Insurance";
      let self = this;
      this.options = {
        "description": desc,
        "image": 'https://www.icicilombard.com/mobile/mclaim/images/favicon.ico',
        "currency": 'INR',
        "key": res.PublicKey,
        "order_id": res.RazorOrderID,
        "method": {
          "netbanking": {
            "order": ["ICIC", "HDFC", "SBIN", "UTIB", "IDFB", "IBKL"]
          }
        },
        "prefill": {
          email: this.custEmail,
          contact: this.custMobile,
          name: this.custName,
          method: type
        },
        "theme": {
          "color": '#E04844',
          "hide_topbar": true
        },
        "handler": function (response: any) {
          console.log(response);
          let payData = JSON.parse(localStorage.getItem('payData'));
          console.log(payData);
          let params = {
            "iPaymentID": payData.PaymentID,
            "PaymentID": response.razorpay_payment_id,
            "OrderID": response.razorpay_order_id,
            "Signature": response.razorpay_signature
          }
          console.log("Screen", screen.width, screen.height);
          let width = screen.width / 2;
          let height = screen.height / 2;
          let left = (screen.width - width) / 2;
          let top = (screen.height - height) / 4;

          let childWindow = window.open('#/razor-pay-fallback', 'Childwindow', 'status=0,toolbar=0,menubar=0,resizable=0,scrollbars=1,top=' + top + ' ,left=' + left + ',height=' + height + ',width=' + width + '');
          if(!childWindow || childWindow.closed || typeof childWindow.closed=='undefined') 
          { 
            window.open('#/razor-pay-fallback', "_self");
            //window.postMessage({ 'razorPayDetails': params, 'url': config.baseURL + '/PaymentGateway/RazorPayPaymentProcess' }, 'http://localhost:8102/#/payment');
            window.postMessage({ 'razorPayDetails': params, 'url': config.baseURL + '/PaymentGateway/RazorPayPaymentProcess' }, config.paymentURL);
            return;
          }
          // window['child'] = childWindow;
          //window['router'] = router;

          //console.log("Child Window 1 ", childWindow);
          self.childPageDetails = childWindow;
          //console.log("Child Window 2 ", self.childPageDetails);
          childWindow.onload = (e) => {
            console.log("Child Window", e)
            if (childWindow == null || !window['postMessage']) {
              alert("failed");
            } else {
              //childWindow.postMessage({ 'razorPayDetails': params, 'url': config.baseURL + '/PaymentGateway/RazorPayPaymentProcess' }, 'http://localhost:8102/#/payment');
              childWindow.postMessage({ 'razorPayDetails': params, 'url': config.baseURL + '/PaymentGateway/RazorPayPaymentProcess' }, config.paymentURL);
            }
          }
        }
      };
      console.log("razorPayOptions = ", this.options);
      var rzp1 = new Razorpay(this.options);
      console.log(rzp1, this.commonDataRes);
      rzp1.open();
      this.cs.showLoader = false;
      resolve();
    });
  }

  ReceiveMessage(evt: any) {
    console.log("Receive Message", evt);
    if (evt.origin != config.razorpayURL) {
      return;
    } else {
      var data = evt.data;
     this.childPageDetails.close();
      if (data.event == "razorpayResponse") {
        if (JSON.parse(localStorage.getItem('planFormURL')) == 'PID') {
          this.router.navigateByUrl('payment', { skipLocationChange: true }).then(() => {
            this.router.navigateByUrl('pid');
            localStorage.setItem('isNewPIDReceivedData', 'true');
            localStorage.setItem('PIDReceiveData', JSON.stringify(data));
          });
        } else {
          console.log("PAYMENT Route", data);
          this.router.navigateByUrl('razor-pay-confirmation');
          localStorage.setItem('razorPayData', JSON.stringify(data));
        }
      }
    }

    // this.getInstance();

  }

  getInstance() {
    this.router.events.subscribe((url: any) => {
      console.log("AFTER END", url)
    })
  }

  showData(): Promise<any> {
    return new Promise((resolve) => {
      var rzp1 = new Razorpay(this.options);
      console.log(rzp1);
      rzp1.open();
      resolve();
    })
  }


  // submitForm(response: any) {
  //   console.log("Success", response, this.commonDataRes, this.options);
  //   let params = {
  //     "iPaymentID": this.commonDataRes.PaymentID,
  //     "PaymentID": response.razorpay_payment_id,
  //     "OrderID": response.razorpay_order_id,
  //     "Signature": response.razorpay_signature
  //   }

  //   var form = document.createElement("form");
  //   form.setAttribute("method", "post");
  //   form.setAttribute("action", config.baseURL + '/PaymentGateway/RazorPayPaymentProcess');
  //   for (var key in params) {
  //     if (params.hasOwnProperty(key)) {
  //       var hiddenField = document.createElement("input");
  //       hiddenField.setAttribute("type", "hidden");
  //       hiddenField.setAttribute("name", key);
  //       hiddenField.setAttribute("value", params[key]);
  //       form.appendChild(hiddenField);
  //     }
  //   }
  //   document.body.appendChild(form);
  //   console.log("Form", form);
  //   form.submit();
  // }

  makePayment2(mode: any) {
    this.router.navigateByUrl('payment-mode');
    localStorage.setItem('paymentMode', JSON.stringify(mode));
  }

  showPID(ev: any) {
    this.forCust(ev).then(() => {
      this.getPIDList();
    })
  }

  forCust(ev: any): Promise<any> {
    return new Promise((resolve) => {
      if (ev.target.checked) {
        this.isCustomer = true;
        this.custType = 'Intermediary';
        localStorage.setItem('custType', JSON.stringify(this.custType));
      } else {
        this.isCustomer = false;
        this.custType = 'Customer';
        localStorage.setItem('custType', JSON.stringify(this.custType));
      }
      resolve();
    })
  }

  getPIDList(): Promise<any> {
    return new Promise((resolve) => {
      console.log(this.pidCustId, this.pidPFCustId);
      localStorage.removeItem('PIDData');
      if (this.isCustomer) {
        this.cs.showLoader = true;
        // this.isCustomer = true;
        console.log("Intermediary");
        let policyType: any;
        if (this.pendingNavigation) {
          policyType = this.policyType;
        }
        else {
          if (this.travelPropReq.ProductType == 'International') {
            policyType = 'Travel';
          }
          else {
            policyType = 'Health';
          }
        }
        let body = {
          "PayerType": "Intermediary",
          "PF_CustomerId": this.pidPFCustId,
          "iPartnerCustomerId": this.pidCustId,
          "PolicyType": policyType
        };
        let str = JSON.stringify(body);
        this.cs.postWithParams('/api/Payment/GetPidList', str).then((res: any) => {
          console.log(res);
          localStorage.setItem('PIDData', JSON.stringify(res));
          this.cs.showLoader = false;
          resolve();
        }).catch((err) => {
          console.log(err);
          this.cs.showLoader = false;
          resolve();
        });
      }
      else {
        // console.log("Customer", this.travelPropReq.Product);
        this.cs.showLoader = true;
        let policyType: any;
        if (this.pendingNavigation) {
          policyType = this.policyType;
        }
        else {
          if (this.travelPropReq.ProductType == 'International') {
            policyType = 'Travel';
          }
          else {
            policyType = 'Health';
          }
        }
        let body = {
          "PayerType": "Customer",
          "PF_CustomerId": this.pidPFCustId,
          "iPartnerCustomerId": this.pidCustId,
          "PolicyType": policyType
        };
        let str = JSON.stringify(body);
        console.log(str);
        this.cs.postWithParams('/api/Payment/GetPidList', str).then((res: any) => {
          console.log(res);
          localStorage.setItem('PIDData', JSON.stringify(res));
          this.cs.showLoader = false;
          resolve();
        }).catch((err) => {
          console.log(err);
          this.cs.showLoader = false;
          resolve();
        });
      }
    });
  }

  payLater(ev: any) {
    console.log(ev);
    document.getElementById("myNav3").style.height = "100%";
    this.payLaterStart().then(() => {
      this.payLaterConfirm();
    });
  }

  payLaterStart(): Promise<any> {
    return new Promise((resolve) => {
      let body = {
        "TransType": "POLICY_PAYMENT",
        "GatewayReturnURL": "",
        "PolicyIDs": this.travelProp.SavePolicy[0].PolicyID,
        "PayerType": this.custType,
        "ModeID": 0,
        "UserRole": "AGENT",
        "IPAddress": config.ipAddress,
        "PaymentMode": "None",
        "PaymentAmount": ""
      }
      let str = JSON.stringify(body);
      console.log(str);
      this.cs.postWithParams('/api/Payment/CommonPayment', str).then((res: any) => {
        console.log(res);
        this.payLaterRes = res;
        resolve();
      }).catch((err) => {
        console.log("Error", err);
        resolve();
      });
    });
  }

  payLaterConfirm(): Promise<any> {
    return new Promise((resolve) => {
      this.cs.postWithParams('/api/Payment/CommonPaymentConfirmationByPID?EPaymentID=' + this.payLaterRes.EPaymentID, '').then((res: any) => {
        console.log(res);
        this.payLaterConfRes = res;
      });
    });
  }

  close(ev: any) {
    console.log(ev);
    document.getElementById("myNav3").style.height = "0%";
    this.cs.goToHome();
  }

  home(ev: any) {
    console.log("Home", ev);
    this.cs.goToHome();
  }


}
