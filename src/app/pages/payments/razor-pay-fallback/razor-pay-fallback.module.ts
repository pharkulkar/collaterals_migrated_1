import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RazorPayFallbackPage } from './razor-pay-fallback.page';

const routes: Routes = [
  {
    path: '',
    component: RazorPayFallbackPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RazorPayFallbackPage]
})
export class RazorPayFallbackPageModule {}
