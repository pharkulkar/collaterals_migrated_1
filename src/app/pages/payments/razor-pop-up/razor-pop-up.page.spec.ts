import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RazorPopUpPage } from './razor-pop-up.page';

describe('RazorPopUpPage', () => {
  let component: RazorPopUpPage;
  let fixture: ComponentFixture<RazorPopUpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RazorPopUpPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RazorPopUpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
