import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-razor-pop-up',
  templateUrl: './razor-pop-up.page.html',
  styleUrls: ['./razor-pop-up.page.scss'],
})
export class RazorPopUpPage implements OnInit {
  parentWindow: any; iFrame: any;
  @ViewChild('iframe') iframe: ElementRef;

  constructor() { }

  ngOnInit() {
    // this.iFrame = document.getElementById('iFrame123') as HTMLInputElement;
    // console.log("VIew Child", this.iframe);
    // if (!window['postMessage']) {
    //   alert("oh crap");
    // } else {
    //   if (window.addEventListener) {
    //     console.log("ifarame1");
    //     window.addEventListener("message", this.ReceiveMessage, false);
    //   }else {
    //     // window.attachEvent("onmessage", this.ReceiveMessage);
    //     console.log("ifarame2");
    //     (<any>window).attachEvent("onmessage", this.ReceiveMessage);
    //   }
    // }
    // this.observer();
    console.log("Razor Pay POP UP", window.addEventListener);
    if (window.addEventListener) {
      window.addEventListener("message", (a) => {
        console.log("Inside Event Listener", a)
        console.log("Parent ", this.iframe);
        console.log("Parent 123", document.getElementById('iFrame123'));
        if (a.origin == 'http://localhost:8100') {
          console.log("Origin Done");
          // this.iframe.nativeElement.contentWindow.postMessage(a);
          if (a.data.razorPayDetails) {
            document.getElementById('iFrame123').onload = function (event) {
              window.postMessage(a.data, '*');
            };
          } else {
            return false;
          }
        } else {
          console.log("No Event Listener")
        }
        // console.log(element);
      })
    }
  }

  // 
  // SendMessage() {
  //   let win = this.iframe.nativeElement.contentWindow;
  //   if (win == null || !window['postMessage'])
  //     alert("oh crap");
  //   else
  //     win.postMessage("hello", "*");
  // }

  observer() {
    new MutationObserver(function (mutations) {
      mutations.some(function (mutation) {
        if (mutation.type === 'attributes' && mutation.attributeName === 'src') {
          console.log(JSON.stringify(mutation));
          console.log('Old src: ', mutation.oldValue);
          console.log('New src: ', mutation.target);
          //this.parentWindow.source.postMessage(mutation.target, "*");
          return true;
        }

        return false;
      });
    }).observe(document.body, {
      attributes: true,
      attributeFilter: ['src'],
      attributeOldValue: true,
      characterData: false,
      characterDataOldValue: false,
      childList: false,
      subtree: true
    });
  }

  ReceiveMessage(evt: any) {
    // document.getElementById("iFrame123").onsubmit = function(e){
    //target the iFrame
    // debugger;
    let self = this;
    var win = self.iframe;
    console.log("WIN", win);

    //use JSON.stringify() to send text...
    // win.postMessage(JSON.stringify({ 'newMessage': document.getElementById("msg").value }), ORIGIN);

    // return false;
    // };
    // let iFrame = document.getElementById("iFrame123");
    // // let doc =  iFrame.contentDocument || iFrame.contentWindow;
    // console.log("Hello", this.iframe, evt);
    // if (evt.origin != "http://localhost:8100")
    //   return;
    // let message: any;
    // console.log("Ifrme", this.iFrame);
    // console.log(document.getElementById('iFrame123'));
    // this.parentWindow = evt;
    // if (false) {
    //   message = 'Something went wrong please try again';
    // } else {
    //   // iFrame.src = '#/razor-pay-fallback';
    //   // var frame  = this.iframe;
    //   // iFrame.nativeElement.contentWindow.postMessage(evt);
    //   if (evt.data.razorPayDetails) {
    //     document.getElementById('iFrame123').onload = function (event) {
    //       window.frames['iFrame123'].postMessage(evt.data, '*');
    //     };
    //   }
    // }
  }



}
