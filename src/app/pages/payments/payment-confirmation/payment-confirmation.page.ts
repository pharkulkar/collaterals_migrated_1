import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { ToastController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-payment-confirmation',
  templateUrl: './payment-confirmation.page.html',
  styleUrls: ['./payment-confirmation.page.scss'],
})
export class PaymentConfirmationPage implements OnInit {
  paymentDetails = false; showCHI = false; showHB = false; showPPAP = false; travelConf = false; healthConf = false;
  showEmailPrompt = false; email:any; successMsg = false; emailRes:any; error:any; isError = false;
  quoteRequest:any; quoteResponse:any; travelPlan:any; proposalResponse:any; customerRequest:any;
  PIDData:any; paymentMode:any; proposalRequest:any; commonPayRes:any; paymentConfRes:any;
  renewalProduct:any; heathRenewal:boolean=false; isRenewal:any; razorPayData:any;
  travelPayConfForm: FormGroup; CHIPayConfForm: FormGroup; HBPayConfForm: FormGroup; PPAPPayConfForm: FormGroup;
  showhospifund=false;HospifundPayConfForm:FormGroup;authData:any;
  showUnderWritter: boolean=false;
  constructor(public cs: CommonService, public toastCtrl: ToastController, public alertCtrl: AlertController) { }

  ngOnInit() {
    // document.getElementById("myNav4").style.height = "0%"; 
    this.createTravelForm(); 
    this.isRenewal= localStorage.getItem('isRenewal');
    this.getData().then(() => {
      this.dataBindTravel();
      this.createCHIForm();
      this.createHBForm();
      this.createPPAPForm();
      this.createHospifundForm();
    });    
  }

  getData(): Promise<any> {
    return new Promise((resolve: any) => {
      this.quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
      this.quoteResponse = JSON.parse(localStorage.getItem('quoteResponse'));
      this.travelPlan = JSON.parse(localStorage.getItem('travelPlan'));
      this.proposalResponse = JSON.parse(localStorage.getItem('proposalResponse'));
      this.proposalRequest = JSON.parse(localStorage.getItem('proposalRequest'));
       this.renewalProduct=JSON.parse(localStorage.getItem('product'));
      this.customerRequest = JSON.parse(localStorage.getItem('customerRequest'));
      this.PIDData = JSON.parse(localStorage.getItem('PIDData'));
      this.paymentMode = JSON.parse(localStorage.getItem('paymentMode'));
      this.commonPayRes = JSON.parse(localStorage.getItem('commonPayRes'));     
      this.paymentConfRes = JSON.parse(localStorage.getItem('paymentConfRes'));
      if (this.proposalRequest.ProductType == 'CHI') {
        this.showCHI = true;
      }
      else if (this.proposalRequest.ProductType == 'HBOOSTER') {
        this.showHB = true;
      }
      else if (this.proposalRequest.ProductType == 'PPAP') {
        this.showPPAP = true;
      }
      else if (this.proposalRequest.ProductType == 'Hospifund') {
        this.showhospifund = true;
      }
      if(this.isRenewal=='false')
      {
        if(this.proposalRequest.Product == 'Travel'){
          this.travelConf = true; this.healthConf = false;this.heathRenewal = false;
        }else{
          this.travelConf = false; this.healthConf = true;this.heathRenewal = false;
        }
      }
      else if(this.isRenewal=='true'){
        if(this.renewalProduct.productName == 'CHI' ||this.renewalProduct.productName == 'Health Booster' || this.renewalProduct.productName == 'PPAP')
        {
          this.travelConf = false; this.healthConf = false;this.heathRenewal = true;
        }
      }
    
      resolve();
    });
  }

  createTravelForm(){
    this.travelPayConfForm = new FormGroup({
      policyNumber: new FormControl(),
      premium: new FormControl(),
      policyStartDate: new FormControl(),
      policyEndDate: new FormControl()
    });
  }

  paymantConfirmation(data:any){
    let msg:any;
    this.cs.getUserData('/api/Payment/CommonPaymentConfirmationByPID?EPaymentID='+data.epaymentId).then((res:any) => {
      console.log(res);
      this.paymentConfRes = res.ConfirmPolicy[0];
      if(res.Message){
        msg = res.Message;
        this.presentAlert(msg);
        localStorage.setItem('isNewPIDReceivedData', 'false');
      }else {
        if(res.PaymentStatus == 'Successful Payment'){
          console.log("PAyment succ");
          msg = 'Payment is Successful. Your Transaction ID is '+ res.TransactionID;
          this.presentAlert(msg);
          if(this.paymentConfRes.PolicyStatus.includes('Pending'))
          {
            this.showUnderWritter=true;
          }
          else{
            this.showUnderWritter=false;
          }
        }else if(res.PaymentStatus == 'Pending Payment'){
          msg = 'Your Cheque details saved successfully';
          this.presentAlert(msg);
        }else if(res.PaymentStatus == 'Failed Payment'){
          if(res.GatewayError != '' || res.GatewayError != null){
            msg = 'Payment is Unsuccessful. Message: '+ data.GatewayError;
            this.presentAlert(msg);
          }else{
            msg = 'Payment is Unsuccessful. Please Try again';
            this.presentAlert(msg);
          }         
        }else{
          msg = 'Payment is Cancelled. Please Try again';
          this.presentAlert(msg);
        }
        localStorage.setItem('isNewPIDReceivedData', 'false');
      }
    });
  }

  dataBindTravel(){
    this.travelPayConfForm = new FormGroup({
      policyNumber: new FormControl(this.paymentConfRes.PolicyID),
      premium: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium),
      policyStartDate: new FormControl(moment(this.paymentConfRes.StartDate).format('YYYY-MM-DD')),
      policyEndDate: new FormControl(moment(this.paymentConfRes.EndDate).format('YYYY-MM-DD'))
    });
  }

  async presentAlert(msg:any) {
    const alert = await this.alertCtrl.create({
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  createCHIForm(){
    this.CHIPayConfForm = new FormGroup({
      policyNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].PolicyID),
      proposalNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].ProposalNumber),
      premium: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium),
      policyStartDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].StartDate).format('YYYY-MM-DD')),
      policyEndDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].EndDate).format('YYYY-MM-DD'))
    });
  }

  createHBForm(){
    this.HBPayConfForm = new FormGroup({
      policyNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].PolicyID),
      proposalNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].ProposalNumber),
      premium: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium),
      policyStartDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].StartDate).format('YYYY-MM-DD')),
      policyEndDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].EndDate).format('YYYY-MM-DD'))
    });
  }

  createPPAPForm(){
    this.PPAPPayConfForm = new FormGroup({
      policyNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].PolicyID),
      proposalNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].ProposalNumber),
      premium: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium),
      policyStartDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].StartDate).format('YYYY-MM-DD')),
      policyEndDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].EndDate).format('YYYY-MM-DD'))
    });
  }
  createHospifundForm(){
    this.HospifundPayConfForm = new FormGroup({
      policyNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].PolicyID),
      proposalNumber: new FormControl(this.paymentConfRes.PolicyDetails[0].ProposalNumber),
      premium: new FormControl(this.proposalResponse.SavePolicy[0].TotalPremium),
      policyStartDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].StartDate).format('YYYY-MM-DD')),
      policyEndDate: new FormControl(moment(this.paymentConfRes.PolicyDetails[0].EndDate).format('YYYY-MM-DD'))
    });
  }

  showDetails(ev:any){
    this.paymentDetails = !this.paymentDetails;
    if (this.paymentDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("details").style.background = "url(" + imgUrl + ")";
      document.getElementById("details").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("details").style.background = "url(" + imgUrl + ")";
      document.getElementById("details").style.backgroundRepeat = 'no-repeat';
    }
  }

  showTabs(data:any){
    console.log(data);
    if(data == 'CHI'){
      this.showCHI = true; this.showHB = false; this.showPPAP = false;this.showhospifund=false;
    }else if(data == 'HB'){
      this.showCHI = false; this.showHB = true; this.showPPAP = false;this.showhospifund=false;
    }else if(data=='PPAP'){
      this.showCHI = false; this.showHB = false; this.showPPAP = true;this.showhospifund=false;
    }
    else{
      this.showCHI = false; this.showHB = false; this.showPPAP = false;this.showhospifund=true;
    }
  }

  showEmail(ev:any){
    // this.showEmailPrompt = !this.showEmailPrompt;
    console.log(document.getElementById("myNav4"));
    document.getElementById("myNav4").style.height = "100%";  
  }

  close(ev:any){
    document.getElementById("myNav4").style.height = "0%";  
    document.getElementById("myNav5").style.height = "0%";  
  }

  downloadPdf(ev:any){   
    if(this.proposalRequest.ProductType == 'Hospifund')
    {
      this.getToken();
    } 
    else{
      let downloadURL = this.cs.pdfDownload('POLICY',this.paymentConfRes.PolicyDetails[0].EPolicyID)
      this.cs.save(downloadURL, this.paymentConfRes.PolicyDetails[0].ProposalNumber+".pdf");
    }
 
    this.presentToast('Your pdf will be downloaded');
  }
  getToken()
  {
    this.cs.getAuthorizationDataforPDF().then(res => {
      this.authData = res;
      this.generatePDF();
    }, err => {
      this.presentToast('Sorry Something went wrong');
    });
  }
  generatePDF()
  {
    this.cs.showLoader=true;
    this.cs.generatePDF(this.paymentConfRes.PolicyDetails[0].PolicyNumber,this.authData).then((res:any) => {
      console.log(res);
      var fileName = this.paymentConfRes.PolicyDetails[0].PolicyNumber+ '.pdf';
      this.cs.saveBlob(res, fileName);
      this.cs.showLoader=false;
    }, err => {
        this.presentToast('Sorry something went wrong');
        this.cs.showLoader=false;
    }); 
  }

  sendEmail(ev:any){
    console.log(this.email);
    let body = {
      "EPolicyID": this.paymentConfRes.PolicyDetails[0].EPolicyID,
      // "EPolicyID": 'ABCDE',
      "PDFType":"POLICY",
      "EmailID": this.email
    }
    let str = JSON.stringify(body);
    this.cs.showLoader = true;
    this.cs.postWithParams('/api/file/sendemailpdf', str).then((res:any) => {
      console.log(res);
      this.emailRes = res.Message;
      if(this.emailRes.indexOf('successfully emailed') > -1){
        document.getElementById("myNav4").style.height = "0%";  
        document.getElementById("myNav5").style.height = "100%"; 
        this.successMsg = true; this.isError = false;
        this.cs.showLoader = false;
      }else{
        this.cs.showLoader = false;
        this.successMsg = false; this.isError = false;
        document.getElementById("myNav4").style.height = "0%";  
        document.getElementById("myNav5").style.height = "100%";
      }      
    }).catch((err:any) => {
      console.log("error", err.statusText);
      this.isError = true;
      this.error = err.statusText;
      this.successMsg = false;
      this.cs.showLoader = false;
      document.getElementById("myNav4").style.height = "0%";  
      document.getElementById("myNav5").style.height = "100%";
    });
  }

  async presentToast(msg:any) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  home(ev:any){
    console.log("Home", ev);
    this.cs.goToHome();
  }
}
