import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import * as moment from 'moment';
import { config } from 'src/app/config.properties';
import { ToastController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment-mode',
  templateUrl: './payment-mode.page.html',
  styleUrls: ['./payment-mode.page.scss'],
})
export class PaymentModePage implements OnInit {

  travelRequest: any; travelResponse: any; planDetails: any; travelProp: any; travelPropReq: any; createCustReq: any;
  chequeForm: FormGroup; PIDForm: FormGroup; dualCheckForm: any; bankNames: any; filterData: any; branchNames: any; filterBranchData: any;
  bankId: any; branchId: any; transType = "POLICY_PAYMENT"; commonPayRequest:any; payLinkBody:any;
  minChequeDate: any; maxChequeDate: any; paymentRes: any; PIDData: any; taggedAmount: any; filterPID: any;
  filterPID1: any; filterPID2: any; filterPID3: any; filterPID4: any; filterPID5: any; totalAmount = 0; paymentId: any;
  paymentDone = false; paymentModePID: any; paymentMode: any; commonRequest: any; PIDArray = []; PFPaymentID: any;
  showMobile = false; showEmail = true; paymentLinkEmail:any; paymentLinkMobile:any; custType:any;
  linkPayment = false; newPID:any; isNewPID = false; emailRes:any
  pendingPaymentData: any;
  isPendingPayment: boolean;
  policyType: string;
  isPendingNavigation: string;
  totalPendingAmount: any;
  isHealthProposal: string;
  proposalNumbers: any =[];
  PF_ProsalNumber: any =[];
  policyIDS: any =[];
  constructor(public alertCtrl: AlertController, public cs: CommonService, public toastCtrl: ToastController, public router: Router) { }

  ngOnInit() {
    this.isPendingNavigation = localStorage.getItem('pendingNavigation');
    this.isHealthProposal = localStorage.getItem('isHealthProposal');
    this.createChequeForm();
    this.createDualCheckForm();
    this.createPIDForm();
    this.getBankName();
    this.getBranchName();
    if(this.isPendingNavigation != 'true'){
      this.isPendingPayment = false;
      this.getData().then(() => {
        this.validationForChequeDate(this.travelRequest);
        console.log("Hello", this.travelProp);
        this.travelPropReq = JSON.parse(localStorage.getItem('proposalRequest'));
        if (this.travelPropReq.ProductType == 'CHI' || this.travelPropReq.ProductType == 'HBOOSTER' || this.travelPropReq.ProductType == 'PPAP' || this.travelPropReq.ProductType == 'Hospifund' || this.travelPropReq.ProductType == 'ASP') {
          this.chequeForm.patchValue({ 'proposalNo': this.travelProp.SavePolicy[0].ProposalNo });
          this.dualCheckForm.patchValue({ 'proposalNo': this.travelProp.SavePolicy[0].ProposalNo });
          if(this.travelPropReq.ProductType == 'Hospifund'){
            this.custType = 'Customer';
          }
        }else {
          this.chequeForm.patchValue({ 'proposalNo': this.travelProp.SavePolicy[0].PF_ProsalNumber });
          this.dualCheckForm.patchValue({ 'proposalNo': this.travelProp.SavePolicy[0].PF_ProsalNumber });
        }
  
      });
    }else{
      if(this.isHealthProposal == 'true'){
        this.policyType = 'Health';
      }else{
        this.policyType = 'Travel'
      }
      this.isPendingPayment = true;
      this.getPendingPaymentData();
    }
    
  }

  createChequeForm() {
    this.chequeForm = new FormGroup({
      proposalNo: new FormControl(),
      chequeNo: new FormControl(),
      chequeDate: new FormControl(),
      bankName: new FormControl(),
      branchName: new FormControl(),
    });
  }

  createPIDForm() {
    this.PIDForm = new FormGroup({
      PID1: new FormControl(),
      PID2: new FormControl(),
      PID3: new FormControl(),
      PID4: new FormControl(),
      PID5: new FormControl()
    });
  }

  createDualCheckForm() {
    this.dualCheckForm = new FormGroup({
      proposalNo: new FormControl(),
      chequeNo: new FormControl(),
      chequeAmount: new FormControl(),
      chequeDate: new FormControl(),
      bankName: new FormControl(),
      branchName: new FormControl(),
      PID1: new FormControl(),
      PID2: new FormControl(),
      PID3: new FormControl(),
      PID4: new FormControl(),
      PID5: new FormControl()
    });
  }


  getData(): Promise<any> {
    return new Promise((resolve: any) => {
      this.travelRequest = JSON.parse(localStorage.getItem('quoteRequest'));
      this.travelResponse = JSON.parse(localStorage.getItem('quoteResponse'));
      this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
      this.travelProp = JSON.parse(localStorage.getItem('proposalResponse'));
      this.travelPropReq = JSON.parse(localStorage.getItem('proposalRequest'));
      this.createCustReq = JSON.parse(localStorage.getItem('customerRequest'));
      this.PIDData = JSON.parse(localStorage.getItem('PIDData'));
      this.paymentMode = JSON.parse(localStorage.getItem('paymentMode'));
      this.custType = JSON.parse(localStorage.getItem('custType'));
      this.newPID = JSON.parse(localStorage.getItem('newPID'));
      if(this.newPID){
        if(this.newPID.isNew){
          this.isNewPID = true;
        }else{
          this.isNewPID = false;
        }        
      }else{
        this.isNewPID = false;
      }
      console.log("IMP", this.travelProp);
      console.log(this.travelRequest);
      console.log(this.travelResponse);
      console.log(this.planDetails);
      console.log(this.travelPropReq);
      console.log(this.createCustReq);
      console.log("PID", this.PIDData);
      console.log("NEW PID", this.newPID);
      resolve();
    });
  }


  getPendingPaymentData(): Promise<any> {
    return new Promise((resolve: any) => {
      this.pendingPaymentData = JSON.parse(localStorage.getItem('paymentData'));
      this.paymentMode = JSON.parse(localStorage.getItem('paymentMode'));
      this.totalPendingAmount = JSON.parse(localStorage.getItem('totalPendingAmount'));
      this.custType = JSON.parse(localStorage.getItem('custType'));
      this.PIDData = JSON.parse(localStorage.getItem('PIDData'));


      this.pendingPaymentData.forEach((data)=>{
        Object.keys(data).forEach((key)=>{
          if(key == 'PFProposalNo'){
            this.proposalNumbers.push(data.PFProposalNo);
            console.log("pfnuber", this.proposalNumbers )
          }
          if(key == 'PF_ProsalNumber'){
            this.PF_ProsalNumber.push(data.PF_ProsalNumber);
          }
          if(key == 'PolicyID'){
            this.policyIDS.push(data.PolicyID);
            console.log("policyIDS", this.policyIDS )
          }
        });
      });
      // let proposalNumber = this.pendingPaymentData[0].PFProposalNo;
      // let PF_ProsalNumber = this.pendingPaymentData[0].PF_ProsalNumber
      // if (this.pendingPaymentData[0].PolicySubType == 12 || this.pendingPaymentData[0].PolicySubType == 14 || this.pendingPaymentData[0].PolicySubType == 15
      //   || this.pendingPaymentData[0].PolicySubType == 17 || this.pendingPaymentData[0].PolicySubType == 16) {
      //   this.policyType = 'CHI';
      // } else if (this.pendingPaymentData[0].PolicySubType == 19 || this.pendingPaymentData[0].PolicySubType == 20) {
      //   this.policyType = 'HB';
      // } else if (this.pendingPaymentData[0].PolicySubType == 21) {
      //   this.policyType = 'PPAP';
      // }
      if (this.pendingPaymentData[0].PolicySubType != 25) {
        this.chequeForm.patchValue({ 'proposalNo': this.proposalNumbers.join(',')});
        this.dualCheckForm.patchValue({ 'proposalNo': this.proposalNumbers.join(',')});
      }else {
        this.chequeForm.patchValue({ 'proposalNo': this.PF_ProsalNumber.join(',') });
        this.dualCheckForm.patchValue({ 'proposalNo': this.PF_ProsalNumber.join(',') });
      }
      this.isNewPID = false;
      this.isPendingPayment = true;
      console.log("IMP", this.pendingPaymentData);
      resolve();
    });
  }

  validationForChequeDate(data: any) {
    console.log(data.LeavingIndia);
    if(this.travelRequest.ProductType == 'PPAP'){
      let date = new Date(data.LeavingIndia);
      let date1 = new Date(data.LeavingIndia);
      date.setDate(date.getDate() - parseInt('15'));
      this.minChequeDate = moment(date).format('YYYY-MM-DD');
      date1.setDate(date1.getDate() + parseInt('15'));
      this.minChequeDate = moment(date1).format('YYYY-MM-DD');
    }else{
      let date = new Date(data.LeavingIndia);
      date.setDate(date.getDate() - parseInt('90'));
      this.minChequeDate = moment(date).format('YYYY-MM-DD');
      console.log(this.minChequeDate);
      this.maxChequeDate = moment(data.LeavingIndia).format('YYYY-MM-DD');
    }
    
  }

  getBankName() {
    this.cs.postWithParams('/api/Agent/GetBankName?PolicyType=1','').then((res: any) => {
      this.bankNames = res.BankDetail;
      console.log(res);
    }).catch((err: any) => {
      console.log(err.error.text);
    });
  }

  selectBank(ev: any) {
    console.log(ev.target.value, this.bankNames);
    let abc = ev.target.value
    this.filterData = this.bankNames.filter((x: any) => (x.Value).toUpperCase().includes(abc.toUpperCase()));
    console.log(this.filterData);
  }

  getBranchName() {
    this.cs.postWithParams('/api/Agent/GetBranchLocations','').then((res: any) => {
      this.branchNames = res.BranchDetail;
      console.log(res);
    }).catch((err: any) => {
      console.log(err.error.text);
    });
  }

  selectBranch(ev: any) {
    console.log(ev.target.value, this.branchNames);
    let abc = ev.target.value
    this.filterBranchData = this.branchNames.filter((x: any) => (x.Value).toUpperCase().includes(abc.toUpperCase()));
    console.log("Filetr Data", this.filterBranchData);
  }

  selectedBankList(data: any) {
    console.log(data);
    this.bankId = data.Key;
    this.chequeForm.patchValue({ 'bankName': data.Value });
    this.dualCheckForm.patchValue({ 'bankName': data.Value });
    localStorage.setItem('chequeBank', JSON.stringify(data.Value));
    this.filterData = [];
  }

  selectedBranchList(data: any) {
    console.log(data);
    this.branchId = data.Key;
    console.log(this.branchId);
    this.chequeForm.patchValue({ 'branchName': data.Value });
    this.dualCheckForm.patchValue({ 'branchName': data.Value });
    this.filterBranchData = [];
  }

  selectPID(ev: any) {
    let data = this.PIDData.find((x: any) => x.PaymentID == ev.target.value);
    this.filterPID = this.PIDData.filter((x: any) => x.PaymentID !== ev.target.value);
    this.paymentModePID = data.PaymentMode;
    if(this.paymentMode == 'dualCheque'){ this.totalAmount = this.totalAmount;  }
    else{  this.totalAmount = 0;   }    
    if (this.paymentModePID !== 'ChequeDemandDraft') {
      if(this.paymentMode == 'dualCheque'){ this.totalAmount = this.totalAmount;  }
      else{  this.totalAmount = 0;   } 
      console.log("Netbanking", this.totalAmount);
      this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
      console.log("Netbanking 2", this.totalAmount);
      this.PIDForm.patchValue({ 'PID2': '' }); this.PIDForm.patchValue({ 'PID3': '' }); this.PIDForm.patchValue({ 'PID4': '' }); this.PIDForm.patchValue({ 'PID5': '' });
      
      if (this.isPendingNavigation != 'true' ? this.travelProp.SavePolicy[0].TotalPremium : this.pendingPaymentData[0].TotalPremium < this.totalAmount) {
        this.paymentDone = true;
        this.PIDArray.push(data.PaymentID);
        this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
      } else {
        this.paymentDone = true;
        this.showToast('Kindly select amount greater then premium amount');
        this.PIDArray.push(data.PaymentID);
      }
    } else {
      if(this.paymentMode == 'dualCheque'){ this.totalAmount = this.totalAmount;  }
      else{  this.totalAmount = 0;   } 
      console.log("Cheque", this.totalAmount);
      this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
      console.log("Final Cheque", this.totalAmount);
      this.PIDForm.patchValue({ 'PID2': '' }); this.PIDForm.patchValue({ 'PID3': '' }); this.PIDForm.patchValue({ 'PID4': '' }); this.PIDForm.patchValue({ 'PID5': '' });
      if (this.isPendingNavigation != 'true' ? this.travelProp.SavePolicy[0].TotalPremium : this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount) {
        this.paymentDone = true;
        this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
        this.PIDArray.push(data.PaymentID);
      } else {
        this.paymentDone = false;
        this.showToast('Kindly select amount greater then premium amount');
        this.PIDArray.push(data.PaymentID);
      }
    }
  }

  selectPID1(ev: any) {
    console.log(ev.target.value);
    let data = this.filterPID.find((x: any) => x.PaymentID == ev.target.value);
    console.log(data);
    this.filterPID1 = this.filterPID.filter((x: any) => x.PaymentID !== ev.target.value);
    console.log("Filter PID", this.filterPID);
    this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
    console.log(this.totalAmount);
    if (this.isPendingNavigation != 'true' ? this.travelProp.SavePolicy[0].TotalPremium : this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount && this.paymentModePID === 'ChequeDemandDraft') {
      console.log("Payment Done");
      this.paymentDone = true;
      this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
      this.PIDArray.push(data.PaymentID);
    } else {
      console.log("Payment Not Done");
      this.paymentDone = false;
      this.showToast('Kindly select amount greater then premium amount');
      this.PIDArray.push(data.PaymentID);
    }
  }

  selectPID2(ev: any) {
    console.log(ev.target.value);
    let data = this.filterPID1.find((x: any) => x.PaymentID == ev.target.value);
    console.log(data);
    this.filterPID2 = this.filterPID1.filter((x: any) => x.PaymentID !== ev.target.value);
    console.log("Filter PID", this.filterPID1);
    this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
    console.log(this.totalAmount);
    if (this.isPendingNavigation != 'true' ? this.travelProp.SavePolicy[0].TotalPremium : this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount && this.paymentModePID === 'ChequeDemandDraft') {
      console.log("Payment Done");
      this.paymentDone = true;
      this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
      this.PIDArray.push(data.PaymentID);
    } else {
      console.log("Payment Not Done");
      this.paymentDone = false;
      this.showToast('Kindly select amount greater then premium amount');
      this.PIDArray.push(data.PaymentID);
    }
  }

  selectPID3(ev: any) {
    console.log(ev.target.value);
    let data = this.filterPID2.find((x: any) => x.PaymentID == ev.target.value);
    console.log(data);
    this.filterPID3 = this.filterPID2.filter((x: any) => x.PaymentID !== ev.target.value);
    console.log("Filter PID", this.filterPID3);
    this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
    console.log(this.totalAmount);
    if (this.isPendingNavigation != 'true' ? this.travelProp.SavePolicy[0].TotalPremium : this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount && this.paymentModePID === 'ChequeDemandDraft') {
      console.log("Payment Done");
      this.paymentDone = true;
      this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
      this.PIDArray.push(data.PaymentID);
    } else {
      console.log("Payment Not Done");
      this.paymentDone = false;
      this.showToast('Kindly select amount greater then premium amount');
      this.PIDArray.push(data.PaymentID);
    }
  }

  selectPID4(ev: any) {
    console.log(ev.target.value);
    let data = this.filterPID3.find((x: any) => x.PaymentID == ev.target.value);
    console.log(data);
    this.filterPID4 = this.filterPID3.filter((x: any) => x.PaymentID !== ev.target.value);
    console.log("Filter PID", this.filterPID4);
    this.totalAmount = this.totalAmount + parseInt(data.TaggedAmount);
    console.log(this.totalAmount);
    if (this.isPendingNavigation != 'true' ? this.travelProp.SavePolicy[0].TotalPremium : this.travelProp.SavePolicy[0].TotalPremium < this.totalAmount && this.paymentModePID === 'ChequeDemandDraft') {
      console.log("Payment Done");
      this.paymentDone = true;
      this.showToast('Selected Payment Is Greater Than Premium So Kindly Proceed');
      this.PIDArray.push(data.PaymentID);
    } else {
      console.log("Payment Not Done");
      this.paymentDone = false;
      this.showToast('Kindly select amount greater then premium amount');
      this.PIDArray.push(data.PaymentID);
    }
  }

  makePay(form: any) {
    console.log(this.paymentMode);
    if(this.isNewPID){
      this.transType = 'PID';
      this.makePayment(form).then(() => {
        this.paymentConfirmation();
      });
    }else{
      if (this.paymentMode == 'PID') {
        let PFPaymentID = this.PIDArray.join(':');
        this.PFPaymentID = PFPaymentID;
        this.paymentId = '0';
        this.paymentConfirmation();
      } else if (this.paymentMode == 'dualCheque') {
        console.log("DUal Cheque");
        let PFPaymentID = this.PIDArray.join(':');
        this.PFPaymentID = PFPaymentID;
        this.paymentId = this.paymentRes.PaymentID;
        this.paymentConfirmation();
      } else if (this.paymentMode == 'paymentLink') {
        console.log("Payment Link");
        this.makePayment(form).then(() => {
          this.payByLink();
        })
      } else {
        this.makePayment(form).then(() => {
          this.PFPaymentID = this.paymentRes.PFPaymentID;
          this.paymentId = this.paymentRes.PaymentID;
          this.paymentConfirmation();
        });
      }
    }
    
  }

  validateCheque(form: any) {
    if (form.value.chequeNo.length == 6) {
      if (form.value.chequeDate !== null && form.value.chequeAmount !== null  && form.value.branchName !== null && form.value.bankName !== null) {
        console.log("Valid");
        this.transType = "PID";
        this.makePayment(form);
        setTimeout(function(){
          $('button').removeClass('cdk-focused cdk-program-focused');
      });
      } else {
        console.log("inValid");
        setTimeout(function(){
          $('button').removeClass('cdk-focused cdk-program-focused');
      });
      }
    } else {
      console.log("inValid cheque");
      setTimeout(function(){
        $('button').removeClass('cdk-focused cdk-program-focused');
    });
    }
  }

  showPaymentLink(data:any){
    if(data == 'email'){
      this.showMobile = false;
      this.showEmail = true;
    }else{
      this.showEmail = false;
      this.showMobile = true;
    }
  }

  payByLink(){
    if(this.showEmail){
      console.log(this.paymentLinkEmail);
      this.payLinkBody = {
        "EmailID": this.paymentLinkEmail,
        "PolicyID": this.isPendingNavigation == 'true' ? this.pendingPaymentData[0].PolicyID : this.travelProp.SavePolicy[0].PolicyID,
        "isRazorPay":true
      }
      let str = JSON.stringify(this.payLinkBody);
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/payment/SendCustomerPaymentLink', str).then((res:any) => {
        console.log(res);
        this.linkPayment = true;
        this.cs.showLoader = false;
        let msg = "Payment link successfully sent to " + this.paymentLinkEmail + "\nNote: Payment will be completed on the customer portal";
        this.presentToast(msg);  
      }).catch((err:any) => {
        console.log(err);
        this.cs.showLoader = false;
      });
    }else{
      this.payLinkBody = {
        "PolicyID":this.isPendingNavigation == 'true' ? this.pendingPaymentData[0].PolicyID : this.travelProp.SavePolicy[0].PolicyID,
        "MobileNo":this.paymentLinkMobile,
        "IsRazorPay":true
      }
      let str = JSON.stringify(this.payLinkBody);
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/payment/SendCustomerPaymentLinkSMS', str).then((res:any) => {
        console.log(res);
        this.linkPayment = true;
        this.cs.showLoader = false;
        let msg = "Payment link successfully sent to " + this.paymentLinkMobile + "\nNote: Payment will be completed on the customer portal";
        this.presentToast(msg);
      }).catch((err:any) => {
        console.log(err);
        this.cs.showLoader = false;  
      });
      console.log(this.paymentLinkMobile);
    }    
  }

  async presentToast(msg:any) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });    
    toast.present();

    this.cs.goToHome();
  }

  makePayment(form: any): Promise<any> {
    return new Promise((resolve) => {
      if(this.isNewPID){
        this.commonPayRequest = {
          "TransType": this.transType,
          "PaymentAmount": this.newPID.pidAmount,
          "PidDetails": {
            "PolicyType": this.newPID.policyType,
            "PF_CustomerId": "",
            "iPartnerCustomerId": this.newPID.CustomerId
          },
          "GatewayReturnURL": config.baseURL + "/PaymentGateway/PGIPaymentProcess",
          "PolicyIDs": "",
          "PayerType": this.newPID.PayerType,
          "ModeID": 0,
          "UserRole": "AGENT",
          "IPAddress": config.ipAddress,
          "PaymentMode": "ChequeDemandDraft",
          "ChequeDetails": {
            "MICR": "MICR",
            "InstrumentNo": form.value.chequeNo,
            "InstrumentDate": moment(form.value.chequeDate).format('DD-MMM-YYYY'),
            "BankID": this.bankId,
            "BankBranch": form.value.branchName,
            "PaymentMode": 1,
            "BranchDepositedLocation": this.branchId,
            "ChequeDDType": "3",
            "IsDepositAtBankBranch": "N"
          }
        }
      }else{
      if (this.paymentMode == 'dualCheque') {
        this.transType = "PID";
        this.commonPayRequest = {
          "PaymentMode": "ChequeDemandDraft",
          "ChequeDetails": {
            "MICR": "MICR",
            "InstrumentNo": form.value.chequeNo,
            "InstrumentDate": moment(form.value.chequeDate).format('DD-MMM-YYYY'),
            "BankID": this.bankId,
            "BankBranch": form.value.branchName,
            "PaymentMode": 1,
            "BranchDepositedLocation": this.branchId,
            "ChequeDDType": "3",
            "IsDepositAtBankBranch": "N"
          },
          "TransType": this.transType,
          "PaymentAmount": form.value.chequeAmount,
          "PidDetails": {
            "PolicyType": this.isHealthProposal == 'true' ? this.policyType : this.planDetails.Product,
            "PF_CustomerId": this.isPendingNavigation == 'true' ? this.pendingPaymentData[0].PFCustomerID : this.travelProp.SavePolicy[0].PF_CustomerID,
            "iPartnerCustomerId": ""
          },
          "GatewayReturnURL": config.baseURL + "/PaymentGateway/PGIPaymentProcess",
          "PolicyIDs": this.isPendingNavigation  == 'true' ? this.policyIDS.join(':') : this.travelProp.SavePolicy[0].PolicyID,
          "PayerType": this.custType,
          "ModeID": 0,
          "UserRole": "AGENT",
          "IPAddress": config.ipAddress
        }
      }if (this.paymentMode == 'paymentLink') {
        this.commonPayRequest = {
          "TransType": "POLICY_PAYMENT",
          "GatewayReturnURL": "",
          "PolicyIDs": this.isPendingNavigation  == 'true' ? this.policyIDS.join(':') : this.travelProp.SavePolicy[0].PolicyID,
          "PayerType": this.custType,
          "ModeID": 0,
          "UserRole": "AGENT",
          "IPAddress":config.ipAddress,
          "PaymentMode": "CUSTOMERPAYMENTLINK",
          "PaymentAmount": ""
      }
      } else {
        this.commonPayRequest = {
          "TransType": this.transType,
          "GatewayReturnURL": config.baseURL + "/PaymentGateway/PGIPaymentProcess",
          "PolicyIDs": this.isPendingNavigation  == 'true' ? this.policyIDS.join(':') : this.travelProp.SavePolicy[0].PolicyID,
          "PayerType": this.custType,
          "ModeID": 0,
          "UserRole": "AGENT",
          "IPAddress":config.ipAddress,
          "PaymentMode": "ChequeDemandDraft",
          "ChequeDetails": {
            "MICR": "MICR",
            "InstrumentNo": form.value.chequeNo,
            "InstrumentDate": moment(form.value.chequeDate).format('DD-MMM-YYYY'),
            "BankID": this.bankId,
            "BankBranch": form.value.branchName,
            "PaymentMode": 1,
            "BranchDepositedLocation": this.branchId,
            "ChequeDDType": "3",
            "IsDepositAtBankBranch": "N"
          }
        }
      }
    }
      let str = JSON.stringify(this.commonPayRequest);
      localStorage.setItem('paymentModeReq', str);
      console.log(str);
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/Payment/CommonPayment', str).then((res: any) => {
        console.log(res);
        this.paymentRes = res;
        if(this.paymentMode == 'dualCheque'){
          this.totalAmount = form.value.chequeAmount;
          this.PIDArray.push(this.paymentRes.PFPaymentID);
        }    
        localStorage.setItem('commonPayRes', JSON.stringify(this.paymentRes));
        this.cs.showLoader = false;
        resolve();
      }).catch((err) => {
        console.log("Error", err);
        this.cs.showLoader = false;
        if(err.error.Message){
          this.presentAlert(err.error.Message);
          return;
        }
       this.totalAmount = form.value.chequeAmount;
        resolve();
      });
    });

  }

  async presentAlert(msg: any) {
    const alert = await this.alertCtrl.create({
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  paymentConfirmation(): Promise<any> {
    return new Promise((resolve) => {
      let body = {
        "MultiPFPaymentID": this.PFPaymentID,
        "PolicyIds": this.isPendingNavigation == 'true' ? this.pendingPaymentData[0].PolicyID : this.travelProp.SavePolicy[0].PolicyID,
        "PaymentID": this.paymentId,
        "TransType": "POLICY_PAYMENT"
      }
      let str = JSON.stringify(body);
      console.log(str, this.paymentMode);
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/Payment/PolicyPaymentTagging', str).then((res: any) => {
        console.log(res);
        localStorage.setItem('paymentConfRes', JSON.stringify(res));
        if(localStorage.getItem('isPendingPage')){
          localStorage.removeItem('isPendingPage');
          localStorage.removeItem('pendingNavigation');
        }       
        this.paymentRes = res;
        if(this.paymentMode == 'Cheque'){
          this.router.navigateByUrl('cheque-confirmation');
        }else{
          this.router.navigateByUrl('payment-confirmation');      
        }
        this.cs.showLoader = false;
        resolve();
      }).catch((err) => {
        console.log("Error", err);
        this.cs.showLoader = false;
        resolve();
      });
      resolve();
    });
  }

  cancelPay(ev:any){
    this.router.navigateByUrl('payment');
  }

  async showToast(msg: any) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 5000
    });
    toast.present();
  }

  home(ev:any){
    console.log("Home", ev);
    this.cs.goToHome();
  }

  getdate1(ev:any){
    setTimeout(function(){
      $('button').removeClass('cdk-focused cdk-program-focused');
  });
  }

}
