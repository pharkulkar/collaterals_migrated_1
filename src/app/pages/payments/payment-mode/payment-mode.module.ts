import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PaymentModePage } from './payment-mode.page';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule  } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { ApplicationPipesModule } from 'src/app/application-pipes/application-pipes.module';
const routes: Routes = [
  {
    path: '',
    component: PaymentModePage
  }
];

@NgModule({
  imports: [ApplicationPipesModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PaymentModePage]
})
export class PaymentModePageModule {}
