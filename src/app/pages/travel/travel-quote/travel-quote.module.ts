import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TravelQuotePage } from './travel-quote.page';
// import { MDBBootstrapModule } from 'angular-bootstrap-md';
// import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule  } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
const routes: Routes = [
  {
    path: '',
    component: TravelQuotePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TravelQuotePage]
})
export class TravelQuotePageModule {}
