import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormGroupDirective } from '@angular/forms';
import * as moment from 'moment';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { ToastController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
// import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
// import { IMyOptions } from 'ng-uikit-pro-standard';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS, MatDateFormats } from "@angular/material";
import { config } from 'src/app/config.properties';

@Component({
  selector: 'app-travel-quote',
  templateUrl: './travel-quote.page.html',
  styleUrls: ['./travel-quote.page.scss'],
})
export class TravelQuotePage implements OnInit {

  customPopoverOptions: any = {}; minDate: any; minDate1: any; dob: any; leaveDate: any; returnDate: any; noOfDays: any;
  subProduct: any; travelType: any; addOn: any; planId: any; planType: any; stateMaster: any; stateDetails: any; isGSTKerala: any;
  travelQuate: any; leavingDate: any; returningDate: any; maxDate: any; minDOB: any; travelSchengenData: any; travelEuropeanUnionMemberData: any;
  showAddOn = false; isMultiTrip = false; isFamilyCover = false; isCategoryValidation = false; isImmigrant = false; validForm = false;
  traveller1 = true; traveller2 = false; traveller3 = false; traveller4 = false; traveller5 = false; traveller6 = false;
  inputLoadingPlan = false; inputLoadingState = false; isCountryList = false; isKerala = false; isKeralaCess = false;
  noOfTraveller = 1; isIndividual = 'False'; isFamily = 'False'; isSeniorCitizen = 'False'; isProfessional = 'false';
  isAdvanture = 'false'; tripType = "Round Trip"; IsAnnualMultiTrip = 'No'; isVisitingUSCanada = ""; isVisitingSCHENGEN = "";
  isVisitingOtherCountries = ""; isTravelDataValid = false; IsCoverNeededIndividual = 'False';
  travelForm: FormGroup;
  plans = []; usaOddData = []; usaEvenData = []; schengenOddData = []; schengenEvenData = []; memberArrayNEW = [];
  isKeralaCessEnabled: any; isQuoteDetails: boolean = true; isPremiumDetails: boolean = true; isQuoteGenerated: boolean = false;
  request: any; response: any; planDetails: any; loading: any; isRecalculate = "false";
  showCoronaDisclaimer: boolean = false;
  hideSubmit: boolean = true;
  // public myDatePickerOptions: IMyOptions = {
  //   // Your options
  //   };
  constructor(public activatedRoute: ActivatedRoute, public cs: CommonService, public formBuilder: FormBuilder, public location: Location, public router: Router, public xml2JsonService: XmlToJsonService, public alertCtrl: AlertController) {

  }
  ngOnInit() {
    let iskeralaCessFeature = localStorage.getItem('isKeralaCessEnabled');
    if (iskeralaCessFeature == 'Y') {
      this.isKeralaCessEnabled = true;
    } else {
      this.isKeralaCessEnabled = false;
    }
    this.isCategoryValidation = false;
    this.getCountryData();
    if (this.cs.isBackFormTravelPremium) {
      this.reCalculate();
    } else {
      this.createTravelForm();
    }
    let date = new Date();
    this.minDate = date;
    // this.minDate = moment(date).format('YYYY-MM-DD');
    console.log(this.minDate);
    let maxDate = new Date(date);
    maxDate.setDate(maxDate.getDate() + parseInt('365'));
    this.maxDate = moment(maxDate).format('YYYY-MM-DD');
    let minDOB = new Date(date);
    minDOB.setDate(minDOB.getDate() - parseInt('91'));
    this.minDOB = moment(minDOB).format('YYYY-MM-DD');
    this.getStateMaster();


    this.travelForm.patchValue({ 'isImmigrantVisa': 'NO' });
    this.travelForm.patchValue({ 'leavingDate': '' });
    this.travelForm.patchValue({ 'returningDate': '' });
    this.travelForm.patchValue({ 'noOfTripDays': 0 });

    this.setTravelLocbyDefault();
  }

  getList(ev: any) {
    console.log(ev);
    if (ev == 'No') {
      this.isCountryList = false;
      document.getElementById("myNav").style.height = "0%";
    } else {
      this.isCountryList = true;
      document.getElementById("myNav").style.height = "100%";
    }
  }

  getCountryData() {
    this.cs.postWithParams('/api/Travel/ListofEuropeanUnionMemberStates', '').then((res: any) => {
      this.travelEuropeanUnionMemberData = res.EuropeanUnionMemberStates;
      res.EuropeanUnionMemberStates.forEach((element: any, index: any) => {
        if (index % 2 == 0) {
          this.usaEvenData.push(res.EuropeanUnionMemberStates[index]);
        } else {
          this.usaOddData.push(res.EuropeanUnionMemberStates[index]);
        }
      });
      console.log(this.usaEvenData, this.usaOddData);
      console.log(this.travelEuropeanUnionMemberData);
    });
    this.cs.postWithParams('/api/Travel/ListofSchengenStates', '').then((res: any) => {
      res.SchengenStates.forEach((element: any, index: any) => {
        if (index % 2 == 0) {
          this.schengenEvenData.push(res.SchengenStates[index]);
        } else {
          this.schengenOddData.push(res.SchengenStates[index]);
        }
      });
      console.log(this.schengenEvenData, this.schengenOddData);
      console.log(this.travelSchengenData);
    });
  }

  closeNav(ev: any) {
    document.getElementById("myNav").style.height = "0%";
  }

  getStateMaster() {
    console.log("State");
    this.inputLoadingState = true;
    this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then((res: any) => {
      this.stateMaster = res;
      let statename = this.stateMaster.find(x => x.StateId == 55).StateName;
      this.travelForm.patchValue({ 'state': statename });
      this.inputLoadingState = false;
      console.log(res);
    }).catch((err: any) => {
      this.inputLoadingState = false;
      console.log(err);
    });
  }

  getStateDetails(ev: any) {
    console.log(ev.target.value);
    this.stateDetails = this.stateMaster.find((x: any) => x.StateName == ev.target.value);
    if (this.isKeralaCessEnabled) {
      if (ev.target.value == 'KERALA') {
        this.isKerala = true;
      } else {
        this.travelForm.patchValue({ 'isKeralaGST': false });
        this.isKerala = false;
      }
    } else {
      this.isKerala = false;
      this.travelForm.patchValue({ 'isKeralaGST': false });
    }

  }

  isKeralaGST(data: any) {
    console.log(data);
    if (data == 'Yes') {
      this.travelForm.patchValue({ 'isKeralaGST': true });;
      this.isGSTKerala = true;
    } else {
      this.travelForm.patchValue({ 'isKeralaGST': false });
      this.isGSTKerala = false;
    }
  }

  createTravelForm() {
    this.travelForm = this.formBuilder.group({
      productName: ['internationalTravel'],
      subProduct: [null], tripType: [null], travelLoc: [null], isImmigrantVisa: [null],
      leavingDate: [null], returningDate: [null], noOfTripDays: [null], coverageType: [null],
      coverNeeded: [null], noOfTraveller: [null], traveller1DOB: [null], traveller2DOB: [null],
      traveller3DOB: [null], traveller4DOB: [null], traveller5DOB: [null], traveller6DOB: [null],
      plan: [null], tripDuration: [null], policyStartDate: [null], state: [null], isKeralaGST: [null]
    });
    this.categoryValidation();
  }

  reCalculate() {
    this.travelForm = this.formBuilder.group({
      productName: ['internationalTravel'],
      subProduct: [this.travelForm.value.subProduct], tripType: [null], travelLoc: [null], isImmigrantVisa: [null],
      leavingDate: [null], returningDate: [null], noOfTripDays: [null], coverageType: [null],
      coverNeeded: [null], noOfTraveller: [null], traveller1DOB: [null], traveller2DOB: [null],
      traveller3DOB: [null], traveller4DOB: [null], traveller5DOB: [null], traveller6DOB: [null],
      plan: [null], tripDuration: [null], policyStartDate: [null], state: [null], isKeralaGST: [null]
    });
    // this.categoryValidation();
  }

  resetForm() {
    this.travelForm.patchValue({ 'leavingDate': "" });
    this.travelForm.patchValue({ 'returningDate': "" });
    this.travelForm.patchValue({ 'noOfTripDays': "" });
    this.travelForm.patchValue({ 'coverageType': "" });
    this.travelForm.patchValue({ 'coverNeeded': "" });
    this.travelForm.patchValue({ 'noOfTraveller': "" });
    this.travelForm.patchValue({ 'traveller1DOB': "" });
    this.travelForm.patchValue({ 'traveller2DOB': "" });
    this.travelForm.patchValue({ 'traveller3DOB': "" });
    this.travelForm.patchValue({ 'traveller4DOB': "" });
    this.travelForm.patchValue({ 'traveller5DOB': "" });
    this.travelForm.patchValue({ 'traveller6DOB': "" });
    this.travelForm.patchValue({ 'plan': "" });
    this.travelForm.patchValue({ 'tripDuration': "" });
    this.travelForm.patchValue({ 'policyStartDate': "" });
    // this.travelForm.patchValue({ 'state': "0" });
    this.travelForm.patchValue({ 'isKeralaGST': "" });
    this.travelType = "";
  }

  categoryValidation(): Promise<any> {
    return new Promise((resolve: any) => {
      this.travelForm.get('subProduct').valueChanges.subscribe((data: any) => {
        if (data == 'Individual') {
          this.travelForm.get('tripType').setValidators([Validators.required]);
          this.travelForm.get('tripType').valueChanges.subscribe((data: any) => {
            console.log("Trip Type", data);
            if (!data) {
              // this.isCategoryValidation = true;
              this.travelForm.get('travelLoc').setValidators([Validators.required]);
              this.travelForm.get('isImmigrantVisa').setValidators([Validators.required]);
              this.travelForm.get('leavingDate').setValidators([Validators.required]);
              this.travelForm.get('returningDate').setValidators([Validators.required]);
              this.travelForm.get('plan').setValidators([Validators.required]);
              this.travelForm.get('state').setValidators([Validators.required]);
              this.travelForm.get('noOfTraveller').setValidators([Validators.required]);
              this.travelForm.get('traveller1DOB').setValidators([Validators.required]);
              this.travelForm.get('tripDuration').clearValidators();
              this.travelForm.get('policyStartDate').clearValidators();
            } else {
              // this.isCategoryValidation = true;
              this.travelForm.get('tripDuration').setValidators([Validators.required]);
              this.travelForm.get('policyStartDate').setValidators([Validators.required]);
              this.travelForm.get('plan').setValidators([Validators.required]);
              this.travelForm.get('state').setValidators([Validators.required]);
              this.travelForm.get('noOfTraveller').setValidators([Validators.required]);
              this.travelForm.get('traveller1DOB').setValidators([Validators.required]);
              this.travelForm.get('travelLoc').clearValidators();
              this.travelForm.get('isImmigrantVisa').clearValidators();
              this.travelForm.get('leavingDate').clearValidators();
              this.travelForm.get('returningDate').clearValidators();
            }
            this.travelForm.get('travelLoc').updateValueAndValidity();
            this.travelForm.get('isImmigrantVisa').updateValueAndValidity();
            this.travelForm.get('leavingDate').updateValueAndValidity();
            this.travelForm.get('returningDate').updateValueAndValidity();
            this.travelForm.get('tripDuration').updateValueAndValidity();
            this.travelForm.get('policyStartDate').updateValueAndValidity();
            this.travelForm.get('plan').updateValueAndValidity();
            this.travelForm.get('state').updateValueAndValidity();
            this.travelForm.get('noOfTraveller').updateValueAndValidity();
            this.travelForm.get('traveller1DOB').updateValueAndValidity();
            // resolve();
          });
        } else if (data == 'Family') {
          // this.isCategoryValidation = true;
          this.travelForm.get('tripType').clearValidators();
          this.travelForm.get('policyStartDate').clearValidators();
          this.travelForm.get('tripDuration').clearValidators();
          this.travelForm.get('travelLoc').setValidators([Validators.required]);
          this.travelForm.get('isImmigrantVisa').setValidators([Validators.required]);
          this.travelForm.get('leavingDate').setValidators([Validators.required]);
          this.travelForm.get('returningDate').setValidators([Validators.required]);
          this.travelForm.get('plan').setValidators([Validators.required]);
          this.travelForm.get('state').setValidators([Validators.required]);
          this.travelForm.get('noOfTraveller').setValidators([Validators.required]);
          this.travelForm.get('traveller1DOB').setValidators([Validators.required]);
        } else if (data == 'Senior Citizen') {
          // this.isCategoryValidation = true;
          this.travelForm.get('tripType').clearValidators();
          this.travelForm.get('tripDuration').clearValidators();
          this.travelForm.get('policyStartDate').clearValidators();
          this.travelForm.get('travelLoc').setValidators([Validators.required]);
          this.travelForm.get('isImmigrantVisa').setValidators([Validators.required]);
          this.travelForm.get('leavingDate').setValidators([Validators.required]);
          this.travelForm.get('returningDate').setValidators([Validators.required]);
          this.travelForm.get('plan').setValidators([Validators.required]);
          this.travelForm.get('state').setValidators([Validators.required]);
          this.travelForm.get('noOfTraveller').setValidators([Validators.required]);
          this.travelForm.get('traveller1DOB').setValidators([Validators.required]);
        } else {
          console.log("Fill The form");
        }
        this.travelForm.get('tripType').updateValueAndValidity();
        this.travelForm.get('travelLoc').updateValueAndValidity();
        this.travelForm.get('isImmigrantVisa').updateValueAndValidity();
        this.travelForm.get('leavingDate').updateValueAndValidity();
        this.travelForm.get('returningDate').updateValueAndValidity();
        this.travelForm.get('tripDuration').updateValueAndValidity();
        this.travelForm.get('policyStartDate').updateValueAndValidity();
        this.travelForm.get('plan').updateValueAndValidity();
        this.travelForm.get('state').updateValueAndValidity();
        this.travelForm.get('noOfTraveller').updateValueAndValidity();
        this.travelForm.get('traveller1DOB').updateValueAndValidity();
        // resolve();
      });
      resolve();
    });
  }

  getCover(ev: any) {
    console.log(ev);
    if (ev.target.id == 'familyPlan') {
      this.travelForm.patchValue({ 'noOfTraveller': '2' });
      this.noOfTraveller = 2;
      this.isFamilyCover = true;
    } else {
      this.travelForm.patchValue({ 'noOfTraveller': '1' });
      this.isFamilyCover = false;
    }
  }

  changeSubProduct(ev: any) {
    console.log(this.travelForm);
    this.isQuoteGenerated = false;
    this.subProduct = ev.target.value;
    this.resetForm();
    console.log("Sub Product", this.subProduct);
    if (this.subProduct == "Individual") {
      this.travelType = 1; this.isIndividual = 'True'; this.isFamily = 'False'; this.isSeniorCitizen = 'False';
      this.travelForm.patchValue({ 'noOfTraveller': 1 });
      let minDOB = new Date();
      minDOB.setDate(minDOB.getDate() - parseInt('91'));
      this.minDOB = moment(minDOB).format('YYYY-MM-DD');
      this.showTripByDefault();
    } else if (this.subProduct == "Family") {
      this.IsCoverNeededIndividual = 'True'; this.memberArrayNEW = [0, 1];
      this.isMultiTrip = false; this.isIndividual = 'False'; this.isFamily = 'True'; this.isSeniorCitizen = 'False';
      this.noOfTraveller = 2; this.travelForm.patchValue({ 'coverageType': "FAMILY" });
      this.travelType = 2; this.travelForm.patchValue({ 'noOfTraveller': '2' });
      this.travelForm.patchValue({ 'tripDuration': "0" }); this.travelForm.patchValue({ 'policyStartDate': "" });
      let minDOB = new Date();
      minDOB.setDate(minDOB.getDate() - parseInt('91'));
      this.minDOB = moment(minDOB).format('YYYY-MM-DD');
      this.setTravelLocbyDefault();
    } else if (this.subProduct == 'Senior Citizen') {
      this.isMultiTrip = false; this.isIndividual = 'False'; this.isFamily = 'False'; this.isSeniorCitizen = 'True';
      this.noOfTraveller = 1; this.travelForm.patchValue({ 'noOfTraveller': '1' }); this.travelForm.patchValue({ 'coverageType': "SENIOR" });
      this.travelType = 3;
      this.travelForm.patchValue({ 'tripDuration': "0" }); this.travelForm.patchValue({ 'policyStartDate': "" });

      let minDOB = new Date();
      minDOB.setFullYear(minDOB.getFullYear() - parseInt('72'));
      console.log(minDOB);
      this.minDOB = moment(minDOB).format('YYYY-MM-DD');
    } else {
      console.log("Invalid SubProduct...");
    }
  }

  seeImmigrant(ev: any) {
    console.log(ev);
    if (ev.target.textContent == 'Yes') {
      this.isImmigrant = true;
      this.travelForm.patchValue({ 'isImmigrantVisa': 'YES' });
      this.travelForm.patchValue({ 'leavingDate': '' });
      this.travelForm.patchValue({ 'returningDate': '' });
      this.travelForm.patchValue({ 'noOfTripDays': 0 });
    } else {
      this.isImmigrant = false;
      this.travelForm.patchValue({ 'isImmigrantVisa': 'NO' });
      this.travelForm.patchValue({ 'leavingDate': '' });
      this.travelForm.patchValue({ 'returningDate': '' });
      this.travelForm.patchValue({ 'noOfTripDays': 0 });
    }
  }

  getMaxDate(ev: any) {

    setTimeout(function () {
      $('button').removeClass('cdk-focused cdk-program-focused');
    });

    let startDate = new Date(ev.target.value);
    this.minDate1 = ev.target.value;
    if (this.travelForm.value.isImmigrantVisa == 'NO') {
      startDate.setDate(startDate.getDate() + parseInt('365'));
      this.maxDate = moment(startDate).format('YYYY-MM-DD');
    } else {
      startDate.setDate(startDate.getDate() + parseInt('89'));
      this.maxDate = moment(startDate).format('YYYY-MM-DD');
    }
  }

  changeTrip(ev: any) {
    console.log("Trip Event", ev.target.id);
    if (ev.target.id == 'triptypeRound') {
      // this.plans = [];
      this.tripType = "Round Trip"; this.isMultiTrip = false; this.IsAnnualMultiTrip = 'No'
      this.travelForm.patchValue({ 'tripType': false }); this.travelForm.patchValue({ 'tripDuration': "0" });
      this.travelForm.patchValue({ 'policyStartDate': "" }); this.travelForm.patchValue({ 'coverageType': "INDIVIDUAL" });
      this.travelForm.patchValue({ 'traveller2DOB': "" }); this.travelForm.patchValue({ 'traveller3DOB': "" }); this.travelForm.patchValue({ 'traveller4DOB': "" }); this.travelForm.patchValue({ 'traveller5DOB': "" }); this.travelForm.patchValue({ 'traveller6DOB': "" });
    } else {
      // this.plans = [];
      this.tripType = "Multi Trip";
      this.isMultiTrip = true; this.IsAnnualMultiTrip = 'Yes'; this.isVisitingUSCanada = 'Yes';
      this.isVisitingSCHENGEN = 'No'; this.isVisitingOtherCountries = 'No'; this.travelForm.patchValue({ 'tripDuration': null });
      this.travelForm.patchValue({ 'tripType': true }); this.travelForm.patchValue({ 'coverageType': "SINGLE" });
      this.travelForm.patchValue({ 'traveller1DOB': null });
      // this.getTravelPlan();
    }
  }

  showTripByDefault() {
    this.tripType = "Round Trip"; this.isMultiTrip = false; this.IsAnnualMultiTrip = 'No'
    this.travelForm.patchValue({ 'tripType': false }); this.travelForm.patchValue({ 'tripDuration': "0" });
    this.travelForm.patchValue({ 'policyStartDate': "" }); this.travelForm.patchValue({ 'coverageType': "INDIVIDUAL" });
    this.travelForm.patchValue({ 'traveller2DOB': "" }); this.travelForm.patchValue({ 'traveller3DOB': "" }); this.travelForm.patchValue({ 'traveller4DOB': "" }); this.travelForm.patchValue({ 'traveller5DOB': "" }); this.travelForm.patchValue({ 'traveller6DOB': "" });
  }

  selectStartDate(ev: any) {
    setTimeout(function () {
      $('button').removeClass('cdk-focused cdk-program-focused');
    });

    if (this.tripType == "Multi Trip") {
      this.travelForm.patchValue({ 'leavingDate': this.travelForm.value.policyStartDate }); this.travelForm.patchValue({ 'returningDate': '' });
      this.travelForm.patchValue({ 'isImmigrantVisa': "" }); this.travelForm.patchValue({ 'noOfTripDays': "" });
      this.getTravelPlan();
    }
  }

  getTravelLoc(ev: any) {
    console.log("Location", ev);
    this.showCoronaDisclaimer = true;
    if (this.tripType == "Round Trip" && ev == 'USAorCANADA') {
      this.travelForm.patchValue({ 'travelLoc': 'USA or Canada' });
      this.isVisitingUSCanada = 'Yes'; this.isVisitingSCHENGEN = 'No'; this.isVisitingOtherCountries = 'No';
    } else if (this.tripType == "Round Trip" && ev == 'SCHENGEN') {
      this.travelForm.patchValue({ 'travelLoc': 'Schengen Countries' });
      this.isVisitingUSCanada = 'No'; this.isVisitingSCHENGEN = 'Yes'; this.isVisitingOtherCountries = 'No';
    } else if (this.tripType == "Round Trip" && ev == 'OTHER') {
      this.travelForm.patchValue({ 'travelLoc': 'Other Countries' });
      this.isVisitingUSCanada = 'No'; this.isVisitingSCHENGEN = 'No'; this.isVisitingOtherCountries = 'Yes';
    }
  }
  setTravelLocbyDefault() {
    //this.travelForm.patchValue({ 'travelLoc': 'Schengen Countries' });
    this.isVisitingUSCanada = 'No'; this.isVisitingSCHENGEN = 'No'; this.isVisitingOtherCountries = 'No';
  }

  getReturnDate(ev: any) {
    setTimeout(function () {
      $('button').removeClass('cdk-focused cdk-program-focused');
    });

    console.log(ev.target.value, this.travelForm.value.isImmigrantVisa);
    this.leaveDate = moment(this.travelForm.value.leavingDate).format('DD-MMMM-YYYY');
    this.returnDate = moment(ev.target.value).format('DD-MMMM-YYYY');
    let difference = new Date(this.returnDate).getTime() - new Date(this.leaveDate).getTime();
    this.noOfDays = (difference / (24 * 3600 * 1000)) + 1;
    this.travelForm.patchValue({ 'noOfTripDays': this.noOfDays });
    this.getTravelPlan();
  }

  getPolStartDate(ev: any) {
    setTimeout(function () {
      $('button').removeClass('cdk-focused cdk-program-focused');
    });

    console.log(ev.detail.value);
    let polStartDate = moment(ev.detail.value).format('DD-MMMM-YYYY');
    this.travelForm.patchValue({ 'policyStartDate': polStartDate });
    this.getTravelPlan();
  }

  selectAddOn(ev: any) {
    console.log(ev.target.id);
    if (ev.target.id == 'semiProSports') {
      this.addOn = "Semi Profetional Sports";
      this.isProfessional = 'True';
      console.log(this.addOn)
    } else {
      this.addOn = "Advanture Sports";
      this.isAdvanture = 'True';
      console.log(this.addOn)
    }

  }

  toggleAddOn(ev: any) {
    console.log("Toggle", ev);
    this.showAddOn = !this.showAddOn;
    if (!this.showAddOn) { this.isAdvanture = 'false'; this.isProfessional = 'false'; }
  }

  getTravelPlan() {
    this.plans = [];
    console.log(this.travelForm.value);
    this.inputLoadingPlan = true;
    let startDate: any;
    if (this.travelForm.value.policyStartDate) {
      startDate = moment(this.travelForm.value.policyStartDate).format('DD-MM-YYYY');
    } else {
      startDate = "";
    }
    let body = {
      "GeoCode": "WORLDWIDE",
      "isMultiTrip": this.travelForm.value.tripType,
      "LeavingIndiaDate": moment(this.travelForm.value.leavingDate).format('DD-MM-YYYY'),
      "PolicyStartDate": startDate,
      "TravelType": this.travelType,
      "NoOfTravellers": this.travelForm.value.noOfTraveller,
      "CoverNeededFloater": false,
      "IndividualPlan": true,
      "FamilyPlan": this.isFamily
    }
    let str = JSON.stringify(body);
    console.log(str);
    this.cs.postWithParams('/api/Travel/GetInternationalTravelPlan', str).then((res: any) => {
      console.log("Response", res);
      if (res.StatuMessage == "Success") {
        console.log("Response", res.lstTravelPlans.length);
        if (res.lstTravelPlans.length) {
          this.plans = [];
          res.lstTravelPlans.forEach((plan: any) => {
            this.plans.push(plan);
          });
          this.inputLoadingPlan = false;
        } else {
          this.presentToast();
          this.inputLoadingPlan = false;
        }
      }
    });
  }

  async presentToast() {
    const alert = await this.alertCtrl.create({
      header: 'ICICI Lombard',
      message: 'No plan available for ' + this.noOfTraveller + ' members',
      buttons: ['OK']
    });
    await alert.present();
  }

  planValidateBaseNoofTraveller() {
    if (this.subProduct == 'Senior Citizen' || this.subProduct == 'Family') {
      this.getTravelPlan();
    } else {
      if (this.travelForm.value.policyStartDate) {
        this.getTravelPlan();
      } else {
        this.commonToast('Please Select Policy Start Date');
      }
    }
  }

  noOfTravellerChange(ev: any) {
    console.log(ev.target.value);
    this.noOfTraveller = ev.target.value;
    this.memberArrayNEW = [];
    let i = 0;
    for (i = 0; i < ev.target.value; i++) {
      this.memberArrayNEW.push(i);
    }
    if (ev.target.value == 1) {
      this.travelForm.patchValue({ 'noOfTraveller': '1' });
      this.planValidateBaseNoofTraveller();
      this.traveller2 = false; this.traveller3 = false; this.traveller4 = false; this.traveller5 = false; this.traveller6 = false;
      this.travelForm.patchValue({ 'traveller2DOB': "" }); this.travelForm.patchValue({ 'traveller3DOB': "" }); this.travelForm.patchValue({ 'traveller4DOB': "" }); this.travelForm.patchValue({ 'traveller5DOB': "" }); this.travelForm.patchValue({ 'traveller6DOB': "" });
    } else if (ev.target.value == 2) {
      this.travelForm.patchValue({ 'noOfTraveller': '2' });
      this.planValidateBaseNoofTraveller();
      this.traveller2 = true; this.traveller3 = false; this.traveller4 = false; this.traveller5 = false; this.traveller6 = false;
      this.travelForm.patchValue({ 'traveller3DOB': "" }); this.travelForm.patchValue({ 'traveller4DOB': "" }); this.travelForm.patchValue({ 'traveller5DOB': "" }); this.travelForm.patchValue({ 'traveller6DOB': "" });
    } else if (ev.target.value == 3) {
      this.travelForm.patchValue({ 'noOfTraveller': '3' });
      this.planValidateBaseNoofTraveller();
      this.traveller2 = true; this.traveller3 = true; this.traveller4 = false; this.traveller5 = false; this.traveller6 = false;
      this.travelForm.patchValue({ 'traveller4DOB': "" }); this.travelForm.patchValue({ 'traveller5DOB': "" }); this.travelForm.patchValue({ 'traveller6DOB': "" });
    } else if (ev.target.value == 4) {
      this.travelForm.patchValue({ 'noOfTraveller': '4' });
      this.planValidateBaseNoofTraveller();
      this.traveller2 = true; this.traveller3 = true; this.traveller4 = true; this.traveller5 = false; this.traveller6 = false;
      this.travelForm.patchValue({ 'traveller5DOB': "" }); this.travelForm.patchValue({ 'traveller6DOB': "" });
    } else if (ev.target.value == 5) {
      this.travelForm.patchValue({ 'noOfTraveller': '5' });
      this.planValidateBaseNoofTraveller();
      this.traveller2 = true; this.traveller3 = true; this.traveller4 = true; this.traveller5 = true; this.traveller6 = false;
      this.travelForm.patchValue({ 'traveller6DOB': "" });
    } else {
      this.travelForm.patchValue({ 'noOfTraveller': '6' });
      this.planValidateBaseNoofTraveller();
      this.traveller2 = true; this.traveller3 = true; this.traveller4 = true; this.traveller5 = true; this.traveller6 = true;
    }
    if (this.subProduct == 'Family' || this.subProduct == 'Individual') {
      this.getTravelPlan();
    }
  }

  getReturningDate(ev: any) {
    setTimeout(function () {
      $('button').removeClass('cdk-focused cdk-program-focused');
    });

    console.log(this.travelForm.value.isImmigrantVisa);
    if (this.travelForm.value.isImmigrantVisa == 'YES') {
      console.log('YES', this.travelForm.value.noOfTripDays);
      if (this.travelForm.value.noOfTripDays < 91) {
        console.log('OK');
        let newDate = new Date(this.travelForm.value.leavingDate);
        newDate.setDate(newDate.getDate() + parseInt(ev.target.value));
        let returnDate = moment(newDate).format('YYYY-MM-DD');
        this.travelForm.patchValue({ 'returningDate': returnDate });
        this.getTravelPlan();
      } else {
        console.log('NOT OK');
        this.commonToast('Kindly Select days below 90');
        this.travelForm.patchValue({ 'noOfTripDays': 0 });
      }
    } else {
      console.log('NO', this.travelForm.value.noOfTripDays);
      let newDate = new Date(this.travelForm.value.leavingDate);
      newDate.setDate(newDate.getDate() + parseInt(ev.target.value));
      let returnDate = moment(newDate).format('YYYY-MM-DD');
      this.travelForm.patchValue({ 'returningDate': returnDate });
      this.getTravelPlan();
    }
  }

  doChange(ev: any) {
    console.log("Event", ev.target.checked, ev.target.id);
  }

  getPlanDetail(ev: any) {
    console.log(ev.target.value);
    let plan = ev.target.value;
    let selectedPlan = this.plans.find(x => x.PlanName == plan);
    console.log(selectedPlan);
    this.planType = selectedPlan.PlanType;
    this.planId = selectedPlan.PlanCode;
    localStorage.setItem('travelPlan', JSON.stringify(selectedPlan));

  }

  validateDate(form: any): Promise<any> {
    return new Promise((resolve: any) => {
      if (form.value.leavingDate != "") {
        this.leavingDate = moment(form.value.leavingDate).format('DD-MMM-YYYY');
        if (form.value.returningDate != "") {
          this.returningDate = moment(form.value.returningDate).format('DD-MMM-YYYY');
        } else {
          this.returningDate = "";
        }
      } else {
        this.leavingDate = "";
      }
      resolve();
    });
  }

  validateForKerala(form: any): Promise<any> {
    return new Promise((resolve) => {
      console.log(this.isKerala == true, form);
      if (this.isKerala && form.value.isKeralaGST == '') {
        this.validForm = false;
        resolve();
      } else {
        this.validForm = true;
        resolve();
      }
    });
  }

  finalValidation(ev: any) {
    console.log(this.tripType, this.travelForm.value.subProduct, this.isTravelDataValid, this.travelForm.value.plan);
    if (this.travelForm.value.subProduct == 'Individual') {
      if (this.tripType == 'Round Trip') {
        if (this.cs.isUndefinedORNull(this.travelForm.value.travelLoc)) {
          this.isTravelDataValid = false;
          this.commonToast("Please Select travel Location");
        }
        else if (this.cs.isUndefinedORNull(this.travelForm.value.leavingDate)) {
          this.isTravelDataValid = false;
          this.commonToast("Please Select Leaving Date");
        } else if (this.cs.isUndefinedORNull(this.travelForm.value.returningDate)) {
          this.isTravelDataValid = false;
          this.commonToast("Please Select Returning Date");
        } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller1DOB)) {
          this.isTravelDataValid = false;
          this.commonToast("Please Select DOB");
        } else if (this.cs.isUndefinedORNull(this.travelForm.value.plan) || this.travelForm.value.plan == '0') {
          this.isTravelDataValid = false;
          this.commonToast("Please Select Plan");
        } else if (this.showAddOn) {
          if (this.cs.isUndefinedORNull(this.addOn) || this.cs.isUndefinedORNull(this.isProfessional)) {
            this.isTravelDataValid = false;
            this.commonToast("Please Select Additional Cover");
          } else {
            this.isTravelDataValid = true;
            this.calculateQuote(this.travelForm);
          }
        } else {
          this.isTravelDataValid = true;
          this.calculateQuote(this.travelForm);
        }
      } else {
        console.log(this.travelForm.value.tripDuration, this.travelForm.value.noOfTraveller);
        if (this.cs.isUndefinedORNull(this.travelForm.value.tripDuration)) {
          this.isTravelDataValid = false;
          this.commonToast("Please Select Trip Duration");
        } else if (this.cs.isUndefinedORNull(this.travelForm.value.policyStartDate)) {
          this.isTravelDataValid = false;
          this.commonToast("Please Select Start Date");
        } else if (!this.cs.isUndefinedORNull(this.travelForm.value.noOfTraveller)) {
          this.travellerDOBValidation();
          if (this.travellerDOBValidation()) {
            if (this.cs.isUndefinedORNull(this.travelForm.value.plan) || this.travelForm.value.plan == '0') {
              this.isTravelDataValid = false;
              this.commonToast("Please Select Plan");
            } else {
              this.isTravelDataValid = true;
              if (this.showAddOn) {
                if (this.cs.isUndefinedORNull(this.addOn) || this.cs.isUndefinedORNull(this.isProfessional)) {
                  this.isTravelDataValid = false;
                  this.commonToast("Please Select Additional Cover");
                } else {
                  this.isTravelDataValid = true;
                  this.calculateQuote(this.travelForm);
                }
              } else {
                this.calculateQuote(this.travelForm);
              }
            }
          };
        }
      }
    } else if (this.travelForm.value.subProduct == 'Family') {
      console.log('Family 1', this.cs.isUndefinedORNull(this.travelForm.value.plan))
      if (this.cs.isUndefinedORNull(this.travelForm.value.leavingDate)) {
        this.isTravelDataValid = false;
        this.commonToast("Please Select Leaving Date");
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.returningDate)) {
        this.isTravelDataValid = false;
        this.commonToast("Please Select Returning Date");
      } else if (!this.cs.isUndefinedORNull(this.travelForm.value.noOfTraveller)) {
        this.isTravelDataValid = false;
        if (this.travellerDOBValidation()) {
          if (this.cs.isUndefinedORNull(this.travelForm.value.plan) || this.travelForm.value.plan == '0') {
            this.isTravelDataValid = false;
            this.commonToast("Please Select Plan");
          } else {
            this.isTravelDataValid = true;
            this.calculateQuote(this.travelForm);
          }
        };
      } else {
        this.commonToast("Please Select number of Traveller");
      }
    } else {
      console.log()
      if (this.cs.isUndefinedORNull(this.travelForm.value.leavingDate)) {
        this.isTravelDataValid = false;
        this.commonToast("Please Select Leaving Date");
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.returningDate)) {
        this.isTravelDataValid = false;
        console.log(this.travelForm.value.noOfTraveller)
        this.commonToast("Please Select Returning Date");
      } else if (!this.cs.isUndefinedORNull(this.travelForm.value.noOfTraveller)) {
        this.isTravelDataValid = false;
        if (this.travellerDOBValidation()) {
          if (this.cs.isUndefinedORNull(this.travelForm.value.plan) || this.travelForm.value.plan == '0') {
            this.isTravelDataValid = false;
            this.commonToast("Please Select Plan");
          } else {
            this.isTravelDataValid = true;
            this.calculateQuote(this.travelForm);
          }
        };
      } else {
        // this.commonToast("Please Select number of Traveller");
      }
    }
  }

  travellerDOBValidation() {
    console.log(this.travelForm.value.noOfTraveller)
    if (this.travelForm.value.noOfTraveller == 1) {
      if (this.cs.isUndefinedORNull(this.travelForm.value.traveller1DOB)) {
        this.commonToast("Please Enter Traveller 1 DOB");
        return false;
      }
      return true;
    } else if (this.travelForm.value.noOfTraveller == 2) {
      if (this.cs.isUndefinedORNull(this.travelForm.value.traveller1DOB)) {
        this.commonToast("Please Enter Traveller 1 DOB");
        return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller2DOB)) {
        this.commonToast("Please Enter Traveller 2 DOB");
        return false;
      }
      return true;
    } else if (this.travelForm.value.noOfTraveller == 3) {
      if (this.cs.isUndefinedORNull(this.travelForm.value.traveller1DOB)) {
        this.commonToast("Please Enter Traveller 1 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller2DOB)) {
        this.commonToast("Please Enter Traveller 2 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller3DOB)) {
        this.commonToast("Please Enter Traveller 3 DOB"); return false;
      } else {
        return true;
      }
    } else if (this.travelForm.value.noOfTraveller == 4) {
      if (this.cs.isUndefinedORNull(this.travelForm.value.traveller1DOB)) {
        this.commonToast("Please Enter Traveller 1 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller2DOB)) {
        this.commonToast("Please Enter Traveller 2 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller3DOB)) {
        this.commonToast("Please Enter Traveller 3 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller4DOB)) {
        this.commonToast("Please Enter Traveller 4 DOB"); return false;
      } else {
        return true;
      }
    } else if (this.travelForm.value.noOfTraveller == 5) {
      if (this.cs.isUndefinedORNull(this.travelForm.value.traveller1DOB)) {
        this.commonToast("Please Enter Traveller 1 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller2DOB)) {
        this.commonToast("Please Enter Traveller 2 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller3DOB)) {
        this.commonToast("Please Enter Traveller 3 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller4DOB)) {
        this.commonToast("Please Enter Traveller 4 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller5DOB)) {
        this.commonToast("Please Enter Traveller 5 DOB"); return false;
      } else {
        return true;
      }
    } else if (this.travelForm.value.noOfTraveller == 6) {
      if (this.cs.isUndefinedORNull(this.travelForm.value.traveller1DOB)) {
        this.commonToast("Please Enter Traveller 1 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller2DOB)) {
        this.commonToast("Please Enter Traveller 2 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller3DOB)) {
        this.commonToast("Please Enter Traveller 3 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller4DOB)) {
        this.commonToast("Please Enter Traveller 4 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller5DOB)) {
        this.commonToast("Please Enter Traveller 5 DOB"); return false;
      } else if (this.cs.isUndefinedORNull(this.travelForm.value.traveller6DOB)) {
        this.commonToast("Please Enter Traveller 6 DOB"); return false;
      } else {
        return true;
      }
    }
  }

  calculateQuote(form: any) {
    console.log(form);
    this.categoryValidation().then(() => {
      if (form.value.subProduct == null || form.value.subProduct == undefined) {
        console.log("Invalid Subproduct");
        this.commonToast("Please Select Sub-Product");
        if (form.value.tripType == null || form.value.subProduct == undefined) {
          console.log("Invalid Trip Type");
          this.commonToast("Please Select Trip Type");
        }
      } else {
        console.log(this.travelForm);
        if (this.travelForm.status == 'VALID') {
          this.validateDate(form).then(() => {
            this.validateForKerala(form).then(() => {
              console.log(this.validForm);
              this.cs.showLoader = true;
              if (this.validForm) {
                console.log(this.travelForm.status);
                this.stateDetails = this.stateMaster.find((x: any) => x.StateName == this.travelForm.value.state);
                let body = {
                  "UserType": "AGENT",
                  "ipaddress": config.ipAddress,
                  "IsResIndia": "Yes",
                  "IsPlanUpgrade": "No",
                  "NoOfTravelers": form.value.noOfTraveller,
                  "IsImmigrantVisa": form.value.isImmigrantVisa,
                  "IsAnnualMultiTrip": this.IsAnnualMultiTrip,
                  "MaximumDurationPerTrip": form.value.tripDuration,
                  "coverageType": form.value.coverageType,
                  "planType": this.planType,
                  "IsVisitingUSCanadaYes": this.isVisitingUSCanada,
                  "IsVisitingSCHENGENYes": this.isVisitingSCHENGEN,
                  "IsVisitingOtherCountries": this.isVisitingOtherCountries,
                  "LeavingIndia": this.leavingDate,
                  "ReturnIndia": this.returningDate,
                  "TripDuration": form.value.noOfTripDays,
                  "IsIndividualPlan": this.isIndividual,
                  "IsFamilyPlan": this.isFamily,
                  "IsCoverNeededIndividual": this.IsCoverNeededIndividual,
                  "IsCoverNeededFloater": "False",
                  "Members": [
                    {
                      "DateOfBirth": moment(form.value.traveller1DOB).format('DD-MMM-YYYY'),
                      "PlanID": this.planId
                    },
                    {
                      "DateOfBirth": moment(form.value.traveller2DOB).format('DD-MMM-YYYY'),
                      "PlanID": this.planId
                    },
                    {
                      "DateOfBirth": moment(form.value.traveller3DOB).format('DD-MMM-YYYY'),
                      "PlanID": this.planId
                    },
                    {
                      "DateOfBirth": moment(form.value.traveller4DOB).format('DD-MMM-YYYY'),
                      "PlanID": this.planId
                    },
                    {
                      "DateOfBirth": moment(form.value.traveller5DOB).format('DD-MMM-YYYY'),
                      "PlanID": this.planId
                    },
                    {
                      "DateOfBirth": moment(form.value.traveller6DOB).format('DD-MMM-YYYY'),
                      "PlanID": this.planId
                    }
                  ],
                  "planIds": this.planId + ',',
                  "IsProfessionalSportCover": this.isProfessional,
                  "IsAdventureSportCover": this.isAdvanture,
                  "GSTStateCode": this.stateDetails.GSTStateCode,
                  "GSTStateName": form.value.state,
                  "isGSTRegistered": form.value.isKeralaGST
                }

                let body1 = Object.assign(body);
                console.log(body1);
                let str = JSON.stringify(body1);
                localStorage.setItem('quoteRequest', str);
                console.log(str);
                this.cs.postWithParams('/api/Travel/ITPFCalculatePremium', str).then((res: any) => {
                  console.log("Response", res);
                  if (res.StatusCode == '1') {
                    this.isQuoteGenerated = true;
                    localStorage.setItem('quoteResponse', JSON.stringify(res));
                    this.getData();
                    this.cs.showLoader = false;
                  } else {
                    console.log("Failed To calculate quate");
                    this.commonToast('Failed To Calculate Quote');
                    this.cs.showLoader = false;
                  }
                }).catch((err) => {
                  console.log(err);
                  this.cs.showLoader = false;
                });
              } else {
                this.commonToast('Kindly select GST details');
                this.cs.showLoader = false;
              }
            })
          });
        } else {
          console.log("Invalid Form");
          this.commonToast("Please filled all data");
          this.cs.showLoader = false;
        }
      }
    });
  }

  async commonToast(message: any) {
    const alert = await this.alertCtrl.create({
      header: 'ICICI Lombard',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  home(ev: any) {
    console.log("Home", ev);
    this.cs.goToHome();
  }

  getData(): Promise<any> {
    return new Promise((resolve: any) => {
      this.request = JSON.parse(localStorage.getItem('quoteRequest'));
      this.response = JSON.parse(localStorage.getItem('quoteResponse'));
      this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
      console.log(this.request);
      console.log(this.response);
      console.log(this.planDetails);
      resolve();
    });
  }

  convertToPolicy(ev: any) {
    console.log("Hello");
    this.router.navigateByUrl('travel-proposal');
  }

  showQuoteDetails(ev: any) {
    this.isQuoteDetails = !this.isQuoteDetails;
    if (this.isQuoteDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("quoteDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("quoteDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("quoteDetails").style.backgroundRepeat = 'no-repeat';
    }
  }

  showPremiumDetails(ev: any) {
    this.isPremiumDetails = !this.isPremiumDetails;
    if (this.isPremiumDetails) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("premiumDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("premiumDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("premiumDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("premiumDetails").style.backgroundRepeat = 'no-repeat';
    }
  }
  Recalculate() {
    this.calculateQuote(this.travelForm);
  }
  getCoronaDisclaimer(val) {
    if (val) {
      this.hideSubmit = true;
      this.isPremiumDetails = true;
    }
    else {
      this.hideSubmit = false;
      this.isQuoteGenerated = false; this.isPremiumDetails = false;
    }
  }

}
