import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelPremiumPage } from './travel-premium.page';

describe('TravelPremiumPage', () => {
  let component: TravelPremiumPage;
  let fixture: ComponentFixture<TravelPremiumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravelPremiumPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelPremiumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
