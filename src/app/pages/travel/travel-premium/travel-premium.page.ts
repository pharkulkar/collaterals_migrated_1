import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-travel-premium',
  templateUrl: './travel-premium.page.html',
  styleUrls: ['./travel-premium.page.scss'],
})
export class TravelPremiumPage implements OnInit {

  request:any;  response:any;  planDetails:any; loading:any; isRecalculate = "false";
  
  constructor(public router: Router, public location: Location, public cs:CommonService) { }

  ngOnInit() {
    this.getData();
  }

  getData():Promise<any>{
    return new Promise((resolve:any) => {
      this.request = JSON.parse(localStorage.getItem('quoteRequest'));
      this.response = JSON.parse(localStorage.getItem('quoteResponse'));
      this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
      console.log(this.request);
      console.log(this.response);
      console.log(this.planDetails);
      resolve();
    });
  }

  convertToPolicy(ev:any){
    console.log("Hello");
    this.router.navigateByUrl('travel-proposal');
  }

  recalculate(ev:any){
    this.location.back();
    this.cs.isBackFormTravelPremium = true;
    this.isRecalculate = "true";
    localStorage.setItem('reCalculate', this.isRecalculate);
  }

  home(ev:any){
    console.log("Home", ev);
    this.cs.goToHome();
  }

}
