import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelProposalPage } from './travel-proposal.page';

describe('TravelProposalPage', () => {
  let component: TravelProposalPage;
  let fixture: ComponentFixture<TravelProposalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravelProposalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelProposalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
