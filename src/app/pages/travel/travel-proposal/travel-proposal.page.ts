import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import * as moment from 'moment';
import { ToastController, AlertController } from '@ionic/angular';
import { config } from 'src/app/config.properties';
@Component({
  selector: 'app-travel-proposal',
  templateUrl: './travel-proposal.page.html',
  styleUrls: ['./travel-proposal.page.scss'],
})
export class TravelProposalPage implements OnInit {

  insuredForm: any; applicantForm: FormGroup;IsreadOnlyPermanentAdd:boolean=false;

  searchData: any; isShowUserList = false; customerName: any; memberObj=[]; gstChange = false;
  isBasic = false; isInsured = false; isApplicant = false; nomRelations: any; pinData: any;
  memberData: FormArray; request: any; response: any; planDetails: any; noOfMem = []; subType = 4;
  applRelations: any; index: any; showPermAdd = false; pinDataPer: any; pinDataValue: any;
  pinDataPermValue: any; showGST = false; isLoading = false; isUIN = false; isGSTIN = false; constitutionName: any; gstName: any; customerType: any;
  titleDetails: any; customerData: any; isSubmit = false; isExistance = false; inputLoading1 = false; inputLoading2 = false;
  minDOBDate: any; applAge: any; selecteUserData: any; memberArray = []; memNo = 0; isGSTValid = false;
  insuTitleDetails1: any; insuTitleDetails2: any; insuTitleDetails3: any; insuTitleDetails4: any; insuTitleDetails5: any; insuTitleDetails6: any;
  nomRela1: any; nomRela2: any; nomRela3: any; nomRela4: any; nomRela5: any; nomRela6: any; applRela1: any; applRela2: any; applRela3: any; applRela4: any;
  applRela5: any; applRela6: any; isCorraddSame: boolean = false; quoteRequest: any; memberArrayNEW = [];

  titles = [{ "id": "0", "val": "Mrs." }, { "id": "1", "val": "Mr." }, { "id": "2", "val": "Ms." }, { "id": "11", "val": "M/S" }, { "id": "12", "val": "Master" }];

  constituentArray = [{ "id": "0", "val": "Select Constitution of Business" }, { "id": "1", "val": "Non Resident Entity" }, { "id": "2", "val": "Foreign company registered in India" }, { "id": "3", "val": "Foreign LLP" },
  { "id": "4", "val": "Government Department" }, { "id": "5", "val": "Hindu Undivided Family" }, { "id": "6", "val": "LLP Partnership" }, { "id": "7", "val": "Local Authorities" }, { "id": "8", "val": "Partnership" },
  { "id": "9", "val": "Private Limited Company" }, { "id": "10", "val": "Proprietorship" }, { "id": "11", "val": "Others" }];

  customerTypeArray = [{ "id": "0", "val": "Select Customer Type" }, { "id": "21", "val": "General" }, { "id": "22", "val": "EOU/STP/EHTP" }, { "id": "23", "val": "Government" }, { "id": "24", "val": "Overseas" },
  { "id": "25", "val": "Related parties" }, { "id": "26", "val": "SEZ" }, { "id": "27", "val": "Others" }];

  gstStatusArray = [{ "id": "0", "val": "Select GST Status" }, { "id": "41", "val": "ARN Generated" }, { "id": "42", "val": "Provision ID Obtained" }, { "id": "43", "val": "To be commenced" },
  { "id": "44", "val": "Enrolled" }, { "id": "45", "val": "Not applicable" }];
  showUIN: boolean;
  errorMsg: boolean = true;
  showPhoneValidation: boolean;
  showEmailValidation: boolean;
  insuredMemberControl: FormArray;
  insuredMembersObj= [];
  showSegment:any=0;
  DOB: any;
  showRelation: boolean=true;
  showRelationNominee:boolean=true;
  pinDataList: any=[];
  pinDataListPermanentAdd: any=[];
  CityName: any=[];
  CityNamePermanentAddress: any=[];
  listOfChannel: any=[];
  SPName: any;
  channelList: any=[];
  listOfSPDetails: any=[];
  ParentIMID: any;

  constructor(public router: Router, public alertCtrl: AlertController, public formBuilder: FormBuilder, public cs: CommonService, public toastCtrl: ToastController) {

  }

  ngOnInit() {
    this.quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
    this.noOfMember(this.quoteRequest.NoOfTravelers);
    let date = new Date();
    console.log(date);
    date.setMonth(date.getMonth() - parseInt('216'));
    this.minDOBDate = moment(date).format('YYYY-MM-DD');
    console.log(date);
    this.createApplForm();
    this.getData();
    this.createInsuForm();
    this.getNomRelations();
    this.getApplRelations();
    this.show1('No');
    // this.SPConditionCheck().then(() => {
    //   this.getChannelNameList(this.ParentIMID);
    //   this.getSPNameList(this.ParentIMID);
    // })
  }

  // SPConditionCheck(): Promise<any> {
  //   return new Promise((resolve) => {
  //     this.cs.showLoader=true
  //     this.cs.SPConditionCheck().then((res: any) => {
  //       this.cs.showLoader=false
  //       if (res.StatusCode != 0) {
  //         this.ParentIMID = res.Parent_IM_ID;
  //         this.applicantForm.get('SPAlternateRMcode').setValidators([Validators.required]);
  //         this.applicantForm.get('SPChannelName').setValidators([Validators.required]);
  //         this.applicantForm.get('SPAlternateRMcode').updateValueAndValidity();
  //         this.applicantForm.get('SPChannelName').updateValueAndValidity();
  //         resolve();
  //       }
  //       else {
  //         this.presentAlert(res.StatusDesc + ' for SP Details')
  //         resolve();
  //       }  
  //     }).catch((err) => {
  //       this.cs.showLoader=false
  //       console.log(err)
  //       return;
  //     });
  //   });
  // }

  // getChannelNameList(Parent_IM_ID) {
  //   let body = {
  //     IM_ID : 'IM-46704', // Parent_IM_ID,
  //     Scope : "esbpfmaster"
  //   }
  //   this.cs.SPPostCall('/api/motor/GetChannelDetails', body).then((res:any) => {
  //     let i;
  //     for(i=0;i<res.length; i++)
  //     {
  //       this.listOfChannel[i] = res[i].channelName;
  //     }
  //   })
  // }

  // getChannelList(e:any)
  // {
  // }

  // getSPName(ev: any) {
  //   this.SPName = this.channelList.find(x => x.SP_Code == ev.target.value).SP_Name;
  //   this.applicantForm.patchValue({'SPName':this.SPName})
  // }

  // getSPNameList(Parent_IM_ID) {
  //   let body = {
  //     IM_ID :'IM-46704',// Parent_IM_ID,
  //     Scope : "esbpfmaster"
  //   }
  //   this.cs.SPPostCall('/api/motor/GetSPDetails', body).then((res:any) => {
  //     this.channelList = res;
  //     let j;
  //     for(j=0;j<res.length; j++)
  //     {
  //       this.listOfSPDetails[j] = res[j].SP_Code;
  //     }
  //   })
  // }

  


  createApplForm() {
    this.applicantForm = new FormGroup({
      aadhaarNo: new FormControl('', [Validators.maxLength(12)]),
      panNumber: new FormControl('', [Validators.maxLength(10)]),
      applTitle: new FormControl('', [Validators.required]),
      applName: new FormControl('', [Validators.required]),
      applDOB: new FormControl('', [Validators.required]),
      mobileNo: new FormControl('', [Validators.required, Validators.pattern('[0-9]+'), Validators.maxLength(10)]),
      applAdd1: new FormControl('', [Validators.required]),
      applAdd2: new FormControl(''),
      applPinCode: new FormControl('', [Validators.required, Validators.maxLength(6)]),
      applCity: new FormControl(''),
      applState: new FormControl(''),
      applEmail: new FormControl('', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.email]),   
      applLandmark: new FormControl(''),
      constitution: new FormControl(''),
      gstPanNo: new FormControl(''),
      customerType: new FormControl(''),
      gstRegStatus: new FormControl(''), 
      GSTIN: new FormControl(''),
      UIN: new FormControl(''), 
      permAdd1: new FormControl('',[Validators.required]),
      permAdd2: new FormControl(''),
      permLandmark: new FormControl(''),
      permPinCode: new FormControl('',[Validators.required]),
      permCity: new FormControl('',[Validators.required]),
      permState: new FormControl('',[Validators.required]),

      // SP Details
      SPAlternateRMcode: new FormControl(),
      SPName: new FormControl(),
      SPUniqueReferenceNo: new FormControl(),
      SPChannelName: new FormControl(),
      SPPrimaryRMCode: new FormControl(),
      SPSecondaryRMCode: new FormControl(),
      SPBancaField_01: new FormControl(),
      SPBancaField_02: new FormControl(),
      SPBancaField_03: new FormControl(),

     
    });
  }

  createInsuForm(){
    this.insuredForm = this.formBuilder.group({
      Members: this.formBuilder.array([])
    })
    this.addInsuredMembered()
  }

  addInsuredMembered(){
    this.insuredMemberControl = this.insuredForm.get('Members') as FormArray  
    for (let i = 0; i < this.memberArrayNEW.length; i++){
      this.DOB = moment(new Date(this.quoteRequest.Members[i].DateOfBirth)).format('YYYY-MM-DD')
      this.insuredMemberControl.push(this.createInsuredMemItem()); 
     
    }
  }

  createInsuredMemItem():FormGroup{
    return this.formBuilder.group({
      insuTitle:['',[Validators.required]],
      insuName:['',[Validators.required]],
      insuRela:['',[Validators.required]],
      insuDOB:[this.DOB,[Validators.required]],
      insuPassNo:['',[Validators.required]],
      insuAilment:['',[Validators.required]],
      insuNomName:['',[Validators.required]],
      insuNomRela:['',[Validators.required]],
    })
  }

  showAdultDiv(val: any) {
   this.showSegment = val.id
  }

  noOfMember(n: any) {
    let i = 0;
    for (i = 0; i < n; i++) {
      this.memberArrayNEW.push(i);
    }
  }

  getPincodeDetails(ev: any) {
    if(this.isExistance){
      this.getCityState(ev.target?ev.target.value:ev).then(() => {
        this.getPinCodeData(this.applicantForm);
      });
    }
   else if ((ev.target && ev.target.selectionStart == 6) || parseInt(ev.length) == 6) {
      this.inputLoading1 = true;
      this.getCityState(ev.target?ev.target.value:ev).then(() => {
        this.getPinCodeData(this.applicantForm);
      });
    } else {
      console.log("Invalid Pin");
      this.applicantForm.patchValue({ 'applCity': '' });
      this.applicantForm.patchValue({ 'applState': '' });
      this.inputLoading1 = false;
    }
  }

  getCityState(ev: any): Promise<any> {
    return new Promise((resolve) => {
      console.log('is existing---------------', this.isExistance);
      if (!this.isExistance) {
        if (ev.length == 6) {
          let body = ev;
          this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then((res:any) => {
            this.pinData = res;
            this.pinDataList = res.CityList;
            localStorage.setItem('CityList',JSON.stringify(res.CityList));
            this.applicantForm.patchValue({ 'applCity': this.pinData.CityList[0].CityName });
            this.applicantForm.patchValue({ 'applState': this.pinData.StateName });
            this.getCityName(this.pinData.CityList[0].CityName)
            this.inputLoading1 = false;
            resolve();
          });
        } else {
          this.inputLoading1 = false;
          console.log("Invalid Pin");
          this.applicantForm.patchValue({ 'applCity': '' });
          this.applicantForm.patchValue({ 'applState': '' });
          resolve();
        }
      } else {
        let body = ev;
        this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then((res:any) => {
          this.pinData = res;
          this.pinDataList = res.CityList;
          console.log(this.pinData);
          this.applicantForm.patchValue({ 'applCity': this.pinData.CityList[0].CityName });
          this.applicantForm.patchValue({ 'applState': this.pinData.StateName });
          this.getCityName(this.pinData.CityList[0].CityName)
          resolve();
        });
      }
    });
  }

  getPinCodeData(form: any): Promise<any> {
    return new Promise((resolve) => {
      this.getCityState(form.value.applPinCode);
      let body = { "Pincode": form.value.applPinCode, "CityID": this.pinData.CityList[0].CityID };
      this.cs.postWithParams('/api/RTOList/GetPincodeID', body).then((res: any) => {
        console.log("Get Pin Data Value", res);
        this.pinDataValue = res;
        resolve();
      });
    });
  }

  getPincodeDetailsPer(ev: any) {
    if ((ev.target && ev.target.selectionStart == 6) || parseInt(ev.length) == 6) {
      this.inputLoading1 = true;
      this.getCityStatePer(ev).then(() => {
        this.getPinCodeDataPerm(this.applicantForm).then(()=>{
        });
      });
    } else {
      console.log("Invalid Pin");
      this.applicantForm.patchValue({ 'permCity': '' });
      this.applicantForm.patchValue({ 'permState': '' });
      this.inputLoading1 = false;
    }
  }

  getCityStatePer(ev: any): Promise<any> {
    return new Promise((resolve: any) => {
      console.log(this.isExistance);
      if (!this.isExistance) {
        if (ev.target.value.length == 6) {
          let body = ev.target.value?ev.target.value:ev;
          this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then((res:any) => {
            this.inputLoading1=false
            this.pinDataPer = res;
            this.pinDataListPermanentAdd = res.CityList
            console.log(this.pinDataPer);
            this.applicantForm.patchValue({ 'permCity': this.pinDataPer.CityList[0].CityName });
            this.applicantForm.patchValue({ 'permState': this.pinDataPer.StateName });
            this.getCityNamePermanentAdd(this.pinDataPer.CityList[0].CityName)
            resolve();
          });
        } else {
          console.log("Invalid Pin");
          resolve();
        }
      } else {
        let body = ev.target ? ev.target.value:ev;
        this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then((res:any) => {
          this.pinDataPer = res;
          this.pinDataListPermanentAdd = res.CityList
          this.inputLoading1=false
          console.log(this.pinDataPer);
          this.applicantForm.patchValue({ 'permCity': this.pinDataPer.CityList[0].CityName });
          this.applicantForm.patchValue({ 'permState': this.pinDataPer.StateName });
          this.getCityNamePermanentAdd(this.pinDataPer.CityList[0].CityName)
          resolve();
        });
      }
    });
  }

  mobileValidation(ev: any): boolean {
    const charCode = (ev.which) ? ev.which : ev.keyCode;
    console.log(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  getCityName(value){
    this.CityName = this.pinDataList.filter(x=>x.CityName==value)
    this.applicantForm.patchValue({ 'applCity': value });
  }

  getCityNamePermanentAdd(value){
    this.CityNamePermanentAddress = this.pinDataListPermanentAdd.filter(x=>x.CityName==value)
    this.applicantForm.patchValue({ 'permCity': value });
  }


  getPinCodeDataPerm(form: any): Promise<any> {
    return new Promise((resolve) => {
      let body = { "Pincode": form.value.permPinCode, "CityID": this.pinDataPer.CityList[0].CityID };
      this.cs.postWithParams('/api/RTOList/GetPincodeID', body).then((res: any) => {
        console.log("Get Perm Pin Data ", res);
        this.pinDataPermValue = res;
        resolve();
      });
    });
  }

  getNomRelations() {
    this.cs.postWithParams('/api/Travel/GetNomineeRelationList', '').then((res: any) => {
      console.log("Relation Nominee", res.lstNomineeRelationshipResponse);
      this.nomRelations = res;
      this.showRelationNominee = false;
    });
  }

  getApplRelations() {
    let body = { "CoverageType": this.request.coverageType, "SubType": this.subType };
    let str = JSON.stringify(body);
    this.cs.postWithParams('/api/Travel/GetRelationWithApplicant', str).then((res: any) => {
      console.log("Relation Appl", res.lstApplicantRelationShipResponse);
      this.applRelations = res;
      this.showRelation = false
    });
  }
  getInsuTitle(ev, index) {
    this.insuredForm.value.Members[index].insuTitle = ev.target.value;
  }

  isGST(msg: any) {
    console.log(msg);
    if (msg == 'Yes') {
      this.showGST = true;
      this.isGSTIN = true;
      this.applicantForm.get('constitution').setValidators([Validators.required]);
      this.applicantForm.get('gstPanNo').setValidators([Validators.required]);
      this.applicantForm.get('customerType').setValidators([Validators.required]);
      this.applicantForm.get('gstRegStatus').setValidators([Validators.required]);
      this.applicantForm.get('GSTIN').setValidators([Validators.required]);
     // this.applicantForm.get('UIN').setValidators([Validators.required]);
      this.applicantForm.get('constitution').updateValueAndValidity();
      this.applicantForm.get('gstPanNo').updateValueAndValidity();
      this.applicantForm.get('customerType').updateValueAndValidity();
      this.applicantForm.get('gstRegStatus').updateValueAndValidity();
      this.applicantForm.get('GSTIN').updateValueAndValidity();
      //this.applicantForm.get('UIN').updateValueAndValidity();
    } else {
      this.showGST = false;
      this.isUIN = false;
      this.isGSTIN = false;
      this.applicantForm.patchValue({ 'GSTIN': "" });
      this.applicantForm.patchValue({ 'UIN': "" });
      this.applicantForm.patchValue({ 'gstRegStatus': "" });
      this.applicantForm.patchValue({ 'customerType': "" });
      this.applicantForm.patchValue({ 'gstPanNo': "" });
      this.applicantForm.patchValue({ 'constitution': "" });
      this.applicantForm.get('constitution').clearValidators();
      this.applicantForm.get('gstPanNo').clearValidators();
      this.applicantForm.get('customerType').clearValidators();
      this.applicantForm.get('gstRegStatus').clearValidators();
      this.applicantForm.get('GSTIN').clearValidators();
      this.applicantForm.get('UIN').clearValidators();
      this.applicantForm.get('constitution').updateValueAndValidity();
      this.applicantForm.get('gstPanNo').updateValueAndValidity();
      this.applicantForm.get('customerType').updateValueAndValidity();
      this.applicantForm.get('gstRegStatus').updateValueAndValidity();
      this.applicantForm.get('GSTIN').updateValueAndValidity();
      this.applicantForm.get('UIN').updateValueAndValidity();
    }
  }

  isGSTINUIN(msg: any) {
    if (msg == 'GSTIN' && this.showGST) {
      this.isGSTIN = true;
      this.showUIN = false
      this.isUIN = false;
      this.applicantForm.get('GSTIN').setValidators([Validators.required]);
      this.applicantForm.get('GSTIN').updateValueAndValidity();
      this.applicantForm.patchValue({ 'UIN': "" });
      this.applicantForm.get('UIN').clearValidators();
      this.applicantForm.get('UIN').updateValueAndValidity();
    } else if (msg == 'UIN' && this.showGST) {
      this.isUIN = true;
      this.isGSTIN = false;
      this.showUIN = true;
      this.applicantForm.get('UIN').setValidators([Validators.required]);
      this.applicantForm.get('UIN').updateValueAndValidity();
      this.applicantForm.patchValue({ 'GSTIN': "" });
      this.applicantForm.get('GSTIN').clearValidators();
      this.applicantForm.get('GSTIN').updateValueAndValidity();
    } else {
      this.applicantForm.get('GSTIN').clearValidators();
      this.applicantForm.get('UIN').clearValidators();
      this.applicantForm.get('GSTIN').updateValueAndValidity();
      this.applicantForm.get('UIN').updateValueAndValidity();
    }
  }

  show1(msg: any) {
    console.log(msg);
    if (msg == 'No') {
      this.IsreadOnlyPermanentAdd = false
      this.showPermAdd = false; this.isCorraddSame = false;
      this.applicantForm.patchValue({ 'permAdd1': '' });
      this.applicantForm.patchValue({ 'permAdd2': '' });
      this.applicantForm.patchValue({ 'permLandmark': '' });
      this.applicantForm.patchValue({ 'permPinCode': '' });
      this.applicantForm.patchValue({ 'permCity': null });
      this.applicantForm.patchValue({ 'permState': '' });
      this.pinDataPermValue = '';
      this.pinDataPer = '';
    } else {
      this.IsreadOnlyPermanentAdd = true
      this.pinDataListPermanentAdd = this.pinDataList
      this.showPermAdd = true; this.isCorraddSame = true;
      this.applicantForm.patchValue({ 'permAdd1': this.applicantForm.value.applAdd1 });
      this.applicantForm.patchValue({ 'permAdd2': this.applicantForm.value.applAdd2 });
      this.applicantForm.patchValue({ 'permLandmark': this.applicantForm.value.applLandmark });
      this.applicantForm.patchValue({ 'permPinCode': this.applicantForm.value.applPinCode });
      this.applicantForm.patchValue({ 'permCity': this.applicantForm.value.applCity });
      this.applicantForm.patchValue({ 'permState': this.applicantForm.value.applState });
      this.getCityNamePermanentAdd(this.applicantForm.value.applCity)
      this.pinDataPermValue = this.pinDataValue;
      this.pinDataPer = this.pinData;
    }
  }

  getGSTName(ev: any) {
    console.log(ev, this.isExistance);
    if (!this.isExistance) {
      let gstNameData = this.gstStatusArray.find((x: any) => x.id == ev.target.value);
      this.gstName = gstNameData.val;
    } else {
      let gstNameData = this.gstStatusArray.find((x: any) => x.id == ev);
      this.gstName = gstNameData.val;
    }
  }

  getCustTypeName(ev: any) {
    console.log(this.isExistance, ev);
    if (!this.isExistance) {
      let custTypeData = this.customerTypeArray.find((x: any) => x.id == ev.target.value);
      this.customerType = custTypeData.val;
    } else {
      let custTypeData = this.customerTypeArray.find((x: any) => x.id == ev);
      this.customerType = custTypeData.val;
    }
  }

  getConstitute(ev: any) {
    if (!this.isExistance) {
      let constData = this.constituentArray.find((x: any) => x.id == ev.target.value);
      this.constitutionName = constData.val;
    } else {
      let constData = this.constituentArray.find((x: any) => x.id == ev);
      this.constitutionName = constData.val;
    }
  }

  getData(): Promise<any> {
    return new Promise((resolve: any) => {
      this.request = JSON.parse(localStorage.getItem('quoteRequest'));
      this.response = JSON.parse(localStorage.getItem('quoteResponse'));
      this.planDetails = JSON.parse(localStorage.getItem('travelPlan'));
      console.log(this.request);
      if (this.request.NoOfTravelers == 2) {
        this.noOfMem.push(1);
      } else {
        this.noOfMem = [];
        for (let i = 1; i <= this.request.NoOfTravelers; i++) {
          this.noOfMem.push(i);
        }
        console.log(this.noOfMem);
      }
      if (this.request.IsAnnualMultiTrip == 'Yes') {
        this.subType = 5;
      } else {
        this.subType = 4;
      }
      resolve();
    });
  }

  showBasic(ev: any) {
    this.isBasic = !this.isBasic;
    if (this.isBasic) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("basic").style.background = "url(" + imgUrl + ")";
      document.getElementById("basic").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("basic").style.background = "url(" + imgUrl + ")";
      document.getElementById("basic").style.backgroundRepeat = 'no-repeat';
    }
  }

  showInsured(ev: any) {
    this.isInsured = !this.isInsured;
    if (this.isInsured) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("insured").style.background = "url(" + imgUrl + ")";
      document.getElementById("insured").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("insured").style.background = "url(" + imgUrl + ")";
      document.getElementById("insured").style.backgroundRepeat = 'no-repeat';
    }
  }

  showApplicant(ev: any) {
    this.isApplicant = !this.isApplicant;
    if (this.isApplicant) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("applicant").style.background = "url(" + imgUrl + ")";
      document.getElementById("applicant").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("applicant").style.background = "url(" + imgUrl + ")";
      document.getElementById("applicant").style.backgroundRepeat = 'no-repeat';
    }
    if (this.request.GSTStateName == 'KERALA') {
      if (this.request.isGSTRegistered) {
        this.gstChange = true;
        this.showGST = true;
        this.applicantForm.get('constitution').setValidators([Validators.required]);
        this.applicantForm.get('gstPanNo').setValidators([Validators.required]);
        this.applicantForm.get('customerType').setValidators([Validators.required]);
        this.applicantForm.get('gstRegStatus').setValidators([Validators.required]);
        this.applicantForm.get('constitution').updateValueAndValidity();
        this.applicantForm.get('gstPanNo').updateValueAndValidity();
        this.applicantForm.get('customerType').updateValueAndValidity();
        this.applicantForm.get('gstRegStatus').updateValueAndValidity();
      } else {
        this.gstChange = false;
      }
    } else {
      this.gstChange = false;
    }
  }

  isExist(ev: any) {
    console.log(ev);
    if (ev == 'No') {
      this.isExistance = false;
      document.getElementById("myNav1").style.height = "0%";
      this.applicantForm.reset();
    } else {
      this.isExistance = true;
      document.getElementById("myNav1").style.height = "100%";
      this.applicantForm.reset();
      this.customerName = ''; this.isShowUserList = false;
    }
  }

  searchCustomer(ev: any) {
    console.log(ev);

    let request = {
      "CustomerName": this.customerName,
      "CustomerEmail": "",
      "CustomerMobile": "",
      "CustomerStateCode": ""
    }
    let str = JSON.stringify(request);
    console.log(str);
    this.cs.showLoader = true;
    this.cs.postWithParams('/api/Customer/SearchCustomer', str).then((res: any) => {
      console.log("search user", res);
      this.searchData = res;
      if (this.searchData.length > 0) {
        this.isShowUserList = true;
        this.cs.showLoader = false;
      } else {
        this.isShowUserList = false;
        this.cs.showLoader = false;
      }
    });
  }

  selectedCust(cust: any) {
    console.log("Selected Data", cust);
    this.getCustDetails(cust);
    this.isExistance = true;
    document.getElementById("myNav1").style.height = "0%";
  }

  @ViewChild('isGSTNO')isGSTNO;
  @ViewChild('isGSTYES')isGSTYES;

  getCustDetails(data: any) {
    console.log(data);
    this.cs.showLoader = true;
    this.cs.getWithParams('/api/Customer/GetCustomerDetails/' + data.CustomerID).then((res: any) => {
      console.log(res);
      if (res != undefined || res != null) {
        this.cs.showLoader = false;
        this.selecteUserData = res;
        if(res.isPermanentAsPresent){
          this.showPermAdd = true;
          this.isCorraddSame = true
          this.IsreadOnlyPermanentAdd=true
        }else{
          this.showPermAdd = false;
          this.isCorraddSame = false
        }
    
        this.getPincodeDetails(this.selecteUserData.PresentAddrPincodeText);
        this.getPincodeDetailsPer(this.selecteUserData.PresentAddrPincodeText);
        this.titleDetails = this.titles.find((x: any) => x.id == this.selecteUserData.TitleValue);
        this.applicantForm.patchValue({ 'applTitle': this.titleDetails.val });
        this.applicantForm.patchValue({ 'applName': this.selecteUserData.Name });
        this.applicantForm.patchValue({ 'applDOB': moment(this.selecteUserData.DateOfBirth, 'DD/MM/YYYY').format('YYYY-MM-DD') });
        this.applicantForm.patchValue({ 'applEmail': this.selecteUserData.EmailAddress });
        this.applicantForm.patchValue({ 'mobileNo': this.selecteUserData.MobileNumber });
        this.applicantForm.patchValue({ 'applAdd1': this.selecteUserData.PresentAddrLine1 });
        this.applicantForm.patchValue({ 'applAdd2': this.selecteUserData.PresentAddrLine2 });
        this.applicantForm.patchValue({ 'applLandmark': this.selecteUserData.PresentAddrLandmark });
        this.applicantForm.patchValue({ 'applPinCode': this.selecteUserData.PresentAddrPincodeText });
        this.applicantForm.patchValue({ 'applCity': this.selecteUserData.PresentAddrCityText });
        this.applicantForm.patchValue({ 'applState': this.selecteUserData.PresentAddrStateText });
        this.applicantForm.patchValue({ 'permAdd1': this.selecteUserData.PresentAddrLine1 });
        this.applicantForm.patchValue({ 'permAdd2': this.selecteUserData.PresentAddrLine2 });
        this.applicantForm.patchValue({ 'permLandmark': this.selecteUserData.PresentAddrLandmark });
        this.applicantForm.patchValue({ 'permPinCode': this.selecteUserData.PresentAddrPincodeText });
        // this.applicantForm.patchValue({ 'permCity': this.selecteUserData.PresentAddrCityText });
        // this.applicantForm.patchValue({ 'permState': this.selecteUserData.PresentAddrStateText });
        this.cs.isDataNullorUndefined(this.selecteUserData.GSTDetails)
        if (this.cs.validArray.includes('Invalid')) {
          this.isGSTNO.nativeElement.click() 
        } else {
          this.isGSTYES.nativeElement.click()
          this.applicantForm.patchValue({ 'GSTIN': this.selecteUserData.GSTDetails.GSTIN_NO });
          this.applicantForm.patchValue({ 'UIN': this.selecteUserData.GSTDetails.UIN_NO });
          this.applicantForm.patchValue({ 'gstRegStatus': this.selecteUserData.GSTDetails.GST_REGISTRATION_STATUS });
          this.applicantForm.patchValue({ 'customerType': this.selecteUserData.GSTDetails.CUSTOMER_TYPE });
          this.applicantForm.patchValue({ 'gstPanNo': this.selecteUserData.GSTDetails.PAN_NO });
          this.applicantForm.patchValue({ 'constitution': this.selecteUserData.GSTDetails.CONSTITUTION_OF_BUSINESS });
          this.isGSTIN = this.selecteUserData.isGSTINApplicable;
        }
        
        this.cs.showLoader = false;

      } else {
        console.log("Invalid Data");
        this.cs.showLoader = false;
      }
    });
  }

@ViewChild('newCustomer')newCustomer

  closeNav(ev: any) {
    this.newCustomer.nativeElement.checked=true
    document.getElementById("myNav1").style.height = "0%";
  }


  validatePassport(ev: any) {
    console.log("Event", ev.target.value);
    var regExp = new RegExp("^([A-Z a-z]){1}([0-9]){7}$");
    console.log(regExp.test(ev.target.value));
    if (regExp.test(ev.target.value) == true) {
      let body = {
        "PassportFilter": {
          "Passport1": ev.target.value
        }
      }
      let str = JSON.stringify(body);
      this.cs.postWithParams('/api/travel/FilterPassport', str).then((res: any) => {
        console.log("Passport", res);
        if (res.StatusCode == '0') {
          this.showToast('Invalid Passport Number');
        }
      });
    } else {
      this.showToast('Invalid Passport Number eg. A1111111');
    }

  }

  async showToast(msg: any) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  calProposal(applform: any, insuForm: any) {
    let str = JSON.stringify(applform.value);
    this.isSubmit = true;
    let is_memeber_Valid = this.validateMember();
    if (is_memeber_Valid) {
      this.validateForm().then(() => {
          this.errorMsg = false;  
          this.createCustomer(applform).then(() => {
            this.createMemberObj(this.insuredForm).then(()=>{
              this.travelProposal(applform, insuForm).then(() => {
              });
            })
          });
      });
    } else {
      //this.showToast('Kindly fill Insured Member Data Properly');
    }

  }

  async presentAlert(message: any) {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  validateMobileLength(e: any, as: any) {
    this.showPhoneValidation = true;
    if (e.target.value.length > 10) {
      this.presentAlert('Mobile No cannot more than 10 digits');
      this.applicantForm.patchValue({ 'mobileNo': null });
    }
    else if (e.target.value.length === 10) {
      this.showPhoneValidation = false;
    }

  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validateMobile(mobile) {
    var mob = /^[0-9]{10}$/;
    return mob.test(mobile);
  }
  validateAdhaarNo(addharno) {
    var re = /^[0-9]{12}$/;
    return re.test(addharno);
  }
  validatePanNo(panno) {
    var re = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
    return re.test(panno);
  }
  validateGST(gst, flag) {
    if (flag == 'gst') {
      var re = /^[0-9]{2}[A-Z]{3}[PCHABGJLEFT][A-Z][0-9]{4}[A-Z]{1}[0-9A-Z]{1}[Z]{1}[0-9A-Z]{1}$/;
    } else {
      var re = /^[0-9]{2}[0-9]{2}[A-Z]{3}[0-9]{5}[A-Z]{3}$/;
    }
    let gstvalid = re.test(gst);
    if (gstvalid) {
      let gstcodeObtained = gst.substring(0, 2);
      let stateData = JSON.parse(localStorage.getItem('selectedStateData'));
      let gstcode = stateData.GSTStateCode;
      if (parseInt(gstcodeObtained) == parseInt(gstcode)) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return gstvalid;
    }
  }

  validateForm(): Promise<any> {
    return new Promise((resolve)=>{
      let key = Object.keys(this.applicantForm.controls)
      for(let i=0;i< key.length;i++){
        const abstractControl = this.applicantForm.get(key[i])
        if(abstractControl && !abstractControl.valid){
          this.presentAlert('Kindly Enter Valid Applicant '+ key[i]);
          return;
        }
      }
      resolve();
    })
    
  }

  validateForExistance(): Promise<any> {
    return new Promise((resolve) => {
      let key = Object.keys(this.applicantForm.controls)
      if (this.showGST) {
        for(let i=19;i< key.length;i++){
          const abstractControl = this.applicantForm.get(key[i])
          if(abstractControl && !abstractControl.valid){
            this.presentAlert('Kindly Enter Valid Applicant '+ key[i]);
            return;
          }
        }
        console.log("Form valid");
        this.isGSTValid = true;
        this.isExistance = false;
        resolve();
      }
    });
  }

  createCustomer(form: any): Promise<any> {
    return new Promise((resolve) => {
      console.log(form.value);
      let body = {
        "CustomerID": 0,
        "HasAddressChanged": true,
        "SetAsDefault": true,
        "TitleText": form.value.applTitle,
        "TitleValue": '1', // pass id
        "Name": form.value.applName,
        "DateOfBirth": moment(form.value.applDOB).format('DD-MMM-YYYY'),
        "MaritalStatusValue": -1,
        "MaritalStatusText": "",
        "OccupationValue": -1,
        "OccupationText": "",
        "AnnualIncomeValue": -1,
        "AnnualIncomeText": "",
        "IdentityProofValue": "2",
        "IdentityProofText": "Aadhaar Card",
        "IdentityNumber": "640660536358",
        "PresentAddrLine1": form.value.applAdd1,
        "PresentAddrLine2": form.value.applAdd2,
        "PresentAddrLandmark": form.value.applLandmark,
        "PresentAddrCityValue": this.CityName[0].CityID, //pass id
        "PresentAddrCityText": form.value.applCity,
        "PresentAddrStateValue": this.pinData.StateId,
        "PresentAddrStateText": form.value.applState,
        "PresentAddrPincodeValue": this.pinDataValue.Details[0].Value,
        "PresentAddrPincodeText": form.value.applPinCode,
        "EmailAddress": form.value.applEmail,
        "MobileNumber": form.value.mobileNo,
        "LandlineNumber": "",
        "EmailAlternate": "",
        "MobileAlternate": "",
        "PANNumber": form.value.panNumber,
        "isPermanentAsPresent": this.showPermAdd,
        "PermanentAddrLine1": form.value.permAdd1,
        "PermanentAddrLine2": form.value.permAdd2,
        "PermanentAddrLandmark": form.value.permLandmark,
        "PermanentAddrCityValue": this.CityNamePermanentAddress[0].CityID,
        "PermanentAddrCityText": form.value.permCity,
        "PermanentAddrStateValue": this.pinDataPer.StateId, //pass id
        "PermanentAddrStateText": form.value.permState,
        "PermanentAddrPincodeValue": this.pinDataPermValue.Details[0].Value,
        "PermanentAddrPincodeText": form.value.permPinCode,
        "isGSTINApplicable": this.isGSTIN,
        "isUINApplicable": this.isUIN,
        "GSTDetails": {
          "GSTIN_NO": this.isGSTIN ? form.value.GSTIN : form.value.UIN,
          "CONSTITUTION_OF_BUSINESS": form.value.constitution,
          "CONSTITUTION_OF_BUSINESS_TEXT": this.constitutionName,
          "CUSTOMER_TYPE": form.value.customerType,
          "CUSTOMER_TYPE_TEXT": this.customerType,
          "PAN_NO": form.value.gstPanNo,
          "GST_REGISTRATION_STATUS": form.value.gstRegStatus,
          "GST_REGISTRATION_STATUS_TEXT": this.gstName
        }
      }
      console.log("Create Cust Body", body);
      let str = JSON.stringify(body);
      localStorage.setItem('customerRequest', str);
      console.log("Create Cust stringify", str);
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/customer/saveeditcustomer', str).then((res: any) => {
        if ((res.PFCustomerID == '' && res.CustomerID == '') || (res.PFCustomerID == undefined && res.CustomerID == undefined)) {
          this.showToast("Failed to create customer");
          this.cs.showLoader = false;
          resolve();
        } else {
          console.log("Travel create Cust", res);
          this.customerData = res;
          this.cs.showLoader = false;
          resolve();
        }
      }).catch((err) => {
        console.log(err);
        this.showToast(err.error.Message);
        this.cs.showLoader = false;
        return;
       // resolve();
      });
    });
  }

  createMemberObj(form: any): Promise<any> {
    return new Promise((resolve) => {
      for (let i = 0; i < form.value.Members.length; i++) { 
          this.memberObj.push({"Title": form.value["Members"][i].insuTitle,
            "InsuredName": form.value["Members"][i].insuName, "PassportNumber": form.value["Members"][i].insuPassNo, "MotherMaidenName": "NA",
            "NomineeName": form.value["Members"][i].insuNomName, "AilmentName": form.value["Members"][i].insuAilment, "DateOfBirth": form.value["Members"][i].insuDOB,
            "RelationshipName": form.value["Members"][i].insuRela, "RelationshipID": form.value["Members"][i].insuRela, 
          });
      }
     
      resolve();
    });
  }

  round(){
    return Math.round(12/this.memberArrayNEW.length)
  }

  travelProposal(form: any, form2: any): Promise<any> {
    return new Promise((resolve) => {
      let body = {
        "UserType": "AGENT",
        "ipaddress": config.ipAddress,
        "Product": "Travel",
        "ProductType": "International",
        "PolicyID": this.response.PolicyID,
        "CustomerID": this.customerData.CustomerID,
        "GSTStateCode": this.request.GSTStateCode,
        "GSTStateName": this.request.GSTStateName,
        "MailPolicyCopy": "NA",
        "SourcingCode": "NA",
        "PanCardNo": this.applicantForm.value.panNumber,
        "Members": this.memberObj,
        "GSTApplicable": "FALSE",
        "HasIbankRelationship": "FALSE",
        "iBankRelationship": { "IsIbankRelationship": "FALSE" },
        "PF_CUSTOMERID": this.customerData.PFCustomerID
      }

      let str = JSON.stringify(body);
      console.log(str);
      localStorage.setItem('proposalRequest', str);
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/Travel/SaveProposalDetails1', str).then((res: any) => {
        console.log("Response", res);
        if (res.StatusCode == 1) {
          localStorage.setItem('proposalResponse', JSON.stringify(res));
          this.router.navigateByUrl('travel-summary');
          this.cs.showLoader = false;
          resolve();
        } else {
          this.showToast(res.StatusMessage);
          console.log("Something Went wrong", res);
          this.cs.showLoader = false;
          this.memberObj=[]
          resolve();
        }
      });
    });
  }

  getAilment(ev: any) {
    console.log(ev.target.value);
    if (ev.target.value != 'none') {
      let msg = 'Due to the ailment, We will not be able to process your request online. Please contact our customer support for further details.'
      this.presentAlertConfirm(msg);
      this.insuredForm.patchValue({ 'insu1Ailment': 'none' }); this.insuredForm.patchValue({ 'insu2Ailment': 'none' });
      this.insuredForm.patchValue({ 'insu1Ailment': 'none' }); this.insuredForm.patchValue({ 'insuAilment': 'none' });
      this.insuredForm.patchValue({ 'insu1Ailment': 'none' }); this.insuredForm.patchValue({ 'insu1Ailment': 'none' });
    }
  }

  async presentAlertConfirm(msg: any) {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      message: msg,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }

  alertMsgForInsured = [
    { "val": "Insured Title" },
    { "val": "Insured Name" },
    { "val": "Relationship with Applicant" },
    { "val": "Date of Birth" },
    { "val": "Passport Number" },
    { "val": "Illness/Disease/Ailment/Deformity" },
    { "val": "Nominee Name" },
    { "val": "Relationship with Nominee" }
  ];


  validateMember() {
    let Key = []
    for (let i = 0; i < this.insuredForm.value.Members.length; i++) {
      let key = Object.keys(this.insuredForm.value.Members[i])
      Key['Adult' + i] = key
      var j = i
    }
    for (let i = 0; i <= j; i++) {
      for (let k = i; k <= i; k++) {
        const abstractControl = this.insuredForm.get('Members').controls[k]
        for(let m = 0; m < 8; m++){
          if (abstractControl.controls[Key["Adult"+i][m]] && !abstractControl.controls[Key["Adult"+i][m]].valid) {
            this.presentAlert("Please Enter "+this.alertMsgForInsured[m].val+" For Adult "+ (i+1));
            return false;
          }
        }    
      }
    }
    return true;
  }

  getdate1(ev: any) {
    setTimeout(function () {
      $('button').removeClass('cdk-focused cdk-program-focused');
    });
  }

  home(ev: any) {
    console.log("Home", ev);
    this.cs.goToHome();
  }

}
