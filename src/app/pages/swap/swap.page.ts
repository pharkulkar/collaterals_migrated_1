import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { config } from 'src/app/config.properties';

@Component({
  selector: 'app-swap',
  templateUrl: './swap.page.html',
  styleUrls: ['./swap.page.scss'],
})
export class SwapPage implements OnInit {


  plan: any;
  loading: any;
  authToken: any;
  agentData: any;
  disabledFeatureData: any;
  agentAuthToken: any;
  showHome: boolean;
  deal: any; decodeddeal: any
  constructor(public cs: CommonService, public alertCtrl: AlertController, public router: Router, public toastController: ToastController, public loadingCtrl: LoadingController, public activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getParamas();
  }

  getParamas() {
    this.showHome = false;
    this.cs.showSwapLoader = true; window.localStorage.clear();
    this.activeRoute.queryParams.forEach((params) => {
      console.log("Par", params);
      this.deal = encodeURIComponent(params.deal);
      this.decodeddeal = params.deal;
      this.plan = (params.productType).toUpperCase();
      localStorage.setItem('planFormURL', JSON.stringify(this.plan));
      localStorage.setItem('custplan', (this.plan));
      console.log(this.plan);
      if (this.deal != undefined) {
        this.cs.showSwapLoader = false;
        this.getToken(this.deal).then(() => {
          this.getDisabledFeaturesList(this.authToken).then(() => {
            this.getCustomerDetails(this.authToken);
          });
        });
      } else {
        this.presentToast('Failed To autherized user...Please try again');
        this.cs.showSwapLoader = false;
      }
    });
  }

  getToken(deal: any): Promise<any> {
    console.log("Token", this.deal);
    return new Promise((resolve) => {
      this.cs.showSwapLoader = true;
      this.cs.getAuthToken('/api/Agent/GetDealAgentMasterData?Deal=' + this.deal).then((res: any) => {
        console.log("", res);
        if (res != '' && res != 'FAILURE') {
          this.authToken = res.Basic;
          localStorage.setItem('authToken', JSON.stringify(this.authToken));
          this.cs.authToken = this.authToken;
          resolve();
        } else {
          this.presentAlert2('Something went wrong please contact to your partner');
          this.cs.showSwapLoader = false;
        }

      }).catch((err) => {
        console.log("Error", err);
        this.presentAlert2('Something went wrong please contact to your partner');
        this.cs.showSwapLoader = false;
        resolve();
      });
    });
  }

  getDisabledFeaturesList(deal: any): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.getWithParams1('/api/Agent/GetDisableFeatures', deal).then((res: any) => {
        this.disabledFeatureData = res.DisableResponseList;
        let isKeralacess = this.disabledFeatureData.find((x: any) => x.Key == 'isKeralaCessEnabled').Value;
        localStorage.setItem('isKeralaCessEnabled', isKeralacess);
        resolve();
      });
    });
  }

  getCustomerDetails(deal: any): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.swapAppMaster('/api/agent/AgentAppMasterData', deal).then((res: any) => {
        console.log("Auth", res);
        this.showHome = true;
        localStorage.setItem('userData', JSON.stringify(res));
        this.agentData = res;
        if ((this.agentData.MappedProduct.Health.isHealthBoosterMapped || this.agentData.MappedProduct.Health.isHealthCHIMapped || this.agentData.MappedProduct.Health.HealthCHISubProducts.Visible_iHealth || this.agentData.MappedProduct.Health.isHealthPPAPMapped || this.agentData.MappedProduct.Health.isHospifundMapped || this.agentData.MappedProduct.Health.isHealthASPMapped) && ((this.plan == 'HEALTHCHI') || (this.plan == 'HEALTHCHI-IHEALTH') || (this.plan == 'HEALTHBOOSTER') || (this.plan == 'HEALTHPPAP') || (this.plan == 'HEALTHHOSPIFUND') || (this.plan == 'HEALTHASP'))) {
          console.log("Health Show");
          if (this.plan == 'HEALTHCHI' && this.agentData.MappedProduct.Health.isHealthCHIMapped && (this.decodeddeal == decodeURIComponent(this.agentData.MappedProduct.Health.HealthCHIDealId))) {
            this.router.navigateByUrl('health');
            this.cs.showSwapLoader = false;
            resolve();
          }
          else if (this.plan == 'HEALTHCHI-IHEALTH' && this.agentData.MappedProduct.Health.isHealthCHIMapped && this.agentData.MappedProduct.Health.HealthCHISubProducts.Visible_iHealth && (this.decodeddeal == decodeURIComponent(this.agentData.MappedProduct.Health.HealthCHIDealId))) {
            this.router.navigateByUrl('health');
            this.cs.showSwapLoader = false;
            resolve();
          }
          else if (this.plan == 'HEALTHBOOSTER' && this.agentData.MappedProduct.Health.isHealthBoosterMapped && (this.decodeddeal == decodeURIComponent(this.agentData.MappedProduct.Health.HealthBoosterDealId))) {
            this.router.navigateByUrl('health');
            this.cs.showSwapLoader = false;
            resolve();
          }
          else if (this.plan == 'HEALTHPPAP' && this.agentData.MappedProduct.Health.isHealthPPAPMapped && (this.decodeddeal == decodeURIComponent(this.agentData.MappedProduct.Health.HealthPPAPDealId))) {
            this.router.navigateByUrl('health');
            this.cs.showSwapLoader = false;
            resolve();
          }
          else if (this.plan == 'HEALTHHOSPIFUND' && this.agentData.MappedProduct.Health.isHospifundMapped) {
            this.router.navigateByUrl('health');
            this.cs.showSwapLoader = false;
            resolve();
          }
          else if (this.plan == 'HEALTHASP' && this.agentData.MappedProduct.Health.isHealthASPMapped) {
            this.router.navigateByUrl('health');
            this.cs.showSwapLoader = false;
            resolve();
          }
          else {
            this.presentAlert();
            this.cs.showSwapLoader = false;
          }
        }
        else if (this.plan == 'TRAVELINT' && this.agentData.MappedProduct.Travel.isTRAVELINTERNATIONALMapped) {
          this.router.navigateByUrl('travel-quote');
          this.cs.showSwapLoader = false;
          resolve();
        }
        else {
          this.presentAlert();
          this.cs.showSwapLoader = false;
        }
      });
    });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'ICICI Lombard',
      message: 'Currently this product is not available, Kindly contact your partner',
      buttons: [{
        text: 'Okay',

      }]
    });
    await alert.present();
  }
  async presentAlert2(msg) {
    const alert = await this.alertCtrl.create({
      header: 'ICICI Lombard',
      message: 'Currently this product is not available, Kindly contact your partner',
      buttons: [{
        text: 'Okay',

      }]
    });
    await alert.present();
  }

}
