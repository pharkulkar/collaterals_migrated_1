import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import { CommonService } from 'src/app/services/common.service';
import { FormGroup, FormControl } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { NgxSpinnerService } from 'ngx-spinner';
import { NetworkService } from 'src/app/services/network.service';
import { debounceTime } from 'rxjs/operators';
import { OfflineManagerService } from 'src/app/services/offline-manager.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  loginForm: FormGroup;
  pageAuth: any;
  loading: any;
  disabledFeatureData: any;
  isConnected: any;
  isOnline:any;
  constructor(public router: Router,
    public loadingCtrl: LoadingController,
    public spinner: NgxSpinnerService,
    public cs: CommonService,
    public network: NetworkService) {
    console.log(this.network.platformStatus);
  }

  ngOnInit() {
    this.createLoginForm();
    localStorage.removeItem('userData');
    console.log("Loader", this.cs.showLoader);
    this.network.networkSubscriber().then(() => {
      console.log("Online", this.network.isConnected);
    });
   
  }


  createLoginForm() {
    this.loginForm = new FormGroup({
      //sanity
      // userName: new FormControl('ankur.saxena@icicilombard.com'),
      // passWord: new FormControl('hello123')
      // userName: new FormControl('IM-165736'),
      // passWord: new FormControl('lombard111')  
      // userName: new FormControl('IM-46232'),
      // passWord: new FormControl('Lombard123')
      // uat
      // userName: new FormControl('ankur.saxena@icicilombard.com'),
      // passWord: new FormControl('testing123')
      // prod
      // userName: new FormControl('IM-546438'),
      // passWord: new FormControl('Lombard2018')
      // userName: new FormControl('IM-501267'),
      // passWord: new FormControl('manita12345')

      //health travel mapped but travel product not working
      // userName: new FormControl('IM-566367'),
      // passWord: new FormControl('lombard123') 
      // userName: new FormControl('IM-552006'),
      // passWord: new FormControl('assure4542') 
      // userName: new FormControl('IM-556246'),
      // passWord: new FormControl('Asc360217') 
      // userName: new FormControl('IM-635185'),
      // passWord: new FormControl('secure20')  
      // userName: new FormControl('IM-532796'),
      // passWord: new FormControl('travel123'),
      // userName: new FormControl('IM-517069'),
      // passWord: new FormControl('u6in4rn5')
      // userName: new FormControl('IM-542002'),
      // passWord: new FormControl('harman12345')
      // userName: new FormControl('IM-546438'),
      // passWord: new FormControl('Lombard2018')


      userName: new FormControl(),
      passWord: new FormControl()
    });
  }

  // getDisabledFeaturesList(token:any):Promise<any>{
  //   return new Promise((resolve:any) => {
  //     this.cs.showLoader = true;
  //     this.cs.getWithParams1('/api/Agent/GetDisableFeatures', token).then((res:any) => {
  //       this.disabledFeatureData = res.DisableResponseList;
  //       let isKeralacess=this.disabledFeatureData.find((x:any) => x.Key == 'isKeralaCessEnabled').Value;
  //       localStorage.setItem('isKeralaCessEnabled',isKeralacess);
  //       this.cs.showLoader = false;
  //       resolve();
  //     });
  //   });
  // }

  login(form: any) {
    window.localStorage.clear();
    console.log(form.value);
    // this.spinner.show();
    this.cs.showLoader = true;
    this.cs.getHomeAuth('/api/agent/AgentAppMasterData', form.value).then((data: any) => {
      let userCredentials = form.value.userName + ":" + form.value.passWord;
      let authToken = "Basic " + btoa(userCredentials);
      localStorage.setItem('authToken', JSON.stringify(authToken));
      this.cs.authToken = authToken;
      this.pageAuth = data.MappedProduct;
      console.log("Response", this.pageAuth, data);
      localStorage.setItem('userData', JSON.stringify(data));
      let isKeralacess = data.DisableFeatures.isKeralaCessEnabled;
      localStorage.setItem('isKeralaCessEnabled', isKeralacess);
      this.router.navigateByUrl('dash-board');
      // this.spinner.hide();
      this.cs.showLoader = false;
    }).catch(err => {
      console.log("Something went wrong....");
      // this.spinner.hide();
      this.cs.showLoader = false;
    });
  }

  goToHealth() {
    console.log("Health");
    this.router.navigateByUrl('cal-health-premium');
  }

  goToTravel() {
    console.log("Travel");
    this.router.navigateByUrl('cal-travel-premium');
  }


}
