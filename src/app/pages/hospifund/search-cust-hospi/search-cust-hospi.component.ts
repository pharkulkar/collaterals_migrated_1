import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { EcryDecryService } from 'src/app/services/ecry-decry.service';
import { ModalController, ToastController } from '@ionic/angular';
import { LoadingService } from "../../../services/loading.service"
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-search-cust-hospi',
  templateUrl: './search-cust-hospi.component.html',
  styleUrls: ['./search-cust-hospi.component.scss'],
})
export class SearchCustHospiComponent implements OnInit {
  searchCustHospiForm: FormGroup;
  agentData: any;
  searchData = [];
  isShowUserList = false;
  data: any;
  selectedData: any;
  isPfSearch:boolean=false;
  searchType:any;

  constructor(public cs: CommonService, public toastCtrl: ToastController, public modalCtrl: ModalController, public alertController: AlertController, public xml2Json: XmlToJsonService, public enCnService: EcryDecryService, public apiloading: LoadingService) { }

  ngOnInit() {
    this.createForm();
    this.agentData = JSON.parse(localStorage.getItem('userData'));
    console.log("Agent Data", this.agentData);
  }

  createForm() {
    this.searchCustHospiForm = new FormGroup({
      custName: new FormControl(),
      custEmailId: new FormControl(),
      custMobileNo: new FormControl(),
      custID: new FormControl()
    });
  }

  formValidate(form: any) {
    let message: any;
    if ((this.searchCustHospiForm.value.custName == undefined || this.searchCustHospiForm.value.custName == null || this.searchCustHospiForm.value.custName == '')
      && (this.searchCustHospiForm.value.custEmailId == undefined || this.searchCustHospiForm.value.custEmailId == null || this.searchCustHospiForm.value.custEmailId == '')
      && (this.searchCustHospiForm.value.custMobileNo == undefined || this.searchCustHospiForm.value.custMobileNo == null || this.searchCustHospiForm.value.custMobileNo == '')
      && (this.searchCustHospiForm.value.custID == undefined || this.searchCustHospiForm.value.custID == null || this.searchCustHospiForm.value.custID == '')
    ) {
      this.presentAlert('Please enter atleast one field for searching');
      return false;
    }
    else {
      return true;
    }
  }

  async presentToast(msg: any) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  searchCustomer(form: any) {
    let valid = this.formValidate(form);

    if (valid) {
      this.cs.showLoader = true;
      if (form.value.custName == null || form.value.custName == undefined) {
        form.value.custName = '';
      }
      if (form.value.custEmailId == null || form.value.custEmailId == undefined) {
        form.value.custEmailId = '';
      }
      if (form.value.custMobileNo == null || form.value.custMobileNo == undefined) {
        form.value.custMobileNo = '';
      }
      if (form.value.custID == null || form.value.custID == undefined) {
        form.value.custID = '';
      }
      let request = {
        "CustomerName": form.value.custName,
        "CustomerEmail": form.value.custEmailId,
        "CustomerMobile": form.value.custMobileNo,
        "CustomerStateCode": form.value.custID
      }
      let str = JSON.stringify(request);
      if (form.value.custID != null && form.value.custID != undefined && form.value.custID != '') {
        this.cs.postWithParams('/api/Customer/FetchCustomerDetails?pFCustomerID=' + form.value.custID, form.value.custID).then((res: any) => {
          let temapArray=[];
      if(res.data != null)
      {
        temapArray.push(res.data);
        this.searchData =temapArray;
        if (this.searchData.length > 0) {
          this.isShowUserList = true;
          this.isPfSearch=true;
          this.searchType='1';
        } 
        else {
          this.isShowUserList = false;  
        }
        this.cs.showLoader = false;
      }
      else {
        this.isShowUserList = false;
        if(res.StatusMessage !=undefined){  this.presentAlert(res.StatusMessage);}
        this.cs.showLoader = false;
      }
        }, err => {
          this.presentAlert(err.error.ExceptionMessage);
          this.cs.showLoader = false;
        });
      }
      else {
        this.cs.postWithParams('/api/Customer/SearchCustomer', str).then((res: any) => {
          console.log("search user", res);
          this.searchData = res;
          if(res.length >0)
          {
            if (this.searchData.length > 0) {
              this.isShowUserList = true;
              this.cs.showLoader = false;
              this.isPfSearch=false;
              this.searchType='0';
            } else {
              this.isShowUserList = false;
            }
            this.cs.showLoader = false;
          }
          else{
            this.isShowUserList = false;
            if(res.StatusMessage !=undefined){  this.presentAlert(res.StatusMessage);}
            this.cs.showLoader = false;
          }
       
        }, err => {
          this.presentAlert(err.error.ExceptionMessage);
          this.cs.showLoader = false;
        });
      }


    }

  }

  selectedUser(data: any) {
    console.log(data);
    this.selectedData = data;
    localStorage.setItem('selectedCust', this.selectedData);
    this.modalCtrl.dismiss(this.selectedData,this.searchType);
  }

  @Output() newCustomerEnable: EventEmitter<any> = new EventEmitter<any>();

  closeModal() {
    this.modalCtrl.dismiss();
  }

  async presentAlert(message: any) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

}
