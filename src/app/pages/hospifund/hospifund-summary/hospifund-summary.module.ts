import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HospifundSummaryPage } from './hospifund-summary.page';

const routes: Routes = [
  {
    path: '',
    component: HospifundSummaryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HospifundSummaryPage]
})
export class HospifundSummaryPageModule {}
