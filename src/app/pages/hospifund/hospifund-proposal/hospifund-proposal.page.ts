
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AlertController, LoadingController } from '@ionic/angular';
import { CommonService } from 'src/app/services/common.service';
import { XmlToJsonService } from 'src/app/services/xml-to-json.service';
import { Router } from '@angular/router';
import { SearchCustHospiComponent } from '../search-cust-hospi/search-cust-hospi.component'
import * as _ from 'underscore';
import * as uuid from 'uuid';
import * as moment from 'moment';
import { config } from 'src/app/config.properties';
@Component({
  selector: 'app-hospifund-proposal',
  templateUrl: './hospifund-proposal.page.html',
  styleUrls: ['./hospifund-proposal.page.scss'],
})
export class HospifundProposalPage implements OnInit {
  showProposalPanel: boolean = true; customerType: string = 'new'; showBasicInfo: boolean = false; showInsuredInfo: boolean = true; showApplicantInfo: boolean = false;
  hasDiabetes: boolean = false; isCustomerNew: boolean = true; isMarried: boolean = true; isBothAddressSame: boolean = false;
  selectedNomineeTitle: any; showPerAddrDetails: boolean = false; showOtherDetails: boolean = false; showNomineeDetails: boolean = false;
  insured_name: any; insured_weight: any; pan_no: any; adhaar_no: any; applicant_name: any; applicant_number: any; applicant_dob: any;
  applicant_aaddr1: any; applicant_aaddr2: any; applicant_landmark: any; applicant_pincode: any; applicant_city: any;
  applicant_state: any; applicant_occpt: any; applicant_email: any; occupations: any; insured_relations: any; nominee_relations: any;
  nominee_name: any; nominee_dob: any; selectedRelation: any; allAilments: any; isChildorAdult: any; ageofInsured: any;
  applicant_title: any; states: any; cities: any; selectedInsuredTitle: any; insured_dob: any; nominee_selectedRelation: any;
  insuredDOB: any; nomineeDOB: any; proposalData: any; pincodeData: any; csc_pin: any; cscData: any; stateCityData: any;
  noofDays: any; siData: any; existing_iparter_id: any; dealData: any; loading: any; relations: any; occupationData: any;
  allAilmentsData: any; carried_over_dob: any; isLoader: boolean = true; cityData: any;
  state_code: any; city_code: any; isNew = true; searchCustModal: any; titleName: any; users: any; isUserModalOpened: any = 'no';
  titles = [{ "id": "0", "val": "Mrs." }, { "id": "1", "val": "Mr." }, { "id": "2", "val": "Ms." }, { "id": "21", "val": "Mx." }];
  nomineeTitles = [{ "id": "0", "val": "Mrs." }, { "id": "1", "val": "Mr." }, { "id": "2", "val": "Ms." }];
  isJammuandKashmir: any; pinData: any; policy_tenure: any; minDate: any; maxDate: any; applicantMaxDate: any;
  quoteData: any;
  constructor(private routes: Router,
    public loadingCtrl: LoadingController,
    public modalController: ModalController,
    public cs: CommonService,
    public xml2Json: XmlToJsonService,
    public modalCtrl: ModalController,
    public alertController: AlertController) { }

  ngOnInit() {
    this.quoteData = JSON.parse(localStorage.getItem('quoteData'));
    this.isChildorAdult = localStorage.getItem('isAdultorKid');
    this.ageofInsured = localStorage.getItem('ageofInsured');
    this.noofDays = localStorage.getItem('noofDays');
    this.siData = localStorage.getItem('siData');
    this.insured_dob = localStorage.getItem('insuredDOB');
    this.carried_over_dob = moment(localStorage.getItem('insuredDOB')).format('DD-MM-YYYY');
    this.selectedRelation = '';
    this.selectedInsuredTitle = ''; this.applicant_title = '';
    this.applicant_state = ''; this.applicant_city = ''; this.applicant_occpt = '';
    this.selectedNomineeTitle = ''; this.nominee_selectedRelation = '';
    this.customerType = 'new';
    this.policy_tenure = localStorage.getItem('Tenure');
    this.maxDate = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
    this.applicantMaxDate = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
    this.minDate = new Date(new Date().setFullYear(new Date().getFullYear() - 151));
    this.isJammuandKashmir = localStorage.getItem('hospiIsJammuandKashmir');
    if (this.isChildorAdult == 'Adult') {
      this.insured_relations = [
        { "id": "1", "val": "BROTHER" }, { "id": "2", "val": "BROTHER IN LAW" },
        { "id": "3", "val": "EMPLOYEE" }, { "id": "4", "val": "FATHER" },
        { "id": "5", "val": "FATHER IN LAW" }, { "id": "6", "val": "GRAND DAUGHTER" },
        { "id": "7", "val": "GRAND SON" }, { "id": "8", "val": "MOTHER" },
        { "id": "9", "val": "MOTHER IN LAW" }, { "id": "10", "val": "OTHERS" },
        { "id": "11", "val": "SELF" }, { "id": "12", "val": "SISTER" },
        { "id": "13", "val": "SON" }, { "id": "14", "val": "SON IN LAW" },
        { "id": "15", "val": "SPOUSE" }, { "id": "16", "val": "DAUGHTER" }
      ]
    }
    else {
      this.insured_relations = [
        { "id": "1", "val": "BROTHER" }, { "id": "2", "val": "EMPLOYEE" },
        { "id": "3", "val": "GRAND DAUGHTER" }, { "id": "4", "val": "GRAND SON" },
        { "id": "5", "val": "OTHERS" }, { "id": "6", "val": "SELF" },
        { "id": "7", "val": "SISTER" }, { "id": "8", "val": "SON" },
        { "id": "9", "val": "SPOUSE" }, { "id": "10", "val": "DAUGHTER" }
      ]
    }
    this.nominee_relations = [
      { "id": "1", "val": "BROTHER" }, { "id": "2", "val": "BROTHER IN LAW" },
      { "id": "3", "val": "EMPLOYEE" }, { "id": "4", "val": "FATHER" },
      { "id": "5", "val": "FATHER IN LAW" }, { "id": "6", "val": "GRAND DAUGHTER" },
      { "id": "7", "val": "GRAND SON" }, { "id": "8", "val": "MOTHER" },
      { "id": "9", "val": "MOTHER IN LAW" }, { "id": "10", "val": "OTHERS" },
      { "id": "11", "val": "SISTER" }, { "id": "12", "val": "SON" },
      { "id": "13", "val": "SON IN LAW" }, { "id": "14", "val": "SPOUSE" }, { "id": "15", "val": "DAUGHTER" }
    ]
    this.occupations = [{ "id": "SELF EMPLOYED", "val": "SELF EMPLOYED" }, { "id": "SALARIED", "val": "SALARIED" }, { "id": "OTHERS", "val": "OTHERS" }];
    this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').then(res => {
      this.states = res;
      this.isLoader = false;
    }, err => {
      this.presentAlert('Sorry Something went wrong');
      this.isLoader = false;
    });
    this.getAilmentsData();
  }

  getAilmentsData() {
    // this.isLoader = true;
    this.cs.getWithParams('/api/healthmaster/GetHealthAilmentList?isAgent=YES').then(res => {
      this.allAilmentsData = res;
      if (this.allAilmentsData.StatusCode == 1) {
        this.isLoader = true;
        this.allAilments = this.allAilmentsData.Details;
        this.isLoader = false;
        for (let i = 0; i < this.allAilments.length; i++) {
          this.allAilments[i].has_disease = false;
        }
      }
      this.isLoader = false;
    }, err => {
      this.presentAlert('Sorry Something went wrong');
      this.isLoader = false;
    });
  }
  getOccupation(val) {

  }
  getNomineeRealtion(val) {

  }
  getCity() {

  }
  getPerCity() {

  }

  async presentAlert(message: any) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  getOccupationforExistingUser(occupation) {
    for (let i = 0; i < this.occupations; i++) {
      if (this.occupations[i].OCCUPATIONID == occupation) {
        this.applicant_occpt = this.occupations[i].OCCUPATIONID;
      }
    }
  }
  getBasicInfo(val) {
    this.showBasicInfo = !this.showBasicInfo;
    // this.showInsuredInfo = false; this.showApplicantInfo = false;
    if (this.showBasicInfo) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("basicDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("basicDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("basicDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("basicDetails").style.backgroundRepeat = 'no-repeat';
    }
  }
  getInsuredInfo(val) {
    // this.showBasicInfo = !this.showInsuredInfo; this.showInsuredInfo = !val; this.showApplicantInfo = false;
    this.showInsuredInfo = !this.showInsuredInfo;
    // this.showBasicInfo = false; this.showApplicantInfo = false;
    if (this.showInsuredInfo) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("insuredDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("insuredDetails").style.backgroundRepeat = 'no-repeat';
    }
  }
  getApplicantInfo() {
    let isApplicantFeildsValid = this.continueApplicant();
    if (this.showApplicantInfo) {
      var imgUrl = './assets/images/minus.png';
      document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
    } else {
      var imgUrl = './assets/images/plus.png';
      document.getElementById("applicantDetails").style.background = "url(" + imgUrl + ")"; document.getElementById("applicantDetails").style.backgroundRepeat = 'no-repeat';
    }
  }
  continueApplicant() {
    if (this.selectedInsuredTitle == undefined || this.selectedInsuredTitle == null || this.selectedInsuredTitle == '') {
      this.presentAlert('Please select title of insured.');
      this.showInsuredInfo = true;
      return false;
    }
    else if (this.insured_name == undefined || this.insured_name == null) {
      this.presentAlert('Please select name of insured.');
      this.showInsuredInfo = true;
      return false;
    }
    else if (this.selectedRelation == undefined || this.selectedRelation == null || this.selectedRelation == '') {
      this.presentAlert('Please select relationship of insured.');
      this.showInsuredInfo = true;
      return false;
    }
    else if (this.insured_dob == undefined || this.insured_dob == null) {
      this.presentAlert('Please select date of birth of insured.');
      return false;
    }
    else {
      let tempData = this.insured_relations.find(x => x.id == this.selectedRelation);
      if (tempData.val.toUpperCase() == 'SELF') {
        this.applicant_name = this.insured_name;
        this.applicant_title = this.selectedInsuredTitle;
        this.applicant_dob = this.insured_dob;
        this.applicantMaxDate = new Date(new Date().setDate(new Date().getDate() - 91));
      }
      else {
        this.applicantMaxDate = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
        // this.applicant_name=null;this.applicant_title='';this.applicant_dob=null;
      }
      this.showInsuredInfo = false;
      this.showApplicantInfo = true;
      return true;

    }
  }
  showProposal(val) {
    this.showProposalPanel = !val;
  }
  getDiabetesData(val) {
    this.hasDiabetes = val;
    // if (val == true) {
    //   this.presentAlert('Currently PED policy not allowed for Hospifund');
    // }
    if (val == false) {
      for (let i = 0; i < this.allAilments.length; i++) {
        this.allAilments[i].has_disease = false;
      }
    }
    console.log("List", val);
  }

  async searchCust() {
    console.log("modal");
    this.searchCustModal = await this.modalCtrl.create({
      component: SearchCustHospiComponent,
      cssClass: 'custom_modal',
      componentProps: { users: this.users },
    });

    this.searchCustModal.onDidDismiss().then((data: any, type: any) => {
      console.log(data.data);
      if (!data.data) {
        this.isNew = true;
        this.isUserModalOpened = 'no'; this.isCustomerNew = true;
        this.existing_iparter_id = null;
      }
      if (data.role == '0') { this.getCustomerDetails(data.data); }
      else if (data.role == '1') { this.mapDataToExistanceUserByPFId(data.data); }
    })
    return await this.searchCustModal.present();

  }

  getCustomerDetails(userData: any) {
    console.log("User Data", userData);
    this.cs.showLoader = true;
    this.cs.getWithParams('/api/Customer/GetCustomerDetails/' + userData.CustomerID).then((res: any) => {
      console.log("Cust Details", res);
      this.getDataName(res).then(() => {
        this.cs.showLoader = false;
        this.mapDataToExistanceUser(res);
      }, (err: any) => {
        this.presentAlert('Sorry Something went wrong');
      });
    });
  }

  mapDataToExistanceUser(data: any) {
    if (data.AadhaarNo != null && data.AadhaarNo != undefined && data.AadhaarNo != '') {
      this.adhaar_no = data.AadhaarNo;
    }
    if (data.PANNumber != null && data.PANNumber != undefined && data.PANNumber != '') {
      this.pan_no = data.PANNumber;
    }
    if (data.TitleValue != null && data.TitleValue != undefined && data.TitleValue != '') {
      let tempTitle = this.titles.find(x => x.id == data.TitleValue.toString());
      if (tempTitle != undefined) { this.applicant_title = tempTitle.val }
    }
    if (data.Name != null && data.Name != undefined && data.Name != '') {
      this.applicant_name = data.Name;
    }
    if (data.DateOfBirth != null && data.DateOfBirth != undefined && data.DateOfBirth != '') {
      this.applicant_dob = moment(data.DateOfBirth, 'DD/MM/YYYY').format('DD-MM-YYYY')
    }
    if (data.MobileNumber != null && data.MobileNumber != undefined && data.MobileNumber != '') {
      this.applicant_number = data.MobileNumber;
    }
    if (data.PresentAddrLine1 != null && data.PresentAddrLine1 != undefined && data.PresentAddrLine1 != '') {
      this.applicant_aaddr1 = data.PresentAddrLine1;
    }
    if (data.PresentAddrLine1 != null && data.PresentAddrLine1 != undefined && data.PresentAddrLine1 != '') {
      this.applicant_aaddr2 = data.PresentAddrLine2;
    }
    if (data.PresentAddrLandmark != null && data.PresentAddrLandmark != undefined && data.PresentAddrLandmark != '') {
      this.applicant_landmark = data.PresentAddrLandmark
    }
    if (data.PresentAddrPincodeText != null && data.PresentAddrPincodeText != undefined && data.PresentAddrPincodeText != '') {
      this.applicant_pincode = data.PresentAddrPincodeText;
      this.getPincodeDetails(this.applicant_pincode);
    }
    if (data.OccupationText != undefined && data.OccupationText != null && data.OccupationText != '') {
      let tempOccupation = this.occupations.find(x => x.val == data.OccupationText);
      if (tempOccupation != undefined) { this.applicant_occpt = tempOccupation.val }
    }
    if (data.EmailAddress != undefined && data.EmailAddress != null && data.EmailAddress != '') {
      this.applicant_email = data.EmailAddress;
    }
    if (data.CustomerID != undefined && data.CustomerID != null && data.CustomerID != '') {
      this.existing_iparter_id = data.CustomerID;
    }
  }

  mapDataToExistanceUserByPFId(data: any) {
    if (data.AadhaarNo != null && data.AadhaarNo != undefined && data.AadhaarNo != '') {
      this.adhaar_no = data.AadhaarNo;
    }
    if (data.PAN_NO != null && data.PAN_NO != undefined && data.PAN_NO != '') {
      this.pan_no = data.PAN_NO;
    }
    if (data.TitleValue != null && data.TitleValue != undefined && data.TitleValue != '') {
      let tempTitle = this.titles.find(x => x.id == data.TitleValue.toString());
      if (tempTitle != undefined) { this.applicant_title = tempTitle.val }
    }
    if (data.UserName != null && data.UserName != undefined && data.UserName != '') {
      this.applicant_name = data.UserName;
    }
    if (data.DOB != null && data.DOB != undefined && data.DOB != '') {
      this.applicant_dob = moment(data.DOB).format('DD-MM-YYYY')
    }
    if (data.Mobile != null && data.Mobile != undefined && data.Mobile != '') {
      this.applicant_number = data.Mobile;
    }
    if (data.Address1 != null && data.Address1 != undefined && data.Address1 != '') {
      this.applicant_aaddr1 = data.Address1;
    }
    if (data.Address2 != null && data.Address2 != undefined && data.Address2 != '') {
      this.applicant_aaddr2 = data.Address2;
    }
    if (data.PresentAddrLandmark != null && data.PresentAddrLandmark != undefined && data.PresentAddrLandmark != '') {
      this.applicant_landmark = data.PresentAddrLandmark
    }
    if (data.PinCode != null && data.PinCode != undefined && data.PinCode != '') {
      this.applicant_pincode = data.PinCode;
      this.getPincodeDetails(this.applicant_pincode);
    }
    if (data.Occupation != undefined && data.Occupation != null && data.Occupation != '') {
      let tempOccupation = this.occupations.find(x => x.val == data.Occupation);
      if (tempOccupation != undefined) { this.applicant_occpt = tempOccupation.val }
    }
    if (data.EmailID != undefined && data.EmailID != null && data.EmailID != '') {
      this.applicant_email = data.EmailID;
    }
  }

  getDataName(data: any): Promise<any> {
    return new Promise((resolve) => {
      this.getTitleName(data);
      resolve();
    });
  }

  getTitleName(data: any) {
    if (data.TitleValue == '0') {
      this.titleName = 'Mrs.'
    } else if (data.TitleValue == '1') {
      this.titleName = 'Mr.'
    } else {
      this.titleName = 'Ms.'
    }
  }



  getCustomerData(val) {
    this.isCustomerNew = val;
    this.isNew = val;
    if (this.isCustomerNew == false) {
      this.isUserModalOpened = 'yes';
      this.searchCust();
    }
    else {
      console.log("value", this.isNew);
      this.isUserModalOpened = 'no'; this.existing_iparter_id = null;
      this.adhaar_no = ''; this.pan_no = ''; this.applicant_name = ''; this.applicant_title = '';
      this.applicant_number = ''; this.applicant_aaddr1 = ''; this.applicant_dob = '';
      this.applicant_aaddr2 = ''; this.applicant_landmark = '';
      this.applicant_pincode = ''; this.applicant_state = '';
      this.applicant_city = ''; this.applicant_occpt = ''; this.applicant_email = '';
    }
  }


  getPincodeDetails(val) {
    if (val.length == 6) {
      let body = val; this.cs.showLoader = true;
      this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).then((res) => {
        this.pinData = res;
        if (this.pinData.StatusCode == 1) {
          this.cityData = this.pinData.CityList;
          this.applicant_city = this.pinData.CityList[0].CityName;
          this.city_code = this.pinData.CityList[0].CityID
          if (this.pinData.CityList[0].StateName == undefined) {
            this.applicant_state = this.pinData.StateName;
            this.state_code = this.pinData.StateId;
          }
          else {
            this.applicant_state = this.pinData.CityList[0].StateName;
            this.state_code = this.pinData.CityList[0].StateId;
          }
          let proposalStateCity = { pin: body, city: this.pinData.CityList[0].CityName, state: this.pinData.StateName };
          localStorage.setItem('CorrrespondentStateCity', JSON.stringify(proposalStateCity));
          this.cs.showLoader = false;
        }
        else {
          this.applicant_city = null;
          this.applicant_state = null;
          this.cs.showLoader = false;
          this.presentAlert(this.pinData.StatusMessage);
        }
      });
    }
  }
  getNomineeTitle(val) {

  }
  getApplicantTitle(val) { }
  getRelation(val) {
    let tempData = this.insured_relations.find(x => x.id == val);
    if (tempData != undefined) {
      if (tempData.val.toUpperCase() == 'SELF') {
        this.applicant_dob = this.insured_dob;
        this.applicantMaxDate = new Date(new Date().setDate(new Date().getDate() - 91));
      }
      else {
        this.applicantMaxDate = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
      }
    }
  }
  getInsuredPersonTitle(val) {
    let tempData = this.insured_relations.find(x => x.id == this.selectedRelation);
    if (tempData != undefined) {
      if (tempData.val.toUpperCase() == 'SELF') {
        this.applicant_title = this.selectedInsuredTitle;
        this.applicantMaxDate = new Date(new Date().setDate(new Date().getDate() - 91));
      }
      else {
        this.applicantMaxDate = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
      }
    }
  }
  checkRelation(val) {
    let tempData = this.insured_relations.find(x => x.id == this.selectedRelation);
    if (tempData != undefined) {
      if (tempData.val.toUpperCase() == 'SELF') {
        this.applicant_name = this.insured_name;
      }
    }
  }
  showNomineePanel(val) {
    this.showNomineeDetails = !val;
  }
  showOtherPanel(val) {
    this.showOtherDetails = !val;
  }
  showPAddrPanel(val) {
    this.showPerAddrDetails = !val;
  }

  generateDealId() {
    let isValid = this.checkforValidation();
    if (isValid) {
      this.cs.showLoader = true;
      // let isPersonwithPED;
      // for (let i = 0; i < this.allAilments.length; i++) {
      //   if (this.allAilments[i].has_disease == true) {
      //     isPersonwithPED = true;
      //   }
      // }
      // if (isPersonwithPED) { this.presentAlert('Currently PED policy not allowed for Hospifund'); this.cs.showLoader = false; }
      // else {
      //   let userData = JSON.parse(localStorage.getItem('userData'));
      //   this.cs.getDealIdForProposal(userData.AgentID).then(res => {
      //     this.dealData = res;
      //     this.generateProposal(this.dealData);
      //   }, err => {
      //     this.presentAlert('Sorry Something went wrong');
      //     this.cs.showLoader = false;
      //   });
      // }
      let userData = JSON.parse(localStorage.getItem('userData'));
      this.cs.getDealIdForProposal(userData.AgentID).then(res => {
        this.dealData = res;
        this.generateProposal(this.dealData);
      }, err => {
        this.presentAlert('Sorry Something went wrong');
        this.cs.showLoader = false;
      });
    }
  }


  generateProposal(dealId) {
    const myId = uuid.v4(); let pedillness; let nomineeRelation, occupt, insuredRelation, Gender; let compAddr;
    let cDetails = localStorage.getItem('coverDetails');

    if (this.hasDiabetes == false) {
      pedillness = '';
    }
    else {
      pedillness = '';
      for (let i = 0; i < this.allAilments.length; i++) {
        if (this.allAilments[i].has_disease == true) {
          pedillness = pedillness + this.allAilments[i].Name + ',';
        }
      }
      if (pedillness == '' || pedillness == undefined) { pedillness = '' } else { pedillness = pedillness.replace(/,\s*$/, ""); }
    }
    for (let j = 0; j < this.nominee_relations.length; j++) {
      if (this.nominee_relations[j].id == this.nominee_selectedRelation) {
        nomineeRelation = this.nominee_relations[j].val;
      }
    }
    for (let j = 0; j < this.insured_relations.length; j++) {
      if (this.insured_relations[j].id == this.selectedRelation) {
        insuredRelation = this.insured_relations[j].val;
      }
    }
    compAddr = this.applicant_aaddr1;
    if (this.applicant_aaddr2 != undefined && this.applicant_aaddr2 != null && this.applicant_aaddr2 != '') {
      compAddr = compAddr + ',' + this.applicant_aaddr2;
    }
    if (this.applicant_landmark != undefined && this.applicant_landmark != null && this.applicant_landmark != '') {
      compAddr = compAddr + ',' + this.applicant_landmark;
    }
    if (this.selectedInsuredTitle == 'Ms.' || this.selectedInsuredTitle == 'Mrs.') { Gender = 'Female'; }
    else if (this.selectedInsuredTitle == 'Mr.') { Gender = 'Male' }
    else { Gender = 'Third gender' }
    let CData = JSON.parse(cDetails); let tempCData = [];
    for (let i = 0; i < CData.length; i++) {
      tempCData.push({ Column1: CData[i].Column1, CoverDescription: CData[i].CoverName, SumInsured: CData[i].CoverSI })
    }
    let tempinsuredDate = this.insured_dob;
    let tempnomineeDate = moment(this.nominee_dob).format('DD-MM-YYYY');
    let Tenure = localStorage.getItem('Tenure'); let corelationId;
    if (Tenure == '1') { corelationId = localStorage.getItem('guid1') }
    else if (Tenure == '2') { corelationId = localStorage.getItem('guid2') }
    else if (Tenure == '3') { corelationId = localStorage.getItem('guid3') }
    else { }
    if (this.pan_no == undefined || this.pan_no == null || this.pan_no == '') {
      this.pan_no == '';
    }
    let applicantDOB;
    if (this.isCustomerNew == true) {
      applicantDOB = moment(this.applicant_dob).format('YYYY-MM-DD');
    }
    else {
      applicantDOB = moment(this.applicant_dob, 'DD-MM-YYYY').format('YYYY-MM-DD');
    }
    let fieldgrid = [];
    fieldgrid.push({ "FieldName": "InsuredName", "FieldValue": this.insured_name },
      { "FieldName": "RelationshipwithApplicant", "FieldValue": insuredRelation },
      { "FieldName": "InsuredDateofBirth", "FieldValue": tempinsuredDate },
      { "FieldName": "Gender", "FieldValue": Gender },
      // { "FieldName": "PEDillness", "FieldValue": pedillness },
      { "FieldName": "NomineeName", "FieldValue": this.nominee_name },
      { "FieldName": "NomineeRelationshipwithInsured", "FieldValue": nomineeRelation },
      {
        "FieldName": "NomineeDateofBirth", "FieldValue": tempnomineeDate
      })
    if (this.applicant_occpt != undefined && this.applicant_occpt != null && this.applicant_occpt != '') {
      for (let j = 0; j < this.occupations.length; j++) {
        if (this.occupations[j].id == this.applicant_occpt) {
          occupt = this.occupations[j].val
        }
      }
      fieldgrid.push({ "FieldName": "Occupation", "FieldValue": occupt })
    }
    if (nomineeRelation == 'EMPLOYEE' || nomineeRelation == 'OTHERS' || nomineeRelation == 'EMPLOYER') {
      fieldgrid.push({ "FieldName": "80DApplicable", "FieldValue": 'Yes' })
      fieldgrid.push({ "FieldName": "80DBasicPremium", "FieldValue": Math.round(this.quoteData.basicPremium) })
      fieldgrid.push({ "FieldName": "80DTotalPremium", "FieldValue": Math.round(this.quoteData.totalPremium) })
      fieldgrid.push({ "FieldName": "80DTax", "FieldValue": Math.round(this.quoteData.totalTax) })
    }
    let applicant_gender;
    if (this.applicant_title == '0' || this.applicant_title == '2') { applicant_gender = 'Female' }
    else if (this.applicant_title == '1') { applicant_gender = 'Male' }
    else { applicant_gender = 'Third Gender' }
    for (let j = 0; j < this.nominee_relations.length; j++) {
      if (this.nominee_relations[j].id == this.nominee_selectedRelation) {
        nomineeRelation = this.nominee_relations[j].val;
      }
    }
    let nominee_date = moment(this.nominee_dob).format('YYYY-MM-DD');
    let quoteDetails = JSON.parse(localStorage.getItem('quoteData'));
    let quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
    let tenure = quoteRequest.PolicyTenure;
    let policyStartDate = moment(quoteDetails.policyStartDate).format('YYYY-MM-DD');
    let policyEndDate = moment(quoteDetails.policyEndDate).format('YYYY-MM-DD');
    let postData = {
      "BusinessType": "New Business",
      "PolicyTenure": tenure,
      "PolicyStartDate": policyStartDate,
      "PolicyEndDate": policyEndDate,
      "ILProductCode": "4150",
      "SubProductCode": config.hospiFundProductCode,
      "FinancierDetails_PrefixSufix": null,
      "FinancierDetails_Remarks": null,
      "CoinsuranceType": null,
      "SpecialConditionDescription": null,
      "DealId": dealId,
      "StampDutyApplicability": false,
      "AlternatePolicyNo": "",
      "riskDetails": {
        "PlanCode": config.hospiFundPlanCode,
        // "Product24062544":nominee_date,
        "InsuredDateOfBirth": this.insured_dob,
        "InwardDate": localStorage.getItem('inwardDate'),
        "MasterPolicyNumber": "",
        "PreExistingIllness": '',
        // "PreExistingIllness": pedillness,
        "NomineeName": this.nominee_name,
        "NomineeRelationship": nomineeRelation,
        "NomineeDOB": nominee_date,
        "AppointeeName": "",
        "AppointeeRelationship": "",
        "AppointeeDOB": "",
        "RisksList": [
          {
            "RiskSIComponent": "Person",
            "BasisOfSumInsured": "Reinstatement Value",
            "coverDetailsList": tempCData
          }
        ]
      },
      "FieldsGrid": fieldgrid,
      "FinancierDetails": null,
      "CustomerDetails": {
        "CustomerType": "Individual",
        "CustomerName": this.applicant_name,
        "DateOfBirth": applicantDOB,
        "PinCode": parseInt(this.applicant_pincode),
        "PANCardNo": this.pan_no,
        "Email": this.applicant_email,
        "MobileNumber": this.applicant_number,
        "AddressLine1": compAddr,
        "CountryCode": "100",
        "StateCode": this.state_code,
        "CityCode": this.city_code,
        "Gender": applicant_gender,
        "MobileISD": null,
        "GSTDetails": null,
        "AadharNumber": null,
        "IsCollectionofform60": false,
        "AadharEnrollmentNo": null,
        "eIA_Number": null,
        "CustomerID": null
      },
      "CorrelationId": corelationId,
      "insuredDataDetails": [
        {
          "InsuredName": this.insured_name,
          "InsuredID": "",
          "InsuredDOB": tempinsuredDate,
          "InsuredAge": this.ageofInsured,
          "InsuredGender": Gender,
          "InsuredRelation": insuredRelation,
          "PreIllness": pedillness,
          "InsuredPAN": "",
          "InsuredAadhar": "",
          "InsuredOccupation": "",
          "InsuredEmailID": "",
          "InsuredPassportNo": "",
          "InsuredMobileNo": ""
        }
      ]
    }
    localStorage.setItem('Proposal_Request', JSON.stringify(postData));
    this.isLoader = true;
    //General/Proposal/Create
    //Zerotat/Create/PROPOSAL
    this.cs.postforHospifund('/ilservices/misc/v1/General/Proposal/Create', postData).then(res => {
      this.proposalData = res;
      if (this.proposalData.status != "SUCCESS") {
        this.presentAlert(this.proposalData.message);
        this.cs.showLoader = false;
      }
      else {
        localStorage.setItem('proposalData', JSON.stringify(this.proposalData));
        this.callGetPinService();
      }
    }, err => {
      this.presentAlert('Sorry Something went wrong');
      this.cs.showLoader = false;
    });

  }
  submitProposal() {
    // if (this.hasDiabetes) {
    //   this.presentAlert('Currently PED policy not allowed for Hospifund');
    // }
    // else {
    //   this.generateDealId();
    // }
    this.generateDealId();
  }

  callGetPinService() {
    this.isLoader = true;
    let body = { "Pincode": this.applicant_pincode, "CityID": this.city_code };
    this.cs.postWithParams('/api/RTOList/GetPincodeID', body).then(res => {
      this.pincodeData = res;
      this.csc_pin = this.pincodeData.Details[0].Value
      this.callCSCService();
    }, err => {
      this.presentAlert('Sorry Something went wrong');
      this.cs.showLoader = false;
      this.callCSCService();
    });
    // this.cs.submitPincode(this.applicant_pincode, this.city_code).then(res => {
    //   this.pincodeData = res;
    //   this.isLoader = false;
    // }, err => {
    //   let data = this.xml2Json.xmlToJson(err.error.text);
    //   this.csc_pin = data.NameValueResponse.Details.NameValueOutputResponse.Value;
    //   this.cs.showLoader = false;
    //   this.callCSCService();
    // });
  }

  callCSCService() {
    let Gender, stateName, Title, gstStateName, nomineeRelation;
    this.isLoader = true;
    const myId = uuid.v4()
    if (this.applicant_title == 'Ms.' || this.applicant_title == 'Mrs.') { Gender = '2'; } else if (this.applicant_title == 'Mr.') { Gender = '1' } else { Gender = 'T' }
    Title = this.titles.find(x => x.val == this.applicant_title).id;

    let tempinsuredDate = moment(this.insured_dob).format('DD-MM-YYYY');
    let quoteDetails = JSON.parse(localStorage.getItem('quoteData'));
    let quoteRequest = JSON.parse(localStorage.getItem('quoteRequest'));
    let tenure = quoteRequest.PolicyTenure;
    let policyStartDate = moment(quoteDetails.policyStartDate).format('YYYY-MM-DD');
    let policyEndDate = moment(quoteDetails.policyEndDate).format('YYYY-MM-DD');
    for (let i = 0; i < this.states.length; i++) {
      if (this.states[i].StateName == this.applicant_state) {
        stateName = this.states[i].StateName;
        gstStateName = this.states[i].GSTStateCode;
      }
    }
    if (this.existing_iparter_id == undefined || this.existing_iparter_id == null) {
      this.existing_iparter_id = 0;
    }
    if (this.applicant_aaddr2 == undefined || this.applicant_aaddr2 == null) {
      this.applicant_aaddr2 = null;
    }
    if (this.pan_no == null || this.pan_no == undefined) {
      this.pan_no = '';
    }
    if (this.adhaar_no == null || this.adhaar_no == undefined) {
      this.adhaar_no = '';
    }
    if (this.applicant_aaddr2 == null || this.applicant_aaddr2 == undefined) {
      this.applicant_aaddr2 = '';
    }
    if (this.applicant_landmark == null || this.applicant_landmark == undefined) {
      this.applicant_landmark = '';
    }
    let userData = JSON.parse(localStorage.getItem('userData'));
    let cDetails = localStorage.getItem('coverDetails');
    let CData = JSON.parse(cDetails); let tempCData = [];
    for (let i = 0; i < CData.length; i++) {
      tempCData.push({ CoverName: CData[i].CoverName, CoverSI: CData[i].CoverSI, Column1: CData[i].Column1 })
    }
    for (let j = 0; j < this.nominee_relations.length; j++) {
      if (this.nominee_relations[j].id == this.nominee_selectedRelation) {
        nomineeRelation = this.nominee_relations[j].val;
      }
    }
    let postData1 =
    {
      "userid": userData.AgentID,
      "ipAddress": config.ipAddress,
      "productcode": config.hospiFundProductCode,
      "plancode": config.hospiFundPlanCode,
      "productType": null,
      "gstStateCode": this.state_code,
      "gstStateName": stateName,
      "tenure": tenure,
      "policyStartDate": policyStartDate,
      "policyEndDate": policyEndDate,
      "basicPremium": this.proposalData.gcnetpremium,
      "totalTax": this.proposalData.gcservicetax,
      "totalPremium": this.proposalData.gctotalpremium,
      "coverPremium": 0,
      "guid": quoteDetails.correlationId,
      "ipartner_user_id": this.existing_iparter_id,
      "title": Title,
      "lastName": this.applicant_name,
      "gender": Gender,
      "panno": this.pan_no,
      "aadharNo": this.adhaar_no,
      "dateOfBirth": tempinsuredDate,
      "line1": this.applicant_aaddr1,
      "line2": this.applicant_aaddr2,
      "line3": null,
      "countryId": "100",
      "stateId": this.state_code,
      "cityId": this.city_code,
      "pinCodeId": this.csc_pin,
      "areaVillageId": 0,
      "emailAddress": this.applicant_email,
      "telephoneNumber": this.applicant_number,
      "modeID": null,
      "dealId": this.dealData,
      "pf_proposalnumber": this.proposalData.proposalNumber,
      "pF_CUSTID": this.proposalData.customerId,
      "addressId": 0,
      "cgstAmt": this.proposalData.gstserviceresponse.gstservicedetails.cgstamount,
      "sgstAmt": this.proposalData.gstserviceresponse.gstservicedetails.sgstamount,
      "igstAmt": this.proposalData.gstserviceresponse.gstservicedetails.igstamount,
      "ugstAmt": this.proposalData.gstserviceresponse.gstservicedetails.utgstamount,
      "totalGSTAmt": this.proposalData.gstserviceresponse.gstservicedetails.cgstamount,
      "cgstAmtRate": this.proposalData.gstserviceresponse.gstservicedetails.cgstrate,
      "sgstAmRate": this.proposalData.gstserviceresponse.gstservicedetails.sgstamount,
      "igstAmtRate": this.proposalData.gstserviceresponse.gstservicedetails.igstrate,
      "utgstAmtRate": this.proposalData.gstserviceresponse.gstservicedetails.utgstrate,
      "totalGSTAmtRate": this.proposalData.gstserviceresponse.gstservicedetails.gstnetrate,
      "taxrate": this.proposalData.gstserviceresponse.gstservicedetails.gstnetrate,
      "CoverDetails": tempCData
    }
    if (nomineeRelation == 'EMPLOYEE' || nomineeRelation == 'OTHERS' || nomineeRelation == 'EMPLOYER') {
      postData1['80DApplicable'] = 'Yes';
      postData1['80DBasicPremium'] = Math.round(this.quoteData.basicPremium);
      postData1['80DTotalPremium'] = Math.round(this.quoteData.totalPremium);
      postData1['80DTax'] = Math.round(this.quoteData.totalTax);

    }
    localStorage.setItem('customerRequest', JSON.stringify(postData1));
    this.cs.postWithParams('/api/HospiFund/CSCDataSaved', postData1).then(res => {
      this.cscData = res;
      localStorage.setItem('PolicyId', this.cscData);
      this.cs.showLoader = false;
      this.routes.navigateByUrl('hospifund-summary');
    }, err => {
      this.presentAlert('Sorry Something went wrong');
      this.cs.showLoader = false;
    });
  }

  checkforValidation() {
    if (this.pan_no != undefined || this.pan_no != null) {
      if (this.pan_no != '') {
        if (!this.validatepanCard(this.pan_no)) {
          this.presentAlert('Please enter valid pan number');
          return false;
        }
      }
    }
    if (this.adhaar_no != undefined || this.adhaar_no != null) {
      if (this.adhaar_no != '') {
        if (!this.aadhaarValidationfunction(this.adhaar_no.toString())) {
          this.presentAlert('Please enter valid aadhar number');
          return false;
        }
      }
    }
    if (this.applicant_dob == undefined || this.applicant_dob == null || this.applicant_dob == '') {
      this.presentAlert('Please enter applicant date of birth');
      return false;
    }
    if (this.nominee_dob == undefined || this.nominee_dob == null || this.nominee_dob == '') {
      this.presentAlert('Please enter nominee date of birth');
      return false;
    }
    if (this.applicant_number != undefined || this.applicant_number != null) {
      if (this.applicant_number.length < 10) {
        this.presentAlert('Please enter valid mobile number');
        return false;
      }
    }
    else {
      this.presentAlert('Please enter mobile number'); return false;
    }
    if ((this.applicant_pincode != undefined || this.applicant_pincode != null)) {
      if (this.applicant_pincode.length < 6) {
        this.presentAlert('Please enter valid pincode'); return false;
      }

    }
    else {
      this.presentAlert('Please enter  pincode'); return false;
    }
    if (!this.validateEmail(this.applicant_email)) {
      this.presentAlert('Please enter valid email address.');
      return false;
    }
    else if (this.applicant_email == undefined || this.applicant_email == null) {
      this.presentAlert('Please enter email address'); return false;
    }
    else if ((this.applicant_dob == undefined || this.applicant_dob == null) || this.applicant_dob == '') {
      this.presentAlert('Please enter date of birth'); return false;
    }
    else if (this.applicant_title == undefined || this.applicant_title == null || this.applicant_title == '') {
      this.presentAlert('Please enter applicant title'); return false;
    }
    else if (this.applicant_name == undefined || this.applicant_name == null) {
      this.presentAlert('Please enter applicant name'); return false;
    }
    else if (this.applicant_state == undefined || this.applicant_state == null || this.applicant_state == '') {
      this.presentAlert('Please enter state'); return false;
    }
    else if (this.applicant_city == undefined || this.applicant_city == null || this.applicant_city == '') {
      this.presentAlert('Please enter city'); return false;
    }
    else if (this.applicant_aaddr1 == undefined || this.applicant_aaddr1 == null || this.applicant_aaddr1 == '') {
      this.presentAlert('Please enter address 1'); return false;
    }
    else if (this.selectedNomineeTitle == undefined || this.selectedNomineeTitle == null || this.selectedNomineeTitle == '') {
      this.presentAlert('Please enter nominee title'); return false;
    }
    else if (this.nominee_name == undefined || this.nominee_name == null) {
      this.presentAlert('Please enter nominee name'); return false;
    }
    else if (this.nominee_dob == undefined || this.nominee_dob == null || this.nominee_dob == '') {
      this.presentAlert('Please enter nominee dob'); return false;
    }
    else if (this.nominee_selectedRelation == undefined || this.nominee_selectedRelation == null || this.nominee_selectedRelation == '') {
      this.presentAlert('Please enter nominee relationship'); return false;
    }
    else {
      return true;
    }

  }
  validatepanCard(val) {
    if (new RegExp('[A-Z]{5}[0-9]{4}[A-Z]{1}').test(val)) {
      return true;
    }
    return false;
  }
  aadhaarValidationfunction(val) {
    if (new RegExp('[0-9]{12}').test(val) && val.length == 12) {
      return true;
    }
    return false;
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  async showLoading() {
    this.loading = await this.loadingCtrl.create({
      spinner: 'lines',
      message: 'Please wait...',
      translucent: true
    });
    return await this.loading.present();
  }
  home(ev: any) {
    console.log("Home", ev);
    this.cs.goToHome();
  }
  getCityList(city) {
    this.city_code = this.pinData.CityList.find((x: any) => x.CityName == city).CityID;
  }
  alphaOnly(event: any): boolean {
    const key = event.keyCode;
    return ((key >= 65 && key <= 90) || key == 8 || key == 32);
  }

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    console.log(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
