import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MatFormFieldModule, MatNativeDateModule, MatInputModule  } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { HospifundProposalPage } from './hospifund-proposal.page';
import { SearchCustHospiComponent } from '../search-cust-hospi/search-cust-hospi.component'
import { ApplicationPipesModule } from 'src/app/application-pipes/application-pipes.module';
const routes: Routes = [
  {
    path: '',
    component: HospifundProposalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApplicationPipesModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HospifundProposalPage,SearchCustHospiComponent],
  entryComponents: [SearchCustHospiComponent, SearchCustHospiComponent],
})
export class HospifundProposalPageModule {}
