export var config = {

  ipAddress: "COBRAND",

  //prod old  
  // baseURL: 'https://origin21-ipartner.icicilombard.com/mobileagentapi',

  //prod new
  // baseURL: 'https://ipartner.icicilombard.com/mobileagentapi',
  // homeURL: 'https://ipartner.icicilombard.com/WebPages/SwapSession.aspx?token=',
  // loginurl: 'https://ipartner.icicilombard.com/WebPages/Login.aspx',
  // hospifundBaseURL: 'https://app9.icicilombard.com',
  // paymentURL: 'https://ipartner.icicilombard.com/mobileagentapi/ipartnermsite/webpage/#/payment',
  // razorpayURL: 'https://ipartner.icicilombard.com',
  // hospiFundPlanCode: '16079',
  // hospiFundProductCode: '6001',

  //  aws sanity
  baseURL: 'https://cldilmobilapp01.insurancearticlez.com/MobileAPI',
  homeURL: 'https://cldiliptrapp01.cloudapp.net/WebPages/SwapSession.aspx?Healthtoken=',
  loginurl: 'https://cldiliptrapp01.cloudapp.net/WebPages/Login.aspx',
  hospifundBaseURL: 'https://cldilbizapp02.cloudapp.net:9001',
  paymentURL: 'https://cldilmobilapp01.insurancearticlez.com/ipartnermsite/webpagesanity/#/payment',
  razorpayURL: 'https://cldilmobilapp01.insurancearticlez.com',
  hospiFundPlanCode: '10528',
  hospiFundProductCode: '1008',
  homeinsurl: 'https://cldilmobilapp01.insurancearticlez.com/ipartnermsite/home-insurance',

  // aws UAT
  // baseURL: 'https://cldilmobilapp01.insurancearticlez.com/MobileAPIUAT',
  // homeURL: 'https://cldiliptrapp01.cloudapp.net/WebPages/SwapSession.aspx?Healthtoken=',
  // loginurl: 'https://cldiliptrapp01.cloudapp.net/WebPages/Login.aspx',
  // hospifundBaseURL: 'https://cldilbizapp02.cloudapp.net:9001',
  // paymentURL: 'https://cldilmobilapp01.insurancearticlez.com/ipartnermsite/webpagesanity/#/payment',
  // razorpayURL: 'https://cldilmobilapp01.insurancearticlez.com',
  // hospiFundPlanCode: '10528',
  // hospiFundProductCode: '1008',
  // homeinsurl: 'https://cldilmobilapp01.insurancearticlez.com/ipartnermsite/home-insuranceuat',

}