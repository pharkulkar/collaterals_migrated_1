import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'swap', pathMatch: 'full' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'health', loadChildren: './pages/health/cal-health-premium/cal-health-premium.module#CalHealthPremiumPageModule' },
  { path: 'travel', loadChildren: './pages/travel/cal-travel-premium/cal-travel-premium.module#CalTravelPremiumPageModule'},
  { path: 'dash-board', loadChildren: './pages/dash-board/dash-board.module#DashBoardPageModule' },
  { path: 'health-proposal', loadChildren: './pages/health/health-proposal/health-proposal.module#HealthProposalPageModule' },
  
  { path: 'health-summary', loadChildren: './pages/health/health-summary/health-summary.module#HealthSummaryPageModule' },
  { path: 'swap', loadChildren: './pages/swap/swap.module#SwapPageModule' },
  { path: 'travel-quote', loadChildren: './pages/travel/travel-quote/travel-quote.module#TravelQuotePageModule' },
  { path: 'travel-premium', loadChildren: './pages/travel/travel-premium/travel-premium.module#TravelPremiumPageModule' },
  { path: 'travel-summary', loadChildren: './pages/travel/travel-summary/travel-summary.module#TravelSummaryPageModule' },
  { path: 'travel-proposal', loadChildren: './pages/travel/travel-proposal/travel-proposal.module#TravelProposalPageModule' },
  { path: 'payment', loadChildren: './pages/payments/payment/payment.module#PaymentPageModule' },
  { path: 'payment-mode', loadChildren: './pages/payments/payment-mode/payment-mode.module#PaymentModePageModule' },
  { path: 'payment-failed', loadChildren: './pages/payments/payment-failed/payment-failed.module#PaymentFailedPageModule' },
  { path: 'cheque-confirmation', loadChildren: './pages/payments/cheque-confirmation/cheque-confirmation.module#ChequeConfirmationPageModule' },
  { path: 'payment-confirmation', loadChildren: './pages/payments/payment-confirmation/payment-confirmation.module#PaymentConfirmationPageModule' },
  
  
  { path: 'razor-pop-up', loadChildren: './pages/payments/razor-pop-up/razor-pop-up.module#RazorPopUpPageModule' },
  { path: 'razor-pay-fallback', loadChildren: './pages/payments/razor-pay-fallback/razor-pay-fallback.module#RazorPayFallbackPageModule' },
  
  
  
  { path: 'razor-pay-confirmation', loadChildren: './pages/payments/razor-pay-confirmation/razor-pay-confirmation.module#RazorPayConfirmationPageModule' },
  
  { path: 'hospifund-proposal', loadChildren: './pages/hospifund/hospifund-proposal/hospifund-proposal.module#HospifundProposalPageModule' },
  { path: 'hospifund-summary', loadChildren: './pages/hospifund/hospifund-summary/hospifund-summary.module#HospifundSummaryPageModule' },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: true})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
